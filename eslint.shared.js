const { createConfig } = require('eslint-config-galex/dist/createConfig');
const {
  createJestOverride
} = require('eslint-config-galex/dist/overrides/jest');
const {
  createReactOverride
} = require('eslint-config-galex/dist/overrides/react');
const {
  createTypeScriptOverride
} = require('eslint-config-galex/dist/overrides/typescript');

module.exports = {
  createConfig: (cwd, dependencies, customOverrides = []) => {
    return createConfig({
      cwd,
      rules: {
        'func-names': 'off', // would rather convert to arrow functions than come up with names, but can't due to `this` references
        'no-negated-condition': 'off', // ternaries are sometimes more readable when `true` branch is most significant branch
        'sort-keys-fix/sort-keys-fix': 'off', // keys should be sorted based on significance
        'import/no-default-export': 'off', // default exports are common in React
        'sonarjs/elseif-without-else': 'off', // `if`/`else if` serves a different purpose than `switch`
        'unicorn/prefer-prototype-methods': 'off', // not really more readable and makes Jest crash
        'unicorn/consistent-destructuring': 'off', // properties available after typeguard may be tedious to destructure (e.g. in JSX)

        // Prefer explicit, consistent return - e.g. `return undefined;`
        'unicorn/no-useless-undefined': 'off',
        'consistent-return': 'error',

        // Enforce curly braces, but only for blocks that span multiple lines
        curly: ['error', 'multi-line'],

        // With this rules if-blocks dont have anymore semantic
        'unicorn/no-lonely-if': 'off',
        'sonarjs/no-collapsible-if': 'off',

        // ===== For ease of migration to `eslint-config-galex` =====
        'new-cap': 'off',
        'no-console': 'off',
        'prefer-destructuring': 'off',
        'import/no-namespace': 'off',
        'import/order': 'off',
        'promise/prefer-await-to-callbacks': 'off',
        'promise/prefer-await-to-then': 'off',
        'require-unicode-regexp': 'off',
        'sonarjs/cognitive-complexity': 'off',
        'sonarjs/no-duplicate-string': 'off',
        'sonarjs/no-identical-expressions': 'off',
        'sonarjs/no-identical-functions': 'off',
        'unicorn/consistent-function-scoping': 'off',
        'unicorn/custom-error-definition': 'off',
        'unicorn/error-message': 'off',
        'unicorn/prefer-add-event-listener': 'off',
        'unicorn/prefer-spread': 'off'
      },
      overrides: [
        {
          files: ['**/*.{js,jsx}'],
          rules: {
            // ===== For ease of migration to `eslint-config-galex` =====
            'consistent-return': 'off',
            eqeqeq: 'off',
            'no-eq-null': 'off',
            'no-implicit-coercion': 'off', // fixable
            'no-promise-executor-return': 'off',
            'no-prototype-builtins': 'off',
            'prefer-promise-reject-errors': 'off',
            'import/dynamic-import-chunkname': 'off',
            'import/no-anonymous-default-export': 'off',
            'promise/no-nesting': 'off',
            'promise/no-promise-in-callback': 'off',
            'promise/param-names': 'off',
            'sonarjs/prefer-immediate-return': 'off', // fixable
            'unicorn/better-regex': 'off', // fixable
            'unicorn/catch-error-name': 'off', // fixable
            'unicorn/escape-case': 'off', // fixable
            'unicorn/explicit-length-check': 'off',
            'unicorn/no-array-push-push': 'off',
            'unicorn/no-hex-escape': 'off', // fixable
            'unicorn/no-new-array': 'off', // fixable
            'unicorn/no-this-assignment': 'off',
            'unicorn/no-unsafe-regex': 'off',
            'unicorn/no-zero-fractions': 'off', // fixable
            'unicorn/numeric-separators-style': 'off',
            'unicorn/prefer-export-from': 'off', // fixable
            'unicorn/prefer-number-properties': 'off', // fixable
            'unicorn/prefer-optional-catch-binding': 'off', // fixable
            'unicorn/prefer-query-selector': 'off', // fixable
            'unicorn/prefer-regexp-test': 'off', // fixable
            'unicorn/prefer-string-slice': 'off',
            'unicorn/prefer-switch': 'off' // fixable
          }
        },
        createReactOverride({
          ...dependencies,
          rules: {
            'react/jsx-no-constructed-context-values': 'off', // too strict

            // `useCameraState` accepts an array of deps like `useEffect`
            // https://github.com/facebook/react/tree/main/packages/eslint-plugin-react-hooks#advanced-configuration
            // 'react-hooks/exhaustive-deps': [
            //   'error',
            //   { additionalHooks: '(useCameraState)' }
            // ],

            // ===== For ease of migration to `eslint-config-galex` =====
            'jsx-a11y/click-events-have-key-events': 'off',
            'jsx-a11y/control-has-associated-label': 'off',
            'jsx-a11y/no-autofocus': 'off',
            'jsx-a11y/no-noninteractive-element-interactions': 'off',
            'jsx-a11y/no-static-element-interactions': 'off',
            'react/jsx-handler-names': 'off',
            'react/jsx-key': 'off',
            'react/jsx-no-useless-fragment': 'off',
            'react/no-array-index-key': 'off',
            'react/no-children-prop': 'off',
            'react/no-find-dom-node': 'off',
            'react/no-unused-prop-types': 'off',
            'react/no-unused-state': 'off',
            'react/static-property-placement': 'off',
            'react-hooks/exhaustive-deps': 'off'
          }
        }),
        createTypeScriptOverride({
          ...dependencies,
          rules: {
            '@typescript-eslint/ban-ts-comment': 'off', // too strict
            '@typescript-eslint/lines-between-class-members': 'off', // allow grouping single-line members
            '@typescript-eslint/prefer-nullish-coalescing': 'off', // `||` is often conveninent and safe to use with TS
            '@typescript-eslint/explicit-module-boundary-types': 'off', // worsens readability sometimes (e.g. for React components)
            '@typescript-eslint/no-unnecessary-type-arguments': 'off', // lots of false positives
            '@typescript-eslint/no-empty-function': 'off', // pointless
            '@typescript-eslint/member-ordering': 'off', // counter intuitive
            '@typescript-eslint/parameter-properties': 'off', // confusing and less explicit

            // Allow removing properties with destructuring
            // '@typescript-eslint/no-unused-vars': [
            //   'warn',
            //   { ignoreRestSiblings: true }
            // ],

            // Allow writing void-returning arrow functions in shorthand to save space
            '@typescript-eslint/no-confusing-void-expression': [
              'error',
              { ignoreArrowShorthand: true }
            ],

            // Prefer `interface` over `type`
            '@typescript-eslint/consistent-type-definitions': [
              'error',
              'interface'
            ],

            '@typescript-eslint/consistent-type-assertions': [
              'error',
              {
                assertionStyle: 'as',
                objectLiteralTypeAssertions: 'allow' // `never` is too strict
              }
            ],

            // ===== For ease of migration to TypeScript =====
            '@typescript-eslint/no-explicit-any': 'off',
            '@typescript-eslint/no-unnecessary-condition': 'off',
            '@typescript-eslint/no-unused-vars': 'off',
            '@typescript-eslint/restrict-template-expressions': 'off',
            '@typescript-eslint/restrict-plus-operands': 'off'

            // Disallows calling function with value of type `any` (disabled by galex due to false positives)
            // Re-enabling because has helped fix a good number of true positives
            // '@typescript-eslint/no-unsafe-argument': 'warn'
          }
        }),
        createJestOverride({
          ...dependencies,
          rules: {
            'jest/no-focused-tests': 'warn', // warning instead of error
            'jest/prefer-strict-equal': 'off', // `toEqual` is shorter and sufficient in most cases
            'jest-formatting/padding-around-all': 'off', // allow writing concise two-line tests
            'jest/require-top-level-describe': 'off', // filename should already be meaningful, extra nesting is unnecessary
            'jest/no-conditional-in-test': 'off', // false positives in E2E tests (snapshots), and too strict (disallows using `||` for convenience)
            'testing-library/no-unnecessary-act': 'off' // `act` is sometimes required when advancing timers manually
          }
        }),
        ...customOverrides
      ]
    });
  }
};
