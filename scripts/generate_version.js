const { exec } = require('child_process');
const fs = require('fs');

function generateVersion() {
  exec('git describe --tags --always', (gitErr, stdout) => {
    if (gitErr) {
      console.log('Could not run git command to get version');
      console.log(gitErr);
      return;
    }

    const version = {
      version: stdout.replace(/\n/, '')
    };

    fs.writeFile(
      './public/meta.json',
      `${JSON.stringify(version)}\n`,
      'utf8',
      writeErr => {
        if (writeErr) {
          console.log('An error occured while writing meta.json');
          console.log(writeErr);
          return;
        }

        console.log('meta.json file has been written');
      }
    );
  });
}

console.log('Generating meta.json with build version');
generateVersion();
