# Contributing

- [Quick start](#quick-start-)
- [Development](#development)
  - [`pnpm` cheat sheet](#pnpm-cheat-sheet)
  - [Dependency management](#dependency-management)
  - [Why not Node 18?](#why-not-node-18)
- [Building for production](#building-for-production)
- [Testing](#testing)
- [Code quality](#code-quality)
  - [Editor integration](#editor-integration)
- [CI](#ci)

## Quick start 🚀

To get started, you'll need:

- [Node 16](https://nodejs.org/) - e.g. `nvm install 16`, `volta install node@16`, etc.
- [pnpm 8](https://pnpm.io/) - e.g. `npm install -g pnpm@8`, `volta install pnpm@8`, etc.

Then, install the dependencies and start the development server with:

```bash
pnpm install
pnpm start
```

To start the Daiquiri UI documentation site in development, run:

```bash
pnpm styleguide
```

## Development

Daiquiri UI is set up with [Create React App](https://reactjs.org/docs/create-a-new-react-app.html). The documentation site is based on [Docusaurus](https://docusaurus.io/).

In development, the back-end API's `baseUrl` is changed automatically to `http://localhost:8080/api`. If HTTPS is required, set environment variable `REACT_APP_HTTPS=true`. If a completely different `baseUrl` is required, set environment variable `REACT_APP_API_URL=http://myhost:1233/api`.

### `pnpm` cheat sheet

- `pnpm install` - install dependencies
- `pnpm add [-D] <pkg-name>` - [add a dependency](https://pnpm.io/cli/add)
- `pnpm [run] <script> [--<arg>]` - run a script
- `pnpm [exec] <binary>` - run a binary located in `node_modules/.bin`
  (equivalent to `npx <pkg-name>` for a package installed in the workspace)
- `pnpm dlx <pkg-name>` - fetch a package from the registry and run its default
  command binary (equivalent to `npx <pkg-name>`)
- `pnpm why <pkg-name>` - show all packages that depend on the specified package
- `pnpm outdated` - list outdated dependencies
- `pnpm up -L <pkg-name>` - update a package to the latest version

### Dependency management

1. Run `pnpm outdated` to list dependencies that can be upgraded.
1. Read the changelogs and release notes of the dependencies you'd like to
   upgrade. Look for potential breaking changes, and for bug fixes and new
   features that may help improve the codebase.
1. Run `pnpm up -L <pkg-name>` to upgrade a dependency to the latest version.
   Alternatively, you can also edit `package.json` manually and run
   `pnpm install`, but make sure to specify an exact dependency version rather
   than a range (i.e. don't prefix the version with a caret or a tilde).

Beware of the following dependency and versioning requirements:

- `stylelint-config-sass-guidelines` requires `postcss` to be installed explicitly; its version must match the one installed by `react-scripts>css-loader`.
- `@testing-library/user-event` requires `@testing-library/dom` to be installed explicitly; its version must match the one installed by `@testing-library/react`.
- `babel-preset-react-app` must be installed explicitly for linting to work in VSCode.
- The major version number of `@types/node` must match the version of Node
  specified in the `engine` field of `package.json`.
- The major version numbers of
  [DefinitelyTyped](https://github.com/DefinitelyTyped/DefinitelyTyped) packages
  must match the major version numbers of their corresponding dependencies (e.g.
  `@types/jest@27` for `jest@27`).

Note that `pnpm` offers multiple solutions for dealing with peer dependency
version conflicts and other package resolution issues:
[`pnpm.overrides`](https://pnpm.io/package_json#pnpmoverrides),
[`pnpm.packageExtensions`](https://pnpm.io/package_json#pnpmpackageextensions),
[`pnpm.peerDependenciesRules`](https://pnpm.io/package_json#pnpmpeerdependencyrules), etc.

### Why not Node 18?

`fabric` has a dependency on [`node-canvas`](https://github.com/Automattic/node-canvas/releases) (`canvas` on the NPM registry), which itself is based on [Cairo](https://www.cairographics.org/). On install, `node-canvas` has to download a pre-built binary from GitHub. Unfortunately, as of [v2.9.3](https://github.com/Automattic/node-canvas/releases/tag/v2.9.3), pre-built binaries for Node 18 (`node-v108`) are not yet provided. This is the reason why `daiquiri-ui` currently requires Node 16.

## Building for production

- `pnpm build` - build Daiquiri UI for production to `/build`
- `pnpm serve` - serve the content of the `/build` folder to double check that the production build worked as expected
- `pnpm styleguide-build` - build the documentation site for production to `/styleguide`
- `pnpm styleguide-preview` - serve the content of the `/styleguide` folder

For Daiquiri UI's production build to work locally, set environment variable `REACT_APP_API_URL=https://localhost:8080/api` when running `pnpm build`.

## Testing

The tests run with [Jest](https://jestjs.io/) and make use of [React Testing Library](https://testing-library.com/docs/react-testing-library/intro). You can run them with `pnpm test`.

A basic test framework is provided that can be used to quickly mock api resources and mount components - cf. `src/helpers/tests`.

## Code quality

- Code linting uses ESLint with the [eslint-config-galex](https://github.com/ljosberinn/eslint-config-galex) configuration.
- Code style is enforced with [Prettier](https://prettier.io/).
- CSS is written using the SASS preprocessor and style enforced using [stylelint](https://github.com/stylelint/stylelint).

The following code quality scripts are available:

- `pnpm prettier` - check that all files have been formatted with Prettier
- `pnpm prettier-fix` - format all files with Prettier
- `pnpm eslint` - lint all TS and JS files with ESLint
- `pnpm eslint-fix` - auto-fix all fixable linting errors in JS/TS files
- `pnpm stylelint`: lint SCSS files with stylelint
- `pnpm stylelint-fix`: auto-fix all fixable linting errors in SCSS files
- `pnpm tsc` - type-check the project with `tsc`
- `pnpm analyze` - inspect the size and content of the JS bundles (run after
  `pnpm build`)

### Editor integration

Most editors support fixing and formatting files automatically on save. The
configuration for VSCode is provided out of the box, so all you need to do is
install the recommended extensions.

## CI

Gitlab CI runs an extensive pipeline when pushing to a branch or to `master`, which includes:

- linting of JS and TS files with ESlint;
- linting of SCSS files with stylelint;
- type-checking of TS files with `tsc`
- code formatting with Prettier;
- testing with Jest;
- generating [code coverage](https://ui.gitlab-pages.esrf.fr/daiquiri-ui/coverage/) (`master` and tags only);
- building Daiquiri UI (to make sure there are no errors);
- building and deploying the documentation site (`master` and tags only);
- [bundle analysis](https://github.com/danvk/source-map-explorer) [report](https://ui.gitlab-pages.esrf.fr/daiquiri-ui/bundle) (manual trigger).
