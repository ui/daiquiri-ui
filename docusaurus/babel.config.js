module.exports = {
  presets: [
    require.resolve('@docusaurus/core/lib/babel/preset'),
    [
      '@babel/preset-react',
      {
        runtime: 'automatic',
        development: process.env.BABEL_ENV === 'development'
      }
    ]
  ]
};
