const lightCodeTheme = require('prism-react-renderer/themes/github');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Daiquiri UI',
  favicon: 'favicon.ico',
  url: 'https://ui.gitlab-pages.esrf.fr/',
  baseUrl: process.env.BASE_URL ?? '/',

  // https://github.com/facebook/docusaurus/issues/3136
  onBrokenLinks: 'ignore',

  plugins: [
    './plugins/absolute-paths',
    './plugins/daiquiri-styles',
    './plugins/raw-assets',
    [
      'docusaurus-plugin-react-docgen-typescript',
      {
        /** @type {import('docusaurus-plugin-react-docgen-typescript').Options} */
        src: [
          // include local docasaurus examples
          // remove mangled json from docgen
          '{./src/{examples,options},../src/components/**}/!(Label|NewScan|OptionTool|HardwareButton|HardwareGroup|Hdf5Plot|NewScanButton|ScanPlot0d|ScanPlot0dValue|ScanPlot1d|ScanPlot2d|ScanTable|TomoSinogram|TomoTiling|TomoReconstructedSinogram|Main|Panel|SampleRegistration|TomoDetector).tsx'
        ],
        parserOptions: {
          propFilter: (prop, component) => {
            if (prop.parent) {
              return !prop.parent.fileName.includes('@types/react');
            }

            return true;
          }
        }
      }
    ],
    'docusaurus-lunr-search',
    'docusaurus2-dotenv'
  ],

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        blog: false,
        pages: false,
        docs: {
          routeBasePath: '/', // docs only, no homepage
          editUrl:
            'https://gitlab.esrf.fr/ui/daiquiri-ui/-/tree/main/docusaurus/'
        },
        theme: {
          customCss: [require.resolve('./src/styles.css')]
        }
      })
    ]
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'Daiquiri UI',
        style: 'primary',
        logo: { src: 'daiquiri.png', alt: '' },
        items: [
          {
            label: 'GitLab',
            href: 'https://gitlab.esrf.fr/ui/daiquiri-ui',
            position: 'right',
            'aria-label': 'Daiquiri-UI GitHub Repository'
          },
          {
            type: 'search',
            position: 'right'
          }
        ]
      },
      prism: { theme: lightCodeTheme },
      colorMode: { disableSwitch: true }
    })
};

module.exports = config;
