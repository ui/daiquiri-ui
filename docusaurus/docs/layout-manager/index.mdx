---
sidebar_position: 3
---

import BrowserOnly from '@docusaurus/BrowserOnly';
import ItemTypes from '@site/src/ItemTypes';
import { DaiquiriLibBadge } from '@site/src/Badge';

# Layout Manager

<DaiquiriLibBadge />

The layout manager generates a UI layout dynamically from a YAML file defined server-side.

A simple layout definition is shown below:

```yaml title=simple-layout.yml
name: Simple Layout
description: A simple layout
children:
  - type: row
    children:
      - type: col
        children:
          - type: scantable
            title: Scans

  - type: row
    children:
      - type: col
        children:
          - type: hardwaregroup
            title: Diffractometer2
            ids:
              - id: omega
                step: 90
                steps: [45, 90, 180]
```

## Sidebar navigation

To include a layout in the sidebar navigation, set `insidebar: true`. To use a meaningful icon for the link, also set `icon` to either:

- a [fontawesome icon](https://fontawesome.com/v4.7/icons/) (e.g `fa-picture-o`), or
- a [fontawesomemore icon](https://gitlab.esrf.fr/ui/fontawesomemore/-/tree/master/glyphs) (e.g. `fam-place-holder`).

```yaml
name: Simple Layout
description: A simple layout
insidebar: true
icon: fa-picture-o
children: ...
```

## Children

The actual layout is defined in the `children` property. The general structure of each item is as follows:

```yaml
- type: row # the type of object
  children: # its children
    ...
```

These can be nested as deeply as needed. For layout consistency, the root items should be rows (`row`) and their direct children should be columns (`col`). See [Wrappers](./wrappers) for more details.

The layout manager can load a number of [wrappers](./wrappers) and [display components](../components) to allow defining complex user interfaces:

### Wrappers

<BrowserOnly>
  {() => {
    const yamlMap = require('@esrf/daiquiri-lib').yamlMap;
    return <ItemTypes items={Object.keys(yamlMap)} />;
  }}
</BrowserOnly>

### Display components

<BrowserOnly>
  {() => {
    const componentMap =
      require('daiquiri-ui/src/components/yaml-layout/components/componentMap').componentMap;
    return <ItemTypes items={Object.keys(componentMap)} />;
  }}
</BrowserOnly>

## Component options

Options defined in YAML are passed through to loaded components. For example in the case of a motor:

```yaml
- type: hardwaregroup
  title: Diffractometer2
  ids:
    - id: omega
      step: 90
      steps: [45, 90, 180]
```

See [Customising](./layout/customising) for more information. The options defined for each component are detailed on each component's page, under [UI Components](../ui-components/index.mdx).

## Restricting access

Layouts can be restricted by setting the `require_staff` flag. This will restrict this particular layout to staff members only:

```yaml
name: Simple Layout
description: A simple layout
require_staff: true
children: ...
```

## Partials, templating, and variables

Layouts can also be constructed via composition. Daiquiri provides some common partial layouts that can be reused between beamlines in the [`daiquiri/resources/layout/partials` folder](https://gitlab.esrf.fr/ui/daiquiri/-/tree/main/daiquiri/resources/layout/partials).

A partial can be included as follows:

```yaml
name: include
description: Include test
children:
  - !include partials/2dview.yml
```

The `include` tag will first search the local directory, so it is also possible to build local partials and include files to build layouts from smaller components.

Some partials contain variables that can be modified in the resulting layout. To override those variables, provide a `variables` property as a child of the `include` tag:

```yaml
name: include
description: Include test
children:
  - !include
    file: partials/2dview.yml
    variables:
      rows: 1
  - !include partials/single_row.yml
```
