---
sidebar_position: 1
---

import BrowserOnly from '@docusaurus/BrowserOnly';
import DaiquiriLayout from '@site/src/DaiquiriLayout';
import ItemTypes from '@site/src/ItemTypes';
import { DaiquiriLibBadge } from '@site/src/Badge';

# Wrappers

<DaiquiriLibBadge />

The following wrapper components are available to build layouts:

<BrowserOnly>
  {() => {
    const yamlMap = require('@esrf/daiquiri-lib').yamlMap;
    return <ItemTypes items={Object.keys(yamlMap)} />;
  }}
</BrowserOnly>

:::info
Layouts are automatically wrapped inside [fluid Bootstrap containers](https://getbootstrap.com/docs/4.0/layout/overview/#containers) for responsiveness,
so you usually don't need to use the `container` component directly.
:::

## Row & Column

`col` and `row` make use of Boostrap's [grid system](https://getbootstrap.com/docs/4.0/layout/grid/) to allow elements to be positioned into rows and columns.

### Example 1: one row, two columns

```yaml
children:
  - type: row
    children:
      - type: col
        children: ...
      - type: col
        children: ...
```

<DaiquiriLayout
  initialIframeHeight={122}
  children={[
    {
      type: 'row',
      children: [
        { type: 'col', children: [{ type: 'label', label: 'Column 1' }] },
        { type: 'col', children: [{ type: 'label', label: 'Column 2' }] },
      ],
    },
  ]}
/>

### Example 2: two rows, multiple columns

```yaml
children:
  - type: row
    children:
      - type: col
        children: ...
      - type: col
        children: ...

  - type: row
    children:
      - type: col
        children: ...
      - type: col
        children: ...
      - type: col
        children: ...
```

results in:

<DaiquiriLayout
  initialIframeHeight={179}
  children={[
    {
      type: 'row',
      children: [
        { type: 'col', children: [{ type: 'label', label: 'Column 1' }] },
        { type: 'col', children: [{ type: 'label', label: 'Column 2' }] },
      ],
    },
    {
      type: 'row',
      children: [
        { type: 'col', children: [{ type: 'label', label: 'Column 1' }] },
        { type: 'col', children: [{ type: 'label', label: 'Column 2' }] },
        { type: 'col', children: [{ type: 'label', label: 'Column 3' }] },
      ],
    },
  ]}
/>

## Panel

Panels can be used to encase components.

:::info
Many components will render panels automatically unless they are nested.
:::

```yaml
children:
  - type: panel
    title: Title # optional
    children: ...
```

<DaiquiriLayout
  initialIframeHeight={148}
  children={[
    {
      type: 'panel',
      title: 'Title',
      children: [{ type: 'label', label: 'Some panel children' }],
    },
  ]}
/>

Panel children can be centered with:

```yaml
children:
  - type: panel
    ...
    centerContent: true
```

## Tabs

Tabs can be used to nest components:

```yaml
children:
  - type: tabs
    title: Tabs title # optional
    children:
      - type: panel
        title: One
        children: ...
      - type: panel
        title: Two
        children: ...
```

<DaiquiriLayout
  initialIframeHeight={241}
  children={[
    {
      type: 'tabs',
      title: 'Tabs title',
      children: [
        {
          type: 'panel',
          title: 'One',
          children: [{ type: 'label', label: 'Tab 1' }],
        },
        {
          type: 'panel',
          title: 'Two',
          children: [{ type: 'label', label: 'Tab 1' }],
        },
      ],
    },
  ]}
/>

## Form

`form` is a wrapper component that lays out widgets and their titles onto two columns. The `title` of
each widget is displayed in the left column and the widget itself in the right column.

To add sub-headings between groups of widgets, declare an item with a unique `header` property.

```yaml
children:
  - type: form
    title: Form Title # optional
    children:
      - type: formheader
        title: 1st header
      - type: label
        label: A widget
        title: My widget
      - type: label
        label: Another widget
        title: My widget2
      - type: formheader
        title: 2nd header
      - type: label
        label: An ultimate widget
        title: My widget3
```

<DaiquiriLayout
  initialIframeHeight={268}
  children={[
    {
      type: 'form',
      title: 'Form title',
      children: [
        { type: 'formheader', title: '1st header' },
        { type: 'label', label: 'A widget', title: 'My widget' },
        { type: 'label', label: 'Another widget', title: 'My widget2' },
        { type: 'formheader', title: '2nd header' },
        { type: 'label', label: 'An ultimate widget', title: 'My widget3' },
      ],
    },
  ]}
/>

## Grid

`grid` provides a compact way to lay out components into a grid.

The sub-components are dispatched into cells from left to right, then
top to bottom.

```yaml
children:
  - type: grid
    title: Grid title # optional
    rows: max-content max-content # 2 rows
    columns: max-content auto max-content # 3 columns; middle one expands to fill available space
    children:
      - type: label
        label: Top-left cell
      - type: label
        label: Top-center cell
      - type: label
        label: Top-left cell
      - type: label
        label: Bottom-left cell
      - type: label
        label: Bottom-center cell
      - type: label
        label: Bottom-right cell
```

<DaiquiriLayout
  initialIframeHeight={268}
  children={[
    {
      type: 'grid',
      title: 'Grid title',
      rows: 'max-content max-content',
      columns: 'max-content auto max-content',
      children: [
        { type: 'label', label: 'Top-left cell' },
        { type: 'label', label: 'Top-center cell' },
        { type: 'label', label: 'Top-right cell' },
        { type: 'label', label: 'Bottom-left cell' },
        { type: 'label', label: 'Bottom-center cell' },
        { type: 'label', label: 'Bottom-right cell' },
      ],
    },
  ]}
/>

### Grid with span

```yaml
children:
  - type: grid
    title: Grid title # optional
    rows: max-content auto max-content
    columns: max-content auto max-content
    children:
      - type: label
        label: Header
        columnSpan: 3
        style:
          backgroundColor: '#ffb2d2'
      - type: label
        label: Left
        rowSpan: 2
        style:
          backgroundColor: '#b2f8ff'
      - type: label
        label: Middle
        rowSpan: 1
        style:
          backgroundColor: '#f9ffb2'
      - type: label
        label: Right
        rowSpan: 2
        style:
          backgroundColor: '#ffb7b2'
      - type: label
        label: Bottom
        rowSpan: 1
        style:
          backgroundColor: '#b8b2ff'
```

<DaiquiriLayout
  initialIframeHeight={268}
  children={[
    {
      type: 'grid',
      title: 'Grid title',
      rows: 'max-content auto max-content',
      columns: 'max-content auto max-content',
      children: [
        {
          type: 'label',
          label: 'Header',
          columnSpan: 3,
          style: { backgroundColor: '#ffb2d2' },
        },
        {
          type: 'label',
          label: 'Left',
          rowSpan: 2,
          style: { backgroundColor: '#b2f8ff' },
        },
        {
          type: 'label',
          label: 'Middle',
          style: { backgroundColor: '#f9ffb2' },
        },
        {
          type: 'label',
          label: 'Right',
          rowSpan: 2,
          style: { backgroundColor: '#ffb7b2' },
        },
        {
          type: 'label',
          label: 'Bottom',
          style: { backgroundColor: '#b8b2ff' },
        },
      ],
    },
  ]}
/>
