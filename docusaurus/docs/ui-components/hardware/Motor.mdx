import BrowserOnly from '@docusaurus/BrowserOnly';
import PropTable from '@site/src/PropTable';
import DaiquiriHardwareObject from '@site/src/DaiquiriHardwareObject';
import DaiquiriCode from '@site/src/DaiquiriCode';
import DaiquiriStore from '@site/src/DaiquiriStore';
import CentredObject from '@site/src/CentredObject';
import { DaiquiriLibBadge } from '@site/src/Badge';

import { Row, Col } from 'react-bootstrap';
import { MotorDefault } from '@esrf/daiquiri-lib';
import { MotorState } from '@esrf/daiquiri-lib';
import Wrapper from 'daiquiri-ui/src/components/hardware/base/mocks/Wrapper';
import MotorSmall from 'daiquiri-ui/src/components/hardware/motor/MotorSmall';
import MotorText from 'daiquiri-ui/src/components/hardware/motor/MotorText';
import MockMotor from 'daiquiri-ui/src/components/hardware/motor/mocks/Motor';
import motor from 'daiquiri-ui/src/components/hardware/motor/mocks/Motor.json';
import halfMotor from 'daiquiri-ui/src/components/hardware/motor/mocks/HalfMotor.json';
import unknownMotor from 'daiquiri-ui/src/components/hardware/motor/mocks/MotorUnknown.json';
import notReadyMotor from 'daiquiri-ui/src/components/hardware/motor/mocks/MotorNotReady.json';
import disconnectedMotor from 'daiquiri-ui/src/components/hardware/motor/mocks/MotorDisconnected.json';
import powerOffMotor from 'daiquiri-ui/src/components/hardware/motor/mocks/MotorPowerOff.json';

<DaiquiriLibBadge />
<div style={{ clear: 'both' }} />

<PropTable component="MotorDefaultOptions" />

Display a motor widget.

## Interactive example

<DaiquiriHardwareObject>
  <MockMotor initialPosition={235.1654} initialVelocity={30}>
    <MotorDefault options={{}} />
  </MockMotor>
</DaiquiriHardwareObject>

## Device states references

<DaiquiriCode>
  <CentredObject centerColSize={6}>
    <Row>
      <Col>
        Moving motor:
        <Wrapper showActions>
          <MotorDefault
            hardware={{
              ...motor,
              properties: {
                ...motor.properties,
                state: ['MOVING'],
                target: 180,
              },
            }}
            options={{}}
          />
        </Wrapper>
      </Col>
      <Col>
        Motor is not ready:
        <Wrapper showActions>
          <MotorDefault hardware={{ ...notReadyMotor }} options={{}} />
        </Wrapper>
      </Col>
      <Col>
        Unknown state:
        <Wrapper showActions>
          <MotorDefault hardware={{ ...unknownMotor }} options={{}} />
        </Wrapper>
      </Col>
      <Col>
        Offline motor:
        <Wrapper showActions>
          <MotorDefault hardware={{ ...disconnectedMotor }} options={{}} />
        </Wrapper>
      </Col>
      <Col>
        Offline motor:
        <Wrapper showActions>
          <MotorDefault hardware={{ ...powerOffMotor }} options={{}} />
        </Wrapper>
      </Col>
    </Row>
  </CentredObject>
</DaiquiriCode>

## Available control system user tags

Some user tags can be specified by tghe control system.

Actually this allow to move the motor even if it is not ready or the power is off.

<DaiquiriCode>
  <CentredObject centerColSize={6}>
    <Row>
      <Col>
        <code>auto_ready</code>
        <Wrapper showActions>
          <MotorDefault
            hardware={{ ...notReadyMotor, user_tags: ['auto_ready'] }}
            options={{}}
          />
        </Wrapper>
      </Col>
      <Col>
        <code>auto_on</code>
        <Wrapper showActions>
          <MotorDefault
            hardware={{ ...powerOffMotor, user_tags: ['auto_on'] }}
            options={{}}
          />
        </Wrapper>
      </Col>
    </Row>
  </CentredObject>
</DaiquiriCode>

## Available configurations

With steps and step size:

<DaiquiriHardwareObject>
  <MotorDefault
    hardware={motor}
    options={{
      steps: [0, 90, 180],
      step: 90,
    }}
  />
</DaiquiriHardwareObject>

When `step` is defined and when the input is focused, the `Up` and `Down` arrows can be used to repeatidly step without mouse interaction.
The component will regain focus after each move.

With precision:

<DaiquiriHardwareObject>
  <MotorDefault hardware={motor} options={{ precision: 2 }} />
</DaiquiriHardwareObject>

Read only:

<DaiquiriHardwareObject>
  <MotorDefault hardware={motor} options={{ readOnly: true }} />
</DaiquiriHardwareObject>

Extended properties:

<DaiquiriHardwareObject>
  <MotorDefault hardware={motor} options={{ extended: true }} />
</DaiquiriHardwareObject>

Header infront:

<DaiquiriHardwareObject>
  <MotorDefault hardware={motor} options={{ header: 'front' }} />
</DaiquiriHardwareObject>

No header:

<DaiquiriHardwareObject>
  <MotorDefault hardware={motor} options={{ header: 'none' }} />
</DaiquiriHardwareObject>

## Variants

### `small`

A more compact motor representation.

:::caution
Deprecated, prefer using default variant with:

```yaml
options:
  header: 'front'
```

:::

##### Interactive example

<DaiquiriHardwareObject>
  <MockMotor initialVelocity={30}>
    <MotorSmall />
  </MockMotor>
</DaiquiriHardwareObject>

##### Device states references

<DaiquiriCode>
  <CentredObject centerColSize={8}>
    <Row>
      <Col>
        Moving motor:
        <Wrapper showActions>
          <MotorSmall
            hardware={{
              ...motor,
              properties: {
                ...motor.properties,
                state: ['MOVING'],
                target: 180,
              },
            }}
            options={{}}
          />
        </Wrapper>
      </Col>
      <Col>
        Disconnected motor:
        <Wrapper showActions>
          <MotorSmall hardware={{ ...disconnectedMotor }} options={{}} />
        </Wrapper>
      </Col>
    </Row>
  </CentredObject>
</DaiquiriCode>

### `text`

A motor represented as it's position. The widget is not interactive.

<DaiquiriCode>
  <CentredObject centerColSize={8}>
    <Row>
      <Col>
        Stand by motor:
        <Wrapper showActions>
          <MotorText hardware={motor} options={{ header: 'none' }} />
        </Wrapper>
      </Col>
      <Col>
        Moving motor:
        <Wrapper showActions>
          <MotorText
            hardware={{
              ...motor,
              properties: {
                ...motor.properties,
                state: ['MOVING'],
                target: 180,
              },
            }}
            options={{ header: 'none' }}
          />
        </Wrapper>
      </Col>
      <Col>
        Disconnected motor:
        <Wrapper showActions>
          <MotorText
            hardware={{ ...disconnectedMotor }}
            options={{ header: 'none' }}
          />
        </Wrapper>
      </Col>
    </Row>
  </CentredObject>
</DaiquiriCode>

### `rotation`

<PropTable component="MotorRotationOptions" />

A rotation motor represented as 360 degree knob widget.

The motor position needs to be in degrees, and the modulus of 360
will be used to display the position.

A `clockwise` option can be provided. By default it is false.

##### Interactive example

<DaiquiriHardwareObject>
  <BrowserOnly>
    {() => {
      const MotorRotation =
        require('daiquiri-ui/src/components/hardware/motor/MotorRotation').default;
      return (
        <MockMotor initialVelocity={30}>
          <MotorRotation />
        </MockMotor>
      );
    }}
  </BrowserOnly>
</DaiquiriHardwareObject>

##### Device states references

<DaiquiriCode>
  <BrowserOnly>
    {() => {
      const MotorRotation =
        require('daiquiri-ui/src/components/hardware/motor/MotorRotation').default;
      return (
        <CentredObject centerColSize={6}>
          <Row>
            <Col>
              Moving motor:
              <Wrapper showActions>
                <MotorRotation
                  hardware={{
                    ...motor,
                    properties: {
                      ...motor.properties,
                      state: ['MOVING'],
                      target: 180,
                    },
                  }}
                  options={{}}
                />
              </Wrapper>
            </Col>
            <Col>
              Disconnected motor:
              <Wrapper showActions>
                <MotorRotation
                  hardware={{ ...disconnectedMotor }}
                  options={{}}
                />
              </Wrapper>
            </Col>
            <Col>
              Half range motor:
              <Wrapper showActions>
                <MotorRotation hardware={{ ...halfMotor }} options={{}} />
              </Wrapper>
            </Col>
          </Row>
        </CentredObject>
      );
    }}
  </BrowserOnly>
</DaiquiriCode>

### `state`

Here is few results with the variant `state`.

<DaiquiriCode>
  <BrowserOnly>
    {() => {
      const MotorRotation =
        require('daiquiri-ui/src/components/hardware/motor/MotorRotation').default;
      return (
        <CentredObject centerColSize={6}>
          <Row>
            <Col>
              Motor offline:
              <MotorState
                hardware={{
                  ...motor,
                  properties: {},
                  online: false,
                }}
              />
            </Col>
            <Col>
              Motor ready:
              <MotorState
                hardware={{
                  ...motor,
                  properties: {
                    ...motor.properties,
                    state: ['READY'],
                  },
                }}
              />
            </Col>
            <Col>
              Motor moving:
              <MotorState
                hardware={{
                  ...motor,
                  properties: {
                    ...motor.properties,
                    state: ['MOVING'],
                    target: 180,
                  },
                }}
              />
            </Col>
            <Col>
              Motor off:
              <MotorState
                hardware={{
                  ...motor,
                  properties: {
                    ...motor.properties,
                    state: ['OFF'],
                    target: 180,
                  },
                }}
              />
            </Col>
            <Col>Motor ready and moving:
              <MotorState
                hardware={{
                  ...motor,
                  properties: {
                    ...motor.properties,
                    state: ['MOVING', 'READY'],
                    target: 180,
                  },
                }}
              />
            </Col>
            <Col>
              Motor ready and home and custom state:
              <MotorState
                hardware={{
                  ...motor,
                  properties: {
                    ...motor.properties,
                    state: ['READY', 'HOME', '_MY_STATE', '_MY_STATE2:Axis in state2'],
                  },
                }}
              />
            </Col>
          </Row>
        </CentredObject>
      );
    }}

  </BrowserOnly>
</DaiquiriCode>

### `tomo_rotation`

<PropTable component="MotorTomoRotationOptions" />

A rotation motor represented as 360 degree knob widget with helpers
for tomography interaction.

The motor position needs to be in degrees, and the modulus of 360
will be used to display the position.

A `clockwise` option can be provided. By default it is `false`.

##### Interactive example

<DaiquiriStore>
  <DaiquiriCode>
    <DaiquiriHardwareObject>
      <BrowserOnly>
        {() => {
          const MotorTomoRotation =
            require('daiquiri-ui/src/components/hardware/motor/MotorTomoRotation').default;
          return (
            <MockMotor initialVelocity={30}>
              <MotorTomoRotation />
            </MockMotor>
          );
        }}
      </BrowserOnly>
    </DaiquiriHardwareObject>
  </DaiquiriCode>
</DaiquiriStore>

##### Device states references

<DaiquiriStore>
  <DaiquiriCode>
    <BrowserOnly>
      {() => {
        const MotorTomoRotation =
          require('daiquiri-ui/src/components/hardware/motor/MotorTomoRotation').default;
        return (
          <CentredObject centerColSize={8}>
            <Row>
              <Col>
                Moving motor:
                <Wrapper showActions>
                  <MotorTomoRotation
                    hardware={{
                      ...motor,
                      properties: {
                        ...motor.properties,
                        state: ['MOVING'],
                        target: 180,
                      },
                    }}
                    options={{}}
                  />
                </Wrapper>
              </Col>
              <Col>
                Disconnected motor:
                <Wrapper showActions>
                  <MotorTomoRotation
                    hardware={{ ...disconnectedMotor }}
                    options={{}}
                  />
                </Wrapper>
              </Col>
            </Row>
          </CentredObject>
        );
      }}
    </BrowserOnly>
  </DaiquiriCode>
</DaiquiriStore>
