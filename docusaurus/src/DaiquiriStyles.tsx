/* Import Daiquiri's styles.
 *
 * The webpack configuration provided by `docusaurus-plugin-sass` injects Daiquiri's
 * styles into a hidden iframe so they don't conflict with Docusaurus' styles.
 *
 * Each Daiquiri code block then takes care of retrieving the Daiquiri styles from
 * the hidden iframe and injecting them into its own iframe. */
import '@site/../src/scss/main.scss';
import './styles.scss';
import './bootstrap/modal.scss';

import { useLayoutEffect } from 'react';

const stylesIframe = document.getElementById(
  'daiquiri-styles'
) as HTMLIFrameElement;

const rootStyles = document.getElementsByTagName('style');

interface Props {
  withRootStyles?: boolean;
  shadowRoot: ShadowRoot;
}

function DaiquiriStyles(props: Props) {
  const { withRootStyles, shadowRoot } = props;

  useLayoutEffect(() => {
    if (!shadowRoot) return;
    stylesIframe.contentWindow.document.head
      .querySelectorAll('style')
      .forEach((elem) => {
        shadowRoot.appendChild(elem.cloneNode(true));
      });

    // We need to copy the CSS variables into the shadow dom
    // https://stackoverflow.com/questions/45763121/list-css-custom-properties-css-variables
    // https://davidwalsh.name/css-variables-javascript
    const variables = Array.from(
      stylesIframe.contentWindow.document.styleSheets
    )
      .filter((styleSheet) => {
        try {
          return styleSheet.cssRules;
        } catch (e) {
          console.warn(e);
        }
      })
      .map((styleSheet) => Array.from(styleSheet.cssRules))
      .flat()
      // @ts-expect-error
      .filter((cssRule) => cssRule.selectorText?.includes(':root'))
      .map((cssRule) =>
        cssRule.cssText.split('{')[1].split('}')[0].trim().split(';')
      )
      .flat()
      .filter((text) => text !== '')
      .map((text) => text.split(':'))
      .map((parts) => ({ key: parts[0].trim(), value: parts[1].trim() }));

    variables.forEach((variable) => {
      // @ts-expect-error
      shadowRoot.host.style.setProperty(variable.key, variable.value);
    });

    if (withRootStyles) {
      // Some libraries helpfully inject their styles automatically into the
      // top of the root document :(
      // ... acejs, plotly
      setTimeout(() => {
        Array.from(rootStyles).forEach((elem) => {
          const newElem = elem.cloneNode(true) as HTMLStyleElement;
          shadowRoot.appendChild(newElem);
          // Plotly uses CSSStyleSheet rules, so clone these too (!)
          for (var i = 0; i < elem.sheet.rules.length; i++) {
            newElem.sheet.insertRule(elem.sheet.rules[i].cssText);
          }
        });
      }, 1);
    }
  }, [shadowRoot]);
  return null;
}

export default DaiquiriStyles;
