import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate, HardwareState } from '@esrf/daiquiri-lib';

// Define the interface of the properties recieved from the REST API
export interface HWComponentSchema extends HardwareTypes.Hardware {
  properties: {
    property: string;
    property2: number;
    state: string;
  };
}

// Define the interface of the component options
export interface HWComponentOptions
  extends HardwareTypes.HardwareWidgetOptions {
  /** Make the arrows large */
  large?: number;
}

export default function HWComponent(
  props: HardwareTypes.HardwareWidgetProps<
    HWComponentSchema,
    HWComponentOptions
  >
) {
  const { hardware, options = {}, disabled } = props;
  const { state } = hardware.properties;

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={
        <TypeIcon name="Component" icon="fa-cog" online={hardware.online} />
      }
      widgetState={
        <HardwareState.HardwareState
          state={state}
          minWidth={6}
          variant={state === 'READY' ? 'success' : 'warning'}
        />
      }
      widgetContent={<p>Component</p>}
    />
  );
}

export function HWComponentOptions(props: HWComponentOptions) {
  return null;
}

export function HWComponentHardware(
  props: HardwareTypes.EditableHardware<HWComponentSchema>
) {
  return null;
}

export function HWComponentSchema(
  props: HardwareTypes.HardwareSchemaDescription
) {
  return null;
}
