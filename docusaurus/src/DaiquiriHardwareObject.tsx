import { PropsWithChildren } from 'react';

import Wrapper from 'daiquiri-ui/src/components/hardware/base/mocks/Wrapper';

import DaiquiriCode from './DaiquiriCode';
import CentredObject from './CentredObject';

interface Props {
  showActions?: boolean;
  centerColSize?: number;
}

export default function DaiquiriHardwareObject(
  props: PropsWithChildren<Props>
) {
  const { children, showActions = true, centerColSize } = props;
  return (
    <DaiquiriCode>
      <CentredObject centerColSize={centerColSize}>
        <Wrapper showActions={showActions}>{children}</Wrapper>
      </CentredObject>
    </DaiquiriCode>
  );
}
