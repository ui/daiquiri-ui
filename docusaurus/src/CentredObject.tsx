import { PropsWithChildren } from 'react';
import { Col, Row } from 'react-bootstrap';

interface Props {
  centerColSize?: number;
}

/*
 * Centre an object in columns
 */
export default function CenteredObject(props: PropsWithChildren<Props>) {
  const { children, centerColSize = 4 } = props;
  const restColSize = (12 - centerColSize) / 2;
  return (
    <Row>
      <Col md={restColSize} sm={12}></Col>
      <Col md={centerColSize} sm={12}>
        {children}
      </Col>
      <Col md={restColSize} sm={12}></Col>
    </Row>
  );
}
