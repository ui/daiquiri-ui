import type {
  HardwareButtonObject as HardwareButtonObjectType,
  HardwareButtonProps,
} from 'daiquiri-ui/src/components/hardware/HardwareButton';

export function HardwareButtonOptions(props: HardwareButtonProps) {
  return null;
}

export function HardwareButtonObject(props: HardwareButtonObjectType) {
  return null;
}
