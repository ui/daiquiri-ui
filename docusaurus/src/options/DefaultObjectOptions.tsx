import type {
  DefaultObjectOptions as DefaultObjectOptionsType,
  PropertiesConfig,
  SecondaryPropertiesConfig,
} from 'daiquiri-ui/src/components/hardware/DefaultObject';

export function DefaultObjectOptions(props: DefaultObjectOptionsType) {
  return null;
}

export function DefaultObjectPropertiesConfig(props: PropertiesConfig) {
  return null;
}

export function DefaultObjectSecondaryPropertiesConfig(
  props: SecondaryPropertiesConfig
) {
  return null;
}
