import { PropsWithChildren, CSSProperties } from 'react';
import { useDynamicImport } from 'docusaurus-plugin-react-docgen-typescript/dist/esm/hooks';

interface TypeSpanProps {
  style?: CSSProperties;
  className?: string;
}

export function TypeSpan(props: PropsWithChildren<TypeSpanProps>) {
  const { children, style, className } = props;
  return (
    <div className={className} style={style}>
      <code style={{ color: 'rgb(153, 0, 85)' }}>{children}</code>
    </div>
  );
}

// https://github.com/atomicpages/docusaurus-plugin-react-docgen-typescript
export default function PropTable({ component }: { component: string }) {
  const props = useDynamicImport(component);

  if (!props) {
    return (
      <div
        style={{ marginBottom: '1rem' }}
        className="theme-admonition theme-admonition-danger alert alert--danger"
      >
        {'<PropTable>'}: Couldn't find prop definition for `{component}`
      </div>
    );
  }

  return (
    <table className="prop-table">
      <thead>
        <tr>
          <th>Name</th>
          <th>Type</th>
          <th>Default Value</th>
          <th>Required</th>
          <th>Description</th>
        </tr>
      </thead>
      <tbody>
        {Object.keys(props).map((key) => {
          return (
            <tr key={key}>
              <td>
                <code style={{ color: 'rgb(102, 153, 0)' }}>{key}</code>
              </td>
              <td>
                <TypeSpan>{props[key].type?.name}</TypeSpan>
              </td>
              <td>
                {props[key].defaultValue && (
                  <code>{props[key].defaultValue.value}</code>
                )}
              </td>
              <td style={{ color: 'rgb(118, 118, 118)' }}>
                {props[key].required ? 'Yes' : 'No'}
              </td>
              <td>{props[key].description}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
