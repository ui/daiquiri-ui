interface Props {
  items: string[];
}

function ItemTypes(props: Props) {
  const { items } = props;

  return (
    <div className="daiquiri-types">
      {items.map((name) => (
        <code key={name}>{name}</code>
      ))}
    </div>
  );
}

export default ItemTypes;
