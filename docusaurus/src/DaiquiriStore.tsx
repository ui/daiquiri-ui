import { PropsWithChildren, useEffect, useState } from 'react';
import { Provider } from 'react-redux';
import { providerRegistry } from '@esrf-ui/redux-provider';

import store from 'daiquiri-ui/src/store';
import Resources from 'daiquiri-ui/src/helpers/tests/resources';

import app from 'daiquiri-ui/src/providers/app';
import hardware from 'daiquiri-ui/src/providers/hardware';
import scans from 'daiquiri-ui/src/providers/scans';
import queue from 'daiquiri-ui/src/providers/queue';
import tomo from 'daiquiri-ui/src/providers/tomo';

import DaiquiriCode, { Props } from './DaiquiriCode';

/**
 * Render a <DaiquiriCode> with the daiquri redux store
 * bootstrapped, with http requests mocked, and config fetched
 */
export default function DaiquiriStore(props: PropsWithChildren<Props>) {
  const { children, ...rest } = props;
  const [ready, setReady] = useState<boolean>(false);

  const _hardware = hardware;
  const _queue = queue;
  const _tomo = tomo;
  const _scans = scans;

  new Resources();
  if (!providerRegistry.started) providerRegistry.start(store);

  useEffect(() => {
    async function fetch() {
      await app.getNamespace('config').fetch(null, true);
    }
    fetch();
    // Have browser only dependencies (Image, etc...)
    const SocketIOService =
      require('daiquiri-ui/src/services/SocketIOService').default;
    SocketIOService.setup({ baseUrl: 'mock' });
    const messageHandler = require('daiquiri-ui/src/messagehandler').default;
    messageHandler.start(store);

    const session = require('daiquiri-ui/src/providers/session');
    setTimeout(() => {
      setReady(true);
    }, 1);
  });

  if (!ready) return <div className="daiquiri-code">Loading...</div>;

  return (
    <DaiquiriCode {...rest}>
      <Provider store={store}>{children}</Provider>
    </DaiquiriCode>
  );
}
