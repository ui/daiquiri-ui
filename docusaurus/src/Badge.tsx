import Link from '@docusaurus/Link';

interface Props {
  title: string;
  bg?: string;
  tooltip?: string;
}

function Badge(props: Props) {
  const { title, bg = 'default', tooltip } = props;

  return (
    <div title={tooltip} className={`daiquiri-badge daiquiri-badge-${bg}`}>
      {title}
    </div>
  );
}

export default Badge;

export function DaiquiriLibBadge() {
  return (
    <div style={{ textAlign: 'right', float: 'right' }}>
      <Link to="/daiquiri-lib">
        <Badge title="daiquiri-lib" tooltip="Included in @esrf/daiquiri-lib" />
      </Link>
    </div>
  );
}
