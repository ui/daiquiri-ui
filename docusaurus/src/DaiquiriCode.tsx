import { PropsWithChildren, useRef } from 'react';
import root from 'react-shadow';

import BrowserOnly from '@docusaurus/BrowserOnly';

export interface Props {
  /** To minimise layout shift */
  initialHeight?: number;
  /** Set a fixed container height */
  fixedHeight?: number;
  /** Import top level styles */
  withRootStyles?: boolean;
}

/*
 * Render Daiquiri code inside a shadowRoot element.
 */
function DaiquiriCode(props: PropsWithChildren<Props>) {
  const { children, fixedHeight, withRootStyles } = props;
  const ref = useRef();

  return (
    <root.div ref={ref} className="daiquiri-code">
      <BrowserOnly>
        {() => {
          const DaiquiriStyles = require('./DaiquiriStyles').default;
          return (
            <DaiquiriStyles
              withRootStyles={withRootStyles}
              shadowRoot={ref?.current?.shadowRoot}
            />
          );
        }}
      </BrowserOnly>
      {fixedHeight ? (
        <div style={{ height: fixedHeight }}>{children}</div>
      ) : (
        children
      )}
    </root.div>
  );
}

export default DaiquiriCode;
