import DaiquiriCode from './DaiquiriCode';

interface Props {
  children: any[];
  initialIframeHeight?: number; // to minimise layout shift
}

function DaiquiriLayout(props: Props) {
  const { children, initialIframeHeight } = props;
  const Main = require('@esrf/daiquiri-lib/').YAMLLayout;

  return (
    <DaiquiriCode initialHeight={initialIframeHeight}>
      <Main id="layout" layout={{ children }} />
    </DaiquiriCode>
  );
}

export default DaiquiriLayout;
