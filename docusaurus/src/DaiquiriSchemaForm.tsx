import { Provider } from 'react-redux';
import { cloneDeep } from 'lodash';
import { providerRegistry } from '@esrf-ui/redux-provider';

import store from 'daiquiri-ui/src/store';
import SchemaForm from 'daiquiri-ui/src/components/SchemaForm';
import schema from 'daiquiri-ui/src/components/mocks/FormSchema.json';

import DaiquiriCode from './DaiquiriCode';
import { useEffect, useState } from 'react';

export function getFormSchema(keys = []) {
  const clonedSchema = cloneDeep(schema);

  const { ExampleSchema } = clonedSchema.definitions;
  ExampleSchema.properties = Object.fromEntries(
    Object.entries(ExampleSchema.properties).filter(([key]) =>
      keys.includes(key)
    )
  );

  return clonedSchema;
}

interface Props {
  schemas: any;
  initialHeight?: number;
}

export default function DaiquiriSchemaForm(props: Props) {
  const { schemas, initialHeight = 380 } = props;
  const defaultProps = {
    schema: 'test',
    actions: {
      fetch: () => new Promise((res) => res(true)),
      setForm: () => {},
      alert: () => {},
      savePreset: () => {},
    },
  };

  const [fetched, setFetched] = useState<boolean>(false);
  useEffect(() => {
    setTimeout(() => {
      setFetched(true);
    }, 200);
  }, []);

  if (!providerRegistry.started) providerRegistry.start(store);

  return (
    <DaiquiriCode initialHeight={initialHeight}>
      <Provider store={store}>
        <div className="border border-secondary p-2 m-2 rounded">
          <SchemaForm schemas={schemas} fetched={fetched} {...defaultProps} />
        </div>
      </Provider>
    </DaiquiriCode>
  );
}
