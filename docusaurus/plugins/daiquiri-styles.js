/*
 * NOTE: after editing this file, delete /docusaurus/node_modules/.cache
 * for the changes to take effect.
 */

/*
 * When Daiquiri's main SCSS stylesheet is imported, process it as usual with webpack,
 * then inject the resulting styles into a hidden iframe so that:
 * 1. they don't conflict with Docusaurus' styles;
 * 2. they can be cloned by each Daiquiri code block (cf. `DaiquiriStyles.tsx`).
 */
module.exports = function docusaurusPluginSass() {
  return {
    name: 'daiquiri-styles',

    injectHtmlTags() {
      return {
        preBodyTags: [
          {
            tagName: 'iframe',
            attributes: { id: 'daiquiri-styles', hidden: true }
          },
          {
            tagName: 'div',
            attributes: { id: 'daiquiri-bootstrap-styles', hidden: true }
          }
        ]
      };
    },

    configureWebpack(_, isServer, utils) {
      const { getStyleLoaders } = utils;
      const isProd = process.env.NODE_ENV === 'production';

      const loaders = getStyleLoaders(isServer);
      const cssLoader = loaders.find(({ loader }) =>
        /[\/\\]css-loader/.test(loader)
      );
      const postCssLoader = loaders.find(({ loader }) =>
        /[\/\\]postcss-loader/.test(loader)
      );

      const sharedLoaders = [
        // 4. Resolve stylesheet imports (`@import`) and asset URLs (`url()`)
        cssLoader,
        // 3. Add vendor prefixes, etc.
        ...(postCssLoader ? [postCssLoader] : []), // undefined when `isServer` is `true` 🤷‍♀️
        {
          // 2. Resolve relative paths in SCSS partials
          loader: 'resolve-url-loader',
          options: { sourceMap: !isProd }
        },
        {
          // 1. Transpile SCSS to CSS
          loader: 'sass-loader',
          options: {
            sourceMap: true
          }
        }
      ];

      return {
        module: {
          rules: [
            {
              test: /\.s[ca]ss$/,
              exclude: /bootstrap\/modal\.scss$/,
              use: [
                {
                  // 5. Inject resulting styles into the page
                  loader: 'style-loader',
                  options: {
                    insert: function insertInTemplateTag(element) {
                      const iframe = /** @type {HTMLIFrameElement} */ (
                        document.getElementById('daiquiri-styles')
                      );
                      const { head } = iframe.contentWindow.document;
                      head.appendChild(element);
                    }
                  }
                },
                ...sharedLoaders
              ]
            },
            // Mimial bootstrap sass to enable modals to work without polluting
            // docusaurus too much
            {
              test: /bootstrap\/modal\.scss$/,
              use: [
                {
                  // 5. Inject resulting styles into the page
                  loader: 'style-loader',
                  options: {
                    insert: function insertInTemplateTag(element) {
                      const div = /** @type {HTMLIFrameElement} */ (
                        document.getElementById('daiquiri-bootstrap-styles')
                      );
                      div.appendChild(element);
                    }
                  }
                },
                ...sharedLoaders
              ]
            }
          ]
        }
      };
    }
  };
};
