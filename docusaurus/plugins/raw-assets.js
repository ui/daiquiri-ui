/*
 * NOTE: after editing this file, delete /docusaurus/node_modules/.cache
 * for the changes to take effect.
 */

/*
 * Load iframe-resizer's `contentWindow` script as raw text so it can be
 * injected into each Daiquiri code block's iframe.
 */
module.exports = function docusaurusPluginSass() {
  return {
    name: 'raw-assets',

    configureWebpack(config) {
      const tsxRule = config.module.rules.find(r => r.test.test('.tsx'));
      tsxRule.resourceQuery = { not: [/raw/] };

      return {
        module: {
          rules: [
            {
              resourceQuery: /raw/,
              type: 'asset/source' // https://webpack.js.org/guides/asset-modules/
            }
          ]
        }
      };
    }
  };
};
