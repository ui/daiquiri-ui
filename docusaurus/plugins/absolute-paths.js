/*
 * NOTE: after editing this file, delete /docusaurus/node_modules/.cache
 * for the changes to take effect.
 */

const path = require('path');

const DAIQUIRI_ROOT_FOLDERS = [
  'components',
  'config',
  'connect',
  'data',
  'helpers',
  'messagehandler',
  'providers',
  'resources',
  'scss',
  'services',
  'store',
  'types'
];

/*
 * Resolve absolute paths in Daiquiri's source files.
 */
module.exports = function docusaurusPluginSass() {
  return {
    name: 'absolute-paths',

    configureWebpack(config) {
      DAIQUIRI_ROOT_FOLDERS.forEach(folder => {
        config.resolve.alias[folder] = path.resolve(
          __dirname,
          '../../src',
          folder
        );
      });
    }
  };
};
