# Daiquiri UI

A react/redux client for https://gitlab.esrf.fr/ui/daiquiri

Latest documentation can be found at https://ui.gitlab-pages.esrf.fr/daiquiri-ui
