import PropTypes from 'prop-types';
import Type from 'types/Type';

class SampleType extends Type {
  types = {
    name: PropTypes.string.isRequired,
    sampleid: PropTypes.number.isRequired,
    datacollections: PropTypes.number.isRequired
  };

  defaults = {
    offsetx: undefined,
    offsety: undefined
  };
}

export default new SampleType();
