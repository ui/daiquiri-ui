/**
 * DataCollection schema description
 */
export interface DataCollection {
  beamsizeatsamplex: any;
  beamsizeatsampley: any;
  comments: string | null;
  datacollectiongroupid: number | null;
  datacollectionid: number;
  datacollectionnumber: number;
  datacollections: number;
  dx_mm: any;
  dy_mm: any;
  experimenttype: any;
  exposuretime: any;
  filetemplate: string | null;
  imagedirectory: string | null;
  numberofimages: number | null;
  numberofpasses: any;
  patchesx: number | null;
  patchesy: number | null;
  runstatus: string | null;
  sample: string | null;
  sampleid: number | null;
  steps_x: number | null;
  steps_y: number | null;
  subsampleid: number | null;
  wavelength: number | null;
  xbeam: any;
  ybeam: any;

  snapshots?: Record<string, boolean>;

  starttime: string | null; // DD-MM-YYYY hh:mm:ss
  endtime: string | null; // DD-MM-YYYY hh:mm:ss
  duration: number; // In second, only updated at the end
}
