/**
 * As exposed by Redux and the server
 */
export type ScanStates =
  | 'CREATED'
  | 'PREPARING'
  | 'RUNNING'
  | 'FINISHED'
  | 'ABORTED'
  | 'FAILED';

/**
 * As exposed by Redux and the server
 */
export interface ScanShapeSchema {
  npoints1?: number;
  npoints2?: number;
  dim?: number;
  requests?: Record<string, any>;
}

/**
 * As exposed by Redux and the server
 */
export interface ScanSchema {
  scanid: number;
  node_name?: string;
  npoints?: number;
  shape?: ScanShapeSchema;
  count_time?: number;
  filename?: string;
  start_timestamp?: number;
  end_timestamp?: number;
  estimated_time?: number;
  status?: ScanStates;
  title?: string;
  type?: string;
  children?: Record<string, any>[];
  group?: boolean;
}
