import type Qty from 'js-quantities';

/**
 * Tomo scan as exposed by Redux
 */
export interface TomoScan {
  scanid: number;
  progress: number;
}

/**
 * Tomo detector as exposed by Redux
 */
export interface TomoDetector {
  running?: TomoScan | null;
  detector_id: string;
  node_name: string;
  data?: {
    scanid: number;
    frame_no: number;
  };
  dark?: number;
  flat?: number;
}

type BlissScanState =
  // BLISS states
  | 'IDLE'
  | 'PREPARING'
  | 'STARTING'
  | 'STOPPING'
  | 'DONE'
  | 'USER_ABORTED'
  | 'KILLED'
  // Extra state added by Daiquiri
  | 'UNKNOWN';

/**
 * Tiling tomo scan sequence as exposed by Redux
 */
export interface TilingTomoScan {
  state: BlissScanState;
  scanid: number;
  tomovisId: string | null;
  keyid: string;
}

/**
 * Metadata from a tomo scan sequence as exposed by Redux
 */
export interface TomoScanInfo {
  subscans: string[] | undefined;
  activesubscan: number;
  state: BlissScanState;
  datacollectionid: number;
  scanid: number;
  title: string;
  sinogram?: {
    actualnbpoints?: number;
  };
  // FIXME: This key is only used by the Redux request
  //        This should be removed at some point
  keyid?: number;
}

export interface TomoScanRoi {
  // absolute location of center of the ROI in the z-axis
  centerZ: number; // in mm
  // absolute location of center of the ROI
  center: {
    x: number; // in mm
    y: number; // in mm
  };
  // absolute location of the rotation axis
  axis: {
    x: number; // in mm
    y: number; // in mm
  };
  // diameter of the ROI
  width: number | null; // in mm
  height: number | null; // in mm
}

export interface TomoScanTaskActions {
  removeScanTask: (id: string) => void;
  createScanTask: (action: {
    x: Qty;
    y: Qty;
    z: Qty;
    detector: string;
    magnification: number | null;
    // FIXME: Ideally it would be good to infer that inside the method
    fov: [Qty, Qty];
    pixelSize: [Qty, Qty];
    active?: boolean;
  }) => void;
  reorderScanTask: (from: number, to: number) => void;
  moveScanTaskRoi: (id: string, x: Qty, y: Qty, z: Qty) => void;
  activateScanTask: (id: string) => void;
}

export interface TomoDataCollectionMetaActions {
  updateDataCollectionMeta: (
    dataCollectionId: number,
    params: {
      theoricalCor?: number | null;
      estimatedCor?: number | null;
      requestedCor?: number | null;
      actualCor?: number | null;
    }
  ) => void;
}

export interface TomoScanTask {
  id: string;
  roi: TomoScanRoi;

  // UI related property which is true when the task is selected
  // only one task is active at a time
  active: boolean;

  // If not null a detector was selected
  acquisition?: {
    detector: string;
    magnification: number | null;
    fov: [number, number]; // in mm
    pixelSize: [number, number]; // in um
  } | null;

  estimationTime: number | null;
  state: 'defined' | 'valid' | 'processing' | 'finished';
  kind: 'default' | 'zseries' /* | 'helical'*/;

  scanParameters: {
    [name: string]: any;

    //
    // Parameters exposed by tomo_config
    //

    // energy: 0.0;
    // source_sample_distance: 145000.0;
    // sample_detector_distance: 0.0;
    start_pos?: number;
    range?: number;
    flat_n?: number;
    dark_n?: number;
    tomo_n?: number;
    exposure_time?: number;

    //
    // Parameters exposed by basic sequence
    //

    comment?: string;
    scan_type?: 'step' | 'continuous' | 'sweep' | 'interlaced';
    // return_images_aligned_to_flats: boolean;
    // save_flag: boolean;
    // display_flag: boolean;
    // latency_time: number;
    // return_to_start_pos: boolean;
    // projection_groups: boolean;
    flat_on?: boolean;
    dark_on?: boolean;
    // dark_at_start: boolean;
    // flat_at_start: boolean;
    // dark_at_end: boolean;
    // flat_at_end: boolean;
    // images_on_return: boolean;
    // full_frame_position: number;
    // acquisition_position: number;
    // return_to_full_frame_position: boolean;
    // move_to_acquisition_position: boolean;
    half_acquisition?: boolean;
    shift_in_fov_factor?: number;
    shift_in_mm?: number;
    shift_in_pixels?: number;
    shift_type?: 'factor' | 'mm' | 'pixel';

    //
    // Parameters exposed as zseries argument
    //

    zarg_start?: number; // in mm
    zarg_stop?: number; // in mm
    zarg_step_n?: number;

    //
    // Parameters exposed by zseries sequence
    //

    // exposed as params
    // beam_check: boolean;
    // refill_check: boolean;
    // wait_for_beam: boolean;

    //
    // Parameters exposed by helical sequence
    //

    // step_size: number;
    // step_time: number;
    // slave_start_pos: number;
    // slave_step_size: number;
    // acq_time: number;
    // npoints: number;
    // scan_mode: FScanMode.TIME,
    // gate_mode: FScanTrigMode.TIME,
    // camera_signal: FScanCamSignal.LEVEL,
    // save_flag: boolean;
    // display_flag: boolean;
    // sync_encoder: boolean;
    // home_rotation: boolean;
    // acc_margin: number;
    // slave_acc_margin: number;
    // sampling_time: number;
    // latency_time: number;
  };
}

/**
 * Identify a field-of-view / pixel-size which is available in related to
 * real detector and optic
 */
export interface TomoAvailableFieldOfView {
  detector: string;
  state: 'READY' | 'DETECTOR_OFFLINE';
  size: [number, number];
  userPixelSize?: number;
  autoPixelSize?: { magnification: number; cameraPixelSize: [number, number] };
}

/**
 * Identify some meta shared to the tomo widget and stored per datacollection
 */
export interface TomoDataCollectionMeta {
  // the center of rotation as defined by the motor position
  theoricalCor: number | null;
  // the center of rotation as guessed by nabu
  estimatedCor: number | null;
  // the center of rotation as requested by the user (during the request)
  requestedCor: number | null;
  // the actually used center of rotation
  actualCor: number | null;
}

export interface TomoConfigStore {
  tomovis_url?: string;
  tomovis_proxy_url?: string;
  avatar?: {
    sprite_sheet?: string;
  };
}

export interface TomoStore {
  scanTasks: any[];
  lastScanTaskId: undefined | number;
  datacollectionsMeta: Record<number, TomoDataCollectionMeta>;
  ns_detectors: {
    default: {
      results: Record<string, TomoDetector>;
    };
  };
  ns_scaninfo: {
    default: {
      results: {
        lastflat: TomoScanInfo | Record<string, never>;
        lastdark: TomoScanInfo | Record<string, never>;
        lastref: TomoScanInfo | Record<string, never>;
        lastgroup: TomoScanInfo | Record<string, never>;
        lastproj: TomoScanInfo | Record<string, never>;
        lasttiling?: TilingTomoScan | Record<string, never>;
      };
    };
  };
}
