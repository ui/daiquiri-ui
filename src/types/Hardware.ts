import type { HardwareTypes } from '@esrf/daiquiri-lib';

/**
 * Hardware Redux storage
 */
export interface HardwareStore {
  [id: string]: HardwareTypes.Hardware;
}

/**
 * Hardware as exposed by Redux
 */
export interface ObjectrefSchema extends HardwareTypes.Hardware {
  properties: {
    ref: string;
  };
}

/**
 * Hardware as exposed by Redux
 */
export interface TestSchema extends HardwareTypes.Hardware {
  properties: {
    state: string;
    number: number;
    string: string;
    option: string;
    read_only: number;
  };
}
