export interface AutoProcDescription {
  autoprocprogramid: number;
  datacollectionid: number;
  programs: string; // name of the program
  automatic: number;
  message: string;
  status: number | null; // 0: failed, 1: success, null: processing
  errors: number;
  warnings: number;
  starttime: string; // iso datetime
  endtime: string | null; // iso datetime
  duration: number | null; // in seconds
}
