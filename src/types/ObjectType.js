import PropTypes from 'prop-types';
import Type from 'types/Type';

class ObjectType extends Type {
  types = {
    positions: PropTypes.objectOf(PropTypes.number),
    subsampleid: PropTypes.number.isRequired,
    type: PropTypes.string.isRequired,
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
    x2: PropTypes.number,
    y2: PropTypes.number,
    datacollections: PropTypes.number.isRequired
  };

  defaults = {
    x2: undefined,
    y2: undefined
  };
}

export default new ObjectType();
