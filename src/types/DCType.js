import PropTypes from 'prop-types';
import Type from 'types/Type';

class DCType extends Type {
  types = {
    datacollectionid: PropTypes.number.isRequired,
    experimenttype: PropTypes.string.isRequired,
    starttime: PropTypes.string,
    endtime: PropTypes.string
  };

  defaults = {
    starttime: undefined,
    endtime: undefined
  };
}

export default new DCType();
