import type { HardwareTypes } from '@esrf/daiquiri-lib';
import type { DataCollection } from './DataCollection';
import type { TomoConfigStore, TomoStore } from './Tomo';

/**
 * Describe the redux storage
 */
export interface ReduxState {
  app: {
    ns_config: {
      default: {
        results: {
          // Available when the tomo component is loaded server side
          tomo?: TomoConfigStore;
        };
      };
    };
  };
  hardware: {
    ns_hardware: {
      default: {
        results: Record<string, HardwareTypes.Hardware>;
      };
    };
  };
  metadata: {
    samples: {
      current?: number;
      selection?: Record<number, number> | null;
    };
    ns_sample: {
      default: {
        results: Record<number, { name: string }>;
      };
    };
    ns_datacollections: {
      default: {
        results: Record<number, DataCollection>;
      };
    };
  };
  session: {
    current: {
      data: {
        blsession: string;
      };
      operator: boolean;
      operator_pending: boolean;
    };
  };
  // Available when the tomo component is loaded server side
  tomo?: TomoStore;
}
