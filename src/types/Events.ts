/**
 * Notification which can be send by server of jobs.
 */
export interface BeamlineNotification {
  type: string;
  autoprocprogramid?: number;
  datacollectionid?: number;
}
