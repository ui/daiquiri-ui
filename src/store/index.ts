import type { Reducer, Store } from 'redux';
import { createStore as rdxCreateStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import {
  persistStore as rdxPersistStore,
  persistReducer,
  persistCombineReducers,
} from 'redux-persist';
import SocketStorage from 'store/SocketStorage';
import {
  seamlessImmutableReconciler,
  // seamlessImmutableTransformCreator
} from 'redux-persist-seamless-immutable';
import { each } from 'lodash';

import { reducerRegistry, providerRegistry } from '@esrf-ui/redux-provider';

// const transformerConfig = {};

export interface PersistStore extends Store {
  persistor: any;
  enablePersist: () => void;
  disablePersist: () => void;
}

const persistConfig = {
  key: 'root',
  storage: new SocketStorage(),
  serialize: false,
  whitelist: [],
  stateReconciler: seamlessImmutableReconciler,
  // transforms: [seamlessImmutableTransformCreator(transformerConfig)],
  throttle: 100,
};

const combine = (reducers: Record<string, Reducer>) => {
  const persisted: Record<string, Reducer> = {};
  each(reducers, (reducer, key) => {
    persisted[key] = reducer;

    if (providerRegistry.instances[key]) {
      const providerReducer = providerRegistry.instances[key].reducers;
      if (providerReducer.persist) {
        persisted[key] = persistReducer(
          {
            ...persistConfig,
            whitelist: providerReducer.persist,
            key,
          },
          reducer
        );
      }
    }
  });

  return persistCombineReducers(persistConfig, persisted);
};

function persistStore(store: PersistStore) {
  if (!store.persistor) {
    /* @ts-expect-error: manualPersist does not exist in PersistorOptions */
    store.persistor = rdxPersistStore(store, { manualPersist: true });
  }
}

export const createStore = () => {
  const store = rdxCreateStore(
    combine(reducerRegistry.getReducers()),
    composeWithDevTools(applyMiddleware(thunk))
  ) as PersistStore;

  reducerRegistry.setOnChange((reducers: any) => {
    store.replaceReducer(combine(reducers));
  });

  // HMR
  if (process.env.NODE_ENV !== 'production') {
    /* @ts-expect-error: module.hot does not exist */
    if (module.hot) {
      /* @ts-expect-error: module.hot does not exist */
      module.hot.accept('@esrf-ui/redux-provider', () => {
        store.replaceReducer(combine(reducerRegistry.getReducers()));
      });
    }
  }

  const enablePersist = () => {
    persistStore(store);
    store.persistor.persist();
  };

  const disablePersist = () => {
    if (store.persistor) {
      store.persistor.pause();
    }
  };

  store.enablePersist = enablePersist;
  store.disablePersist = disablePersist;

  return store;
};

export default createStore();
