import SocketIOService from 'services/SocketIOService';

export default class ServerStorage {
  public setItem(key: any, value: any) {
    return SocketIOService.emit('persist', 'set_item', [key, value]);
  }

  public getItem(key: any, cb: any) {
    return SocketIOService.emit('persist', 'get_item', key, (value: any) =>
      cb?.(false, value)
    );
  }

  public removeItem(key: any) {
    return SocketIOService.emit('persist', 'rem_item', key);
  }

  public getAllKeys(cb: any) {
    return SocketIOService.emit(
      'persist',
      'get_all_items',
      null,
      (value: any) => cb?.(false, value)
    );
  }
}
