// eslint-disable import/prefer-default-export

/// <reference types="react-scripts" />

declare module 'redux-persist-seamless-immutable' {
  export const seamlessImmutableReconciler: any;
}

declare module '@esrf-ui/redux-provider' {
  export const reducerRegistry: any;
  export const providerRegistry: any;

  class NamespaceInstance<T> {
    public localSelector(name: string, state: any): { [id: string]: T };

    public updateLocal(request: Partial<T>): void;
  }

  class Namespace<T> {
    public getInstance(name: string): NamespaceInstance<T>;
  }

  export class Provider {
    public key: string;

    public actions: Actions;
    public reducers: Reducers;
    public selectors: Selectors;
    protected providers: Provider[];

    public store: Store;

    public start();
    public setStore(store: Store);

    public selector(path: string, state: any): any;
    public slice(state: any);

    public dispatch(actionId: string, action?: any): Promise<any>;

    public getNamespace(name: string): Namespace<T>;
  }

  export class Reducers {
    public merge(state: any, newState: any);

    public setIn(state: any, keys: any[], newSubState: any);

    public getNamespace(name: string): Namespace<T>;
  }

  export class Selectors<T> {
    public getNamespace(name: string): Namespace<T>;
  }

  export class CachedSelectors<T> extends Selectors<T> {}

  export class Actions {
    public constructor(key: string);

    public store: Store;

    public dispatch(actionId: string, action: any): Promise<any>;

    public dispatchSimple(actionId: string, payload?: any);

    public getNamespace<T>(name: string): Namespace<T>;
  }
}

declare module 'stacktrace-gps' {
  export default class StackTraceGPS {
    public constructor();
    public pinpoint(stacktrace: StackFrame): {
      then: (
        callback: (frame: StackFrame) => void,
        errback: (frameError: StackFrame) => void
      ) => void;
    };
  }
}

declare module '@cycjimmy/jsmpeg-player' {
  export class Player {
    public constructor(
      url: string,
      options: {
        canvas: HTMLCanvasElement;
        videoBufferSize: number;
        audio: boolean;
        protocols: string[];
      }
    );

    public destroy();
  }
}
