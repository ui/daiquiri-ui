import moment from 'moment';
import { each, keyBy, values, find } from 'lodash';

import {
  Provider,
  Reducers,
  CachedSelectors,
  Actions
} from '@esrf-ui/redux-provider';
import { namespaceMixin, Namespace } from 'providers/namespace';
import RestService from 'services/RestService';

class AppSelectors extends CachedSelectors {
  selectors = {
    ready: 'ready',
    alerts: 'alerts',
    toasts: 'toasts',

    layoutCurrent: 'layout.current',

    uiVersion: 'uiVersion',

    offcanvas: 'offcanvas',
    modal: 'modal',
    sidebar: 'sidebar',
    tab: 'tab',
    form: 'form',
    logging: 'logging.logging',
    loggingFetching: 'logging.fetching',
    loggingEmpty: 'logging.empty',

    networkError: 'networkError',

    cursor: 'mouse.cursor',
    window: 'window',

    chatCursor: 'chat.cursor',

    config: (state, extra) => {
      const cns = this.getNamespace('config').getInstance('default');
      const config = cns.localSelector('results', state);
      if (extra) return config[extra];
      return config;
    }
  };

  selectSelectedLayout(state) {
    const lns = this.getNamespace('layouts').getInstance('default');
    return lns.localSelector('results', state)[state.layout.current];
  }
}

class AppReducers extends Reducers {
  persist = [
    'offcanvas',
    'sidebar',
    'modal',
    'tab',
    'form',
    'layout',
    'mouse',
    'window'
  ];

  initialState = {
    toasts: [],
    alerts: [],
    offcanvas: {},
    modal: {},
    sidebar: {},
    tab: {},
    form: {},

    layout: {
      current: 0
    },
    ready: false,
    logging: {
      logging: {},
      empty: false,
      fetching: false,
      fetched: false
    },
    networkError: false,
    uiVersion: {
      current: null,
      available: null,
      fetching: false,
      fetched: false
    },
    mouse: {
      cursor: { x: 0, y: 0 }
    },
    window: {
      width: 0,
      height: 0
    },
    chat: {
      cursor: 0
    }
  };

  toastId = 0;

  alertId = 0;

  onAppReady(state) {
    return this.merge(state, { ready: true });
  }

  onAddLog(state, action) {
    return this.setIn(
      state,
      ['logging', 'logging', action.payload.epoch],
      action.payload
    );
  }

  onFetchLogs(state) {
    return this.setIn(
      state,
      ['logging'],
      this.merge(state.logging, { fetching: true })
    );
  }

  onFetchLogsResolved(state, action) {
    const { user } = action.payload.data.logs;
    return this.setIn(state, ['logging'], {
      fetching: false,
      fetched: true,
      empty: user.length === 0,
      logging: {
        ...state.logging.logging,
        ...keyBy(user, l => l.epoch)
      }
    });
  }

  onFetchLogsRejected(state, action) {
    return this.setIn(
      state,
      ['logging'],
      this.merge(state.logging, {
        fetching: false,
        fetched: false,
        error: action.payload
      })
    );
  }

  onFetchUiVersion(state) {
    const newUI = this.merge(state.uiVersion, {
      fetched: false,
      fetching: true
    });

    return this.setIn(state, ['uiVersion'], newUI);
  }

  onFetchUiVersionRejected(state) {
    const newUI = this.merge(state.uiVersion, {
      fetched: false,
      fetching: false
    });
    return this.setIn(state, ['uiVersion'], newUI);
  }

  onFetchUiVersionResolved(state, action) {
    const newUI = this.merge(state.uiVersion, {
      fetched: true,
      fetching: false,
      [state.uiVersion.current ? 'available' : 'current']:
        action.payload.data.version
    });

    return this.setIn(state, ['uiVersion'], newUI);
  }

  onAddToast(state, action) {
    return this.setIn(
      state,
      ['toasts'],
      state.toasts.concat({
        ...action.payload,
        id: this.toastId++,
        time: moment().format('HH:mm:ss')
      })
    );
  }

  onRemoveToast(state, action) {
    return this.setIn(
      state,
      ['toasts'],
      state.toasts.filter(o => o.id !== action.payload)
    );
  }

  onAddAlert(state, action) {
    return this.setIn(
      state,
      ['alerts'],
      state.alerts.concat([
        {
          ...action.payload,
          id: action.payload.id || this.alertId++,
          time: moment().format('HH:mm:ss')
        }
      ])
    );
  }

  onRemoveAlert(state, action) {
    return this.setIn(
      state,
      ['alerts'],
      state.alerts.filter(o => o.id !== action.payload)
    );
  }

  onToggleOffcanvas(state, action) {
    return this.setIn(
      state,
      ['offcanvas', action.payload],
      !state.offcanvas[action.payload]
    );
  }

  onToggleModal(state, action) {
    return this.setIn(
      state,
      ['modal', action.payload],
      !state.modal[action.payload]
    );
  }

  onShowModal(state, action) {
    return this.setIn(state, ['modal', action.payload], true);
  }

  onHideModal(state, action) {
    return this.setIn(state, ['modal', action.payload], false);
  }

  onSelectTab(state, action) {
    return this.setIn(state, ['tab', action.payload.key], action.payload.value);
  }

  onSetForm(state, action) {
    return this.setIn(
      state,
      ['form', action.payload.key],
      action.payload.value
    );
  }

  onToggleSidebar(state, action) {
    const actioned = !state.sidebar[action.payload];

    const all = {};
    each(state.sidebar, (v, k) => {
      all[k] = false;
    });

    return this.setIn(
      state,
      ['sidebar'],
      this.setIn(this.merge(state.sidebar, all), [action.payload], actioned)
    );
  }

  onSwitchLayout(state, action) {
    const lns = this.getNamespace('layouts').getInstance('default');
    const layouts = values(lns.localSelector('results', state));
    if (action.payload >= layouts.length) {
      return this.setIn(state, ['layout', 'current'], 0);
    }

    return this.setIn(state, ['layout', 'current'], action.payload);
  }

  onSwitchLayoutSlug(state, action) {
    const lns = this.getNamespace('layouts').getInstance('default');
    const layouts = values(lns.localSelector('results', state));
    const layout = find(
      layouts,
      l => l.acronym.toLowerCase() === action.payload
    );
    if (layout) {
      const idx = layouts.indexOf(layout);
      return this.setIn(state, ['layout', 'current'], idx);
    }

    return this.setIn(state, ['layout', 'current'], 0);
  }

  onNetworkError(state, action) {
    return this.merge(state, { networkError: action.payload });
  }

  onUpdateCursor(state, action) {
    return this.setIn(state, ['mouse', 'cursor'], action.payload);
  }

  onUpdateWindow(state, action) {
    return this.setIn(state, ['window'], action.payload);
  }

  onMarkAllMessagesReadResolved(state, action) {
    return this.setIn(state, ['chat', 'cursor'], action.payload.data.maxid);
  }
}

class AppActions extends Actions {
  actions = [
    'APP_READY',
    'ADD_LOG',
    'ADD_TOAST',
    'REMOVE_TOAST',
    'ADD_ALERT',
    'REMOVE_ALERT',
    'TOGGLE_OFFCANVAS',
    'TOGGLE_MODAL',
    'SHOW_MODAL',
    'HIDE_MODAL',
    'TOGGLE_SIDEBAR',
    'SELECT_TAB',
    'SET_FORM',
    'SWITCH_LAYOUT',
    'SWITCH_LAYOUT_SLUG',
    'VALIDATE_SCHEMA',
    'NETWORK_ERROR',

    'UPDATE_CURSOR',
    'UPDATE_WINDOW'
  ];

  asyncActions = {
    VALIDATE_SCHEMA: {
      promise: payload =>
        RestService.post(`/schema/validate/${payload.name}`, payload)
    },
    SAVE_SCHEMA_PRESET: {
      promise: payload =>
        RestService.post(`/schema/preset/${payload.name}`, payload)
    },
    FETCH_LOGS: {
      promise: payload => RestService.get('/logging', payload)
    },
    FETCH_UI_VERSION: {
      promise: payload => RestService.get('/meta.json', {}, false)
    },
    MARK_MESSAGE_READ: {
      promise: payload => RestService.patch(`/chat/${payload}`)
    },
    MARK_ALL_MESSAGES_READ: {
      promise: payload => RestService.patch('/chat', payload)
    }
  };

  dispatchAddAlert(payload) {
    return (dispatch, getState) => {
      let exists = false;
      const alerts = this.selector('alerts', getState());
      each(alerts, a => {
        if (a.id === payload.id) exists = true;
      });

      if (exists) return;

      this.dispatchSimple('ADD_ALERT', payload);
    };
  }
}

const arrayToObj = arr => {
  const obj = {};
  each(arr, (o, i) => {
    obj[i] = o;
  });
  return obj;
};

class App extends namespaceMixin(Provider) {
  actions = AppActions;

  reducers = AppReducers;

  selectors = AppSelectors;

  nss = {
    saving: new Namespace({
      action: 'SAVING',
      transform: data => data,
      promise: payload => RestService.get('/saving', payload)
    }),

    layouts: new Namespace({
      action: 'LAYOUT',
      transform: data => arrayToObj(data.rows),
      promise: payload => RestService.get('/layout', payload)
    }),

    config: new Namespace({
      action: 'CONFIG',
      reset: false,
      transform: data => data,
      promise: payload => RestService.get('/components/config', payload)
    }),

    schema: new Namespace({
      action: 'SCHEMA',
      transform: data => data,
      promise: payload => RestService.get('/schema', payload),
      single: payload => RestService.get(`/schema/${payload}`)
    }),

    version: new Namespace({
      action: 'VERSION',
      reset: false,
      transform: data => data,
      promise: payload => RestService.get('/version', payload)
    }),

    chat: new Namespace({
      action: 'CHAT',
      merge: true,
      keyid: 'messageid',
      transform: data => keyBy(data.rows, m => m.messageid),
      promise: payload => RestService.get('/chat', payload),
      add: payload => RestService.post('/chat', payload)
    })
  };
}

export default new App('app');
