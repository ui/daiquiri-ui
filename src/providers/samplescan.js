import { keyBy } from 'lodash';
import {
  Provider,
  Reducers,
  Selectors,
  Actions
} from '@esrf-ui/redux-provider';
import RestService from 'services/RestService';

import { namespaceMixin, Namespace } from 'providers/namespace';

class SampleScanSelectors extends Selectors {
  selectors = {};
}

class SampleScanReducers extends Reducers {
  initialState = {};
}

class SampleScanActions extends Actions {}

class SampleScan extends namespaceMixin(Provider) {
  actions = SampleScanActions;

  reducers = SampleScanReducers;

  selectors = SampleScanSelectors;

  nss = {
    scans: new Namespace({
      action: 'SCANS_TYPES',
      keyid: 'actor',
      transform: data => keyBy(data.rows, d => d.actor),
      promise: payload => RestService.get('/samplescan', payload)
    })
  };
}

export default new SampleScan('samplescan');
