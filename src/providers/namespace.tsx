/* eslint-disable @typescript-eslint/default-param-last */
import type { ComponentType } from 'react';
import { Component } from 'react';
import { each, upperFirst, camelCase, keyBy, map, isEqual } from 'lodash';
import type { InferableComponentEnhancerWithProps } from 'types/reactUtils';

export const namespaceMixin = (superclass: any) =>
  class NamespaceProvider extends superclass {
    public nss: Record<string, any> = {};

    public start() {
      super.start();
      this.actions.getNamespace = this.getNamespace.bind(this);
      this.selectors.getNamespace = this.getNamespace.bind(this);
      this.reducers.getNamespace = this.getNamespace.bind(this);

      if (this.nss) {
        each(this.nss, (namespace: any) => {
          namespace.start(this);
        });
      }

      // eslint-disable-next-line unicorn/no-this-assignment, @typescript-eslint/no-this-alias
      const provider = this;
      this.reducers.onReset = function (state: any) {
        let newState = state;
        each(provider.nss, (namespace, key) => {
          each(namespace.namespaces, (instance, instanceKey) => {
            if (instance.getReset()) {
              newState = this.setIn(
                newState,
                [namespace.props.key, instanceKey],
                instance.getInitialState()
              );
            }
          });
        });
        return newState;
      };
      this.reducers.onReset.bind(this.reducers);
    }

    public setStore(store: any) {
      super.setStore(store);

      if (this.nss) {
        each(this.nss, (namespace: any) => {
          namespace.setStore();
        });
      }
    }

    public getNamespace(namespace: any) {
      if (namespace in this.nss) {
        return this.nss[namespace];
      }
      throw new Error(
        `No such namespace "${namespace}" on provider ${this.key}`
      );
    }

    public getNamespaces() {
      return this.nss;
    }
  };

function getDisplayName(WrappedComponent: ComponentType<any>) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

interface WithNamespace {
  <TOwnProps = {}, TDispatchProps = {}>( // eslint-disable-line @typescript-eslint/ban-types
    providers: Record<string, /* NamespaceProvider*/ any>
  ): InferableComponentEnhancerWithProps<TDispatchProps, TOwnProps>;
}

interface InjectedProps {
  providers?: Record<string, any>;
}

function _withNamespace<P>(
  providers: Record<string, any>
): (
  WrappedComponent: ComponentType<P & InjectedProps>
) => ComponentType<P & InjectedProps> {
  return (WrappedComponent) => {
    class WithNamespace extends Component<P & InjectedProps> {
      private providers: Record<string, any>;
      public constructor(props: InjectedProps & P) {
        super(props);

        this.providers = {};
        if (props.providers) {
          each(providers, (provider, name) => {
            each(props.providers, (namespaces, provkey) => {
              if (provkey === name) {
                this.providers[provkey] = {};

                each(namespaces, (opts, nskey) => {
                  const namespace = provider.getNamespace(nskey);
                  if (opts.namespace) {
                    namespace.checkNamespace(opts.namespace);
                  }

                  const inst = namespace.getInstance(
                    opts.namespace || 'default'
                  );

                  if (opts.params) {
                    inst.setParams(opts.params);
                  }

                  if (opts.pageSize) {
                    inst.setPageSize(opts.pageSize);
                  }

                  if (opts.page) {
                    inst.setPage(opts.page);
                  }

                  this.providers[provkey][nskey] = inst;
                });
              }
            });
          });
        }

        each(providers, (provider, name) => {
          if (!(name in this.providers)) this.providers[name] = {};
          each(provider.getNamespaces(), (ns, nskey) => {
            if (!(nskey in this.providers[name])) {
              this.providers[name][nskey] = ns.getInstance('default');
            }
          });
        });
      }

      public render() {
        const { providers: _, ...rest } = this.props;
        //  @ts-expect-error
        return <WrappedComponent {...rest} providers={this.providers} />;
      }
    }

    // @ts-expect-error
    WithNamespace.displayName = `WithNamespace(${getDisplayName(
      WrappedComponent
    )})`;

    return WithNamespace;
  };
}

export const withNamespace = _withNamespace as WithNamespace;

export class Namespace {
  private readonly props: any;
  private readonly namespaces: Record<string, any>;
  /*
  static propTypes = {
    action: PropTypes.string.isRequired,
    transform: PropTypes.func,
    order: PropTypes.func,
    promise: PropTypes.func.isRequired,
    keyid: PropTypes.string,
    add: PropTypes.func,
    update: PropTypes.func,
    delete: PropTypes.func,
    single: PropTypes.func,

    initialResults: PropTypes.any,
  };*/

  public constructor(props: any) {
    this.props = {
      key: `ns_${camelCase(props.action).toLowerCase()}`,
      ...props,
    };
    /*
    PropTypes.checkPropTypes(
      // eslint-disable-next-line react/forbid-foreign-prop-types
      Namespace.propTypes,
      this.props,
      'prop',
      'Namespace'
    );*/

    this.namespaces = {};
    this.addNamespace('default');
  }

  public setProvider(provider: any) {
    this.props.provider = provider;
    each(this.namespaces, (ns) => {
      ns.setProvider(provider);
    });

    this.addActions();
    this.addReducers();
  }

  public setStore() {
    each(this.namespaces, (ns, k) => {
      this.props.provider.dispatch(`ADD_NAMESPACE_${this.props.action}`, k);
    });
  }

  public start(provider: any) {
    this.setProvider(provider);
  }

  public checkNamespace(namespace: any) {
    if (!(namespace in this.namespaces)) {
      this.addNamespace(namespace);
    }
  }

  public getInstance(namespace: any) {
    return this.namespaces[namespace];
  }

  public getInstances() {
    return Object.keys(this.namespaces);
  }

  public addNamespace(namespace: any) {
    this.namespaces[namespace] = new NamespaceInstance({
      namespace,
      ...this.props,
    });

    this.namespaces[namespace].setProvider(this.props.provider);
    if (this.props.provider?.store) {
      this.props.provider.dispatch(
        `ADD_NAMESPACE_${this.props.action}`,
        namespace
      );
    }
  }

  public initialState() {
    const init: Record<string, any> = {};
    each(this.namespaces, (ns, k) => {
      init[k] = ns.getInitialState();
    });

    return {
      [this.props.key]: init,
    };
  }

  public getInitialState(namespace: any) {
    return this.namespaces[namespace].getInitialState();
  }

  public addActions() {
    const action = this.props.provider.actions;
    action.createAction(`ADD_NAMESPACE_${this.props.action}`);
  }

  public addReducers() {
    const reducer = this.props.provider.reducers;
    const camel = upperFirst(camelCase(this.props.action));
    const { key } = this.props;

    const namespace = this; // eslint-disable-line unicorn/no-this-assignment, @typescript-eslint/no-this-alias
    reducer[`onAddNamespace${camel}`] = function (state: any, action: any) {
      return this.setIn(
        state,
        [key, action.payload],
        namespace.getInitialState(action.payload)
      );
    };
    reducer[`onAddNamespace${camel}`].bind(reducer);
  }

  public addSelectors() {}

  public selector(selector: any, state: any, namespace = 'default') {
    return this.namespaces[namespace].selector(selector, state);
  }

  public fetch(namespace = 'default', all = false, payload: any) {
    if (all) {
      const ps: Promise<unknown>[] = [];
      each(this.namespaces, (ns) => {
        ps.push(ns.fetch(payload));
      });

      return Promise.all(ps);
    }
    return this.namespaces[namespace].fetch(payload);
  }

  public fetchSingle(namespace = 'default', all = false, payload: any) {
    if (all) {
      const ps: Promise<unknown>[] = [];
      each(this.namespaces, (ns) => {
        ps.push(ns.fetchSingle(payload));
      });

      return Promise.all(ps);
    }
    return this.namespaces[namespace].fetchSingle(payload);
  }

  public addLocal(namespace = 'default', all = false, payload: any) {
    if (all) {
      const ps: Promise<unknown>[] = [];
      each(this.namespaces, (ns) => {
        ps.push(ns.addLocal(payload));
      });

      return Promise.all(ps);
    }
    return this.namespaces[namespace].addLocal(payload);
  }

  public updateLocal(namespace = 'default', all = false, payload: any) {
    if (all) {
      const ps: Promise<unknown>[] = [];
      each(this.namespaces, (ns) => {
        ps.push(ns.updateLocal(payload));
      });

      return Promise.all(ps);
    }
    return this.namespaces[namespace].updateLocal(payload);
  }

  public setParams(params: any, namespace = 'default', fetch = false) {
    return this.namespaces[namespace].setParams(params, fetch);
  }

  public setPageSize(size: any, namespace = 'default') {
    this.namespaces[namespace].setPageSize(size);
  }

  public setPage(page: any, namespace = 'default') {
    this.namespaces[namespace].setPage(page);
  }
}

class NamespaceInstance {
  protected readonly props: any;
  protected readonly initialState: any;
  protected lastOrdered: any;

  public constructor(props: any) {
    this.props = props;

    this.initialState = {
      results:
        this.props.initialResults !== undefined
          ? this.props.initialResults
          : {},
      order: [],
      fetched: false,
      fetching: false,
      updating: false,
      adding: false,
      error: null,
      updatingError: null,
      addingError: null,
      page: props.page || 1,
      per_page: props.per_page,
      params: {
        ...props.params,
      },
    };
  }

  public getNamespace() {
    return this.props.namespace;
  }

  public getReset() {
    return this.props.reset !== undefined ? this.props.reset : true;
  }

  public setProvider(provider: any) {
    this.props.provider = provider;

    if (provider) {
      this.addActions();
      this.addReducers();
      this.addSelectors();
    }
  }

  public getInitialState() {
    return {
      ...this.initialState,
    };
  }

  public addActions() {
    const action = this.props.provider.actions;
    const base = `${this.props.action}_${this.props.namespace}`;

    action.createAsyncAction(`FETCH_${base}`, { promise: this.props.promise });
    action.createAsyncAction(`FETCH_SINGLE_${base}`, {
      promise: this.props.single,
    });
    action.createAction(
      `SET_PARAMS_${this.props.action}_${this.props.namespace}`
    );
    action.createAction(
      `SET_PAGE_SIZE_${this.props.action}_${this.props.namespace}`
    );
    action.createAction(
      `SET_PAGE_${this.props.action}_${this.props.namespace}`
    );

    each(['add', 'update', 'delete'], (t) => {
      if (this.props[t]) {
        action.createAsyncAction(`${t.toUpperCase()}_${base}`, {
          promise: this.props[t],
        });
        this.addCUDReducer(t);
      }
    });

    each(['adding', 'updating', ''], (type) => {
      const ty = type ? `${type.toUpperCase()}_` : '';
      action.createAction(
        `CLEAR_${ty}ERROR_${this.props.action}_${this.props.namespace}`
      );
    });

    action.createAction(
      `UPDATE_LOCAL_${this.props.action}_${this.props.namespace}`
    );
    this.addCUDReducer('updateLocal');

    action.createAction(
      `ADD_LOCAL_${this.props.action}_${this.props.namespace}`
    );
    this.addCUDReducer('addLocal');

    action.createAction(
      `DELETE_LOCAL_${this.props.action}_${this.props.namespace}`
    );
    this.addCUDReducer('deleteLocal');
  }

  public addCUDReducer(type: any) {
    const reducer = this.props.provider.reducers;
    const camel = upperFirst(camelCase(this.props.action));
    const { key, keyid, updateReplace } = this.props;

    const k = this.props.namespace;
    const baseName = `${camel}${upperFirst(k)}`;

    if (type === 'add') {
      reducer[`onAdd${baseName}`] = function (
        state: any,
        action: { payload: Record<string, any> }
      ) {
        return this.setIn(state, [key, k, 'adding'], true);
      };

      reducer[`onAdd${baseName}Resolved`] = function (
        state: any,
        action: { payload: Record<string, any> }
      ) {
        const newVal = this.setIn(
          state,
          [key, k, 'results', action.payload.data[keyid]],
          action.payload.data
        );
        return this.setIn(newVal, [key, k, 'adding'], false);
      };

      reducer[`onAdd${baseName}Rejected`] = function (
        state: any,
        action: { payload: Record<string, any> }
      ) {
        return this.setIn(
          state,
          [key, k],
          this.merge(state[key][k], {
            adding: false,
            addingError: action.payload,
          })
        );
      };

      each(['', 'Resolved', 'Rejected'], (ty) => {
        reducer[`onAdd${baseName}${ty}`].bind(reducer);
      });

      reducer[`onClearAddingError${baseName}`] = function (state: any) {
        return this.setIn(state, [key, k, 'addingError'], null);
      };
      reducer[`onClearAddingError${baseName}`].bind(reducer);
    }

    if (type === 'addLocal') {
      reducer[`onAddLocal${baseName}`] = function (
        state: any,
        action: { payload: Record<string, any> }
      ) {
        return this.setIn(
          state,
          [key, k, 'results', action.payload[keyid]],
          action.payload
        );
      };
      reducer[`onAddLocal${baseName}`].bind(reducer);
    }

    if (type === 'update') {
      reducer[`onUpdate${baseName}`] = function (state: any) {
        return this.setIn(state, [key, k, 'updating'], true);
      };

      reducer[`onUpdate${baseName}Rejected`] = function (
        state: any,
        action: { payload: Record<string, any> }
      ) {
        return this.setIn(
          state,
          [key, k],
          this.merge(state[key][k], {
            updating: false,
            updatingError: action.payload,
          })
        );
      };

      reducer[`onUpdate${baseName}Resolved`] = function (
        state: any,
        action: { payload: Record<string, any> }
      ) {
        let newState;
        if (updateReplace) {
          newState = this.setIn(
            state,
            [key, k, 'results'],
            action.payload.data
          );
        } else {
          const current = state[key][k].results[action.payload.data[keyid]];
          newState = this.setIn(
            state,
            [key, k, 'results', action.payload.data[keyid]],
            this.merge(current, action.payload.data)
          );
        }

        return this.setIn(newState, [key, k, 'updating'], false);
      };

      each(['', 'Resolved', 'Rejected'], (ty) => {
        reducer[`onUpdate${baseName}${ty}`].bind(reducer);
      });

      reducer[`onClearUpdatingError${baseName}`] = function (state: any) {
        return this.setIn(state, [key, k, 'updatingError'], null);
      };
      reducer[`onClearUpdatingError${baseName}`].bind(reducer);
    }

    if (type === 'updateLocal') {
      reducer[`onUpdateLocal${baseName}`] = function (
        state: Record<string, any>,
        action: {
          payload: {
            [key: string]: any;
            __mergekey__?: string;
            __mergedeep__?: boolean;
          };
        }
      ) {
        if (updateReplace) {
          return this.setIn(state, [key, k, 'results'], action.payload);
        }
        const {
          __mergekey__: mergeKey,
          __mergedeep__: mergeDeep = true,
          ...payload
        } = action.payload;
        const current = state[key][k].results[payload[keyid]];
        if (!current) return state;

        if (!mergeKey) {
          return this.setIn(
            state,
            [key, k, 'results', payload[keyid]],
            mergeDeep
              ? this.mergeDeep(current, payload)
              : this.merge(current, payload)
          );
        }
        return this.setIn(
          state,
          [key, k, 'results', payload[keyid], mergeKey],
          mergeDeep
            ? this.mergeDeep(current[mergeKey], payload[mergeKey])
            : this.merge(current[mergeKey], payload[mergeKey])
        );
      };
      reducer[`onUpdateLocal${baseName}`].bind(reducer);
    }

    if (type === 'delete') {
      reducer[`onDelete${baseName}Resolved`] = function (
        state: any,
        action: { meta?: any }
      ) {
        return this.setIn(
          state,
          [key, k, 'results'],
          this.without(state[key][k].results, action.meta)
        );
      };
      reducer[`onDelete${baseName}Resolved`].bind(reducer);
    }

    if (type === 'deleteLocal') {
      reducer[`onDeleteLocal${baseName}`] = function (
        state: any,
        action: { payload: Record<string, any> }
      ) {
        return this.setIn(
          state,
          [key, k, 'results'],
          this.without(state[key][k].results, action.payload)
        );
      };
      reducer[`onDeleteLocal${baseName}`].bind(reducer);
    }
  }

  public addReducers() {
    const reducer = this.props.provider.reducers;
    const camel = upperFirst(camelCase(this.props.action));
    const { key } = this.props;
    const { keyid, merge } = this.props;

    const k = this.props.namespace;
    const baseName = `onFetch${camel}${upperFirst(k)}`;
    const transform =
      this.props.transform || ((data: any) => keyBy(data, (o) => o[keyid]));
    const order = this.props.order || ((data: any) => map(data, keyid));

    reducer[baseName] = function (state: any, action: any) {
      return this.setIn(
        state,
        [key, k],
        this.merge(state[key][k], {
          fetching: true,
          error: null,
        })
      );
    };

    reducer[`${baseName}Resolved`] = function (state: any, action: any) {
      const results = merge
        ? this.merge(state[key][k].results, transform(action.payload.data))
        : transform(action.payload.data);

      const resultsOrder = merge
        ? state[key][k].order.concat(order(action.payload.data))
        : order(action.payload.data);

      return this.setIn(
        state,
        [key, k],
        this.merge(state[key][k], {
          fetching: false,
          fetched: true,
          results,
          order: resultsOrder,
          total: action.payload.data.total,
        })
      );
    };

    reducer[`${baseName}Rejected`] = function (state: any, action: any) {
      return this.setIn(
        state,
        [key, k],
        this.merge(state[key][k], {
          fetching: false,
          fetched: false,
          error: action.payload,
          results: {},
          order: [],
        })
      );
    };

    each(['', 'Resolved', 'Rejected'], (ty) => {
      reducer[`${baseName}${ty}`].bind(reducer);
    });

    reducer[`onClearError${baseName}`] = function (state: any) {
      return this.setIn(state, [key, k, 'error'], null);
    };
    reducer[`onClearError${baseName}`].bind(reducer);

    const singleBase = `onFetchSingle${camel}${upperFirst(k)}`;
    reducer[`${singleBase}Resolved`] = function (state: any, action: any) {
      return this.setIn(
        state,
        [key, k],
        this.merge(state[key][k], {
          fetching: false,
          fetched: true,
          results: {
            ...state[key][k].results,
            [action.meta]: action.payload.data,
          },
        })
      );
    };
    reducer[`${singleBase}Rejected`] = function (state: any, action: any) {
      return this.setIn(
        state,
        [key, k],
        this.merge(state[key][k], {
          fetching: false,
          fetched: true,
          error: true,
        })
      );
    };
    reducer[singleBase] = reducer[baseName];
    reducer[`${singleBase}Rejected`].bind(reducer);
    reducer[`${singleBase}Resolved`].bind(reducer);

    reducer[`onSetParams${camel}${upperFirst(k)}`] = function (
      state: any,
      action: any
    ) {
      return this.setIn(
        state,
        [key, k, 'params'],
        this.merge(state[key][k].params, action.payload)
      );
    };
    reducer[`onSetParams${camel}${upperFirst(k)}`].bind(reducer);

    reducer[`onSetPageSize${camel}${upperFirst(k)}`] = function (
      state: any,
      action: any
    ) {
      return this.setIn(state, [key, k, 'per_page'], action.payload);
    };
    reducer[`onSetPageSize${camel}${upperFirst(k)}`].bind(reducer);

    reducer[`onSetPage${camel}${upperFirst(k)}`] = function (
      state: any,
      action: any
    ) {
      return this.setIn(state, [key, k, 'page'], action.payload);
    };
    reducer[`onSetPage${camel}${upperFirst(k)}`].bind(reducer);
  }

  public addSelectors() {
    const selector = this.props.provider.selectors;
    const baseSelector = `${this.props.key}.${this.props.namespace}`;
    each(
      [
        'results',
        'order',
        'fetching',
        'fetched',
        'error',
        'page',
        'per_page',
        'params',
        'total',
        'updating',
        'updatingError',
        'adding',
        'addingError',
      ],
      (p) => {
        const sel = `${baseSelector}.${p}`;
        selector.selectors[sel] = sel;
      }
    );

    selector.selectors[`${baseSelector}.ordered`] = (state: any) => {
      const newOrdered = map(
        this.localSelector('order', state),
        (id) => this.localSelector('results', state)[id]
      );
      if (!isEqual(newOrdered, this.lastOrdered)) {
        this.lastOrdered = newOrdered;
      }

      return this.lastOrdered;
    };
  }

  public providerSlice() {
    return this.props.provider.slice(this.props.provider.store.getState());
  }

  public slice() {
    return this.providerSlice()[this.props.key][this.props.namespace];
  }

  public setParams(params: any, fetch?: boolean) {
    const act = `SET_PARAMS_${this.props.action}_${this.props.namespace}`;
    const promise = this.props.provider.dispatch(act, params);

    if (fetch) return this.fetch();
    return promise;
  }

  public setPageSize(size: any, fetch: boolean) {
    const act = `SET_PAGE_SIZE_${this.props.action}_${this.props.namespace}`;
    const promise = this.props.provider.dispatch(act, size);

    if (fetch) return this.fetch();
    return promise;
  }

  public setPage(page: any, fetch: boolean) {
    const act = `SET_PAGE_${this.props.action}_${this.props.namespace}`;
    const promise = this.props.provider.dispatch(act, page);

    if (fetch) return this.fetch();
    return promise;
  }

  public fetch(payload?: any) {
    const fullState = this.props.provider.store.getState();
    const slice = this.slice();

    if (slice.fetching) {
      // console.log(
      //   'Already fetching',
      //   this.props.key,
      //   this.props.namespace,
      //   slice
      // );
      return new Promise((resolve, reject) => {
        resolve(undefined);
      });
    }

    const params: Record<string, any> = {};
    each(slice.params, (v, p) => {
      if (typeof v === 'string' && v.startsWith('$state.')) {
        const key = v.replace('$state.', '');
        params[p] = this.props.provider.selector(key, fullState);
      } else {
        params[p] = v;
      }
    });

    if (slice.page !== null && slice.page !== undefined) {
      params.page = slice.page;
    }

    return this.props.provider.dispatch(
      `FETCH_${this.props.action}_${this.props.namespace}`,
      {
        per_page: slice.per_page,
        ...params,
        ...payload,
      }
    );
  }

  public fetchSingle(payload: any) {
    return this.props.provider.dispatch(
      `FETCH_SINGLE_${this.props.action}_${this.props.namespace}`,
      payload
    );
  }

  public add(payload: any) {
    return this.props.provider.dispatch(
      `ADD_${this.props.action}_${this.props.namespace}`,
      payload
    );
  }

  public addLocal(payload: any) {
    return this.props.provider.dispatch(
      `ADD_LOCAL_${this.props.action}_${this.props.namespace}`,
      payload
    );
  }

  public update(payload: any) {
    return this.props.provider.dispatch(
      `UPDATE_${this.props.action}_${this.props.namespace}`,
      payload
    );
  }

  public updateLocal(payload: any) {
    return this.props.provider.dispatch(
      `UPDATE_LOCAL_${this.props.action}_${this.props.namespace}`,
      payload
    );
  }

  public delete(payload: any) {
    return this.props.provider.dispatch(
      `DELETE_${this.props.action}_${this.props.namespace}`,
      payload
    );
  }

  public deleteLocal(payload: any) {
    return this.props.provider.dispatch(
      `DELETE_LOCAL_${this.props.action}_${this.props.namespace}`,
      payload
    );
  }

  public clearError(type: any) {
    const ty = type ? `${type.toUpperCase()}_` : '';
    return this.props.provider.dispatch(
      `CLEAR_${ty}ERROR_${this.props.action}_${this.props.namespace}`
    );
  }

  public localSelector(selector: any, state: any) {
    const sel = `${this.props.key}.${this.props.namespace}.${selector}`;
    return this.props.provider.selectors.get(sel, state);
  }

  public selector(selector: any, state: any) {
    const sel = `${this.props.key}.${this.props.namespace}.${selector}`;
    return this.props.provider.selector(sel, state);
  }
}
