import { find, values, max, keyBy } from 'lodash';
import {
  Provider,
  Reducers,
  CachedSelectors,
  Actions
} from '@esrf-ui/redux-provider';
import { namespaceMixin, Namespace } from 'providers/namespace';
import autoPendingMixin from 'providers/AutoPendingMixin';
import RestService from 'services/RestService';

class TwoDSelectors extends CachedSelectors {
  selectors = {
    mapSelection: 'maps.selection',
    currentAction: 'currentAction',
    clampOrigin: 'sources.clampOrigin',
    fillOrigin: 'sources.fillOrigin',
    sourceUrl: 'sources.url',
    sourceHover: 'sources.hover',
    centreOnClick: 'centreOnClick',
    drawQuantize: 'drawQuantize',
    drawSnap: 'drawSnap',
    multiROI: 'multiROI',
    clampLOI: 'clampLOI',
    cursor: 'cursor',
    zoom: 'zoom',
    viewport: 'viewport',
    mapPointer: 'maps.pointer',
    mapHover: 'maps.hover',
    messages: 'messages',
    saveCanvas: 'saveCanvas',
    creatingAdditional: 'creatingAdditional'
  };

  selectMapSelectionCount(state) {
    return values(state.maps.selection).length;
  }

  selectOrigin(state) {
    const sources = this.getNamespace('sources').getInstance('default');
    const orig = find(values(sources.localSelector('results', state)), {
      origin: true
    });

    return orig;
  }

  selectDisableOrigin(state) {
    const orig = this.selectOrigin(state);
    return orig && orig.hide;
  }
}

class TwoDReducers extends Reducers {
  persist = [
    'currentAction',
    'zoom',
    'viewport',
    'centreOnClick',
    'drawQuantize',
    'drawSnap',
    'multiROI',
    'clampLOI',
    'cursor,',
    'sources',
    'maps'
  ];

  initialState = {
    currentAction: 'pan',
    zoom: 1,
    viewport: {},
    centreOnClick: false,
    drawQuantize: [5000, 5000],
    drawSnap: false,
    multiROI: false,
    clampLOI: false,
    cursor: false,
    sources: {
      clampOrigin: true,
      fillOrigin: false,
      hover: {},
      url: ''
    },
    maps: {
      selection: {},
      pointer: {},
      hover: {}
    },
    messages: {},
    saveCanvas: false
  };

  constructor(props) {
    super(props);
    this.compositeid = 1;
  }

  onSelectAction(state, action) {
    return this.merge(state, { currentAction: action.payload });
  }

  onClampOrigin(state, action) {
    return this.setIn(
      state,
      ['sources'],
      this.merge(state.sources, {
        clampOrigin: action.payload
      })
    );
  }

  onFillOrigin(state, action) {
    return this.setIn(
      state,
      ['sources'],
      this.merge(state.sources, {
        fillOrigin: action.payload
      })
    );
  }

  onSourceUrl(state, action) {
    return this.setIn(
      state,
      ['sources'],
      this.merge(state.sources, {
        url: action.payload
      })
    );
  }

  onSetSourceHover(state, action) {
    return this.setIn(state, ['sources', 'hover'], action.payload);
  }

  onCentreOnClick(state, action) {
    return this.merge(state, { centreOnClick: action.payload });
  }

  onChangeDrawQuantize(state, action) {
    return this.merge(state, { drawQuantize: action.payload });
  }

  onToggleDrawSnap(state, action) {
    return this.merge(state, { drawSnap: action.payload });
  }

  onToggleMultiRoi(state, action) {
    return this.merge(state, { multiROI: action.payload });
  }

  onToggleClampLoi(state, action) {
    return this.merge(state, { clampLOI: action.payload });
  }

  onToggleCursor(state, action) {
    return this.merge(state, { cursor: action.payload });
  }

  onSelectMap(state, action) {
    const maxVal = max(values(state.maps.selection)) || 0;
    return this.setIn(
      state,
      ['maps', 'selection', action.payload.mapid],
      maxVal + 1
    );
  }

  onUnselectMap(state, action) {
    return this.setIn(
      state,
      ['maps', 'selection'],
      this.without(state.maps.selection, action.payload)
    );
  }

  onResetMapSelection(state) {
    return this.setIn(state, ['maps', 'selection'], {});
  }

  onSetMapPointer(state, action) {
    return this.setIn(state, ['maps', 'pointer'], action.payload);
  }

  onSetMapHover(state, action) {
    return this.setIn(state, ['maps', 'hover'], action.payload);
  }

  onSetZoom(state, action) {
    return this.merge(state, { zoom: action.payload });
  }

  onSetViewport(state, action) {
    return this.merge(state, { viewport: action.payload });
  }

  onRegenerateMapsRejected(state) {
    return state;
  }

  onSetMessage(state, action) {
    return this.setIn(
      state,
      ['messages', action.payload.type],
      action.payload.message
    );
  }

  onSaveCanvas(state) {
    return this.merge(state, { saveCanvas: true });
  }

  onClearSaveCanvas(state) {
    return this.merge(state, { saveCanvas: false });
  }
}

class TwoDActions extends Actions {
  actions = [
    'MODIFY_SOURCE',

    'SELECT_ACTION',
    'CLAMP_ORIGIN',
    'FILL_ORIGIN',
    'SOURCE_URL',
    'CENTRE_ON_CLICK',

    'CHANGE_DRAW_QUANTIZE',
    'TOGGLE_DRAW_SNAP',
    'TOGGLE_MULTI_ROI',
    'TOGGLE_CLAMP_LOI',
    'TOGGLE_CURSOR',

    'SELECT_MAP',
    'UNSELECT_MAP',
    'RESET_MAP_SELECTION',
    'SET_MAP_POINTER',
    'SET_MAP_HOVER',
    'SET_SOURCE_HOVER',
    'SET_ZOOM',
    'SET_VIEWPORT',

    'UPDATE_ADDITIONAL',
    'UPDATE_ORIGIN',
    'UPDATE_SOURCE',

    'SET_MESSAGE',

    'SAVE_CANVAS',
    'CLEAR_SAVE_CANVAS'
  ];

  asyncActions = {
    REGENERATE_MAPS: {
      promise: payload => {
        const { subsampleid, ...pl } = payload;
        return RestService.post(
          `/imageviewer/maps/generate/${payload.subsampleid}`,
          pl
        );
      }
    },
    MOVE_TO_COORDS: {
      promise: payload => RestService.post('/imageviewer/move', payload)
    },
    MOVE_TO_SUBSAMPLE: {
      promise: payload => RestService.post(`/imageviewer/move/${payload}`)
    },
    SAVE_IMAGE: {
      promise: payload =>
        RestService.post('/imageviewer/sources/image', payload)
    },
    SAVE_CANVAS_REMOTE: {
      promise: payload =>
        RestService.post(`/imageviewer/image/${payload.sampleid}`, payload)
    },
    AUTOFOCUS_IMAGE: {
      promise: payload =>
        RestService.post('/imageviewer/sources/autofocus', payload)
    },
    EXPORT_SUBSAMPLES: {
      promise: payload => RestService.post('/imageviewer/export', payload)
    },
    MOSAIC: {
      promise: payload => RestService.post('/imageviewer/mosaic', payload)
    },
    ADDITIONAL_MAP: {
      promise: payload =>
        RestService.post('/imageviewer/maps/additional', payload)
    }
  };

  dispatchUpdateAdditional(payload) {
    this.dispatchSimple('UPDATE_ADDITIONAL');
    return () => {
      this.dispatch('UPDATE_ORIGIN', { additional: payload });
    };
  }

  dispatchUpdateOrigin(payload) {
    this.dispatchSimple('UPDATE_ORIGIN');
    return (dispatch, getState) => {
      const orig = this.selector('origin', getState());
      if (orig) {
        this.dispatch('UPDATE_SOURCE', {
          sourceid: orig.sourceid,
          ...payload
        });
      }
    };
  }

  dispatchUpdateSource(payload) {
    this.dispatchSimple('UPDATE_SOURCE');
    return () => {
      const sources = this.getNamespace('sources').getInstance('default');
      sources.updateLocal(payload);
    };
  }
}

class TwoD extends autoPendingMixin(namespaceMixin(Provider)) {
  actions = TwoDActions;

  reducers = TwoDReducers;

  selectors = TwoDSelectors;

  nss = {
    maps: new Namespace({
      action: 'MAPS',
      updateReplace: true,
      transform: data => data,
      update: payload => RestService.patch('/imageviewer/maps', payload),
      promise: payload => RestService.get('/imageviewer/maps', payload)
    }),

    sources: new Namespace({
      action: 'SOURCE',
      keyid: 'sourceid',
      transform: data => keyBy(data.rows, o => o.sourceid),
      promise: payload => RestService.get('/imageviewer/sources', payload)
    }),

    origin: new Namespace({
      action: 'ORIGIN',
      updateReplace: true,
      transform: data => data,
      update: payload =>
        RestService.patch('/imageviewer/sources/origin', payload),
      promise: payload =>
        RestService.get('/imageviewer/sources/origin', payload)
    }),

    markings: new Namespace({
      action: 'MARKING',
      keyid: 'markingid',
      promise: payload => RestService.get('/imageviewer/markings', payload),
      update: payload =>
        RestService.patch(`/imageviewer/markings/${payload.markingid}`, payload)
    })
  };
}

export default new TwoD('twod');
