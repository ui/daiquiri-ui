import { keyBy, map, get, debounce } from 'lodash';
import {
  Provider,
  Reducers,
  CachedSelectors,
  Actions,
} from '@esrf-ui/redux-provider';
import RestService from 'services/RestService';

import { namespaceMixin, Namespace } from 'providers/namespace';
import selectionMixin from 'providers/selection';

import app from 'providers/app';

function isValueFunction<T>(entity: unknown): entity is (state: T) => T {
  return typeof entity === 'function';
}

class MetadataSelectors extends CachedSelectors<any> {
  public selectors = {
    selectedAutoProcProgram: 'autoprocs.selected',
  };
}

class MetadataReducers extends Reducers {
  public persist = ['samples', 'subsamples'];

  public initialState = {
    autoprocprograms: {
      selected: undefined,
    },
  };

  public onSelectAutoProcProgram(state: any, action: any) {
    return this.setIn(state, ['autoprocs', 'selected'], action.payload);
  }

  public onUpdateUserCache<T>(
    state: any,
    action: { payload: { path: string[]; value: T | ((state: T) => T) } }
  ) {
    const { payload } = action;
    const { path, value } = payload;

    function getNewValue(): T {
      if (isValueFunction<T>(value)) {
        const prevValue = get(state.ns_cache.default.results.cache, path);
        return value(prevValue);
      }
      return value;
    }

    const newValue = getNewValue();
    return this.setIn(
      state,
      ['ns_cache', 'default', 'results', 'cache', ...path],
      newValue
    );
  }
}

class MetadataActions extends Actions {
  public actions = ['SELECT_AUTO_PROC_PROGRAM', 'UPDATE_USER_CACHE'];
  public debouncedSaveCache;

  public constructor(key: string) {
    super(key);
    this.debouncedSaveCache = debounce(this.saveCache, 2000);
  }

  public dispatchUpdateUserCache(payload: any) {
    console.log('metadata: update user cache');
    this.dispatchSimple('UPDATE_USER_CACHE', payload);
    this.debouncedSaveCache();
    return (dispatch: any) => {};
  }

  public saveCache() {
    console.log('metadata: save user cache');
    const currentValue =
      this.store.getState().metadata.ns_cache.default.results;
    this.dispatch('UPDATE_CACHE_default', currentValue);
  }

  public dispatchSelectSample(payload: any) {
    this.dispatchSimple('SELECT_SAMPLE', payload);
    return (dispatch: any) => {
      const saving = app.getNamespace('saving').getInstance('default');
      saving
        .setParams(
          {
            sampleid: payload,
          },
          true
        )
        .catch((error: any) => console.log('Couldnt set scan saving'));
    };
  }

  public dispatchSelectSubSample(payload: any) {
    this.dispatchSimple('SELECT_SUB_SAMPLE', payload);
    return (dispatch: any) => {
      const saving = app.getNamespace('saving').getInstance('default');
      saving.setParams(
        {
          subsampleid: payload.subsampleid,
        },
        true
      );
    };
  }
}

class Metadata extends selectionMixin(namespaceMixin(Provider)) {
  public actions = MetadataActions;

  public reducers = MetadataReducers;

  public selectors = MetadataSelectors;

  public selectable = {
    datacollections: {
      actionKey: 'DATA_COLLECTION',
      itemKey: 'datacollectionid',
    },
    subsamples: { actionKey: 'SUB_SAMPLE', itemKey: 'subsampleid' },
    samples: { actionKey: 'SAMPLE', itemKey: 'sampleid' },
  };

  public nss = {
    user: new Namespace({
      action: 'USER',
      transform: (data: any) => data,
      promise: (payload: any, state: any) =>
        RestService.get('/metadata/users/current', payload),
    }),

    cache: new Namespace({
      action: 'CACHE',
      updateReplace: true,
      transform: (data: any) => data,
      promise: (payload: any, state: any) =>
        RestService.get('/metadata/users/cache', payload),
      update: (payload: any) =>
        RestService.patch('/metadata/users/cache', payload),
    }),

    blsessions: new Namespace({
      action: 'BLSESSION',
      keyid: 'session',
      transform: (data: any) => keyBy(data.rows, (o) => o.session),
      promise: (payload: any, state: any) =>
        RestService.get('/metadata/sessions', payload),
    }),

    components: new Namespace({
      action: 'COMPONENT',
      keyid: 'componentid',
      transform: (data: any) => keyBy(data.rows, (o) => o.componentid),
      promise: (payload: any, state: any) =>
        RestService.get('/metadata/components', payload),
      add: (payload: any) => RestService.post('/metadata/components', payload),
      update: (payload: any) =>
        RestService.patch(
          `/metadata/components/${payload.componentid}`,
          payload
        ),
    }),

    samples: new Namespace({
      action: 'SAMPLE',
      keyid: 'sampleid',
      transform: (data: any) => keyBy(data.rows, (o) => o.sampleid),
      promise: (payload: any, state: any) =>
        RestService.get('/metadata/samples', payload),
      add: (payload: any) => RestService.post('/metadata/samples', payload),
      update: (payload: any) =>
        RestService.patch(`/metadata/samples/${payload.sampleid}`, payload),
    }),

    images: new Namespace({
      action: 'SAMPLE_IMAGES',
      keyid: 'sampleimageid',
      transform: (data: any) => keyBy(data.rows, (o) => o.sampleimageid),
      promise: (payload: any, state: any) =>
        RestService.get('/metadata/samples/images', payload),
    }),

    subsamples: new Namespace({
      action: 'SUBSAMPLE',
      keyid: 'subsampleid',
      transform: (data: any) => keyBy(data.rows, (o) => o.subsampleid),
      promise: (payload: any, state: any) =>
        RestService.get('/metadata/samples/sub', payload),
      add: (payload: any) => RestService.post('/metadata/samples/sub', payload),
      update: (payload: any) =>
        RestService.patch(
          `/metadata/samples/sub/${payload.subsampleid}`,
          payload
        ),
      delete: (payload: any) =>
        RestService.delete(`/metadata/samples/sub/${payload}`),
    }),

    sampletags: new Namespace({
      action: 'SAMPLE_TAGS',
      transform: (data: any) => data,
      promise: (payload: any, state: any) =>
        RestService.get('/metadata/samples/tags', payload),
    }),

    datacollections: new Namespace({
      action: 'DATA_COLLECTIONS',
      keyid: 'datacollectionid',
      transform: (data: any) => keyBy(data.rows, (o) => o.datacollectionid),
      order: (data: any) => map(data.rows, 'datacollectionid'),
      promise: (payload: any, state: any) =>
        RestService.get('/metadata/datacollections', payload),
      update: (payload: any) =>
        RestService.patch(
          `/metadata/datacollections/${payload.datacollectionid}`,
          payload
        ),
    }),

    dcattachments: new Namespace({
      action: 'DC_ATTACHMENTS',
      transform: (data: any) =>
        keyBy(data.rows, (o) => o.datacollectionfileattachmentid),
      promise: (payload: any, state: any) =>
        RestService.get('/metadata/datacollections/attachments', payload),
    }),

    sqis: new Namespace({
      action: 'SQIS',
      transform: (data: any) => data,
      promise: (payload: any, state: any) =>
        RestService.get(
          `/metadata/datacollections/sqis/${payload.datacollectionid}`,
          payload
        ),
    }),

    xrf_map_rois: new Namespace({
      action: 'XRF_MAP_ROI',
      keyid: 'maproiid',
      transform: (data: any) => keyBy(data.rows, (o) => o.maproiid),
      promise: (payload: any, state: any) =>
        RestService.get('/metadata/xrf/maps/rois', payload),
      add: (payload: any) =>
        RestService.post('/metadata/xrf/maps/rois', payload),
      update: (payload: any) =>
        RestService.patch(
          `/metadata/xrf/maps/rois/${payload.maproiid}`,
          payload
        ),
      delete: (payload: any) =>
        RestService.delete(`/metadata/xrf/maps/rois/${payload}`),
    }),

    xrf_composites: new Namespace({
      action: 'XRF_COMPOSITE',
      keyid: 'compositeid',
      transform: (data: any) => keyBy(data.rows, (o) => o.compositeid),
      promise: (payload: any, state: any) =>
        RestService.get('/metadata/xrf/maps/composite', payload),
      add: (payload: any) =>
        RestService.post('/metadata/xrf/maps/composite', payload),
      update: (payload: any) =>
        RestService.patch(
          `/metadata/xrf/maps/composite/${payload.compositeid}`,
          payload
        ),
      delete: (payload: any) =>
        RestService.delete(`/metadata/xrf/maps/composite/${payload}`),
    }),

    xrf_maps: new Namespace({
      action: 'XRF_MAP',
      keyid: 'mapid',
      transform: (data: any) => keyBy(data.rows, (o) => o.mapid),
      promise: (payload: any, state: any) =>
        RestService.get('/metadata/xrf/maps', payload),
      update: (payload: any) =>
        RestService.patch(`/metadata/xrf/maps/${payload.mapid}`, payload),
      delete: (payload: any) =>
        RestService.delete(`/metadata/xrf/maps/${payload}`),
    }),

    xrf_map_value: new Namespace({
      action: 'XRF_MAP_VALUE',
      initialResults: null,
      transform: (data: any) => data,
      promise: (payload: any) =>
        RestService.get(`/metadata/xrf/maps/value/${payload.mapid}`, payload),
    }),

    xrf_map_histograms: new Namespace({
      action: 'XRF_MAP_HISTOGRAM',
      transform: (data: any) => keyBy([data], (d) => d.mapid),
      merge: true,
      promise: (payload: any) =>
        RestService.get(
          `/metadata/xrf/maps/histogram/${payload.mapid}`,
          payload
        ),
    }),

    autoprocprograms: new Namespace({
      action: 'AUTOPROCPROGRAM',
      transform: (data: any) => keyBy(data.rows, (d) => d.autoprocprogramid),
      promise: (payload: any) =>
        RestService.get(`/metadata/autoprocs`, payload),
    }),

    appattachments: new Namespace({
      action: 'APP_ATTACHMENTS',
      transform: (data: any) =>
        keyBy(data.rows, (o) => o.autoprocprogramattachmentid),
      promise: (payload: any) =>
        RestService.get('/metadata/autoprocs/attachments', payload),
    }),

    appmessages: new Namespace({
      action: 'APP_MESSAGES',
      transform: (data: any) =>
        keyBy(data.rows, (o) => o.autoprocprogrammessageid),
      promise: (payload: any) =>
        RestService.get('/metadata/autoprocs/messages', payload),
    }),
  };
}

// @ts-expect-error
export default new Metadata('metadata');
