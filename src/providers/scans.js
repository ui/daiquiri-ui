import { keyBy, find, map } from 'lodash';

import {
  Provider,
  Reducers,
  Selectors,
  Actions
} from '@esrf-ui/redux-provider';
import { namespaceMixin, Namespace } from 'providers/namespace';
import selectionMixin from 'providers/selection';
import RestService from 'services/RestService';

class ScansSelectors extends Selectors {
  selectors = {
    scans: 'scans',
    fetchedScans: 'fetchedScans',
    currentPoint: 'currentPoint',
    currentImageNode: 'currentImageNode',
    running: 'running',
    runningScanid: 'running.scanid',
    selectedDataSeries: 'selectedSeries',
    selectedDataAxes: 'selectedAxes'
  };

  selectRunningScan(state) {
    const sns = this.getNamespace('list').getInstance('default');
    const scans = sns.localSelector('results', state);
    return find(scans, { scanid: state.running.scanid });
  }
}

class ScansReducers extends Reducers {
  initialState = {
    currentPoint: null,
    currentImageNode: null,
    running: {
      progress: 0,
      scanid: null
    },
    selectedSeries: {},
    selectedAxes: {}
  };

  onFetchScanStatusResolved(state, action) {
    return this.merge(state, {
      running: action.payload.data
    });
  }

  onScanCurrentPoint(state, action) {
    return this.merge(state, { currentPoint: action.payload });
  }

  onScanNew(state, action) {
    return this.setIn(state, ['running'], {
      progress: 0,
      scanid: action.payload.scanid
    });
  }

  onScanEnd(state) {
    return this.setIn(state, ['running'], {
      progress: 100,
      scanid: null
    });
  }

  onScanProgress(state, action) {
    return this.setIn(
      state,
      ['running'],
      this.merge(state.running, {
        progress: action.payload.progress
      })
    );
  }

  onScanDataSelectSeries(state, action) {
    return this.setIn(state, ['selectedSeries', action.payload], true);
  }

  onScanDataUnselectSeries(state, action) {
    return this.setIn(
      state,
      ['selectedSeries'],
      this.without(state.selectedSeries, action.payload)
    );
  }

  onScanDataResetSeries(state) {
    return this.setIn(state, ['selectedSeries'], {});
  }

  onScanDataSelectAxes(state, action) {
    return this.setIn(
      state,
      ['selectedAxes', action.payload.type],
      action.payload.axis
    );
  }

  onScanDataUnselectAxes(state, action) {
    return this.setIn(
      state,
      ['selectedAxes'],
      this.without(state.selectedAxes, action.payload)
    );
  }

  onScanDataResetAxes(state) {
    return this.setIn(state, ['selectedAxes'], {});
  }
}

class ScansActions extends Actions {
  actions = [
    'SCAN_NEW',
    'SCAN_END',
    'SCAN_PROGRESS',
    'SCAN_CURRENT_POINT',
    'SCAN_CURRENT_IMAGE_NODE',
    'SCAN_DATA_SELECT_SERIES',
    'SCAN_DATA_UNSELECT_SERIES',
    'SCAN_DATA_RESET_SERIES',
    'SCAN_DATA_SELECT_AXES',
    'SCAN_DATA_UNSELECT_AXES',
    'SCAN_DATA_RESET_AXES'
  ];

  asyncActions = {
    FETCH_SCAN_STATUS: {
      promise: () => {
        return RestService.get('/scans/status');
      }
    }
  };
}

class Scans extends selectionMixin(namespaceMixin(Provider)) {
  actions = ScansActions;

  reducers = ScansReducers;

  selectors = ScansSelectors;

  selectable = {
    scans: {
      actionKey: 'SCAN',
      itemKey: 'scanid'
    }
  };

  nss = {
    list: new Namespace({
      action: 'SCAN_LIST',
      transform: data => keyBy(data.rows, s => s.scanid),
      order: data => map(data.rows, 'scanid'),
      promise: (payload, state) => {
        if (state.ns_scanlist.default.fetching) return undefined;
        return RestService.get('/scans', payload);
      },
      single: payload => RestService.get(`/scans/${payload}`)
    }),
    spectra: new Namespace({
      action: 'SCAN_SPECTRA',
      transform: data => data,
      promise: (payload, state) => {
        if (state.ns_scanspectra.default.fetching) return undefined;
        return RestService.get(`/scans/spectra/${payload.scanid}`, payload);
      }
    }),

    data: new Namespace({
      action: 'SCAN_DATA',
      transform: data => keyBy([data], d => d.scanid),
      merge: true,
      promise: (payload, state) => {
        // if (state.ns_scandata.default.fetching) return undefined;
        return RestService.get(`/scans/data/${payload.scanid}`, payload);
      }
    })
  };
}

export default new Scans('scans');
