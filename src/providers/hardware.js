import { keyBy } from 'lodash';
import {
  Provider,
  Reducers,
  Selectors,
  Actions
} from '@esrf-ui/redux-provider';
import { namespaceMixin, Namespace } from 'providers/namespace';
import RestService from 'services/RestService';

import app from 'providers/app';

class HardwareSelectors extends Selectors {}
class HardwareReducers extends Reducers {}

class HardwareActions extends Actions {
  actions = [
    'UPDATE_HARDWARE',
    'UPDATE_HARDWARE_ONLINE',
    'UPDATE_HARDWARE_LOCKED',
    'UPDATE_HARDWARE_GROUP'
  ];

  asyncActions = {
    REQUEST_HARDWARE_CHANGE: {
      promise: payload => {
        if (payload.function) {
          return RestService.post(`/hardware/${payload.id}`, {
            function: payload.function,
            value: payload.value
          });
        }
        return RestService.put(`/hardware/${payload.id}`, {
          property: payload.property,
          value: payload.value
        });
      }
    }
  };

  dispatchRequestHardwareChangeRejectedAfter(error) {
    app.dispatch('ADD_TOAST', {
      title: 'Error Requesting Change',
      text: error.response.data.error,
      type: 'error'
    });
  }

  dispatchUpdateHardwareOnline(payload) {
    this.dispatchSimple('UPDATE_HARDWARE_ONLINE');
    return (dispatch, state) => {
      const hw = this.getNamespace('hardware').getInstance('default');
      hw.updateLocal({
        id: payload.id,
        online: payload.state
      });
    };
  }

  dispatchUpdateHardwareLocked(payload) {
    this.dispatchSimple('UPDATE_HARDWARE_LOCKED');
    return (dispatch, state) => {
      const hw = this.getNamespace('hardware').getInstance('default');
      hw.updateLocal({
        id: payload.id,
        locked: payload.state
      });
    };
  }

  dispatchUpdateHardwareGroup(payload) {
    this.dispatchSimple('UPDATE_HARDWARE_GROUP');
    return (dispatch, state) => {
      const gr = this.getNamespace('groups').getInstance('default');
      gr.updateLocal({
        groupid: payload.groupid,
        ...payload.data
      });
    };
  }

  dispatchUpdateHardware(payload) {
    this.dispatchSimple('UPDATE_HARDWARE');
    return (dispatch, state) => {
      const hw = this.getNamespace('hardware').getInstance('default');
      hw.updateLocal({
        id: payload.id,
        properties: {
          ...payload.data
        },
        __mergekey__: 'properties',
        __mergedeep__: false
      });
    };
  }
}

class Hardware extends namespaceMixin(Provider) {
  actions = HardwareActions;

  reducers = HardwareReducers;

  selectors = HardwareSelectors;

  nss = {
    hardware: new Namespace({
      action: 'HARDWARE',
      keyid: 'id',
      transform: data => keyBy(data.rows, d => d.id),
      promise: (payload, state) => {
        if (
          payload.first &&
          (state.ns_hardware.default.fetched ||
            state.ns_hardware.default.fetching)
        ) {
          return undefined;
        }
        return RestService.get('/hardware');
      }
    }),

    groups: new Namespace({
      action: 'HARDWARE_GROUP',
      keyid: 'groupid',
      transform: data => keyBy(data.rows, d => d.groupid),
      promise: (payload, state) => {
        if (
          payload.first &&
          (state.ns_hardwaregroup.default.fetched ||
            state.ns_hardwaregroup.default.fetching)
        ) {
          return undefined;
        }
        return RestService.get('/hardware/groups');
      }
    })
  };
}

export default new Hardware('hardware');
