/*
    A mixin to provide a common way to make selections in redux

    class Metadata extends selectionMixin(Provider) {
        selectable = {
          datacollections: {
            actionKey: 'DATA_COLLECTION',
            itemKey: 'datacollectionid'
          },
        };

        ...
    }
 */
import { each, upperFirst, camelCase } from 'lodash';

const selectionMixin = superclass =>
  class SelectionProvider extends superclass {
    start() {
      super.start();

      each(this.selectable, ({ actionKey, itemKey }, storeKey) => {
        const actionKeyCamel = upperFirst(camelCase(actionKey));

        this.reducers[`onAddSelectable${actionKeyCamel}`] = function reduce(
          state
        ) {
          return this.setIn(state, [storeKey, 'selection'], null);
        };
        this.reducers[`onAddSelectable${actionKeyCamel}`].bind(this.reducer);

        this.reducers[`onSelect${actionKeyCamel}`] = function reduce(
          state,
          action
        ) {
          return this.setIn(
            state,
            [storeKey, 'selection'],
            state[storeKey].selection
              ? state[storeKey].selection.concat([action.payload[itemKey]])
              : [action.payload[itemKey]]
          );
        };
        this.reducers[`onSelect${actionKeyCamel}`].bind(this.reducer);

        this.reducers[`onUnselect${actionKeyCamel}`] = function reduce(
          state,
          action
        ) {
          return this.setIn(
            state,
            [storeKey, 'selection'],
            state[storeKey].selection.filter(o => o !== action.payload)
          );
        };
        this.reducers[`onUnselect${actionKeyCamel}`].bind(this.reducer);

        this.reducers[`onReset${actionKeyCamel}Selection`] = function reduce(
          state
        ) {
          return this.setIn(state, [storeKey, 'selection'], []);
        };
        this.reducers[`onReset${actionKeyCamel}Selection`].bind(this.reducer);

        each(
          [
            `ADD_SELECTABLE_${actionKey}`,
            `SELECT_${actionKey}`,
            `UNSELECT_${actionKey}`,
            `RESET_${actionKey}_SELECTION`
          ],
          action => {
            this.actions.createAction(action);
          }
        );

        this.selectors[`selectSelected${actionKeyCamel}s`] = function select(
          state
        ) {
          const { selection } = state[storeKey];
          if (selection) {
            if (selection.length === 0) return null;
            return selection;
          }
          return undefined;
        };
        // Assuming the key is *not* pluralised (!)
        this.selectors[`selectSelected${actionKeyCamel}`] = function select(
          state
        ) {
          const { selection } = state[storeKey];
          if (selection) {
            const retval = selection.slice(-1)[0] || null;

            return retval;
          }

          return undefined;
        };

        // Legacy current{key} selector
        this.selectors[`selectCurrent${actionKeyCamel}`] = function (state) {
          return this[`selectSelected${actionKeyCamel}`](state);
        };
      });
    }

    setStore(store) {
      super.setStore(store);
      each(this.selectable, ({ actionKey }) => {
        this.dispatch(`ADD_SELECTABLE_${actionKey}`);
      });
    }
  };

export default selectionMixin;
