import { keyBy } from 'lodash';

import {
  Provider,
  Reducers,
  Selectors,
  Actions
} from '@esrf-ui/redux-provider';
import { namespaceMixin, Namespace } from 'providers/namespace';
import RestService from 'services/RestService';

class HDF5Selectors extends Selectors {}

class HDF5Reducers extends Reducers {}

class HDF5Actions extends Actions {}

class HDF5 extends namespaceMixin(Provider) {
  actions = HDF5Actions;

  reducers = HDF5Reducers;

  selectors = HDF5Selectors;

  nss = {
    files: new Namespace({
      action: 'HDF5_CONTENTS',
      transform: data => keyBy([data], d => d.file),
      merge: true,
      promise: payload => {
        return RestService.get(`/hdf5`, payload);
      }
    }),

    groups: new Namespace({
      action: 'HDF5_GROUPS',
      transform: data => keyBy([data], d => d.uri),
      merge: true,
      promise: payload => {
        return RestService.get(`/hdf5/groups${payload.uri}`, payload);
      }
    })
  };
}

export default new HDF5('hdf5');
