import { keyBy } from 'lodash';
import {
  Provider,
  Reducers,
  Selectors,
  Actions
} from '@esrf-ui/redux-provider';

import { namespaceMixin, Namespace } from 'providers/namespace';
import RestService from 'services/RestService';

class EditorSelectors extends Selectors {
  selectors = {
    selectedFile: 'selectedFile',
    leafState: 'leafState'
  };
}

class EditorReducers extends Reducers {
  initialState = {
    selectedFile: null,
    leafState: {}
  };

  onSelectFile(state, action) {
    return this.merge(state, { selectedFile: action.payload });
  }

  onToggleLeaf(state, action) {
    return this.setIn(
      state,
      ['leafState', action.payload],
      !state.leafState[action.payload]
    );
  }
}

class EditorActions extends Actions {
  actions = ['SELECT_FILE', 'TOGGLE_LEAF'];
}

class Editor extends namespaceMixin(Provider) {
  actions = EditorActions;

  reducers = EditorReducers;

  selectors = EditorSelectors;

  nss = {
    files: new Namespace({
      action: 'FILES',
      keyid: 'path',
      transform: data => data,
      single: payload => RestService.get(`/editor/file/${payload}`),
      update: payload =>
        RestService.patch(`/editor/file/${payload.path}`, payload),
      promise: () => {}
    }),

    directories: new Namespace({
      action: 'DIRECTORIES',
      keyid: 'name',
      transform: data => keyBy(data.rows, d => d.name),
      promise: payload => RestService.get('/editor/directory', payload)
    })
  };
}

export default new Editor('editor');
