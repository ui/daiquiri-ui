import { each } from 'lodash';

import {
  Provider,
  Reducers,
  Selectors,
  Actions
} from '@esrf-ui/redux-provider';
import RestService from 'services/RestService';
import SocketIOService from 'services/SocketIOService';
import XHRImage from 'helpers/XHRImage';
import { AuthenticatedEndpoint } from 'resources/endpoints/Authenticated';

import app from 'providers/app';
import metadata from 'providers/metadata';

class SessionSelectors extends Selectors {
  selectors = {
    error: 'error',
    current: 'current',
    operator: 'current.operator',
    sessionid: 'current.sessionid',
    mirrorid: 'current.data.mirror',
    sessions: 'sessions',
    auth: 'auth',
    checkToken: 'checkToken',
    controlRequest: 'controlRequest',
    currentBLSession: 'current.data.blsession'
  };
}

class SessionReducers extends Reducers {
  initialState = {
    sessions: [],
    current: {},
    auth: false,
    error: null,
    checkToken: false,
    controlRequest: null
  };

  onCheckToken(state) {
    return this.merge(state, { checkToken: false });
  }

  onCheckedToken(state) {
    return this.merge(state, { checkToken: true });
  }

  onFetchSessionsResolved(state, action) {
    return this.setIn(state, ['sessions'], action.payload.data.rows);
  }

  onLogin(state) {
    return this.merge(state, {
      error: null
    });
  }

  onLoginRejected(state, action) {
    return this.merge(state, {
      auth: false,
      error: action.payload
    });
  }

  onLoginResolved(state, action) {
    return this.merge(state, {
      current: action.payload.data
    });
  }

  onLogoutResolved(state) {
    return this.merge(state, {
      auth: false,
      current: {}
    });
  }

  onAuthReady(state) {
    return this.merge(state, {
      auth: true
    });
  }

  onUpdateOperator(state, action) {
    return this.setIn(state, ['current', 'operator'], action.payload);
  }

  onControlRequest(state, action) {
    return this.merge(state, { controlRequest: action.payload });
  }

  onRespondControlRequestResolved(state) {
    return this.merge(state, { controlRequest: null });
  }

  onSelectBlsessionResolved(state, action) {
    return this.setIn(
      state,
      ['current', 'data', 'blsession'],
      action.payload.data.session
    );
  }
}

class SessionActions extends Actions {
  actions = [
    'RESTORE_SESSION',
    'CHECK_TOKEN',
    'CHECKED_TOKEN',
    'INVALID_SESSION',
    'UPDATE_OPERATOR',
    'AUTH_READY',
    'CONTROL_REQUEST',
    'DENY_REQUEST'
  ];

  asyncActions = {
    FETCH_CURRENT_SESSION: {
      promise: () => RestService.get('/session/current')
    },

    FETCH_SESSIONS: {
      promise: (payload, state) => {
        if (!state.auth) return undefined;
        return RestService.get('/session');
      }
    },
    SELECT_BLSESSION: {
      promise: payload =>
        RestService.post('/metadata/sessions/select', { session: payload })
    },

    LOGIN: {
      promise: payload => RestService.post('/authenticator/login', payload)
    },

    LOGOUT: {
      promise: () => RestService.delete('/authenticator/login')
    },

    REQUEST_CONTROL: {
      promise: () => RestService.post('/session/current/control')
    },
    YIELD_CONTROL: {
      promise: () => RestService.delete('/session/current/control')
    },

    RESPOND_CONTROL_REQUEST: {
      promise: payload =>
        RestService.post('/session/current/control/respond', { grant: payload })
    },
    REQUEST_MIRROR: {
      promise: payload => RestService.post('/session/mirror', payload)
    }
  };

  setToken(token) {
    RestService.setToken(token);
    SocketIOService.setToken(token);
    XHRImage.setToken(token);
    AuthenticatedEndpoint.accessToken = token;
  }

  fetchUser() {
    const ps = [];
    ps.push(metadata.getNamespace('user').fetch(null, true));
    ps.push(metadata.getNamespace('cache').fetch(null, true));
    ps.push(metadata.getNamespace('blsessions').fetch(null, true));
    ps.push(app.getNamespace('layouts').fetch(null, true));
    ps.push(app.getNamespace('schema').fetch(null, true));

    return Promise.all(ps)
      .catch(err => {
        this.removeToken();
        throw err;
      })
      .then(() => {
        this.dispatch('AUTH_READY');
      });
  }

  // TODO: Bug in here with then firing when rejected
  dispatchRestoreSession() {
    this.dispatchSimple('RESTORE_SESSION');
    return () => {
      this.dispatch('CHECK_TOKEN');
      const token = sessionStorage.getItem('token');
      if (token) {
        this.setToken(token);
        this.dispatch('FETCH_CURRENT_SESSION');
      } else {
        this.dispatch('CHECKED_TOKEN');
      }
    };
  }

  dispatchFetchCurrentSessionResolvedAfter(payload) {
    // console.log('dispatchRestoreSession resolved', payload);
    this.dispatch('LOGIN_RESOLVED', payload);
    if (payload.data.mirrored) {
      this.store.enablePersist();
    }
    this.fetchUser().then(() => {
      this.dispatch('CHECKED_TOKEN');
    });
  }

  dispatchFetchCurrentSessionRejectedAfter(payload) {
    console.log('token is invalid', payload);
    this.dispatch('LOGOUT_RESOLVED');
    this.removeToken();
    this.dispatch('CHECKED_TOKEN');
  }

  dispatchLoginResolvedAfter(response) {
    if (response.data.token) {
      sessionStorage.setItem('token', response.data.token);
      this.setToken(response.data.token);
      app.dispatch('MARK_ALL_MESSAGES_READ');
      this.fetchUser();
    }
  }

  dispatchLogoutResolvedAfter() {
    this.removeToken();
    SocketIOService.disconnect();
    this.store.dispatch({ type: 'RESET' });
  }

  dispatchInvalidSession(message) {
    return (dispatch, getState) => {
      const slice = this.slice(getState());
      if (!slice.auth) return;

      if (message && message.indexOf('Expired') > -1) {
        this.dispatch('LOGIN_REJECTED', message);
      }

      this.dispatch('LOGOUT_RESOLVED');
      this.removeToken();
      SocketIOService.disconnect();
      this.store.dispatch({ type: 'RESET' });
    };
  }

  dispatchFetchSessionsResolvedAfter() {
    this.dispatch('UPDATE_OPERATOR');
  }

  dispatchUpdateOperator() {
    return (dispatch, getState) => {
      const state = getState();
      const sid = this.selector('sessionid', state);

      let isOperator = false;
      each(this.selector('sessions', state), s => {
        if (s.sessionid === sid) {
          if (s.operator) isOperator = true;
        }
      });

      this.dispatchSimple('UPDATE_OPERATOR', isOperator);
    };
  }

  dispatchRequestControlResolvedAfter() {
    app.dispatch('ADD_TOAST', {
      title: 'Control Granted',
      text: 'You were granted control',
      type: 'success'
    });
  }

  dispatchRequestControlRejectedAfter() {
    app.dispatch('ADD_TOAST', {
      title: 'Could not get control',
      text: 'Could not get control at this time, you request has been queued',
      type: 'error'
    });
  }

  dispatchYieldControlResolvedAfter() {
    app.dispatch('ADD_TOAST', {
      title: 'Control Yielded',
      text: 'You successfully yielded control',
      type: 'success'
    });
  }

  removeToken() {
    app.dispatch('SWITCH_LAYOUT', 0);
    sessionStorage.removeItem('token');
  }

  dispatchControlRequest(sessionid) {
    return (dispatch, getState) => {
      const operator = this.selector('operator', getState());
      if (operator) {
        this.dispatchSimple('CONTROL_REQUEST', sessionid);
      }
    };
  }

  dispatchDenyRequest(sessionid) {
    console.log('deny request', sessionid);
    return (dispatch, getState) => {
      if (sessionid === this.selector('sessionid', getState())) {
        app.dispatch('ADD_TOAST', {
          title: 'Control Request Denied',
          text: 'Your control request was denied',
          type: 'error'
        });
      }
    };
  }
}

class Session extends Provider {
  actions = SessionActions;

  reducers = SessionReducers;

  selectors = SessionSelectors;
}

export default new Session('session');
