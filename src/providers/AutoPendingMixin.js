import { each, upperFirst, camelCase } from 'lodash';

const autoPendingMixin = superclass =>
  class AutoPendingProvider extends superclass {
    start() {
      super.start();

      const { reducers } = this;
      this.reducers.initialState.pending = {};

      each(this.actions._actions, (act, key) => {
        if (!act.async) return;
        const camelKey = upperFirst(camelCase(key));

        const old = this.reducers[`on${camelKey}`];
        this.reducers[`on${camelKey}`] = function (state, action) {
          const newState = old ? old.call(reducers, state, action) : state;
          return this.setIn(newState, ['pending', camelKey], true);
        };

        const oldResolved = this.reducers[`on${camelKey}Resolved`];
        this.reducers[`on${camelKey}Resolved`] = function (state, action) {
          const newState = old
            ? oldResolved.call(reducers, state, action)
            : state;
          return this.setIn(newState, ['pending', camelKey], false);
        };

        const oldRejected = this.reducers[`on${camelKey}Rejected`];
        this.reducers[`on${camelKey}Rejected`] = function (state, action) {
          const newState = old
            ? oldRejected.call(reducers, state, action)
            : state;
          return this.setIn(newState, ['pending', camelKey], false);
        };

        each(['', 'Resolved', 'Rejected'], ty => {
          this.reducers[`on${camelKey}${ty}`].bind(this.reducer);
        });

        this.selectors.selectors[`pending${camelKey}`] = `pending.${camelKey}`;
      });
    }
  };

export default autoPendingMixin;
