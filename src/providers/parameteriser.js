import { keyBy, map } from 'lodash';
import {
  Provider,
  Reducers,
  Selectors,
  Actions
} from '@esrf-ui/redux-provider';
import RestService from 'services/RestService';

import { namespaceMixin, Namespace } from 'providers/namespace';

class ParameteriserSelectors extends Selectors {
  selectors = {};
}

class ParameteriserReducers extends Reducers {
  initialState = {};
}

class ParameteriserActions extends Actions {}

class Parameteriser extends namespaceMixin(Provider) {
  actions = ParameteriserActions;

  reducers = ParameteriserReducers;

  selectors = ParameteriserSelectors;

  nss = {
    parameters: new Namespace({
      action: 'PARAMETERS',
      keyid: 'name',
      order: data => map(data.rows, 'name'),
      transform: data => keyBy(data.rows, o => o.name),
      promise: payload => RestService.get('/parameteriser', payload)
    })
  };
}

export default new Parameteriser('parameteriser');
