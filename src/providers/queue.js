import {
  Provider,
  Reducers,
  Selectors,
  Actions
} from '@esrf-ui/redux-provider';

import { namespaceMixin, Namespace } from 'providers/namespace';
import RestService from 'services/RestService';

class QueueSelectors extends Selectors {
  selectors = {
    selectedItem: 'selectedItem',
    actorLog: 'actorLog'
  };

  selectRunning(state) {
    const queue = this.getNamespace('queue').getInstance('default');
    const results = queue.localSelector('results', state);
    return (results && results.running) || false;
  }

  selectReady(state) {
    const queue = this.getNamespace('queue').getInstance('default');
    const results = queue.localSelector('results', state);
    return (results && results.ready) || false;
  }
}

class QueueReducers extends Reducers {
  initialState = {
    selectedItem: null,
    actorLog: {}
  };

  onSelectItem(state, action) {
    return this.merge(state, { selectedItem: action.payload });
  }

  onUpdateActorLog(state, action) {
    return this.setIn(
      state,
      ['actorLog', action.payload.uuid],
      action.payload.output
    );
  }
}

class QueueActions extends Actions {
  actions = ['SELECT_ITEM', 'UPDATE_ACTOR_LOG'];

  asyncActions = {
    SET_QUEUE_STATUS: {
      promise: payload =>
        RestService.put('/queue', {
          state: payload
        })
    },

    UPDATE_QUEUE: {
      promise: payload => RestService.patch('/queue', payload)
    },

    MOVE_ITEM: {
      promise: payload => RestService.post('/queue/move', payload)
    },
    REMOVE_ITEM: {
      promise: payload => RestService.delete(`/queue/${payload}`)
    },
    ADD_PAUSE: {
      promise: () => RestService.post('/queue/pause')
    },
    ADD_SLEEP: {
      promise: payload => RestService.post('/queue/sleep', payload)
    },
    CLEAR_QUEUE: {
      promise: () => RestService.delete('/queue')
    },
    KILL_QUEUE: {
      promise: () => RestService.post('/queue/kill')
    }
  };
}

class Queue extends namespaceMixin(Provider) {
  actions = QueueActions;

  reducers = QueueReducers;

  selectors = QueueSelectors;

  nss = {
    queue: new Namespace({
      action: 'QUEUE',
      transform: data => data,
      promise: (payload, state) => RestService.get('/queue', payload)
    })
  };
}

export default new Queue('queue');
