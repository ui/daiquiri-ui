import { keyBy } from 'lodash';

import {
  Provider,
  Reducers,
  Selectors,
  Actions
} from '@esrf-ui/redux-provider';
import { namespaceMixin, Namespace } from 'providers/namespace';
import RestService from 'services/RestService';

class H5ViewerSelectors extends Selectors {}

class H5ViewerReducers extends Reducers {}

class H5ViewerActions extends Actions {}

class H5Viewer extends namespaceMixin(Provider) {
  actions = H5ViewerActions;

  reducers = H5ViewerReducers;

  selectors = H5ViewerSelectors;

  nss = {
    h5grove: new Namespace({
      action: 'H5GROVE_API',
      transform: data => keyBy([data], d => d.file), // TODO
      merge: true,
      promise: payload => {
        return RestService.get(`/h5grove`, payload);
      }
    })
  };
}

export default new H5Viewer('h5viewer');
