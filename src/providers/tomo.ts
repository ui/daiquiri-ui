import { cloneDeep, keyBy } from 'lodash';

import {
  Provider,
  Reducers,
  Selectors,
  Actions,
} from '@esrf-ui/redux-provider';
import { namespaceMixin, Namespace } from 'providers/namespace';
import RestService from 'services/RestService';
import type {
  TomoDataCollectionMeta,
  TomoDataCollectionMetaActions,
} from 'types/Tomo';
import type {
  TomoDetector,
  TomoScanInfo,
  TomoScanTask,
  TomoScanTaskActions,
} from 'types/Tomo';
import type Qty from 'js-quantities';

class TomoSelectors extends Selectors<TomoDetector> {
  public selectors = {
    scanTasks: 'scanTasks',
    datacollectionsMeta: 'datacollectionsMeta',
  };

  /** Returns data ref stored from the requested detector */
  public detectorData(state: any, detectorId: string) {
    const detector = this.detectorState(state, detectorId);
    if (detector === null) {
      return {};
    }
    // FIXME: Sounds like this function is not used and broken
    // @ts-expect-error
    return detector.filter((item) => item.id !== 'running');
  }

  /** Returns running scan from the requested detector */
  public detectorRunningScan(state: any, detectorId: string) {
    const detector = this.detectorState(state, detectorId);
    if (detector === null) {
      return {};
    }
    const { running } = detector;
    if (running === undefined) {
      return null;
    }
    return running;
  }

  /** Returns the whole state stored from a detector */
  private detectorState(state: any, detectorId: string): TomoDetector | null {
    const sns = this.getNamespace('detectors').getInstance('default');
    const detectors = sns.localSelector('results', state);
    if (detectors === undefined) {
      return null;
    }
    const detector = detectors[detectorId];
    if (detector === undefined) {
      return null;
    }
    return detector;
  }
}

interface ReducerState {
  scanTasks: any[];
  lastScanTaskId: undefined | number;
  datacollectionsMeta: Record<number, TomoDataCollectionMeta>;
}

type FirstParameter<T> = T extends (
  first: infer FirstArgument,
  ...args: any[]
) => any
  ? FirstArgument
  : never;

class TomoReducers extends Reducers {
  public initialState: ReducerState = {
    scanTasks: [],
    lastScanTaskId: undefined,
    datacollectionsMeta: {},
  };

  public onRemoveScanTask(
    state: ReducerState,
    action: { payload: { id: string } }
  ) {
    const tasks: TomoScanTask[] = state.scanTasks;
    const newTasks = tasks.filter(
      (t: TomoScanTask) => t.id !== action.payload.id
    );
    return this.merge(state, { scanTasks: newTasks });
  }

  public onActivateScanTask(
    state: ReducerState,
    action: { payload: { id: string } }
  ) {
    const tasks: TomoScanTask[] = state.scanTasks;
    const newTasks = tasks.map((t: TomoScanTask) => {
      return { ...t, active: t.id === action.payload.id };
    });
    return this.merge(state, { scanTasks: newTasks });
  }

  public onCreateScanTask(
    state: ReducerState,
    action: {
      payload: FirstParameter<TomoScanTaskActions['createScanTask']>;
    }
  ) {
    const payload = action.payload;
    const tasks: TomoScanTask[] = state.scanTasks;
    const newTasks = payload.active
      ? tasks.map((t: TomoScanTask) => {
          return { ...t, active: false };
        })
      : tasks;

    const id = (state.lastScanTaskId || 0) + 1;
    const newTask: TomoScanTask = {
      id: id.toString(),
      active: payload.active || false,
      estimationTime: null,
      roi: {
        centerZ: payload.z.to('mm').scalar,
        center: {
          x: payload.x.to('mm').scalar,
          y: payload.y.to('mm').scalar,
        },
        axis: {
          x: payload.x.to('mm').scalar,
          y: payload.y.to('mm').scalar,
        },
        width: payload.fov[0].to('mm').scalar,
        height: payload.fov[1].to('mm').scalar,
      },
      acquisition: {
        detector: payload.detector,
        magnification: payload.magnification,
        fov: [payload.fov[0].to('mm').scalar, payload.fov[1].to('mm').scalar],
        pixelSize: [
          payload.pixelSize[0].to('um').scalar,
          payload.pixelSize[1].to('um').scalar,
        ],
      },
      kind: 'default',
      state: 'defined',
      scanParameters: {},
    };
    return this.merge(state, {
      scanTasks: newTasks.concat(newTask),
      lastScanTaskId: id,
    });
  }

  public onMoveScanTaskRoi(
    state: ReducerState,
    action: {
      payload: {
        id: string;
        x: Qty;
        y: Qty;
        z: Qty;
      };
    }
  ) {
    const payload = action.payload;
    const tasks: TomoScanTask[] = state.scanTasks;
    const taskIndex = tasks.findIndex(
      (t: TomoScanTask) => t.id === action.payload.id
    );
    if (taskIndex === -1) {
      console.error(`ScanTask {action.payload.id} is missing. Move ignored`);
      return state;
    }
    const task = cloneDeep(tasks[taskIndex]);

    task.roi.centerZ = payload.z.to('mm').scalar;
    task.roi.axis.x = payload.x.to('mm').scalar;
    task.roi.axis.y = payload.y.to('mm').scalar;
    task.roi.center.x = payload.x.to('mm').scalar;
    task.roi.center.y = payload.y.to('mm').scalar;

    const newTasks = [...tasks];
    newTasks[taskIndex] = task;
    return this.merge(state, { scanTasks: newTasks });
  }

  public onReorderScanTask(
    state: ReducerState,
    action: { payload: { from: number; to: number } }
  ) {
    const { from, to } = action.payload;
    if (from === to) {
      return state;
    }

    const tasks: TomoScanTask[] = state.scanTasks;
    if (!(from >= 0 && to >= 0 && from <= tasks.length && to <= tasks.length)) {
      console.error(`Move scan tasks called with wrong id ${from} / ${to}`);
      return state;
    }

    function getNewList() {
      const current = tasks.slice(from, from + 1);
      if (from < to) {
        return [
          ...tasks.slice(0, from),
          ...tasks.slice(from + 1, to + 1),
          ...current,
          ...tasks.slice(to + 1, tasks.length),
        ];
      }
      return [
        ...tasks.slice(0, to),
        ...current,
        ...tasks.slice(to, from),
        ...tasks.slice(from + 1, tasks.length),
      ];
    }
    const newTasks = getNewList();
    return this.merge(state, {
      scanTasks: newTasks,
    });
  }

  public onUpdateDataCollectionMeta(
    state: ReducerState,
    action: {
      payload: {
        dataCollectionId: number;
        params: {
          theoricalCor?: number | null;
          // the center of rotation as guessed by nabu
          estimatedCor?: number | null;
          // the center of rotation as requested by the user (during the request)
          requestedCor?: number | null;
          // the actually used center of rotation
          actualCor?: number | null;
        };
      };
    }
  ) {
    const { dataCollectionId, params } = action.payload;
    const datacollectionsMeta: TomoDataCollectionMeta = { ...state }
      .datacollectionsMeta[dataCollectionId] ?? {
      actualCor: null,
      theoricalCor: null,
      estimatedCor: null,
      requestedCor: null,
    };
    let newDatacollectionsMeta = { ...datacollectionsMeta };
    newDatacollectionsMeta = {
      ...datacollectionsMeta,
      ...(params.actualCor !== undefined
        ? { actualCor: params.actualCor }
        : null),
      ...(params.theoricalCor !== undefined
        ? { theoricalCor: params.theoricalCor }
        : null),
      ...(params.estimatedCor !== undefined
        ? { estimatedCor: params.estimatedCor }
        : null),
      ...(params.requestedCor !== undefined
        ? { requestedCor: params.requestedCor }
        : null),
    };
    // FIXME: There is a memory leak here, we have to remove old stuffs at some point
    return this.setIn(
      state,
      ['datacollectionsMeta', dataCollectionId],
      newDatacollectionsMeta
    );
  }
}

class TomoActions extends Actions {
  public actions = [
    'DETECTOR_SCAN_START',
    'DETECTOR_SCAN_STOP',
    'DETECTOR_DATA_UPDATE',
    'DETECTOR_FLAT_UPDATE',
    'DETECTOR_DARK_UPDATE',
    'UPDATE_SCAN_INFO',
    'UPDATE_SCAN_INFO_SINOGRAM',
    'UPDATE_SCAN_INFO_SUBSCAN',
    'REMOVE_SCAN_TASK',
    'CREATE_SCAN_TASK',
    'REORDER_SCAN_TASK',
    'MOVE_SCAN_TASK_ROI',
    'ACTIVATE_SCAN_TASK',
    'UPDATE_DATA_COLLECTION_META',
  ];

  public asyncActions = {};

  public dispatchDetectorScanStart(payload: {
    detector_id: string;
    scanid: number;
  }) {
    this.dispatchSimple('DETECTOR_SCAN_START');
    return (dispatch: any, state: any) => {
      const detectors =
        this.getNamespace<TomoDetector>('detectors').getInstance('default');
      detectors.updateLocal({
        detector_id: payload.detector_id,
        running: {
          progress: 0,
          scanid: payload.scanid,
        },
      });
    };
  }

  public dispatchDetectorScanStop(payload: { detector_id: string }) {
    this.dispatchSimple('DETECTOR_SCAN_STOP');
    return (dispatch: any, state: any) => {
      const detectors =
        this.getNamespace<TomoDetector>('detectors').getInstance('default');
      detectors.updateLocal({
        detector_id: payload.detector_id,
        running: null,
      });
    };
  }

  public dispatchDetectorDataUpdate(payload: {
    detector_id: string;
    frame_no: number;
    scanid: number;
    progress: number;
    has_dark?: boolean;
    has_flat?: boolean;
  }) {
    this.dispatchSimple('DETECTOR_DATA_UPDATE');
    return (dispatch: any, state: any) => {
      const detectors =
        this.getNamespace<TomoDetector>('detectors').getInstance('default');
      detectors.updateLocal({
        detector_id: payload.detector_id,
        data: { scanid: payload.scanid, frame_no: payload.frame_no },
        running: { scanid: payload.scanid, progress: payload.progress },
        ...(payload.has_dark ? {} : { dark: undefined }),
        ...(payload.has_flat ? {} : { flat: undefined }),
      });
    };
  }

  public dispatchDetectorFlatUpdate(payload: {
    detector_id: string;
    has_dark?: boolean;
    has_proj?: boolean;
  }) {
    this.dispatchSimple('DETECTOR_FLAT_UPDATE');
    return (dispatch: any, state: any) => {
      // FIXME: Would be better to replace that by a timestamp or a ref to the scanid/frame_no
      const detectorState =
        state().tomo.ns_detectors.default.results[payload.detector_id];
      const flat = detectorState.flat ?? -1;
      const detectors =
        this.getNamespace<TomoDetector>('detectors').getInstance('default');
      detectors.updateLocal({
        detector_id: payload.detector_id,
        flat: flat + 1,
        ...(payload.has_dark ? {} : { dark: undefined }),
        ...(payload.has_proj ? {} : { data: undefined }),
      });
    };
  }

  public dispatchDetectorDarkUpdate(payload: {
    detector_id: string;
    has_flat?: boolean;
    has_proj?: boolean;
  }) {
    this.dispatchSimple('DETECTOR_DARK_UPDATE');
    // FIXME: Would be better to replace that by a timestamp or a ref to the scanid/frame_no
    return (dispatch: any, state: any) => {
      const detectorState =
        state().tomo.ns_detectors.default.results[payload.detector_id];
      const dark = detectorState.dark ?? -1;
      const detectors =
        this.getNamespace<TomoDetector>('detectors').getInstance('default');
      detectors.updateLocal({
        detector_id: payload.detector_id,
        dark: dark + 1,
        ...(payload.has_flat ? {} : { flat: undefined }),
        ...(payload.has_proj ? {} : { data: undefined }),
      });
    };
  }

  public dispatchUpdateScanInfo(payload: { id: number; data: any }) {
    this.dispatchSimple('UPDATE_SCAN_INFO');
    return (dispatch: any, state: any) => {
      const scaninfo =
        this.getNamespace<TomoScanInfo>('scaninfo').getInstance('default');
      scaninfo.updateLocal({
        keyid: payload.id,
        ...payload.data,
      });
    };
  }

  public dispatchUpdateScanInfoSinogram(payload: {
    id: number;
    actualnbpoints: number;
  }) {
    this.dispatchSimple('UPDATE_SCAN_INFO_SINOGRAM');
    return (dispatch: any, state: any) => {
      const scaninfo =
        this.getNamespace<TomoScanInfo>('scaninfo').getInstance('default');
      scaninfo.updateLocal({
        keyid: payload.id,
        sinogram: { actualnbpoints: payload.actualnbpoints },
      });
    };
  }

  public dispatchUpdateScanInfoSubscan(payload: {
    id: number;
    activesubscan: number;
  }) {
    this.dispatchSimple('UPDATE_SCAN_INFO_SUBSCAN');
    return (dispatch: any, state: any) => {
      const scaninfo =
        this.getNamespace<TomoScanInfo>('scaninfo').getInstance('default');
      scaninfo.updateLocal({
        keyid: payload.id,
        activesubscan: payload.activesubscan,
      });
    };
  }
}

class Tomo extends namespaceMixin(Provider) {
  public actions = TomoActions;

  public reducers = TomoReducers;

  public selectors = TomoSelectors;

  public nss = {
    detectors: new Namespace({
      action: 'DETECTORS',
      keyid: 'detector_id',
      transform: (data: { rows: TomoDetector[] }) =>
        keyBy(data.rows, (s) => s.detector_id),
      promise: (payload: any, state: any) => {
        if (state.ns_detectors.default.fetching) {
          // Do not execute while already fetching for the result
          return undefined;
        }
        if (state.ns_detectors.default.fetched) {
          // Was already executed. The following state is updated using events
          return undefined;
        }
        return RestService.get('/tomo/detectors', payload);
      },
    }),
    scaninfo: new Namespace({
      action: 'SCANINFO',
      transform: (data: any) => data,
      keyid: 'keyid',
      promise: (payload: any, state: any) => {
        if (state.ns_scaninfo.default.fetching) {
          // Do not execute while already fetching for the result
          return undefined;
        }
        if (state.ns_scaninfo.default.fetched) {
          // Was already executed. The following state is updated using events
          return undefined;
        }
        return RestService.get('/tomo/scaninfo', payload);
      },
    }),
  };

  public scanTaskActions(): TomoScanTaskActions {
    return {
      removeScanTask: (id: string) => this.dispatch('REMOVE_SCAN_TASK', { id }),
      createScanTask: (action) => {
        this.dispatch('CREATE_SCAN_TASK', action);
      },
      reorderScanTask: (from: number, to: number) => {
        this.dispatch('REORDER_SCAN_TASK', { from, to });
      },
      moveScanTaskRoi: (id: string, x: Qty, y: Qty, z: Qty) => {
        this.dispatch('MOVE_SCAN_TASK_ROI', { id, x, y, z });
      },
      activateScanTask: (id: string) =>
        this.dispatch('ACTIVATE_SCAN_TASK', { id }),
    };
  }

  public getScanTasks(state: any): TomoScanTask[] {
    const result: TomoScanTask[] = this.selector('scanTasks', state);
    return result;
  }

  public dataCollectionMetaActions(): TomoDataCollectionMetaActions {
    return {
      updateDataCollectionMeta: (
        dataCollectionId: number,
        params: {
          theoricalCor?: number | null;
          estimatedCor?: number | null;
          requestedCor?: number | null;
          actualCor?: number | null;
        }
      ) =>
        this.dispatch('UPDATE_DATA_COLLECTION_META', {
          dataCollectionId,
          params,
        }),
    };
  }
  public getDataCollectionMeta(
    state: any,
    dataCollectionId: number
  ): TomoDataCollectionMeta | undefined {
    const result: TomoDataCollectionMeta | undefined = this.selector(
      'datacollectionsMeta',
      state
    )[dataCollectionId];
    return result;
  }
}

// @ts-expect-error
export default new Tomo('tomo');
