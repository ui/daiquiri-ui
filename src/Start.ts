import RestService from 'services/RestService';
import SocketIOService from 'services/SocketIOService';
import { providerRegistry } from '@esrf-ui/redux-provider';

import config from 'config/config';
import messageHandler from 'messagehandler';
import session from 'providers/session';
import app from 'providers/app';
import type { PersistStore } from './store';
import editor from 'providers/editor';
import hdf5 from 'providers/hdf5';
import parameteriser from 'providers/parameteriser';

// TODO: Workaround for namespace initialisation
const _editor = editor;
const _hdf5 = hdf5;
const _parameteriser = parameteriser;

class Start {
  private _unsubscribe: (() => void) | null = null;
  private _layout: undefined | string | number = undefined;
  private _store: PersistStore | null = null;

  public constructor() {
    this._unsubscribe = null;
  }

  /**
   * Start the application
   *
   * Attributes:
   *    layout: index or acronym of the layout to display. If undefined
   *            the first layout from the description is used.
   */
  public start(store: PersistStore, layout: string | undefined) {
    this._layout = layout;
    const opts = {
      baseUrl: config.baseUrl,
      onUnauthorised: (message: string) =>
        session.dispatch('INVALID_SESSION', message),
      onNetworkError: () => {
        if (!app.selector('networkError', store.getState())) {
          app.dispatch('NETWORK_ERROR', true);
        }
      },
      onNetworkOk: () => {
        if (app.selector('networkError', store.getState())) {
          app.dispatch('NETWORK_ERROR', false);
        }
      },
    };
    RestService.setup(opts);
    SocketIOService.setup(opts);

    providerRegistry.start(store);
    messageHandler.start(store);

    this._store = store;
    this._unsubscribe = store.subscribe(this.checkReady.bind(this));

    app
      .getNamespace('config')
      .fetch(null, true)
      .catch((error: string | undefined) => console.warn(error));

    session.dispatch('RESTORE_SESSION');
  }

  public unsubscribe() {
    if (this._unsubscribe) {
      this._unsubscribe();
      this._unsubscribe = null;
    }
  }

  public checkReady() {
    if (!this._store) {
      return;
    }

    const state = this._store.getState();
    const cfg = app.getNamespace('config').getInstance('default');
    const ready =
      cfg.selector('fetched', state) && session.selector('checkToken', state);

    if (ready) {
      this.unsubscribe();
      if (typeof this._layout === 'string') {
        app.dispatch('SWITCH_LAYOUT_SLUG', this._layout);
      } else {
        app.dispatch('SWITCH_LAYOUT', this._layout || 0);
      }
      app.dispatch('APP_READY', true);
    }
  }
}

export default new Start();
