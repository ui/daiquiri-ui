const proxy = require('http-proxy-middleware');

const host = 'localhost:6789';

module.exports = app => {
  app.use(
    proxy('/api', {
      target: `http://${host}`,
      pathRewrite: { '^/api': '/' }
    })
  );

  app.use(
    proxy('/socket.io', {
      target: `ws://${host}`,
      ws: true
    })
  );
};
