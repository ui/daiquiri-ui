import { values, each } from 'lodash';

import { setupProviders, createStore } from 'helpers/tests';

import metadata from 'providers/metadata';
import Resources from 'helpers/tests/resources';

describe('Metadata', () => {
  const resources = new Resources(['metadata']);
  const metadataHandler = resources.getHandler('metadata');

  const store = createStore();
  setupProviders(store);

  const tests = {
    samples: {
      namespace: metadata.getNamespace('samples').getInstance('default'),
      resp: () =>
        metadataHandler.getResponse({
          url: '/samples',
          method: 'get'
        }),
      store: namespace => namespace.selector('results', store.getState()),
      assert: (storeValues, resp) => {
        expect(storeValues).toEqual(resp.rows);
      }
    },

    subsamples: {
      namespace: metadata.getNamespace('subsamples').getInstance('default'),
      resp: () =>
        metadataHandler.getResponse({
          url: '/samples/sub',
          method: 'get'
        }),
      store: namespace => namespace.selector('results', store.getState()),
      assert: (storeValues, resp) => {
        expect(storeValues).toEqual(resp.rows);
      }
    }
  };

  each(tests, (test, act) => {
    it(act, async () => {
      await test.namespace.fetch();
      test.assert(values(test.store(test.namespace)), test.resp());
    });
  });
});
