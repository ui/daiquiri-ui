import { each, keys } from 'lodash';
import { screen, userEvent, waitFor, within } from 'helpers/tests';
import { withControl, withSubsample } from 'helpers/tests/state';

import startApp from 'helpers/tests/startApp';
import scans from 'providers/scans';

const waitSubsamples = async view => {
  const subsamples = view.container.querySelector('.table-subsamples');
  const withinSubsamples = within(subsamples);

  await waitFor(() =>
    expect(withinSubsamples.getAllByRole('row').length).toEqual(2)
  );

  return withinSubsamples;
};

const waitSelectSubsample = async view => {
  withSubsample(1);
  const objSelector = within(view.getByTestId('objectview'));
  await waitFor(() =>
    expect(objSelector.getByText(/XRF map/i)).toBeInTheDocument()
  );

  return { objSelector };
};

describe.skip('TwoD views', () => {
  test('Load twod views, select subsample', async () => {
    const { view } = await startApp({ layout: 'layout_twod' });

    const { getAllByRole } = await waitSubsamples(view);

    const rows = getAllByRole('row');
    const cells = within(rows[1]).getAllByRole('cell');
    userEvent.click(cells[0]);

    await waitFor(() =>
      expect(screen.queryByText(/no data collections/)).not.toBeInTheDocument()
    );

    const objSelector = within(screen.getByTestId('objectview'));
    expect(objSelector.getByText(/XRF map/i)).toBeInTheDocument();
  });

  test('Create scalar map', async () => {
    const { view, store } = await startApp({ layout: 'layout_twod' });

    await waitSubsamples(view);
    const { objSelector } = await waitSelectSubsample(view);

    const tables = objSelector.getAllByRole('table');

    const dcSelector = within(tables[0]);
    const buttons = dcSelector.getAllByRole('button');

    userEvent.click(buttons[0]);

    await waitFor(() =>
      expect(
        screen.getByRole('button', { name: /Create Map/i })
      ).toBeInTheDocument()
    );

    const input = screen.getByTestId('scalar-selection');
    userEvent.click(input);

    // Check scan counters match
    const scalarOptions = screen.getByRole('listbox');

    const data = scans.getNamespace('data').getInstance('mfs');
    const scanData = data.selector('results', store.getState());

    const optsSelector = within(scalarOptions);
    each(optsSelector.getAllByRole('option'), opt => {
      expect(opt.text in scanData[keys(scanData)[0]].data).toBeTruthy();
    });

    const modalSelector = within(screen.getByRole('dialog'));
    const close = modalSelector.getAllByRole('button', { name: /Close/ });
    userEvent.click(close[0]);

    await waitFor(() =>
      expect(
        screen.queryByRole('button', { name: /Create Map/i })
      ).not.toBeInTheDocument()
    );
  });

  test('Open ROI browser, add ROI from periodic table', async () => {
    const { view } = await startApp({ layout: 'layout_twod' });

    await waitSubsamples(view);
    const { objSelector } = await waitSelectSubsample(view);

    withControl();
    await waitFor(() =>
      expect(screen.getByText(/session has control/)).toBeTruthy()
    );

    const roiBrowser = objSelector.getByRole('button', {
      name: /ROIs/
    });

    userEvent.click(roiBrowser);

    const dialogSelector = within(screen.getByRole('dialog'));
    userEvent.click(dialogSelector.getByText('Mn'));

    // Add ROI
    userEvent.click(screen.getByText('K-L3'));

    const dialog2Selector = within(screen.getByRole('dialog'));
    userEvent.click(
      dialog2Selector.getAllByRole('button', { name: /Close/ })[0]
    );

    await waitFor(() =>
      expect(dialogSelector.getByText('5866')).toBeInTheDocument()
    );

    userEvent.click(
      dialogSelector.getAllByRole('button', { name: /Close/ })[0]
    );

    await waitFor(() =>
      expect(screen.queryByText('Mn')).not.toBeInTheDocument()
    );
  });
});
