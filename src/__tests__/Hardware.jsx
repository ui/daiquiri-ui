import { each } from 'lodash';
import { screen, userEvent, waitFor } from 'helpers/tests';
import { withControl } from 'helpers/tests/state';

import startApp from 'helpers/tests/startApp';
import hardware from 'providers/hardware';

describe('Hardware views', () => {
  test('load hardware objects, check label text is visible', async () => {
    const { resources } = await startApp({ layout: 'layout_hardware' });
    const res = resources.getHandler('hardware');

    await waitFor(() => expect(screen.getByText('omega 123')).toBeTruthy());

    const objs = res.getResponse({ method: 'get', url: '' });

    each(objs.rows, obj => {
      if (!obj.name.includes('objectref')) {
        expect(screen.getByText(obj.name)).toBeTruthy();
      }
    });
  });

  test('select motor, change value, submit', async () => {
    const { resources, store } = await startApp({ layout: 'layout_hardware' });

    await waitFor(() => expect(screen.getByText('omega 123')).toBeTruthy());

    const omega = screen.getByLabelText('omega 123');
    expect(omega).toBeDisabled();

    withControl();
    await waitFor(() =>
      expect(screen.getByText(/session has control/)).toBeTruthy()
    );
    expect(omega).not.toBeDisabled();

    const spy = jest.spyOn(resources.client(), 'post');
    const newVal = 20;

    userEvent.clear(omega);
    userEvent.type(omega, `${newVal}{Enter}`);

    await waitFor(() =>
      expect(spy).toHaveBeenCalledWith(
        expect.stringMatching(/hardware\/omega$/),
        expect.objectContaining({
          function: 'move',
          value: newVal
        }),
        expect.anything(Object)
      )
    );

    const hw = hardware.getNamespace('hardware').getInstance('default');
    await hw.fetch();

    const objs = hw.selector('results', store.getState());
    expect(objs.omega.properties.position).toEqual(newVal);
  });

  test('select motor, change value, dont submit', async () => {
    jest.useFakeTimers();

    await startApp({ layout: 'layout_hardware' });

    await waitFor(() => expect(screen.getByText('omega 123')).toBeTruthy());

    const omega = screen.getByLabelText('omega 123');

    withControl();
    await waitFor(() =>
      expect(screen.getByText(/session has control/)).toBeTruthy()
    );
    expect(omega).not.toBeDisabled();

    const lastVal = omega.value;
    const newVal = `${parseFloat(lastVal) + 20}`;

    userEvent.clear(omega);
    userEvent.type(omega, newVal);
    expect(omega.value).toEqual(newVal);

    userEvent.tab();
    jest.runOnlyPendingTimers();
    expect(omega.value).toEqual(lastVal);
  });
});
