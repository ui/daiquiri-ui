import {
  render as rtlRender,
  createStore,
  screen,
  userEvent,
  waitFor
} from 'helpers/tests';
import App from 'App';
import Start from 'Start';

import Resources from 'helpers/tests/resources';
import app from 'providers/app';
import session from 'providers/session';

describe('<App/>', () => {
  const resources = new Resources();
  const authHandler = resources.getHandler('authenticator');

  it('login, select session', async () => {
    const obj = authHandler.getObject({
      url: '/login',
      method: 'post'
    });

    const store = createStore();
    Start.start(store);
    rtlRender(<App />, { store });

    await waitFor(() =>
      expect(app.selector('ready', store.getState())).toBeTruthy()
    );

    const user = screen.getByLabelText('Username');
    const pass = screen.getByLabelText('Password');
    const submit = screen.getByRole('button', { name: /Login/i });

    userEvent.type(user, obj.validUser);
    userEvent.type(pass, 'abcd');
    userEvent.click(submit);

    await waitFor(() =>
      expect(session.selector('auth', store.getState())).toBeTruthy()
    );
    expect(screen.getByText(/Select a session/)).toBeTruthy();

    const rows = screen.getAllByRole('row');
    userEvent.click(rows[1]);

    await waitFor(() =>
      expect(
        session.selector('currentBLSession', store.getState())
      ).toBeTruthy()
    );
    expect(screen.getAllByText(/does not have control/)).toBeTruthy();
  });
});
