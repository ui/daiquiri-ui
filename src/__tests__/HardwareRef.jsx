import { screen, waitFor } from 'helpers/tests';

import startApp from 'helpers/tests/startApp';
import app from 'providers/app';
import hardware from 'providers/hardware';

describe('Hardware views with ref', () => {
  async function waitForLayout({ layout, contains }) {
    await startApp();
    app.dispatch('SWITCH_LAYOUT_SLUG', layout);
    await waitFor(() => {
      const element = screen.queryByText(contains);
      expect(element).toBeInTheDocument();
    });
    const mainLayout = screen.getByTestId('main-layout');
    expect(mainLayout).toBeInTheDocument();
    // screen.debug(mainLayout);
  }

  test('An empty objectref displays nothing', async () => {
    await waitForLayout({
      layout: 'layout_hardware_ref1',
      contains: 'Test objectref_to_nothing'
    });
    const element = screen.queryByText('Missing');
    expect(element).toBeInTheDocument();
  });
  test('An objectref displays the subcomponent', async () => {
    await waitForLayout({
      layout: 'layout_hardware_ref2',
      contains: 'Test objectref_to_omega'
    });
    const element = screen.queryByText('omega 123');
    expect(element).toBeInTheDocument();
  });
  test('A double objectref displays the subcomponent', async () => {
    await waitForLayout({
      layout: 'layout_hardware_ref3',
      contains: 'Test objectref_to_objectref_to_omega'
    });
    const element = screen.queryByText('omega 123');
    expect(element).toBeInTheDocument();
  });
  test('Updating an objectref displays the right subcomponent', async () => {
    await waitForLayout({
      layout: 'layout_hardware_ref2',
      contains: 'Test objectref_to_omega'
    });
    hardware.dispatch('UPDATE_HARDWARE', {
      id: 'objectref_to_omega',
      data: { ref: 'hardware:omega1' }
    });
    expect(screen.queryByText('omega 123')).not.toBeInTheDocument();
    expect(screen.queryByText('omega1')).toBeInTheDocument();
  });
  test('Updating double objectref leaf displays the right subcomponent', async () => {
    await waitForLayout({
      layout: 'layout_hardware_ref3',
      contains: 'Test objectref_to_objectref_to_omega'
    });
    hardware.dispatch('UPDATE_HARDWARE', {
      id: 'objectref_to_omega',
      data: { ref: 'hardware:omega1' }
    });
    expect(screen.queryByText('omega 123')).not.toBeInTheDocument();
    expect(screen.queryByText('omega1')).toBeInTheDocument();
  });
  test('Updating double objectref root displays the right subcomponent', async () => {
    await waitForLayout({
      layout: 'layout_hardware_ref3',
      contains: 'Test objectref_to_objectref_to_omega'
    });
    hardware.dispatch('UPDATE_HARDWARE', {
      id: 'objectref_to_objectref_to_omega',
      data: { ref: 'hardware:objectref_to_omega1' }
    });
    expect(screen.queryByText('omega 123')).not.toBeInTheDocument();
    expect(screen.queryByText('omega1')).toBeInTheDocument();
  });
});
