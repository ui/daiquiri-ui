import { keys } from 'lodash';
import { screen, waitFor, userEvent, within } from 'helpers/tests';

import startApp from 'helpers/tests/startApp';

import scans from 'providers/scans';

describe('scan views', () => {
  test('select first scan, check legends are present, select series', async () => {
    const { view, resources, store } = await startApp({
      layout: 'layout_scanviewer'
    });
    const res = resources.getHandler('scans');

    const scantable = view.container.querySelector('.scan-table');
    const withinScans = within(scantable);
    const rows = withinScans.getAllByRole('row');
    const cells = within(rows[1]).getAllByRole('cell');
    userEvent.click(cells[0]);

    const sdata = scans.getNamespace('data').getInstance('default');
    await waitFor(() =>
      expect(sdata.selector('fetched', store.getState())).toBeTruthy()
    );

    const selected = scans.selector('selectedScan', store.getState());
    const data = res.getResponse({
      method: 'get',
      url: `/data/${selected}`
    });

    const series = screen.getByRole('button', { name: /Series/i });
    userEvent.click(series);

    const last = data.axes.ys.scalars[data.axes.ys.scalars.length - 1];

    const check = screen.getByRole('checkbox', { name: last });
    userEvent.click(check);

    const selectedDataSeries = scans.selector(
      'selectedDataSeries',
      store.getState()
    );
    expect(keys(selectedDataSeries)).toEqual(expect.arrayContaining([last]));

    const spectra = res.getResponse({
      method: 'get',
      url: `/spectra/${selected}`
    });
    expect(
      screen.getByText(keys(spectra.data)[0], { hidden: true })
    ).toBeTruthy();
  });
});
