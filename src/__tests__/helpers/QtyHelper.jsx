import Qty from 'js-quantities';
import { toHumanReadableUnit } from 'helpers/QtyHelper';

describe('Qty helper', () => {
  test('check already normalized', async () => {
    const q = new Qty('10 mm');
    const result = toHumanReadableUnit(q);
    expect(result.toPrec(0.1).toString()).toEqual('10 mm');
  });
  test('check with smaller unit than expected', async () => {
    const q = new Qty('1000 mm');
    const result = toHumanReadableUnit(q);
    expect(result.toPrec(0.1).toString()).toEqual('1 m');
  });
  test('check with bigger unit than expected', async () => {
    const q = new Qty('0.02 mm');
    const result = toHumanReadableUnit(q);
    expect(result.toPrec(0.1).toString()).toEqual('20 um');
  });
  test('check all precision with 1 digit', async () => {
    const q = new Qty('0.0222 mm');
    const result = toHumanReadableUnit(q);
    expect(result.toPrec(0.1).toString()).toEqual('22.2 um');
  });
  test('check all precision with 0 digit', async () => {
    const q = new Qty('0.222 mm');
    const result = toHumanReadableUnit(q);
    expect(result.toPrec(0.1).toString()).toEqual('222 um');
  });
  test('check with a value bigger than 200', async () => {
    const q = new Qty('0.500 mm');
    const result = toHumanReadableUnit(q);
    expect(result.toPrec(0.1).toString()).toEqual('0.5 mm');
  });
  test('check zero', async () => {
    const q = new Qty('0 mm');
    const result = toHumanReadableUnit(q);
    expect(result.toPrec(0.1).toString()).toEqual('0 mm');
  });
  test('check without prefix', async () => {
    const q = new Qty('0.0256 m');
    const result = toHumanReadableUnit(q);
    expect(result.toPrec(0.1).toString()).toEqual('25.6 mm');
  });

  // The foolowing tests are only here is no changes between implementation
  test('check above rounding error', async () => {
    const q = new Qty('0.995 mm');
    const result = toHumanReadableUnit(q);
    expect(result.toPrec(0.1).toString()).toEqual('995 um');
  });
  test('check under rounding error', async () => {
    const q = new Qty('0.996 mm');
    const result = toHumanReadableUnit(q);
    expect(result.toPrec(0.1).toString()).toEqual('1 mm');
  });
});
