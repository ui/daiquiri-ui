import { IffReader } from '../../helpers/iff';

describe('Check IFF reader', () => {
  test('Read single block', () => {
    const A = 65;
    const array = new Uint8Array([A, A, A, A, 0, 0, 0, 2, 0xff, 0xff]);
    const iff = new IffReader(array.buffer);
    expect(iff.readChunkType()).toBe('AAAA');
    expect(iff.readChunk().byteLength).toBe(2);
    expect(iff.eof()).toBeTruthy();
  });
  test('Read blocks', () => {
    const A = 65;
    const B = 66;
    const array = new Uint8Array([
      A,
      A,
      A,
      A,
      0,
      0,
      0,
      4,
      0xff,
      0xff,
      0xff,
      0xff,
      B,
      B,
      B,
      B,
      0,
      0,
      0,
      0,
    ]);
    const iff = new IffReader(array.buffer);
    expect(iff.readChunkType()).toBe('AAAA');
    expect(iff.readChunk().byteLength).toBe(4);
    expect(iff.readChunkType()).toBe('BBBB');
    expect(iff.readChunk().byteLength).toBe(0);
    expect(iff.eof()).toBeTruthy();
  });
  test('Read odd blocks', () => {
    const A = 65;
    const B = 66;
    const array = new Uint8Array([
      A,
      A,
      A,
      A,
      0,
      0,
      0,
      3,
      0xff,
      0xff,
      0xff,
      0,
      B,
      B,
      B,
      B,
      0,
      0,
      0,
      0,
    ]);
    const iff = new IffReader(array.buffer);
    expect(iff.readChunkType()).toBe('AAAA');
    expect(iff.readChunk().byteLength).toBe(3);
    expect(iff.readChunkType()).toBe('BBBB');
    expect(iff.readChunk().byteLength).toBe(0);
    expect(iff.eof()).toBeTruthy();
  });
});
