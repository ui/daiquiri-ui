import { useScanData } from '../../services/ScanDataService';
import { renderHook, act } from '@testing-library/react-hooks';
import Resources from '../../helpers/tests/resources';
import ResourceHandler from '../../helpers/tests/resources/ResourceHandler';
import type { AxiosRequestConfig } from 'axios';

const SCAN_DATA = {
  data: {
    foo: {
      dtype: '<u1',
      data: [],
      name: 'foo',
      shape: [],
    },
    foo2: {
      dtype: '<u2',
      data: [],
      name: 'foo2',
      shape: [],
    },
    image1: {
      dtype: '<u1',
      data: [],
      name: 'image1',
      shape: [2, 2],
    },
  },
  scanid: 10,
};

class Scans extends ResourceHandler {
  public baseUrl = '/scans';

  public resources = {
    '/status': {
      get: {
        status: 200,
        response: () => ({ progress: 50, scanid: null }),
      },
    },

    '/data/10': {
      get: {
        status: 200,
        response: () => SCAN_DATA,
      },
    },

    '/data/binary/10': {
      get: {
        status: 200,
        response: (payload: unknown, request: AxiosRequestConfig) => {
          const { channel, selection } = request?.params;
          if (channel === 'foo') {
            if (selection === '0:4') {
              return Uint8Array.from([1, 2, 3, 4]).buffer;
            }
            if (selection === '4:8') {
              return Uint8Array.from([5, 6, 7, 8]).buffer;
            }
          }
          if (channel === 'foo2') {
            if (selection === '0:4') {
              return Uint8Array.from([1, 0, 2, 0, 3, 0, 4, 0]).buffer;
            }
            if (selection === '4:8') {
              return Uint8Array.from([5, 0, 6, 0, 7, 0, 8, 0]).buffer;
            }
          }
          if (channel === 'image1') {
            if (selection === '0:1') {
              return Uint8Array.from([0, 1, 2, 3]).buffer;
            }
          }
          return Uint8Array.from([]).buffer;
        },
      },
    },
  };
}

let RESOURCES: Resources | undefined;

describe('Check useScanData', () => {
  beforeAll(() => {
    RESOURCES = new Resources({
      handlers: {
        scans: Scans,
      },
    });
  });
  afterAll(() => {
    RESOURCES = undefined;
  });

  beforeEach(() => {
    jest.spyOn(global.console, 'warn').mockImplementation();
    jest.spyOn(global.console, 'error').mockImplementation();
  });
  afterEach(() => {
    expect(console.warn).toBeCalledTimes(0);
    expect(console.error).toBeCalledTimes(0);
    // @ts-expect-error
    console.warn.mockRestore();
    // @ts-expect-error
    console.error.mockRestore();
  });
  test('No scan', () => {
    const { result } = renderHook(() => {
      return useScanData({
        scanId: undefined,
        channelNames: ['foo'],
        start: 0,
        stop: 4,
      });
    });
    expect(result.current).toStrictEqual({
      loading: false,
      data: {},
    });
  });
  test('Read single', async () => {
    const { result, waitForNextUpdate } = renderHook(() => {
      return useScanData({
        scanId: 10,
        channelNames: ['foo'],
        start: 0,
        stop: 4,
      });
    });
    await waitForNextUpdate();
    expect(result.current?.data?.foo).toBeDefined();
    expect(result.current.data.foo?.data).toStrictEqual(
      Uint8Array.from([1, 2, 3, 4])
    );
  });
  test('Update to bigger', async () => {
    const initialProps = {
      scanId: 10,
      channelNames: ['foo'],
      start: 0,
      stop: 4,
    };
    const { result, waitForNextUpdate, rerender } = renderHook(
      (props) => {
        return useScanData(props);
      },
      {
        initialProps,
      }
    );
    await waitForNextUpdate();
    rerender({ ...initialProps, stop: 8 });
    await waitForNextUpdate();
    expect(result.current?.data?.foo).toBeDefined();
    expect(result.current.data.foo?.data).toStrictEqual(
      Uint8Array.from([1, 2, 3, 4, 5, 6, 7, 8])
    );
  });
  test('Update to smaller', async () => {
    const initialProps = {
      scanId: 10,
      channelNames: ['foo'],
      start: 0,
      stop: 4,
    };
    const { result, waitForNextUpdate, rerender } = renderHook(
      (props) => {
        return useScanData(props);
      },
      {
        initialProps,
      }
    );
    await waitForNextUpdate();
    rerender({ ...initialProps, start: 1, stop: 3 });
    await waitForNextUpdate();
    expect(result.current?.data?.foo).toBeDefined();
    expect(result.current.data.foo?.data).toStrictEqual(
      Uint8Array.from([2, 3])
    );
  });
  test('Uint16 channel', async () => {
    const initialProps = {
      scanId: 10,
      channelNames: ['foo2'],
      start: 0,
      stop: 4,
    };
    const { result, waitForNextUpdate, rerender } = renderHook(
      (props) => {
        return useScanData(props);
      },
      {
        initialProps,
      }
    );
    await waitForNextUpdate();
    rerender({ ...initialProps, stop: 8 });
    await waitForNextUpdate();
    expect(result.current?.data?.foo2).toBeDefined();
    expect(result.current.data.foo2?.data).toStrictEqual(
      Uint16Array.from([1, 2, 3, 4, 5, 6, 7, 8])
    );
  });
  test('Image channel', async () => {
    const initialProps = {
      scanId: 10,
      channelNames: ['image1'],
      start: 0,
      stop: 1,
    };
    const { result, waitForNextUpdate, rerender } = renderHook(
      (props) => {
        return useScanData(props);
      },
      {
        initialProps,
      }
    );
    await waitForNextUpdate();
    expect(result.current?.data?.image1).toBeDefined();
    expect(result.current.data.image1?.shape).toStrictEqual([1, 2, 2]);
    expect(result.current.data.image1?.data?.buffer).toStrictEqual(
      Uint8Array.from([0, 1, 2, 3]).buffer
    );
  });
});
