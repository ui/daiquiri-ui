import { each } from 'lodash';

import {
  setupProviders,
  mockClientSizing,
  createStore,
  waitFor,
  render as rtlRender,
  screen,
  userEvent,
  getAllByRole,
  within
} from 'helpers/tests';

import Resources from 'helpers/tests/resources';

import ConnectedObjectList from 'connect/2dview/ObjectList';
import metadata from 'providers/metadata';
import app from 'providers/app';

describe('<ObjectList />', () => {
  const resources = new Resources();
  const metadataHandler = resources.getHandler('metadata');
  resources.getHandler('schema');

  const subsamples = metadata.getNamespace('subsamples').getInstance('default');
  const schema = app.getNamespace('schema').getInstance('default');

  const store = createStore();
  setupProviders(store);

  const render = () => {
    mockClientSizing();
    return rtlRender(<ConnectedObjectList />, { store });
  };

  it('matches empty', () => {
    render();
    expect(screen.getByText(/No objects/)).toBeTruthy();
  });

  it('matches loaded subsamples', async () => {
    await schema.fetch();
    await subsamples.fetch();

    render();
    expect(screen.getByText(/ROI/)).toBeTruthy();
  });

  it('select object', async () => {
    await schema.fetch();
    await subsamples.fetch();

    render();

    const rows = screen.getAllByRole('row');
    const cells = within(rows[1]).getAllByRole('cell');
    userEvent.click(cells[0]);

    const objects = subsamples.selector('results', store.getState());
    const selected = metadata.selector('selectedSubSamples', store.getState());

    expect(objects[1].subsampleid).toEqual(selected[0]);
  });

  it('delete object', async () => {
    await schema.fetch();
    await subsamples.fetch();

    render();

    const samples = metadataHandler.getResponse({
      url: '/samples',
      method: 'get'
    });

    const rows = screen.getAllByRole('row');
    expect(rows.length - 1).toEqual(samples.rows.length);

    const cells = getAllByRole(rows[1], 'cell');
    const del = getAllByRole(cells[cells.length - 1], /button/)[1];
    expect(del.classList.contains('delete')).toBe(true);

    userEvent.click(del);

    const spy = jest.spyOn(resources.client(), 'delete');
    const button = screen.getByRole('button', { name: /OK/i, hidden: true });
    userEvent.click(button);

    await waitFor(() => expect(spy).toHaveBeenCalled());

    expect(screen.getByText(/No objects/)).toBeTruthy();
  });

  it('modify object', async () => {
    await schema.fetch();
    await subsamples.fetch();

    render();

    const rows = screen.getAllByRole('row');
    const cells = getAllByRole(rows[1], 'cell');
    const edit = getAllByRole(cells[cells.length - 1], /button/)[0];
    expect(edit.classList.contains('view')).toBe(true);

    userEvent.click(edit);
    const spy = jest.spyOn(resources.client(), 'patch');

    const vals = {
      width: 300,
      height: 400
    };
    each(vals, (v, k) => {
      const eregex = new RegExp(`edit-${k}`, 'i');
      userEvent.click(screen.getByLabelText(eregex));

      const regex = new RegExp(k, 'i');
      userEvent.clear(screen.getByLabelText(regex));
      userEvent.type(screen.getByLabelText(regex), v.toString());
      userEvent.click(
        screen.getByRole('button', { name: /Save/i, hidden: true })
      );
    });

    await waitFor(() => expect(spy).toHaveBeenCalledTimes(2));

    const objects = subsamples.selector('results', store.getState());

    expect(objects[1].x2 - objects[1].x).toBeCloseTo(vals.width);
    expect(objects[1].y2 - objects[1].y).toBeCloseTo(vals.height);
  });
});
