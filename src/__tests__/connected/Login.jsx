import {
  setupProviders,
  mockClientSizing,
  createStore,
  render as rtlRender,
  screen,
  userEvent,
  waitFor
} from 'helpers/tests';

import Resources from 'helpers/tests/resources';

import Login from 'connect/Login';
import session from 'providers/session';

describe('Login', () => {
  const resources = new Resources();
  const authHandler = resources.getHandler('authenticator');

  const store = createStore();
  setupProviders(store);

  const render = () => {
    mockClientSizing();
    const view = rtlRender(<Login />, { store });

    const user = screen.getByLabelText('Username');
    const pass = screen.getByLabelText('Password');
    const submit = screen.getByRole('button', { name: /Login/i });

    return { view, user, pass, submit };
  };

  test('login failed', async () => {
    const { user, pass, submit } = render();

    const method = {
      url: '/login',
      method: 'post'
    };
    const obj = authHandler.getObject(method);

    userEvent.type(user, obj.invalidUser);
    userEvent.type(pass, 'abcd');
    userEvent.click(submit);

    await waitFor(() =>
      expect(session.selector('auth', store.getState())).toEqual(false)
    );

    const resp = authHandler.getResponse({
      ...method,
      payload: {
        username: obj.invalidUser
      }
    });

    expect(session.selector('error', store.getState())).toEqual(resp.error);
  });

  test('login success', async () => {
    const { user, pass, submit } = render();

    const method = {
      url: '/login',
      method: 'post'
    };
    const obj = authHandler.getObject(method);

    userEvent.type(user, obj.validUser);
    userEvent.type(pass, 'abcd');
    userEvent.click(submit);

    await waitFor(() =>
      expect(session.selector('auth', store.getState())).toEqual(true)
    );
  });
});
