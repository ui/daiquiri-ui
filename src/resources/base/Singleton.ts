import type { Schema } from '@rest-hooks/rest';
import { createResource } from '@rest-hooks/rest';

import { AuthenticatedEndpoint } from '../endpoints/Authenticated';

export default function createSingletonResource<
  U extends string,
  S extends Schema
>({ path, schema }: { readonly path: U; readonly schema: S }) {
  return createResource({
    path,
    schema,
    Endpoint: AuthenticatedEndpoint,
  });
}
