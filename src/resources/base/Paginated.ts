import type { Schema } from '@rest-hooks/rest';
import { createResource } from '@rest-hooks/rest';

import { AuthenticatedEndpoint } from '../endpoints/Authenticated';

export default function createPaginatedResource<
  U extends string,
  S extends Schema
>({ path, schema }: { readonly path: U; readonly schema: S }) {
  const BaseResource = createResource({
    path,
    schema,
    Endpoint: AuthenticatedEndpoint,
  });

  return {
    ...BaseResource,
    getList: BaseResource.getList.extend({
      schema: { rows: [schema], total: 0 },
    }),
  };
}
