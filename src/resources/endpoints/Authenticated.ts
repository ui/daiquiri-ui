import type { RestGenerics } from '@rest-hooks/rest';
import { BaseEndpoint } from './Base';

export class AuthenticatedEndpoint<
  O extends RestGenerics = any
> extends BaseEndpoint<O> {
  public static accessToken?: string;

  public getHeaders(headers: HeadersInit): HeadersInit {
    return {
      ...headers,
      Authorization: `Bearer ${AuthenticatedEndpoint.accessToken}`,
    };
  }
}
