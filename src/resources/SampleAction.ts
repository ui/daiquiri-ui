import { Entity } from '@rest-hooks/rest';
import createPaginatedResource from './base/Paginated';

export interface SampleAction {
  sampleactionid: number;
  has_result: boolean;
}

export class SampleActionEntity extends Entity {
  public sampleactionid: number | undefined = undefined;
  public has_result = false;

  public pk() {
    return this.sampleactionid?.toString();
  }

  public static get key() {
    return 'SampleAction';
  }
}

export const SampleActionResource = createPaginatedResource({
  path: '/metadata/samples/actions/:sampleactionid',
  schema: SampleActionEntity,
});
