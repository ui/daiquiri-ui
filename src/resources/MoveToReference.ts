import { Entity } from '@rest-hooks/rest';
import createSingletonResource from './base/Singleton';

class MoveToReferenceEntity extends Entity {
  public moveid: number | undefined = undefined;
  public x = 0;
  public y = 0;
  public execute = true;
  public positions: Record<string, any> = {};

  public pk() {
    return this.moveid?.toString();
  }
  public static get key() {
    return 'MoveToReference';
  }
}

export const MoveToReferenceResource = createSingletonResource({
  path: '/imageviewer/move/reference/:moveid',
  schema: MoveToReferenceEntity,
});
