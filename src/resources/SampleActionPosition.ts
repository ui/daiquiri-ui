import { Entity } from '@rest-hooks/rest';
import createPaginatedResource from './base/Paginated';

export interface SampleActionPosition {
  sampleactionpositionid?: number;
  posx: number;
  posy: number;
  sampleactionid: number;
  type?: string;
  id?: number;
}

class SampleActionPositionEntity extends Entity {
  public sampleactionpositionid: number | undefined = undefined;
  public posx = 0;
  public posy = 0;
  public sampleactionid = 0;
  public type = '';
  public id = 0;

  public pk() {
    return this.sampleactionpositionid?.toString();
  }

  public static get key() {
    return 'SampleActionPosition';
  }
}

export const SampleActionPositionResource = createPaginatedResource({
  path: '/metadata/samples/actions/positions/:sampleactionpositionid',
  schema: SampleActionPositionEntity,
});
