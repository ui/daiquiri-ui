import { Entity } from '@rest-hooks/rest';
import createSingletonResource from './base/Singleton';

class SetReferenceMatrixEntity extends Entity {
  public matrixid: number | undefined = undefined;
  public sampleactionid = 0;

  public pk() {
    return this.matrixid?.toString();
  }

  public static get key() {
    return 'SetReferenceMatrix';
  }
}

export const SetReferenceMatrixResource = createSingletonResource({
  path: '/imageviewer/move/reference/matrix/:matrixid',
  schema: SetReferenceMatrixEntity,
});
