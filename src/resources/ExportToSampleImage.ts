import { Entity } from '@rest-hooks/rest';
import createSingletonResource from './base/Singleton';

class ExportToSampleImageEntity extends Entity {
  public exportid: number | undefined = undefined;
  public sampleactionid = 0;
  public crop: Record<string, number[]> = {};

  public pk() {
    return this.exportid?.toString();
  }

  public static get key() {
    return 'ExportToSampleImage';
  }
}

export const ExportToSampleImage = createSingletonResource({
  path: '/imageviewer/reference/export/:exportid',
  schema: ExportToSampleImageEntity,
});
