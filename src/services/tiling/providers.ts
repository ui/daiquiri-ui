import { getLayerSizes, TilesApi } from '@h5web/lib';
import type { Size } from '@h5web/lib';
import type { AxiosInstance } from 'axios';
import { clamp } from 'lodash';
import type { Options as LRUCacheOptions } from 'lru-cache';
import ndarray from 'ndarray';
import type { NdArray } from 'ndarray';
import type { Vector2 } from 'three';
import { createSuspenseFetch } from 'use-suspense-fetch';

class ChessboardTilesApi extends TilesApi {
  public readonly layerDType: LayerDType;

  public constructor(size: Size, tileSize: Size) {
    super(tileSize, getLayerSizes(size, tileSize));
    this.layerDType = LayerDType.Float32;
  }

  public get(layer: number, offset: Vector2): NdArray<Float32Array> {
    // Clip slice to size of the level
    const layerSize = this.layerSizes[layer];
    const width = clamp(layerSize.width - offset.x, 0, this.tileSize.width);
    const height = clamp(layerSize.height - offset.y, 0, this.tileSize.height);

    const value = (offset.x / width + offset.y / height) % 2;
    return ndarray(new Float32Array(width * height).fill(value), [
      height,
      width,
    ]);
  }

  public getPendingTilesCount(): number {
    return 0;
  }
}

enum LayerDType {
  UInt8 = 'uint8',
  Float16 = 'float16',
  Float32 = 'float32',
}

interface LayerParams {
  file: string;
  path: string;
  tomovisRestService: AxiosInstance;
  size: Size;
  dtype: LayerDType;
}

type TileNdArray = NdArray<Uint16Array | Float32Array | Uint8Array>;

class H5grovePyramidTilesApi extends TilesApi {
  private readonly store;
  public readonly layerDType: LayerDType;
  public readonly layers: LayerParams[];

  public constructor(
    tileSize: Size,
    layers: LayerParams[],
    lruCacheOptions: LRUCacheOptions<any, any> = {}
  ) {
    const layerSizes = layers.map((layer) => layer.size);
    super(tileSize, layerSizes);
    this.layerDType = layers[0].dtype;
    this.layers = [...layers];

    this.store = createSuspenseFetch<TileNdArray>(0, lruCacheOptions);
  }

  public get(layer: number, offset: Vector2): TileNdArray {
    const { x, y } = offset;

    const key = `${layer}|${x}|${y}`;

    return this.store.fetch(key, async () => {
      const { file, path, tomovisRestService, size, dtype } =
        this.layers[layer];

      // Clip slice to size of the level
      const width = clamp(size.width - x, 0, this.tileSize.width);
      const height = clamp(size.height - y, 0, this.tileSize.height);

      const selection = `${y}:${y + height},${x}:${x + width}`;

      const arrayBuffer = await tomovisRestService
        .get('/h5grove/data/', {
          params: { file, path, selection, format: 'bin' },
          responseType: 'arraybuffer',
        })
        .then((response) => response.data);

      const ArrayType =
        dtype === LayerDType.Float16
          ? Uint16Array
          : dtype === LayerDType.Float32
          ? Float32Array
          : Uint8Array;
      return ndarray(new ArrayType(arrayBuffer), [height, width]);
    });
  }

  public getPendingTilesCount(): number {
    const cache = this.store.getCache();
    const pendingPromiseCache = cache
      .values()
      .filter((value) => !value.error && !value.response);
    return pendingPromiseCache.length;
  }
}

export type TomoTilesApi = ChessboardTilesApi | H5grovePyramidTilesApi;
export type { LayerParams };
export { ChessboardTilesApi, H5grovePyramidTilesApi, LayerDType };
