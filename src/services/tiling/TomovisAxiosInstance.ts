import type { AxiosInstance } from 'axios';
import axios from 'axios';
import { useEffect, useState } from 'react';
import baseUrl from 'helpers/baseUrl';
import RestService from 'services/RestService';

export function useTomovisAxiosInstance(
  tomovisUrl: string | undefined,
  tomovisProxyUrl: string | undefined
): AxiosInstance | undefined {
  const [service, setService] = useState<AxiosInstance | undefined>(undefined);

  useEffect(() => {
    if (!tomovisUrl && !tomovisProxyUrl) {
      setService(undefined);
      return;
    }
    if (tomovisUrl === 'mock') {
      setService(() => axios.create({ baseURL: tomovisUrl }));
      return;
    }
    if (tomovisProxyUrl) {
      setService(() =>
        axios.create({
          baseURL: new URL(tomovisProxyUrl, baseUrl()).toString(),
          ...RestService.getOptions(),
        })
      );
    } else {
      setService(undefined);
    }
    if (tomovisUrl) {
      axios
        .get('status', { baseURL: tomovisUrl, timeout: 3000 })
        .then((response) =>
          setService(() => axios.create({ baseURL: tomovisUrl }))
        );
    }
  }, [tomovisUrl, tomovisProxyUrl, RestService.token]);

  return service;
}
