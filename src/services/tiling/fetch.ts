import type { Domain, HistogramParams as Histogram } from '@h5web/lib';
import type { AxiosInstance } from 'axios';
import { trimEnd } from 'lodash';
import type { NdArray, TypedArray } from 'ndarray';
import { LayerDType } from './providers';
import type { LayerParams } from './providers';
import { toNdArray } from 'helpers/ndarray';

export async function fetchH5GroveDataset(
  tomovisRestService: AxiosInstance,
  file: string,
  path: string
): Promise<NdArray<TypedArray>> {
  const meta = await tomovisRestService
    .get('/h5grove/meta/', {
      params: {
        file,
        path,
      },
    })
    .then((response) => response.data);

  if (meta.type !== 'dataset') {
    throw new Error('Not a dataset');
  }
  const { shape, dtype } = meta as { shape: number[]; dtype: string };

  const buffer = await tomovisRestService
    .get('/h5grove/data/', {
      params: { file, path, format: 'bin', dtype: 'safe' },
      responseType: 'arraybuffer',
    })
    .then((response) => response.data);
  return toNdArray(buffer, shape, dtype);
}

function absPath(relOrAbsPath: string, base: string) {
  if (relOrAbsPath.startsWith('/')) {
    return relOrAbsPath;
  }
  return `${trimEnd(base, '/')}/${relOrAbsPath}`;
}

interface H5grovePyramidMetadata {
  histogram: Histogram;
  layers: LayerParams[];
  yDomain: Domain;
  zDomain: Domain;
}

export async function fetchH5grovePyramidMetadata(
  tomovisRestService: AxiosInstance,
  file: string,
  path: string
): Promise<H5grovePyramidMetadata> {
  const values = await fetchH5GroveDataset(
    tomovisRestService,
    file,
    absPath('histogram/count', path)
  );
  const bins = await fetchH5GroveDataset(
    tomovisRestService,
    file,
    absPath('histogram/bin_edges', path)
  );
  const yDomainArray = await fetchH5GroveDataset(
    tomovisRestService,
    file,
    absPath('y', path)
  );
  const yDomain: Domain = [yDomainArray.data[0], yDomainArray.data[1]];
  const zDomainArray = await fetchH5GroveDataset(
    tomovisRestService,
    file,
    absPath('z', path)
  );
  const zDomain: Domain = [zDomainArray.data[0], zDomainArray.data[1]];
  const layerPaths: string[] = await tomovisRestService
    .get('/h5grove/data/', { params: { file, path: absPath('layers', path) } })
    .then((response) => response.data);
  const layers = await Promise.all(
    layerPaths.map(async (layerPath): Promise<LayerParams> => {
      const absLayerPath = absPath(layerPath, path);
      const meta = await tomovisRestService
        .get('/h5grove/meta/', {
          params: {
            file,
            path: absLayerPath,
          },
        })
        .then((response) => response.data);
      if (meta.type !== 'dataset') {
        throw new Error('Not a dataset');
      }
      const { shape, dtype } = meta as { shape: number[]; dtype: string };
      if (shape.length !== 2) {
        throw new Error('dataset is not 2D');
      }
      const [height, width] = shape;

      if (dtype !== '<f2' && dtype !== '<f4' && dtype !== '|u1') {
        throw new Error(`Unsupported dtype: ${dtype}`);
      }
      const LayerDtype =
        dtype === '<f2'
          ? LayerDType.Float16
          : dtype === '<f4'
          ? LayerDType.Float32
          : LayerDType.UInt8;

      return {
        file,
        path: absLayerPath,
        tomovisRestService,
        size: { width, height },
        dtype: LayerDtype,
      };
    })
  );

  return {
    histogram: { values: values.data, bins: bins.data },
    layers,
    yDomain,
    zDomain,
  };
}
