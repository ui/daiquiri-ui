import type { AxiosInstance } from 'axios';
import { useCallback, useEffect, useState } from 'react';
import { Box2, Vector2 } from 'three';
import type { TomoTilesApi } from './providers';
import { ChessboardTilesApi, H5grovePyramidTilesApi } from './providers';
import { fetchH5GroveDataset, fetchH5grovePyramidMetadata } from './fetch';
import { stripHistogram } from 'components/h5web/utils';
import type { ArrayHistogram } from 'components/h5web/colormap';
import { toNdArray } from 'helpers/ndarray';

interface TomovisTilingService {
  apiFront: TomoTilesApi | undefined;
  apiSide: TomoTilesApi | undefined;
  histogram: ArrayHistogram | undefined;
  tilingBoxFront: Box2;
  tilingBoxSide: Box2;
  // Recreate the service
  invalidate: () => void;
  // Wait for the end of the actual tile refresh, and recreate the service only when it's done
  lazyRefresh: () => void;
}

export function useTomovisTilingService(
  tomovisRestService: AxiosInstance | undefined,
  id: string | null | undefined
): TomovisTilingService {
  const [refresh, setRefresh] = useState<boolean>(false);
  const [refreshRequested, setRefreshRequested] = useState<boolean>(false);
  const [apiFront, setApiFront] = useState<TomoTilesApi | undefined>(undefined);
  const [apiSide, setApiSide] = useState<TomoTilesApi | undefined>(undefined);
  const [histogram, setHistogram] = useState<ArrayHistogram>();
  const [tilingBoxFront, setTilingBoxFront] = useState<Box2>(
    new Box2(new Vector2(), new Vector2(1, 1))
  );
  const [tilingBoxSide, setTilingBoxSide] = useState<Box2>(
    new Box2(new Vector2(), new Vector2(1, 1))
  );

  const setDefault = useCallback(() => {
    setApiFront(undefined);
    setApiSide(undefined);
    setHistogram(undefined);
    setTilingBoxFront(new Box2(new Vector2(), new Vector2(1, 1)));
    setTilingBoxSide(new Box2(new Vector2(), new Vector2(1, 1)));
  }, [
    setApiFront,
    setApiSide,
    setHistogram,
    setTilingBoxFront,
    setTilingBoxSide,
  ]);

  useEffect(() => {
    const tileSize = { width: 256, height: 256 };

    if (!tomovisRestService) {
      return;
    }

    if (tomovisRestService.defaults.baseURL === 'mock') {
      // Demo mode
      const api = new ChessboardTilesApi(
        { width: 2048, height: 4096 },
        tileSize
      );
      setApiFront(api);
      setApiSide(api);
      setHistogram(undefined);
      setTilingBoxFront(new Box2(new Vector2(-50, -100), new Vector2(50, 100)));
      setTilingBoxSide(new Box2(new Vector2(-50, -100), new Vector2(50, 100)));
      return;
    }

    if (!id) {
      setDefault();
      return;
    }

    tomovisRestService
      .get(`/image_tiling/${id}`)
      .then((response) => {
        const { status, output } = response.data;
        if (status !== 'done' && status !== 'running') {
          throw new Error('Status is not done');
        }

        if (output.length !== 2) {
          throw new Error('Error in retrieve tiling information');
        }
        const { location: frontFile, hdf5_entry: frontPath } = output[0];
        const { location: sideFile, hdf5_entry: sidePath } = output[1];
        if (!frontFile || !frontPath) {
          throw new Error('Error in retrieved tiling locations');
        }

        return Promise.all([
          fetchH5grovePyramidMetadata(tomovisRestService, frontFile, frontPath),
          fetchH5grovePyramidMetadata(tomovisRestService, sideFile, sidePath),
          fetchH5GroveDataset(
            tomovisRestService,
            frontFile,
            '/sequence/histogram/count'
          ),
          fetchH5GroveDataset(
            tomovisRestService,
            frontFile,
            '/sequence/histogram/bin_edges'
          ),
        ]);
      })
      .catch((error) => setDefault())
      .then((configs) => {
        if (!configs) {
          setDefault();
          return;
        }
        const { layers, yDomain, zDomain } = configs[0];
        setApiFront(new H5grovePyramidTilesApi(tileSize, layers));
        setTilingBoxFront(
          new Box2(
            new Vector2(yDomain[0], zDomain[0]),
            new Vector2(yDomain[1], zDomain[1])
          )
        );

        const {
          layers: layersSide,
          yDomain: yDomainSide,
          zDomain: zDomainSide,
        } = configs[1];
        setApiSide(new H5grovePyramidTilesApi(tileSize, layersSide));
        setTilingBoxSide(
          new Box2(
            new Vector2(yDomainSide[0], zDomainSide[0]),
            new Vector2(yDomainSide[1], zDomainSide[1])
          )
        );

        const histogram = stripHistogram({
          values: configs[2].data,
          bins: configs[3].data,
        });
        setHistogram({
          values: toNdArray(
            new Float64Array(histogram.values),
            [histogram.values.length],
            '<f8'
          ),
          bins: toNdArray(
            new Float64Array(histogram.bins),
            [histogram.bins.length],
            '<f8'
          ),
        });
      });
  }, [id, tomovisRestService, refresh, setDefault]);

  useEffect(() => {
    if (!refreshRequested) {
      return () => {};
    }
    if (!apiSide || !apiFront) {
      setDefault();
      setRefresh((value) => !value);
      setRefreshRequested(false);
      return () => {};
    }
    const updateFinished =
      apiSide.getPendingTilesCount() + apiFront.getPendingTilesCount() === 0;
    if (updateFinished) {
      setDefault();
      setRefresh((value) => !value);
      setRefreshRequested(false);
      return () => {};
    }
    const refreshInterval = setInterval(() => {
      setDefault();
      setRefresh((value) => !value);
      setRefreshRequested(false);
    }, 1000);
    return () => {
      clearInterval(refreshInterval);
    };
  }, [apiSide, apiFront, refreshRequested, setDefault]);

  return {
    apiFront,
    apiSide,
    histogram,
    tilingBoxFront,
    tilingBoxSide,
    invalidate: () => {
      setDefault();
      setRefresh((value) => !value);
    },
    lazyRefresh: () => {
      setRefreshRequested(true);
    },
  };
}
