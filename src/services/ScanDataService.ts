import RestService from './RestService';
import type { NdArray, TypedArray } from 'ndarray';
import { useEffect, useMemo, useState } from 'react';
import type { Dtype } from '../helpers/ndarray';
import {
  toTypedArray,
  parseDtype,
  assertShape,
  toSafeDtype,
} from '../helpers/ndarray';
import ndarray from 'ndarray';

interface ScanDataRaw {
  data: Record<
    string,
    { name: string; data: number[]; shape: number[]; dtype: string }
  >;
}

interface ScanDataRequest {
  scanId: number | undefined;
  channelNames?: string[];
  start?: number;
  stop?: number;
}

export interface ScanDataResult {
  loading: boolean;
  data: Record<string, NdArray<TypedArray> | undefined>;
  request?: {
    channelNames: string[];
    start: number;
    stop: number;
    scanId: number;
  };
  errors?: string[];
}

interface Request {
  channelNames: string[];
  start: number;
  stop: number;
}

function concateArrays<T extends TypedArray>(a: undefined | T, b: T): T {
  if (a === undefined) {
    return b;
  }
  // @ts-expect-error
  const c = new b.constructor(a.length + b.length);
  c.set(a);
  c.set(b, a.length);
  return c;
}

function sliceIfNeeded(array: TypedArray, start: number, end: number) {
  if (start === 0 && array.length === end) {
    return array;
  }
  return array.subarray(start, end);
}

class AsyncFetcher {
  private isAborted: boolean;

  private isFetching: boolean;

  private readonly cache: Record<
    string,
    {
      dtype: Dtype;
      /** The supposed type returned when we use 'safe' in the fetch API */
      safeDtype: Dtype;
      raw: TypedArray | undefined;
      nbItems: number;
      /** Shape of 1 element, `[]` is a scalar */
      shape: number[];
      /** Number of values per elements. 1 for a scalar */
      elementSize: number;
      /** The result of the request */
      ndarray: NdArray<TypedArray> | undefined;
    }
  >;

  private readonly updateCallback: React.Dispatch<
    React.SetStateAction<ScanDataResult>
  >;

  private readonly scanId: number;

  private description: ScanDataRaw | undefined;

  private pendingRequest: Request | undefined;

  public constructor(
    updateCallback: React.Dispatch<React.SetStateAction<ScanDataResult>>,
    scanId: number
  ) {
    this.isAborted = false;
    this.isFetching = false;
    this.pendingRequest = undefined;
    this.description = undefined;
    this.updateCallback = updateCallback;
    this.scanId = scanId;
    const res = RestService.get(`/scans/data/${scanId}`);
    this.cache = {};
    res
      .then((response) => {
        this.setupScan(response.data);
      })
      .catch((error) => {
        // FIXME: This should be part of the result errors instead
        console.error(error);
      });
  }

  private setupScan(description: ScanDataRaw) {
    this.description = description;

    if (this.pendingRequest) {
      this.fetchPendingRequest();
    }
  }

  private async fetchData(request: Request, previousErrors?: string[]) {
    if (this.isAborted) {
      return;
    }

    const resultRequest = {
      ...request,
      scanId: this.scanId,
      errors: previousErrors,
    };

    const newErrors: string[] = [];

    this.updateCallback({
      loading: true,
      data: Object.fromEntries(
        request.channelNames.map((name) => {
          return [name, this.cache[name]?.ndarray];
        })
      ),
      request: resultRequest,
    });

    request.channelNames.forEach((n) => {
      if (!(n in this.cache)) {
        const description = this.description?.data[n];
        if (description !== undefined) {
          const dtype = description.dtype;
          const shape = description.shape;
          assertShape(shape);
          const elementSize = shape.reduce((a, b) => a * b, 1);
          if (dtype !== undefined) {
            this.cache[n] = {
              dtype: parseDtype(dtype),
              safeDtype: parseDtype(toSafeDtype(dtype)),
              shape,
              elementSize,
              raw: undefined,
              ndarray: undefined,
              nbItems: 0,
            };
          }
        }
      }
    });

    const promises = request.channelNames.map(async (name) => {
      const channelStore = this.cache[name];
      if (!channelStore || !channelStore.safeDtype) {
        newErrors.push(`channel name '${name}' in not part of the scan`);
        return;
      }
      if (request.stop <= request.start) {
        // no data requested
        channelStore.ndarray = undefined;
        return;
      }
      // FIXME: For now read anyway the data from absolute offset 0
      const start = channelStore.nbItems ?? 0;
      if (request.stop <= start) {
        // data already there
        const array = sliceIfNeeded(
          channelStore.raw as TypedArray,
          request.start,
          request.stop
        );
        channelStore.ndarray = ndarray(array, [
          request.stop - request.start,
          ...channelStore.shape,
        ]);
        return;
      }
      const fetchedArray = await RestService.get(
        `/scans/data/binary/${this.scanId}`,
        {
          channel: name,
          selection: `${start}:${request.stop}`,
          dtype: 'safe', // FIXME: The problem here is we can't guess the resulting type
        },
        true,
        'arraybuffer'
      )
        .then((response) => response.data)
        .catch((error) => {
          const message =
            error instanceof Error ? error.message : String(error);
          newErrors.push(`Error while fetching '${name}': ${message}`);
          return undefined;
        });
      if (fetchedArray === undefined) {
        return;
      }
      // NOTE: the retrived data can be smaller than what we request
      const newArray = toTypedArray(fetchedArray, channelStore.safeDtype.numpy);
      const raw = concateArrays(channelStore.raw, newArray);
      channelStore.raw = raw;
      channelStore.nbItems = raw.length / channelStore.elementSize;
      if (channelStore.nbItems <= request.start) {
        channelStore.ndarray = undefined;
        return;
      }
      const array = sliceIfNeeded(
        raw,
        request.start * channelStore.elementSize,
        channelStore.nbItems * channelStore.elementSize
      );
      channelStore.ndarray = ndarray(array, [
        channelStore.nbItems - request.start,
        ...channelStore.shape,
      ]);
    });
    await Promise.all(promises);

    const errors = newErrors.length === 0 ? undefined : newErrors;

    if (this.pendingRequest === undefined) {
      this.updateCallback({
        loading: false,
        data: Object.fromEntries(
          request.channelNames.map((name) => {
            return [name, this.cache[name]?.ndarray];
          })
        ),
        request: resultRequest,
        errors,
      });
      this.isFetching = false;
    } else {
      this.fetchPendingRequest(errors);
    }
  }

  private fetchPendingRequest(previousError?: string[]) {
    if (this.pendingRequest) {
      const r = this.pendingRequest;
      this.pendingRequest = undefined;
      this.fetchData(r, previousError);
    }
  }

  public fetch(request: Request) {
    if (this.isAborted) {
      return;
    }
    if (this.description !== undefined && !this.isFetching) {
      this.isFetching = true;
      this.fetchData(request);
    } else {
      this.pendingRequest = request;
    }
  }

  public abort() {
    this.isAborted = true;
  }
}

/**
 * Service providing data from scans.
 *
 * It returns a map of ndarrays per channels.
 *
 * This provider always start to fetch the data from index 0.
 * As result, it is mostly adapted for 0d channels (scalars in time), but
 * could be memory consuming for 1d or 2d data. This could be improved in
 * the future.
 *
 * A more adapter provider could be created for 1d/2d+ arrays, which could
 * only try to fetch data of a single index in time.
 */
export function useScanData(props: ScanDataRequest): ScanDataResult {
  const [result, setResult] = useState<ScanDataResult>({
    loading: props.scanId !== undefined,
    data: {},
  });
  const { scanId, channelNames, start, stop } = props;

  const fetcher = useMemo(() => {
    if (scanId === undefined) {
      return undefined;
    }
    return new AsyncFetcher(setResult, scanId);
  }, [scanId]);

  useEffect(() => {
    return () => {
      if (fetcher) {
        fetcher.abort();
      }
    };
  }, [fetcher]);

  useEffect(() => {
    if (fetcher && channelNames && start !== undefined && stop !== undefined) {
      fetcher.fetch({ channelNames, start, stop });
    }
  }, [fetcher, JSON.stringify(channelNames), start, stop]);

  return result;
}
