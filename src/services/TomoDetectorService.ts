import type { Dispatch, SetStateAction } from 'react';
import { useState, useEffect } from 'react';
import type { XHRImageData } from 'components/utils/UseXHRImage';
import Qty from 'js-quantities';
import type { TomoDetector } from 'types/Tomo';
import { AutoscaleMode } from 'components/h5web/colormap';
import type {
  ArrayStatistics,
  ArrayHistogram,
} from 'components/h5web/colormap';
import { NdNormalizedUint8Array } from 'components/h5web/NdNormalizedUint8Array';
import { NdFloat16Array } from 'components/h5web/NdFloat16Array';
import debug from 'debug';
import { asFloat32Array, toNdArray, toTypedArray } from 'helpers/ndarray';
import RestService from 'services/RestService';
import type { AxiosResponse } from 'axios';
import type { ColorMap } from '@h5web/lib';
import { ScaleType } from '@h5web/lib';
import { IffReader } from 'helpers/iff';
import ops from 'ndarray-ops';
import type { NdArray, TypedArray } from 'ndarray';
import ndarray from 'ndarray';

const logger = debug('daiquiri.services.TomoDetectorService');

/**
 * Available profile for data compression
 *
 * NOTE: This strings match the server side code
 */
export enum ImageProfile {
  Raw = 'raw',
  F16 = 'f16',
  U8 = 'u8',
}

/**
 * Available data processing
 *
 * NOTE: This strings match the server side code
 */
export enum TomoProcess {
  Dark = 'dark',
  Flat = 'flat',
  Proj = 'proj',
  Flatfield = 'flatfield',
}

export enum TomoDataKind {
  Unknown = 'unknown',
  Dark = 'dark',
  Flat = 'flat',
  Proj = 'proj',
  ProjNorm = 'proj_norm',
  ProjDarkNorm = 'proj_dark_norm',
  ProjFlatNorm = 'proj_flat_norm',
  FlatfieldNorm = 'flatfield_norm',
}

/**
 * Received data from tomo detector
 */
export interface TomoDetectorArray {
  detectorSize: {
    width: number;
    height: number;
  };
  array: NdArray<TypedArray> | NdNormalizedUint8Array | NdFloat16Array;
  displayArray: NdArray<Float32Array> | NdNormalizedUint8Array | NdFloat16Array;
  datakind: TomoDataKind;
  sampyPosition: Qty | null;
  syPosition: Qty;
  szPosition: Qty;
  imagePixelSize: Qty | null;
  imagePosition: Qty | null;
  imageSomegaPosition: Qty | null;
  detectorCenterY: Qty | null;
  detectorCenterZ: Qty | null;
  statistics: ArrayStatistics;
  histogram?: ArrayHistogram;
  info?: {
    profile: string;
    byteLength: number;
    dtype: string;
  };
}

const silxParamForLut: { [key in ColorMap]?: string } = {
  Greys: 'gray',
  Viridis: 'viridis',
  Cividis: 'cividis',
  Magma: 'magma',
  Inferno: 'inferno',
  Plasma: 'plasma',
};

const silxParamForScale: Record<ScaleType, string> = {
  [ScaleType.Linear]: 'linear',
  [ScaleType.Log]: 'log',
  [ScaleType.Sqrt]: 'sqrt',
  [ScaleType.Gamma]: 'gamma',
  [ScaleType.SymLog]: 'arcsinh',
};

function getRequestParams(props: {
  detector: TomoDetector;
  lut: ColorMap;
  vmin?: number;
  vmax?: number;
  autoscale: AutoscaleMode;
  normalization: ScaleType;
  process: TomoProcess;
  histogram?: boolean;
  encoding: 'img' | 'bin' | 'iff';
  profiles?: ImageProfile[];
}): Record<string, string | undefined> | null {
  if (!props.detector) {
    return null;
  }

  const { flat, dark, data } = props.detector;

  // FIXME: The hash is a trick to enforce update of the react URL state
  const scanid = data ? data.scanid : 0;
  const frameid = data ? data.frame_no : 0;
  const hash =
    (dark !== undefined ? dark + 1 : 0) +
    (flat !== undefined ? flat + 1 : 0) +
    scanid +
    frameid;

  const profiles = props.profiles?.map((p) => p.toString()).join(';');

  function scale() {
    if (props.vmin && props.vmax) {
      return {
        vmin: props.vmin.toString(),
        vmax: props.vmax.toString(),
      };
    }
    if (props.autoscale === AutoscaleMode.None) {
      return {};
    }
    return { autoscale: props.autoscale.toString() };
  }

  return {
    image_no: hash.toString(),
    norm: silxParamForScale[props.normalization],
    lut: silxParamForLut[props.lut] ?? 'gray',
    detectorid: props.detector.detector_id,
    process: props.process.toString(),
    encoding: props.encoding,
    histogram: props.histogram ? 'true' : undefined,
    profiles,
    ...scale(),
  };
}

function readHeaderAsQuantity<R>(
  xhrImage: XHRImageData | AxiosResponse<any>,
  key: string,
  defaultValue: R | Qty
): R | Qty {
  const string = xhrImage.headers[key];
  if (string) {
    return new Qty(string);
  }
  return defaultValue;
}

function readHeaderAsShape(
  xhrImage: XHRImageData | AxiosResponse<any>,
  key: string
): number[] {
  const line = xhrImage.headers[key];
  const elements = line.split(' ');
  const result: number[] = elements.map((e: string) => Number.parseInt(e));
  return result;
}

function readHeaderAsNumber(
  xhrImage: XHRImageData | AxiosResponse<any>,
  key: string
): number {
  return Number.parseFloat(xhrImage.headers[key]);
}

function readSampleStageMeta(xhrImage: XHRImageData | AxiosResponse<any>) {
  const pixelSize = readHeaderAsQuantity(
    xhrImage,
    'dqr-pixelsize',
    new Qty('1 px')
  );
  const sy = readHeaderAsQuantity(xhrImage, 'dqr-sy', new Qty('0 mm'));
  const sz = readHeaderAsQuantity(xhrImage, 'dqr-sz', new Qty('0 mm'));
  const sampy = readHeaderAsQuantity(xhrImage, 'dqr-sampy', new Qty('0 mm'));
  const somega = readHeaderAsQuantity<null>(xhrImage, 'dqr-somega', null);
  const detcy = readHeaderAsQuantity<null>(xhrImage, 'dqr-detcy', null);
  const detcz = readHeaderAsQuantity<null>(xhrImage, 'dqr-detcz', null);
  const position = sy.add(sampy);
  return {
    imagePixelSize: pixelSize,
    sampyPosition: sampy,
    syPosition: sy,
    szPosition: sz,
    imagePosition: position,
    imageSomegaPosition: somega,
    detectorCenterY: detcy,
    detectorCenterZ: detcz,
  };
}

/**
 * Decode a response from an 'bin' REST request.
 */
function decodeBinResponse(response: AxiosResponse<any>): TomoDetectorArray {
  const sampleStage = readSampleStageMeta(response);
  const dtype = response.headers['dqr-dtype'];
  const shape = readHeaderAsShape(response, 'dqr-shape');
  const min = readHeaderAsNumber(response, 'dqr-min');
  const max = readHeaderAsNumber(response, 'dqr-max');
  const mean = readHeaderAsNumber(response, 'dqr-mean');
  const std = readHeaderAsNumber(response, 'dqr-std');
  const datakind = response.headers['dqr-datakind'] ?? TomoDataKind.Unknown;
  const array = toNdArray(response.data, shape, dtype);
  const displayArray = asFloat32Array(array);
  // FIXME: minPositive have to be passed by the provider
  return {
    ...sampleStage,
    detectorSize: { height: shape[0], width: shape[1] },
    array,
    displayArray,
    datakind,
    statistics: { min, max, mean, std, minPositive: null },
  };
}

/**
 * Decode a response from an 'iff' REST request.
 */
function decodeIffResponse(response: AxiosResponse<any>): TomoDetectorArray {
  const iff = new IffReader(response.data);

  const header = iff.readChunkType();
  if (header !== 'DQR0') {
    throw new Error(`Unexpected IFF BOM: ${header}`);
  }
  iff.skipChunk();

  let tomo: any = null;
  let form: any = null;
  let data: Uint8Array | null = null;
  let histo: Uint8Array | null = null;

  while (!iff.eof()) {
    const type = iff.readChunkType();
    switch (type) {
      case 'FORM': {
        form = iff.readJsonChunk();
        break;
      }
      case 'DATA': {
        data = iff.readChunk();
        break;
      }
      case 'HST0': {
        histo = iff.readChunk();
        break;
      }
      case 'TOMO': {
        tomo = iff.readJsonChunk();
        break;
      }
      default:
        iff.skipChunk();
    }
  }

  if (data === null) {
    throw new Error('Response does not contain DATA chunk');
  }

  function extractHistogram(): ArrayHistogram | undefined {
    if (histo === null) {
      return undefined;
    }
    const fhisto = toTypedArray(histo, '<f4');
    return {
      values: ndarray(fhisto.subarray(0, 256), [256]),
      bins: ndarray(fhisto.subarray(256, 256 + 257), [257]),
    };
  }

  const histogram = extractHistogram();
  const dtype = form.dtype;
  const shape: number[] = form.shape;
  const profile = form.profile;

  const min = form.min;
  const max = form.max;
  const mean = form.mean;
  const std = form.std;

  function toArray(
    raw: ArrayBuffer | Uint8Array,
    denormalize: boolean
  ):
    | ndarray.NdArray<ndarray.TypedArray>
    | NdNormalizedUint8Array
    | NdFloat16Array {
    if (profile === 'u8') {
      const array = toTypedArray(raw, '|u1');
      if (!denormalize) {
        return new NdNormalizedUint8Array(array as Uint8Array, shape, {
          min: (form.u8_min as number) ?? 0,
          max: (form.u8_max as number) ?? 1,
          normalization: (form.u8_norm as ScaleType) ?? 'linear',
          autoscale: (form.u8_autoscale as AutoscaleMode) ?? 'none',
        });
      }
      const input = ndarray(array, shape);
      const result = ndarray(new Float32Array(shape[0] * shape[1]), shape);
      ops.assign(result, input);
      ops.mulseq(result, max - min);
      ops.divseq(result, 255);
      ops.addseq(result, min);
      return result;
    }
    if (profile === 'f16') {
      const array = toTypedArray(raw, '<u2');
      return new NdFloat16Array(array as Uint16Array, shape);
    }
    if (profile === 'raw') {
      const array = toTypedArray(raw, dtype);
      return ndarray(array, shape);
    }
    throw new Error(`Unsupported profile ${profile}`);
  }

  const array = toArray(data, false);
  const displayArray =
    array instanceof NdNormalizedUint8Array || array instanceof NdFloat16Array
      ? array
      : asFloat32Array(array);

  const byteLength = data.byteLength;

  function readAsQuantity<R>(
    header: Record<string, any>,
    key: string,
    defaultValue: R | Qty
  ): R | Qty {
    const string = header[key];
    if (string) {
      return Qty(string);
    }
    return defaultValue;
  }

  const pixelSize = readAsQuantity(tomo, 'DQR-pixelsize', Qty('1 px'));
  const sy = readAsQuantity(tomo, 'DQR-sy', Qty('0 mm'));
  const sz = readAsQuantity(tomo, 'DQR-sz', Qty('0 mm'));
  const sampy = readAsQuantity(tomo, 'DQR-sampy', Qty('0 mm'));
  const somega = readAsQuantity<null>(tomo, 'DQR-somega', null);
  const detcy = readAsQuantity<null>(tomo, 'DQR-detcy', null);
  const detcz = readAsQuantity<null>(tomo, 'DQR-detcz', null);
  const datakind = tomo['DQR-datakind'] ?? TomoDataKind.Unknown;
  const position = sy.add(sampy);

  return {
    imagePixelSize: pixelSize,
    sampyPosition: sampy,
    syPosition: sy,
    szPosition: sz,
    imagePosition: position,
    imageSomegaPosition: somega,
    detectorCenterY: detcy,
    detectorCenterZ: detcz,
    detectorSize: { height: shape[0], width: shape[1] },
    array,
    displayArray,
    datakind,
    info: {
      profile,
      byteLength,
      dtype,
    },
    // FIXME: minPositive have to be passed by the provider
    statistics: { min, max, mean, std, minPositive: null },
    histogram,
  };
}

const codecs: Record<
  string,
  (response: AxiosResponse<any>) => TomoDetectorArray
> = {
  bin: decodeBinResponse,
  iff: decodeIffResponse,
};

export interface TomoDetectorRequest {
  detector: TomoDetector;
  lut: ColorMap;
  autoscale: AutoscaleMode;
  vmin?: number;
  vmax?: number;
  normalization: ScaleType;
  process: TomoProcess;
  histogram?: boolean;
  profiles?: ImageProfile[];
}

export interface TomoDetectorResult {
  loading: boolean;
  error?: string;
  data: TomoDetectorArray | null;
  requestUpdate: (request: TomoDetectorRequest) => void;
}

class TomoDetectorFetcher {
  private readonly setResult: Dispatch<SetStateAction<TomoDetectorResult>>;

  private isAborted: boolean;

  private isFetching: boolean;

  private pendingRequest: TomoDetectorRequest | undefined;

  public constructor(setResult: Dispatch<SetStateAction<TomoDetectorResult>>) {
    this.setResult = setResult;
    this.setResult({
      loading: false,
      data: null,
      requestUpdate: (request: TomoDetectorRequest) => {
        // closure for this
        this.requestUpdate(request);
      },
    });
    this.isAborted = false;
    this.pendingRequest = undefined;
    this.isFetching = false;
  }

  public requestUpdate(request: TomoDetectorRequest) {
    if (this.isAborted) {
      return;
    }
    if (!this.isFetching) {
      this.isFetching = true;
      this.fetchData(request);
    } else {
      this.pendingRequest = request;
    }
  }

  private fetchPendingRequest() {
    if (this.isAborted) {
      return;
    }
    if (this.pendingRequest) {
      const r = this.pendingRequest;
      this.pendingRequest = undefined;
      this.fetchData(r);
    } else if (this.isFetching) {
      this.isFetching = false;
      this.setResult((prev) => ({
        ...prev,
        loading: false,
      }));
    }
  }

  private async fetchData(request: TomoDetectorRequest) {
    // const encoding = 'bin';
    const encoding = 'iff';
    if (this.isAborted) {
      return;
    }

    this.setResult((prev) => ({
      ...prev,
      loading: true,
    }));

    try {
      let response: AxiosResponse<any> | null;
      logger('Get image from rest API /tomo/data');
      try {
        response = await RestService.get(
          '/tomo/data',
          getRequestParams({ ...request, encoding }),
          true,
          'arraybuffer'
        );
      } catch (error: any) {
        if (this.isAborted) {
          return;
        }
        if (error.response.status === 404) {
          response = null;
        } else {
          const message =
            error instanceof Error ? error.message : String(error);
          this.setResult((prev) => ({
            ...prev,
            data: null,
            error: `Error while fetching data: ${message}`,
          }));
          return;
        }
      }

      if (this.isAborted) {
        return;
      }

      try {
        if (!response) {
          throw new Error(`No ${request.process} yet available`);
        }
        if (!(encoding in codecs)) {
          throw new Error(`Unsupported response encoding '${encoding}'`);
        }
        const data = codecs[encoding](response);
        this.setResult((prev) => ({
          ...prev,
          data,
          error: undefined,
        }));
      } catch (error) {
        const message = error instanceof Error ? error.message : String(error);
        this.setResult((prev) => ({
          ...prev,
          data: null,
          error: message,
        }));
      }
    } finally {
      this.fetchPendingRequest();
    }
  }

  public abort() {
    this.isAborted = true;
  }
}

/**
 * Service exposing a single detector data at a time.
 */
export function useTomoDetectorService(): TomoDetectorResult {
  const [result, setResult] = useState<TomoDetectorResult>({
    loading: false,
    data: null,
    requestUpdate: () => {},
  });

  useEffect(() => {
    const fetcher = new TomoDetectorFetcher(setResult);
    return () => {
      fetcher.abort();
    };
  }, []);

  return result;
}
