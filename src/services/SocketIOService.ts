import type { Socket } from 'socket.io-client';
import { Manager } from 'socket.io-client';
import { each } from 'lodash';

class SocketIOService {
  private sockets: Record<any, Socket> = {};
  private onUnauthorisedFired = false;
  private baseUrl: any;
  private onConnectErrorCB: any; // unused should be removed
  private onUnauthorisedCB: any;
  private manager: Manager | null = null;
  private token = '';

  public setup(options: {
    baseUrl: string;
    onNetworkOk?: () => void;
    onNetworkError?: () => void;
    onUnauthorised?: (message: string) => void;
    onConnectError?: () => void;
  }) {
    this.baseUrl = options.baseUrl.replace('/api', '');
    this.onConnectErrorCB = options.onConnectError;
    this.onUnauthorisedCB = options.onUnauthorised;
    this.manager = new Manager(this.baseUrl, {
      query: { token: this.token },
      autoConnect: false,
      // reconnection: false,
      reconnectionDelayMax: 30_000,
    });
  }

  public setToken(token: any) {
    this.token = token;
    // TODO: Manager not initialised during tests
    if (this.manager) {
      // @ts-expect-error This `disconnect` function is supposed to be private
      this.manager.disconnect();
      this.manager.opts.query = { token };
    }
    each(this.sockets, (socket) => {
      socket.connect();
    });
  }
  public disconnect() {
    each(this.sockets, (socket) => {
      socket.disconnect();
    });
  }

  public addNamespace(namespace: string) {
    if (namespace in this.sockets) return;
    if (!this.manager) {
      throw new Error('SocketIOService not yet setup');
    }

    const socket = this.manager.socket(`/${namespace}`);
    socket.on('connect', this.onConnect.bind(this, namespace));
    socket.on('disconnect', this.onDisconnect.bind(this, namespace));
    socket.on('connect_error', this.onConnectError.bind(this, namespace));

    this.sockets[namespace] = socket;

    if (this.token) {
      this.setToken(this.token);
    }
  }

  public addCallback(
    namespace: string,
    event: string,
    callback: (data: any) => void
  ) {
    if (!(namespace in this.sockets)) {
      throw new Error(`No socket for namespace ${namespace}`);
    }
    this.sockets[namespace].on(event, callback);
  }

  public emit(
    namespace: string,
    message: any,
    payload: any,
    cb: ((response: any) => void) | null = null
  ) {
    if (!(namespace in this.sockets)) {
      throw new Error(`No socket for namespace ${namespace}`);
    }

    return new Promise((resolve) => {
      const interceptCB = (resp: any) => {
        resolve(resp);
        if (cb) cb(resp);
      };
      this.sockets[namespace].emit(message, payload, interceptCB);
    });
  }

  private onConnect(namespace: string) {
    // console.log('onConnect', namespace);
  }

  private onDisconnect(namespace: string) {
    // console.log('onDisconnect', namespace);
    this.onUnauthorisedFired = false;
  }

  private onConnectError(namespace: string, err: Error) {
    // console.log('onConnectError', namespace, err.message);
    if (
      !this.onUnauthorisedFired &&
      this.onUnauthorisedCB &&
      // @ts-expect-error `description` is not supposed to be there
      (err.description === 401 || err.message?.includes('Invalid Session'))
    ) {
      this.onUnauthorisedCB();
      this.onUnauthorisedFired = true;
    }
  }
}

export default new SocketIOService();
