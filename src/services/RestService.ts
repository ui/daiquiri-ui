import type { ResponseType } from 'axios';
import axios from 'axios';
import qs from 'qs';

class RestService {
  private _baseUrl: string | undefined;
  public token = '';

  public setup(options: {
    baseUrl: string;
    onNetworkOk?: () => void;
    onNetworkError?: () => void;
    onUnauthorised?: (message: string) => void;
  }) {
    this._baseUrl = options.baseUrl;

    axios.interceptors.response.use(
      (response) => {
        if (options.onNetworkOk) {
          options.onNetworkOk();
        }

        return response;
      },
      (error) => {
        if (!error.response) {
          console.log('RestService addAlert', error);
          if (options.onNetworkError) {
            options.onNetworkError();
          }

          return Promise.reject(error);
        }

        console.log('RestService', /* error.response, */ error);

        if (error.response.status === 401 && options.onUnauthorised) {
          options.onUnauthorised(error.response.data?.error);
        }
        return Promise.reject(error);
      }
    );
  }

  public setToken(token: any) {
    this.token = token;
  }

  public getOptions(): { headers?: Record<string, string> } {
    const options: { headers?: Record<string, string> } = {};
    if (this.token) {
      options.headers = { Authorization: `Bearer ${this.token}` };
    }
    return options;
  }

  public get(
    endpoint: string,
    params?: any,
    withBaseUrl = true,
    responseType: ResponseType | undefined = undefined
  ) {
    const opts = {
      ...this.getOptions(),
      paramsSerializer: (ps: any) =>
        qs.stringify(ps, { arrayFormat: 'repeat' }),
      params,
      responseType,
    };

    return axios
      .get((withBaseUrl ? this._baseUrl : '') + endpoint, opts)
      .then((response) => response);
  }

  public post(endpoint: string, payload: any) {
    return axios
      .post(this._baseUrl + endpoint, payload, this.getOptions())
      .then((response) => response);
  }

  public put(endpoint: string, payload: any) {
    return axios
      .put(this._baseUrl + endpoint, payload, this.getOptions())
      .then((response) => response);
  }

  public patch(endpoint: string, payload: any) {
    return axios
      .patch(this._baseUrl + endpoint, payload, this.getOptions())
      .then((response) => response);
  }

  public delete(endpoint: string) {
    return axios
      .delete(this._baseUrl + endpoint, this.getOptions())
      .then((response) => response);
  }
}

export default new RestService();
