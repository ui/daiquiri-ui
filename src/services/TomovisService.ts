import type { AxiosInstance } from 'axios';
import debug from 'debug';
import { useState, useCallback, useEffect } from 'react';
import { isEqual } from 'lodash';

const logger = debug('daiquiri.services.TomovisService');

export type ImageTilingId = string;

/**
 * Image tiling item as exposed by the tomovis Rest service
 */
export interface ImageTilingItem {
  id: ImageTilingId;
  status: 'done' | string; // eslint-disable-line @typescript-eslint/no-redundant-type-constituents
  create_time: string;
}

type TomovisServiceStates = 'UNKNOWN' | 'READY' | 'FAILED' | 'PROCESSING';

export interface TomovisService {
  // List of available image tiling as exposed by the service
  imageTilings: ImageTilingItem[];

  // Returns the last ready to use image tiling, if one
  lastReadyImageTiling: ImageTilingId | null;

  // Request to update the list of image tiling
  updateImageTilings: () => void;

  // Request to update the list of image tiling
  deleteImageTiling: (id: ImageTilingId) => void;

  // If true the tomovis service is avaiable
  state: TomovisServiceStates;
}

/**
 * Service to synchronize the available tomovis tiling ids.
 *
 * A `lastReadyImageTiling` is exposed. It is the last image tiling which is
 * ready to be displayed. It is computed from the `lastKnownImageTiling`
 * provided by Daiquiri when creating the request; plus a pulling of the tomovis
 * service to check when the id is really available.
 *
 * FIXME: This have to be moved inside a redux provider
 */
export function useTomovisService(
  tomovisRestService: AxiosInstance | undefined,
  lastKnownImageTiling: ImageTilingId | null,
  refreshPeriod: number
): TomovisService {
  const [imageTilings, setIds] = useState<ImageTilingItem[]>([]);
  const [lastReadyImageTiling, setLastReadyImageTiling] =
    useState<ImageTilingId | null>(null);
  const [state, setState] = useState<TomovisServiceStates>('UNKNOWN');

  const updateState = useCallback(
    (imageTilings: ImageTilingItem[]) => {
      let notDone = 0;
      imageTilings.forEach((imageTiling) => {
        if (imageTiling.status !== 'done') {
          notDone += 1;
        }
        if (
          imageTiling.id === lastKnownImageTiling &&
          (imageTiling.status === 'done' || imageTiling.status === 'running')
        ) {
          setLastReadyImageTiling(lastKnownImageTiling);
        }
      });
      setState(notDone ? 'PROCESSING' : 'READY');
    },
    [lastKnownImageTiling, lastReadyImageTiling, setState]
  );

  const updateImageTilings = useCallback(() => {
    if (!tomovisRestService) {
      return;
    }
    if (tomovisRestService.defaults.baseURL === 'mock') {
      setIds([{ id: 'chessboard', status: 'done', create_time: '1871-03-18' }]);
    }

    tomovisRestService
      .get('/image_tiling/')
      .then((response) => {
        const list: ImageTilingItem[] | undefined = response.data.items;
        if (list === undefined) {
          throw new Error('No items field received.');
        }
        updateState(list);
        setIds((prev) => {
          return isEqual(prev, list) ? prev : list;
        });
      })
      .catch((error) => {
        setState('FAILED');
        console.error('Error while reading available tomovis tiling', error);
      });
  }, [
    tomovisRestService,
    lastReadyImageTiling,
    lastKnownImageTiling,
    updateState,
    setState,
  ]);

  useEffect(() => {
    // Very first refresh at mounting
    updateImageTilings();
  }, [updateImageTilings]);

  const deleteImageTiling = useCallback(
    (imageTilingId: string) => {
      if (!tomovisRestService) {
        return;
      }
      tomovisRestService
        .delete(`/image_tiling/${imageTilingId}`)
        .then((response) => {
          const newIds = imageTilings.filter((imageTiling) => {
            return imageTiling.id !== imageTilingId;
          });
          setIds(newIds);
          return null;
        })
        .catch((error) => {
          console.error(
            'Error while requesting tiling deletion: "%s"',
            imageTilingId,
            error
          );
        });
    },
    [tomovisRestService, imageTilings]
  );

  const [actualRefreshPeriod, setActualRefreshPeriod] = useState(refreshPeriod);

  useEffect(() => {
    const interval = setInterval(() => {
      updateImageTilings();
    }, actualRefreshPeriod);
    return () => clearInterval(interval);
  }, [updateImageTilings, actualRefreshPeriod]);

  useEffect(() => {
    if (
      (lastKnownImageTiling && lastKnownImageTiling !== lastReadyImageTiling) ||
      state === 'PROCESSING'
    ) {
      if (refreshPeriod > 2000) {
        logger('Polling every %sms', 2000);
        setActualRefreshPeriod(2000);
      }
    } else {
      logger('Polling every %sms', refreshPeriod);
      setActualRefreshPeriod(refreshPeriod);
    }
  }, [lastReadyImageTiling, lastKnownImageTiling, refreshPeriod, state]);

  return {
    imageTilings,
    lastReadyImageTiling,
    updateImageTilings,
    deleteImageTiling,
    state,
  };
}
