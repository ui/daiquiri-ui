import RestService from './RestService';
import type { NdArray, TypedArray } from 'ndarray';
import { useEffect, useState } from 'react';
import { toNdArray, toSafeDtype, toTypedArray } from '../helpers/ndarray';
import Qty from 'js-quantities';
import { NdFloat16Array } from '../components/h5web/NdFloat16Array';

/**
 * Group as it is exposed by the h5grove provider
 */
interface H5groveRawMeta {
  attributes: H5groveAttribute[];
  children: (H5groveRawDataset | H5groveRawGroup)[];
  name: string;
  type: 'group';
}

/**
 * Attributes as it is exposed by the h5grove provider
 */
interface H5groveAttribute {
  name: string;
  dtype: string;
  shape: number[];
  data: undefined;
}

/**
 * Datasets as it is exposed by the h5grove provider
 */
interface H5groveRawDataset {
  attributes: H5groveAttribute[];
  name: string;
  dtype: string;
  shape: number[];
  type: 'dataset';
}

/**
 * Datasets as it is exposed by the h5grove provider
 */
interface H5groveRawGroup {
  name: string;
  type: 'group';
}

export type H5groveData = undefined | NdArray<TypedArray> | NdFloat16Array;

export interface H5groveAttr {
  data: H5groveData;
  name: string;
  dtype: string;
  shape: number[];
}

export type H5groveAttrs = Record<string, H5groveAttr>;

export interface H5groveGroup {
  attrs: H5groveAttrs;
  groups: Record<string, H5groveGroup>;
  datasets: Record<string, H5groveDataset>;
  name: string;
}

export interface H5groveDataset {
  attrs: H5groveAttrs;
  data: H5groveData;
  name: string;
  dtype: string;
  shape: number[];
}

interface Request {
  datacollectionid: number | undefined;
  autoprocprogramid?: number | undefined;
  autoprocprogramattachmentid?: number;
  path: string;
  type?: string;
}

class Aborted extends Error {}

class AsyncFetcher {
  private isAborted: boolean;

  private request: Request | undefined;

  private readonly supportsFloat16Array: boolean;

  private readonly error: string | undefined;

  private readonly updateCallback: React.Dispatch<
    React.SetStateAction<H5groveResult>
  >;

  public constructor(
    updateCallback: React.Dispatch<React.SetStateAction<H5groveResult>>,
    supportsFloat16Array: boolean
  ) {
    this.isAborted = false;
    this.request = undefined;
    this.updateCallback = updateCallback;
    this.supportsFloat16Array = supportsFloat16Array;
  }

  private async fetchGroup(path: string): Promise<H5groveGroup> {
    const res = await RestService.get('/h5grove/meta/', {
      ...this.request,
      path,
    });
    // the task was cancelled
    if (this.isAborted) {
      throw new Aborted();
    }
    const meta: H5groveRawMeta = res.data as H5groveRawMeta;
    if (meta.type !== 'group') {
      throw new Error(`The path '${path}' is not a group`);
    }

    let attrs: H5groveAttrs | undefined;
    const datasets: Record<string, H5groveDataset> = {};
    const groups: Record<string, H5groveGroup> = {};

    const fetchAttrs = async () => {
      attrs = await this.fetchAttributes(path, meta.attributes);
    };

    const promises = meta.children.map(async (c) => {
      if (c.type === 'group') {
        groups[c.name] = await this.fetchGroup(`${path}/${c.name}`);
      } else {
        if (c.dtype === '|O') {
          datasets[c.name] = await this.fetchObjDataset(`${path}/${c.name}`, c);
        } else {
          datasets[c.name] = await this.fetchDataset(`${path}/${c.name}`, c);
        }
      }
    });
    await Promise.all([...promises, fetchAttrs()]);
    for (const c of meta.children) {
      if (this.isAborted) {
        throw new Aborted();
      }
    }
    if (attrs === undefined) {
      throw new Error(`Attributes from path '${path}' was not fetched`);
    }
    return { attrs, datasets, groups, name: meta.name };
  }

  private async fetchObjDataset(
    path: string,
    node: H5groveRawDataset
  ): Promise<H5groveDataset> {
    const attrs = await this.fetchAttributes(path, node.attributes);
    const res = await RestService.get('/h5grove/data/', {
      ...this.request,
      path,
      format: 'json',
    })
      .then((response) => response.data)
      .catch((error) => undefined);
    return { ...node, attrs, data: res };
  }

  private async fetchDataset(
    path: string,
    node: H5groveRawDataset
  ): Promise<H5groveDataset> {
    let attrs: H5groveAttrs | undefined;
    let data: NdArray<TypedArray> | NdFloat16Array | undefined;

    const fetchAttrs = async () => {
      attrs = await this.fetchAttributes(path, node.attributes);
    };

    const readAsFloat16 = this.supportsFloat16Array && node.dtype === '<f2';
    const fetchData = async () => {
      const response = await RestService.get(
        '/h5grove/data/',
        {
          ...this.request,
          path,
          format: 'bin',
          dtype: readAsFloat16 ? 'origin' : 'safe',
        },
        true,
        'arraybuffer'
      )
        .then((response) => response.data)
        .catch((error) => {
          console.error(error);
          return undefined;
        });

      function readData(data: ArrayBuffer) {
        if (!data) {
          return undefined;
        }
        const shape = node.shape.length === 0 ? [1] : node.shape;
        if (readAsFloat16) {
          const array = toTypedArray(data, '<u2');
          return new NdFloat16Array(array as Uint16Array, shape);
        }
        const safeDtype = toSafeDtype(node.dtype);
        return toNdArray(data, shape, safeDtype);
      }

      data = readData(response);
    };

    await Promise.all([fetchData(), fetchAttrs()]);
    if (attrs === undefined) {
      throw new Error(`Attributes from path '${path}' was not fetched`);
    }
    return { ...node, attrs, data };
  }

  private async fetchAttributes(
    path: string,
    attrs: H5groveAttribute[]
  ): Promise<H5groveAttrs> {
    const res = await RestService.get('/h5grove/attr/', {
      ...this.request,
      path,
      attr_keys: attrs.map((a) => a.name),
    });
    const data = res.data;
    const result: H5groveAttrs = {};
    attrs.forEach((a) => {
      result[a.name] = { ...a, data: data[a.name] };
    });
    return result;
  }

  public async fetch(props: { request: Request }) {
    const { request } = props;
    this.isAborted = false;
    this.request = request;
    if (request.datacollectionid === undefined) {
      this.updateCallback({ loading: false });
      return;
    }
    try {
      this.updateCallback({ loading: true });
      const group = await this.fetchGroup(request.path);
      if (this.isAborted) {
        throw new Aborted();
      }
      this.updateCallback({ loading: false, group, request });
    } catch (error) {
      if (error instanceof Aborted) {
        // do nothing
      } else {
        const message = error instanceof Error ? error.message : String(error);
        this.updateCallback({ loading: false, error: message, request });
      }
    }
  }

  public abort() {
    this.isAborted = true;
  }
}

export interface H5groveResult {
  loading: boolean;
  error?: string;
  request?: Request;
  group?: H5groveGroup;
}

export function useHdf5Tree(props: {
  datacollectionid: number | undefined;
  autoprocprogramid?: number | undefined;
  autoprocprogramattachmentid?: number;
  path: string;
  type?: string;
  supportsFloat16Array?: boolean;
}): H5groveResult {
  const [result, setResult] = useState<H5groveResult>({ loading: false });

  useEffect(() => {
    const fetcher = new AsyncFetcher(
      setResult,
      props.supportsFloat16Array ?? false
    );
    fetcher.fetch({
      request: {
        datacollectionid: props.datacollectionid,
        autoprocprogramid: props.autoprocprogramid,
        autoprocprogramattachmentid: props.autoprocprogramattachmentid,
        path: props.path,
        type: props.type,
      },
    });
    return () => {
      fetcher.abort();
    };
  }, [
    props.datacollectionid,
    props.autoprocprogramid,
    props.autoprocprogramattachmentid,
    props.path,
    props.type,
  ]);

  return result;
}

/**
 * Read an attribute as a string list, else null
 *
 * A scalar string will be returned as a list of a single element.
 */
export function readAsStringList(
  attr: H5groveAttr | undefined
): string[] | null {
  if (attr === undefined) {
    return null;
  }
  if (attr.dtype !== '|O') {
    return null;
  }
  const data = attr.data;
  if (typeof data === 'string') {
    return [data];
  }
  // @ts-expect-error
  return attr;
}

/**
 * Read an attribute as a string, else null
 */
export function readAsString(attr: H5groveAttr | undefined): string | null {
  if (attr === undefined) {
    return null;
  }
  if (attr.dtype !== '|O') {
    return null;
  }
  const data = attr.data;
  if (typeof data !== 'string') {
    return null;
  }
  return data;
}

/**
 * Read a dataset as a number, else null
 */
export function readAsNumber(
  dataset: H5groveDataset | undefined
): number | null {
  if (!dataset) {
    return null;
  }
  const value = dataset.data?.get(0);
  if (value === undefined) {
    return null;
  }
  return value;
}

/**
 * Read a dataset as quantity, else null
 */
export function readAsScalarQuantity(
  dataset: H5groveDataset | undefined
): Qty | null {
  if (!dataset) {
    return null;
  }
  const value = dataset.data?.get(0);
  if (value === undefined) {
    return null;
  }
  const unit = readAsString(dataset.attrs.units);
  if (unit === null) {
    return null;
  }
  return new Qty(value, unit);
}
