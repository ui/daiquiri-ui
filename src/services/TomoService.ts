import type Qty from 'js-quantities';
import baseUrl from '../helpers/baseUrl';
import XHRImage from '../helpers/XHRImage';

/**
 * Send a move request to the tomo service.
 *
 * The defined motors will move to defined absolute position.
 *
 * FIXME: It would be good to rework this implementation with a proper
 * rest service
 */
export function requestMoveSampleStage(props: {
  sz?: Qty;
  sy?: Qty;
  sampx?: Qty;
  sampy?: Qty;
  sampu?: Qty;
  sampv?: Qty;
}) {
  const {
    sz = null,
    sy = null,
    sampx = null,
    sampy = null,
    sampu = null,
    sampv = null,
  } = props;
  const url = new URL(`${baseUrl()}/tomo/move`);
  if (sy) {
    url.searchParams.append('sy', sy.toString());
  }
  if (sz) {
    url.searchParams.append('sz', sz.toString());
  }
  if (sampx) {
    url.searchParams.append('sampx', sampx.toString());
  }
  if (sampy) {
    url.searchParams.append('sampy', sampy.toString());
  }
  if (sampu) {
    url.searchParams.append('sampu', sampu.toString());
  }
  if (sampv) {
    url.searchParams.append('sampv', sampv.toString());
  }
  url.searchParams.append('relative', 'false');

  const xhr = new XMLHttpRequest();
  xhr.open('GET', url.href, true);
  if (XHRImage.token) {
    xhr.setRequestHeader('Authorization', `Bearer ${XHRImage.token}`);
  }

  // xhr.onload
  // xhr.onprogress
  // xhr.onloadstart
  // xhr.onloadend
  // xhr.onerror
  xhr.send();
}

/**
 * Send a request to process a slice reconstruction.
 */
export function requestSliceReconstruction(props: {
  datacollectionid: number;
  axisposition?: number;
  deltabeta?: number;
}) {
  const { datacollectionid, axisposition, deltabeta } = props;
  if (datacollectionid === undefined) {
    return;
  }
  const url = new URL(`${baseUrl()}/tomo/slice_reconstruction`);
  if (datacollectionid !== undefined) {
    url.searchParams.append('datacollectionid', datacollectionid.toString());
  }
  if (axisposition !== undefined) {
    url.searchParams.append('axisposition', axisposition.toString());
  }
  if (deltabeta !== undefined) {
    url.searchParams.append('deltabeta', deltabeta.toString());
  }

  const xhr = new XMLHttpRequest();
  xhr.open('GET', url.href, true);
  if (XHRImage.token) {
    xhr.setRequestHeader('Authorization', `Bearer ${XHRImage.token}`);
  }

  // xhr.onload
  // xhr.onprogress
  // xhr.onloadstart
  // xhr.onloadend
  // xhr.onerror
  xhr.send();
}
