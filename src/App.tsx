import { BrowserRouter, Route, Routes, Outlet } from 'react-router-dom';
import type { Store } from 'redux';
import { connect } from 'react-redux';
import CookieConsent from 'react-cookie-consent';

// The order of these matter re circular imports
import Loader from 'components/Loader';
import ServerWarning from 'components/ServerWarning';
import ErrorBoundary from 'components/ErrorBoundary';

import Main from 'connect/main/Main';
import Header from 'connect/header/Header';
import PrivateRoute from 'connect/utils/PrivateRoute';
import Login from 'connect/Login';
import Toasts from 'connect/Toasts';
import SessionMirror from 'connect/SessionMirror';
import LoginRoute from './connect/utils/LoginRoute';

import app from 'providers/app';

function NoMatch() {
  return (
    <Loader>
      <p>404 Page Not Found</p>
    </Loader>
  );
}

interface Props {
  ready: boolean;
  networkError: boolean;
}

function App(props: Props) {
  return (
    <div className="app">
      {props.ready && (
        <SessionMirror>
          <BrowserRouter>
            <Header />
            <Toasts />
            <ErrorBoundary>
              <Routes>
                <Route
                  path="login"
                  element={
                    <LoginRoute>
                      <Login />
                    </LoginRoute>
                  }
                />

                <Route
                  path="/"
                  element={
                    <PrivateRoute>
                      <Outlet />
                    </PrivateRoute>
                  }
                >
                  <Route index element={<Main />} />
                  <Route path=":layout" element={<Main />} />
                </Route>

                <Route path="*" element={<NoMatch />} />
              </Routes>
            </ErrorBoundary>
          </BrowserRouter>
        </SessionMirror>
      )}

      {!props.ready && (
        <>
          <div className="network-error">
            <ServerWarning show={props.networkError} />
          </div>
          <Loader />
        </>
      )}
      <CookieConsent
        disableStyles
        containerClasses="cookie-consent"
        buttonClasses="confirm-button"
        contentClasses="content"
      >
        This website uses cookies to enhance the user experience.
        <p className="small mb-0">
          We store your username in order to identify you.
        </p>
      </CookieConsent>
    </div>
  );
}

function mapStateToProps(state: Store): Props {
  return {
    ready: app.selector('ready', state),
    networkError: app.selector('networkError', state),
  };
}

export default connect(mapStateToProps)(App);
