import config from 'config/config';

function baseUrl() {
  const { location } = window;
  const port = location.port ? `:${location.port}` : '';
  const windowBase = `${location.protocol}//${location.hostname}${port}`;

  return config.baseUrl === '/api' ? `${windowBase}/api` : config.baseUrl;
}

export default baseUrl;
