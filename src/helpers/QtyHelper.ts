import type Qty from 'js-quantities';

const PREFIX: Record<string, number> = {
  '<tera>': 12,
  '<giga>': 9,
  '<mega>': 6,
  '<kilo>': 3,
  '<1>': 0,
  '<milli>': -3,
  '<micro>': -6,
  '<nano>': -9,
  '<pico>': -12,
};

const PLACES_TO_PREFIX: Record<number, string> = {};

Object.keys(PREFIX).forEach((key) => {
  PLACES_TO_PREFIX[PREFIX[key]] = key;
});

/**
 * Returns a quantity with a unit inside a readable range of values
 *
 * The prefix of the unit (n/u/k/M) is picked to make the value
 * in the range 0.1 .. 999
 */
export function toHumanReadableUnit(quantity: Qty) {
  if (quantity.scalar === 0) {
    return quantity;
  }

  function getNextDeltaPlace(q: Qty) {
    const v = q.scalar;
    if (v > 200) {
      return 3;
    }
    if (v < 0.3) {
      return -3;
    }
    return 0;
  }

  function getUnitWithNewPrefix(q: Qty, prefix: string) {
    function cleanup(elemString: string | undefined): string {
      if (elemString === undefined) return '';
      if (elemString === '<1>') return '';
      return elemString.slice(1, -1);
    }
    let n = '';
    for (let ni = 0; ni < q.numerator.length; ni += 1) {
      const elem = q.numerator[ni];
      if (ni === 0) {
        const notPrefix = PREFIX[elem] === undefined;
        n += cleanup(prefix) + (notPrefix ? cleanup(elem) : '');
      } else {
        n += cleanup(elem);
      }
    }
    let d = '';
    for (const elem of q.denominator) {
      d += cleanup(elem);
    }
    if (d === '') {
      return n;
    }
    if (n === '') {
      return `1/${d}`;
    }
    return `${n}/${d}`;
  }

  function getCurrentPlace(q: Qty) {
    return PREFIX[q.numerator[0]] || 0;
  }

  function shiftUnit(q: Qty, deltaPlace: number) {
    const place = getCurrentPlace(q) + deltaPlace;
    const prefixNumerator = PLACES_TO_PREFIX[place];
    if (prefixNumerator === undefined) {
      return q;
    }
    const unit = getUnitWithNewPrefix(quantity, prefixNumerator);
    return q.to(unit);
  }

  let current = quantity;
  for (let i = 0; i < 10; i += 1) {
    const deltaPlace = getNextDeltaPlace(current);
    if (deltaPlace === 0) {
      break;
    }
    current = shiftUnit(current, deltaPlace);
  }

  // Check if we loose a bit of digits
  const fixed = current.scalar.toFixed(2);
  if (
    fixed.length === 4 &&
    fixed.startsWith('0.') &&
    fixed.slice(3, 4) !== '0'
  ) {
    return shiftUnit(current, -3);
  }
  return current;
}
