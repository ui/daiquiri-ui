import type { Location } from 'react-router-dom';

export function assertLocationObj(
  location: unknown
): asserts location is Location {
  if (!location || typeof location !== 'object' || !('pathname' in location)) {
    throw new Error('Expected location object');
  }
}
