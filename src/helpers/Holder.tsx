import { useEffect, useRef, useState } from 'react';

interface Props {
  width: string | number;
  height: string | number;
  updateOnResize?: boolean;
  text?: string;
  className?: string;
  bg?: string;
}

export default function Holder(props: Props) {
  const { width, height, bg = '#ddd', text, className } = props;
  const parent = useRef<HTMLDivElement>(null);
  const [parentWidthHeight, setParentWidthHeight] = useState<[number, number]>([
    0, 0,
  ]);

  let modifiedWidth = width;
  let modifiedHeight = height;
  if (modifiedWidth === '100p') {
    modifiedWidth = parentWidthHeight[0];
  }
  if (modifiedHeight === '100p') {
    modifiedHeight = parentWidthHeight[1];
  }

  if (typeof modifiedWidth === 'string') {
    modifiedWidth = Number.parseInt(modifiedWidth);
  }
  if (typeof modifiedHeight === 'string') {
    modifiedHeight = Number.parseInt(modifiedHeight);
  }

  useEffect(() => {
    if (parent.current) {
      setParentWidthHeight([
        parent.current.clientWidth,
        parent.current.clientHeight,
      ]);
    }
  }, []);

  return (
    <div ref={parent} className={className}>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width={modifiedWidth}
        height={modifiedHeight}
        aria-label={text}
      >
        <title>{text}</title>
        <rect
          x="0"
          y="0"
          rx="5"
          width={modifiedWidth}
          height={modifiedHeight}
          fill={bg}
        />
        {text && (
          <text
            x={modifiedWidth / 2 - text.length * 10}
            y={modifiedHeight / 2 + 10}
            stroke="#eee"
            fill="#eee"
            style={{
              fontFamily: 'Poppins',
              fontSize: '2rem',
              textTransform: 'uppercase',
            }}
          >
            {text}
          </text>
        )}
      </svg>
    </div>
  );
}
