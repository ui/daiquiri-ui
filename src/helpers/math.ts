/**
 * Smallest angular oriented distance between 'a' and 'b', in degree
 */
export function angularDist(a: number, b: number): number {
  const delta = ((b - a) * Math.PI) / 180;
  return (Math.atan2(Math.sin(delta), Math.cos(delta)) * 180) / Math.PI;
}
