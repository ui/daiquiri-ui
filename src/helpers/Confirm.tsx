import type { MouseEvent, ReactChild, RefObject } from 'react';
import { Component, createRef } from 'react';

import { Button } from 'react-bootstrap';

import ModalDialog from 'components/layout/ModalDialog';

type ConfirmCallback = (
  eventhandler: (event: MouseEvent) => void
) => (event: MouseEvent) => void;

interface Props {
  title: string;
  message: string;
  children: (confirm: ConfirmCallback) => ReactChild | ReactChild[];
}

interface State {
  open: boolean;
  callback: null | (() => void);
}

/**
 * https://itnext.io/add-confirmation-dialog-to-react-events-f50a40d9a30d
 */
export default class Confirm extends Component<Props, State> {
  private readonly targetRef: RefObject<HTMLElement>;

  public constructor(props: Props) {
    super(props);
    this.targetRef = createRef<HTMLElement>();
    this.state = {
      open: false,
      callback: null,
    };
  }

  private readonly show =
    (callback: (event: MouseEvent) => void) => (event: MouseEvent) => {
      event.preventDefault();
      // @ts-expect-error value is not defined inside this specific event
      const value = event.target.value;
      const eventCopy = {
        ...event,
        target: { ...event.target, value },
      };

      this.setState({
        open: true,
        callback: () => callback(eventCopy),
      });
    };

  private readonly hide = () => {
    this.setState({ open: false, callback: null });
  };

  private readonly confirm = () => {
    this.state.callback?.();
    this.hide();
  };

  public render() {
    return (
      <>
        {this.props.children(this.show)}

        <ModalDialog
          title={this.props.title}
          show={this.state.open}
          buttons={
            <Button className="confirm" onClick={this.confirm}>
              OK
            </Button>
          }
          cancelText="Cancel"
          actions={{
            onClose: this.hide,
          }}
          target={this.targetRef}
        >
          <p>{this.props.message}</p>
        </ModalDialog>
      </>
    );
  }
}
