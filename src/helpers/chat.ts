import { filter, map, max } from 'lodash';

export function getUnread(options: {
  messages: any[];
  chatCursor: number;
  sessionid: any;
}) {
  const mids = map(options.messages, 'messageid');
  const lastId = mids.indexOf(options.chatCursor) + 1;
  const newMessages = filter(options.messages.slice(lastId), (m) => {
    return options.sessionid !== m.sessionid;
  });

  const cursor = options.chatCursor || 0;
  return max(mids) > cursor ? newMessages.length : 0;
}

export default getUnread;
