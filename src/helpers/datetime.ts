function prependZero(n: number) {
  if (n < 9) {
    return `0${n}`;
  }
  return n;
}

/**
 * Human readable formatter to display the location of an event in time.
 *
 * - While the even is very close from now, a relative
 *   duration from now is displayed.
 * - Then while the event was occured today/yestrerday/this week, the absolute
 *   hour/minute is displayed. It's convenient to use week of the day because
 *   most of the experiments are done during a week.
 * - Finally the full locale date time is displayed.
 */
export function humanReadableEvent(date: Date): string {
  const now = Date.now();
  const distance = now - date.valueOf();

  if (distance < 50 * 1000) {
    return 'Few seconds ago';
  }
  if (distance < 80 * 1000) {
    return `1 minute ago`;
  }
  if (distance < 90 * 60 * 1000) {
    const minutes = Math.round(distance / (1000 * 60));
    return `${minutes} minutes ago`;
  }
  if (distance < 7 * 24 * 60 * 60 * 1000) {
    const today = new Date();
    if (date.getDate() === today.getDate()) {
      return `Today at ${date.getHours()}:${prependZero(date.getMinutes())}`;
    }
    const yesterday = new Date(today);
    yesterday.setDate(yesterday.getDate() - 1);
    if (date.getDate() === yesterday.getDate()) {
      return `Yesterday at ${date.getHours()}:${prependZero(
        date.getMinutes()
      )}`;
    }
    const monday = new Date(today);
    monday.setDate(monday.getDate() - today.getDay());
    monday.setHours(0);
    monday.setMinutes(0);
    monday.setSeconds(0);
    monday.setMilliseconds(0);
    if (date >= monday) {
      const weekDay = date.toLocaleString('default', { weekday: 'long' });
      return `${weekDay} at ${date.getHours()}:${prependZero(
        date.getMinutes()
      )}`;
    }
  }
  return date.toLocaleDateString();
}
