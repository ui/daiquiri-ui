import Lightbox from 'react-image-lightbox';
import XHRImage from 'helpers/XHRImage';

import 'react-image-lightbox/style.css';

// Override default load method with XHRImage
export default class XHRLightbox extends Lightbox {
  loadImage(srcType, imageSrc, done) {
    if (this.isImageLoaded(imageSrc)) {
      this.setTimeout(() => {
        done();
      }, 1);
      return;
    }

    const inMemoryImage = new XHRImage();

    if (this.props.imageCrossOrigin) {
      inMemoryImage.crossOrigin = this.props.imageCrossOrigin;
    }

    inMemoryImage.onerror = errorEvent => {
      this.props.onImageLoadError(imageSrc, srcType, errorEvent);
      this.setState(prevState => ({
        loadErrorStatus: { ...prevState.loadErrorStatus, [srcType]: true }
      }));

      done(errorEvent);
    };

    inMemoryImage.onload = () => {
      console.log('XHRImage load');
      this.props.onImageLoad(imageSrc, srcType, inMemoryImage);

      this.imageCache[imageSrc] = {
        loaded: true,
        width: inMemoryImage.width,
        height: inMemoryImage.height,
        xhr: inMemoryImage
      };

      done();
    };

    inMemoryImage.load(imageSrc);
  }

  getBestImageForType(srcType) {
    const best = super.getBestImageForType(srcType);

    if (best) {
      const imageSrc = this.props[srcType];
      best.src = this.imageCache[imageSrc].xhr.src;
    }
    return best;
  }
}
