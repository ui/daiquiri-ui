import type { ReactChild } from 'react';

export default function ConditionalWrap<C = JSX.Element>(props: {
  condition: any;
  wrap: (children: C) => C;
  children: C;
}): C {
  return props.condition ? props.wrap(props.children) : props.children;
}
