import PropTypes from 'prop-types';
import { render as rtlRender } from '@testing-library/react';
import tluserEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';

import { providerRegistry } from '@esrf-ui/redux-provider';

import { createStore as appCreateStore } from 'store';

export const createStore = (initialState = {}) => appCreateStore(initialState);

export const setupProviders = (store = appCreateStore()) => {
  providerRegistry.start(store);
};

export const mockClientSizing = () => {
  Object.defineProperty(window.HTMLDivElement.prototype, 'clientHeight', {
    get() {
      return 100;
    },
    enumerable: true,
    configurable: true
  });
  Object.defineProperty(window.HTMLDivElement.prototype, 'clientWidth', {
    get() {
      return 300;
    },
    enumerable: true,
    configurable: true
  });
};

export function render(ui, { store, ...renderOptions } = {}) {
  function Wrapper({ children }) {
    return <Provider store={store}>{children}</Provider>;
  }
  Wrapper.propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]).isRequired
  };
  mockClientSizing();
  return rtlRender(ui, { wrapper: Wrapper, ...renderOptions });
}

export {
  screen,
  within,
  getAllByRole,
  getByRole,
  waitFor
} from '@testing-library/react';
export { tluserEvent as userEvent };

const localStorageMock = (initialData = {}) => {
  let store = initialData;

  return {
    getItem(key) {
      return store[key] || null;
    },
    setItem(key, value) {
      store[key] = value.toString();
    },
    removeItem(key) {
      delete store[key];
    },
    clear() {
      store = {};
    }
  };
};

export const withToken = () => {
  Object.defineProperty(window, 'sessionStorage', {
    value: localStorageMock({ token: 'abc.def.ghi' })
  });
};
