import type { AxiosRequestConfig } from 'axios';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { isFunction } from 'lodash';
import { Formatting } from '@esrf/daiquiri-lib';
import config from 'config/config';
import RestService from 'services/RestService';

export type HttpRequestMethod = 'get' | 'post' | 'patch';

export interface MockResponse {
  url: string;
  method: HttpRequestMethod;
  status: ((body: unknown, request: AxiosRequestConfig) => number) | number;
  noBase?: boolean;
  json?: boolean;
  response:
    | ((
        body: unknown,
        request: AxiosRequestConfig
      ) => Record<string, unknown> | Buffer)
    | ((body: unknown, request: AxiosRequestConfig) => void)
    | Record<string, unknown>
    | Buffer;
  payload?: unknown;
}

export default class MockAxios {
  private readonly mockAxios: MockAdapter;

  public constructor() {
    RestService.setup({ baseUrl: config.baseUrl });
    this.mockAxios = new MockAdapter(axios);
  }

  public register(options: MockResponse) {
    const method = options.method
      ? `on${Formatting.ucfirst(options.method)}`
      : 'onGet';
    // @ts-expect-error
    this.mockAxios[method](
      `${options.noBase ? '' : config.baseUrl}${options.url}`
    ).reply((conf: AxiosRequestConfig) => {
      const body = options.json ? JSON.parse(conf.data) : conf.data;
      // @ts-expect-error
      const resp = options.response(body, conf);
      const status = isFunction(options.status)
        ? options.status(body, conf)
        : options.status;
      return [status, resp];
    });
  }

  public finally() {
    this.mockAxios.onAny().reply(() => {
      return [404, { error: 'File not found ' }];
    });
  }

  public client() {
    // @ts-expect-error
    return this.mockAxios.axiosInstance;
  }
}
