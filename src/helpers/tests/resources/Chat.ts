import ResourceHandler from 'helpers/tests/resources/ResourceHandler';

export default class Chat extends ResourceHandler {
  public baseUrl = '/chat';

  public resources = {
    '': {
      get: {
        status: 200,
        response: () => ({ total: 0, rows: [] }),
      },

      patch: {
        status: 200,
        response: () => ({ maxid: 0 }),
      },
    },
  };
}
