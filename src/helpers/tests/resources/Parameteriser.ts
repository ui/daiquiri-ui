import ResourceHandler from 'helpers/tests/resources/ResourceHandler';

export default class Parameteriser extends ResourceHandler {
  public baseUrl = '/parameteriser';

  public resources = {
    '': {
      get: {
        status: 200,
        response: () => ({
          rows: [
            {
              directory: '/data/bl/tmp/blc00001/parameteriser/sample',
              file_name: 'scircle_test.yml',
              instance_type: 'scircle',
              name: 'test',
              parameter_type: 'sample',
              parameters: {
                center_horizontal: 5,
                center_vertical: 5,
                diameter: 5,
                name: 'test',
                sessionid: 1,
              },
            },
            {
              directory: '/data/bl/tmp/blc00001/parameteriser/sample',
              file_name: 'scircle_test2.yml',
              instance_type: 'scircle',
              name: 'test2',
              parameter_type: 'sample',
              parameters: {
                center_horizontal: 7,
                center_vertical: 7,
                diameter: 7,
                name: 'test2',
                sessionid: 1,
              },
            },
            {
              directory: '/data/bl/tmp/blc00001/parameteriser/sample',
              file_name: 'srect_rect1.yml',
              instance_type: 'srect',
              name: 'rect1',
              parameter_type: 'sample',
              parameters: {
                center_horizontal: 2,
                center_vertical: 2,
                name: 'rect1',
                sessionid: 1,
                size_horizontal: 12,
                size_vertical: 13,
              },
            },
          ],
          total: 3,
        }),
      },
    },
  };
}
