import ResourceHandler from 'helpers/tests/resources/ResourceHandler';

export default class Authenticator extends ResourceHandler {
  public baseUrl = '/authenticator';

  public resources = {
    '/login': {
      post: {
        validUser: 'abcd',
        invalidUser: 'abc',
        json: true,
        status: (payload: any) => {
          return payload.username === 'abcd' ? 200 : 401;
        },
        response: (payload: any) => {
          return payload.username === 'abcd'
            ? {
                data: { username: 'abcd', client: null },
                token: 'abc.def.ghi',
                sessionid: 'a98798-sdsfs-dfgdfg-dfgd434',
                last_access: 1_597_910_881.600_363,
                operator: false,
              }
            : { error: 'Invalid Credentials' };
        },
      },
    },
  };
}
