import type { AxiosRequestConfig } from 'axios';
import { isFunction } from 'lodash';
import type { HttpRequestMethod, MockResponse } from '../MockAxios';
import type MockAxios from '../MockAxios';

export interface Response {
  status: ((payload: unknown, request: AxiosRequestConfig) => number) | number;
  json?: boolean;
  response:
    | ((
        payload: any,
        request: AxiosRequestConfig
      ) => Record<string, unknown> | Buffer | undefined)
    | ((payload: any, request: AxiosRequestConfig) => void)
    | Record<string, unknown>
    | Buffer;
}

export default class ResourceHandler {
  public baseUrl: string | null = null;

  public resources: Record<
    string,
    Partial<Record<HttpRequestMethod, Response>>
  > = {};

  public setup(axios: MockAxios) {
    Object.entries(this.resources).forEach(([url, methods]) => {
      Object.entries(methods).forEach(([method, res]) => {
        axios.register({
          url: `${this.baseUrl}${url}`,
          method: method as HttpRequestMethod,
          ...res,
        });
      });
    });
  }

  public getObject(options: MockResponse): any {
    if (!(options.url in this.resources)) {
      throw new Error(`getObject: No response for url ${options.url}`);
    }

    const resources = this.resources[options.url];
    if (!(options.method in resources)) {
      throw new Error(
        `getObject: No method ${options.method} for url ${options.url}`
      );
    }
    return resources[options.method];
  }

  public getResponse(options: MockResponse) {
    const res = this.getObject(options);
    if (!res) {
      throw new Error(
        `getResponse: No response for method ${options.method} at ${options.url}`
      );
    }
    return res.response(options.payload);
  }

  public getStatus(options: MockResponse) {
    const res = this.getObject(options);
    if (!res) {
      throw new Error(
        `getStatus: No response for method ${options.method} at ${options.url}`
      );
    }
    return isFunction(res.status) ? res.status(options.payload) : res.status;
  }
}
