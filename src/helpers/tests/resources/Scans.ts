import ResourceHandler from 'helpers/tests/resources/ResourceHandler';

import ScanData from 'helpers/tests/resources/mock/ScanData.json';
import ScanSpectra from 'helpers/tests/resources/mock/ScanSpectra.json';

export default class Scans extends ResourceHandler {
  public baseUrl = '/scans';

  public resources = {
    '/status': {
      get: {
        status: 200,
        response: () => ({ progress: 50, scanid: null }),
      },
    },

    '': {
      get: {
        status: 200,
        response: () => ({
          rows: [
            {
              count_time: 1,
              scanid: 3_505_583_049,
              npoints: 11,
              shape: {
                npoints2: null,
                requests: { 'axis:omega': { start: 0, stop: 20, points: 11 } },
                npoints1: null,
                dim: null,
              },
              node_name:
                'test_session:data:bl:tmp:blc00001:sample1:sample1_roi5_8:5_ascan',
              start_timestamp: 1_598_595_844.136_872_3,
              end_timestamp: 1_598_595_859.672_304,
              filename:
                '/data/bl/tmp/blc00001/sample1/sample1_roi5_8/sample1_roi5_8.h5',
              type: 'ascan',
              estimated_time: 0,
              title: 'ascan omega 0 20 10 1',
              status: 'FINISHED',
            },
            {
              count_time: 0.1,
              scanid: 3_932_571_604,
              npoints: 11,
              shape: {
                npoints2: null,
                requests: { 'axis:omega': { start: 0, stop: 20, points: 11 } },
                npoints1: null,
                dim: null,
              },
              node_name:
                'test_session:data:bl:tmp:blc00001:sample1:sample1_roi5_8:4_ascan',
              start_timestamp: 1_598_595_754.729_296_2,
              end_timestamp: 1_598_595_758.632_287,
              filename:
                '/data/bl/tmp/blc00001/sample1/sample1_roi5_8/sample1_roi5_8.h5',
              type: 'ascan',
              estimated_time: 0,
              title: 'ascan omega 0 20 10 0.1',
              status: 'FINISHED',
            },
            {
              count_time: 0.1,
              scanid: 2_312_664_229,
              npoints: 11,
              shape: {
                npoints2: null,
                requests: { 'axis:omega': { start: 0, stop: 20, points: 11 } },
                npoints1: null,
                dim: null,
              },
              node_name:
                'test_session:data:bl:tmp:blc00001:sample1:sample1_roi5_8:3_ascan',
              start_timestamp: 1_598_595_741.008_721_8,
              end_timestamp: 1_598_595_745.070_549,
              filename:
                '/data/bl/tmp/blc00001/sample1/sample1_roi5_8/sample1_roi5_8.h5',
              type: 'ascan',
              estimated_time: 0,
              title: 'ascan omega 0 20 10 0.1',
              status: 'FINISHED',
            },
            {
              count_time: 0.1,
              scanid: 3_619_515_598,
              npoints: 11,
              shape: {
                npoints2: null,
                requests: { 'axis:omega': { start: 0, stop: 20, points: 11 } },
                npoints1: null,
                dim: null,
              },
              node_name:
                'test_session:data:bl:tmp:blc00001:sample1:sample1_roi5_8:2_ascan',
              start_timestamp: 1_598_595_714.767_154,
              end_timestamp: 1_598_595_720.354_833,
              filename:
                '/data/bl/tmp/blc00001/sample1/sample1_roi5_8/sample1_roi5_8.h5',
              type: 'ascan',
              estimated_time: 0,
              title: 'ascan omega 0 20 10 0.1',
              status: 'FINISHED',
            },
          ],
          total: 4,
        }),
      },
    },

    '/data/3505583049': {
      get: {
        status: 200,
        response: () => ScanData,
      },
    },

    '/spectra/3505583049': {
      get: {
        status: 200,
        response: () => ScanSpectra,
      },
    },
  };
}
