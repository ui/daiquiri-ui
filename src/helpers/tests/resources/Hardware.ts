import ResourceHandler from 'helpers/tests/resources/ResourceHandler';

import Motor from 'components/hardware/motor/mocks/Motor.json';
import Multiposition from 'components/hardware/multiposition/mocks/Multiposition.json';
import BeamShutter from 'components/hardware/shutter/mocks/BeamShutter.json';
import FrontEnd from 'components/hardware/frontend/mocks/FrontEnd.json';
import Beamviewer from 'components/hardware/beamviewer/mocks/Beamviewer.json';
import Gauge from 'components/hardware/gauge/mocks/Gauge.json';
import Pump from 'components/hardware/pump/mocks/Pump.json';
import Valve from 'components/hardware/valve/mocks/Valve.json';
import Wago from 'components/hardware/wago/mocks/Wago.json';
import HardwareRefs from './mock/HardwareRefs.json';

export default class Hardware extends ResourceHandler {
  public baseUrl = '/hardware';

  private omegaPosition: number | undefined = undefined;

  public resources = {
    '': {
      get: {
        status: 200,
        response: () => {
          const result = [
            {
              ...Motor,
              id: 'omega1',
              name: 'omega1',
            },
            {
              ...Motor,
              properties: {
                ...Motor.properties,
                position: this.omegaPosition || Motor.properties.position,
              },
            },
            Multiposition,
            BeamShutter,
            FrontEnd,
            Beamviewer,
            Gauge,
            Pump,
            Valve,
            Wago,
            ...HardwareRefs,
          ];
          return {
            total: result.length,
            rows: result,
          };
        },
      },
    },

    '/groups': {
      get: {
        status: 200,
        response: () => ({
          total: 1,
          rows: [
            {
              description: 'motors and status for the diffractometer',
              groupid: 'diffractometer',
              name: 'Diffractometer',
              objects: ['omega', 'beamstop'],
              state: true,
            },
          ],
        }),
      },
    },

    '/omega': {
      post: {
        json: true,
        status: 200,
        response: (payload: any) => {
          this.omegaPosition = Number.parseFloat(payload.value);
        },
      },
    },
  };
}
