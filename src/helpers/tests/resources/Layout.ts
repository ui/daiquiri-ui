import ResourceHandler from 'helpers/tests/resources/ResourceHandler';

export default class Layout extends ResourceHandler {
  public baseUrl = '/layout';

  public resources = {
    '': {
      get: {
        status: 200,
        response: () => ({
          total: 7,
          rows: [
            {
              acronym: 'layout_empty',
              children: [],
            },
            {
              acronym: 'layout_scanviewer',
              children: [
                {
                  children: [
                    {
                      children: [
                        {
                          type: 'scantable',
                          title: 'Scans',
                        },
                      ],
                      type: 'col',
                    },
                    {
                      children: [
                        {
                          type: 'scanplot0d',
                          title: 'Scalar Plot',
                        },
                      ],
                      type: 'col',
                    },
                  ],
                  type: 'row',
                },
                {
                  children: [
                    {
                      children: [
                        {
                          type: 'scanplot1d',
                          title: 'Spectra Plot',
                        },
                      ],
                      type: 'col',
                    },
                    {
                      children: [
                        {
                          type: 'scanplot2d',
                          style: {
                            height: '40vh',
                          },
                          title: 'Image Plot',
                        },
                      ],
                      type: 'col',
                    },
                  ],
                  type: 'row',
                },
              ],
              description: 'View running and past scans',
              name: 'Scan Viewer',
            },

            {
              acronym: 'layout_hardware',
              description: 'View hardware objects',
              name: 'Hardware View',
              children: [
                {
                  type: 'hardwaregroup',
                  ids: [
                    { id: 'omega' },
                    { id: 'omega1', variant: 'small' },
                    { id: 'beamstop' },
                    { id: 'abs1' },
                    { id: 'fe' },
                    { id: 'bv1' },
                    { id: 'pir121' },
                    { id: 'ip121' },
                    { id: 'rv12' },
                    { id: 'generic' },
                  ],
                },
              ],
            },
            {
              acronym: 'layout_twod',
              children: [
                {
                  children: [
                    {
                      type: 'twod',
                      providers: {
                        metadata: {
                          datacollections: {
                            namespace: 'subsample',
                            params: {
                              subsampleid: '$state.selectedSubSample',
                            },
                          },
                          images: {
                            params: {
                              sampleid: '$state.currentSample',
                            },
                          },
                          subsamples: {
                            params: {
                              sampleid: '$state.currentSample',
                            },
                          },
                          xrf_composites: {
                            params: {
                              sampleid: '$state.currentSample',
                            },
                          },
                          xrf_map_rois: {
                            params: {
                              sampleid: '$state.currentSample',
                            },
                          },
                          xrf_maps: {
                            params: {
                              sampleid: '$state.currentSample',
                            },
                          },
                        },
                      },
                    },
                  ],
                  xs: 8,
                  type: 'col',
                },
                {
                  children: [
                    {
                      type: 'twodobjectlist',
                    },
                    {
                      type: 'twodobject',
                      providers: {
                        metadata: {
                          datacollections: {
                            namespace: 'subsample',
                          },
                        },
                      },
                    },
                  ],
                  xs: 4,
                  type: 'col',
                },
              ],
              style: {
                flex: '0 0 70%',
              },
              type: 'row',
            },
            {
              acronym: 'layout_hardware_ref1',
              description: 'View hardware objects containing ref',
              name: 'Hardware View2',
              children: [
                {
                  type: 'label',
                  label: 'Test objectref_to_nothing',
                },
                {
                  type: 'hardwaregroup',
                  ids: [{ id: 'objectref_to_nothing.ref' }],
                },
              ],
            },
            {
              acronym: 'layout_hardware_ref2',
              description: 'View hardware objects containing ref',
              name: 'Hardware View2',
              children: [
                {
                  type: 'label',
                  label: 'Test objectref_to_omega',
                },
                {
                  type: 'hardwaregroup',
                  ids: [{ id: 'objectref_to_omega.ref' }],
                },
              ],
            },
            {
              acronym: 'layout_hardware_ref3',
              description: 'View hardware objects containing double ref',
              name: 'Hardware View3',
              children: [
                {
                  type: 'label',
                  label: 'Test objectref_to_objectref_to_omega',
                },
                {
                  type: 'hardwaregroup',
                  ids: [{ id: 'objectref_to_objectref_to_omega.ref.ref' }],
                },
              ],
            },
          ],
        }),
      },
    },
  };
}
