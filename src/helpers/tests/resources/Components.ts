import ResourceHandler from 'helpers/tests/resources/ResourceHandler';

export default class Authenticator extends ResourceHandler {
  public baseUrl = '/components';

  public resources = {
    '/config': {
      get: {
        status: 200,
        response: () => ({
          beamline: 'bl',
          chat: {},
          console: {},
          editor: {},
          hardware: {
            monitor: [
              {
                comparator: 'in',
                comparison: [50, 60],
                name: 'Omega',
                overlay: 'omega.position',
                value: 'omega.position',
              },
              {
                comparator: '==',
                comparison: 'READY',
                name: 'S1D',
                value: 's1d.state',
              },
              {
                name: 'robz',
                value: 'robz.position',
              },
            ],
          },
          imageviewer: {
            options: {
              polylines: true,
            },
            scantypes: {
              loi: ['loiscan'],
              poi: ['poiscan'],
              roi: ['roiscan', 'fluoxas', 'myscan'],
            },
          },
          logging: {},
          parameteriser: {
            parametertypes: {
              measurement: [{ actor: 'mexafs' }, { actor: 'mxanes' }],
              sample: [{ actor: 'scircle' }, { actor: 'srect' }],
            },
          },
          samplescan: {},
          scans: {},
          simplescan: {},
          synoptic: {
            elements: [
              {
                group: 'slits',
                name: 'S1',
                value: 's1b.position',
              },
              {
                group: null,
                name: 'RV1',
                value: null,
              },
              {
                group: 'virtual',
                name: 'Virtual',
                value: null,
              },
              {
                group: null,
                name: 'after_OH1',
                value: null,
              },
              {
                group: 'others',
                name: 'S2',
                value: 's1ho.position',
              },
              {
                group: null,
                name: 'RV1',
                value: null,
              },
              {
                group: 'diffractometer',
                name: 'Diff',
                value: null,
              },
              {
                group: 'robot',
                name: 'Robot',
                value: null,
              },
              {
                group: null,
                name: 'd1',
                value: null,
              },
            ],
            synoptic: 'example.svg',
          },
          version: {},
        }),
      },
    },
  };
}
