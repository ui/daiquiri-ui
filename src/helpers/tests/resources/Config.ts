import ResourceHandler from 'helpers/tests/resources/ResourceHandler';

export default class Config extends ResourceHandler {
  public baseUrl = '/config';

  public resources = {
    '': {
      get: {
        status: 200,
        response: () => ({
          beamline: 'bl',
          celery: {},
          chat: {},
          editor: {},
          h5grove: {},
          hardware: {
            monitor: [
              {
                comparator: 'in',
                comparison: [50, 60],
                name: 'Omega',
                overlay: 'omega.position',
                value: 'omega.position',
              },
              {
                comparator: '==',
                comparison: 'READY',
                name: 'S1D',
                value: 's1d.state',
              },
              { name: 'robz', value: 'robz.position' },
            ],
          },
          imageviewer: {
            options: {
              autofocus: true,
              export: true,
              extra_snap_sizes: [13, 18],
              import_reference: true,
              polylines: true,
            },
            scantypes: {
              loi: ['loiscan'],
              poi: ['poiscan'],
              roi: ['roiscan'],
            },
            upload_canvas: true,
          },
          logging: {},
          samplescan: {},
          scans: { mca: { conversion: { scale: 25, zero: 0 }, sigma: 120 } },
          simplescan: {},
          synoptic: {
            synoptics: [
              {
                elements: [
                  { group: 'slits', name: 'S1', value: 's1b.position' },
                  { group: null, name: 'RV1', value: null },
                  { group: 'virtual', name: 'Virtual', value: null },
                  { group: null, name: 'after_OH1', value: null },
                  { group: 'others', name: 'S2', value: 's1ho.position' },
                  { group: null, name: 'RV1', value: null },
                  { group: 'diffractometer', name: 'Diff', value: null },
                  { group: 'robot', name: 'Robot', value: null },
                  { group: null, name: 'd1', value: null },
                ],
                synoptic: 'example.svg',
              },
            ],
          },
          version: {},
        }),
      },
    },
  };
}
