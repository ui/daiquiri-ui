import ResourceHandler from 'helpers/tests/resources/ResourceHandler';

const session = {
  data: {
    username: 'abcd',
    client: null,
    sio: 'fbf9e21840e245f39d60378add541195',
    blsession: 'blc00001-1',
  },
  last_access: 1_598_596_579.595_204_6,
  operator: false,
  sessionid: 'cd264324-665e-4607-a8ba-afc6ba459026',
  operator_pending: false,
};

export default class Session extends ResourceHandler {
  public baseUrl = '/session';

  private operator = false;

  public resources = {
    '': {
      get: {
        status: 200,
        response: () => ({
          total: 1,
          rows: [{ ...session, operator: this.operator || false }],
        }),
      },
    },

    '/current': {
      get: {
        status: 200,
        response: () => ({ ...session, operator: this.operator || false }),
      },
    },

    '/current/control': {
      post: {
        status: 200,
        response: () => {
          this.operator = true;
          return {};
        },
      },
    },
  };
}
