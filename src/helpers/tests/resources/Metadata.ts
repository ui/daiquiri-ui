import ResourceHandler from 'helpers/tests/resources/ResourceHandler';

export default class Metadata extends ResourceHandler {
  public baseUrl = '/metadata';

  private maproi: any;

  public resources = {
    '/components': {
      get: {
        status: 200,
        response: () => ({
          total: 1,
          rows: [
            {
              name: 'Component 1',
              samples: 1,
              componentid: 1,
              acronym: 'comp1',
              datacollections: 1,
            },
          ],
        }),
      },
    },

    '/samples': {
      get: {
        status: 200,
        response: () => ({
          total: 1,
          rows: [
            {
              component: 'comp1',
              name: 'sample1',
              componentid: 1,
              sampleid: 1,
              queued: false,
              offsety: 0,
              comments: null,
              offsetx: 0,
              subsamples: 3,
              datacollections: 1,
              extrametadata: {},
            },
          ],
        }),
      },
    },

    '/samples/sub': {
      get: {
        status: 200,
        response: () => ({
          total: 1,
          rows: [
            {
              subsampleid: 1,
              x: 0,
              y: 0,
              x2: 500,
              y2: 500,
              datacollections: 0,
              type: 'roi',
              extrametadata: {},
            },
          ],
        }),
      },
    },

    '/samples/sub/1': {
      patch: {
        status: 200,
        json: true,
        response: (payload: any) => ({
          subsampleid: 1,
          x2: payload.x2,
          y2: payload.y2,
        }),
      },

      delete: {
        status: 200,
        response: () => ({
          message: 'Subsample deleted',
        }),
      },
    },

    '/samples/tags': {
      get: {
        status: 200,
        response: () => ({
          tags: ['test'],
        }),
      },
    },

    '/samples/images': {
      get: {
        status: 200,
        response: () => ({
          total: 1,
          rows: [
            {
              scaley: -5133.801_937_103_271_5,
              url: '/metadata/samples/images/1',
              offsety: -320_309,
              offsetx: 9_192_014,
              sampleimageid: 1,
              scalex: 5633.801_937_103_271_5,
            },
          ],
        }),
      },
    },

    '/sessions': {
      get: {
        status: 200,
        response: () => ({
          total: 1,
          rows: [
            {
              session: 'blc00001-1',
              enddate: '01-01-2030 23:59',
              proposal: 'blc00001',
              proposalid: 1,
              startdate: '01-01-2020 00:00',
              beamlinename: 'bl',
            },
          ],
        }),
      },
    },

    '/sessions/select': {
      post: {
        status: 200,
        response: () => ({
          proposal: 'blc00001',
          session: 'blc00001-1',
          beamlinename: 'bl',
          enddate: '01-01-2030 23:59',
          proposalid: 1,
          startdate: '01-01-2020 00:00',
        }),
      },
    },

    '/users/cache': {
      get: {
        status: 200,
        response: () => ({
          cache: {},
        }),
      },
    },

    '/users/current': {
      get: {
        status: 200,
        response: () => ({
          familyname: 'User',
          personid: 1,
          permissions: [],
          fullname: 'Test User',
          givenname: 'Test',
          is_staff: false,
        }),
      },
    },

    '/datacollections': {
      get: {
        status: 200,
        response: () => ({
          rows: [
            {
              beamsizeatsampley: null,
              duration: 49,
              snapshots: { 1: true, 2: false, 3: false, 4: false },
              beamsizeatsamplex: null,
              dy_mm: 0.05,
              steps_x: 4,
              runstatus: 'Successful',
              subsampleid: 1,
              datacollectionnumber: 3_505_583_049,
              experimenttype: 'XRF map',
              xbeam: null,
              wavelength: 1.239_84,
              filetemplate: 'sample1_roi1_1.h5',
              starttime: '16-06-2020 15:52:28',
              datacollectionid: 1,
              dx_mm: 0.05,
              endtime: '16-06-2020 15:53:17',
              exposuretime: 0.1,
              steps_y: 4,
              sampleid: 1,
              imagedirectory: '/data/bl/tmp/blc00001/sample1/sample1_roi1_1',
              ybeam: null,
              numberofimages: 16,
              numberofpasses: null,
            },
          ],
          total: 1,
        }),
      },
    },

    '/xrf/maps/rois': {
      get: {
        status: 200,
        response: () => {
          const ret = [
            {
              start: 6370,
              maproiid: 1,
              sampleid: 1,
              edge: 'Ka',
              element: 'Fe',
              maps: 1,
              end: 6440,
            },
          ];

          if (this.maproi) ret.push(this.maproi);
          return { rows: ret, total: ret.length };
        },
      },

      post: {
        json: true,
        status: 200,
        response: (payload: any) => {
          this.maproi = {
            ...payload,
            maproiid: 2,
          };
          return this.maproi;
        },
      },
    },

    '/xrf/maps': {
      get: {
        status: 200,
        response: () => ({
          total: 2,
          rows: [
            {
              colourmap: 'viridis',
              element: 'Fe',
              subsampleid: 1,
              url: '/metadata/xrf/maps/1',
              datacollectionnumber: 2_073_510_334,
              orientation: 'horizontal',
              snaked: false,
              histogram: { bins: [0], width: [1], hist: [16] },
              datacollectionid: 1,
              min: null,
              h: 4,
              points: 16,
              maproiid: 1,
              sampleid: 1,
              scalar: null,
              mapid: 1,
              edge: 'Ka',
              composites: 0,
              w: 4,
              opacity: 1,
              max: null,
            },
            {
              colourmap: null,
              element: null,
              subsampleid: 1,
              url: '/metadata/xrf/maps/2',
              datacollectionnumber: 2_073_510_334,
              orientation: 'horizontal',
              snaked: false,
              histogram: {
                bins: [
                  0.435_828_373_114_367_7, 0.439_627_194_074_229_45,
                  0.443_426_015_034_091_2, 0.447_224_835_993_952_9,
                  0.451_023_656_953_814_7,
                ],
                width: [
                  0.003_798_820_959_861_726, 0.003_798_820_959_861_781_7,
                  0.003_798_820_959_861_726, 0.003_798_820_959_861_781_7,
                  0.003_798_820_959_861_726,
                ],
                hist: [2, 2, 3, 4, 5],
              },
              datacollectionid: 1,
              min: null,
              h: 4,
              points: 16,
              maproiid: 2,
              sampleid: 1,
              scalar: 'simu1:deadtime_det1',
              mapid: 2,
              edge: null,
              composites: 0,
              w: 4,
              opacity: 1,
              max: null,
            },
          ],
        }),
      },
    },

    '/xrf/maps/composite': {
      get: {
        status: 200,
        response: () => ({ rows: [], total: 0 }),
      },
    },
  };
}
