import ResourceHandler from 'helpers/tests/resources/ResourceHandler';

import SchemaM from 'helpers/tests/resources/mock/Schema.json';

export default class Schema extends ResourceHandler {
  public baseUrl = '/schema';

  public resources = {
    '': {
      get: {
        status: 200,
        response: () => SchemaM,
      },
    },
    '/ScircleSchema': {
      get: {
        status: 200,
        response: () => SchemaM.ScircleSchema,
      },
    },
    '/SrectSchema': {
      get: {
        status: 200,
        response: () => SchemaM.SrectSchema,
      },
    },
  };
}
