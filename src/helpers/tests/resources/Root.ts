import ResourceHandler from 'helpers/tests/resources/ResourceHandler';

export default class Authenticator extends ResourceHandler {
  public baseUrl = '';

  public resources = {
    '/meta.json': {
      get: {
        noBase: true,
        status: () => {
          return 200;
        },
        response: () => ({
          version: '61fd5be',
        }),
      },
    },
  };
}
