import ResourceHandler from 'helpers/tests/resources/ResourceHandler';

export default class Imageviewer extends ResourceHandler {
  public baseUrl = '/imageviewer';

  public resources = {
    '/maps': {
      get: {
        status: 200,
        response: () => ({
          during_scan: true,
        }),
      },
    },
    '/sources': {
      get: {
        status: 200,
        response: () => ({
          total: 1,
          rows: [
            {
              polylines: {
                limits: [
                  [-999_999_999_009_999.9, 999_999_999_009_999.9],
                  [999_999_999_009_999.9, 999_999_999_009_999.9],
                  [999_999_999_009_999.9, -999_999_999_009_999.9],
                  [-999_999_999_009_999.9, -999_999_999_009_999.9],
                  [-999_999_999_009_999.9, 999_999_999_009_999.9],
                ],
                'fine motors': [
                  [9_012_999.999_999_998, -566_999.999_999_999_9],
                  [9_032_999.999_999_998, -566_999.999_999_999_9],
                  [9_032_999.999_999_998, -586_999.999_999_999_9],
                  [9_012_999.999_999_998, -586_999.999_999_999_9],
                  [9_012_999.999_999_998, -566_999.999_999_999_9],
                ],
              },
              name: 'vlm',
              center: [9_192_014.059_999_999, -320_309.899_999_999_9],
              url: '/resources/images/xtal.png',
              type: 'video',
              pixelsize: [5633.801_999_999_992, -5133.802],
              sourceid: 1,
              markings: {
                beam: {
                  position: [9_022_999.999_999_998, -576_999.999_999_999_9],
                  center: [9_022_882.632_109_301, -576_999.999_999_999_9],
                  size: [852.799_096_394_330_3, 300],
                  origin: true,
                },
              },
              additional: { z: 3 },
              origin: true,
            },
          ],
        }),
      },
    },
  };
}
