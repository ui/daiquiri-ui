import { each, keys } from 'lodash';
import MockAxios from 'helpers/tests/MockAxios';

import Config from 'helpers/tests/resources/Config';
import Metadata from 'helpers/tests/resources/Metadata';
import Saving from 'helpers/tests/resources/Saving';
import Schema from 'helpers/tests/resources/Schema';
import Authenticator from 'helpers/tests/resources/Authenticator';
import Layout from 'helpers/tests/resources/Layout';
import Chat from 'helpers/tests/resources/Chat';
import Components from 'helpers/tests/resources/Components';
import Root from 'helpers/tests/resources/Root';
import Scans from 'helpers/tests/resources/Scans';
import Hardware from 'helpers/tests/resources/Hardware';
import Queue from 'helpers/tests/resources/Queue';
import Session from 'helpers/tests/resources/Session';
import Imageviewer from 'helpers/tests/resources/Imageviewer';
import Parameteriser from 'helpers/tests/resources/Parameteriser';
import type ResourceHandler from './ResourceHandler';

export default class Resource {
  public classes: Record<string, typeof ResourceHandler> = {
    config: Config,
    metadata: Metadata,
    saving: Saving,
    schema: Schema,
    authenticator: Authenticator,
    layout: Layout,
    chat: Chat,
    components: Components,
    root: Root,
    scans: Scans,
    hardware: Hardware,
    queue: Queue,
    session: Session,
    imageviewer: Imageviewer,
    parameteriser: Parameteriser,
  };

  public handlers: Record<string, ResourceHandler> = {};
  public mockAxios: MockAxios;

  public constructor(options?: {
    handlers?: Record<string, typeof ResourceHandler>;
  }) {
    const classes = options?.handlers || this.classes;

    this.mockAxios = new MockAxios();
    each(classes, (Cl, key) => {
      this.handlers[key] = new Cl();
      this.handlers[key].setup(this.mockAxios);
    });

    this.mockAxios.finally();
  }

  public getClasses() {
    return keys(this.classes);
  }

  public getHandler(key: string) {
    if (!(key in this.handlers)) {
      throw new Error(`No such handler ${key} on Resource`);
    }

    return this.handlers[key];
  }

  public client() {
    return this.mockAxios.client();
  }
}
