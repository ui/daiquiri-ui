import ResourceHandler from 'helpers/tests/resources/ResourceHandler';

export default class Saving extends ResourceHandler {
  public baseUrl = '/saving';

  public resources = {
    '': {
      get: {
        status: 200,
        response: (payload: any) => ({
          current: 'test1/',
          template: '{sample}/{subsampletype}{subsampleid}',
          root: '/data/bl/2019/sw1234-1',
        }),
      },
    },
  };
}
