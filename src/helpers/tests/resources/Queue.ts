import ResourceHandler from 'helpers/tests/resources/ResourceHandler';

export default class Queue extends ResourceHandler {
  public baseUrl = '/queue';

  public resources = {
    '': {
      get: {
        status: 200,
        response: () => ({ current: null, stack: [], all: [], running: false }),
      },
    },
  };
}
