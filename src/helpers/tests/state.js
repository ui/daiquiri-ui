import session from 'providers/session';
import metadata from 'providers/metadata';

export const withControl = async () => {
  await session.dispatch('REQUEST_CONTROL');
  return session.dispatch('FETCH_SESSIONS');
};

export const hasControl = store => {
  return session.selector('operator', store.getState());
};

export const withSubsample = subsampleid => {
  return metadata.dispatch('SELECT_SUB_SAMPLE', { subsampleid });
};
