import { createStore, withToken, render, waitFor } from 'helpers/tests';

import Resources from 'helpers/tests/resources';

import App from 'App';
import Start from 'Start';
import { screen } from 'helpers/tests';

let store;
let resources;

const startApp = async ({ initialState = {}, layout = 0 } = {}) => {
  if (!store) {
    resources = new Resources();
    withToken();

    store = createStore(initialState);
    Start.start(store, layout);
  }

  const view = await render(<App />, { store });

  await waitFor(() => {
    expect(screen.queryByText('Starting Up')).not.toBeInTheDocument();
  });

  return { view, store, resources };
};

export default startApp;
