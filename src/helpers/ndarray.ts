import ndarray from 'ndarray';
import type { NdArray } from 'ndarray';
import type { TypedArray } from 'ndarray';
import { useMemo } from 'react';
import { NdFloat16Array } from '../components/h5web/NdFloat16Array';

/**
 * Function to mimick the server side behaviour of 'safe' type
 */
export function toSafeDtype(dtype: string) {
  const dtype2 = `<${dtype.slice(1)}`;
  const mapping: Record<string, string> = {
    '<f1': '<f4',
    '<f2': '<f4',
    '<i8': '<i4',
    '<u8': '<u4',
  };
  return mapping[dtype2] ?? dtype2;
}

/**
 * Create a typed array from an ArrayBuffer or an Uint8Array.
 *
 * It returns a view on the underlaying buffer, without memory copy.
 *
 * FIXME: little and big endian are not supported
 */
export function toTypedArray(buffer: ArrayBuffer | Uint8Array, dtype: string) {
  const mapping: Record<string, unknown> = {
    '<f4': Float32Array,
    '<f8': Float64Array,
    '<i1': Int8Array,
    '|i1': Int8Array,
    i1: Int8Array, // FIXME: should be removed
    '<i2': Int16Array,
    '<i4': Int32Array,
    '<u1': Uint8Array,
    '|u1': Uint8Array,
    u1: Uint8Array, // FIXME: should be removed
    '<u2': Uint16Array,
    '<u4': Uint32Array,
  };
  const arrayClass = mapping[dtype];
  if (!arrayClass) {
    throw new Error(`Unsupported dtype: ${dtype}`);
  }
  if (buffer instanceof Uint8Array) {
    // Here we reuse the parent buffer and offset/length to avoid memory-copy
    // @ts-expect-error
    const bytes_per_element = arrayClass.BYTES_PER_ELEMENT;
    try {
      // @ts-expect-error
      return new arrayClass(
        buffer.buffer,
        buffer.byteOffset,
        buffer.byteLength / bytes_per_element
      );
    } catch {
      console.warn(
        `toTypedArray is using a memory copy for a '${arrayClass}'. Probably because the buffer is not properly aligned.`
      );
      // @ts-expect-error
      return new arrayClass(buffer);
    }
  }
  // @ts-expect-error
  return new arrayClass(buffer);
}

/**
 * Convert an ArrayBuffer to a NdArray<TypedArray>
 *
 * NOTE: This is a very specific function, it would be better to remove it
 */
export function toNdArray(
  buffer: ArrayBuffer,
  shape: number[],
  dtype: string
): NdArray<TypedArray> {
  const nElements = shape.reduce((previous, current) => previous * current);
  let ArrayType;
  switch (dtype) {
    case '<f2': // TODO either float16 or float32 depending on h5grove
      ArrayType =
        nElements * 2 === buffer.byteLength ? Uint16Array : Float32Array;
      break;
    case '<f4':
      ArrayType = Float32Array;
      break;
    case '<f8':
      ArrayType = Float64Array;
      break;
    case '<i1':
    case '|i1':
    case 'i1':
      ArrayType = Int8Array;
      break;
    case '<i2':
      ArrayType = Int16Array;
      break;
    case '<i4':
    case '<i8': // TODO converted server side
      ArrayType = Int32Array;
      break;
    case '<u1':
    case '|u1':
    case 'u1':
      ArrayType = Uint8Array;
      break;
    case '<u2':
      ArrayType = Uint16Array;
      break;
    case '<u4':
    case '<u8': // TODO converted server side
      ArrayType = Uint32Array;
      break;
    default:
      throw new Error(`Unsupported dtype: ${dtype}`);
  }
  return ndarray(new ArrayType(buffer), shape);
}

/**
 * Convert a typed array to a float32 if needed. Else return the array without copy.
 */
export function asFloat32Array(
  array: ndarray.NdArray<ndarray.TypedArray>
): ndarray.NdArray<Float32Array> {
  if (array.dtype === 'float32') {
    return array as ndarray.NdArray<Float32Array>;
  }
  return ndarray(Float32Array.from(array.data), array.shape);
}

export function isNdArrayTypedArray(
  entity: unknown
): entity is NdArray<TypedArray> {
  return (
    entity instanceof Object &&
    'data' in entity &&
    'stride' in entity &&
    'shape' in entity
  );
}

export function assertNdArrayTypedArray(
  entity: unknown,
  message = 'Expected NdArray<TypedArray>'
): asserts entity is NdArray<TypedArray> {
  if (!isNdArrayTypedArray(entity)) {
    throw new Error(message);
  }
}

export function assertDisplayableNdArray(
  entity: unknown,
  message = 'Expected NdArray<TypedArray> or NdFloat16Array'
): asserts entity is NdArray<TypedArray> {
  if (!isNdArrayTypedArray(entity) && !(entity instanceof NdFloat16Array)) {
    throw new Error(message);
  }
}

/**
 * Slice an NdArray.
 *
 * It only support 1d arrays.
 */
export function ndslice(
  array: NdArray<TypedArray>,
  start: number,
  stop: number
): NdArray<TypedArray> {
  if (array.dimension !== 1) {
    throw new Error('Slicing only supported for 1dim array');
  }
  const array2 = array.data.subarray(start, stop);
  return ndarray(array2, [stop - start]);
}

export interface Dtype {
  byteorder: '<' | '>';
  type: 'f' | 'i' | 'u';
  itemsize: number;
  numpy: string;
}

export function parseDtype(format: string): Dtype {
  if (format.length !== 3) {
    throw new Error(`Unsupported dtype format ${format}`);
  }
  return {
    // @ts-expect-error
    byteorder: format[0],
    // @ts-expect-error
    type: format[1],
    itemsize: Number.parseInt(format[2]),
    numpy: format,
  };
}

export function useSlicedArray(
  array: NdArray<TypedArray>,
  start: number,
  stop: number
): NdArray<TypedArray> {
  return useMemo(() => {
    return ndslice(array, start, stop);
  }, [array, start, stop]);
}

export function isShape(entity: unknown): boolean {
  if (!Array.isArray(entity)) {
    return false;
  }
  for (const value of entity) {
    if (typeof value !== 'number') {
      return false;
    }
  }
  return true;
}

export function assertShape(
  entity: unknown,
  message = 'Expected number[]'
): asserts entity is number[] {
  if (!isShape(entity)) {
    throw new Error(message);
  }
}
