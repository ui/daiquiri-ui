import RestService from 'services/RestService';
import baseUrl from 'helpers/baseUrl';

function sign(params: {
  url: string;
  callback: (protectedUrl: string) => void;
}) {
  const urlObj = new URL(baseUrl());
  RestService.post('/session/sign', {
    url: `${urlObj.pathname}${params.url}`,
  }).then((resp) => {
    params.callback(
      `${baseUrl().replace(urlObj.pathname, '')}${resp.data.url}?bewit=${
        resp.data.bewit
      }`
    );
  });
}

export default sign;
