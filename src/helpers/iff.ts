const ALIGN_BLOCK = 4;

/**
 * Interchange file format (IFF) reader.
 *
 * The IFF format is a very basic container for binary data.
 *
 * See https://en.wikipedia.org/wiki/Interchange_File_Format
 *
 * It is compound by a list of chunks.
 *
 * Each chunk is compumnd by:
 * - A "fourcc" type, an ASCII name of 4 characters (uint32)
 * - The size of the following block (uint32 big-endian)
 * - The data block
 *
 * The blocks have to start at a position aligned to 4 bytes.
 */
export class IffReader {
  private readonly buffer: Uint8Array;
  private readonly view: DataView;
  private pos: number;

  public constructor(buffer: ArrayBuffer) {
    this.buffer = new Uint8Array(buffer);
    this.view = new DataView(buffer);
    this.pos = 0;
  }

  /**
   * Read a chunk type at the cursor location.
   *
   * This moves the cursor for the following content.
   */
  public readChunkType(): string {
    // make it start at an aligned position
    const align = this.pos % ALIGN_BLOCK;
    if (align) {
      this.pos += ALIGN_BLOCK - align;
    }
    const p = this.pos;
    this.pos += 4;
    const charbuf = this.buffer.subarray(p, this.pos);
    // @ts-expect-error
    return String.fromCharCode.apply(null, charbuf);
  }

  /**
   * Read a chunk size at the cursor location.
   *
   * This moves the cursor for the following content.
   */
  public readChunkSize(): number {
    const uint = this.view.getUint32(this.pos, false);
    this.pos += 4;
    return uint;
  }

  /**
   * Read a data chunk at the cursor location.
   *
   * It is composed by a uint32 big endian size following by the data.
   *
   * This moves the cursor for the following content.
   */
  public readChunk(): Uint8Array {
    const size = this.readChunkSize();
    const p = this.pos;
    this.pos += size;
    return this.buffer.subarray(p, p + size);
  }

  /**
   * Skip the data chunk at the cursor location.
   *
   * It is composed by a uint32 big endian size following by the data.
   *
   * This moves the cursor for the following content.
   */
  public skipChunk() {
    const size = this.readChunkSize();
    const p = this.pos;
    this.pos += size;
  }

  /**
   * Read a JSON data chunk at the cursor location.
   *
   * This moves the cursor for the following content.
   */
  public readJsonChunk(): Record<string, any> {
    const size = this.readChunkSize();
    const p = this.pos;
    this.pos += size;
    const charbuf = this.buffer.subarray(p, p + size);
    // @ts-expect-error
    const text = String.fromCharCode.apply(null, charbuf);
    return JSON.parse(text);
  }

  /**
   * True if the cursor is at the end of the file.
   */
  public eof(): boolean {
    return (
      this.pos + (ALIGN_BLOCK - (this.pos % ALIGN_BLOCK)) >=
      this.buffer.byteLength
    );
  }
}
