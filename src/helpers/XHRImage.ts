export default class XHRImage extends Image {
  public static token = '';

  public completedPercentage = 0;

  public headers: { [index: string]: string } = {};

  // @ts-expect-error
  public onprogress: ((pc: number) => void) | null = null;

  // @ts-expect-error
  public onerror:
    | ((status: number, e: Event, xhr: XMLHttpRequest) => void)
    | null = null;

  public xhr: XMLHttpRequest | null = null;

  public static setToken(token: string) {
    XHRImage.token = token;
  }

  public load(url: string) {
    this.completedPercentage = 0;
    this.xhr = new XMLHttpRequest();

    if (url.startsWith('data:image')) {
      this.src = url;
      return;
    }

    this.xhr.open('GET', url, true);
    if (XHRImage.token) {
      this.xhr.setRequestHeader('Authorization', `Bearer ${XHRImage.token}`);
    }
    this.xhr.responseType = 'arraybuffer';

    this.xhr.onload = this.onLoad.bind(this);
    this.xhr.onprogress = this.onProgress.bind(this);

    this.xhr.onloadstart = () => {
      this.completedPercentage = 0;
    };

    this.xhr.onloadend = () => {
      this.completedPercentage = 100;
    };

    this.xhr.onerror = () => {
      console.log('img network error');
    };

    this.xhr.send();
  }

  public onProgress(e: ProgressEvent) {
    if (e.lengthComputable) {
      const pc = (e.loaded / e.total) * 100;

      if (pc !== this.completedPercentage) {
        this.completedPercentage = pc;
        if (this.onprogress) this.onprogress(pc);
      }

      this.completedPercentage = pc;
    }
  }

  public onLoad(e: Event) {
    if (!this.xhr) {
      return;
    }
    if (this.xhr.status === 0 || this.xhr.status !== 200) {
      if (this.onerror) this.onerror(this.xhr.status, e, this.xhr);
      return;
    }

    /**
     * Split the HTTP header content as key-value pair.
     *
     * The keys are converted as lower case
     */
    function parseHeadersAsDict(headerString: string) {
      const headers: { [index: string]: string } = {};
      const lines = headerString.split(/\r\n/);
      lines.forEach((line) => {
        if (line.length > 0) {
          const matched = line.matchAll(/^(.*?):\s*(.*)$/g);
          Array.from(matched).forEach((m) => {
            const [, g1, g2] = m;
            headers[g1.toLowerCase()] = g2;
          });
        }
      });
      return headers;
    }

    const h = this.xhr.getAllResponseHeaders();
    this.headers = parseHeadersAsDict(h);
    const mimeType = this.headers['content-type'] || 'image/png';

    const blob = new Blob([this.xhr.response], { type: mimeType });
    this.src = window.URL.createObjectURL(blob);
  }
}
