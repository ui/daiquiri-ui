import { createRef, Component } from 'react';
import PropTypes from 'prop-types';
import { fabric } from 'fabric';

fabric.Object.prototype.selectable = false;

/**
 * A simple helper component, wrapping retina logic for canvas and
 * auto-resizing the canvas to fill its parent container.
 *
 * To determine size/layout, we just use CSS on the div containing
 * the Canvas component (we're using this with flexbox, for example).
 *
 * Expects a "paint" function that takes a "context" to draw on
 * Whenever this component updates it will call this paint function
 * to draw on the canvas.
 *
 * For convenience, pixel dimensions are stored
 * in context.width, context.height and contex.pixelRatio.
 *
 * See https://gist.github.com/JobLeonard/987731e86b473d42cd1885e70eed616a#file-canvas-js-L12
 */
export default class CanvasEnhancer extends Component {
  static propTypes = {
    className: PropTypes.string,
    style: PropTypes.object,
    children: PropTypes.node
  };

  constructor(props) {
    super(props);

    this.state = {};

    this.canvasRef = createRef();
    this.containerRef = createRef();
  }

  componentDidMount() {
    const view = this.containerRef.current;
    // const ratio = window.devicePixelRatio || 1
    const ratio = 1;

    const newstate = {
      width: view.clientWidth * ratio,
      height: view.clientHeight * ratio,
      ratio,
      loadingMessage: null
    };

    this.setState(newstate);
  }

  componentDidUpdate() {
    if (this.state.ratio && !this.fabric) {
      this.initFabric();
    }
  }

  initFabric() {
    this.fabric = new fabric.Canvas(this.canvasRef.current);
  }

  render() {
    return (
      <div
        ref={this.containerRef}
        style={this.props.style}
        className={this.props.className}
      >
        {this.props.children}
        {this.state.ratio && (
          <canvas
            ref={this.canvasRef}
            width={this.state.width}
            height={this.state.height}
          />
        )}
      </div>
    );
  }
}
