import { useEffect, useMemo, useState } from 'react';
import { useSelector, useStore } from 'react-redux';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import hardwarep from 'providers/hardware';

import type { ReduxState } from 'types/ReduxStore';
import { getObjFromId } from '../../connect/hardware/utils';
import hardware from '../../providers/hardware';
import type { BeamlineNotification } from '../../types/Events';
import messageHandler from 'messagehandler';
import type CeleryMessageHandler from '../../messagehandler/CeleryMessageHandler';
import type { HardwareChangeActions } from '@esrf/daiquiri-lib/src/hardware/utils/types';

export function useOperator(): boolean {
  return useSelector<ReduxState, boolean>((state) => {
    return state.session.current.operator ?? false;
  });
}

/**
 * Expose the generic actions provided by a specific hardware.
 *
 * If null is passed, dummy actions are exposed.
 *
 * @param id: Daiquiri id of the hardware, or null
 */
export function useHardwareChangeActions(
  id: string | null
): HardwareChangeActions {
  return useMemo(() => {
    if (id === null) {
      return {
        setProperty: (name: string, value: any) => {
          return Promise.resolve();
        },
        call: (name: string, value?: any) => {
          return Promise.resolve();
        },
      };
    }
    return {
      setProperty: (name: string, value: any) => {
        const payload = { property: name, value, id };
        return hardwarep.dispatch('REQUEST_HARDWARE_CHANGE', payload);
      },
      call: (name: string, value?: any) => {
        const payload = { function: name, value, id };
        return hardwarep.dispatch('REQUEST_HARDWARE_CHANGE', payload);
      },
    };
  }, [id]);
}

/**
 * Get hardware object description from an id.
 *
 * The id can contains dot to express reference to subcomponent exposed by object property.
 */
export function useHardware(id: string | null): HardwareTypes.Hardware | null {
  const store = useStore<ReduxState>();

  useEffect(() => {
    const state = store.getState();
    const ns = hardware.getNamespace('hardware');
    const fetching = ns.selector('fetching', state);
    const fetched = ns.selector('fetched', state);
    if (!fetched && !fetching) {
      ns.fetch('default', { first: true });
    }
  }, [hardware]);

  return useSelector<ReduxState, HardwareTypes.Hardware | null>((store) => {
    if (id === null) {
      return null;
    }
    const hardwareMap = store.hardware.ns_hardware.default.results;
    return getObjFromId(hardwareMap, id);
  });
}

/** Expose the last notification which match the event type filter */
export function useLastNotification(
  events?: string[]
): BeamlineNotification | null {
  const [lastEvent, setLastEvent] = useState<BeamlineNotification | null>(null);

  const celery = useMemo(() => {
    return messageHandler.getHandler('Celery') as CeleryMessageHandler;
  }, []);

  useEffect(() => {
    function onMessage(data: any) {
      if (events?.includes(data.type)) {
        setLastEvent(data);
      }
    }
    celery.addListener(onMessage);
    return () => {
      celery.removeListener(onMessage);
    };
  }, [events]);

  return lastEvent;
}
