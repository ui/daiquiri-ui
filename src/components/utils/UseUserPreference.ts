import type { Dispatch, SetStateAction } from 'react';
import useUserCache, { getUserCacheValue } from './useUserCache';

export default function useUserPreference<T = any>(
  key: string,
  defaultValue: T
): [T, Dispatch<SetStateAction<T>>] {
  return useUserCache(key, defaultValue, 'preferences');
}

export function getUserPreferencesValue<T>(
  key: string,
  defaultValue: T,
  state: any
) {
  return getUserCacheValue<T>(key, defaultValue, state, 'preferences');
}
