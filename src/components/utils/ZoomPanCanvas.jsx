import PropTypes from 'prop-types';
import { fabric } from 'fabric';

import XHRImage from 'helpers/XHRImage';
import CanvasEnhancer from 'components/utils/CanvasEnhancer';

export default class ZoomPanCanvas extends CanvasEnhancer {
  static propTypes = {
    maxZoom: PropTypes.number,
    onError: PropTypes.func
  };

  static defaultProps = {
    maxZoom: 2,
    onError: undefined,
    lastError: false
  };

  constructor(props) {
    super(props);

    this.img = new XHRImage();
    this.img.onload = this.onLoad.bind(this);
    this.img.onerror = this.onError.bind(this);
    this.img.onprogress = this.onProgress.bind(this);
    this.first = true;

    this.state.lastZoom = null;
  }

  onError(status, event, xhr) {
    let resp = '';
    const enc = new TextDecoder('utf-8');
    if (xhr.response) {
      resp = enc.decode(xhr.response);
      try {
        const json = JSON.parse(resp);
        resp = json.error || json.message;
      } catch (error) {
        console.log('Error response was not json');
      }
    }
    if (this.props.onError) {
      this.props.onError(status, event, resp);
    } else {
      console.error('Error while loading image', status, event, resp);
    }
    this.img.src =
      'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';
    this.setState({ lastError: true }, () => {
      this.onLoad(true);
    });
  }

  onLoad(error) {
    this.fimg.setSrc(
      this.img.src,
      () => {
        this.fabric.renderAll();
        const initialZoom = this.state.width / this.fimg.width;
        if (this.state.lastZoom === null || this.state.lastError) {
          this.fabric.setZoom(initialZoom);
        }
        this.clampViewPort();
        this.props.onLoadProgress(100);
        if (this.state.lastZoom === null || this.state.lastError) {
          this.props.onZoomChange(initialZoom);
          this.setState({ lastZoom: initialZoom });
        }
      },
      { left: 0, top: 0, angle: 0 }
    );
    if (!error) {
      this.setState({ lastError: false });
    }
  }

  onProgress(pc) {
    this.props.onLoadProgress(pc);
  }

  loadImage() {
    if (this.fabric && this.props.url) {
      this.img.load(this.props.url);
    }
  }

  componentDidMount() {
    super.componentDidMount();
    this.first = true;
  }

  componentDidUpdate(prevProps) {
    super.componentDidUpdate(prevProps);
    if (prevProps.url !== this.props.url || this.first) {
      this.loadImage();
      this.first = false;
    }
  }

  componentWillUnmount() {
    this.img.onload = () => {};
    this.img.onprogress = () => {};
    if (this.fabric) {
      this.fabric.dispose();
    }
  }

  initFabric() {
    super.initFabric();

    this.fabric.selection = false;
    this.fimg = new fabric.Image();
    this.fabric.add(this.fimg);

    this.fabric.on('mouse:wheel', this.onMouseWheel);
    this.fabric.on('mouse:down', this.onMouseDown);
    this.fabric.on('mouse:move', this.onMouseMove);
    this.fabric.on('mouse:up', this.onMouseUp);

    if (this.props.onReady) this.props.onReady();
  }

  onMouseWheel = opt => {
    const clampedZoom = this.clampZoom(
      this.fabric.getZoom() + (-1 * opt.e.deltaY) / 200
    );
    this.props.onZoomChange(clampedZoom);

    this.fabric.zoomToPoint(
      { x: opt.e.offsetX, y: opt.e.offsetY },
      clampedZoom
    );
    this.clampViewPort();
    this.fimg.setCoords();

    opt.e.preventDefault();
    opt.e.stopPropagation();
  };

  onMouseDown = opt => {
    const { e } = opt;
    this.isDragging = true;
    this.lastPosX = e.clientX;
    this.lastPosY = e.clientY;
  };

  onMouseMove = opt => {
    if (this.isDragging) {
      const { e } = opt;
      this.fabric.viewportTransform[4] += e.clientX - this.lastPosX;
      this.fabric.viewportTransform[5] += e.clientY - this.lastPosY;

      this.clampViewPort();
      this.fimg.setCoords();
      this.fabric.requestRenderAll();

      this.lastPosX = e.clientX;
      this.lastPosY = e.clientY;
    }
  };

  onMouseUp = () => {
    this.isDragging = false;
  };

  clampZoom(zoom) {
    const minZoom = this.state.width / this.fimg.width;
    return Math.min(Math.max(zoom, minZoom), this.props.maxZoom);
  }

  clampViewPort() {
    const imageWidth = this.fimg.width * this.fabric.getZoom();
    const imageHeight = this.fimg.height * this.fabric.getZoom();

    const diffX = imageWidth - this.state.width;
    const diffY = imageHeight - this.state.height;

    const centX = Math.max((this.state.width - imageWidth) / 2, 0);
    const centY = Math.max((this.state.height - imageHeight) / 2, 0);

    const currX = this.fabric.viewportTransform[4];
    this.fabric.viewportTransform[4] = Math.min(Math.max(currX, -diffX), centX);

    const currY = this.fabric.viewportTransform[5];
    this.fabric.viewportTransform[5] = Math.min(Math.max(currY, -diffY), centY);
  }
}
