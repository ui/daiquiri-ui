import { Component } from 'react';
import PropTypes from 'prop-types';
import { debounce } from 'lodash';

export default class RemountOnResize extends Component {
  static propTypes = {
    className: PropTypes.string,
    style: PropTypes.object,
    children: PropTypes.node
  };

  constructor(props) {
    super(props);
    this.state = { resizing: false };

    const resize = () => {
      this.setState({ resizing: true });
    };
    this.setResize = debounce(resize, 500);
  }

  componentDidMount() {
    window.addEventListener('resize', this.setResize);
    this.setState({ resizing: false });
  }

  componentDidUpdate(prevProps, prevState) {
    if (!prevState.resizing && this.state.resizing) {
      this.setState({ resizing: false });
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setResize);
  }

  render() {
    return this.state.resizing ? null : this.props.children;
  }
}
