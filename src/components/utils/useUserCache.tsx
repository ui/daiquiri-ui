import { useMemo } from 'react';
import type { Dispatch, SetStateAction } from 'react';
import { get, isEqual } from 'lodash';
import { useSelector } from 'react-redux';
import metadata from '../../providers/metadata';

const deepEqual = (oldValue: any, newValue: any) => isEqual(oldValue, newValue);

function getPath(key: string, rootPath?: string) {
  return rootPath ? [rootPath, ...key.split('/')] : key.split('/');
}

function getValue(state: any, path: string[]) {
  const cacheNamespace = metadata.getNamespace('cache').getInstance('default');
  return get(cacheNamespace.selector('results', state).cache, path);
}

export default function useUserCache<T>(
  key: string,
  defaultValue: T,
  rootPath?: string
): [T, Dispatch<SetStateAction<T>>] {
  const path = useMemo(() => getPath(key, rootPath), [key, rootPath]);
  const currentValue: T = useSelector(
    (state) => getValue(state, path),
    deepEqual
  );

  function setState(value: SetStateAction<T>) {
    metadata.dispatch('UPDATE_USER_CACHE', {
      path,
      value,
    });
  }

  return [currentValue ?? defaultValue, setState];
}

/**
 * Programic way to retrieve value from cache
 *  Needs the current store state, could be removed in future
 */
export function getUserCacheValue<T>(
  key: string,
  defaultValue: T,
  state: any,
  rootPath?: string
) {
  const path = getPath(key, rootPath);
  return getValue(state, path) ?? defaultValue;
}
