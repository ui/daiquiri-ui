import type { PropsWithChildren } from 'react';
import { Navigate, useLocation } from 'react-router-dom';

interface Props {
  isLoggedOut: boolean;
}

function PrivateRoute(props: PropsWithChildren<Props>) {
  const { children, isLoggedOut } = props;
  const location = useLocation();

  if (isLoggedOut) {
    /* Redirect to login page. Save current location to location state
     * so `LoginRoute` can redirect back to it after user logs in. */
    return <Navigate to="/login" state={location} replace />;
  }

  return children;
}

export default PrivateRoute;
