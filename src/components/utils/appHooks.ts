import { useMemo } from 'react';
import app from 'providers/app';

/**
 * Hook to provide a function to add temporary message on the Daiquiri
 * top level interface.
 */
export function useToast() {
  return useMemo(() => {
    return (props: { type: string; title: string; text: string }) =>
      app.dispatch('ADD_TOAST', props);
  }, []);
}
