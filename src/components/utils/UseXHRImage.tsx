import { useState, useEffect } from 'react';
import XHRImage from '../../helpers/XHRImage';

const enum XHRImageStatus {
  LOADING = 'loading',
  LOADED = 'loaded',
  FAILED = 'failed',
}

interface XHRImageHeader {
  [index: string]: string;
}

export interface XHRImageData {
  url: string | undefined;
  image: string | undefined;
  status: XHRImageStatus;
  progress: number;
  errorEvent: Event | undefined;
  errorStatus: number | undefined;
  headers: XHRImageHeader;
}

const defaultState = {
  url: undefined,
  image: undefined,
  status: XHRImageStatus.LOADING,
  progress: 0,
  errorEvent: undefined,
  errorStatus: undefined,
  headers: {} as XHRImageHeader,
};

/**
 * Create an image state based on an url.
 *
 * If abort is true, a new Url value will abort the previous request.
 *
 * FIXME: If false, this have to be properly debounced, but the code is not
 * good enough right now.
 */
export default function useXHRImage(
  url: undefined | null | string,
  abortable: boolean
): XHRImageData {
  const [state, setState] = useState<XHRImageData>(defaultState);

  useEffect(() => {
    if (!url) {
      return undefined;
    }
    const img = new XHRImage();

    function onload() {
      const { src, headers } = img;
      setState({
        ...defaultState,
        status: XHRImageStatus.LOADED,
        progress: 100,
        image: src,
        url: typeof url === 'string' ? url : undefined,
        headers: headers as XHRImageHeader,
      });
    }

    function onprogress(percent: number) {
      setState({
        ...defaultState,
        status: XHRImageStatus.LOADING,
        progress: percent,
        url: typeof url === 'string' ? url : undefined,
      });
    }

    function onerror(status: number, ev: Event) {
      setState({
        ...defaultState,
        status: XHRImageStatus.FAILED,
        errorEvent: ev,
        errorStatus: status,
        url: typeof url === 'string' ? url : undefined,
      });
    }

    img.onload = onload;
    img.onerror = onerror;
    img.onprogress = onprogress;
    img.load(url);

    return () => {
      if (abortable && img.xhr) img.xhr.abort();
      setState(defaultState);
    };
  }, [url, setState, abortable]);

  return state;
}
