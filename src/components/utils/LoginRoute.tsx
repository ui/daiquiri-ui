import type { PropsWithChildren } from 'react';
import { Navigate, useLocation } from 'react-router-dom';
import type { Location } from 'react-router-dom';
import { assertLocationObj } from '../../helpers/assertions';

interface Props {
  isLoggedOut: boolean;
}

function LoginRoute(props: PropsWithChildren<Props>) {
  const { children, isLoggedOut } = props;
  const { state } = useLocation();

  if (isLoggedOut) {
    // Render login page
    return children;
  }

  if (state) {
    // Redirect to location previously saved by `PrivateRoute`
    assertLocationObj(state);
    return (
      <Navigate to={`${state.pathname}${state.search}${state.hash}`} replace />
    );
  }

  // Redirect to homepage (i.e. default layout)
  return <Navigate to="/" replace />;
}

export default LoginRoute;
