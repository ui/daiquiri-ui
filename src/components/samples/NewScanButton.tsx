import { useState, useRef, useCallback } from 'react';
import { keys } from 'lodash';

import { Button, Form, Col, Row } from 'react-bootstrap';
import NewScan from 'connect/2dview/NewScan';
import ButtonTriggerModalStoreConnect from 'connect/layout/ButtonTriggerModalStore';
import type { Sample, Config } from 'components/2dview/NewScan';
import type { ButtonTriggerModalStoreRef } from 'components/layout/models';

interface Props {
  objects: Sample[];
  config?: Config;
  type?: string;
  button?: Element | JSX.Element;
  buttonProps?: any;
  formData?: any;
  disabled?: boolean;
  inset?: boolean;
  prefix?: string;
}

function NewScanButton(props: Props) {
  const onSubmitRef = useRef((value: boolean) => {});
  const disabledRef = useRef(false);
  const modalRef = useRef<ButtonTriggerModalStoreRef>();
  const closeOnValidateInputRef = useRef<HTMLInputElement>(null);
  const [render, forceRender] = useState(true);
  const {
    objects = [],
    config = { scantypes: {} },
    button,
    buttonProps = {},
    formData = {},
    disabled = false,
    inset = false,
    prefix = '',
    type,
  } = props;

  const doOnSubmit = useCallback(() => {
    if (
      modalRef.current &&
      closeOnValidateInputRef.current &&
      closeOnValidateInputRef.current.checked
    ) {
      modalRef.current.close();
    }
  }, []);

  const hasObjects = keys(objects).length;
  return (
    <ButtonTriggerModalStoreConnect
      ref={modalRef}
      id={`newScan${prefix}`}
      button={
        button || (
          <>
            <i className="fa fa-plus me-1" />
            New
          </>
        )
      }
      buttonProps={{
        disabled: hasObjects === 0 || disabled,
        className: inset ? 'me-1' : 'float-end',
        ...buttonProps,
      }}
      otherButtons={
        <Form.Group as={Row}>
          <Col xs={1}>
            <Form.Check
              defaultChecked
              id="close-on-validate"
              ref={closeOnValidateInputRef}
            />
          </Col>
          <Form.Label column xs={11} htmlFor="close-on-validate">
            Close the form when validated
          </Form.Label>
        </Form.Group>
      }
      buttons={
        <>
          <Button
            onClick={() => onSubmitRef.current(true)}
            disabled={disabledRef.current}
          >
            Queue
          </Button>
          <Button
            onClick={() => onSubmitRef.current(false)}
            disabled={disabledRef.current || hasObjects > 1}
          >
            Execute now
          </Button>
        </>
      }
      title="New Scan"
    >
      <NewScan
        onSubmit={doOnSubmit}
        objects={objects}
        config={config}
        formData={{ ...formData }}
        /* @ts-expect-error */
        type={type}
        button={(d: boolean, os: (value: boolean) => void) => {
          disabledRef.current = d;
          onSubmitRef.current = os;
          forceRender(!render);
        }}
      />
    </ButtonTriggerModalStoreConnect>
  );
}

export default NewScanButton;
