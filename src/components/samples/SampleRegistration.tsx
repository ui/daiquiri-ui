import { Suspense } from 'react';
import { useToast } from '../utils/appHooks';
import SampleActionPositions from './registration/SampleActionPositions';
import SampleActions from './registration/SampleActions';
import TiledView from './registration/TiledView';
import type { TiledViewProps } from './registration/TiledViewProps';

export default function SampleRegistration(props: TiledViewProps) {
  const addToast = useToast();
  return (
    <div
      className="h-100"
      style={{
        display: 'flex',
        flexDirection: 'row',
      }}
    >
      <div
        style={{
          flex: '1 1 70%',
          display: 'flex',
          flexDirection: 'column',
          backgroundColor: '#efefef',
          borderRadius: '3px',
        }}
      >
        <TiledView {...props} addToast={addToast} />
      </div>

      <div
        style={{
          flex: '1 1 30%',
          padding: '0.5rem',
        }}
      >
        <Suspense fallback={<p>...</p>}>
          <SampleActions sampleid={props.currentSample} addToast={addToast} />
        </Suspense>
        <Suspense fallback={<p>...</p>}>
          <SampleActionPositions />
        </Suspense>
      </div>
    </div>
  );
}
