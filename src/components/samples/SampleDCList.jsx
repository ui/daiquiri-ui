import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { FullSizer } from '@esrf/daiquiri-lib';

import RemountOnResize from 'components/utils/RemountOnResize';

import DCList from 'components/dc/DCList';
import NewScanButtonSample from 'components/samples/NewScanButtonSample';
import SavingButton from 'connect/2dview/SavingButton';

import DCType from 'types/DCType';
import SampleType from 'types/SampleType';

function SampleDCList(props) {
  useEffect(() => {
    props.actions.setParams({ sampleid: props.currentSample });
  }, [props.currentSample, props.actions]);

  const { operator, currentSample, sample, ...dcProps } = props;

  return (
    <RemountOnResize>
      <FullSizer>
        <div className="object">
          <h5>
            Sample:{' '}
            {props.currentSample ? props.sample.name : 'Create a sample first'}
            <NewScanButtonSample
              sampleid={props.currentSample}
              disabled={!props.operator}
            />
            <SavingButton />
          </h5>

          <DCList
            pages
            filter
            scan
            {...dcProps}
            createScalarMap={false}
            namespace={`sampledc${props.dcnamespace}`}
          />
        </div>
      </FullSizer>
    </RemountOnResize>
  );
}

SampleDCList.defaultProps = {
  sample: {},
  operator: false,
  currentSample: null,
  dcnamespace: '',
  selectable: false
};

SampleDCList.propTypes = {
  currentSample: PropTypes.number,
  operator: PropTypes.bool,
  datacollections: PropTypes.arrayOf(PropTypes.shape(DCType.types)).isRequired,
  dcnamespace: PropTypes.string,
  sample: PropTypes.shape(SampleType.types),
  /** Whether to allow selection of a datacollection */
  selectable: PropTypes.bool,
  actions: PropTypes.shape({
    setParams: PropTypes.func.isRequired
  }).isRequired
};

export default SampleDCList;
