import { useState, useRef, useCallback } from 'react';
import type { MutableRefObject } from 'react';
import { Button, Row, Col, Form } from 'react-bootstrap';
import NewScan from 'connect/samples/NewScan';
import ButtonTriggerModalStoreConnect from 'connect/layout/ButtonTriggerModalStore';
import type { ButtonTriggerModalStoreRef } from 'components/layout/models';

function CustomButtons(props: {
  queueRunning?: boolean;
  queueReady?: boolean;
  onSubmitRef: MutableRefObject<(value: boolean) => void>;
  disabledRef: MutableRefObject<boolean>;
}) {
  function readDisableMsgFromQueue() {
    if (props.queueRunning) {
      return 'A job is already running in the queue';
    }
    if (!props.queueReady) {
      return 'The queue is not ready';
    }
    return '';
  }

  const disableMsgFromQueue = readDisableMsgFromQueue();

  return (
    <>
      <Button
        onClick={() => props.onSubmitRef.current(true)}
        disabled={props.disabledRef.current}
      >
        Queue
      </Button>
      <Button
        onClick={() => props.onSubmitRef.current(false)}
        disabled={props.disabledRef.current || disableMsgFromQueue !== ''}
        title={disableMsgFromQueue}
      >
        Execute now
      </Button>
    </>
  );
}

interface Props {
  sampleid: number | null;
  type?: string;
  tags?: string[];
  button?: Element | JSX.Element;
  buttonProps?: any;
  formData?: Record<string, any>;
  additionalFormData?: Record<string, any> | (() => Record<string, any>);
  disabled?: boolean;
  inset?: boolean;
  prefix?: string;
  queueRunning?: boolean;
  queueReady?: boolean;
}

function NewScanButtonSample(props: Props) {
  const onSubmitRef = useRef((value: boolean) => {});
  const disabledRef = useRef(false);
  const modalRef = useRef<ButtonTriggerModalStoreRef>(null);
  const closeOnValidateInputRef = useRef<HTMLInputElement>(null);
  const [render, forceRender] = useState(true);
  const {
    sampleid = null,
    type = undefined,
    tags = undefined,
    button = undefined,
    buttonProps = {},
    formData = {},
    additionalFormData = {},
    disabled = false,
    inset = false,
    prefix = '',
    queueRunning = false,
    queueReady = true,
  } = props;

  const doOnSubmit = useCallback(() => {
    if (
      modalRef.current &&
      closeOnValidateInputRef.current &&
      closeOnValidateInputRef.current.checked
    ) {
      modalRef.current.close();
    }
  }, []);

  return (
    <ButtonTriggerModalStoreConnect
      ref={modalRef}
      id={`newScanSample${prefix}`}
      button={
        button || (
          <>
            <i className="fa fa-plus me-1" />
            New Scan
          </>
        )
      }
      buttonProps={{
        disabled: disabled || !sampleid,
        className: inset ? 'me-1' : 'float-end',
        ...buttonProps,
      }}
      otherButtons={
        <Form.Group as={Row}>
          <Col xs={1}>
            <Form.Check
              defaultChecked
              id="close-on-validate"
              ref={closeOnValidateInputRef}
            />
          </Col>
          <Form.Label column xs={11} htmlFor="close-on-validate">
            Close the form when validated
          </Form.Label>
        </Form.Group>
      }
      buttons={
        <CustomButtons
          disabledRef={disabledRef}
          onSubmitRef={onSubmitRef}
          queueRunning={queueRunning}
          queueReady={queueReady}
        />
      }
      title="New Scan"
    >
      <NewScan
        onSubmit={doOnSubmit}
        sampleid={sampleid}
        formData={{ ...formData }}
        additionalFormData={additionalFormData}
        tags={tags}
        type={type}
        button={(d: boolean, os: (value: boolean) => void) => {
          disabledRef.current = d;
          onSubmitRef.current = os;
          forceRender(!render);
        }}
      />
    </ButtonTriggerModalStoreConnect>
  );
}

export default NewScanButtonSample;
