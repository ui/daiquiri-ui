import { createRef, Component } from 'react';
import PropTypes from 'prop-types';
import { keys, values } from 'lodash';

import { Button, Badge, Alert } from 'react-bootstrap';

import SelectableTable from 'components/table/Selectable';
import ButtonTriggerModalStore from 'connect/layout/ButtonTriggerModalStore';
import SchemaForm from 'connect/SchemaForm';
import InlineEditable from 'components/common/InlineEditable';
import ComponentView from 'connect/samples/ComponentView';
import ExtraMetadata from 'connect/2dview/ExtraMetadata';
import { TagCell } from 'components/2dview/ObjectList';

class EditSample extends Component {
  static defaultProps = {
    comments: undefined
  };

  static propTypes = {
    sampleSchema: PropTypes.shape({
      definitions: PropTypes.shape({
        SampleSchema: PropTypes.shape({
          properties: PropTypes.shape({
            name: PropTypes.shape({}),
            offsetx: PropTypes.shape({}),
            offsety: PropTypes.shape({}),
            comments: PropTypes.shape({}),
            componentid: PropTypes.shape({})
          }),
          uischema: PropTypes.shape({})
        })
      })
    }).isRequired,
    sampleid: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    offsetx: PropTypes.number.isRequired,
    offsety: PropTypes.number.isRequired,
    componentid: PropTypes.number.isRequired,
    component: PropTypes.string.isRequired,
    comments: PropTypes.string,
    onChange: PropTypes.func.isRequired
  };

  onChange = ({ field, value }) => {
    return this.props.onChange({
      sampleid: this.props.sampleid,
      [field]: value
    });
  };

  render() {
    if (!this.props.sampleSchema) {
      return <Alert>No sample schema defined</Alert>;
    }

    const { properties } = this.props.sampleSchema.definitions.SampleSchema;
    const schema = {
      name: properties.name,
      offsetx: properties.offsetx,
      offsety: properties.offsety,
      comments: properties.comments,
      componentid: properties.componentid
    };
    const formData = {
      name: this.props.name,
      offsetx: this.props.offsetx,
      offsety: this.props.offsety,
      comments: this.props.comments,
      componentid: this.props.componentid
    };

    const staticValues = {
      componentid: this.props.component
    };

    const extraComponents = {
      componentid: <ComponentView componentid={this.props.componentid} />
    };

    return (
      <ButtonTriggerModalStore
        id={`sampleDetail${this.props.sampleid}`}
        button={<i className="fa fa-search" />}
        buttonProps={{
          variant: 'info',
          size: 'sm'
        }}
        title="Sample Details"
      >
        <InlineEditable
          editable={['name', 'comments', 'offsetx', 'offsety', 'componentid']}
          staticValues={staticValues}
          extraComponents={extraComponents}
          schema={schema}
          formData={formData}
          onChange={this.onChange}
          uiSchema={this.props.sampleSchema.uischema}
        />
        <ExtraMetadata
          samples
          onChange={this.onChange}
          extraMetadata={this.props.extrametadata}
          providers={{
            metadata: {
              sampletags: {
                namespace: 'samples'
              }
            }
          }}
        />
      </ButtonTriggerModalStore>
    );
  }
}

function ActionCell(cell, row, rowIndex, formatExtraData) {
  return (
    <>
      <EditSample
        {...row}
        onChange={formatExtraData.updateSample}
        sampleSchema={formatExtraData.sampleSchema}
        updating={formatExtraData.updating}
      />
    </>
  );
}

function DataCell(cell, row) {
  const badges = [];

  if (row.datacollections > 0) {
    badges.push(
      <Badge bg="success" className="me-1" key="b1">
        Data <Badge bg="light">{row.datacollections}</Badge>
      </Badge>
    );
  }

  if (row.queued === true) {
    badges.push(
      <Badge bg="warning" key="b2">
        Queued
      </Badge>
    );
  }

  return badges;
}

export default class SampleList extends Component {
  static defaultProps = {
    currentSampleId: undefined,
    updating: false
  };

  static propTypes = {
    currentSampleId: PropTypes.number,
    samples: PropTypes.objectOf(
      PropTypes.shape({
        sampleid: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        subsamples: PropTypes.number,
        datacollections: PropTypes.number
      })
    ).isRequired,
    actions: PropTypes.shape({
      fetchSamples: PropTypes.func.isRequired,
      fetchComponents: PropTypes.func.isRequired,
      addSample: PropTypes.func.isRequired,
      updateSample: PropTypes.func.isRequired,
      addComponent: PropTypes.func.isRequired,
      addSelection: PropTypes.func.isRequired,
      removeSelection: PropTypes.func.isRequired,
      resetSelection: PropTypes.func.isRequired
    }).isRequired,
    sampleSchema: PropTypes.shape({}).isRequired,
    updating: PropTypes.bool
  };

  constructor(props) {
    super(props);
    this.modalRef = createRef();
    this.modalRefComp = createRef();

    this.state = {
      onSubmit: () => {},
      disabled: false,
      onSubmitComp: () => {},
      disabledComp: false
    };
  }

  componentDidMount() {
    this.props.actions.fetchComponents();
    this.props.actions.fetchSamples().then(() => {
      if (!this.props.currentSampleId) {
        const kys = keys(this.props.samples);
        if (kys.length) {
          this.selectSample(this.props.samples[kys[0]].sampleid);
        }
      }
    });
  }

  onSubmit = formData =>
    this.props.actions.addSample(formData).then(resp => {
      this.modalRef.current.close();
      if (resp.status === 201) {
        this.selectSample(resp.data.sampleid);
      }
      // FIXME: status 400 have to display an error
    });

  onSubmitComponent = formData =>
    this.props.actions
      .addComponent(formData)
      .then(() => this.modalRefComp.current.close());

  selectSample(sampleid) {
    this.props.actions.resetSelection();
    this.props.actions.addSelection({ sampleid });
  }

  render() {
    const columns = [
      { dataField: 'sampleid', text: 'id' },
      { dataField: 'name', text: 'Name' },
      { dataField: 'component', text: 'Component' },
      { dataField: 'extrametadata', text: 'Tags', formatter: TagCell },
      { dataField: 'subsamples', text: '#Sub' },
      { dataField: 'datacollections', text: '', formatter: DataCell },
      {
        dataField: 'action',
        text: '',
        formatter: ActionCell,
        classes: 'text-end',
        formatExtraData: {
          updateSample: this.props.actions.updateSample,
          sampleSchema: this.props.sampleSchema,
          updating: this.props.updating
        }
      }
    ];

    return (
      <div className="sample-list">
        <div>
          <ButtonTriggerModalStore
            id="newComponent"
            ref={this.modalRefComp}
            title="New Component"
            buttonProps={{ className: 'float-end mb-1' }}
            buttons={
              <Button
                onClick={() => this.state.onSubmitComp()}
                disabled={this.state.disabledComp}
              >
                Add Component
              </Button>
            }
            button={
              <>
                <i className="fa fa-plus" />
                Add Component
              </>
            }
          >
            <SchemaForm
              allowSubmitOnEnter
              onSubmit={this.onSubmitComponent}
              schema="NewComponentSchema"
              uiSchema={{
                name: {
                  'ui:autofocus': true
                }
              }}
              button={(disabled, onSubmit) =>
                this.setState({
                  disabledComp: disabled,
                  onSubmitComp: onSubmit
                })
              }
            />
          </ButtonTriggerModalStore>

          <ButtonTriggerModalStore
            id="newSample"
            ref={this.modalRef}
            title="New Sample"
            buttonProps={{ className: 'float-end mb-1  me-1' }}
            buttons={
              <Button
                onClick={() => {
                  this.state.onSubmit();
                }}
                disabled={this.state.disabled}
              >
                Add Sample
              </Button>
            }
            button={
              <>
                <i className="fa fa-plus" />
                Add Sample
              </>
            }
          >
            <SchemaForm
              allowSubmitOnEnter
              onSubmit={this.onSubmit}
              schema="NewSampleSchema"
              uiSchema={{
                name: {
                  'ui:autofocus': true
                }
              }}
              button={(disabled, onSubmit) =>
                this.setState({ disabled, onSubmit })
              }
            />
          </ButtonTriggerModalStore>
        </div>
        <SelectableTable
          localPerPage={15}
          keyField="sampleid"
          data={values(this.props.samples) || []}
          columns={columns}
          bordered={false}
          classes="table-sm"
          striped
          noDataIndication="No samples defined"
          selectedItems={this.props.selectedSamples}
          actions={{
            addSelection: this.props.actions.addSelection,
            removeSelection: this.props.actions.removeSelection,
            resetSelection: this.props.actions.resetSelection
          }}
        />
      </div>
    );
  }
}
