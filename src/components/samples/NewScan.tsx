import { createRef, Component } from 'react';
import { values, map, filter } from 'lodash';
import { Form, Row, Col, ListGroup } from 'react-bootstrap';
import { Formatting } from '@esrf/daiquiri-lib';
import SchemaForm from 'connect/SchemaForm';

interface Props {
  actions: {
    fetchScansTypes: () => Promise<void>;
    setSchema: (schema: any) => void;
  };
  onSubmit?: () => void;
  schema: string;
  button?: (d: boolean, os: (value: boolean) => void) => void;
  requestCloseModal: () => void;
  scans?: any;
  tags?: string[];
  sampleid?: number;
  formData?: Record<string, any>;
  additionalFormData?: Record<string, any> | (() => Record<string, any>);
}

interface State {
  asyncAdditional: any;
}

export default class NewScan extends Component<Props, State> {
  private readonly selectRef: React.RefObject<HTMLSelectElement>;

  public constructor(props: Props) {
    super(props);

    this.selectRef = createRef<HTMLSelectElement>();
    this.state = {
      asyncAdditional: {},
    };
  }

  public componentDidMount() {
    this.props.actions.fetchScansTypes().then(() => {
      this.changeSchema();
    });
  }

  public render() {
    const opts = map(this.filteredScans(), (s) => (
      <option key={s.actor} value={s.actor}>
        {s.actor}
      </option>
    ));

    return (
      <>
        <Form>
          <Form.Group as={Row}>
            <Form.Label column sm={3}>
              Type
            </Form.Label>
            <Col>
              <Form.Control
                ref={this.selectRef}
                as="select"
                value={this.props.schema}
                onChange={this.changeSchema}
              >
                {opts}
              </Form.Control>
            </Col>
          </Form.Group>
        </Form>
        <hr />
        {this.props.schema === '' ? null : (
          <>
            <SchemaForm
              schema={`${Formatting.ucfirst(this.props.schema)}Schema`}
              formData={{
                sampleid: this.props.sampleid,
                ...this.props.formData,
              }}
              additionalFormData={this.props.additionalFormData}
              onSubmit={this.onSubmit}
              requestCloseModal={this.props.requestCloseModal}
              requireOperator
              onAsyncValidate={this.onAsyncValidate}
              submitText="Execute now"
              queueText="Queue"
              button={this.props.button}
            />
            {this.state.asyncAdditional.time_estimate && (
              <ListGroup
                className="mt-1 mx-auto"
                style={{ bottom: 0, position: 'sticky', width: '50%' }}
              >
                <ListGroup.Item className="text-center">
                  Estimated Time:{' '}
                  {Formatting.toHoursMins(
                    this.state.asyncAdditional.time_estimate
                  )}
                </ListGroup.Item>
              </ListGroup>
            )}
          </>
        )}
      </>
    );
  }

  /**
   * Returns the available samplescans
   *
   * This is filtered by tags.
   */
  private filteredScans() {
    if (this.props.scans === undefined) {
      return {};
    }
    if (this.props.tags === undefined) {
      return this.props.scans;
    }
    const tags = new Set(this.props.tags);

    function subSet(set: Set<any>, otherSet: Set<any>) {
      if (set.size > otherSet.size) {
        return false;
      }
      for (const elem of set) {
        if (!otherSet.has(elem)) {
          return false;
        }
      }
      return true;
    }

    function containsAllTags(s: Set<any>) {
      if (!('tags' in s)) {
        return false;
      }
      // @ts-expect-error
      const scanTags = new Set(s.tags);
      return subSet(tags, scanTags);
    }
    return filter(this.props.scans, containsAllTags);
  }

  private readonly changeSchema = () => {
    const val = this.selectRef?.current?.value;
    // rjsf seems to do some internal caching of the definitions
    // part of the schema, passing a changed schema does not clear this
    // cached value, need to force a component remount on changing
    // schemas
    this.props.actions.setSchema('');
    setTimeout(() => this.props.actions.setSchema(val), 50);
  };

  private readonly onSubmit = (formData: any, e: any, doSubmit: any) => {
    return doSubmit({
      ...formData,
      sampleid: this.props.sampleid,
    }).then(() => {
      if (this.props.onSubmit) this.props.onSubmit();
    });
  };

  private readonly onAsyncValidate = (data: any) => {
    this.setState({ asyncAdditional: data.additionalAsync });
  };
}
