import { useController, useSuspense } from '@rest-hooks/react';

import type { SampleActionPosition } from 'resources/SampleActionPosition';
import { SampleActionPositionResource } from 'resources/SampleActionPosition';
import Table from 'components/table';
import DeleteButton from 'components/common/DeleteButton';
import { useSelectedEntities } from './store';

function Buttons({
  sampleactionpositionid,
}: {
  sampleactionpositionid: number;
}) {
  const { fetch } = useController();

  function onDelete() {
    fetch(SampleActionPositionResource.delete, { sampleactionpositionid });
  }

  return (
    <DeleteButton
      id={sampleactionpositionid}
      onClick={() => onDelete()}
      confirm
      show
    />
  );
}

function ActionCell(cell: string, row: SampleActionPosition) {
  return (
    row.sampleactionpositionid && (
      <Buttons sampleactionpositionid={row.sampleactionpositionid} />
    )
  );
}

export default function SampleActionPositions() {
  const { selectedSampleActionId } = useSelectedEntities();
  const sampleactionpositions = useSuspense(
    SampleActionPositionResource.getList,
    selectedSampleActionId ? { sampleactionid: selectedSampleActionId } : null
  );

  const columns = [
    { dataField: 'id', text: '#' },
    { dataField: 'type', text: 'Type' },
    { dataField: 'posx', text: 'X' },
    { dataField: 'posy', text: 'Y' },
    {
      dataField: '',
      text: '',
      formatter: ActionCell,
      classes: 'text-end',
    },
  ];

  return (
    <>
      <Table
        keyField="sampleactionpositionid"
        data={sampleactionpositions.rows || []}
        columns={columns}
        noDataIndication="No reference positions"
        overlay
      />
    </>
  );
}
