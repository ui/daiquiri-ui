import React, { useCallback } from 'react';
import { useSuspense, useController } from '@rest-hooks/react';
import { SampleActionPositionResource } from 'resources/SampleActionPosition';
import { useSelectedEntities } from './store';

export function withSampleActionPositions(
  WrappedComponent: React.ComponentType,
  type = 'real'
) {
  return (props: any) => {
    const { fetch, invalidateAll } = useController();
    const { selectedSampleActionId } = useSelectedEntities();
    const positionArgs: Record<string, any> = {
      sampleactionid: selectedSampleActionId,
      type,
    };
    const sampleActionPositions = useSuspense(
      SampleActionPositionResource.getList,
      selectedSampleActionId ? positionArgs : null
    );

    const addSampleActionPosition = useCallback(
      async ({ x, y }: { x: number; y: number }): Promise<any> => {
        const maxId =
          sampleActionPositions.rows && sampleActionPositions.rows.length > 0
            ? Math.max(...sampleActionPositions.rows.map((row) => row.id))
            : 0;

        const response = await fetch(SampleActionPositionResource.create, {
          posx: x,
          posy: y,
          sampleactionid: selectedSampleActionId,
          type,
          id: maxId + 1,
        });
        invalidateAll(SampleActionPositionResource.getList);
        return response;
      },
      [sampleActionPositions]
    );

    return (
      <WrappedComponent
        {...props}
        selectedSampleActionId={selectedSampleActionId}
        sampleActionPositions={sampleActionPositions.rows}
        addSampleActionPosition={addSampleActionPosition}
      />
    );
  };
}
