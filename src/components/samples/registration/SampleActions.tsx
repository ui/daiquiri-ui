import { useCallback, useState } from 'react';
import { useController, useSuspense } from '@rest-hooks/react';
import { Button } from 'react-bootstrap';

import type { SampleAction } from 'resources/SampleAction';
import { SampleActionResource } from 'resources/SampleAction';
import { SetReferenceMatrixResource } from 'resources/SetReferenceMatrix';
import { ExportToSampleImage } from 'resources/ExportToSampleImage';
import SelectableTable from 'components/table/Selectable';
import Tooltip from 'components/common/Tooltip';
import { ToastType } from './TiledViewProps';
import type { AddToast } from './TiledViewProps';
import { useSelectedEntities, useTiledViewConfig } from './store';
import type { ColumnDescription } from 'react-bootstrap-table-next';

interface MatrixButtonProps extends SampleAction {
  addToast: AddToast;
}

function SelectForMatrixButton({
  sampleactionid,
  addToast,
}: MatrixButtonProps) {
  const { visibleDomain } = useTiledViewConfig();
  const { fetch } = useController();
  const [selectMatrixLoading, setSelectMatrixLoading] =
    useState<boolean>(false);
  const [exportImageLoading, setExportImageLoading] = useState<boolean>(false);

  const onClick = useCallback(async () => {
    setSelectMatrixLoading(true);
    try {
      await fetch(SetReferenceMatrixResource.create, {
        sampleactionid,
      });
      addToast({
        type: ToastType.success,
        title: 'Matrix calculated',
        text: 'Transformation matrix successfully calculated',
      });
    } catch (error: any) {
      const json = await error.response.json();
      addToast({
        type: ToastType.error,
        title: "Couldn't calculate matrix",
        text: json.error,
      });
    }
    setSelectMatrixLoading(false);
  }, [sampleactionid]);

  const onExport = useCallback(async () => {
    setExportImageLoading(true);
    try {
      await fetch(ExportToSampleImage.create, {
        sampleactionid,
        crop: {
          x: visibleDomain.xVisibleDomain.map((value) =>
            Math.max(0, Math.round(value))
          ),
          y: visibleDomain.yVisibleDomain.map((value) =>
            Math.max(0, Math.round(value))
          ),
        },
      });
      addToast({
        type: ToastType.success,
        title: 'Reference Exported',
        text: 'Exported reference to sample image',
      });
    } catch (error: any) {
      const json = await error.response.json();
      addToast({
        type: ToastType.error,
        title: "Couldn't export reference to sample image",
        text: json.error,
      });
    }
    setExportImageLoading(false);
  }, [sampleactionid]);

  return (
    <>
      <Tooltip tooltip="Select this reference for coordinate transform">
        <Button
          size="sm"
          onClick={() => void onClick()}
          disabled={selectMatrixLoading}
        >
          {selectMatrixLoading && <i className="fa fa-spinner fa-spin" />}
          {!selectMatrixLoading && <i className="fa fa-th" />}
        </Button>
      </Tooltip>
      <Tooltip tooltip="Export this image transform to the 2d view">
        <Button
          size="sm"
          onClick={() => void onExport()}
          className="ms-1"
          disabled={exportImageLoading}
        >
          {exportImageLoading && <i className="fa fa-spinner fa-spin" />}
          {!exportImageLoading && <i className="fa fa-file-export" />}
        </Button>
      </Tooltip>
    </>
  );
}

function SelectForMatrix(
  cell: string,
  row: SampleAction,
  rowIndex: number,
  formatExtraData: any
) {
  return (
    <>
      {row.has_result && (
        <SelectForMatrixButton {...row} {...formatExtraData} />
      )}
    </>
  );
}

export default function SampleActions({
  sampleid,
  addToast,
}: {
  sampleid?: number;
  addToast: AddToast;
}) {
  const {
    selectedSampleActionId,
    setSelectedSampleActionId,
    setSelectedSampleActionHasResult,
  } = useSelectedEntities();
  const sampleactions = useSuspense(SampleActionResource.getList, {
    actiontype: 'reference',
    ...(sampleid ? { sampleid } : null),
  });

  const columns: ColumnDescription[] = [
    { dataField: 'sampleactionid', text: 'Id' },
    { dataField: 'status', text: 'Status' },
    { dataField: 'message', text: 'Message', classes: 'text-break' },
    {
      dataField: 'has_result',
      text: 'Image',
      formatter: (cell: string, row: SampleAction) =>
        row.has_result ? 'Yes' : 'No',
    },
    {
      text: '',
      dataField: '',
      formatter: SelectForMatrix,
      classes: 'text-end text-nowrap',
      formatExtraData: {
        addToast,
      },
    },
  ];

  return (
    <>
      <SelectableTable
        keyField="sampleactionid"
        data={sampleactions.rows || []}
        columns={columns}
        noDataIndication="No reference images"
        selectedItems={
          selectedSampleActionId !== undefined ? [selectedSampleActionId] : []
        }
        actions={{
          addSelection: (selection: Record<string, number>) => {
            const { sampleactionid } = selection;
            setSelectedSampleActionId(sampleactionid);
            const sampleaction = sampleactions.rows.filter(
              (sampleaction) => sampleaction.sampleactionid === sampleactionid
            );
            if (sampleaction.length > 0) {
              setSelectedSampleActionHasResult(sampleaction[0].has_result);
            }
          },
          removeSelection: (args: any) => undefined,
          resetSelection: () => undefined,
        }}
      />
    </>
  );
}
