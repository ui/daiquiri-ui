import type { ColorMap, CustomDomain, Domain } from '@h5web/lib';
import { ScaleType } from '@h5web/lib';
import { create } from 'zustand';
import type { VisViewpoint } from '../../h5web/models';

export interface VisibleDomain {
  xVisibleDomain: Domain;
  yVisibleDomain: Domain;
}

export interface SelectedEntities {
  selectedSampleActionHasResult: boolean;
  setSelectedSampleActionHasResult: (hasResult: boolean) => void;
  selectedSampleActionId: number | undefined;
  setSelectedSampleActionId: (id: number) => void;
  cameraViewport: VisViewpoint | undefined;
  setCameraViewport: (viewport: VisViewpoint) => void;
}

export const useSelectedEntities = create<SelectedEntities>((set) => ({
  selectedSampleActionId: undefined,
  setSelectedSampleActionId: (selectedSampleActionId: number) =>
    set({ selectedSampleActionId }),
  selectedSampleActionHasResult: false,
  setSelectedSampleActionHasResult: (hasResult: boolean) =>
    set({ selectedSampleActionHasResult: hasResult }),
  cameraViewport: undefined,
  setCameraViewport: (viewport: VisViewpoint) =>
    set({ cameraViewport: viewport }),
}));

export interface TiledViewConfig {
  currentAction: string;
  setCurrentAction: (action: string) => void;
  customDomain: CustomDomain;
  setCustomDomain: (customDomain: CustomDomain) => void;
  scaleType: ScaleType;
  setScaleType: (scaleType: ScaleType) => void;
  colorMap: ColorMap;
  setColorMap: (colorMap: ColorMap) => void;
  invertColorMap: boolean;
  setInvertColorMap: (invertColorMap: boolean) => void;
  doMove: boolean;
  setDoMove: (move: boolean) => void;
  visibleDomain: VisibleDomain;
  setVisibleDomain: (domain: VisibleDomain) => void;
}

export const useTiledViewConfig = create<TiledViewConfig>((set) => ({
  currentAction: 'pan',
  setCurrentAction: (action: string) => set({ currentAction: action }),
  customDomain: [0, 254],
  setCustomDomain: (customDomain: CustomDomain) => set({ customDomain }),
  scaleType: ScaleType.Linear,
  setScaleType: (scaleType: ScaleType) => set({ scaleType }),
  colorMap: 'Viridis',
  setColorMap: (colorMap: ColorMap) => set({ colorMap }),
  invertColorMap: false,
  setInvertColorMap: (invertColorMap: boolean) => set({ invertColorMap }),
  doMove: true,
  setDoMove: (doMove: boolean) => set({ doMove }),
  visibleDomain: {
    xVisibleDomain: [0, 0],
    yVisibleDomain: [0, 0],
  },
  setVisibleDomain: (domain: VisibleDomain) => set({ visibleDomain: domain }),
}));
