import { useCallback, useEffect, useState } from 'react';
import type { Box2 } from 'three';
import { Vector2 } from 'three';
import { useSafeDomain, useVisCanvasContext, useVisDomain } from '@h5web/lib';
import type { Domain, TilesApi } from '@h5web/lib';
import { MouseButton, Pan, TiledHeatmapMesh, Zoom } from '@h5web/lib';
import { useController, useSuspense } from '@rest-hooks/react';

import PointerClick from 'components/h5web/SelectionPoint';
import LinearVisCanvas from 'components/h5web/LinearVisCanvas';
import type { SampleActionPosition } from 'resources/SampleActionPosition';
import { SampleActionPositionResource } from 'resources/SampleActionPosition';
import { MoveToReferenceResource } from 'resources/MoveToReference';
import { CrossMarker } from 'components/h5web/items/shapes/CrossMarker';
import Label from 'components/h5web/items/Label';
import sassVariables from 'scss/variables.module.scss';
import { useRestService, useTilingInfo } from './hooks';
import type { VisibleDomain } from './store';
import { useSelectedEntities, useTiledViewConfig } from './store';
import TiledViewToolbar from './TiledViewToolbar';
import type { ReferenceBeam, AddToast } from './TiledViewProps';
import { ToastType } from './TiledViewProps';
import type { TiledViewProps } from './TiledViewProps';
import _debounce from 'lodash/debounce';
import { useThree } from '@react-three/fiber';
import VisViewpointRestore from '../../h5web/VisViewpointRestore';
import { LoadingMessage } from '../../h5web/items/LoadingMessage';
import type { H5grovePyramidTilesApi } from '../../../services/tiling/providers';

function SubSamples() {
  const x = 1000;
  const y = 1000;
  return (
    <CrossMarker
      x={x}
      y={y}
      color={sassVariables.primary}
      sizeInScreen={20}
      lineWidth={1}
    />
  );
}

function ReferenceBeamMarker({ beam }: { beam?: number[] }) {
  return (
    <>
      {beam && (
        <CrossMarker
          x={beam[0]}
          y={beam[1]}
          color={sassVariables.warning}
          sizeInScreen={20}
          lineWidth={1}
        />
      )}
    </>
  );
}

function ReferenceMarkers({
  positions,
}: {
  positions: SampleActionPosition[];
}) {
  return (
    <>
      {positions
        ?.filter(
          (position: SampleActionPosition) => position.type === 'reference'
        )
        .map((position: SampleActionPosition) => (
          <>
            <CrossMarker
              key={position.sampleactionpositionid}
              x={position.posx}
              y={position.posy}
              color="magenta"
              sizeInScreen={20}
              lineWidth={1}
            />
            <Label
              color="magenta"
              datapos={[position.posx, position.posy]}
              text={`${position.id}`}
            />
          </>
        ))}
    </>
  );
}

function Interactions({
  currentAction,
  setReference,
  move,
}: {
  currentAction: string;
  setReference: (x: number, y: number) => Promise<any>;
  move: (x: number, y: number) => void;
}) {
  return (
    <>
      {currentAction === 'reference' && (
        <PointerClick
          onClick={(event) =>
            void setReference(
              Math.round(event.dataPt.x),
              Math.round(event.dataPt.y)
            )
          }
        />
      )}
      {currentAction === 'pan' && (
        <>
          <Pan button={MouseButton.Left} />
          <Zoom />
        </>
      )}
      {currentAction === 'move' && (
        <PointerClick
          onClick={(event) =>
            move(Math.round(event.dataPt.x), Math.round(event.dataPt.y))
          }
        />
      )}
    </>
  );
}

function GetVisibleDomain() {
  const { getVisibleDomains } = useVisCanvasContext();
  const camera = useThree((state) => state.camera);
  const { setVisibleDomain } = useTiledViewConfig();

  function doStoreVisibleDomain(domains: VisibleDomain) {
    setVisibleDomain(domains);
  }

  const debounceStoreVisibleDomain = useCallback(
    _debounce(doStoreVisibleDomain, 200),
    []
  );

  const visibleDomains = getVisibleDomains(camera);
  debounceStoreVisibleDomain(visibleDomains);
  return null;
}

interface TilingProps {
  api: H5grovePyramidTilesApi;
  tilingBox: Box2;
  dataDomain: Domain;
  sampleactionid: number;
  referenceBeam?: ReferenceBeam;
  addToast: AddToast;
}

function SampleRegistrationTiling(props: TilingProps) {
  const {
    api,
    tilingBox,
    dataDomain,
    sampleactionid,
    referenceBeam,
    addToast,
  } = props;
  const { fetch, invalidateAll } = useController();

  const size = tilingBox.getSize(new Vector2());
  const center = tilingBox.getCenter(new Vector2());

  const {
    colorMap,
    scaleType,
    invertColorMap,
    customDomain,
    currentAction,
    setCurrentAction,
    doMove,
  } = useTiledViewConfig();

  const [pendingTiles, setPendingTiles] = useState<number>(0);
  useEffect(() => {
    const updateTimeout = setInterval(() => {
      const pending = api.getPendingTilesCount();
      if (pending !== pendingTiles) {
        setPendingTiles(pending);
      }
    }, 1000);
    return () => {
      clearTimeout(updateTimeout);
    };
  }, [api, pendingTiles]);

  const { cameraViewport, setCameraViewport } = useSelectedEntities();

  const visDomain = useVisDomain(customDomain, dataDomain);
  const [safeDomain] = useSafeDomain(visDomain, dataDomain, scaleType);

  const positionArgs = {
    sampleactionid,
    type: 'reference',
  };
  const sampleactionpositions = useSuspense(
    SampleActionPositionResource.getList,
    positionArgs
  );

  const setReference = useCallback(
    async (x: number, y: number) => {
      const maxId =
        sampleactionpositions.rows.length > 0
          ? Math.max(...sampleactionpositions.rows.map((row) => row.id))
          : 0;

      await fetch(SampleActionPositionResource.create, {
        posx: x,
        posy: y,
        sampleactionid,
        type: 'reference',
        id: maxId + 1,
      });
      invalidateAll(SampleActionPositionResource.getList);
      setCurrentAction('pan');
    },
    [sampleactionpositions]
  );

  const move = useCallback(
    async (x: number, y: number) => {
      try {
        const resp = await fetch(MoveToReferenceResource.create, {
          x,
          y,
          execute: doMove,
        });

        addToast({
          type: ToastType.info,
          title: 'Move To Poisiton',
          text: `${resp.positions.x.motor}: ${resp.positions.x.destination}, ${resp.positions.y.motor}: ${resp.positions.y.destination}`,
        });
      } catch (error: any) {
        const json = await error.response.json();
        addToast({
          type: ToastType.error,
          title: "Couldn't move to position",
          text: json.error,
        });
      }
    },
    [sampleactionid, doMove]
  );

  return (
    <div
      style={{
        flex: '1',
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <LinearVisCanvas
        abscissaConfig={{
          visDomain: [0, size.x],
        }}
        ordinateConfig={{
          visDomain: [0, size.y],
          flip: true,
        }}
      >
        <LoadingMessage
          info={pendingTiles > 0 ? `Waiting for ${pendingTiles} tiles` : ''}
        />
        <VisViewpointRestore
          viewpoint={cameraViewport}
          setViewpoint={setCameraViewport}
        />
        <group position={[center.x, center.y, 0]}>
          <TiledHeatmapMesh
            size={{ width: size.x, height: size.y }}
            api={api}
            colorMap={colorMap}
            invertColorMap={invertColorMap}
            domain={safeDomain}
            scaleType={scaleType}
            displayLowerResolutions={false}
            qualityFactor={0}
          />
        </group>
        <GetVisibleDomain />
        <ReferenceBeamMarker beam={referenceBeam?.beam} />
        <ReferenceMarkers positions={sampleactionpositions.rows} />
        <SubSamples />
        <Interactions
          currentAction={currentAction}
          setReference={setReference}
          move={(...args) => void move(...args)}
        />
      </LinearVisCanvas>
    </div>
  );
}

export default function TiledView(props: TiledViewProps) {
  const { selectedSampleActionId, selectedSampleActionHasResult } =
    useSelectedEntities();
  const restService = useRestService({
    sampleactionid: selectedSampleActionId,
  });
  const { api, tilingBox, histogram } = useTilingInfo(restService);
  const dataDomain: Domain = [0, 254];
  if (!selectedSampleActionId) {
    return <p style={{ padding: '10px' }}>Please select a reference image</p>;
  }

  if (!selectedSampleActionHasResult) {
    return (
      <p style={{ padding: '10px' }}>
        Selected reference does not have an image
      </p>
    );
  }
  if (!api) return null;

  return (
    <>
      <TiledViewToolbar
        dataDomain={dataDomain}
        histogram={histogram}
        {...props}
      />
      {selectedSampleActionId && selectedSampleActionHasResult && (
        <SampleRegistrationTiling
          sampleactionid={selectedSampleActionId}
          dataDomain={dataDomain}
          api={api}
          tilingBox={tilingBox}
          referenceBeam={props.sources?.['1']?.reference}
          addToast={props.addToast}
        />
      )}
    </>
  );
}
