import { useEffect, useState } from 'react';
import { Box2, Vector2 } from 'three';
import type { TilesApi, HistogramParams as Histogram } from '@h5web/lib';
import axios from 'axios';
import type { AxiosInstance } from 'axios';

import baseUrl from 'helpers/baseUrl';
import RestService from 'services/RestService';
import { H5grovePyramidTilesApi } from 'services/tiling/providers';
import { fetchH5grovePyramidMetadata } from 'services/tiling/fetch';

const DEFAULT_HISTOGRAM = {
  values: new Float32Array([0]),
  bins: new Float32Array([0, 255]),
};

interface TilingInfo {
  api: H5grovePyramidTilesApi | undefined;
  histogram: Histogram;
  tilingBox: Box2;
}

export function useTilingInfo(
  restService: AxiosInstance | undefined
): TilingInfo {
  const tileSize = { width: 256, height: 256 };
  const [api, setApi] = useState<H5grovePyramidTilesApi | undefined>(undefined);
  const [tilingBox, setTilingBox] = useState<Box2>(
    new Box2(new Vector2(), new Vector2(1, 1))
  );
  const [histogram, setHistogram] = useState<Histogram>(DEFAULT_HISTOGRAM);

  useEffect(() => {
    async function getConfig() {
      if (!restService) return;
      const config = await fetchH5grovePyramidMetadata(
        restService,
        'sample_registration',
        'sample_registration'
      );

      const { histogram, layers, yDomain, zDomain } = config;
      const apiInstance = new H5grovePyramidTilesApi(tileSize, layers);
      setApi(apiInstance);
      setHistogram(histogram);
      setTilingBox(
        new Box2(
          new Vector2(yDomain[0], zDomain[0]),
          new Vector2(yDomain[1], zDomain[1])
        )
      );
    }

    getConfig();
  }, [restService]);

  return {
    api,
    histogram,
    tilingBox,
  };
}

export function useRestService({
  sampleactionid,
}: {
  sampleactionid?: number;
}): AxiosInstance | undefined {
  const [service, setService] = useState<AxiosInstance | undefined>(undefined);

  useEffect(() => {
    setService(() =>
      axios.create({
        baseURL: baseUrl(),
        params: {
          sampleactionid,
        },
        ...RestService.getOptions(),
      })
    );
  }, [RestService.token, sampleactionid]);

  return service;
}
