export enum ToastType {
  error = 'error',
  warning = 'warning',
  success = 'success',
  info = 'info',
}

export interface Toast {
  type: ToastType;
  title: string;
  text: string;
}

export type AddToast = (toast: Toast) => void;

export interface ReferenceBeam {
  beam: number[];
}

interface Source {
  reference: ReferenceBeam;
  origin: boolean;
}

export interface TiledViewProps {
  operator: boolean;
  runningScan: string | undefined;
  currentSample: number | undefined;
  sources: Record<string, Source>;
  addToast: AddToast;
}
