import { ScaleType } from '@h5web/lib';
import { ColorMapSelector, DomainSlider, ScaleSelector } from '@h5web/lib';
import type { Domain, HistogramParams as Histogram } from '@h5web/lib';
import {
  Navbar,
  Form,
  ButtonToolbar,
  ToggleButton,
  ToggleButtonGroup,
} from 'react-bootstrap';

import Tooltip from 'components/common/Tooltip';
import type { TiledViewProps } from './TiledViewProps';
import { useTiledViewConfig } from './store';

interface ToolbarProps extends TiledViewProps {
  histogram: Histogram;
  dataDomain: Domain;
}

export default function TiledViewToolbar(props: ToolbarProps) {
  const {
    currentAction,
    setCurrentAction,
    colorMap,
    setColorMap,
    scaleType,
    setScaleType,
    invertColorMap,
    setInvertColorMap,
    setCustomDomain,
    customDomain,
    doMove,
    setDoMove,
  } = useTiledViewConfig();

  const { dataDomain, histogram, currentSample, operator, runningScan } = props;
  return (
    <Navbar bg="cyan" className="p-2" style={{ zIndex: 99 }}>
      <Form>
        <ButtonToolbar>
          <ToggleButtonGroup
            type="radio"
            name="action"
            value={currentAction}
            onChange={(value) => setCurrentAction(value)}
            className="me-1"
          >
            <ToggleButton
              id="reference"
              value="reference"
              size="sm"
              disabled={!currentSample}
            >
              <i className="fa fa-map-marker me-1" />
              Reference
            </ToggleButton>
            <ToggleButton
              id="poi"
              value="poi"
              size="sm"
              disabled={!currentSample}
            >
              <i className="fa fa-map-pin me-1" />
              POI
            </ToggleButton>
            <ToggleButton id="pan" value="pan" size="sm">
              <i className="fa fa-arrows me-1" />
              Pan
            </ToggleButton>
            <ToggleButton
              id="move"
              value="move"
              size="sm"
              disabled={!operator || !!runningScan}
            >
              <i className="fa fa-map-marker me-1" />
              Move
              <Tooltip tooltip="Enable Move">
                <Form.Check
                  className="d-inline ms-1"
                  type="checkbox"
                  checked={doMove}
                  value="1"
                  onChange={() => setDoMove(!doMove)}
                />
              </Tooltip>
            </ToggleButton>
          </ToggleButtonGroup>
        </ButtonToolbar>
      </Form>

      <DomainSlider
        dataDomain={dataDomain}
        customDomain={customDomain}
        scaleType={scaleType}
        onCustomDomainChange={(domain) => setCustomDomain(domain)}
        histogram={histogram}
      />

      <ScaleSelector
        value={scaleType}
        onScaleChange={setScaleType}
        options={[ScaleType.Linear, ScaleType.SymLog, ScaleType.Sqrt]}
      />

      <ColorMapSelector
        value={colorMap}
        onValueChange={setColorMap}
        invert={invertColorMap}
        onInversionChange={() => setInvertColorMap(!invertColorMap)}
      />
    </Navbar>
  );
}
