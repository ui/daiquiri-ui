import { keys } from 'lodash';
import NewScanButton from 'components/samples/NewScanButton';
import NewScanButtonSample from 'components/samples/NewScanButtonSample';
import type { Sample, Config } from 'components/2dview/NewScan';

interface Props {
  objects: Sample[];
  subsampleid?: number;
  sampleid?: number;
  inset?: boolean;
  config?: Config;
  args?: any;
  type?: string;
  disabled: boolean;
}

export default function CopyScan(props: Props) {
  function getObject(props: Props) {
    if (!props.objects) {
      return null;
    }
    if (!props.subsampleid) {
      return null;
    }
    return props.objects[props.subsampleid] ?? null;
  }

  const obj = getObject(props);
  const objs = obj ? [obj] : [];
  const { sampleid = null } = props;

  return props.subsampleid ? (
    <NewScanButton
      prefix="copy"
      inset={props.inset}
      config={props.config}
      objects={objs}
      formData={props.args}
      type={props.type}
      button={<i className="fa fa-copy" />}
      buttonProps={{
        size: 'sm',
        disabled: props.disabled || keys(obj).length === 0,
      }}
    />
  ) : (
    <NewScanButtonSample
      prefix="copy"
      sampleid={props.sampleid || null}
      inset={props.inset}
      formData={props.args}
      type={props.type}
      button={<i className="fa fa-copy" />}
      buttonProps={{ size: 'sm', disabled: props.disabled }}
    />
  );
}
