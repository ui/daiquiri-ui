import { useState } from 'react';
import { map, each } from 'lodash';

import { Button, DropdownButton, Row, Col } from 'react-bootstrap';

function HideShowButton(props: {
  onChange: (args: { sampleimageid: number; hide: boolean }) => void;
  hide?: boolean;
  sampleimageid: number;
}) {
  const { hide = false } = props;

  const onClick = () => {
    props.onChange({
      sampleimageid: props.sampleimageid,
      hide: !hide,
    });
  };

  return (
    <Button
      className="ms-auto"
      title={!hide ? 'Hide' : 'Show'}
      size="sm"
      onClick={onClick}
      variant={!hide ? 'primary' : 'secondary'}
    >
      <i className={`fa fa-${!hide ? 'eye' : 'eye-slash'}`} />
    </Button>
  );
}

interface Props {
  images: Record<
    string,
    {
      sampleimageid: number;
      offsetx: number;
      offsety: number;
    }
  >;
  actions: {
    updateImage: (args: { sampleimageid: number; hide: boolean }) => void;
  };
}

function ImageSelection(props: Props) {
  const [toggle, setToggle] = useState(true);

  const toggleAll = () => {
    each(props.images, (image) => {
      props.actions.updateImage({
        sampleimageid: image.sampleimageid,
        hide: toggle,
      });
    });
    setToggle(!toggle);
  };

  return (
    <div className="d-grid gap-2 mb-1">
      <DropdownButton
        align="end"
        size="sm"
        title={
          <>
            <i className="fa fa-eye me-1" />
            Visibility
          </>
        }
        className="image-selection"
      >
        {/* <Container> */}
        <Row>
          <Col xs={10}>Show/Hide All Images</Col>
          <Col xs={2}>
            <Button
              size="sm"
              onClick={toggleAll}
              variant={!toggle ? 'primary' : 'secondary'}
              className="ms-auto d-inline-block"
            >
              <i className={`fa fa-${toggle ? 'eye-slash' : 'eye'}`} />
            </Button>
          </Col>
        </Row>
        {map(props.images, (image) => (
          <Row key={image.sampleimageid}>
            <Col xs={2}>{image.sampleimageid}</Col>
            <Col xs={4}>{image.offsetx}</Col>
            <Col xs={4}>{image.offsety}</Col>
            <Col xs={2}>
              <HideShowButton {...image} onChange={props.actions.updateImage} />
            </Col>
          </Row>
        ))}
        {/* </Container> */}
      </DropdownButton>
    </div>
  );
}

export default ImageSelection;
