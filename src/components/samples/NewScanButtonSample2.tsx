import { useEffect } from 'react';
import NewScanButtonSample from 'components/samples/NewScanButtonSample';

interface Props {
  currentSample?: number;
  operator?: boolean;
  actions?: {
    setParams: (params: { sampleid: number | undefined }) => void;
  };
  formData?: Record<string, any>;
  queueRunning?: boolean;
  queueReady?: boolean;
  button?: JSX.Element | Element;
  additionalFormData?: Record<string, any> | (() => Record<string, any>);
  options: {
    tags?: string[];
  };
  disabled?: boolean;
}

export default function NewScanButtonSample2(props: Props) {
  useEffect(() => {
    props.actions?.setParams({ sampleid: props.currentSample });
  }, [props.currentSample, props.actions]);
  const {
    operator = false,
    currentSample = null,
    formData,
    options,
    button = undefined,
    additionalFormData,
    disabled = false,
  } = props;
  const { tags = undefined } = options;

  return (
    <NewScanButtonSample
      sampleid={currentSample}
      disabled={disabled || !operator}
      formData={{ ...formData }}
      additionalFormData={additionalFormData}
      tags={tags}
      button={button}
      inset
      buttonProps={{ style: { display: 'block', width: '100%' } }}
      queueRunning={props.queueRunning}
      queueReady={props.queueReady}
    />
  );
}
