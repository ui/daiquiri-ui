import { each } from 'lodash';

import ButtonTriggerModalStore from 'connect/layout/ButtonTriggerModalStore';
import InlineEditable from 'components/common/InlineEditable';

interface Props {
  componentSchema: {
    definitions?: {
      ComponentSchema?: {
        properties?: Record<string, any>;
        uischema?: Record<string, any>;
      };
    };
  };
  componentid: number;
  component?: Record<string, any>;
  actions: {
    updateComponent: (args: Record<string, any>) => Promise<void>;
  };
}

function ComponentView(props: Props) {
  const properties =
    props.componentSchema.definitions?.ComponentSchema?.properties ?? {};
  const editable = ['name', 'acronym', 'molecularmass', 'density', 'sequence'];

  const schema: Record<string, any> = {};
  each(editable, (e) => {
    schema[e] = properties[e];
  });

  const formData: Record<string, any> = {};
  each(editable, (e) => {
    formData[e] = props.component?.[e];
  });

  const onChange = (e: { field: string; value: any }) => {
    return props.actions.updateComponent({
      componentid: props.componentid,
      [e.field]: e.value,
    });
  };

  return (
    <ButtonTriggerModalStore
      id={`componentDetail${props.componentid}`}
      button={<i className="fa fa-search" />}
      buttonProps={{
        variant: 'info',
        size: 'sm',
      }}
      title="Component Details"
    >
      <InlineEditable
        editable={editable}
        schema={schema}
        formData={formData}
        onChange={onChange}
      />
    </ButtonTriggerModalStore>
  );
}

export default ComponentView;
