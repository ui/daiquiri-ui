import { useEffect, useState, useRef } from 'react';
import { debounce } from 'lodash';

import {
  Row,
  Col,
  Button,
  OverlayTrigger,
  Popover,
  Form,
} from 'react-bootstrap';

import Table from 'components/table';
import PlotEnhancer from 'components/PlotEnhancer';
import DeleteButton from 'components/common/DeleteButton';
import type { RowSelectionType } from 'react-bootstrap-table-next';

interface IMapHistogram {
  bins: number[];
  hist: number[];
  width: number[];
}

interface IMap {
  mapid: number;
  subsampleid: number;
  w: number;
  h: number;
  data: number[];
  opacity: number;
  min: number;
  max: number;
  scale: string;
  colourmap: string;
  histogram: IMapHistogram;
}

interface IMapHistogramView {
  histogram: IMapHistogram;
  fetchHistogram: (payload?: Record<string, any>) => Promise<any>;
  updateSelection: (payload: any) => Promise<any>;
  autoscaleMap: () => Promise<any>;
}

function MapHistogram(props: IMapHistogramView) {
  const { histogram, autoscaleMap, fetchHistogram, updateSelection } = props;
  const [autoscaled, setAutoscaled] = useState<boolean>(false);

  useEffect(() => {
    fetchHistogram();
  }, []);

  function autoscaleHistogram() {
    setAutoscaled(() => {
      const newScaled = !autoscaled;
      fetchHistogram({ autoscale: newScaled });
      return newScaled;
    });
  }

  function onSelected(e: any) {
    updateSelection({
      min: Number.parseFloat(e.range.x[0]),
      max: Number.parseFloat(e.range.x[1]),
    });
  }

  function onReset() {
    const { bins } = histogram;
    if (bins.length > 0) {
      updateSelection({
        min: bins[0],
        max: bins[bins.length - 1],
      });
    }
  }

  if (!histogram) return null;

  const { bins, hist, width } = histogram;
  const data = [
    {
      x: bins,
      y: hist,
      //   width,
      type: 'bar',
    },
  ];

  return (
    <>
      <div style={{ height: 180, position: 'relative' }}>
        <div style={{ position: 'absolute', right: 0, zIndex: 1 }}>
          <Button
            active={autoscaled}
            size="sm"
            onClick={() => autoscaleHistogram()}
            title="Autoscale Histogram"
          >
            <i
              className={`fa ${
                autoscaled
                  ? 'fa-down-left-and-up-right-to-center'
                  : 'fa-up-right-and-down-left-from-center'
              }`}
            />
          </Button>
        </div>
        <PlotEnhancer
          // @ts-expect-error

          data={data}
          layout={{
            dragmode: 'select',
            selectdirection: 'h',
            uirevision: 1,
          }}
          onSelected={onSelected}
        />
      </div>
      <Row className="mb-1">
        <Col>
          <div className="d-grid gap-2">
            <Button onClick={() => onReset()}>Reset</Button>
          </div>
        </Col>
        <Col>
          <div className="d-grid gap-2">
            <Button onClick={() => void autoscaleMap()}>Autoscale</Button>
          </div>
        </Col>
      </Row>
    </>
  );
}

interface IOverlayUpdater {
  map: IMap;
  histogram: IMapHistogram;
  onChange: (payload: Record<string, any>) => Promise<any>;
  fetchHistogram: (payload: Record<string, any>) => Promise<any>;
}

function OverlayUpdater(props: IOverlayUpdater) {
  const { map, histogram, onChange: doOnChange, fetchHistogram } = props;

  const minRef = useRef<HTMLInputElement>(null);
  const maxRef = useRef<HTMLInputElement>(null);
  const onChangeDebounced = debounce(doOnChange, 200);

  useEffect(() => {
    if (minRef.current) minRef.current.value = String(map.min);
  }, [map.min]);
  useEffect(() => {
    if (maxRef.current) maxRef.current.value = String(map.max);
  }, [map.max]);

  function onChange(key: string, toFloat: boolean, e: any) {
    const val = e.target.value;
    if (key === 'min') {
      if (val > map.max) return;
    }
    if (key === 'max') {
      if (val < map.min) return;
    }
    onChangeDebounced({
      mapid: map.mapid,
      [key]: toFloat ? Number.parseFloat(val) : val,
    });
  }

  function autoscaleMap() {
    return doOnChange({
      mapid: map.mapid,
      autoscale: true,
    });
  }

  function updateSelection(selection: any) {
    return doOnChange({
      mapid: map.mapid,
      min: selection.min,
      max: selection.max,
    });
  }
  const updateSelectionDebounced = debounce(updateSelection, 500);

  return (
    <OverlayTrigger
      trigger="click"
      placement="left"
      rootClose
      overlay={
        <Popover>
          <Popover.Header>Map Controls</Popover.Header>
          <Popover.Body>
            <Form.Group>
              <Form.Label>Opacity</Form.Label>
              <Form.Control
                type="range"
                min="0"
                max="1"
                step="0.01"
                defaultValue={map.opacity}
                onChange={(e) => onChange('opacity', true, e)}
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Color Map</Form.Label>
              <Form.Control
                as="select"
                onChange={(e) => onChange('colourmap', false, e)}
                value={map.colourmap}
              >
                <option>viridis</option>
                <option>jet</option>
                <option>binary</option>
                <option>gray</option>
              </Form.Control>
            </Form.Group>

            <Form.Group>
              <Form.Label>Scaling</Form.Label>
              <Form.Control
                as="select"
                onChange={(e) => onChange('scale', false, e)}
                value={map.scale}
              >
                <option>linear</option>
                <option>logarithmic</option>
              </Form.Control>
            </Form.Group>

            <Form.Group className="mb-0">
              <Form.Label>Histogram</Form.Label>
              <MapHistogram
                histogram={histogram}
                // @ts-expect-error
                updateSelection={updateSelectionDebounced}
                autoscaleMap={() => autoscaleMap()}
                fetchHistogram={(payload: any) =>
                  fetchHistogram({
                    mapid: map.mapid,
                    ...payload,
                  })
                }
              />
            </Form.Group>

            <Form.Group as={Row} className="no-gutters mb-0">
              <Col>
                <Form.Control
                  type="text"
                  onChange={(e) => onChange('min', true, e)}
                  defaultValue={map.min}
                  ref={minRef}
                  placeholder="Min"
                />
              </Col>
              <Col>
                <Form.Control
                  type="text"
                  onChange={(e) => onChange('max', true, e)}
                  defaultValue={map.max}
                  ref={maxRef}
                  placeholder="Max"
                />
              </Col>
            </Form.Group>
          </Popover.Body>
        </Popover>
      }
    >
      <Button title="Map Controls" size="sm">
        <i className="fa fa-sun-o" />
      </Button>
    </OverlayTrigger>
  );
}

interface IHideShowButton {
  onChange: (payload: Record<string, any>) => Promise<any>;
  mapid: number;
  opacity: number;
}

function HideShowButton(props: IHideShowButton) {
  const { opacity, onChange, mapid } = props;

  function onClick() {
    onChange({
      mapid,
      opacity: opacity ? 0 : 1,
    });
  }

  return (
    <Button
      className="me-1"
      title={opacity ? 'Hide' : 'Show'}
      size="sm"
      onClick={onClick}
      variant={opacity ? 'primary' : 'secondary'}
    >
      <i className={`fa fa-${opacity ? 'eye' : 'eye-slash'}`} />
    </Button>
  );
}

function ActionCell(
  cell: any,
  row: any,
  rowIndex: number,
  formatExtraData: any
) {
  return (
    <>
      <HideShowButton {...row} onChange={formatExtraData.updateMap} />
      <OverlayUpdater
        map={row}
        onChange={formatExtraData.updateMap}
        fetchHistogram={formatExtraData.fetchHistogram}
        histogram={formatExtraData.mapHistograms[row.mapid]?.histogram}
      />
      <DeleteButton
        id={row.mapid}
        onClick={formatExtraData.deleteMap}
        confirm
        show={row.composites === 0}
      />
    </>
  );
}

function ROICell(cell: any, row: any) {
  return row.scalar || `${row.element}-${row.edge}`;
}

interface IMapList {
  maps: IMap[];
  mapHistograms: Record<string, any>;
  actions: {
    updateMap: (payload: Record<string, any>) => Promise<any>;
    selectMap: ({ mapid, index }: { mapid: number; index: number }) => void;
    unselectMap: (mapid: number) => void;
    deleteMap: (mapid: number) => Promise<any>;
    fetchHistogram: (payload: Record<string, any>) => Promise<any>;
  };
}

export default function MapList(props: IMapList) {
  const { actions, maps, mapHistograms } = props;
  const setSelection = (row: any, isSelect: boolean, rowIndex: number) => {
    if (isSelect) {
      actions.selectMap({ mapid: row.mapid, index: rowIndex });
    } else {
      actions.unselectMap(row.mapid);
    }
  };

  const columns = [
    { dataField: 'mapid', text: 'id' },
    { dataField: 'datacollectionid', text: 'DC' },
    {
      dataField: 'element',
      text: 'ROI',
      formatter: ROICell,
      classes: 'text-break',
    },
    { dataField: 'w', text: 'Px' },
    { dataField: 'h', text: 'Py' },
    {
      dataField: '',
      text: '',
      formatter: ActionCell,
      formatExtraData: {
        updateMap: actions.updateMap,
        deleteMap: actions.deleteMap,
        mapHistograms,
        fetchHistogram: actions.fetchHistogram,
      },
      classes: 'text-end text-nowrap',
    },
  ];

  const selectRow = {
    mode: 'checkbox' as RowSelectionType,
    hideSelectAll: true,
    onSelect: setSelection,
  };

  return (
    <Table
      keyField="mapid"
      data={props.maps || []}
      columns={columns}
      noDataIndication="No maps for this object"
      selectRow={selectRow}
    />
  );
}
