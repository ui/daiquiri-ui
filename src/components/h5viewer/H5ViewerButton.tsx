import H5Viewer from './H5Viewer';
import ButtonTriggerModalStore from 'connect/layout/ButtonTriggerModalStore';
import { useState } from 'react';

interface Props {
  datacollectionid?: number;
  autoprocprogramattachmentid?: number;
  buttonProps?: Record<string, any>;
  fileName: string;
}

function H5ViewerButton(props: Props) {
  const {
    datacollectionid,
    buttonProps,
    autoprocprogramattachmentid,
    fileName,
  } = props;
  const [showH5Web, setShowH5Web] = useState(false);

  if (!datacollectionid && !autoprocprogramattachmentid) {
    return <p>No file selected</p>;
  }

  return (
    <ButtonTriggerModalStore
      id={`h5view${datacollectionid || autoprocprogramattachmentid}`}
      button={<i className="fa fa-search" />}
      title="Explore in H5Web"
      onShow={() => setShowH5Web(true)}
      onClose={() => setShowH5Web(false)}
      modalBodyClassName="h5view-container"
      buttonProps={{ ...buttonProps, size: 'sm' }}
      size="xl"
      fullscreen
    >
      {showH5Web && (
        <H5Viewer
          datacollectionid={datacollectionid}
          autoprocprogramattachmentid={autoprocprogramattachmentid}
          fileName={fileName}
        />
      )}
    </ButtonTriggerModalStore>
  );
}

export default H5ViewerButton;
