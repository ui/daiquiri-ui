import { H5GroveProvider, App } from '@h5web/app';
import baseUrl from 'helpers/baseUrl';
import RestService from 'services/RestService';

interface Props {
  datacollectionid?: number;
  autoprocprogramattachmentid?: number;
  fileName?: string;
}

function H5Viewer(props: Props) {
  const { datacollectionid, autoprocprogramattachmentid, fileName } = props;

  return (
    <H5GroveProvider
      url={`${baseUrl()}/h5grove`}
      filepath={fileName ?? 'File.h5'}
      axiosConfig={{
        params: { datacollectionid, autoprocprogramattachmentid },
        ...RestService.getOptions(),
      }}
    >
      <App disableDarkMode />
    </H5GroveProvider>
  );
}

export default H5Viewer;
