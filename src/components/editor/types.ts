export interface Directory {
  ext?: string;
  type: string;
  path: string;
  name: string;
  children?: Directory[];
  leafState?: boolean;
}
