import { map } from 'lodash';
import { Button, Collapse } from 'react-bootstrap';
import classNames from 'classnames';
import type { Directory } from './types';

interface Props {
  directory: Directory;
  renderTree: (directory: Directory) => JSX.Element;
  fetchFile: (filePath: string) => Promise<void>;
  selectFile: (filePath: string) => void;
  actions: {
    toggleLeafState: () => void;
  };
}

function FileEditorLeaf(props: Props) {
  const { directory } = props;

  const onClick = (path: string) => {
    props.fetchFile(path).then(() => {
      props.selectFile(path);
    });
  };

  return (
    <li
      key={directory.path}
      className={classNames(
        { 'leaf-open': directory.leafState, file: directory.type === 'file' },
        directory.ext
      )}
    >
      {directory.type === 'directory' && (
        <Button variant="link" onClick={() => props.actions.toggleLeafState()}>
          {directory.name}
        </Button>
      )}
      {directory.type === 'file' && (
        <Button variant="link" onClick={() => onClick(directory.path)}>
          {directory.name}
        </Button>
      )}
      {(directory.children?.length ?? 0) > 0 && (
        <Collapse in={directory.leafState}>
          <ul>{map(directory.children, (child) => props.renderTree(child))}</ul>
        </Collapse>
      )}
    </li>
  );
}

FileEditorLeaf.defaultProps = {
  leafState: false,
  children: [],
  ext: undefined,
};

export default FileEditorLeaf;
