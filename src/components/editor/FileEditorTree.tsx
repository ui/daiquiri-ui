import { useEffect } from 'react';
import type { Directory } from './types';

import { map } from 'lodash';
import { Alert } from 'react-bootstrap';
import { FullSizer } from '@esrf/daiquiri-lib';

import FileEditorLeaf from 'connect/editor/FileEditorLeaf';

interface Props {
  error?: string;
  directories: Record<string, Directory>;
  actions: {
    fetchDirectories: () => void;
    fetchFile: (filePath: string) => void;
    selectFile: (filePath: string) => void;
  };
}

function FileEditorTree(props: Props) {
  const renderTree = (tree: Directory) => {
    return (
      <FileEditorLeaf
        key={tree.name}
        directory={tree}
        renderTree={renderTree}
        fetchFile={props.actions.fetchFile}
        selectFile={props.actions.selectFile}
      />
    );
  };

  useEffect(() => {
    props.actions.fetchDirectories();
  }, [props.actions]);

  return (
    <FullSizer>
      {props.error && (
        <Alert variant="danger">
          <i className="fa fa-warning" /> Could not fetch directory listing
          <div className="description">{props.error}</div>
        </Alert>
      )}
      {map(props.directories, (dir) => (
        <ul className="editor-tree" key={dir.name}>
          {renderTree(dir)}
        </ul>
      ))}
    </FullSizer>
  );
}

export default FileEditorTree;
