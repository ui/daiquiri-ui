import { useState, useRef, useEffect, useCallback } from 'react';

import { map, values } from 'lodash';

import AceEditor from 'react-ace';

import 'ace-builds/src-noconflict/mode-yaml';
import 'ace-builds/src-noconflict/mode-python';
import 'ace-builds/src-noconflict/theme-monokai';
import 'ace-builds/src-min-noconflict/ext-language_tools';
import 'ace-builds/src-noconflict/snippets/python';
import 'ace-builds/src-min-noconflict/ext-searchbox';
import 'ace-builds/src-min-noconflict/ext-statusbar';

import { Tabs, Tab, Button, Alert } from 'react-bootstrap';
import { FullSizer } from '@esrf/daiquiri-lib';

import Confirm from 'helpers/Confirm';

interface FileDescription {
  path: string;
  contents: string;
  ext: string;
}

function TabTitle(props: {
  title: string;
  close: () => void;
  dirty?: boolean;
}) {
  return (
    <>
      <div className="title-wrap">{props.title}</div>
      {props.dirty && '*'}
      {props.dirty && (
        <Confirm
          title="File Modified"
          message="This file has been modified, are you sure you want to close without saving?"
        >
          {(confirm) => (
            <Button size="sm" className="ms-1" onClick={confirm(props.close)}>
              <i className="fa fa-times" />
            </Button>
          )}
        </Confirm>
      )}
      {!props.dirty && (
        <Button size="sm" className="ms-1" onClick={props.close}>
          <i className="fa fa-times" />
        </Button>
      )}
    </>
  );
}

function EditorTab(props: {
  file: FileDescription;
  name: string;
  save: (args: { path: string; contents: string }) => Promise<void>;
  setDirty: (dirty: boolean) => void;
}) {
  const { name, setDirty, file } = props;
  const [contents, setContents] = useState(file.contents);
  const [dimensions, setDimensions] = useState<{
    width: string;
    height: string;
  }>({ width: '', height: '' });
  const ref = useRef(contents);

  const types: Record<string, string> = {
    yml: 'yaml',
    py: 'python',
  };

  const onChange = useCallback(
    (value: any) => {
      ref.current = value;
      setDirty(value !== file.contents);
      setContents(value);
    },
    [setDirty, file]
  );

  const setTabRef = (el: AceEditor | null) => {
    const node = el?.refEditor?.parentNode?.parentNode;
    if (node && !dimensions.width) {
      const html = node as HTMLBaseElement;
      if (!html.clientWidth || !html.clientHeight) {
        console.error('No client size defined');
        return;
      }
      setDimensions({
        width: `${html.clientWidth}px`,
        height: `${html.clientHeight}px`,
      });
    }
  };

  const onSave = useCallback(() => {
    props
      .save({
        path: file.path,
        contents: ref.current,
      })
      .then(() => {
        setDirty(false);
      });
  }, [ref, file]);

  return (
    <AceEditor
      ref={(el) => setTabRef(el)}
      width={dimensions.width}
      height={dimensions.height}
      mode={types[file.ext]}
      theme="monokai"
      value={contents}
      onChange={onChange}
      name={name}
      editorProps={{ $blockScrolling: true }}
      enableBasicAutocompletion
      enableLiveAutocompletion
      enableSnippets
      commands={[
        {
          name: 'save',
          bindKey: { win: 'Ctrl-S', mac: 'Command-S' },
          exec: onSave,
        },
      ]}
    />
  );
}

function FileEditor(props: {
  error?: string;
  selectedFile?: string;
  files: Record<string, FileDescription>;
  actions: {
    selectFile: (fileName: string | null) => void;
    closeFile: (fileName: string) => void;
    updateFile: (args: { path: string; contents: string }) => Promise<void>;
  };
}) {
  const { files, selectedFile = '', actions } = props;
  const [dirty, setDirty] = useState<Record<string, boolean>>({});

  useEffect(() => {
    if (!(selectedFile in files)) {
      const filesValues = values(files);
      if (values(filesValues).length > 0) {
        setTimeout(() => {
          props.actions.selectFile(filesValues[filesValues.length - 1].path);
        }, 200);
      }
    }
  }, [files, selectedFile, actions]);

  return (
    <FullSizer className="editor">
      {props.error && (
        <Alert variant="danger">
          <i className="fa fa-warning" /> Could not open file
          <div className="description">{props.error}</div>
        </Alert>
      )}
      {values(props.files).length === 0 && (
        <p>Select a file from the tree to edit it</p>
      )}
      {values(props.files).length > 0 && (
        <Tabs
          activeKey={props.selectedFile}
          onSelect={(k) => props.actions.selectFile(k)}
          transition={false}
        >
          {map(props.files, (file, name) => (
            <Tab
              key={name.replace('/', '')}
              title={
                <TabTitle
                  title={name}
                  close={() => props.actions.closeFile(name)}
                  dirty={dirty[name]}
                />
              }
              eventKey={name}
            >
              <EditorTab
                name={name}
                file={file}
                save={props.actions.updateFile}
                setDirty={(d) => setDirty({ ...dirty, [name]: d })}
              />
            </Tab>
          ))}
        </Tabs>
      )}
    </FullSizer>
  );
}

FileEditor.defaultProps = {
  selectedFile: undefined,
  error: undefined,
};

export default FileEditor;
