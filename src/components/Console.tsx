import { useEffect, useRef } from 'react';
import { Terminal } from 'xterm';
import { FitAddon } from 'xterm-addon-fit';
import { WebglAddon } from 'xterm-addon-webgl';
import 'xterm/css/xterm.css';
import { FullSizer } from '@esrf/daiquiri-lib';

import SocketIOService from 'services/SocketIOService';
import RemountOnResize from 'components/utils/RemountOnResize';

const emit = (namespace: string, message: string, payload: unknown) => {
  SocketIOService.emit(namespace, message, payload);
};

interface Props {
  operator?: boolean;
}

function Console(props: Props) {
  const fitAddon = new FitAddon();
  const term = new Terminal({
    cursorBlink: true,
    macOptionIsMeta: true,
    scrollback: 3,
    theme: {
      background: '#272822',
    },
  });
  term.loadAddon(fitAddon);

  const xterm = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const resize = () => {
      fitAddon.fit();
      emit('pty', 'resize', { cols: term.cols, rows: term.rows });
    };

    if (xterm.current) {
      term.open(xterm.current);
      term.loadAddon(new WebglAddon());
      term.options = {
        fontSize: 12,
        fontFamily: 'Monaco, Menlo',
      };
      term.write('Starting connection to console...');

      // term.writeUtf8(new Uint8Array(data.output))
      SocketIOService.addNamespace('pty');
      SocketIOService.addCallback(
        'pty',
        'output',
        (data: { output: string }) => {
          term.write(data.output);
        }
      );
      term.onKey((key) => {
        emit('pty', 'input', { input: key.key });
      });

      resize();
      emit('pty', 'input', { input: '\n' });
    }
  }, [term, fitAddon, xterm.current]);

  const { operator } = props;
  useEffect(() => {
    term.options = {
      disableStdin: !operator,
      cursorBlink: operator,
    };
  }, [term, operator]);

  return <div ref={xterm} style={{ width: '100%', height: '100%' }} />;
}

function ConsoleWrap(props: Props) {
  return (
    <RemountOnResize>
      <FullSizer>
        <Console {...props} />
      </FullSizer>
    </RemountOnResize>
  );
}

export default ConsoleWrap;
