import type { ReactElement } from 'react';
import { OverlayTrigger, Tooltip as BSToolTip } from 'react-bootstrap';
import type { Placement } from 'react-bootstrap/types';

interface Props {
  children: ReactElement;
  tooltip: string;
  top?: boolean;
  left?: boolean;
  right?: boolean;
  bottom?: boolean;
}

export default function Tooltip(props: Props) {
  function getPlacement(): Placement | undefined {
    if (props.top) return 'top';
    if (props.bottom) return 'bottom';
    if (props.right) return 'right';
    if (props.bottom) return 'bottom';
    return undefined;
  }
  const placement = getPlacement();

  return (
    <OverlayTrigger
      placement={placement}
      overlay={<BSToolTip id="toolbar1">{props.tooltip}</BSToolTip>}
    >
      {props.children}
    </OverlayTrigger>
  );
}
