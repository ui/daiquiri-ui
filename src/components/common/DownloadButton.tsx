import type { MouseEvent } from 'react';
import { Button } from 'react-bootstrap';
import sign from 'helpers/Sign';

interface Props {
  url: string;
}

export default function DownloadButton(props: Props) {
  function onClick(e: MouseEvent) {
    e.preventDefault();

    sign({
      url: props.url,
      callback: (protectedUrl) => {
        /* @ts-expect-error this is not assignable */
        window.location = protectedUrl;
      },
    });
  }

  return (
    <Button size="sm" onClick={onClick}>
      <i className="fa fa-download" />
    </Button>
  );
}
