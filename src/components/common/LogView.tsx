import type { ReactChild, ReactElement } from 'react';
import { Component } from 'react';
import PropTypes from 'prop-types';

import { Alert } from 'react-bootstrap';

import ButtonTriggerModal from 'components/layout/ButtonTriggerModal';
import RestService from 'services/RestService';
import sign from 'helpers/Sign';
import baseUrl from 'helpers/baseUrl';

interface Props {
  url: string;
}

interface State {
  contents: ReactElement | null;
  contentType: string;
  errorMessage: string;
  fetched: boolean;
}

class Log extends Component<Props, State> {
  public constructor(props: Props) {
    super(props);

    this.state = {
      contents: null,
      contentType: '',
      errorMessage: '',
      fetched: false,
    };
  }

  public componentDidMount() {
    sign({
      url: this.props.url,
      callback: (protectedUrl) => {
        RestService.get(protectedUrl.replace(baseUrl(), ''))
          .then((resp) => {
            this.setState({
              fetched: true,
              contents: resp.data,
              contentType: resp.headers['content-type'],
            });
          })
          .catch((error) => {
            console.log(error);
            this.setState({
              fetched: true,
              errorMessage: `${error}`,
            });
          });
      },
    });
  }

  public render() {
    const { contents, contentType } = this.state;
    const Wrap = (props: { children: ReactElement | null }) =>
      contentType.includes('text/plain') ? (
        <pre>{props.children}</pre>
      ) : (
        <>{props.children}</>
      );

    return (
      <div>
        {this.state.errorMessage && (
          <Alert variant="warning">
            Couldnt load attachment:
            {this.state.errorMessage}
          </Alert>
        )}
        {!this.state.fetched && <span>Fetching</span>}
        {!this.state.errorMessage && this.state.fetched && (
          <Wrap>{contents}</Wrap>
        )}
      </div>
    );
  }
}

export default function LogView(props: { url: string; filename: string }) {
  return (
    <ButtonTriggerModal
      button={<i className="fa fa-search" />}
      buttonProps={{ size: 'sm' }}
      title={`Log Viewer: ${props.filename}`}
    >
      <Log {...props} />
    </ButtonTriggerModal>
  );
}
