import type { PropsWithChildren } from 'react';

/**
 * Separator between elements
 */
export function Separator() {
  return <div className="separator" />;
}

/**
 * Spacer between elements
 */
export function Spacer() {
  return <span className="spacer" />;
}

/**
 * A toolbar
 */
export default function Toolbar(
  props: PropsWithChildren<{
    align?: 'center' | 'left' | 'right';
    spacing?: number;
    style?: Record<string, unknown>;
  }>
) {
  const spacing = props.spacing ?? 0.2;
  const alignToJustifyContent = {
    center: 'center',
    left: 'flex-start',
    right: 'flex-end',
  };
  const justifyContent = alignToJustifyContent[props.align ?? 'left'];
  return (
    <div
      className="toolbar"
      style={{
        justifyContent,
        gap: `${spacing}em`,
        ...props.style,
      }}
    >
      {props.children}
    </div>
  );
}
