import type { ReactChild } from 'react';
import { Component } from 'react';
import classNames from 'classnames';

interface Props {
  timeout?: number;
  transition?: number;
  children: ReactChild;
}

interface State {
  children: ReactChild | null;
  emptyChildren: boolean;
  class: string | null;
}

export default class TransitionChildren extends Component<Props, State> {
  private timeout1: NodeJS.Timeout | null = null;
  private timeout2: NodeJS.Timeout | null = null;

  private static getDefaultedProps(props: Props): Props {
    const { timeout = 3000, transition = 1000, children } = props;
    return { timeout, transition, children };
  }

  public constructor(props: Props) {
    super(TransitionChildren.getDefaultedProps(props));
    this.state = {
      children: null,
      emptyChildren: false,
      class: null,
    };
  }

  private static getDerivedStateFromProps(props: Props) {
    if (props.children) {
      return {
        children: props.children,
        emptyChildren: false,
      };
    }

    if (!props.children) {
      return {
        emptyChildren: true,
      };
    }

    return null;
  }

  public componentDidUpdate(prevProps: Props, prevState: State) {
    if (prevState.emptyChildren !== this.state.emptyChildren) {
      if (this.state.emptyChildren) {
        this.timeout1 = setTimeout(() => {
          this.setState({
            class: 'transistion-children-out',
          });

          this.timeout2 = setTimeout(() => {
            this.setState({
              children: null,
              emptyChildren: false,
              class: null,
            });
          }, this.props.transition);
        }, this.props.timeout);
      }
    }
  }

  public componentWillUnmount() {
    if (this.timeout1) clearTimeout(this.timeout1);
    if (this.timeout2) clearTimeout(this.timeout2);
  }

  public render() {
    return (
      <div className={classNames('transistion-children', this.state.class)}>
        {this.state.children}
      </div>
    );
  }
}
