import PropTypes from 'prop-types';
import { useState } from 'react';
import type { InteractiveHookEvent, InteractiveHookResult } from 'rc-knob';
import { Knob, Value, Pointer, Scale, Range, Label } from 'rc-knob';
import sassVariables from 'scss/variables.module.scss';

const Origin: Record<string, number> = {
  up: 0,
  right: -90,
  down: 180,
  left: 90,
};

interface Props {
  size: number;
  value?: number | null;
  target?: number | null;
  min?: number; // TODO: to be implemented
  max?: number; // TODO: to be implemented
  disabled?: boolean;
  readOnly?: boolean;
  origin?: 'up' | 'down' | 'left' | 'right';
  direction?: 'clockwise' | 'anticlockwise';
  onChange?: (value: number) => void;
  onInteractiveChange?: (value: number) => void;
  className: string;
}

/**
 * Widget to handle cyclic angle value in degree displayed as an infinite knob
 */
export default function InfiniteKnob360(props: Props) {
  const {
    size,
    value = null,
    target = null,
    min = null,
    max = null,
    disabled = false,
    readOnly = false,
    origin = 'up',
    direction = 'clockwise',
    onChange = (angle: number) => {},
    onInteractiveChange = null,
    className,
  } = props;
  const [editedValue, setEditedValue] = useState<number | null>(null);
  function onKnobEnd() {
    setEditedValue(null);
  }

  function onKnobInteractiveChange(angle: number) {
    setEditedValue(angle);
    if (onInteractiveChange) {
      onInteractiveChange(angle);
    }
  }

  const tickSize = 8;
  const tickMargin = 5;
  const pointerMargin = 12;
  const outerTickRadius = size * 0.5;
  const outerKnobRadius = outerTickRadius - tickMargin - tickSize;
  const pointerSize = 4;
  const pointerPosRadius = outerKnobRadius - pointerMargin - pointerSize / 2;
  const editable = !disabled && !readOnly;
  const shadowWidth = 5;
  const localTarget = editedValue !== null ? editedValue : target;

  function interactiveHook(e: InteractiveHookEvent): InteractiveHookResult {
    if (e.mouseRadius < outerKnobRadius / 3) {
      return { readOnly: true };
    }
    if (e.ctrlKey) {
      return { steps: 8 };
    }
    if (
      outerTickRadius - tickSize - tickMargin <= e.mouseRadius &&
      e.mouseRadius <= outerTickRadius + tickMargin
    ) {
      return { steps: 8 };
    }
    return {};
  }

  function getUnreachableRange(): [number, number] | null {
    if (min === null || max === null) {
      return null;
    }
    const range = max - min;
    if (range >= 360) {
      return null;
    }
    return [(max - 360) / 360, (max - 360 + 360 - range) / 360];
  }

  const unrachableRange = getUnreachableRange();

  const angleRange = direction === 'clockwise' ? 360 : -360;
  const angleOffset = Origin[origin] + (direction === 'clockwise' ? 0 : 360);

  return (
    <Knob
      size={size}
      value={value}
      angleOffset={angleOffset}
      angleRange={angleRange}
      min={0}
      max={360}
      multiRotation
      onChange={onChange}
      onInteractiveChange={onKnobInteractiveChange}
      interactiveHook={interactiveHook}
      onEnd={onKnobEnd}
      readOnly={!editable}
      tracking={false}
      useMouseWheel={false}
      className={className}
    >
      <defs>
        <radialGradient
          id="knob-shadow"
          cx={size / 2}
          cy={size / 2}
          fx={size * 0.75}
          fy={size * 0.75}
          r={outerKnobRadius + shadowWidth}
          gradientUnits="userSpaceOnUse"
        >
          <stop
            offset={outerKnobRadius / (outerKnobRadius + shadowWidth)}
            stopColor={sassVariables.backgroundD10}
            stopOpacity="0.1"
          />
          <stop
            offset={1}
            stopColor={sassVariables.backgroundD10}
            stopOpacity="0"
          />
        </radialGradient>
        <linearGradient
          id="knob-enabled"
          x1={0}
          y1={size / 2 - outerKnobRadius}
          x2={0}
          y2={size / 2 + outerKnobRadius}
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0%" stopColor={sassVariables.backgroundD10} />
          <stop offset="30%" stopColor={sassVariables.background} />
          <stop offset="70%" stopColor={sassVariables.background} />
          <stop offset="100%" stopColor={sassVariables.backgroundD10} />
        </linearGradient>
        <linearGradient
          id="knob-disabled"
          x1={0}
          y1={size / 2 - outerKnobRadius}
          x2={0}
          y2={size / 2 + outerKnobRadius}
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0%" stopColor={sassVariables.disabledD10} />
          <stop offset="30%" stopColor={sassVariables.disabled0} />
          <stop offset="70%" stopColor={sassVariables.disabled0} />
          <stop offset="100%" stopColor={sassVariables.disabledD10} />
        </linearGradient>
      </defs>
      <circle
        r={outerKnobRadius + shadowWidth}
        cx={size / 2}
        cy={size / 2}
        fill="url(#knob-shadow)"
      />
      <circle
        r={outerKnobRadius}
        cx={size / 2}
        cy={size / 2}
        fill={editable ? 'url(#knob-enabled)' : 'url(#knob-disabled)'}
      />
      {unrachableRange && (
        <Range
          percentageFrom={unrachableRange[0]}
          percentageTo={unrachableRange[1]}
          radius={outerTickRadius}
          color={sassVariables.danger}
          arcWidth={tickSize}
        />
      )}
      <Scale
        steps={8}
        tickWidth={2}
        tickHeight={tickSize}
        radius={outerTickRadius}
        color={sassVariables.textD10}
      />
      <Scale
        steps={32}
        tickWidth={1}
        tickHeight={tickSize / 2}
        radius={outerTickRadius}
        color={sassVariables.textD10}
      />
      <Label
        percentage={0.1}
        radius={outerTickRadius + 6}
        label="+"
        style={{ fontSize: '80%', fontWeight: 'bold' }}
      />
      <Label
        percentage={-0.1}
        radius={outerTickRadius + 6}
        label="-"
        style={{ fontSize: '80%', fontWeight: 'bold' }}
      />
      {localTarget !== null && value !== null && (
        <Range
          percentageFrom={value / 360}
          percentageTo={localTarget / 360}
          radius={pointerPosRadius + pointerSize * 1.5}
          color={
            editedValue !== null ? sassVariables.edited : sassVariables.warning
          }
          arcWidth={pointerSize}
        />
      )}
      {localTarget !== null && (
        <Pointer
          percentage={localTarget / 360}
          width={pointerSize}
          height={pointerSize}
          radius={pointerPosRadius}
          type="circle"
          color={
            editedValue !== null ? sassVariables.edited : sassVariables.warning
          }
        />
      )}
      <Pointer
        percentage={0}
        width={2.5}
        height={tickSize}
        radius={outerTickRadius - tickSize * 1.5}
        type="circle"
        color={sassVariables.text}
      />
      {value !== null && (
        <Pointer
          percentage={value / 360}
          width={pointerSize}
          height={pointerSize}
          radius={pointerPosRadius}
          type="circle"
          color={sassVariables.text}
        />
      )}
      <Value
        value={editedValue !== null ? editedValue : value}
        decimalPlace={1}
        marginBottom={size / 2}
        className="knob-text"
      />
    </Knob>
  );
}
