import { Badge } from 'react-bootstrap';

/**
 * Display a name stacked with a state.
 *
 * If `minWidth` is pecified (in `em`) it ensure the widget width will stay the
 * same when the state change
 */
export default function StackedNameState(props: {
  className?: string;
  name: string;
  state?: string;
  minWidth?: number;
  stateVariant?: string;
  description?: string;
}) {
  const { name, state, stateVariant = 'secondary', description } = props;

  const style = {
    display: 'inline-block',
    minWidth: '',
  };

  if (props.minWidth) style.minWidth = `${props.minWidth}em`;

  return (
    <div
      className={props.className}
      style={{
        textAlign: 'center',
      }}
    >
      <strong
        title={description}
        style={{
          display: 'block',
          margin: '0px',
          whiteSpace: 'nowrap',
        }}
      >
        {name}
      </strong>
      <div style={style}>
        <div style={{ position: 'relative', top: -5 }}>
          <Badge title={description} bg={stateVariant}>
            {props.state}
          </Badge>
        </div>
      </div>
    </div>
  );
}
