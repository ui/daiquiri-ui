import type { IdSchema, Registry, UiSchema, WidgetProps } from '@rjsf/core';
import type { JSONSchema7 } from 'json-schema';
import {
  getDefaultRegistry,
  getUiOptions,
  getWidget,
  hasWidget,
  isSelect,
  optionsList, // @ts-expect-error
} from '@rjsf/core/dist/es/utils';
import { useEffect, useRef, useState } from 'react';
import { Button, InputGroup, OverlayTrigger, Tooltip } from 'react-bootstrap';

interface StaticFieldProps {
  name: string;
  value: string | null | undefined;
  isEditable: boolean;
  onClick: () => void;
  title?: string;
}

function StaticField(props: StaticFieldProps) {
  const { name, title, value, isEditable, onClick } = props;

  if (!isEditable) {
    return (
      <div className="value">
        <span>{value}</span>
      </div>
    );
  }

  return (
    <OverlayTrigger
      placement="top"
      overlay={<Tooltip id={name}>Click to edit</Tooltip>}
    >
      <Button
        variant="light"
        className="form-control editable"
        onClick={onClick}
        aria-label={`edit-${title || name}`}
      >
        {value !== null && value !== undefined ? (
          value
        ) : (
          <span className="click">Click to edit</span>
        )}
      </Button>
    </OverlayTrigger>
  );
}

interface EditableFieldProps {
  widget: unknown;
  widgetProps: Partial<WidgetProps>;
  onSave: () => Promise<void>;
  onCancel: () => void;
  setEditing: (editing: boolean) => void;
}

function EditableField(props: EditableFieldProps) {
  const { widget, widgetProps, onSave, onCancel, setEditing } = props;
  const { widgets } = widgetProps.registry as Registry;
  const Widget = getWidget(widgetProps.schema, widget, widgets);
  const { onChange, disabled, ...otherProps } = widgetProps;

  const unmounted = useRef(false);
  const [edited, setEdited] = useState(false);
  const [saving, setSaving] = useState(false);
  const [error, setError] = useState<string>();

  useEffect(() => {
    return () => {
      unmounted.current = true;
    };
  }, []);

  const handleSave = () => {
    setSaving(true);
    onSave()
      .then(() => {
        if (unmounted.current) return;
        setEditing(false);
        // console.log('saveField', resp);
      })
      .catch((error_: unknown) => {
        if (unmounted.current) return;
        // console.log('saveField error', e);
        setError(`${error_}`);
      })
      .finally(() => {
        setSaving(false);
      });
  };

  const hasErrors = !!widgetProps.rawErrors?.length;

  return (
    <>
      <Widget
        disabled={disabled || saving}
        onChange={(v: string) => {
          if (onChange) {
            onChange(v);
          }
          setEdited(true);
        }}
        className={`form-control ${edited ? 'form-control-edited' : ''}`}
        autofocus
        onKeyDown={(evt: KeyboardEvent) => {
          if (evt.key === 'Enter' && !hasErrors) {
            handleSave();
          }
        }}
        {...otherProps}
      />
      {error && <span className="editable-error text-danger">{error}</span>}
      <>
        <Button
          variant="success"
          size="sm"
          onClick={handleSave}
          disabled={saving || hasErrors}
        >
          <i
            className={`fa ${saving ? 'fa-spin fa-spinner me-1' : 'fa-check'}`}
          />
          <span className="sr-only">{saving ? 'Saving' : 'Save'}</span>
        </Button>
        <Button variant="danger" size="sm" onClick={onCancel} disabled={saving}>
          <i className="fa fa-times" />
          <span className="sr-only">Cancel</span>
        </Button>
      </>
    </>
  );
}

interface Props {
  schema: JSONSchema7;
  name: string;
  uiSchema: UiSchema;
  idSchema: IdSchema;
  formData: string;
  required?: boolean;
  disabled?: boolean;
  readonly?: boolean;
  onChange: (v: string) => void;
  onBlur?: (id: string, value: string) => void;
  onFocus?: (id: string, value: string) => void;
  registry?: Registry;
  rawErrors?: string[];
}

function StringField(props: Props) {
  const {
    schema,
    name,
    uiSchema,
    idSchema,
    formData,
    required,
    disabled,
    readonly,
    onChange,
    onBlur,
    onFocus,
    registry = getDefaultRegistry(),
    rawErrors = [],
  } = props;

  const prevValue = useRef<string>();
  const [editing, setEditing] = useState(false);

  const { title, format } = schema;
  const { widgets, formContext } = registry as Registry;
  const enumOptions = isSelect(schema) ? optionsList(schema) : {};
  let defaultWidget = isSelect(schema) ? 'select' : 'text';
  if (format && hasWidget(schema, format, widgets)) {
    defaultWidget = format;
  }
  const {
    widget = defaultWidget,
    placeholder = '',
    ...options
  } = getUiOptions(uiSchema);

  const onClick = () => {
    prevValue.current = formData;
    setEditing(true);
  };

  const onSave = () => {
    return formContext.onSave({
      field: name,
      value: formData,
    }) as Promise<void>;
  };

  const cancelEditing = () => {
    setEditing(false);
    if (prevValue.current) {
      onChange(prevValue.current);
    }
  };

  return (
    <>
      {editing ? (
        <EditableField
          widgetProps={{
            options: { ...options, enumOptions },
            schema,
            id: idSchema?.$id,
            label: title === undefined ? name : title,
            value: formData,
            onChange,
            onBlur,
            onFocus,
            required,
            disabled,
            readonly,
            formContext,
            registry,
            placeholder,
            rawErrors,
          }}
          widget={widget}
          onSave={onSave}
          setEditing={setEditing}
          onCancel={cancelEditing}
        />
      ) : (
        <>
          <StaticField
            name={name}
            title={title}
            value={formContext.staticValues[name] || formData}
            isEditable={(formContext.editable as string[]).includes(name)}
            onClick={onClick}
          />
          {formContext.extraComponents[name] && (
            <>{formContext.extraComponents[name]}</>
          )}
        </>
      )}
    </>
  );
}

export default StringField;
