import type { PropsWithChildren, ReactChild } from 'react';

export default function WithToolbar(
  props: PropsWithChildren<{
    toolbar?: ReactChild;
    toolbarLocation: 'top' | 'bottom';
    className?: string;
    style?: Record<string, unknown>;
  }>
) {
  const { toolbarLocation } = props;
  return (
    <div className={props.className} style={props.style}>
      {toolbarLocation === 'top' && props.toolbar}
      {props.children}
      {toolbarLocation === 'bottom' && props.toolbar}
    </div>
  );
}
