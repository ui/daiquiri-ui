import type { MouseEvent } from 'react';
import { Component } from 'react';

import { Button } from 'react-bootstrap';

import Confirm from 'helpers/Confirm';

interface Props {
  onClick: (id: number) => void;
  show?: boolean;
  id: number;
  confirm?: boolean;
  disabled?: boolean;
}

export default function DeleteButton(props: Props) {
  const { show = true } = props;

  function onClick(e: MouseEvent) {
    props.onClick(props.id);
  }

  return (
    <>
      {show && (
        <Confirm
          title="Confirm"
          message="Are you sure you want to delete this item?"
        >
          {(confirm) => (
            <Button
              className="ms-1 delete"
              variant="danger"
              onClick={props.confirm ? confirm(onClick) : onClick}
              size="sm"
              disabled={props.disabled}
            >
              <i className="fa fa-times" />
            </Button>
          )}
        </Confirm>
      )}
    </>
  );
}
