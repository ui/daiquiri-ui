import type { MouseEvent, RefObject, SyntheticEvent } from 'react';
import { createRef } from 'react';
import { Component } from 'react';
import { map, filter, flatten } from 'lodash';

import Table from 'components/table';
import ModalDialog from 'components/layout/ModalDialog';
import fixedCell from 'components/table/cells/fixedCell';

interface Props {
  modal: Record<string, boolean>;
  element?: {
    name?: string;
    symbol?: string;
    edges?: any[];
  };
  sigmaMca?: number;
  operator?: boolean;
  sampleid: number;
  actions: {
    addMapROI: (params: {
      sampleid: number;
      element: string | undefined;
      edge: string;
      start: number;
      end: number;
    }) => Promise<any>;
    toggleModal: (id: string) => void;
  };
}

export default class LinesModal extends Component<Props> {
  private readonly key = 'lines-modal';

  private readonly targetRef: RefObject<HTMLElement>;

  public constructor(props: any) {
    super(props);
    this.targetRef = createRef<HTMLElement>();
  }

  private readonly onCloseModal = () => {
    this.props.actions.toggleModal(this.key);
  };

  private readonly onRowClick = (e: SyntheticEvent, row: any) => {
    if (!this.props.operator) return;

    const eHole = 3.85;
    const fano = 0.114;

    const eev = row.energy * 1000;
    const sigmaMca = this.props.sigmaMca ?? 45;
    const sig = Math.sqrt(sigmaMca ** 2 + fano * eHole * eev);

    const element = this.props.element ?? {};
    this.props.actions.addMapROI({
      sampleid: this.props.sampleid,
      element: element.symbol,
      edge: row.siegbahn,
      start: Math.round(eev - sig / 2),
      end: Math.round(eev + sig / 2),
    });
  };

  private readonly rowClasses = () => {
    return this.props.operator ? 'pointer' : '';
  };

  public show() {
    this.props.actions.toggleModal(this.key);
  }

  public render() {
    const roiLines = new Set([
      'K-L3',
      'L1-M3',
      'L2-M4',
      'L3-M5',
      'M1-N2',
      'M2-N4',
      'M3-N5',
      'M4-N6',
      'M5-N7',
    ]);
    const roiExtra = new Set(['K-M3', 'L2-M4']);

    const lines = flatten(
      this.props.element && map(this.props.element.edges, (e) => e.lines)
    );
    const filtered = filter(lines, (l) => {
      return roiLines.has(l.iupac) || roiExtra.has(l.iupac);
    });

    const columns = [
      { dataField: 'iupac', text: 'IUPAC' },
      { dataField: 'siegbahn', text: 'Siegbahn' },
      {
        dataField: 'energy',
        text: 'Energy (keV)',
        formatter: fixedCell,
        formatExtraData: { dp: 3 },
      },
    ];

    return (
      <ModalDialog
        show={this.props.modal[this.key]}
        actions={{ onClose: this.onCloseModal }}
        title={`${this.props.element?.name} Emission Lines`}
        target={this.targetRef}
      >
        <>{this.props.operator && <p>Click to add an ROI to the list</p>}</>
        <Table
          keyField="iupac"
          data={filtered}
          columns={columns}
          hover={this.props.operator}
          rowClasses={this.rowClasses}
          rowEvents={{ onClick: this.onRowClick }}
          noDataIndication="No emission lines for this element"
        />
      </ModalDialog>
    );
  }
}
