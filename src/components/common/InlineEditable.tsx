import { each } from 'lodash';

import StringField from './StringField';

import SimpleSchemaForm from 'components/SimpleSchemaForm';

interface Props {
  schema: Record<string, any>;
  editable?: string[];
  formData: Record<string, any>;
  onChange: (props: { field: string; value: any }) => Promise<void>;
  colWidth?: number;
  staticValues?: Record<string, any>;
  extraComponents?: Record<string, any>;
  className?: string;
  uiSchema?: any;
}

function InlineEditable(props: Props) {
  function onSave(e: { field: string; value: any }) {
    return props.onChange(e);
  }

  // Convert null to undefined so rjsf doesnt validate empty fields
  const undefedFormData: Record<string, any> = {};
  each(props.formData, (fd, key) => {
    undefedFormData[key] = fd === null ? undefined : fd;
  });

  return (
    <SimpleSchemaForm
      colField
      colWidth={props.colWidth}
      className={`editable-form ${props.className}`}
      fields={{ StringField }}
      schema={props.schema}
      formData={undefedFormData}
      uiSchema={props.uiSchema}
      button={false}
      formContext={{
        editable: props.editable,
        staticValues: props.staticValues,
        extraComponents: props.extraComponents,
        onSave,
      }}
    />
  );
}

InlineEditable.defaultProps = {
  editable: [],
  colWidth: 5,
  staticValues: {},
  extraComponents: {},
  className: '',
};

export default InlineEditable;
