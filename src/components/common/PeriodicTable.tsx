import type { MouseEvent } from 'react';
import { Component } from 'react';
import { map, find, range } from 'lodash';
import classNames from 'classnames';

import elements from 'data/elements';

interface PeriodicItemProps {
  id: number;
  elements: any;
  noEdges: boolean;
  selected: boolean;
  clickElement: (id: number, element: any) => void;
}

class PeriodicItem extends Component<PeriodicItemProps> {
  private readonly el: any;

  public constructor(props: PeriodicItemProps) {
    super(props);
    const ext = this.props.id > 144 ? -1 : 0;

    this.el = find(this.props.elements, {
      xpos: (this.props.id % 18) + 1 + ext,
      ypos: Math.floor(this.props.id / 18) + 1,
    });
  }

  private readonly onClick = (e: MouseEvent) => {
    this.props.clickElement(this.props.id, this.el);
  };

  public render() {
    return (
      <div className="item">
        {this.el !== undefined && (
          <div
            className={classNames(
              'periodic-element',
              `period-${this.el.period}`,
              {
                selected: this.props.selected,
                'no-edges':
                  this.props.noEdges &&
                  (!this.el.edges ||
                    (this.el.edges && this.el.edges.length === 0)),
              }
            )}
            onClick={this.onClick}
          >
            <span className="number">{this.el.number}</span>
            <h2>{this.el.symbol}</h2>
            <p>{this.el.name}</p>
          </div>
        )}
      </div>
    );
  }
}

interface Props {
  selectedElement?: number;
  onClick?: () => void;
  noEdges?: boolean;
  actions: { onClick?: (id: number, element: any) => void };
}

export default class PeriodicTable extends Component<Props> {
  private readonly clickElement = (id: number, el: any) => {
    if (this.props.actions?.onClick) {
      this.props.actions.onClick(id, el);
    }
  };

  public render() {
    const items = map(range(18 * 10), (i) => {
      return (
        <PeriodicItem
          key={i}
          id={i}
          elements={elements}
          clickElement={this.clickElement}
          selected={this.props.selectedElement === i}
          noEdges={this.props.noEdges || false}
        />
      );
    });

    return <div className="periodic-table">{items}</div>;
  }
}
