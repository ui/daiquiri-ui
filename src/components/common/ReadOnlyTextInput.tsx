import { Form } from 'react-bootstrap';

/**
 * Readonly text displayed as an input.
 *
 * Is supports variants, and can be properly integrated inside a
 * `FormGroup` to replace the main input.
 */
export default function ReadOnlyTextInput(props: {
  variant?: string;
  value?: string;
  style?: Record<string, any>;
  className?: string;
  title?: string;
}) {
  const { value, variant, style, className, title } = props;
  const defaultClasses = variant
    ? `badge-${variant} bg-${variant} text-bg-${variant} border-${variant}`
    : 'border-light';
  return (
    <Form.Control
      className={`${defaultClasses} ${className} bg-secondary`}
      style={style}
      value={value}
      title={title}
      readOnly
    />
  );
}
