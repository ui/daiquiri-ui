import { createRef, Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { map, values, keys } from 'lodash';

import Badge from 'react-bootstrap/Badge';

class LogEntry extends Component {
  static defaultProps = {
    stack_trace: '',
    you: false
  };

  static propTypes = {
    type: PropTypes.string.isRequired,
    timestamp: PropTypes.string.isRequired,
    level: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
    stack_trace: PropTypes.string,
    you: PropTypes.bool
  };

  render() {
    return (
      <div className="log">
        <div className="log-left">
          <div
            className={`type type-${this.props.type}`}
            title={this.props.type}
          />
        </div>
        <div className="log-right">
          <div className="title">
            <span
              title={moment(this.props.timestamp).format('DD-MM-YYYY HH:mm:ss')}
            >
              {moment(this.props.timestamp).fromNow()}
            </span>
            {this.props.you && <Badge bg="secondary">You</Badge>}
          </div>
          <div className="message">
            <span
              className={`level level-${this.props.level}`}
              title={this.props.level}
            />
            <p>{this.props.message}</p>
            {this.props.stack_trace && (
              <span className="stack-trace">{this.props.stack_trace}</span>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default class Logging extends Component {
  static propTypes = {
    current: PropTypes.shape({
      sessionid: PropTypes.string.isRequired
    }).isRequired,
    logging: PropTypes.shape({}).isRequired,
    user: PropTypes.shape({
      is_staff: PropTypes.bool
    }).isRequired,
    actions: PropTypes.shape({
      fetchLogs: PropTypes.func.isRequired
    }).isRequired
  };

  constructor(props) {
    super(props);
    if (this.props.user.is_staff) this.props.actions.fetchLogs();
    this.containerRef = createRef();
    this.bottomRef = createRef();
  }

  componentDidMount() {
    const observer = new IntersectionObserver(entries => {
      entries.forEach(() => {
        this.loadNext();
      });
    });
    observer.observe(this.bottomRef.current);
  }

  loadNext() {
    if (this.props.user.is_staff) {
      this.props.actions.fetchLogs({
        offset: values(this.props.logging).length
      });
    }
  }

  render() {
    const sortedKeys = keys(this.props.logging).sort(
      (a, b) => parseFloat(b) - parseFloat(a)
    );
    return (
      <div className="logging" ref={this.containerRef}>
        {map(sortedKeys, (k, id) => (
          <LogEntry
            {...this.props.logging[k]}
            key={id}
            you={
              this.props.logging[k].sessionid === this.props.current.sessionid
            }
          />
        ))}
        <div ref={this.bottomRef} className="logging-bottom" />
      </div>
    );
  }
}
