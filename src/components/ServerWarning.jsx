import { Component } from 'react';
import PropTypes from 'prop-types';
import Alert from 'react-bootstrap/Alert';

export default class ServerWarning extends Component {
  static propTypes = {
    show: PropTypes.bool
  };

  render() {
    return (
      <div>
        {this.props.show && (
          <Alert variant="danger" className={`center ${this.props.className}`}>
            Cant connect to the server, is it running?
          </Alert>
        )}
      </div>
    );
  }
}
