import { Button } from 'react-bootstrap';

interface MapEntity {
  mapid: number;
  opacity: number;
}

interface IProps {
  maps: MapEntity[];
  actions: {
    updateMap: ({
      mapid,
      opacity,
    }: {
      mapid: number;
      opacity: number;
    }) => Promise<any>;
  };
}

export default function HideAllMaps(props: IProps) {
  function hideMaps() {
    props.maps.forEach((map) => {
      if (map.opacity > 0) {
        props.actions.updateMap({
          mapid: map.mapid,
          opacity: 0,
        });
      }
    });
  }

  return (
    <Button
      disabled={
        props.maps.length === 0 ||
        props.maps.map((map) => map.opacity).reduce((sum, x) => sum + x) === 0
      }
      className="float-end me-1"
      onClick={hideMaps}
      title="Hide All Maps"
    >
      <i className="fa fa-eye-slash" />
    </Button>
  );
}
