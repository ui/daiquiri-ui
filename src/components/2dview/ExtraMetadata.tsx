import { Container, Row, Col, Button } from 'react-bootstrap';
import { Typeahead } from 'react-bootstrap-typeahead';
import { useEffect, useState, useMemo, useCallback } from 'react';
import SimpleSchemaForm from '../SimpleSchemaForm';

interface ISampleSchema {
  definitions: {
    SampleSchema: {
      properties: {
        extrametadata: any;
      };
    };
  };
}

interface IExtraMetadataData {
  tags?: string[];
  [key: string]: any;
}

interface IExtraMetadata {
  onChange: (
    { field, value }: { field: string; value: any },
    addedTags: string[],
    removedTags: string[]
  ) => Promise<any>;
  extraMetadata: IExtraMetadataData;
  availableSampleTags: {
    tags: string[];
  };
  actions: {
    fetchSampleTags: (payload?: any) => Promise<any>;
  };
  sampleSchema: ISampleSchema;
  showKeyValues: boolean;
  subSamples: boolean;
  samples: boolean;
}

interface IMetadataChange {
  onChange: (metadata: IExtraMetadataData) => Promise<any>;
  extraMetadata: IExtraMetadataData;
  sampleSchema: ISampleSchema;
}

interface INewTag {
  label: string;
}

function AddMetadata({
  onChange,
  extraMetadata,
  sampleSchema,
}: IMetadataChange) {
  const [newMetadata, setNewMetadata] = useState<Record<string, any>>({});
  const [rerender, setRerender] = useState<boolean>(false);
  function saveMetadata() {
    onChange({
      ...extraMetadata,
      ...newMetadata,
    }).then(() => {
      setRerender((currentRerender) => !currentRerender);
    });
  }
  const updateNewMetadata = useCallback(
    (formData: Record<string, any>) => {
      setNewMetadata(formData.extrametadata);
    },
    [setNewMetadata]
  );

  const memoForm = useMemo(
    () => (
      <SimpleSchemaForm
        className="object-form-additional mt-1"
        schema={{
          extrametadata:
            sampleSchema.definitions.SampleSchema.properties.extrametadata,
        }}
        onChange={updateNewMetadata}
        button={false}
      />
    ),
    [sampleSchema, updateNewMetadata, rerender]
  );

  return (
    <>
      {memoForm}
      {Object.keys(newMetadata).length > 0 && (
        <div className="d-grid gap-2">
          <Button onClick={saveMetadata}>Save</Button>
        </div>
      )}
    </>
  );
}

export default function ExtraMetadata({
  extraMetadata = {},
  availableSampleTags,
  onChange,
  actions,
  sampleSchema,
  showKeyValues = false,
  subSamples = false,
  samples = false,
}: IExtraMetadata) {
  const { tags: currentTags = [], ...rest } = extraMetadata || {};
  // Typeahead helpfully mutates its data array :|
  const tags = [...currentTags];
  const mutableAvailableSampleTags = [...(availableSampleTags.tags ?? [])];

  function removeKey(key: string) {
    const { [key]: remove, ...remainingData } = extraMetadata;
    return metadataChange({
      ...remainingData,
    });
  }

  function metadataChange(metadata: IExtraMetadataData) {
    const addedTags = (metadata?.tags || []).filter(
      (tag) => !(extraMetadata?.tags || []).includes(tag)
    );
    const removedTags = (extraMetadata?.tags || []).filter(
      (tag) => !(metadata?.tags || []).includes(tag)
    );

    return onChange(
      {
        field: 'extrametadata',
        value: metadata,
      },
      addedTags,
      removedTags
    );
  }

  function updateTags(selected: string[]) {
    const newTags = selected.map((tag: string | INewTag) =>
      typeof tag === 'string' ? tag : tag.label
    );

    return metadataChange({
      ...extraMetadata,
      tags: newTags,
    }).then(() => {
      actions.fetchSampleTags(
        samples ? { type: 'sample' } : subSamples ? { type: 'subsample' } : null
      );
    });
  }

  useEffect(() => {
    actions.fetchSampleTags(
      samples ? { type: 'sample' } : subSamples ? { type: 'subsample' } : null
    );
  }, []);

  return (
    <>
      <Container>
        <Row className="mt-1">
          <Col xs={4}>Tags:</Col>
          <Col>
            <Typeahead
              id="typeahead-tags"
              multiple
              allowNew
              newSelectionPrefix="Add tag: "
              onChange={(selection) => void updateTags(selection as string[])}
              selected={tags}
              options={mutableAvailableSampleTags}
            />
          </Col>
        </Row>
        {showKeyValues && (
          <>
            <Row>
              <Col>
                <hr />
              </Col>
            </Row>
            {Object.keys(rest).length === 0 && <p>No additional metadata</p>}
            {Object.keys(rest).length > 0 && <p>Additional metadata:</p>}
            {Object.entries(rest).map(([key, value]) => (
              <Row>
                <Col xs={3}>{key}</Col>
                <Col xs={7}>
                  {typeof value === 'object' ? JSON.stringify(value) : value}
                </Col>
                <Col xs={2} className="text-end">
                  <Button
                    variant="danger"
                    size="sm"
                    onClick={() => void removeKey(key)}
                  >
                    <i className="fa fa-minus" />
                  </Button>
                </Col>
              </Row>
            ))}
            <Row>
              <Col>
                <AddMetadata
                  onChange={metadataChange}
                  extraMetadata={extraMetadata}
                  sampleSchema={sampleSchema}
                />
              </Col>
            </Row>
          </>
        )}
      </Container>
    </>
  );
}
