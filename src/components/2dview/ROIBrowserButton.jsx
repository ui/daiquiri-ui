import { createRef, Component } from 'react';
import PropTypes from 'prop-types';
import { each } from 'lodash';
import { Button } from 'react-bootstrap';

import ButtonTriggerModalStore from 'connect/layout/ButtonTriggerModalStore';
import ROIBrowser from 'connect/2dview/ROIBrowser';

export default class ROIBrowserButton extends Component {
  static propTypes = {
    sampleid: PropTypes.number.isRequired,
    operator: PropTypes.bool.isRequired,
    actions: PropTypes.shape({
      addMapROI: PropTypes.func.isRequired
    }).isRequired
  };

  constructor(props) {
    super(props);
    this.modalRef = createRef();
  }

  onApply = e => {
    e.preventDefault();

    if (this.state.rois) {
      const ps = [];
      each(this.state.rois, roi => {
        ps.push(
          this.props.actions.addMapROI({
            ...roi,
            sampleid: this.props.sampleid
          })
        );
      });
      Promise.all(ps).then(() => this.modalRef.current.close());
    }
  };

  onChange = rois => {
    this.setState({ rois });
  };

  render() {
    return (
      <ButtonTriggerModalStore
        id="roiBrowser"
        ref={this.modalRef}
        title="ROI Browser"
        buttonProps={{
          className: 'me-1',
          disabled: !this.props.operator
        }}
        button={
          <>
            <i className="fa fa-search me-1" />
            History
          </>
        }
        buttons={<Button onClick={this.onApply}>Apply</Button>}
      >
        <ROIBrowser
          onChange={this.onChange}
          providers={{
            metadata: {
              xrf_map_rois: {
                namespace: 'browser'
              }
            }
          }}
        />
      </ButtonTriggerModalStore>
    );
  }
}
