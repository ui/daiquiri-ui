import { Component } from 'react';
import PropTypes from 'prop-types';
import { values, each, keys } from 'lodash';
import { FullSizer, Formatting } from '@esrf/daiquiri-lib';

import RemountOnResize from 'components/utils/RemountOnResize';

import config from 'config/config';
import SelectableTable from 'components/table/Selectable';
import DeleteButton from 'components/common/DeleteButton';
import InlineEditable from 'components/common/InlineEditable';
import Tooltip from 'components/common/Tooltip';
import Confirm from 'helpers/Confirm';

import ObjectType from 'types/ObjectType';
import SampleType from 'types/SampleType';

import { Button, Badge, OverlayTrigger, Popover } from 'react-bootstrap';
import ImportReferenceButton from './ImportReferenceButton';
import ExtraMetadataButton from './ExtraMetadataButton';
import ExtraMetadata from 'connect/2dview/ExtraMetadata';

class ViewButton extends Component {
  static propTypes = {
    subsample: PropTypes.shape(ObjectType.types).isRequired,
    subSampleSchema: PropTypes.shape({
      definitions: PropTypes.shape({
        SubSampleSchema: PropTypes.shape({})
      })
    }).isRequired,
    updateObject: PropTypes.func.isRequired,
    hasData: PropTypes.bool.isRequired
  };

  onChange = ({ field, value }) => {
    const data = {};
    if (this.props.subsample.type === 'roi') {
      if (field === 'w') {
        data.x2 = Math.round(
          this.props.subsample.x +
            value / this.engw.multiplier / config.pixelSize
        );
      }

      if (field === 'h') {
        data.y2 = Math.round(
          this.props.subsample.y +
            value / this.engw.multiplier / config.pixelSize
        );
      }
    }

    each(this.props.subsample.positions, (v, k) => {
      if (k === field) {
        data.positions = { [k]: value };
      }
    });

    if (!keys(data).length) data[field] = value;

    return this.props.updateObject({
      subsampleid: this.props.subsample.subsampleid,
      ...data
    });
  };

  render() {
    const title = `${this.props.subsample.type.toUpperCase()} ${
      this.props.subsample.subsampleid
    }`;

    const extraFormData = {};
    const extraFields = {};

    const editable = ['comments'];

    if (this.props.subsample.type === 'roi') {
      const w = this.props.subsample.x2 - this.props.subsample.x;
      const h = this.props.subsample.y2 - this.props.subsample.y;
      this.engw = Formatting.formatEng(w * config.pixelSize);
      this.engh = this.engw.multiplier * h * config.pixelSize;

      extraFields.w = {
        title: 'Width',
        type: 'number',
        unit: `${this.engw.prefix}m`
      };
      extraFields.h = {
        title: 'Height',
        type: 'number',
        unit: `${this.engw.prefix}m`
      };

      extraFormData.w = parseFloat(Formatting.round(this.engw.scalar, 12));
      extraFormData.h = parseFloat(Formatting.round(this.engh, 12));

      if (!this.props.hasData) {
        editable.push('w');
        editable.push('h');
      }
    }

    each(this.props.subsample.positions, (v, k) => {
      extraFields[k] = { title: k, type: 'number' };
      extraFormData[k] = v;
      if (!this.props.hasData) editable.push(k);
    });

    const { properties } =
      this.props.subSampleSchema.definitions.SubSampleSchema;
    const schema = {
      comments: properties.comments,
      ...extraFields
    };

    return (
      <>
        <OverlayTrigger
          trigger="click"
          placement="left"
          rootClose
          overlay={
            <Popover className="inline-editable">
              <Popover.Header>{title}</Popover.Header>
              <Popover.Body>
                <InlineEditable
                  colWidth={4}
                  editable={editable}
                  schema={schema}
                  formData={{
                    ...this.props.subsample,
                    ...extraFormData
                  }}
                  onChange={this.onChange}
                />
                <ExtraMetadata
                  subSamples
                  onChange={this.onChange}
                  extraMetadata={this.props.subsample.extrametadata}
                />
              </Popover.Body>
            </Popover>
          }
        >
          <Button variant="info" size="sm" className="view">
            <i className="fa fa-search" />
          </Button>
        </OverlayTrigger>
      </>
    );
  }
}

class MoveButton extends Component {
  static defaultProps = {
    disabled: false
  };

  static propTypes = {
    moveToSubSample: PropTypes.func.isRequired,
    addToast: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired,
    disabled: PropTypes.bool
  };

  onClick = () => {
    this.props.moveToSubSample(this.props.id).catch(err => {
      this.props.addToast({
        type: 'error',
        title: 'Could not move to subsample',
        text:
          (err.response && err.response.data && err.response.data.error) ||
          err.message
      });
    });
  };

  render() {
    return (
      <Button
        className="me-1 move"
        size="sm"
        variant="secondary"
        title="Move to"
        onClick={this.onClick}
        disabled={this.props.disabled}
      >
        <i className="fa fa-map-pin" />
      </Button>
    );
  }
}

function ExportButton(props) {
  const onClick = () => {
    props
      .exportSubSamples({ subsampleids: props.selectedObjects })
      .then(resp => {
        props.addToast({
          type: 'success',
          title: 'Exported sub samples',
          text: resp.data.message
        });
      });
  };

  return (
    <Tooltip tooltip="Export selected sub samples">
      <Button
        onClick={onClick}
        className="float-end me-1"
        size="sm"
        disabled={
          (props.selectedObjects && props.selectedObjects.length === 0) ||
          !props.selectedObjects ||
          props.pendingExportSubsamples
        }
      >
        <i
          className={`fa fa-${
            props.pendingExportSubsamples ? 'spinner fa-spin' : 'save'
          }`}
        />
      </Button>
    </Tooltip>
  );
}

ExportButton.defaultProps = {
  pendingExportSubsamples: false,
  selectedObjects: null
};

ExportButton.propTypes = {
  selectedObjects: PropTypes.arrayOf(PropTypes.number),
  pendingExportSubsamples: PropTypes.bool,
  exportSubSamples: PropTypes.func.isRequired,
  addToast: PropTypes.func.isRequired
};

function MoveCell(cell, row, rowIndex, formatExtraData) {
  return (
    <MoveButton
      id={row.subsampleid}
      moveToSubSample={formatExtraData.moveToSubSample}
      addToast={formatExtraData.addToast}
      disabled={!formatExtraData.operator || formatExtraData.runningScan}
    />
  );
}

function ActionCell(cell, row, rowIndex, formatExtraData) {
  return (
    <>
      <ViewButton
        id={row.subsampleid}
        subsample={row}
        updateObject={formatExtraData.updateObject}
        hasData={row.datacollections !== 0}
        subSampleSchema={formatExtraData.subSampleSchema}
      />
      <DeleteButton
        id={row.subsampleid}
        show={row.datacollections === 0}
        onClick={formatExtraData.removeObject}
        confirm
      />
    </>
  );
}

function TypeCell(cell, row) {
  const types = {
    poi: ['POI', 'info'],
    roi: ['ROI', 'primary'],
    loi: ['LOI', 'secondary']
  };

  return <Badge bg={types[row.type][1]}>{types[row.type][0]}</Badge>;
}

function DataCell(cell, row) {
  const badges = [];

  if (row.datacollections > 0) {
    badges.push(
      <Badge bg="success" className="me-1" key="b1">
        Data <Badge bg="light">{row.datacollections}</Badge>
      </Badge>
    );
  }

  if (row.queued === true) {
    badges.push(
      <Badge bg="warning" key="b2">
        Queued
      </Badge>
    );
  }

  return badges;
}

function SizeCell(cell, row) {
  if (row.type === 'roi') {
    const engw = Formatting.formatEng((row.x2 - row.x) * config.pixelSize);
    const engh = engw.multiplier * (row.y2 - row.y) * config.pixelSize;

    return `${Formatting.round(engw.scalar, 12)}x${Formatting.round(
      engh,
      12
    )} ${engw.prefix}m`;
  }

  if (row.type === 'loi') {
    const len = Math.sqrt((row.x2 - row.x) ** 2 + (row.y2 - row.y) ** 2);
    const eng = Formatting.formatEng(len * config.pixelSize);
    return `${Formatting.round(eng.scalar, 3)} ${eng.prefix}m`;
  }

  return '';
}

export function TagCell(cell) {
  return (
    <>
      {cell &&
        cell.tags &&
        cell.tags.map(tag => <Badge className="me-1">{tag}</Badge>)}
    </>
  );
}

export default class ObjectList extends Component {
  static defaultProps = {
    samples: {},
    currentSample: undefined,
    selectedObject: null,
    runningScan: undefined,
    operator: false,
    config: {
      options: {}
    },
    pendingExportSubsamples: false
  };

  static propTypes = {
    objects: PropTypes.objectOf(PropTypes.shape(ObjectType.types)).isRequired,
    samples: PropTypes.objectOf(PropTypes.shape(SampleType.types)),
    currentSample: PropTypes.number,
    selectedObject: PropTypes.number,
    selectedObjects: PropTypes.arrayOf(PropTypes.number),
    actions: PropTypes.shape({
      fetchObjects: PropTypes.func.isRequired,
      removeObject: PropTypes.func.isRequired,
      updateObject: PropTypes.func.isRequired,
      moveToSubSample: PropTypes.func.isRequired,
      addSelection: PropTypes.func.isRequired,
      removeSelection: PropTypes.func.isRequired,
      resetSelection: PropTypes.func.isRequired,
      exportSubSamples: PropTypes.func.isRequired,
      addToast: PropTypes.func.isRequired
    }).isRequired,
    operator: PropTypes.bool,
    runningScan: PropTypes.number,
    fetchingObjects: PropTypes.bool.isRequired,
    fetchedObjects: PropTypes.bool.isRequired,
    pendingExportSubsamples: PropTypes.bool,
    config: PropTypes.shape({})
  };

  constructor(props) {
    super(props);
    this.state = { deletingObjects: false };
  }

  componentDidMount() {
    if (
      !this.props.fetchingObjects &&
      !this.props.fetchedObjects &&
      this.props.currentSample
    ) {
      this.props.actions.fetchObjects();
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.currentSample !== this.props.currentSample) {
      this.props.actions.resetSelection();

      if (!this.props.fetchingObjects && !this.props.fetchedObjects) {
        if (this.props.currentSample) this.props.actions.fetchObjects();
      }
    }
  }

  deleteMultiple = () => {
    this.setState({ deletingObjects: true });
    const promises = [];
    each(this.props.selectedObjects, subsampleid => {
      promises.push(this.props.actions.removeObject(subsampleid));
    });

    Promise.all(promises).then(() => {
      this.setState({ deletingObjects: false });
      this.props.actions.resetSelection();
    });
  };

  render() {
    const columns = [
      { dataField: 'subsampleid', text: '#' },
      { dataField: 'type', text: 'Type', formatter: TypeCell },
      { dataField: 'size', text: 'Size', formatter: SizeCell },
      { dataField: 'extrametadata', text: 'Tags', formatter: TagCell },
      { dataField: 'datacollections', text: '', formatter: DataCell },
      {
        dataField: 'move',
        text: '',
        formatter: MoveCell,
        classes: 'text-end',
        formatExtraData: {
          moveToSubSample: this.props.actions.moveToSubSample,
          addToast: this.props.actions.addToast,
          operator: this.props.operator,
          runningScan: this.props.runningScan
        }
      },
      {
        dataField: 'action',
        text: '',
        formatter: ActionCell,
        classes: 'text-end text-nowrap',
        formatExtraData: {
          removeObject: this.props.actions.removeObject,
          updateObject: this.props.actions.updateObject,
          subSampleSchema: this.props.subSampleSchema
        }
      }
    ];

    const sample = this.props.samples[this.props.currentSample];

    const caption = (
      <>
        {(sample && sample.name) || 'No sample selected'}
        <Confirm
          title="Confirm"
          message="Are you sure you want to delete these items?"
        >
          {confirm => (
            <Button
              onClick={confirm(this.deleteMultiple)}
              className="float-end"
              size="sm"
              variant="danger"
              disabled={
                (this.props.selectedObjects &&
                  this.props.selectedObjects.length < 2) ||
                !this.props.selectedObjects ||
                this.state.deletingObjects
              }
            >
              <i
                className={`fa ${
                  this.state.deletingObjects ? 'fa-spin fa-spinner' : 'fa-times'
                }`}
              />
            </Button>
          )}
        </Confirm>
        {this.props.config.options.export && (
          <ExportButton
            selectedObjects={this.props.selectedObjects}
            pendingExportSubsamples={this.props.pendingExportSubsamples}
            exportSubSamples={this.props.actions.exportSubSamples}
            addToast={this.props.actions.addToast}
          />
        )}
        {this.props.config.options.import_reference && (
          <ImportReferenceButton sampleid={this.props.currentSample} />
        )}
        <ExtraMetadataButton
          objects={this.props.objects}
          selectedObjects={this.props.selectedObjects}
          updateObject={this.props.actions.updateObject}
        />
      </>
    );

    return (
      <RemountOnResize>
        <FullSizer>
          <SelectableTable
            caption={caption}
            classes="table-subsamples table-sticky-caption"
            keyField="subsampleid"
            data={values(this.props.objects) || []}
            loading={this.props.fetchingObjects}
            overlay
            columns={columns}
            noDataIndication="No objects defined"
            selectedItems={this.props.selectedObjects}
            typeKey="type"
            multiple
            actions={{
              addSelection: this.props.actions.addSelection,
              removeSelection: this.props.actions.removeSelection,
              resetSelection: this.props.actions.resetSelection
            }}
          />
        </FullSizer>
      </RemountOnResize>
    );
  }
}
