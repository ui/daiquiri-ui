import { createRef, Component } from 'react';
import PropTypes from 'prop-types';
import { keys, map, each } from 'lodash';
import { Button, Form, Container, Col, Row, Alert } from 'react-bootstrap';

import ButtonTriggerModalStore from 'connect/layout/ButtonTriggerModalStore';

export default class CompositeButton extends Component {
  static defaultProps = {
    selection: {},
    disabled: false,
    addingError: ''
  };

  static propTypes = {
    addCompositeMap: PropTypes.func.isRequired,
    selection: PropTypes.objectOf(PropTypes.number),
    maps: PropTypes.shape({}).isRequired,
    subsampleid: PropTypes.number.isRequired,
    disabled: PropTypes.bool,
    adding: PropTypes.bool.isRequired,
    addingError: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.colorRefs = [];
    each(['r', 'g', 'b'], c => {
      this.colorRefs[c] = createRef();
      this.colorRefs[`o${c}`] = createRef();
    });

    this.modalRef = createRef();
  }

  onClick = () => {
    const props = {};
    each(['r', 'g', 'b'], c => {
      props[c] = parseInt(this.colorRefs[c].current.value, 10);
      props[`${c}opacity`] =
        parseFloat(this.colorRefs[`o${c}`].current.value) / 100;
    });

    this.props
      .addCompositeMap({
        subsampleid: this.props.subsampleid,
        ...props
      })
      .then(() => this.modalRef.current.close());
  };

  render() {
    const opts = map(keys(this.props.selection), o => {
      const xmap = this.props.maps[o];
      const key = xmap.scalar || `${xmap.element} - ${xmap.edge}`;
      return (
        <option key={o} value={o}>
          {o}:{key}
        </option>
      );
    });

    const { selection } = this.props;
    const sortedSelection = Object.keys(selection).sort(
      (a, b) => selection[a] - selection[b]
    );

    return (
      <ButtonTriggerModalStore
        id="composite"
        button={<i className="fa fa-picture-o" />}
        buttonProps={{
          className: 'me-1 float-end',
          disabled: this.props.disabled
        }}
        title="Generate Composite Map"
        buttons={
          <Button disabled={this.props.adding} onClick={this.onClick}>
            {this.props.adding && (
              <>
                <i className="fa fa-spin fa-spinner" /> Creating
              </>
            )}
            {!this.props.adding && <>Create</>}
          </Button>
        }
        ref={this.modalRef}
        onClose={() => this.props.clearError('adding')}
      >
        <Container className="mt-3">
          {this.props.addingError && (
            <Alert variant="danger">
              Could not create composite map: {this.props.addingError}
            </Alert>
          )}

          <Form>
            <Form.Group as={Row}>
              <Col sm={3} />
              <Col>Map</Col>
              <Col>Opacity</Col>
            </Form.Group>
            <Form.Group as={Row}>
              <Form.Label column sm={3}>
                Red
              </Form.Label>
              <Col>
                <Form.Control
                  as="select"
                  defaultValue={sortedSelection[0]}
                  ref={this.colorRefs.r}
                >
                  {opts}
                </Form.Control>
              </Col>
              <Col>
                <Form.Control
                  type="range"
                  min="0"
                  max="100"
                  defaultValue="100"
                  ref={this.colorRefs.or}
                />
              </Col>
            </Form.Group>
            <Form.Group as={Row}>
              <Form.Label column sm={3}>
                Green
              </Form.Label>
              <Col>
                <Form.Control
                  as="select"
                  defaultValue={sortedSelection[1]}
                  ref={this.colorRefs.g}
                >
                  {opts}
                </Form.Control>
              </Col>
              <Col>
                <Form.Control
                  type="range"
                  min="0"
                  max="100"
                  defaultValue="100"
                  ref={this.colorRefs.og}
                />
              </Col>
            </Form.Group>
            <Form.Group as={Row}>
              <Form.Label column sm={3}>
                Blue
              </Form.Label>
              <Col>
                <Form.Control
                  as="select"
                  defaultValue={sortedSelection[2]}
                  ref={this.colorRefs.b}
                >
                  {opts}
                </Form.Control>
              </Col>
              <Col>
                <Form.Control
                  type="range"
                  min="0"
                  max="100"
                  defaultValue="100"
                  ref={this.colorRefs.ob}
                />
              </Col>
            </Form.Group>
          </Form>
        </Container>
      </ButtonTriggerModalStore>
    );
  }
}
