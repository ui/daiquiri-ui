import { each, range, min, max } from 'lodash';

export default class CompositeMap {
  constructor(comp, maps) {
    this.setMaps(comp, maps);
  }

  setMaps(comp, maps) {
    this.comp = comp;
    this.maps = maps;
    this.mins = {};
    this.maxs = {};
    each(maps, (map, i) => {
      if (map) {
        this.mins[i] = map.min || min(map.data);
        this.maxs[i] = map.max || max(map.data);
      }
    });
  }

  scaleValue(mid, val, log) {
    if (val >= this.maxs[mid]) return 1;
    if (val <= this.mins[mid]) return 0;

    if (log) {
      return (
        Math.log(val - this.mins[mid]) /
        Math.log(this.maxs[mid] - this.mins[mid])
      );
    }
    return (val - this.mins[mid]) / (this.maxs[mid] - this.mins[mid]);
  }

  image() {
    console.log('composite', this.maps, this.maps[0]);
    const map0 = this.maps[0];

    const buffer = new Uint8ClampedArray(map0.w * map0.h * 4);
    const { opacity } = this.comp;

    const rgb = ['r', 'g', 'b'];
    each(range(map0.h), y => {
      each(range(map0.w), x => {
        const pos = (y * map0.w + x) * 4;

        each(this.maps, (map, i) => {
          if (map) {
            const { snaked } = map;
            const did =
              snaked && y % 2 === 1
                ? y * map.w + (map.w - x - 1)
                : y * map.w + x;

            buffer[pos + i] =
              this.scaleValue(i, map.data[did], map.log) *
              255 *
              this.comp[`${rgb[i]}opacity`];
          }
        });

        buffer[pos + 3] = 255 * opacity;
      });
    });

    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');

    canvas.width = map0.w;
    canvas.height = map0.h;

    const idata = ctx.createImageData(map0.w, map0.h);
    idata.data.set(buffer);
    ctx.putImageData(idata, 0, 0);

    return canvas.toDataURL();
  }
}
