import { useState, useRef, useEffect } from 'react';
import { values } from 'lodash';

import { Button } from 'react-bootstrap';

import Table from 'components/table';
import DeleteButton from 'components/common/DeleteButton';
import ButtonTriggerModalStore from 'connect/layout/ButtonTriggerModalStore';
import type { ButtonTriggerModalStoreRef } from '../layout/models';
import PeriodicTable from 'components/common/PeriodicTable';
import LinesModal from 'components/common/LinesModal';

import SchemaForm from 'connect/SchemaForm';
import ROIBrowserButton from 'components/2dview/ROIBrowserButton';

interface INewROI {
  edge: string;
  element?: string;
  start: number;
  end: number;
  sampleid: number;
}

interface IROI extends INewROI {
  maproiid: number;
  maps: number;
}

interface IUpdateROIButton {
  roi: IROI;
  updateRoi: (payload: any) => Promise<any>;
  disabled: boolean;
}

function UpdateROIButton(props: IUpdateROIButton) {
  const { roi, updateRoi, disabled } = props;
  const modalRef = useRef<ButtonTriggerModalStoreRef>();
  const onSubmitRef = useRef(() => {});
  const disabledRef = useRef(false);
  const [render, forceRender] = useState<boolean>(true);

  function onSubmit(formData: Record<string, any>) {
    return updateRoi(formData).then(() => modalRef.current?.close());
  }
  return (
    <ButtonTriggerModalStore
      id={`updateMCAROI${roi.maproiid}`}
      ref={modalRef}
      title="Update MCA ROI"
      buttons={
        <Button
          onClick={() => onSubmitRef.current?.()}
          disabled={disabledRef.current}
        >
          Update ROI
        </Button>
      }
      button={<i className="fa fa-pencil" />}
      buttonProps={{ size: 'sm', disabled }}
    >
      <SchemaForm
        schema="XRFMapROISchema"
        onSubmit={onSubmit}
        formData={roi}
        requireOperator
        button={(d: boolean, os: () => void) => {
          disabledRef.current = d;
          onSubmitRef.current = os;
          forceRender(!render);
        }}
      />
    </ButtonTriggerModalStore>
  );
}

function ActionCell(
  cell: string,
  row: IROI,
  rowIndex: number,
  formatExtraData: Record<string, any>
) {
  return (
    <>
      <UpdateROIButton
        roi={row}
        updateRoi={formatExtraData.updateRoi}
        disabled={!formatExtraData.operator}
      />
      <DeleteButton
        id={row.maproiid}
        onClick={formatExtraData.deleteRoi}
        confirm
        show={row.maps === 0}
        disabled={!formatExtraData.operator}
      />
    </>
  );
}

interface IROIManager {
  rois: Record<string, IROI>;
  config: {
    mca: {
      sigma: number;
    };
  };
  operator: boolean;
  currentSample: number;
  actions: {
    deleteMapROI: (mapROIId: number) => Promise<any>;
    updateMapROI: (payload: IROI) => Promise<any>;
    addMapROI: (payload: INewROI) => Promise<any>;
    toggleModal: () => void;
  };
  modal: Record<string, any>;
}

export default function ROIManager(props: IROIManager) {
  const { rois, actions, operator, currentSample, config, modal } = props;
  const onSubmitRef = useRef(() => {});
  const disabledRef = useRef(false);
  const [render, forceRender] = useState<boolean>(true);
  const [currentElement, setCurrentElement] = useState<Record<string, any>>();
  const modalRef = useRef<ButtonTriggerModalStoreRef>();
  const linesModal = useRef<LinesModal>(null);

  function selectElement(id: number, element: Record<string, any>) {
    setCurrentElement(element);
    linesModal.current?.show();
  }

  function onSubmit(formData: IROI) {
    return actions.addMapROI(formData).then(() => modalRef.current?.close());
  }

  const columns = [
    { dataField: 'maproiid', text: 'Id' },
    { dataField: 'element', text: 'Element' },
    { dataField: 'edge', text: 'Edge' },
    { dataField: 'start', text: 'Start (eV)' },
    { dataField: 'end', text: 'End (eV)' },
    {
      dataField: 'action',
      text: '',
      classes: 'text-end',
      formatter: ActionCell,
      formatExtraData: {
        deleteRoi: actions.deleteMapROI,
        updateRoi: actions.updateMapROI,
        operator,
      },
    },
  ];

  return (
    <>
      <LinesModal
        ref={linesModal}
        actions={{
          toggleModal: actions.toggleModal,
          addMapROI: actions.addMapROI,
        }}
        modal={modal}
        operator={operator}
        element={currentElement}
        sampleid={currentSample}
        sigmaMca={config?.mca.sigma}
      />
      <PeriodicTable noEdges actions={{ onClick: selectElement }} />
      <div className="text-end mt-2 mb-1">
        <ROIBrowserButton
          operator={operator}
          sampleid={currentSample}
          actions={{
            addMapROI: actions.addMapROI,
          }}
        />
        <ButtonTriggerModalStore
          id="newMCAROI"
          ref={modalRef}
          title="Add MCA ROI"
          buttons={
            <Button
              onClick={() => onSubmitRef.current()}
              disabled={disabledRef.current}
            >
              Add ROI
            </Button>
          }
          button={
            <>
              <i className="fa fa-plus" />
              Add
            </>
          }
          buttonProps={{
            disabled: !operator,
          }}
        >
          <SchemaForm
            schema="NewXRFMapROISchema"
            onSubmit={onSubmit}
            formData={{
              sampleid: currentSample,
            }}
            requireOperator
            button={(d: boolean, os: () => void) => {
              disabledRef.current = d;
              onSubmitRef.current = os;
              forceRender(!render);
            }}
          />
        </ButtonTriggerModalStore>
      </div>
      <Table
        keyField="maproiid"
        data={values(rois) || []}
        columns={columns}
        noDataIndication="No ROIs defined"
      />
    </>
  );
}
