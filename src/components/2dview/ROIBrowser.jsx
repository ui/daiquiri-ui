import { createRef, Component } from 'react';
import PropTypes from 'prop-types';
import { values, map, each } from 'lodash';

import Table from 'components/table';

import { Form, Col, Row } from 'react-bootstrap';

export default class ROIBrowser extends Component {
  static propTypes = {
    samples: PropTypes.shape({}).isRequired,
    rois: PropTypes.shape({}).isRequired,
    onChange: PropTypes.func.isRequired,
    actions: PropTypes.shape({
      fetchMapROIs: PropTypes.func.isRequired
    }).isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      selection: {}
    };

    this.selectRef = createRef();
  }

  componentDidMount() {
    this.props.actions.fetchMapROIs(this.selectRef.current.value);
  }

  onChange() {
    if (this.props.onChange) {
      const rois = [];
      each(this.props.rois, r => {
        const { maproiid, maps, ...roi } = r;
        if (
          maproiid in this.state.selection &&
          this.state.selection[maproiid]
        ) {
          rois.push(roi);
        }
      });
      this.props.onChange(rois);
    }
  }

  setSelection = (row, isSelect, rowIndex, e) => {
    this.setState(
      (prev, props) => ({
        selection: {
          [row.maproiid]: isSelect,
          ...prev.selection
        }
      }),
      () => this.onChange()
    );
  };

  setSelectAll = (isSelect, rows, e) => {
    each(rows, (row, i) => {
      this.setSelection(row, isSelect, i, e);
    });
  };

  loadROIs = e => {
    this.props.actions.fetchMapROIs(e.target.value);
  };

  render() {
    const columns = [
      { dataField: 'maproiid', text: 'Id' },
      { dataField: 'element', text: 'Element' },
      { dataField: 'edge', text: 'Edge' },
      { dataField: 'start', text: 'Start (eV)' },
      { dataField: 'end', text: 'End (eV)' }
    ];

    return (
      <>
        <Form.Group as={Row}>
          <Form.Label column sm={3}>
            Sample
          </Form.Label>
          <Col>
            <Form.Control
              ref={this.selectRef}
              className="me-1"
              as="select"
              size="sm"
              onChange={this.loadROIs}
            >
              {map(this.props.samples, s => (
                <option value={s.sampleid} key={s.sampleid}>
                  {s.name}
                </option>
              ))}
            </Form.Control>
          </Col>
        </Form.Group>
        <Table
          keyField="maproiid"
          data={values(this.props.rois) || []}
          columns={columns}
          noDataIndication="No ROIs found"
          selectRow={{
            mode: 'checkbox',
            onSelect: this.setSelection,
            onSelectAll: this.setSelectAll
          }}
        />
      </>
    );
  }
}
