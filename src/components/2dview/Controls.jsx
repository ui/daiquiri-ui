import { Component, useCallback, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { map, find } from 'lodash';

import {
  Button,
  ButtonToolbar,
  Navbar,
  Form,
  DropdownButton,
  ToggleButtonGroup,
  ToggleButton,
  Container,
  Row,
  Col,
  InputGroup
} from 'react-bootstrap';

import Tooltip from 'components/common/Tooltip';
import ImageSelection from 'connect/samples/ImageSelection';
import ButtonTriggerModalStore from 'connect/layout/ButtonTriggerModalStore';

function SetQuantizeModal(props) {
  const sizeXRef = useRef();
  const sizeYRef = useRef();
  const modalRef = useRef();
  const formRef = useRef();
  const [isValidated, setIsValidated] = useState(false);

  const onSetQuantization = useCallback(() => {
    setIsValidated(true);
    if (!formRef.current.checkValidity()) return;
    props.setQuantization([
      parseFloat(sizeXRef.current.value, 10) * 1000, // in microns
      parseFloat(sizeYRef.current.value, 10) * 1000
    ]);
    modalRef.current.close();
  });

  return (
    <ButtonTriggerModalStore
      id="setQuantization"
      title="Set Quantization"
      ref={modalRef}
      buttonProps={{ className: 'me-1', size: 'sm' }}
      buttons={<Button onClick={onSetQuantization}>Set Quantization</Button>}
      button={
        <>
          <i className="fa fa-plus" />
        </>
      }
    >
      <Form ref={formRef} validated={isValidated}>
        <Form.Group as={Row}>
          <Form.Label column sm={3}>
            Quantization
          </Form.Label>
          <Col>
            <InputGroup>
              <Form.Control
                ref={sizeXRef}
                type="number"
                step="0.001"
                required
              />
              <InputGroup.Text>&micro;m</InputGroup.Text>
              <Form.Control.Feedback type="invalid">
                Please provide a quantization in X
              </Form.Control.Feedback>
            </InputGroup>
          </Col>
          <Col xs={1} className="text-center">
            x
          </Col>
          <Col>
            <InputGroup>
              <Form.Control
                ref={sizeYRef}
                type="number"
                step="0.001"
                required
              />
              <InputGroup.Text>&micro;m</InputGroup.Text>
              <Form.Control.Feedback type="invalid">
                Please provide a quantization in Y
              </Form.Control.Feedback>
            </InputGroup>
          </Col>
        </Form.Group>
      </Form>
    </ButtonTriggerModalStore>
  );
}

export default class Controls extends Component {
  static defaultProps = {
    showStaff: false,
    runningScan: undefined,
    currentSample: undefined,
    pendingSaveImage: false,
    pendingSaveCanvas: false,
    disableOrigin: false
  };

  static propTypes = {
    currentAction: PropTypes.string.isRequired,
    currentSample: PropTypes.number,
    actions: PropTypes.shape({
      selectAction: PropTypes.func.isRequired,
      toggleClampOrigin: PropTypes.func.isRequired,
      toggleCentreOnClick: PropTypes.func.isRequired,
      toggleDrawSnap: PropTypes.func.isRequired,
      toggleMultiROI: PropTypes.func.isRequired,
      toggleClampLOI: PropTypes.func.isRequired,
      toggleFillOrigin: PropTypes.func.isRequired,
      toggleDisableOrigin: PropTypes.func.isRequired,
      toggleCursor: PropTypes.func.isRequired,
      changeDrawQuantize: PropTypes.func.isRequired,
      saveImage: PropTypes.func.isRequired,
      saveCanvas: PropTypes.func.isRequired,
      addToast: PropTypes.func.isRequired,
      autoFocusImage: PropTypes.func.isRequired,
      setSourceHover: PropTypes.func.isRequired
    }).isRequired,
    operator: PropTypes.bool.isRequired,
    clampOrigin: PropTypes.bool.isRequired,
    fillOrigin: PropTypes.bool.isRequired,
    disableOrigin: PropTypes.bool,
    centreOnClick: PropTypes.bool.isRequired,
    drawSnap: PropTypes.bool.isRequired,
    multiROI: PropTypes.bool.isRequired,
    clampLOI: PropTypes.bool.isRequired,
    cursor: PropTypes.bool.isRequired,
    drawQuantize: PropTypes.number.isRequired,
    runningScan: PropTypes.number,
    user: PropTypes.shape({
      is_staff: PropTypes.bool
    }).isRequired,
    showStaff: PropTypes.bool,
    pendingSaveImage: PropTypes.bool,
    pendingSaveCanvas: PropTypes.bool,
    config: PropTypes.shape({})
  };

  componentDidMount() {
    this.props.actions.fetchOriginSettings();
  }

  setAction = type => {
    if (this.props.currentAction === type) this.props.actions.selectAction('');
    else {
      this.props.actions.selectAction(type);
    }
  };

  saveImage = () => {
    this.props.actions
      .saveImage({ sampleid: this.props.currentSample })
      .catch(err => {
        this.props.actions.addToast({
          type: 'error',
          title: 'Could not save image',
          text:
            (err.response && err.response.data && err.response.data.error) ||
            err.message
        });
      });
  };

  toggleClampOrigin = () => {
    this.props.actions.toggleClampOrigin(
      !this.props.clampOrigin,
      this.props.fillOrigin
    );
  };

  toggleFillOrigin = () => {
    this.props.actions.toggleFillOrigin(
      !this.props.fillOrigin,
      this.props.clampOrigin
    );
  };

  toggleDisableOrigin = () => {
    this.props.actions.toggleDisableOrigin(!this.props.disableOrigin);
  };

  toggleCentreOnClick = () => {
    this.props.actions.toggleCentreOnClick(!this.props.centreOnClick);
  };

  changeDrawQuantize = e => {
    const value = parseInt(e.target.value, 10);
    this.props.actions.changeDrawQuantize([value, value]);
  };

  toggleDrawSnap = () => {
    this.props.actions.toggleDrawSnap(!this.props.drawSnap);
  };

  toggleMultiROI = () => {
    this.props.actions.toggleMultiROI(!this.props.multiROI);
  };

  toggleClampLOI = () => {
    this.props.actions.toggleClampLOI(!this.props.clampLOI);
  };

  toggleCursor = () => {
    if (this.props.cursor) this.props.actions.setSourceHover({});
    this.props.actions.toggleCursor(!this.props.cursor);
  };

  saveCanvas = () => {
    this.props.actions.saveCanvas();
  };

  autoFocusImage = () => {
    this.props.actions.autoFocusImage();
  };

  toggleOriginSetting = settingKey => {
    this.props.actions.updateOriginSettings({
      [settingKey]: !this.props.originSettings[settingKey]
    });
  };

  selectSourceUrl = e => {
    this.props.actions.setSourceUrl(e.target.value);
  };

  render() {
    const drawQuantizations = {
      50000: '50um',
      25000: '25um',
      20000: '20um',
      10000: '10um',
      5000: '5um',
      1000: '1um',
      500: '500nm',
      200: '200nm'
    };

    const isSquareQuantize = this.props.drawQuantize
      ? this.props.drawQuantize[0] === this.props.drawQuantize[1]
      : true;

    const currentQuantization = !isSquareQuantize
      ? 'custom'
      : this.props.drawQuantize
      ? this.props.drawQuantize[0]
      : this.props.drawQuantize;

    const orig = find(this.props.sources, { origin: true });

    return (
      <Navbar bg="cyan" className="p-2">
        <Form className="text-nowrap">
          <Tooltip tooltip="Enable draw quantisation">
            <Form.Control
              className="me-1 d-inline"
              style={{ width: 'auto' }}
              as="select"
              size="sm"
              value={currentQuantization}
              onChange={this.changeDrawQuantize}
            >
              {map(drawQuantizations, (name, value) => (
                <option value={value}>{name}</option>
              ))}
              {this.props.drawQuantize &&
                this.props.drawQuantize[0] !== this.props.drawQuantize[1] && (
                  <option value="custom">
                    {this.props.drawQuantize[0] / 1000}x
                    {this.props.drawQuantize[1] / 1000} &micro;m
                  </option>
                )}
            </Form.Control>
          </Tooltip>
          <SetQuantizeModal
            setQuantization={this.props.actions.changeDrawQuantize}
          />

          <Tooltip tooltip="Snap to nearest object">
            <ToggleButtonGroup
              type="checkbox"
              value={this.props.drawSnap}
              onChange={this.toggleDrawSnap}
            >
              <ToggleButton value size="sm" className=" me-2" id="snap">
                <i className="fa fa-object-ungroup me-1" />
                Snap
              </ToggleButton>
            </ToggleButtonGroup>
          </Tooltip>
        </Form>

        <Form>
          <ButtonToolbar>
            <ToggleButtonGroup
              type="radio"
              name="action"
              value={this.props.currentAction}
              onChange={this.setAction}
              className="me-1"
            >
              <ToggleButton value="" size="sm" id="off">
                <i className="fa fa-power-off me-1" />
                Off
              </ToggleButton>
              <ToggleButton
                value="mosaic"
                id="mosaic"
                size="sm"
                disabled={
                  !this.props.operator ||
                  this.props.runningScan ||
                  !this.props.currentSample
                }
              >
                <i className="fa fa-th me-1" />
                Mosaic
              </ToggleButton>
              {this.props.config.options.import_reference && (
                <ToggleButton
                  value="reference"
                  id="reference"
                  size="sm"
                  disabled={
                    !this.props.operator ||
                    this.props.runningScan ||
                    !this.props.selectedSampleActionId
                  }
                >
                  <i className="fa fa-map-marker me-1" />
                  Ref
                </ToggleButton>
              )}
              <ToggleButton
                value="poi"
                size="sm"
                id="poi"
                disabled={!this.props.currentSample}
              >
                <i className="fa fa-map-pin me-1" />
                POI
              </ToggleButton>
              <ToggleButton
                value="roi"
                id="roi"
                size="sm"
                disabled={!this.props.currentSample}
              >
                <i className="fa fa-square-o me-1" />
                ROI
                <Tooltip tooltip="Draw multiple ROIs">
                  <Form.Check
                    className="d-inline ms-1"
                    type="checkbox"
                    checked={this.props.multiROI}
                    value="1"
                    onChange={this.toggleMultiROI}
                  />
                </Tooltip>
              </ToggleButton>
              <ToggleButton
                value="loi"
                id="loi"
                size="sm"
                disabled={!this.props.currentSample}
              >
                <i className="fa fa-long-arrow-right me-1" />
                LOI
                <Tooltip tooltip="Clamp LOIs">
                  <Form.Check
                    className="d-inline ms-1"
                    type="checkbox"
                    checked={this.props.clampLOI}
                    value="1"
                    onChange={this.toggleClampLOI}
                  />
                </Tooltip>
              </ToggleButton>
              <ToggleButton value="measure" id="measure" size="sm">
                <i className="fa fa-expand me-1" />
                Measure
              </ToggleButton>
              <ToggleButton value="pan" id="pan" size="sm">
                <i className="fa fa-arrows me-1" />
                Pan
              </ToggleButton>
              <ToggleButton
                value="move"
                id="move"
                size="sm"
                disabled={!this.props.operator || this.props.runningScan}
              >
                <i className="fa fa-map-marker me-1" />
                Move
                {(this.props.originSettings.fine_fixed ||
                  this.props.originSettings.coarse_fixed) &&
                  this.props.originSettings.config.allow_fixed_axes && (
                    <Tooltip tooltip="One axis is currently fixed">
                      <div className="bg-warning px-1 ml-1 rounded text-dark">
                        <i className="fa fa-warning" />
                      </div>
                    </Tooltip>
                  )}
              </ToggleButton>

              {this.props.user.is_staff && this.props.showStaff && (
                <ToggleButton value="beam" size="sm">
                  <i
                    className="fa fa-crosshairs me-1"
                    disabled={this.props.runningScan}
                  />
                  Move Beam
                </ToggleButton>
              )}
              {this.props.user.is_staff && this.props.showStaff && (
                <ToggleButton
                  value="calib"
                  size="sm"
                  disabled={this.props.runningScan}
                >
                  <i className="fa fa-map-calib me-1" />
                  Calibrate
                </ToggleButton>
              )}
            </ToggleButtonGroup>

            {this.props.originSettings.has_fine &&
              this.props.originSettings.config.allow_fixed_axes && (
                <DropdownButton size="sm" menuAlign="right">
                  <Container className="pl-2 pr-2">
                    <Form.Check
                      type="switch"
                      id="fine-switch"
                      label="Fix Fine Axes"
                      checked={this.props.originSettings.fine_fixed}
                      onChange={() => this.toggleOriginSetting('fine_fixed')}
                    />
                    <Form.Check
                      type="switch"
                      id="coarse-switch"
                      label="Fix Coarse Axes"
                      className="text-nowrap"
                      checked={this.props.originSettings.coarse_fixed}
                      onChange={() => this.toggleOriginSetting('coarse_fixed')}
                    />
                  </Container>
                </DropdownButton>
              )}

            {this.props.config.options.centre_selection && (
              <Tooltip tooltip="Centre object on selection">
                <ToggleButtonGroup
                  className="me-1"
                  type="checkbox"
                  value={this.props.centreOnClick}
                  onChange={this.toggleCentreOnClick}
                >
                  <ToggleButton value size="sm">
                    <i className="fa fa-align-center me-1" />
                    Centre
                  </ToggleButton>
                </ToggleButtonGroup>
              </Tooltip>
            )}

            <Tooltip tooltip="Toggle showing cursor values and click to show">
              <ToggleButtonGroup
                type="checkbox"
                value={this.props.cursor}
                onChange={this.toggleCursor}
                className="ms-1 me-1"
              >
                <ToggleButton
                  value
                  id="cursor"
                  size="sm"
                  className="text-center"
                >
                  <i className="fa fa-crosshairs me-1" />
                  Cursor
                </ToggleButton>
              </ToggleButtonGroup>
            </Tooltip>

            <DropdownButton
              size="sm"
              title={
                <>
                  <i className="fa fa-video-camera me-1" /> Video
                </>
              }
              className="me-1"
            >
              <Container className="pl-2 pr-2">
                <div className="d-grid gap-2 mb-1">
                  <Tooltip tooltip="Clamp the viewport to the video source (unclamp to pan)">
                    <ToggleButtonGroup
                      type="checkbox"
                      value={this.props.clampOrigin}
                      onChange={this.toggleClampOrigin}
                    >
                      <ToggleButton
                        value
                        size="sm"
                        className="text-start"
                        id="clamp-viewport"
                      >
                        <i
                          className={`fa fa-fw me-1 ${
                            this.props.clampOrigin ? 'fa-lock' : 'fa-unlock'
                          }`}
                        />
                        Clamp
                      </ToggleButton>
                    </ToggleButtonGroup>
                  </Tooltip>
                </div>

                {orig?.additional_urls?.length > 0 && (
                  <div className="d-grid gap-2 mb-1 border bg-primary rounded p-1 text-light">
                    <div className="mx-1" style={{ fontSize: '0.7875rem' }}>
                      <i className="fa fa-fw me-1 fa-film" />
                      Stream
                    </div>
                    <div
                      className="bg-light text-primary rounded p-1"
                      style={{ fontSize: '0.7875rem' }}
                    >
                      <Form.Check
                        name="sourceUrl"
                        checked={this.props.sourceUrl === ''}
                        type="radio"
                        value=""
                        label="default"
                        onChange={this.selectSourceUrl}
                        className="mb-0"
                      />
                      {orig.additional_urls.map(additional_url => {
                        return (
                          <>
                            <hr style={{ margin: '0.1rem' }} />
                            <Form.Check
                              name="sourceUrl"
                              type="radio"
                              checked={
                                this.props.sourceUrl === additional_url.name
                              }
                              value={additional_url.name}
                              label={additional_url.name}
                              onChange={this.selectSourceUrl}
                              className="mb-0"
                            />
                          </>
                        );
                      })}
                    </div>
                  </div>
                )}

                <div className="d-grid gap-2 mb-1">
                  <Tooltip tooltip="Fill the viewport with the video source">
                    <ToggleButtonGroup
                      type="checkbox"
                      value={this.props.fillOrigin}
                      onChange={this.toggleFillOrigin}
                    >
                      <ToggleButton
                        value
                        size="sm"
                        className="text-start"
                        id="fill-viewport"
                      >
                        <i className="fa fa-fw fa-plus-square-o me-1" />
                        Fill
                      </ToggleButton>
                    </ToggleButtonGroup>
                  </Tooltip>
                </div>

                {this.props.config.options.autofocus && (
                  <div className="d-grid gap-2 mb-1">
                    <Tooltip tooltip="Autofocus the video source">
                      <Button
                        size="sm"
                        onClick={this.autoFocusImage}
                        disabled={
                          !this.props.operator || this.props.runningScan
                        }
                        className="text-start"
                      >
                        <i className="fa fa-fw fa-eye me-1" /> Autofocus
                      </Button>
                    </Tooltip>
                  </div>
                )}

                <div className="d-grid gap-2 mb-1">
                  <Tooltip tooltip="Disable the video source">
                    <ToggleButtonGroup
                      type="checkbox"
                      value={this.props.disableOrigin}
                      onChange={this.toggleDisableOrigin}
                    >
                      <ToggleButton
                        value
                        size="sm"
                        className="text-start"
                        id="disable-video"
                      >
                        <i className="fa fa-fw fa-power-off me-1" /> Disable
                      </ToggleButton>
                    </ToggleButtonGroup>
                  </Tooltip>
                </div>
              </Container>
            </DropdownButton>

            <DropdownButton
              size="sm"
              title={
                <>
                  <i className="fa fa-picture-o me-1" /> Images
                </>
              }
              menuAlign="right"
            >
              <Container className="pl-2 pr-2">
                <div className="d-grid gap-2 mb-1">
                  <Tooltip tooltip="Save the current sample image">
                    <Button
                      size="sm"
                      onClick={this.saveImage}
                      disabled={
                        !this.props.currentSample || this.props.pendingSaveImage
                      }
                      className="text-start"
                    >
                      <i className="me-1 fa fa-camera" />
                      {this.props.pendingSaveImage && (
                        <>
                          Saving <i className="fa fa-spin fa-spinner" />
                        </>
                      )}
                      {!this.props.pendingSaveImage && <>Save</>}
                    </Button>
                  </Tooltip>
                </div>

                <div className="d-grid gap-2 mb-1">
                  <Tooltip tooltip="Save the current canvas">
                    <Button
                      size="sm"
                      onClick={this.saveCanvas}
                      disabled={
                        !this.props.currentSample ||
                        this.props.pendingSaveCanvas
                      }
                      className="text-start"
                    >
                      <i className="me-1 fa fa-picture-o" />
                      {this.props.pendingSaveCanvas && (
                        <>
                          Saving <i className="fa fa-spin fa-spinner" />
                        </>
                      )}
                      {!this.props.pendingSaveCanvas && <>Canvas</>}
                    </Button>
                  </Tooltip>
                </div>

                <ImageSelection />
              </Container>
            </DropdownButton>
          </ButtonToolbar>
        </Form>
      </Navbar>
    );
  }
}
