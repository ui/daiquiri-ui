import { createRef, Component } from 'react';
import { keys, each, values, map } from 'lodash';
import { Form, Row, Col, ListGroup } from 'react-bootstrap';
import { Formatting } from '@esrf/daiquiri-lib';

import SchemaForm from 'connect/SchemaForm';

export interface Sample {
  subsampleid: number;
  type: string;
}

export interface Config {
  scantypes: Record<string, string[]>;
}

interface Props {
  objects?: Sample[];
  config?: Config;
  onSubmit?: () => void;
  actions: {
    setSchema: (schema: any) => void;
    setSchemaType: (type: string) => void;
  };
  schema?: string;
  formData?: any;
  type?: string;
  button?: (d: boolean, os: (value: boolean) => void) => void;
}

interface State {
  asyncAdditional: any;
  scanType: string;
}

export default class NewScan extends Component<Props, State> {
  private readonly selectRef: React.RefObject<HTMLSelectElement>;
  private readonly scans: string[];

  public constructor(props: Props) {
    super(props);

    const otype = values(this.props.objects)[0].type;
    if (this.props.config && otype in this.props.config.scantypes) {
      this.scans = this.props.config.scantypes[otype];
    } else {
      this.scans = [];
    }

    this.state = {
      asyncAdditional: {},
      scanType: otype,
    };

    this.selectRef = createRef();
  }

  public componentDidMount() {
    if ((this.props.type || '') !== this.state.scanType) {
      this.props.actions.setSchemaType(this.state.scanType);
      this.changeSchema();
    }
  }

  public render() {
    const opts = map(this.scans, (s) => (
      <option key={s} value={s}>
        {s}
      </option>
    ));

    function getFirstId(props: Props) {
      if (!props.objects) {
        return null;
      }
      if (kys.length === 0) {
        return null;
      }
      // @ts-expect-error
      return props.objects[kys[0]].subsampleid;
    }

    const kys = keys(this.props.objects);
    const firstid = getFirstId(this.props);
    const { schema = '', formData = {}, objects = [] } = this.props;

    return (
      <>
        <Form>
          <Form.Group as={Row}>
            <Form.Label column sm={3}>
              Type
            </Form.Label>
            <Col>
              <Form.Control
                ref={this.selectRef}
                as="select"
                value={schema}
                onChange={this.changeSchema}
              >
                {opts}
              </Form.Control>
            </Col>
          </Form.Group>
        </Form>

        <hr />

        {schema === '' ? null : (
          <>
            <SchemaForm
              schema={`${Formatting.ucfirst(schema)}Schema`}
              formData={{ subsampleid: firstid, ...formData }}
              onSubmit={this.onSubmit}
              requireOperator
              additionalFormData={{
                objects,
              }}
              onAsyncValidate={this.onAsyncValidate}
              submitText="Execute now"
              button={this.props.button}
            />
            {this.state.asyncAdditional.time_estimate && (
              <ListGroup
                className="mt-1 mx-auto"
                style={{ bottom: 0, position: 'sticky', width: '50%' }}
              >
                <ListGroup.Item className="text-center">
                  Estimated Time:{' '}
                  {Formatting.toHoursMins(
                    this.state.asyncAdditional.time_estimate
                  )}
                </ListGroup.Item>
              </ListGroup>
            )}
          </>
        )}
      </>
    );
  }

  private readonly changeSchema = () => {
    // rjsf seems to do some internal caching of the definitions
    // part of the schema, passing a changed schema does not clear this
    // cached value, need to force a component remount on changing
    // schemas
    const newValue = this.selectRef?.current?.value;
    this.props.actions.setSchema('');
    setTimeout(() => this.props.actions.setSchema(newValue), 50);
  };

  private readonly onSubmit = (formData: any, e: any, doSubmit: any) => {
    const ps = [];
    if (this.props.objects && this.props.objects.length > 0) {
      each(this.props.objects, (obj) => {
        ps.push(
          doSubmit({
            ...formData,
            subsampleid: obj.subsampleid,
          })
        );
      });
    } else {
      ps.push(doSubmit(formData));
    }

    return Promise.all(ps).then(() => {
      if (this.props.onSubmit) this.props.onSubmit();
    });
  };

  private readonly onAsyncValidate = (data: any) => {
    this.setState({ asyncAdditional: data.additionalAsync });
  };
}
