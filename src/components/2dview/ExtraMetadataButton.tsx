import ButtonTriggerModalStore from 'connect/layout/ButtonTriggerModalStore';
import ExtraMetadata from 'connect/2dview/ExtraMetadata';
import { cloneDeep } from 'lodash';

interface Props {
  objects: Record<string, Record<string, any>>;
  updateObject: (data: Record<string, any>) => Promise<any>;
  selectedObjects: number[];
}

export default function ExtraMetadataButton(props: Props) {
  const { selectedObjects, updateObject, objects } = props;
  function onChange(
    { field, value }: { field: string; value: any },
    addedTags: string[],
    removedTags: string[]
  ) {
    const promises = selectedObjects.map((subsampleid) => {
      const extraMetadata = cloneDeep(objects[subsampleid].extrametadata);
      const newTags = [
        ...new Set([...(extraMetadata?.tags || []), ...addedTags]),
      ];
      if (removedTags.length > 0) {
        const removedIndex = newTags.indexOf(removedTags[0]);
        newTags.splice(removedIndex, 1);
      }

      return updateObject({
        subsampleid,
        [field]: {
          ...extraMetadata,
          tags: newTags,
        },
      });
    });

    return Promise.all(promises);
  }

  const subSampleTags =
    Object.values(objects).length > 0 && selectedObjects
      ? Object.values(objects)
          .filter((obj) => selectedObjects.includes(obj.subsampleid))
          .map((obj) => (obj.extrametadata ? obj.extrametadata.tags : []))
      : [];
  const commonTags =
    subSampleTags.length > 0
      ? subSampleTags.reduce((a, b) => a.filter((c: string) => b?.includes(c)))
      : [];

  return (
    <ButtonTriggerModalStore
      tooltip="Manage extra metadata"
      id="extraMetadata"
      button={<i className="fa fa-list" />}
      buttonProps={{
        className: 'float-end me-1',
        size: 'sm',
        disabled:
          (selectedObjects && selectedObjects.length === 0) || !selectedObjects,
      }}
      title="Extra Metadata"
    >
      <ExtraMetadata
        subSamples
        onChange={onChange}
        extraMetadata={{ tags: commonTags }}
      />
    </ButtonTriggerModalStore>
  );
}
