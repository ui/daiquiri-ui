import { useEffect } from 'react';
import { filter, keys, values } from 'lodash';
import { Form } from 'react-bootstrap';
import { FullSizer } from '@esrf/daiquiri-lib';

import RemountOnResize from 'components/utils/RemountOnResize';
import DCList from 'components/dc/DCList';
import MapList from 'components/samples/MapList';
import CompositeList from 'components/2dview/CompositeList';
import CompositeButton from 'components/2dview/CompositeButton';
import NewScanButton from 'components/samples/NewScanButton';
import RoiManagerButton from 'components/2dview/ROIManagerButton';
import HideAllMapsButton from 'components/2dview/HideAllMapsButton';
import SavingButton from 'connect/2dview/SavingButton';
import RegenerateMapsButton from 'connect/2dview/RegenerateMapsButton';

import AutoScalarMaps from './AutoScalarMaps';
import { useLastNotification } from 'components/utils/hooks';
import type { DataCollection } from '../../types/DataCollection';

interface IObjectView {
  dcnamespace: string;
  config: { scantypes: Record<string, any> };
  object: Record<string, any>;
  maps: Record<string, any>;
  mapHistograms: Record<string, any>;
  composites: Record<string, any>;
  subsamples: Record<string, any>;
  datacollections: DataCollection[];
  selectedSubSamples: number[];
  mapSelection: Record<string, any>;
  mapSettings: {
    during_scan: boolean;
  };
  operator: boolean;
  compositeAdding: boolean;
  compositeError: string;
  actions: {
    updateMap: (payload: Record<string, any>) => Promise<any>;
    selectMap: ({ mapid, index }: { mapid: number; index: number }) => void;
    unselectMap: (mapid: number) => void;
    deleteMap: (mapid: number) => Promise<any>;
    fetchMapHistogram: (payload: Record<string, any>) => Promise<any>;
    resetMapSelection: () => void;
    addCompositeMap: (compositeMap: Record<string, any>) => Promise<any>;
    updateCompositeMap: (compositeMap: Record<string, any>) => Promise<any>;
    deleteCompositeMap: (compositeId: number) => Promise<any>;
    fetchAssociated: () => Promise<any>;
    clearErrorCompositeMap: (error: string) => void;
    fetchMapSettings: () => Promise<any>;
    updateMapSettings: (settings: Record<string, any>) => Promise<any>;
  };
}

export default function ObjectView(props: IObjectView) {
  useEffect(() => {
    props.actions.fetchMapSettings();
  }, []);

  useEffect(() => {
    if (keys(props.object).length > 0) {
      props.actions.resetMapSelection();
      props.actions.fetchAssociated();
    }
  }, [props.object]);

  const event = useLastNotification(['xrf_map']);

  useEffect(() => {
    console.log('new event', event);
    props.actions.fetchAssociated();
  }, [event]);

  if (keys(props.object).length === 0) return <div>No object selected</div>;

  const maps = filter(props.maps, {
    subsampleid: props.object.subsampleid,
  });
  const composites = filter(props.composites, {
    subsampleid: props.object.subsampleid,
  });

  const sel = keys(props.mapSelection);

  const objects = filter(values(props.subsamples), (obj) =>
    props.selectedSubSamples.includes(Number.parseInt(obj.subsampleid, 10))
  );

  return (
    <RemountOnResize>
      <FullSizer>
        <div className="object" data-testid="objectview">
          <h2>
            Data Collections
            <NewScanButton
              prefix="object"
              objects={objects}
              config={props.config}
              disabled={!props.operator}
            />
            <SavingButton />
          </h2>
          <DCList
            datacollections={props.datacollections}
            namespace={props.dcnamespace}
            scan={false}
            createScalarMap
            showFollow={false}
            pages={0}
            fetching={false}
            page={0}
            per_page={0}
            total={0}
          />
          {props.object.type === 'roi' && (
            <>
              <h2>
                Maps
                <RoiManagerButton />
                <RegenerateMapsButton
                  subsampleid={props.object.subsampleid}
                  buttonProps={{ className: 'float-end me-1' }}
                />
                <CompositeButton
                  maps={props.maps}
                  selection={props.mapSelection}
                  subsampleid={props.object.subsampleid}
                  disabled={sel.length !== 3}
                  addCompositeMap={props.actions.addCompositeMap}
                  adding={props.compositeAdding}
                  addingError={props.compositeError}
                  clearError={props.actions.clearErrorCompositeMap}
                />
                <HideAllMapsButton
                  maps={maps}
                  actions={{
                    updateMap: props.actions.updateMap,
                  }}
                />
                <AutoScalarMaps
                  mapSettings={props.mapSettings}
                  updateMapSettings={props.actions.updateMapSettings}
                />
                <span className="inline-toggle float-end me-1">
                  <Form.Switch
                    variant="secondary"
                    label="Update"
                    id="during-scan"
                    onClick={() =>
                      void props.actions.updateMapSettings({
                        during_scan: !props.mapSettings.during_scan,
                      })
                    }
                    defaultChecked={props.mapSettings.during_scan}
                  />
                </span>
              </h2>
              <MapList
                maps={maps}
                mapHistograms={props.mapHistograms}
                actions={{
                  updateMap: props.actions.updateMap,
                  selectMap: props.actions.selectMap,
                  unselectMap: props.actions.unselectMap,
                  deleteMap: props.actions.deleteMap,
                  fetchHistogram: props.actions.fetchMapHistogram,
                }}
              />
              <CompositeList
                composites={composites}
                actions={{
                  updateCompositeMap: props.actions.updateCompositeMap,
                  deleteCompositeMap: props.actions.deleteCompositeMap,
                }}
              />
            </>
          )}
        </div>
      </FullSizer>
    </RemountOnResize>
  );
}
