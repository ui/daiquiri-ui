import { createRef } from 'react';
import {
  each,
  eachRight,
  find,
  values,
  remove,
  debounce,
  invert,
  filter,
  map
} from 'lodash';
import PropTypes from 'prop-types';
import { fabric } from 'fabric';
import { Button } from 'react-bootstrap';
import { Formatting } from '@esrf/daiquiri-lib';

import config from 'config/config';
import Drawing from 'components/2dview/Drawing';
import CanvasEnhancer from 'components/utils/CanvasEnhancer';
import MCAModal from 'connect/2dview/MCAModal';
import ValueModal from 'connect/2dview/ValueModal';
import ButtonTriggerModalStore from 'connect/layout/ButtonTriggerModalStore';

export default class Overlay extends CanvasEnhancer {
  static defaultProps = {
    maxZoom: 8,
    minZoom: 0.00002
  };

  static propTypes = {
    currentSampleId: PropTypes.number,
    currentAction: PropTypes.string,
    objects: PropTypes.objectOf(
      PropTypes.shape({
        subsampleid: PropTypes.number.isRequired,
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired
      })
    )
  };

  zindexes = {
    images: 0,
    sources: 50,
    maps: 200,
    composite: 300,
    polyline: 500,
    rois: 1000,
    pois: 2000,
    lois: 2500,
    markings: 3000,
    refs: 2800
  };

  render() {
    return (
      <>
        <ButtonTriggerModalStore
          id="mosaic"
          ref={this.mosaicRef}
          showButton={false}
          title="Start Mosaic"
          onClose={this.cancelMosaic}
          buttons={<Button onClick={this.startMosaic}>Start</Button>}
        >
          <p>Are you sure you want to start this mosaic?</p>
        </ButtonTriggerModalStore>
        <ValueModal />
        <MCAModal
          providers={{
            scans: {
              spectra: {
                namespace: 'mcamodal'
              }
            }
          }}
        />
        {super.render()}
      </>
    );
  }

  constructor(props) {
    super(props);
    this.props.actions.setZoom = debounce(this.props.actions.setZoom, 50);
    this.debouncedPlotImages = debounce(this.plotImages, 50);
    this.props.actions.setViewport = debounce(
      this.props.actions.setViewport,
      50
    );
    this.mcaModal = createRef();
    window.addEventListener('blur', this.onFocusLost);
  }

  addObject(options) {
    // console.log('addObject', options, this.objs[options.type]);
    this.objs[options.type].push(options.object);
  }

  getSelectedObjects = () => {
    return this.props.selectedObjects || [];
  };

  initFabric() {
    this.mosaicRef = createRef();
    this.inExternalUpdate = false;

    this.objs = {
      sources: [],
      images: [],
      maps: [],
      composite: [],
      rois: [],
      pois: [],
      lois: [],
      markings: [],
      polyline: [],
      refs: []
    };
    super.initFabric();

    const ctx = this.canvasRef.current.getContext('2d');
    ctx.imageSmoothingEnabled = false;

    this.draw = new Drawing({
      fabric: this.fabric,
      width: this.state.width,
      addObject: this.addObject.bind(this),
      flip_x: this.props.config.options.flip_x,
      getSelectedObjects: this.getSelectedObjects
    });

    this.fabric.selection = false;
    this.fabric.setZoom(0.2);

    this.fabric.on('mouse:wheel', this.onMouseWheel);
    this.fabric.on('mouse:down', this.onMouseDown);
    this.fabric.on('mouse:move', this.onMouseMove);
    this.fabric.on('mouse:up', this.onMouseUp);

    this.fabric.on('object:moved', this.objectMoved);
    this.fabric.on('object:scaled', this.objectScaled);
    this.fabric.on('object:scaling', this.objectScaling);

    this.fabric.on('selection:created', this.selectionCreated);
    this.fabric.on('selection:updated', this.selectionUpdated);
    this.fabric.on('selection:cleared', this.selectionCleared);

    this.props.actions.fetchSources().then(() => {
      this.plotSources();
      this.props.actions.fetchComponents();
    });

    this.calibStep = 0;

    this.markingInterval = setInterval(() => {
      this.plotMarkings();
    }, 500);

    this.roiMouseMove = debounce(this.roiMouseMove.bind(this), 200);
    this.roiMouseOut = debounce(this.roiMouseOut.bind(this), 500);
    this.sourceMouseMove = debounce(this.sourceMouseMove.bind(this), 10);
    this.requestAnimFrame();
  }

  requestAnimFrame = () => {
    if (this.destroyed) return;

    this.fabric.requestRenderAll();
    clearTimeout(this.refreshThread);
    this.refreshThread = setTimeout(() => {
      this.requestAnimFrame();
    }, 66);
  };

  componentWillUnmount() {
    clearTimeout(this.refreshThread);
    clearTimeout(this.markingInterval);
    clearTimeout(this.measureTimeout);
    this.destroyed = true;
    this.fabric.dispose();
    window.removeEventListener('blur', this.onFocusLost);
  }

  onFocusLost = () => {
    if (this.props.currentAction === 'move') {
      this.props.actions.selectAction('pan');
    }
  };

  saveCanvas() {
    if (this.props.config.upload_canvas) {
      this.props.actions
        .saveCanvasRemote({
          sampleid: this.props.currentSampleId,
          image: this.fabric.toDataURL()
        })
        .then(() => {
          this.props.actions.clearSaveCanvas();
        })
        .catch(() => {
          this.props.actions.clearSaveCanvas();
        });
    } else {
      const link = document.createElement('a');
      link.href = this.fabric.toDataURL();
      link.download = `${this.props.currentSample.name}_canvas.png`;
      link.click();

      this.props.actions.clearSaveCanvas();
    }
  }

  selectSample() {
    console.log('select sample', this.props.currentSampleId);
    if (this.props.currentSampleId) {
      this.props.actions.fetchSample({ sampleid: this.props.currentSampleId });
    }
  }

  objectScaling = e => {
    const obj = e.target;

    const w = obj.width * obj.scaleX;
    const h = obj.height * obj.scaleY;

    let qw = w;
    let qh = h;
    if (this.props.drawQuantize) {
      const dw = w % this.props.drawQuantize[0];
      const dh = h % this.props.drawQuantize[1];

      qw =
        w -
        dw +
        (dw > this.props.drawQuantize[0] / 2 ? this.props.drawQuantize[0] : 0);
      qh =
        h -
        dh +
        (dh > this.props.drawQuantize[1] / 2 ? this.props.drawQuantize[1] : 0);
    }

    obj.set({
      width: qw,
      height: qh,
      scaleX: 1,
      scaleY: 1
    });

    obj.getObjects()[0].set({
      left: -qw / 2,
      top: -qh / 2,
      width: qw,
      height: qh
    });

    obj.getObjects()[1].set({
      left: qw / 2,
      top: qh / 2
    });
  };

  objectScaled = e => {
    const obj = e.target;
    const w = obj.width * obj.scaleX;
    const h = obj.height * obj.scaleY;

    obj.set({
      width: w,
      height: h,
      scaleX: 1,
      scaleY: 1
    });

    obj.getObjects()[0].set({
      left: -w / 2,
      top: -h / 2,
      width: w,
      height: h
    });

    obj.getObjects()[1].set({
      left: w / 2,
      top: h / 2
    });

    const xp = obj.left;
    const yp = obj.top;
    this.props.actions.updateObject({
      subsampleid: obj.subsampleid,
      x: xp,
      y: yp,
      x2: xp + w,
      y2: yp + h
    });
  };

  objectMoved = e => {
    const obj = e.target;
    const object = this.props.objects[obj.subsampleid];

    const zoom = this.fabric.getZoom();
    const nearest = this.getNearest(obj.left, obj.top, zoom);

    const left = nearest ? nearest.left : obj.left;
    const top = nearest ? nearest.top : obj.top;

    this.props.actions.updateObject({
      subsampleid: obj.subsampleid,
      x: Math.round(left),
      y: Math.round(top),
      x2: Math.round(left + (object.x2 - object.x)),
      y2: Math.round(top + (object.y2 - object.y))
    });
  };

  selectionCreated = e => {
    // if (this.props.currentAction) return false

    if (e.target) {
      this.draw.select({ object: e.target });
      if (!this.inExternalUpdate) {
        this.props.actions.resetSelection();
        this.props.actions.addSelection({ subsampleid: e.target.subsampleid });
      }
    }
  };

  selectionUpdated = e => {
    // if (this.props.currentAction) return false

    if (e.deselected) {
      this.draw.select({ object: e.deselected[0], deselect: true });
      if (!this.inExternalUpdate) this.props.actions.resetSelection();
    }

    if (e.selected) {
      this.draw.select({ object: e.selected[0] });

      if (!this.inExternalUpdate) {
        this.props.actions.resetSelection();
        this.props.actions.addSelection({ subsampleid: e.target.subsampleid });
      }
    }
  };

  selectionCleared = e => {
    // if (this.props.currentAction) return false

    if (e.deselected) {
      this.draw.select({ object: e.deselected[0], deselect: true });
      if (!this.inExternalUpdate) this.props.actions.resetSelection();
    }
  };

  updateSelectedObject() {
    this.inExternalUpdate = true;
    // TODO: What does this cost?
    this.plotObjects();
    const selectedObject =
      this.props.selectedObjects && this.props.selectedObjects.slice(-1)[0];
    each(['rois', 'pois', 'lois'], ty => {
      each(this.objs[ty], obj => {
        if (obj.subsampleid === selectedObject) {
          this.fabric.setActiveObject(obj);
          if (this.props.centreOnClick) this.zoomToObject(obj);
        }
      });
    });
    this.inExternalUpdate = false;
    if (this.destroyed) return;
    this.fabric.renderAll();
  }

  zoomToObject(obj) {
    let zoom = (this.state.width / obj.getScaledWidth()) * 0.25;
    if (zoom > this.props.maxZoom) zoom = this.props.maxZoom;
    this.setZoom(zoom);
    this.setViewport({
      x:
        -(obj.left + obj.getScaledWidth() / 2) * this.fabric.getZoom() +
        this.state.width / 2,
      y:
        -(obj.top + obj.getScaledHeight() / 2) * this.fabric.getZoom() +
        this.state.height / 2
    });
    this.draw.drawScaleBar({ height: this.state.height });
  }

  roiMouseDown = ({ e, target, pointer }) => {
    if (!this.props.cursor) return;
    const xmap = this.getTopMap(e, pointer);
    if (xmap) {
      const { x, y } = this.getMapPoint(e, target, xmap);
      this.props.actions.setMapPointer({
        // TODO: Account for snaked scans
        point: y * xmap.w + x,
        mapid: xmap.mapid,
        scanid: xmap.datacollectionnumber
      });
    }
  };

  roiMouseMove = ({ e, target, pointer }) => {
    if (!this.props.cursor) return;
    const xmap = this.getTopMap(e, pointer);
    if (xmap) {
      const { x, y } = this.getMapPoint(e, target, xmap);
      this.props.actions.setMapHover({
        x,
        y,
        point: y * xmap.w + x,
        mapid: xmap.mapid,
        px: pointer.x,
        py: pointer.y
      });
    }
  };

  roiMouseOut = () => {
    if (!this.props.cursor) return;
    this.props.actions.setMapHover();
  };

  sourceMouseMove = pointer => {
    if (!this.props.cursor) return;
    each(this.objs.sources, source => {
      const width = source.width * source.scaleX;
      const height = source.height * source.scaleY;
      if (
        pointer.x >= source.left - width / 2 &&
        pointer.x <= source.left + width / 2 &&
        pointer.y >= source.top - height / 2 &&
        pointer.y <= source.top + height / 2
      ) {
        const round = 10;
        const deltaXCentre =
          Math.round(((pointer.x - source.left) / source.scaleX) * round) /
          round;
        const deltaYCentre =
          Math.round(((pointer.y - source.top) / source.scaleY) * round) /
          round;

        const deltaX =
          Math.round(
            ((pointer.x - (source.left - width / 2)) / source.scaleX) * round
          ) / round;
        const deltaY =
          Math.round(
            ((pointer.y - (source.top - height / 2)) / source.scaleY) * round
          ) / round;

        this.props.actions.setSourceHover({
          x: deltaX,
          y: deltaY,
          xCentre: deltaXCentre,
          yCentre: deltaYCentre
        });
      }
    });
  };

  getMapPoint(e, target, xmap) {
    const lp = target.getLocalPointer(e);
    return {
      x: Math.floor((xmap.w * lp.x) / target.width),
      y: Math.floor((xmap.h * lp.y) / target.height)
    };
  }

  getTopMap(e, pointer) {
    const targets = this.fabric.getObjects().filter(obj => {
      return (
        this.fabric.containsPoint(e, obj, pointer) &&
        this.objs.maps.indexOf(obj) > -1 &&
        !this.fabric.isTargetTransparent(obj, pointer.x, pointer.y)
      );
    });

    const mapids = targets.map(m => m.mapid);
    return mapids.length ? this.props.maps[mapids[mapids.length - 1]] : null;
  }

  plotObjects() {
    // console.log('plotObjects')
    if (!this.props.currentSample) {
      console.log('waiting for current sample');
      return;
    }

    each(this.props.objects, (obj, id) => {
      let fobj;
      if (obj.type === 'roi') {
        const r = find(this.objs.rois, { subsampleid: obj.subsampleid });
        if (r) {
          this.draw.updateROI(r, obj, this.props.currentSample);
          this.draw.scaleStrokeWidth(r);
          return;
        }

        fobj = this.draw.drawROI(obj, this.props.currentSample, id);
        fobj.on('mousedown', this.roiMouseDown);
        fobj.on('mousemove', this.roiMouseMove);
        fobj.on('mouseout', this.roiMouseOut);
        this.fabric.add(fobj);
      } else if (obj.type === 'loi') {
        const r = find(this.objs.lois, { subsampleid: obj.subsampleid });
        if (r) {
          this.draw.updateLOI(
            r,
            obj,
            this.props.currentSample,
            this.props.currentAction == 'roi' ||
              this.props.currentAction == 'loi'
          );
          this.draw.scaleStrokeWidth(r);
          return;
        }

        fobj = this.draw.drawLOI(obj, this.props.currentSample, id);
        this.fabric.add(fobj);
      } else {
        const r = find(this.objs.pois, { subsampleid: obj.subsampleid });
        if (r) {
          this.draw.updatePOI(r, obj, this.props.currentSample);
          this.draw.scaleStrokeWidth(r);
          return;
        }

        fobj = this.draw.drawPOI(obj, this.props.currentSample, id);
        this.fabric.add(fobj);
      }
      fobj.object = obj;
      fobj.subsampleid = obj.subsampleid;

      this.draw.scaleStrokeWidth(fobj);
      this.updateSelectedObject();
    });

    each(['pois', 'rois', 'lois'], ty => {
      const toRem = [];
      each(this.objs[ty], ob => {
        if (!(ob.subsampleid in this.props.objects)) {
          console.log('remove', ob);
          toRem.push(ob);
        }
      });

      each(toRem, rem => {
        this.fabric.remove(rem);
        remove(this.objs[ty], rem);
      });
    });

    this.zindex();
    this.plotMaps();
  }

  plotReferences() {
    each(this.props.sampleActionPositions, pos => {
      const r = find(this.objs.refs, {
        sampleactionpositionid: pos.sampleactionpositionid
      });
      if (r) {
        this.draw.updateReference(r, pos);
        return;
      }

      const fobj = this.draw.drawReference(pos);
      this.fabric.add(fobj);

      fobj.sampleactionpositionid = pos.sampleactionpositionid;
    });
    this.zindex();
  }

  loadMap(number) {
    const tmpMap =
      number < this.mapsToLoad.length ? this.mapsToLoad[number] : null;
    if (tmpMap) {
      const [map, obj] = tmpMap;
      const prom = this.draw.drawMap(map, obj, this.props.currentSample);
      if (prom) {
        prom
          .then(img => {
            img.mapid = map.mapid;
            img.subsampleid = map.subsampleid;
            this.fabric.add(img);
            this.mapLoadStatus(number);
            this.loadMap(number + 1);
          })
          .catch(err => {
            console.log('could not load map', map.mapid, err);
            this.loadMap(number + 1);
          });
      }
    }
  }

  clearMapStatus(timeout) {
    this.props.actions.setMessage({
      type: 'mapProgress',
      message: {
        text: '',
        timeout: timeout || 2000
      }
    });
  }

  mapLoadStatus(number) {
    if (this.mapsToLoad.length === 0) {
      this.clearMapStatus(100);
      return;
    }

    if (number === this.mapsToLoad.length - 1) {
      this.props.actions.setMessage({
        type: 'mapProgress',
        message: {
          text: 'Maps loaded'
        }
      });

      this.clearMapStatus();

      this.zindex();
      this.plotComposites();
    } else {
      this.props.actions.setMessage({
        type: 'mapProgress',
        message: {
          text: `Loading maps: ${number}/${this.mapsToLoad.length - 1}`
        }
      });
    }
  }

  plotMaps() {
    this.remObjs('maps', 'mapid', 'maps');
    if (!values(this.props.objects).length) {
      console.log('waiting for objects');
      return;
    }

    this.mapsToLoad = [];
    const subSampleMap = {};
    // console.log('plotMaps')
    eachRight(this.props.maps, map => {
      const obj = find(this.objs.rois, { subsampleid: map.subsampleid });

      if (!obj) {
        console.log('cant find object for map');
        return;
      }

      // console.log('plotMaps', obj, map.subsampleid, this.props.objects)
      const r = find(this.objs.maps, { mapid: map.mapid });

      if (map.opacity === 0) {
        if (r) {
          remove(this.objs.maps, r);
          this.fabric.remove(r);
          if (subSampleMap[r.subsampleid] === r.mapid) {
            delete subSampleMap[r.subsampleid];
          }
        }

        return;
      }

      const subForMap = subSampleMap[map.subsampleid];
      if (subForMap && subForMap !== map.mapid) {
        if (r) {
          remove(this.objs.maps, r);
          this.fabric.remove(r);
        }

        return;
      }

      subSampleMap[map.subsampleid] = map.mapid;

      if (r) {
        this.draw.updateMap(r, map, obj, this.props.currentSample);
        return;
      }

      this.mapsToLoad.push([map, obj]);
    });

    if (this.mapsToLoad.length === 0) {
      this.zindex();
      this.plotComposites();
    } else {
      this.loadMap(0);
    }
  }

  plotComposites() {
    this.remObjs('composite', 'compositeid', 'composites');
    if (!values(this.props.maps).length) {
      console.log('waiting for maps');
      return;
    }

    const ps = [];
    each(this.props.composites, map => {
      const obj = find(this.objs.rois, { subsampleid: map.subsampleid });

      if (!obj) {
        console.log('cant find object for composite');
        return;
      }

      const r = find(this.objs.composite, { compositeid: map.compositeid });
      if (r) {
        this.draw.updateComposite(r, map, obj, this.props.maps);
        return;
      }

      const prom = this.draw.drawComposite(map, obj, this.props.maps);
      if (prom) {
        ps.push(
          prom
            .then(img => {
              img.compositeid = map.compositeid;
              this.fabric.add(img);
            })
            .catch(err => {
              console.log('could not load composite map', map.compositeid, err);
            })
        );
      }
    });

    return Promise.all(ps).then(() => this.zindex());
  }

  plotSources({ updatedUrl } = {}) {
    const ps = [];
    // console.log('plotSources')
    each(this.props.sources, src => {
      const r = find(this.objs.sources, { sourceid: src.sourceid });
      if (r) {
        if ((src.url.startsWith('ws') && src.hide) || updatedUrl) {
          r.dispose();
          delete this.obj;
          this.objs.sources.splice(this.objs.sources.indexOf(r), 1);
          if (!updatedUrl) return null;
        }

        if (!updatedUrl) {
          this.draw.updateSource(r, src, this.props.sourceUrl);
          return null;
        }
      }

      if (src.url.startsWith('ws') && src.hide) return null;

      const source = this.draw.drawSource(src, this.props.sourceUrl);
      if (source) {
        ps.push(
          source.then(img => {
            // console.log('set sourceid');
            img.sourceid = src.sourceid;
            img.origin = src.origin;
            this.fabric.add(img);
          })
        );
      }
    });

    return Promise.all(ps).then(() => {
      if (this.props.clampOrigin) {
        this.fabric.setZoom(this.getMinZoom());
        this.clampViewPort();
        this.draw.drawScaleBar({ height: this.state.height });
      }

      this.plotMarkings();
      this.plotObjects();
      this.plotReferences();
      this.plotImages();
      this.plotPolylines();
    });
  }

  plotPolylines() {
    if (!this.props.config.options.polylines) return;

    // console.log("plotPolylines")
    each(this.props.sources, src => {
      each(src.polylines, (poly, key) => {
        const r = find(this.objs.polyline, { key });
        if (r) {
          this.draw.updatePolyline(r, poly);
          return;
        }

        const fobj = this.draw.drawPolyline(poly);
        fobj.key = key;
        this.fabric.add(fobj);
        this.draw.updatePolyline(fobj, poly);
        // console.log('polylines', fobj);
      });

      // Draw fov for when origin is hidden
      const beam = src.markings.beam;
      const bWidth = 1024 * 4 * Math.abs(src.pixelsize[0]);
      const bHeight = 768 * 4 * Math.abs(src.pixelsize[1]);
      const poly = [
        [beam.position[0], beam.position[1]],
        [beam.position[0] + bWidth, beam.position[1]],
        [beam.position[0] + bWidth, beam.position[1] - bHeight],
        [beam.position[0], beam.position[1] - bHeight],
        [beam.position[0], beam.position[1]]
      ];
      const r = find(this.objs.polyline, { key: 'fov' });
      if (r) {
        this.draw.updatePolyline(r, poly);
        return;
      }

      const fobj = this.draw.drawPolyline(poly, true);
      fobj.key = 'fov';
      this.fabric.add(fobj);
      this.draw.updatePolyline(fobj, poly);
    });

    this.zindex();
  }

  loadImage(number) {
    const images = values(this.props.images);
    const im = number < images.length ? images[number] : null;

    if (im) {
      const currentSize =
        im.width * Math.abs(im.scalex) * this.fabric.getZoom();
      const imageScale =
        2 ** Math.round(Math.log2(Math.min(currentSize / im.width, 1)));
      // Make sure scale is an integer number of pixels
      const regularisedScale = Math.floor(im.width * imageScale) / im.width;

      const r = find(this.objs.images, { sampleimageid: im.sampleimageid });
      if (r) {
        if (!r.isOnScreen()) {
          console.log('removing', r.sampleimageid);
          this.fabric.remove(r);
          remove(this.objs.images, r);
          this.loadImage(number + 1);
          return;
        }

        if (r.loadedScale !== regularisedScale && r.isOnScreen()) {
          this.fabric.remove(r);
          remove(this.objs.images, r);
        } else {
          this.draw.updateImage(r, im, this.props.currentSample);
          this.loadImage(number + 1);
          return;
        }
      }

      const img = this.draw.drawImage(
        im,
        this.props.currentSample,
        regularisedScale
      );
      if (img) {
        img
          .then(ximg => {
            ximg.image = im;
            ximg.sampleimageid = im.sampleimageid;
            this.fabric.add(ximg);

            this.imageLoadStatus(number);
            this.loadImage(number + 1);
          })
          .catch(err => {
            console.log('Error loading image', err);
          });
      } else {
        this.loadImage(number + 1);
      }
    } else {
      this.remObjs('images', 'sampleimageid', 'images');
      this.zindex();
    }
  }

  clearStatus(timeout) {
    this.props.actions.setMessage({
      type: 'imageProgress',
      message: {
        text: '',
        timeout: timeout || 2000
      }
    });
  }

  imageLoadStatus(number) {
    const images = values(this.props.images);

    if (images.length === 0) {
      this.clearStatus(100);
      return;
    }

    if (number === images.length - 1) {
      this.props.actions.setMessage({
        type: 'imageProgress',
        message: {
          text: 'Sample images loaded'
        }
      });

      this.clearStatus();

      this.remObjs('images', 'sampleimageid', 'images');
      this.zindex();
    } else {
      this.props.actions.setMessage({
        type: 'imageProgress',
        message: {
          text: `Loading sample images: ${number}/${images.length - 1}`
        }
      });
    }
  }

  plotImages() {
    this.loadImage(0);
  }

  remObjs(type, idfield, prop) {
    const rem = [];
    each(this.objs[type], im => {
      const r = find(this.props[prop], { [idfield]: im[idfield] });
      if (!r) rem.push(im);
    });
    // console.log('trying to remove', rem, type, idfield)
    each(rem, r => {
      remove(this.objs[type], r);
      this.fabric.remove(r);
    });
  }

  plotMarkings() {
    // console.log('plotMarkings')
    each(this.props.sources, src => {
      each(src.markings, (obj, key) => {
        const r = find(this.objs.markings, { key });
        if (r) {
          // console.log('object exists for ', obj)
          this.draw.updateMarking(r, obj);
          return;
        }

        const fobj = this.draw.drawMarking(obj);
        this.fabric.add(fobj);

        fobj.key = key;
        fobj.origin = obj.origin;
      });
    });
    // this.plotObjects();
    // this.plotImages();
    this.zindex();
  }

  zindex() {
    each(
      values(this.zindexes).sort((a, b) => a - b),
      zindex => {
        const key = invert(this.zindexes)[zindex];
        each(this.objs[key], o => {
          o.bringToFront();
        });
      }
    );

    each(['newRect', 'newLine', 'newLineText'], obj => {
      if (this[obj] && this[obj].canvas) this[obj].bringToFront();
    });

    this.draw.drawScaleBar({ height: this.state.height });
  }

  componentDidMount() {
    super.componentDidMount();
    this.setCurrentSampleMessage();
    if (this.props.currentSampleId) this.selectSample();
  }

  componentDidUpdate(prevProps) {
    super.componentDidUpdate(prevProps);
    // console.log('componentDidUpdate', this.ready)
    if (prevProps.currentSampleId !== this.props.currentSampleId) {
      this.selectSample();
      this.setCurrentSampleMessage();
    }

    if (prevProps.objects !== this.props.objects) this.plotObjects();
    if (prevProps.maps !== this.props.maps) this.plotMaps();
    if (prevProps.composites !== this.props.composites) this.plotComposites();
    if (prevProps.images !== this.props.images) this.plotImages();
    if (prevProps.sources !== this.props.sources) this.plotSources();
    if (prevProps.sourceUrl !== this.props.sourceUrl) {
      this.plotSources({ updatedUrl: true });
    }
    if (prevProps.sampleActionPositions !== this.props.sampleActionPositions) {
      this.plotReferences();
    }

    if (prevProps.selectedObjects !== this.props.selectedObjects) {
      this.updateSelectedObject(prevProps);
    }

    if (
      prevProps.clampOrigin !== this.props.clampOrigin &&
      this.props.clampOrigin
    ) {
      this.setZoom(this.getMinZoom());
    }
    if (
      prevProps.fillOrigin !== this.props.fillOrigin &&
      (this.props.fillOrigin || this.props.clampOrigin)
    ) {
      this.firstFillOrigin = true;
      this.setZoom(this.getMinZoom());
    }

    if (this.props.mirrorid) {
      if (this.props.zoom !== prevProps.zoom) this.dosetZoom(this.props.zoom);
      if (this.props.viewport !== prevProps.viewport) {
        this.doSetViewport(this.props.viewport);
      }
    }

    if (prevProps.saveCanvas !== this.props.saveCanvas) {
      if (this.props.saveCanvas) this.saveCanvas();
    }

    if (prevProps.sourceHover !== this.props.sourceHover) {
      this.draw.drawCursor({
        text:
          this.props.sourceHover.x !== undefined
            ? `x: ${this.props.sourceHover.x} y: ${this.props.sourceHover.y} | cx: ${this.props.sourceHover.xCentre} cy: ${this.props.sourceHover.yCentre}`
            : ''
      });
    }

    // Replot objects on current action change
    if (prevProps.currentAction !== this.props.currentAction) {
      this.plotObjects();
    }
  }

  setCurrentSampleMessage() {
    this.props.actions.setMessage({
      type: 'currentSample',
      message: {
        text: this.props.currentSampleId
          ? ''
          : 'No sample selected. Please create one from the samples sidebar',
        timeout: 0
      }
    });
  }

  onMouseWheel = opt => {
    const zf = 400 / this.fabric.getZoom();
    const delta = opt.e.deltaMode === 1 ? opt.e.deltaY * 18 : opt.e.deltaY;
    const clampedZoom = this.clampZoom(
      this.fabric.getZoom() + (-1 * delta) / zf
    );
    this.setZoom(clampedZoom, opt.e.offsetX, opt.e.offsetY);

    opt.e.preventDefault();
    opt.e.stopPropagation();
  };

  dosetZoom(zoom, x = null, y = null) {
    if (x && y) {
      this.fabric.zoomToPoint({ x, y }, zoom);
      this.props.actions.setViewport({
        x: this.fabric.viewportTransform[4],
        y: this.fabric.viewportTransform[5]
      });
    } else this.fabric.setZoom(zoom);
    this.clampViewPort();

    each(this.objs, (objs, type) => {
      each(objs, o => {
        o.setCoords();
        if (type === 'polyline') this.draw.scaleStrokeWidth(o);
      });
    });

    if (this.newRect) this.draw.scaleStrokeWidth(this.newRect);

    this.plotMarkings();
    this.plotObjects();
    this.plotReferences();
    this.debouncedPlotImages();
    this.plotPolylines();
    this.draw.drawScaleBar({ height: this.state.height });
  }

  setZoom(zoom, x = null, y = null) {
    this.dosetZoom(zoom, x, y);
    this.props.actions.setZoom(zoom);
  }

  doSetViewport(pos) {
    this.fabric.viewportTransform[4] = pos.x;
    this.fabric.viewportTransform[5] = pos.y;
  }

  setViewport(pos) {
    this.doSetViewport(pos);
    this.props.actions.setViewport(pos);
  }

  clampViewPort() {
    let orig = find(this.objs.sources, { origin: true });
    // origin is hidden, use the fov polyline
    if (!orig) {
      orig = find(this.objs.polyline, { key: 'fov' });
    }
    // console.log('clampViewPort', orig);
    if (orig && this.props.clampOrigin) {
      const scale = this.fabric.getZoom();
      const imageWidth = orig.getScaledWidth() * scale;
      const imageHeight = orig.getScaledHeight() * scale;
      const imageLeft = orig.left * scale;
      const imageTop = orig.top * scale;

      const maxX = -(
        imageWidth +
        imageLeft -
        imageWidth / 2 -
        this.state.width
      );
      const maxY = -(
        imageHeight +
        imageTop -
        imageHeight / 2 -
        this.state.height
      );
      const minX = -imageLeft + imageWidth / 2;
      const minY = -imageTop + imageHeight / 2;

      if (this.props.fillOrigin && this.firstFillOrigin) {
        const beam = find(this.objs.markings, { key: 'beam' });
        if (beam) {
          const middleY = (minY - maxY) / 2;
          const beamToImageY = beam.top * scale - imageTop;
          this.fabric.viewportTransform[5] = maxY + middleY - beamToImageY;

          const middleX = (minX - maxX) / 2;
          const beamToImageX = beam.left * scale - imageLeft;
          this.fabric.viewportTransform[4] = maxX + middleX - beamToImageX;
          this.firstFillOrigin = false;
        }
      }

      if (this.fabric.viewportTransform[4] < maxX) {
        this.fabric.viewportTransform[4] = maxX;
      }
      if (this.fabric.viewportTransform[4] > minX) {
        this.fabric.viewportTransform[4] = minX;
      }

      if (this.fabric.viewportTransform[5] < maxY) {
        this.fabric.viewportTransform[5] = maxY;
      }
      if (this.fabric.viewportTransform[5] > minY) {
        this.fabric.viewportTransform[5] = minY;
      }
    }
  }

  clampZoom(zoom) {
    return Math.min(Math.max(zoom, this.getMinZoom()), this.props.maxZoom);
  }

  getMinZoom() {
    let orig = find(this.objs.sources, { origin: true });
    // origin is hidden, use the fov polyline
    if (!orig) {
      orig = find(this.objs.polyline, { key: 'fov' });
    }
    if (orig && this.props.clampOrigin) {
      const w = orig.getScaledWidth();
      const h = orig.getScaledHeight();

      const wr = this.state.width / w;
      const hr = this.state.height / h;

      if (this.props.fillOrigin) {
        return wr < hr ? this.state.height / h : this.state.width / w;
      }
      return wr < hr ? this.state.width / w : this.state.height / h;
    }

    return this.props.minZoom;
  }

  getNearest(x, y, zoom) {
    let nearest = null;
    if (this.props.drawSnap) {
      each(this.objs.rois, roi => {
        const corners = {
          tr: { left: roi.left + roi.width, top: roi.top },
          bl: { left: roi.left, top: roi.top + roi.height }
        };

        each(corners, c => {
          const dist = Math.sqrt((x - c.left) ** 2 + (y - c.top) ** 2);
          if (dist < 30 / zoom) {
            nearest = { left: c.left, top: c.top };
            // console.log('nearest tr', roi.subsampleid, dist);
          }
        });
      });
    }

    return nearest;
  }

  findIntersectingImages(object) {
    const objects = filter(
      filter(this.objs.images, img => img.intersectsWithObject(object)),
      object => !this.props.images[object.sampleimageid].hide
    );
    return map(objects, object => this.props.images[object.sampleimageid]);
  }

  onMouseDown = opt => {
    this.moouseIsDown = true;

    const zoom = this.fabric.getZoom();
    const pointer = this.fabric.getPointer(opt.e);
    this.origX = pointer.x;
    this.origY = pointer.y;

    if (this.props.currentAction === 'move') {
      if (this.props.runningScan) return;
      this.props.actions
        .moveToCoords({
          x: parseInt(pointer.x, 10),
          y: parseInt(pointer.y, 10)
        })
        .catch(err => {
          this.props.actions.addToast({
            type: 'error',
            title: 'Could Not Move',
            text:
              (err.response && err.response.data && err.response.data.error) ||
              err.message
          });
        });
    }

    if (
      this.props.currentAction === 'roi' ||
      this.props.currentAction === 'mosaic'
    ) {
      const nearest = this.getNearest(this.origX, this.origY, zoom);

      this.newRect = new fabric.Rect({
        left: nearest ? nearest.left : this.origX,
        top: nearest ? nearest.top : this.origY,
        width: 0,
        height: 0,
        stroke: 'green',
        strokeDashArray: [5 / zoom, 5 / zoom],
        fill: null
      });
      this.newRect.selectable = true;
      this.fabric.add(this.newRect);
    }

    if (this.props.currentAction === 'loi') {
      this.newLine = new fabric.Line(
        [this.origX, this.origY, pointer.x, pointer.y],
        {
          stroke: 'green',
          strokeDashArray: [5 / zoom, 5 / zoom]
        }
      );
      this.newLine.selectable = true;
      this.fabric.add(this.newLine);
    }

    if (this.props.currentAction === 'measure') {
      this.newLineText = new fabric.Text('0m', {
        left: this.origX,
        top: this.origY,
        fontSize: 14,
        fontFamily: 'Poppins',
        fill: 'purple'
      });
      this.newLine = new fabric.Line(
        [this.origX, this.origY, this.origX, this.origY],
        {
          stroke: 'purple'
          // strokeDashArray: [5/zoom, 5/zoom],
        }
      );
      this.fabric.add(this.newLine);
      this.fabric.add(this.newLineText);
    }

    if (this.props.currentAction === 'pan') {
      console.log('scrolling mouseDown');
      this.lastPosX = opt.e.clientX;
      this.lastPosY = opt.e.clientY;
    }

    if (this.props.currentAction === 'beam') {
      const orig = find(this.objs.sources, { origin: true });
      const mark = find(this.objs.markings, { origin: true });

      const offsetX = Math.round((pointer.x - orig.left) / orig.scaleX);
      const offsetY = Math.round((pointer.y - orig.top) / orig.scaleY);

      const osrc = find(this.props.sources, { sourceid: orig.sourceid });
      this.props.actions.updateMarking({
        markingid: mark.markingid,
        x: parseInt(offsetX, 10),
        y: parseInt(offsetY, 10),
        scalelabel: osrc.scalelabel
      });
    }

    if (this.props.currentAction === 'calib') {
      const oobj = find(this.objs.sources, { origin: true });
      const orig = find(this.props.sources, { origin: true });
      const offsetX = (pointer.x - oobj.left) / oobj.scaleX;
      const offsetY = (pointer.y - oobj.top) / oobj.scaleY;

      console.log(pointer, offsetX, offsetY);
      // return
      if (this.calibStep === 0) {
        this.calib = {
          offsetx: orig.offsetx,
          offsety: orig.offsety,
          pointerx: offsetX,
          pointery: offsetY
        };

        this.calibStep += 1;
      } else {
        // const end = {
        //   offsetx: orig.offsetx,
        //   offsety: orig.offsety,
        //   pointerx: offsetX,
        //   pointery: offsetY
        // };

        // each(['x', 'y'], a => {
        //   const doff = end[`offset${a}`] - this.calib[`offset${a}`];
        //   const dpix = end[`pointer${a}`] - this.calib[`pointer${a}`];
        //   // console.log(a, doff, dpix, doff / dpix);
        // });

        this.calibStep = 0;
      }
    }
  };

  onMouseMove = opt => {
    const pointer = this.fabric.getPointer(opt.e);
    const zoom = this.fabric.getZoom() < 1 ? this.fabric.getZoom() : 1;
    this.sourceMouseMove(pointer);

    if (!this.moouseIsDown) return;

    if (this.props.currentAction === 'roi') {
      if (this.origX > pointer.x) {
        this.newRect.set({ left: pointer.x });
      }
      if (this.origY > pointer.y) {
        this.newRect.set({ top: pointer.y });
      }

      const w = Math.abs(this.origX - pointer.x);
      const h = Math.abs(this.origY - pointer.y);

      let qw = w;
      let qh = h;
      if (this.props.drawQuantize) {
        const dw = w % this.props.drawQuantize[0];
        const dh = h % this.props.drawQuantize[1];

        qw =
          w -
          dw +
          (dw > this.props.drawQuantize[0] / 2
            ? this.props.drawQuantize[0]
            : 0);
        qh =
          h -
          dh +
          (dh > this.props.drawQuantize[1] / 2
            ? this.props.drawQuantize[1]
            : 0);
      }

      this.newRect.set({
        width: qw,
        height: qh
      });
      this.draw.scaleStrokeWidth(this.newRect);
    }

    if (this.props.currentAction === 'loi') {
      let qx2 = pointer.x;
      let qy2 = pointer.y;

      if (this.props.clampLOI) {
        if (qx2 - this.newLine.x1 < 50 / zoom) {
          qx2 = this.newLine.x1;
        } else {
          qy2 = this.newLine.y1;
        }
      }

      if (this.props.drawQuantize) {
        const x2 = qx2 - this.origX;
        const y2 = qy2 - this.origY;
        const len = Math.sqrt(x2 * x2 + y2 * y2);
        const angle = Math.atan2(y2, x2);
        const dlen = len % this.props.drawQuantize[0];
        const qlen =
          len -
          dlen +
          (dlen > this.props.drawQuantize[0] / 2
            ? this.props.drawQuantize[0]
            : 0);
        qx2 = Math.cos(angle) * qlen + this.origX;
        qy2 = Math.sin(angle) * qlen + this.origY;
      }

      this.newLine.set({
        x2: qx2,
        y2: qy2
      });
      this.draw.scaleStrokeWidth(this.newLine);
    }

    if (this.props.currentAction === 'mosaic') {
      if (this.origX > pointer.x) {
        this.newRect.set({ left: Math.abs(pointer.x) });
      }
      if (this.origY > pointer.y) {
        this.newRect.set({ top: Math.abs(pointer.y) });
      }

      const w = Math.abs(this.origX - pointer.x);
      const h = Math.abs(this.origY - pointer.y);

      const orig = find(this.objs.sources, { origin: true });

      const dw = w % orig.getScaledWidth();
      const dh = h % orig.getScaledHeight();

      const qw =
        w - dw + (dw > orig.getScaledWidth() / 2 ? orig.getScaledWidth() : 0);
      const qh =
        h - dh + (dh > orig.getScaledHeight() / 2 ? orig.getScaledHeight() : 0);

      this.newRect.set({
        width: qw,
        height: qh
      });
      this.draw.scaleStrokeWidth(this.newRect);
    }

    if (this.props.currentAction === 'measure') {
      this.newLine.set({
        x2: pointer.x,
        y2: pointer.y
      });

      const len = Math.sqrt(
        (pointer.x - this.newLine.x1) ** 2 + (pointer.y - this.newLine.y1) ** 2
      );
      const size = Formatting.formatEng(len * config.pixelSize);

      this.newLineText.set({
        left: this.origX + (pointer.x - this.origX) / 2 + 20,
        top: this.origY + (pointer.y - this.origY) / 2 + 20,
        fontFamily: 'Poppins',
        text: `${size.scalar.toFixed(1)}${size.prefix}m`
      });
      this.newLineText.scaleX = 1 / zoom;
      this.newLineText.scaleY = 1 / zoom;
      this.draw.scaleStrokeWidth(this.newLine);
    }

    if (this.props.currentAction === 'pan') {
      this.setViewport({
        x: this.fabric.viewportTransform[4] + opt.e.clientX - this.lastPosX,
        y: this.fabric.viewportTransform[5] + opt.e.clientY - this.lastPosY
      });

      this.clampViewPort();

      this.lastPosX = opt.e.clientX;
      this.lastPosY = opt.e.clientY;

      this.draw.drawScaleBar({ height: this.state.height });
      this.debouncedPlotImages();
    }

    this.fabric.renderAll();
  };

  onMouseUp = opt => {
    this.moouseIsDown = false;
    const orig = find(this.props.sources, { origin: true });

    if (this.props.currentAction === 'roi') {
      let additional = orig.additional;
      // If the origin (video) is hidden we can retreive the additional positions
      // from a sample image that intersects the current position
      if (orig.hide) {
        const images = this.findIntersectingImages(this.newRect);
        if (images.length) {
          additional = images[images.length - 1].positions;
          if (images.length > 1) {
            this.props.actions.addToast({
              type: 'warning',
              title: 'Additional Positions',
              text: `Multiple images are visible, the most recent has been used.\n${JSON.stringify(
                additional,
                null,
                2
              )}`
            });
          }
        } else {
          this.props.actions.addToast({
            type: 'warning',
            title: 'Additional Positions',
            text: `The origin is hidden but no images were found. Additional positions have been taken from the origin.\n${JSON.stringify(
              additional,
              null,
              2
            )}`
          });
        }
      }
      if (this.newRect.width && this.newRect.height) {
        const xp = parseInt(this.newRect.left, 10);
        const yp = parseInt(this.newRect.top, 10);
        this.props.actions.addObject({
          sampleid: this.props.currentSampleId,
          x: xp,
          y: yp,
          x2: xp + parseInt(this.newRect.width, 10),
          y2: yp + parseInt(this.newRect.height, 10),
          type: 'roi',
          positions: additional
        });
      } else if (this.props.drawQuantize) {
        const xp = parseInt(
          this.newRect.left - this.props.drawQuantize[0] / 2,
          10
        );
        const yp = parseInt(
          this.newRect.top - this.props.drawQuantize[1] / 2,
          10
        );
        this.props.actions.addObject({
          sampleid: this.props.currentSampleId,
          x: xp,
          y: yp,
          x2: xp + parseInt(this.props.drawQuantize[0], 10),
          y2: yp + parseInt(this.props.drawQuantize[1], 10),
          type: 'roi',
          positions: orig.additional
        });
      }
      this.fabric.remove(this.newRect);
      if (!this.props.multiROI) this.props.actions.selectAction('');
    }

    if (this.props.currentAction === 'loi') {
      if (this.newLine.x2 && this.newLine.y2) {
        this.props.actions.addObject({
          sampleid: this.props.currentSampleId,
          x: parseInt(this.newLine.x1, 10),
          y: parseInt(this.newLine.y1, 10),
          x2: parseInt(this.newLine.x2, 10),
          y2: parseInt(this.newLine.y2, 10),
          type: 'loi',
          positions: orig.additional
        });
      }
      this.fabric.remove(this.newLine);
      this.props.actions.selectAction('');
    }

    if (this.props.currentAction === 'mosaic') {
      this.mosaicRef.current.showModal();
    }

    if (this.props.currentAction === 'poi') {
      const pointer = this.fabric.getPointer(opt.e);
      this.props.actions.addObject({
        sampleid: this.props.currentSampleId,
        x: parseInt(pointer.x, 10),
        y: parseInt(pointer.y, 10),
        type: 'poi',
        positions: orig.additional
      });
    }

    if (this.props.currentAction === 'reference') {
      const pointer = this.fabric.getPointer(opt.e);
      this.props.addSampleActionPosition({
        x: parseInt(pointer.x, 10),
        y: parseInt(pointer.y, 10)
      });
      this.props.actions.selectAction('');
    }

    if (this.props.currentAction === 'measure') {
      this.measureTimeout = setTimeout(() => {
        this.fabric.remove(this.newLine);
        this.fabric.remove(this.newLineText);
      }, 3000);
    }
  };

  getMarkOffset() {
    const mark = find(this.objs.markings, { origin: true });
    const src = find(this.objs.sources, { origin: true });

    if (src && mark) {
      return {
        x: mark.left - src.left,
        y: mark.top - src.top
      };
    }

    return { x: 0, y: 0 };
  }

  cancelMosaic = () => {
    this.fabric.remove(this.newRect);
  };

  startMosaic = () => {
    const origObj = find(this.objs.sources, { origin: true });
    if (this.newRect.width && this.newRect.height) {
      this.props.actions.mosaic({
        x1: parseInt(this.newRect.left + origObj.getScaledWidth() / 2, 10),
        y1: parseInt(this.newRect.top + origObj.getScaledHeight() / 2, 10),
        x2: parseInt(
          this.newRect.left + this.newRect.width - origObj.getScaledWidth() / 2,
          10
        ),
        y2: parseInt(
          this.newRect.top +
            this.newRect.height -
            origObj.getScaledHeight() / 2,
          10
        ),
        steps_x: this.newRect.width / origObj.getScaledWidth(),
        steps_y: this.newRect.height / origObj.getScaledHeight(),
        sampleid: this.props.currentSampleId
      });
      this.fabric.remove(this.newRect);
      this.mosaicRef.current.close();
    }
  };
}
