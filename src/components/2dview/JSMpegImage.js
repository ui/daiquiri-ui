/* eslint-disable object-shorthand */
import { fabric } from 'fabric';
import JSMpeg from '@cycjimmy/jsmpeg-player';

const JSMpegImage = fabric.util.createClass(fabric.Image, {
  initialize: function (options) {
    this.callSuper('initialize', options);
    this.first = true;
    this.firstRender = true;
    this.readyCallback = options.readyCallback;

    this.mcanvas = document.createElement('canvas');
    this.mplayer = new JSMpeg.Player(options.url, {
      canvas: this.mcanvas,
      videoBufferSize: 1024 * 1024 * 4,
      audio: false,
      onVideoDecode: this.onVideoDecode.bind(this),
      protocols: [],
      disableGl: true
    });

    if (options.autoplay) this.mplayer.play();
  },

  onVideoDecode: function () {
    if (this.first) {
      console.log(
        'first video decode',
        this.mcanvas.width,
        this.mcanvas.height
      );
      if (this.mcanvas.width && this.mcanvas.height) {
        this.setSrc(this.mcanvas.toDataURL());
        this.width = this.mcanvas.width;
        this.height = this.mcanvas.height;
        this.setCoords();

        this.first = false;
        if (this.readyCallback) this.readyCallback();
      }
    }
  },

  play: function () {
    this.mplayer.play();
  },

  stop: function () {
    this.mplayer.stop();
  },

  _renderFill: function (ctx) {
    if (this.firstRender) {
      this.setCoords();
      this.firstRender = false;
    }

    const elementToDraw = this._element;
    if (!elementToDraw) {
      return;
    }
    const w = this.width;
    const h = this.height;
    const sW = Math.min(
      elementToDraw.naturalWidth || elementToDraw.width,
      w * this._filterScalingX
    );
    const sH = Math.min(
      elementToDraw.naturalHeight || elementToDraw.height,
      h * this._filterScalingY
    );
    const x = -w / 2;
    const y = -h / 2;
    const sX = Math.max(0, this.cropX * this._filterScalingX);
    const sY = Math.max(0, this.cropY * this._filterScalingY);

    if (elementToDraw) ctx.drawImage(this.mcanvas, sX, sY, sW, sH, x, y, w, h);
  },

  dispose: function (options) {
    console.log('JSMpegImage dispose');
    if (this.mplayer) this.mplayer.destroy();
    this.mplayer = null;
    this.callSuper('dispose', options);
  }
});

export default JSMpegImage;
