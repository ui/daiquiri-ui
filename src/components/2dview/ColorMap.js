import { each, range } from 'lodash';
import lerp from 'lerp';
import colormap from 'colormap';

export default class ColorMap {
  maps = {
    r: [
      { index: 0, rgb: [0, 0, 0, 1] },
      { index: 1, rgb: [255, 0, 0, 1] }
    ],
    g: [
      { index: 0, rgb: [0, 0, 0, 1] },
      { index: 1, rgb: [0, 255, 0, 1] }
    ],
    b: [
      { index: 0, rgb: [0, 0, 0, 1] },
      { index: 1, rgb: [0, 0, 255, 1] }
    ],
    ra: [
      { index: 0, rgb: [0, 0, 0, 0] },
      { index: 1, rgb: [255, 0, 0, 1] }
    ],
    ga: [
      { index: 0, rgb: [0, 0, 0, 0] },
      { index: 1, rgb: [0, 255, 0, 1] }
    ],
    ba: [
      { index: 0, rgb: [0, 0, 0, 0] },
      { index: 1, rgb: [0, 0, 255, 1] }
    ]
  };

  constructor(spec = {}) {
    this.shades = spec.shades || 100;

    let map = spec.colourmap;
    if (map in this.maps) map = this.maps[map];

    this.colors = colormap({
      colormap: map || 'viridis',
      nshades: this.shades + 1,
      format: 'rba',
      alpha: 1
    });

    this.min = spec.min;
    this.max = spec.max;
  }

  lerp(val) {
    const step = parseInt(val * this.shades, 10);
    const start = this.colors[step];
    const end = this.colors[step + 1];

    if (val * this.shades === step) return start;
    if (val * this.shades === step + 1) return end;

    const pc = val * this.shades - step;

    // console.log('colormap', val, step, pc, start, end)

    const col = [];
    each(range(3), c => {
      col.push(parseInt(lerp(start[c], end[c], pc / this.shades), 10));
    });
    col.push(lerp(start[3], end[3], pc / this.shades));

    return col;
  }
}
