import { Component } from 'react';

import RoiManager from 'connect/2dview/ROIManager';
import ButtonTriggerModalStore from 'connect/layout/ButtonTriggerModalStore';

export default class RoiManagerButton extends Component {
  render() {
    return (
      <ButtonTriggerModalStore
        id="MCAROIManager"
        title="MCA ROIs"
        button={
          <>
            <i className="fa fa-area-chart me-1" />
            ROIs
          </>
        }
        buttonProps={{
          className: 'float-end'
        }}
      >
        <RoiManager />
      </ButtonTriggerModalStore>
    );
  }
}
