import { each, range, min, max } from 'lodash';

import ColorMap from 'components/2dview/ColorMap';

export default class ImageMap {
  constructor(map) {
    this.setMap(map);
  }

  setMap(map) {
    this.map = map;
    this.min = map.min || min(this.map.data);
    this.max = map.max || max(this.map.data);
  }

  scaleValue(val) {
    if (val >= this.max) return 1;
    if (val <= this.min) return 0;

    if (this.map.log) {
      return Math.log(val - this.min) / Math.log(this.max - this.min);
    }
    return (val - this.min) / (this.max - this.min);
  }

  image() {
    const cmap = new ColorMap({ colourmap: this.map.colourmap });

    const buffer = new Uint8ClampedArray(this.map.w * this.map.h * 4);
    const opacity = this.map.opacity === undefined ? 1 : this.map.opacity;
    each(range(this.map.h), y => {
      each(range(this.map.w), x => {
        const pos = (y * this.map.w + x) * 4;

        const { snaked } = this.map;
        const did =
          snaked && y % 2 === 1
            ? y * this.map.w + (this.map.w - x - 1)
            : y * this.map.w + x;

        if (this.map.data[did] === -1) {
          buffer[pos] = 0;
          buffer[pos + 1] = 0;
          buffer[pos + 2] = 0;
          buffer[pos + 3] = 0;
        } else {
          const [r, g, b, a] = cmap.lerp(this.scaleValue(this.map.data[did]));
          buffer[pos] = r;
          buffer[pos + 1] = g;
          buffer[pos + 2] = b;
          buffer[pos + 3] = a * 255 * opacity;
        }
      });
    });

    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');

    canvas.width = this.map.w;
    canvas.height = this.map.h;

    const idata = ctx.createImageData(this.map.w, this.map.h);
    idata.data.set(buffer);
    ctx.putImageData(idata, 0, 0);

    return canvas.toDataURL();
  }
}
