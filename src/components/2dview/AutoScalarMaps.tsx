import { Button, OverlayTrigger, Popover } from 'react-bootstrap';
import Table from '../table';
import { useCallback } from 'react';

interface IAutoScalarMaps {
  mapSettings: Record<string, any>;
  updateMapSettings: (settings: Record<string, any>) => Promise<any>;
}

export default function AutoScalarMaps(props: IAutoScalarMaps) {
  const { mapSettings, updateMapSettings } = props;
  const removeScalarMap = useCallback(
    (scalarMapName: string) => {
      const newScalarMaps = [...mapSettings.scalar_maps].filter(
        (scalarMap: string) => {
          return scalarMap !== scalarMapName;
        }
      );
      updateMapSettings({ scalar_maps: newScalarMaps });
    },
    [mapSettings.scalar_maps, updateMapSettings]
  );

  return (
    <OverlayTrigger
      trigger="click"
      placement="left"
      rootClose
      overlay={
        <Popover id="auto-scalar-maps">
          <Popover.Header>Scalar Maps</Popover.Header>
          <Popover.Body>
            <Table
              keyField="name"
              data={props.mapSettings?.scalar_maps?.map((name: string) => ({
                name,
              }))}
              noDataIndication="No scalars selected"
              columns={[
                {
                  dataField: 'name',
                  text: 'Scalar',
                  classes: 'text-nowrap',
                },
                {
                  dataField: 'action',
                  text: '',
                  formatter: (cell: string, row: Record<string, string>) => (
                    <Button
                      size="sm"
                      onClick={() => removeScalarMap(row.name)}
                      variant="danger"
                    >
                      <i className="fa fa-times" />
                    </Button>
                  ),
                },
              ]}
            />
          </Popover.Body>
        </Popover>
      }
    >
      <Button title="Scalar Maps" className="float-end me-1">
        <i className="fa fa-map" />
      </Button>
    </OverlayTrigger>
  );
}
