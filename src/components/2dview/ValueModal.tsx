import { useEffect } from 'react';

interface IFetchValue {
  mapid: number;
  x: number;
  y: number;
}

interface IPointer {
  x: number;
  y: number;
  px: number;
  py: number;
  point: number;
  mapid: number;
}

interface IValueModal {
  pointer: IPointer;
  actions: {
    setMapHover: (payload?: number) => void;
    fetchValue: (payload: IFetchValue) => Promise<any>;
  };
  valueError: string;
  valueFetched: boolean;
  value: { x: number; y: number; value: number };
}

export default function ValueModal(props: IValueModal) {
  const {
    actions,
    pointer = {} as IPointer,
    valueFetched,
    valueError,
    value,
  } = props;

  useEffect(() => {
    if (pointer.x !== undefined && pointer.y !== undefined) {
      actions
        .fetchValue({
          mapid: pointer.mapid,
          x: pointer.x,
          y: pointer.y,
        })
        .catch(() => {
          console.log('Couldnt load value');
        });
    } else {
      actions.setMapHover();
    }
  }, [pointer]);

  if (pointer.x === undefined || (!valueFetched && !valueError)) {
    return null;
  }
  return (
    <div
      className="tooltip-inner"
      style={{
        position: 'absolute',
        top: pointer.py + 30,
        left: pointer.px + 30,
        zIndex: 1070,
      }}
    >
      {valueError && (
        <span>
          x: {value.x} y: {value.y} p: {pointer.point + 1} | Could not fetch
          value
        </span>
      )}

      {!valueError && valueFetched && (
        <div className="value-modal">
          x: {value.x} y: {value.y} p: {pointer.point + 1} | v:{' '}
          {value.value.toFixed(2)}
        </div>
      )}
    </div>
  );
}
