import { each, map as mapLodash, sum, find } from 'lodash';
import { fabric } from 'fabric';
import { Formatting } from '@esrf/daiquiri-lib';

import config from 'config/config';
import XHRImage from 'helpers/XHRImage';
import JSMpegImage from 'components/2dview/JSMpegImage';

fabric.Object.prototype.objectCaching = false;

export default class Drawing {
  annotationSize = 14;

  colors = {
    object: 'blue',
    hasdata: 'green',
    queued: 'yellow',
    selected: 'red',
    selectedGroup: 'orange',
    scaleBar: 'grey',
    marking: 'yellow',
    markingInvert: 'cyan',
    polyline: 'purple',
    reference: 'magenta'
  };

  constructor(options) {
    this.pending = {};
    this.pendingComposites = {};
    this.pendingImages = {};
    this.pendingSources = {};
    this.fabric = options.fabric;
    this.width = options.width;
    this.addObject = options.addObject;
    this.x_dir = options.flip_x ? -1 : 1;
    this.lastMarkingStroke = null;
    this.markingDebounce = 1000;
    this.lastMarkingTime = 0;
    this.getSelectedObjects = options.getSelectedObjects;

    this.lastLum = 0;
    this.lumDebounce = 2000;
    this.lastLumTime = 0;
  }

  select(options) {
    this.setStroke({
      object: options.object,
      stroke: options.deselect
        ? this.getStrokeColor(options.object.object)
        : this.colors.selected
    });
  }

  getStrokeColor(object) {
    const active = this.fabric.getActiveObject();
    if (active && active.object.subsampleid === object.subsampleid) {
      return this.colors.selected;
    }

    const objs = this.getSelectedObjects();
    if (objs.indexOf(object.subsampleid) > -1) return this.colors.selectedGroup;

    return object.queued
      ? this.colors.queued
      : object.datacollections
      ? this.colors.hasdata
      : this.colors.object;
  }

  setStroke(options) {
    if (options.object.type === 'group') {
      each(options.object.getObjects(), o => {
        if (o.type === 'text') {
          o.set({ fill: options.stroke, stroke: options.stroke });
        } else o.set({ stroke: options.stroke });
      });
    } else {
      options.object.set({ stroke: options.stroke });
    }
  }

  scaleStrokeWidth(object) {
    let size = Math.round(1 / this.fabric.getZoom());
    if (size < 1) size = 1;
    if (object.type === 'group') {
      each(object.getObjects(), o => {
        if (o.type === 'text') return;
        o.set({ strokeWidth: size });
      });
    } else {
      object.set({ strokeWidth: size });
    }
  }

  drawCursor(options) {
    if (this.cursor) {
      this.updateCursor(options);
      return;
    }

    this.cursor = new fabric.Text('', {
      fill: this.colors.scaleBar,
      left: 10,
      top: 10,
      fontSize: 11,
      fontFamily: 'Poppins',
      backgroundColor: options && options.text ? '#333' : 'transparent'
    });

    this.fabric.add(this.cursor);
    this.updateCursor(options);
  }

  updateCursor(options) {
    const scale = 1 / this.fabric.getZoom();
    this.fabric.bringToFront(this.cursor);

    this.cursor.scaleX = scale;
    this.cursor.scaleY = scale;
    this.cursor.set({
      top: scale * (10 - this.fabric.viewportTransform[5]),
      left: scale * (10 - this.fabric.viewportTransform[4])
    });
    if (options) {
      this.cursor.set({
        text: options.text,
        backgroundColor: options && options.text ? '#333' : 'transparent'
      });
    }
    this.cursor.setCoords();
  }

  drawScaleBar(options) {
    if (this.scalebar) {
      // console.log('drawScaleBar updating (already rendered)')
      this.updateScale(options);
      return;
    }

    const size = 50;
    const x = 0;
    const y = 0;

    const line1 = new fabric.Line([x, y, x, y + size], {
      stroke: this.colors.scaleBar,
      strokeWidth: 2,
      originY: 'bottom'
    });

    const line2 = new fabric.Line([x + size, y + size, x, y + size], {
      stroke: this.colors.scaleBar,
      strokeWidth: 2,
      originY: 'bottom'
    });

    const text = new fabric.Text('50um', {
      fill: this.colors.scaleBar,
      left: x + 10,
      top: y + size - 20,
      fontSize: 11,
      fontFamily: 'Poppins'
    });

    this.scalebar = new fabric.Group([line1, line2, text]);
    this.fabric.add(this.scalebar);
    this.updateScale(options);
  }

  updateScale(options) {
    const scale = 1 / this.fabric.getZoom();
    const ppm = scale * config.pixelSize;

    const eng = Formatting.formatEng(ppm * 50);

    const rem = eng.scalar % parseInt(eng.scalar, 10);
    const mul = (eng.scalar - rem) / eng.scalar;

    this.scalebar.getObjects()[1].scaleX = mul;
    this.scalebar.getObjects()[0].scaleY = mul;

    this.fabric.bringToFront(this.scalebar);
    this.scalebar.getObjects()[2].set({
      text: `${(eng.scalar - rem).toFixed(0)} ${eng.prefix}m`
    });

    this.scalebar.scaleX = scale;
    this.scalebar.scaleY = scale;
    this.scalebar.set({
      top: scale * (options.height - 70 - this.fabric.viewportTransform[5]),
      left: scale * (20 - this.fabric.viewportTransform[4])
    });
    this.scalebar.setCoords();
    this.drawCursor();
  }

  selectionDefault(obj, move = false, resize = false) {
    // console.log('selectionDefault', obj, move)
    obj.selectable = true;
    obj.lockScalingX = !resize;
    obj.lockScalingY = !resize;
    obj.hasRotatingPoint = false;

    obj.setControlsVisibility({
      mt: false,
      mb: false,
      ml: false,
      mr: false,
      bl: false,
      br: !!resize,
      tl: false,
      tr: false,
      mtr: false
    });

    if (!move) {
      obj.lockMovementX = true;
      obj.lockMovementY = true;
    }
  }

  drawROI(obj, smp, index) {
    console.log('drawROI');
    const stroke = this.getStrokeColor(obj);

    const x = obj.x + smp.offsetx;
    const y = obj.y + smp.offsety;

    const w = obj.x2 - obj.x;
    const h = obj.y2 - obj.y;

    const rect = new fabric.Rect({
      left: x,
      top: y,
      width: w,
      height: h,
      stroke,
      fill: null,
      strokeWidth: 1,
      hasBorders: false,
      hoverCursor: 'pointer'
    });

    const text = new fabric.Text(`${index}`, {
      left: x + w,
      top: y + h,
      fontSize: this.annotationSize,
      fontFamily: 'Poppins',
      fill: stroke
    });

    const roi = new fabric.Group([rect, text], {
      hasBorders: false,
      hoverCursor: 'pointer'
    });

    this.selectionDefault(
      roi,
      obj.datacollections === 0,
      obj.datacollections === 0
    );
    this.addObject({ type: 'rois', object: roi });
    this.updateROI(roi, obj, smp);

    return roi;
  }

  updateROI(fobj, obj, smp) {
    let scale = 1 / this.fabric.getZoom();
    if (scale < 1) scale = 1;

    const stroke = this.getStrokeColor(obj);

    const w = obj.x2 - obj.x;
    const h = obj.y2 - obj.y;
    const x = obj.x + smp.offsetx;
    const y = obj.y + smp.offsety;

    const rect = fobj.getObjects()[0];
    rect.set({
      stroke,
      left: -w / 2,
      top: -h / 2,
      width: w,
      height: h
    });

    const text = fobj.getObjects()[1];
    text.set({
      stroke,
      scaleX: scale,
      scaleY: scale,
      left: w / 2,
      top: h / 2
    });

    fobj.set({
      left: x,
      top: y,
      width: w * fobj.scaleX,
      height: h * fobj.scaleY,
      scaleX: 1,
      scaleY: 1
    });

    this.selectionDefault(
      fobj,
      obj.datacollections === 0,
      obj.datacollections === 0
    );
  }

  drawCrossText({ x, y, annotationText, strokeColor }) {
    const size = 21;
    const half = Math.round(size / 2);

    const line1 = new fabric.Line([0, -half, 0, half], {
      stroke: strokeColor,
      strokeWidth: 1,
      hasBorders: false,
      originX: 'center',
      originY: 'center'
    });

    const line2 = new fabric.Line([-half, 0, half, 0], {
      stroke: strokeColor,
      strokeWidth: 1,
      hasBorders: false,
      originX: 'center',
      originY: 'center'
    });

    const text = new fabric.Text(`${annotationText}`, {
      left: half,
      top: half,
      fontSize: this.annotationSize,
      fontFamily: 'Poppins',
      fill: strokeColor
    });

    return new fabric.Group([line1, line2, text], {
      hasBorders: false,
      hoverCursor: 'pointer',
      left: x,
      top: y,
      originX: 'center',
      originY: 'center'
    });
  }

  updateCrossText({ fobj, x, y, strokeColor }) {
    let scale = 1 / this.fabric.getZoom();
    if (scale < 1) scale = 1;

    fobj.set({
      left: x,
      top: y
    });

    const line = fobj.getObjects()[0];
    line.scaleY = scale;
    line.stroke = strokeColor;

    const line2 = fobj.getObjects()[1];
    line2.scaleX = scale;
    line2.stroke = strokeColor;

    fobj.getObjects()[2].set({
      stroke: strokeColor,
      left: line.left + line.width,
      top: line.top + line.height,
      scaleX: scale,
      scaleY: scale
    });
  }

  drawReference(obj, index) {
    const stroke = this.colors.reference;
    const ref = this.drawCrossText({
      x: obj.posx,
      y: obj.posy,
      annotationText: obj.id,
      strokeColor: stroke
    });

    this.selectionDefault(ref, obj.datacollections === 0);
    this.addObject({ type: 'refs', object: ref });
    this.updateReference(ref, obj);

    return ref;
  }

  updateReference(fobj, obj) {
    const stroke = this.colors.reference;
    this.updateCrossText({
      fobj,
      x: obj.posx,
      y: obj.posy,
      strokeColor: stroke
    });
  }

  drawPOI(obj, smp, index) {
    const x = obj.x + smp.offsetx;
    const y = obj.y + smp.offsety;

    const stroke = this.getStrokeColor(obj);
    const poi = this.drawCrossText({
      x,
      y,
      annotationText: index,
      strokeColor: stroke
    });

    this.selectionDefault(poi, obj.datacollections === 0);
    this.addObject({ type: 'pois', object: poi });
    this.updatePOI(poi, obj, smp);

    return poi;
  }

  updatePOI(fobj, obj, smp) {
    const stroke = this.getStrokeColor(obj);
    this.updateCrossText({
      fobj,
      x: obj.x + smp.offsetx,
      y: obj.y + smp.offsety,
      strokeColor: stroke
    });
  }

  drawLOI(obj, smp, index) {
    const x = obj.x + smp.offsetx;
    const y = obj.y + smp.offsety;
    const x2 = obj.x2 + smp.offsetx;
    const y2 = obj.y2 + smp.offsety;

    const stroke = this.getStrokeColor(obj);

    const line = new fabric.Line([x, y, x2, y2], {
      stroke,
      strokeWidth: 1,
      hasBorders: false,
      originX: 'center',
      originY: 'center'
    });

    const angle = (Math.atan2(y2 - y, x2 - x) * 180) / Math.PI;
    const pointer = new fabric.Triangle({
      top: y2,
      left: x2,
      angle: angle + 90,
      width: 10,
      height: 10,
      stroke,
      fill: false,
      originX: 'center',
      originY: 'center'
    });

    const text = new fabric.Text(`${index}`, {
      left: x2,
      top: y2,
      fontSize: this.annotationSize,
      fontFamily: 'Poppins',
      fill: stroke
    });

    const loi = new fabric.Group([line, pointer, text], {
      hasBorders: false,
      hoverCursor: 'pointer',
      originX: 'center',
      originY: 'center'
    });

    this.selectionDefault(loi, obj.datacollections === 0);
    this.addObject({ type: 'lois', object: loi });
    this.updateLOI(loi, obj, smp);

    return loi;
  }

  updateLOI(fobj, obj, smp, fixed) {
    let scale = 1 / this.fabric.getZoom();
    if (scale < 1) scale = 1;

    fobj.set({
      x1: obj.x + smp.offsetx,
      y1: obj.y + smp.offsety
    });

    fobj.lockMovementX = fixed;
    fobj.lockMovementY = fixed;

    const stroke = this.getStrokeColor(obj);

    const line = fobj.getObjects()[0];
    line.scale = scale;
    line.stroke = stroke;
    fobj.getObjects()[1].set({
      stroke,
      width: scale * 10,
      height: scale * 10
    });
    fobj.getObjects()[2].set({
      stroke,
      scaleX: scale,
      scaleY: scale
    });
  }

  drawMarking(mrk) {
    const position = this.toCanvasCoords(mrk.position);

    const half = Math.round(30 / 2);
    const stroke = this.colors.marking;

    const line1 = new fabric.Line([0, -half, 0, half], {
      stroke,
      strokeWidth: 1,
      hasBorders: false,
      originX: 'center',
      originY: 'center'
    });

    const line2 = new fabric.Line([-half, 0, half, 0], {
      stroke,
      strokeWidth: 1,
      hasBorders: false,
      originX: 'center',
      originY: 'center'
    });

    const ellip = new fabric.Ellipse({
      left: 0,
      top: 0,
      originX: 'center',
      originY: 'center',
      strokeWidth: 1,
      stroke: this.colors.marking,
      fill: false,
      rx: mrk.size[0] / 2,
      ry: mrk.size[1] / 2
    });

    const marking = new fabric.Group([line1, line2, ellip], {
      hasBorders: false,
      hoverCursor: 'cursor',
      originX: 'center',
      originY: 'center',
      left: Math.round(position[0]),
      top: Math.round(position[1])
    });

    this.addObject({ type: 'markings', object: marking });
    this.updateMarking(marking, mrk);

    return marking;
  }

  updateMarking(fobj, mrk) {
    const position = this.toCanvasCoords(mrk.position);

    let scale = 1 / this.fabric.getZoom();
    if (scale < 1) scale = 1;

    const y = fobj.getObjects()[0];
    const x = fobj.getObjects()[1];

    y.scaleY = scale;
    x.scaleX = scale;

    const w = x.getScaledWidth();
    const h = y.getScaledHeight();

    const pixels = fabric.util.transformPoint(
      { x: position[0], y: position[1] },
      this.fabric.viewportTransform
    );
    const wh = fabric.util.transformPoint(
      { x: position[0] + w, y: position[1] + h },
      this.fabric.viewportTransform
    );

    if (Number.isNaN(wh.x) || Number.isNaN(wh.y)) return;

    const now = Date.now();
    if (now - this.lastLumTime > this.lumDebounce) {
      const half = (wh.x - pixels.x) / 3;
      const ctx = this.fabric.contextContainer;
      const ratio = window.devicePixelRatio;
      const { data } = ctx.getImageData(
        (pixels.x - half) * ratio,
        (pixels.y - half) * ratio,
        half * 2 * ratio,
        half * 2 * ratio
      );

      const lums = [];
      for (let i = 0; i < data.length; i += 4) {
        // rgb -> lum
        lums.push(
          0.299 * data[i] +
            0.587 * data[i + 1] +
            0.114 ** data[i + 2] * (data[i + 3] / 255.0)
        );
      }

      this.lastLum = lums.length ? sum(lums) / lums.length : 0;
      this.lastLumTime = now;
    }

    const threshold = 150;
    const stroke =
      this.lastLum > threshold
        ? this.colors.markingInvert
        : this.colors.marking;

    if (
      stroke !== this.lastMarkingStroke &&
      now - this.lastMarkingTime > this.markingDebounce
    ) {
      y.set({ stroke });
      x.set({ stroke });
      fobj.getObjects()[2].set({ stroke });

      this.lastMarkingTime = now;
      this.lastMarkingStroke = stroke;
    }

    this.scaleStrokeWidth(fobj);
    fobj.set({
      left: Math.round(position[0]),
      top: Math.round(position[1])
    });
    fobj.setCoords();
  }

  refreshImage(src, img) {
    const now = Date.now();
    const im = new Image();
    im.onload = () => {
      img.setElement(im);
      this.refreshThread = setTimeout(
        this.refreshImage.bind(this, src, img),
        100
      );
    };
    im.src = `${src.url}?${now}`;
  }

  // Coordinates in html5 canvas space are x, -y, and not x, y as you
  // might expect (!)
  toCanvasCoords(coords) {
    return [coords[0], -coords[1]];
  }

  drawSource(src, sourceUrl) {
    if (src.sourceid in this.pendingSources) {
      // console.log('drawSource pending', src.sourceid);
      return null;
    }
    this.pendingSources[src.sourceid] = 1;

    let url = src.url;
    let scale = src.scale;
    if (sourceUrl) {
      const additional_url = find(src.additional_urls, { name: sourceUrl });
      if (additional_url) {
        url = additional_url.url;
        scale = additional_url.scale;
      }
    }

    return new Promise(resolve => {
      const center = this.toCanvasCoords(src.center);
      if (url.startsWith('ws')) {
        const source = new JSMpegImage({
          url,
          readyCallback: () => {
            source.set({
              left: center[0],
              top: center[1],
              hoverCursor: 'default',
              originX: 'center',
              originY: 'center'
            });
            source.setCoords();

            const pixelsize = this.toCanvasCoords(src.pixelsize);
            this.scaleObject(source, pixelsize, scale);

            this.addObject({ type: 'sources', object: source });
            resolve(source);
            delete this.pendingSources[src.sourceid];
          }
        });
      } else {
        // eslint-disable-next-line no-new
        new fabric.Image.fromURL(src.url, img => {
          img.set({
            left: center[0],
            top: center[1],
            hoverCursor: 'default',
            width: img.width,
            height: img.height,
            originX: 'center',
            originY: 'center'
          });

          const pixelsize = this.toCanvasCoords(src.pixelsize);
          this.scaleObject(img, pixelsize, src.scale);

          this.addObject({ type: 'sources', object: img });
          resolve(img);
          delete this.pendingSources[src.sourceid];
        });
      }
    });
  }

  scaleObject(obj, pixelsize, scale = 1) {
    obj.scaleToWidth(obj.width * Math.abs(pixelsize[0] * scale), true);
    obj.scaleY = Math.abs(pixelsize[1] * scale);
    obj.flipX = Math.sign(pixelsize[0]) === -1;
    obj.flipY = Math.sign(pixelsize[1]) === -1;
  }

  updateSource(fobj, src, sourceUrl) {
    const center = this.toCanvasCoords(src.center);
    fobj.set({
      left: center[0],
      top: center[1],
      opacity: src.hide ? 0 : 1
    });

    const additional_url = find(src.additional_urls, { name: sourceUrl });
    const scale = additional_url ? additional_url.scale : src.scale;

    const pixelsize = this.toCanvasCoords(src.pixelsize);
    this.scaleObject(fobj, pixelsize, scale);
    fobj.setCoords();
  }

  drawImage(img, smp, scale = 1) {
    const viewport = this.fabric.vptCoords;
    const center = this.toCanvasCoords([img.offsetx, img.offsety]);
    const pixelsize = this.toCanvasCoords([img.scalex, img.scaley]);
    const x1 = smp.offsetx + center[0] - Math.abs(img.width * pixelsize[0]) / 2;
    const x2 = smp.offsetx + center[0] + Math.abs(img.width * pixelsize[0]) / 2;
    const y1 =
      smp.offsety + center[1] - Math.abs(img.height * pixelsize[1]) / 2;
    const y2 =
      smp.offsety + center[1] + Math.abs(img.height * pixelsize[1]) / 2;

    if (
      !(
        x2 > viewport.tl.x &&
        x1 < viewport.br.x &&
        y2 > viewport.tl.y &&
        y1 < viewport.br.y
      )
    ) {
      return;
    }

    if (img.sampleimageid in this.pendingImages) {
      return null;
    }
    this.pendingImages[img.sampleimageid] = 1;

    return new Promise((resolve, reject) => {
      const ximg = new XHRImage();
      ximg.onerror = () => {
        reject();
      };
      ximg.onload = () => {
        const loadedScale = ximg.width / img.width;
        const image = new fabric.Image(ximg, {
          left: smp.offsetx + center[0],
          top: smp.offsety + center[1],
          hoverCursor: 'default',
          width: ximg.width,
          height: ximg.height,
          originX: 'center',
          originY: 'center'
        });

        this.scaleObject(image, pixelsize, 1 / loadedScale);
        image.loadedScale = loadedScale;

        this.addObject({ type: 'images', object: image });
        delete this.pendingImages[img.sampleimageid];
        resolve(image);
      };

      ximg.load(`${config.baseUrl + img.url}?scale=${scale}`);
    });
  }

  updateImage(fobj, img, smp) {
    const center = this.toCanvasCoords([img.offsetx, img.offsety]);
    fobj.set({
      left: smp.offsetx + center[0],
      top: smp.offsety + center[1],
      opacity: img.hide ? 0 : 1
    });
    const pixelsize = this.toCanvasCoords([img.scalex, img.scaley]);
    this.scaleObject(fobj, pixelsize, 1 / fobj.loadedScale);
    fobj.setCoords();
  }

  calcPoint(pt) {
    const point = this.toCanvasCoords(pt);
    return { x: point[0], y: point[1] };
  }

  drawPolyline(poly, originCentered = false) {
    // console.log('drawPolyline', poly);
    const stroke = originCentered ? null : this.colors.polyline;
    const points = mapLodash(poly, pt => this.calcPoint(pt));
    const polyline = new fabric.Polyline(points, {
      stroke,
      strokeWidth: 1,
      fill: null,
      hoverCursor: 'default',
      originX: originCentered ? 'center' : null,
      originY: originCentered ? 'center' : null
    });

    this.addObject({ type: 'polyline', object: polyline });
    this.updatePolyline(polyline, poly);

    return polyline;
  }

  updatePolyline(fobj, poly) {
    // console.log('updatePolyline');
    const points = mapLodash(poly, pt => this.calcPoint(pt));
    fobj.set({
      points
    });

    this.scaleStrokeWidth(fobj);

    // TODO: Polylines cannot be updated
    // https://github.com/fabricjs/fabric.js/issues/3488
    // https://stackoverflow.com/questions/29011717/update-fabric-js-path-points-dynamically
    const dims = fobj._calcDimensions();
    fobj.set({
      width: dims.width,
      height: dims.height,
      left: dims.left,
      top: dims.top,
      pathOffset: {
        x: dims.width / 2 + dims.left,
        y: dims.height / 2 + dims.top
      },
      dirty: true
    });
    fobj.setCoords();
  }

  drawMap(map, obj) {
    if (map.mapid in this.pending) {
      // console.log('drawMap pending', map.mapid);
      return null;
    }

    this.pending[map.mapid] = 1;
    return new Promise((resolve, reject) => {
      const ximg = new XHRImage();
      ximg.onerror = () => {
        reject();
      };
      ximg.onload = () => {
        const img = new fabric.Image(ximg, {
          left: obj.left,
          top: obj.top,
          width: ximg.width,
          height: ximg.height,
          hoverCursor: 'default'
        });

        img.scaleToWidth(obj.width);
        img.scaleY = obj.height / img.height;
        this.addObject({ type: 'maps', object: img });
        delete this.pending[map.mapid];
        resolve(img);
        img.lastMap = map;
      };

      ximg.load(config.baseUrl + map.url);
    });
  }

  updateMap(fobj, map, obj) {
    let changed = false;
    each(['min', 'max', 'points', 'colourmap', 'opacity', 'scale'], k => {
      if (map[k] !== fobj.lastMap[k]) {
        changed = true;
      }
    });

    fobj.set({
      left: obj.left,
      top: obj.top
    });

    if (changed) {
      const ximg = new XHRImage();
      ximg.onload = () => {
        fobj.setElement(ximg, img => {
          img.set({
            hoverCursor: 'default',
            selectable: true
          });
        });
        fobj.lastMap = map;
      };
      ximg.load(config.baseUrl + map.url);
    }
  }

  drawComposite(comp, obj) {
    if (comp.compositeid in this.pendingComposites) {
      // console.log('drawComposite pending', comp.compositeid);
      return null;
    }

    this.pendingComposites[comp.compositeid] = 1;
    return new Promise((resolve, reject) => {
      const ximg = new XHRImage();
      ximg.onerror = () => {
        reject();
      };
      ximg.onload = () => {
        const img = new fabric.Image(ximg, {
          left: obj.left,
          top: obj.top,
          width: ximg.width,
          height: ximg.height,
          hoverCursor: 'default'
        });

        img.scaleToWidth(obj.width);
        img.scaleY = obj.height / img.height;
        img.compositeMap = comp;
        this.addObject({ type: 'composite', object: img });
        delete this.pendingComposites[comp.compositeid];
        resolve(img);
      };
      ximg.load(config.baseUrl + comp.url);
    });
  }

  updateComposite(fobj, comp, obj) {
    let changed = false;
    each(['ropacity', 'gopacity', 'bopacity', 'opacity'], k => {
      if (comp[k] !== fobj.compositeMap[k]) {
        changed = true;
      }
    });

    fobj.set({
      left: obj.left,
      top: obj.top
    });

    if (changed) {
      const ximg = new XHRImage();
      ximg.onload = () => {
        fobj.setElement(ximg, img => {
          img.set({
            hoverCursor: 'default',
            selectable: true
          });
        });
        fobj.compositeMap = comp;
      };
      ximg.load(config.baseUrl + comp.url);
    }
  }
}
