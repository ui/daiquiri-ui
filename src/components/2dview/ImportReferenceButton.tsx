import { useRef, useCallback, useState } from 'react';
import { Button } from 'react-bootstrap';

import SchemaForm from 'connect/SchemaForm';
import ButtonTriggerModalStore from 'connect/layout/ButtonTriggerModalStore';
import type { ButtonTriggerModalStoreRef } from 'components/layout/models';

interface Props {
  sampleid: number;
}

export default function ImportReferenceButton(props: Props) {
  const onSubmitRef = useRef((value: boolean) => {});
  const disabledRef = useRef(false);
  const modalRef = useRef<ButtonTriggerModalStoreRef>(null);
  const [render, forceRender] = useState(true);

  const onSubmit = useCallback(async (formData: any, e: any, doSubmit: any) => {
    const response = await doSubmit(formData);
    modalRef.current?.close();
    return response;
  }, []);

  return (
    <ButtonTriggerModalStore
      ref={modalRef}
      id="importReference"
      button={<i className="fa fa-image" />}
      buttonProps={{
        className: 'float-end me-1',
        size: 'sm',
      }}
      title="Import Reference Image"
      buttons={
        <>
          <Button
            onClick={() => onSubmitRef.current(false)}
            disabled={disabledRef.current}
          >
            Import
          </Button>
        </>
      }
    >
      <SchemaForm
        schema="ReferenceSchema"
        formData={{
          sampleid: props.sampleid,
        }}
        requireOperator
        button={(d: boolean, os: (value: boolean) => void) => {
          disabledRef.current = d;
          onSubmitRef.current = os;
          forceRender(!render);
        }}
        onSubmit={onSubmit}
      />
    </ButtonTriggerModalStore>
  );
}
