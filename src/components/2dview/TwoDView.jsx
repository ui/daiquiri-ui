import { Component } from 'react';

import RemountOnResize from 'components/utils/RemountOnResize';
import Controls from 'connect/2dview/Controls';
import Overlay from 'connect/2dview/Overlay';
import OverlayMessages from 'components/2dview/OverlayMessages';

export default class TwoDView extends Component {
  render() {
    return (
      <RemountOnResize>
        <div className="h-100-container twod">
          <Controls />
          <Overlay className="overlay" providers={this.props.providers} />
          <OverlayMessages messages={this.props.messages} />
        </div>
      </RemountOnResize>
    );
  }
}
