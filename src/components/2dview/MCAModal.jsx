import { createRef, Component } from 'react';
import PropTypes from 'prop-types';
import { map } from 'lodash';

import { Container, Row, Col, Alert } from 'react-bootstrap';

import ButtonTriggerModalStore from 'connect/layout/ButtonTriggerModalStore';
import ScanPlot1d from 'components/scans/ScanPlot1d';

export default class MCAModal extends Component {
  static defaultProps = {
    pointer: {},
    spectra: {},
    rois: {},
    spectraError: '',
    spectraFetched: false,
    spectraFetching: false
  };

  static propTypes = {
    pointer: PropTypes.shape({
      scanid: PropTypes.number,
      point: PropTypes.number
    }),
    spectra: PropTypes.shape({
      npoints: PropTypes.number
    }),
    rois: PropTypes.shape({}),
    actions: PropTypes.shape({
      setMapPointer: PropTypes.func.isRequired,
      fetchScanSpectra: PropTypes.func.isRequired
    }).isRequired,
    spectraError: PropTypes.string,
    spectraFetched: PropTypes.bool,
    spectraFetching: PropTypes.bool
  };

  constructor(props) {
    super(props);
    this.modalRef = createRef();
  }

  componentDidUpdate(last) {
    if (
      this.props.pointer.point !== last.pointer.point ||
      this.props.pointer.scanid !== last.pointer.scanid
    ) {
      if (this.props.pointer.point !== undefined) {
        this.getSpectra();
        this.modalRef.current.showModal();
      } else {
        this.props.actions.setMapPointer();
      }
    }
  }

  getSpectra() {
    this.props.actions
      .fetchScanSpectra({
        scanid: this.props.pointer.scanid,
        point: this.props.pointer.point
      })
      .catch(() => {
        console.log('Couldnt load spectra');
      });
  }

  render() {
    const shapes = map(this.props.rois, r => ({
      type: 'rect',
      xref: 'x',
      yref: 'paper',
      y0: 0,
      y1: 1,
      x0: r.start,
      x1: r.end,
      fillcolor: '#ccc',
      opacity: 0.5,
      line: {
        width: 0
      }
    }));

    let i = 0;
    const annotations = map(this.props.rois, r => ({
      xref: 'x',
      yref: 'paper',
      y: 0.8 - i++ * 0.06,
      x: r.start,
      text: `${r.element}-${r.edge}`,
      showarrow: false
    }));

    return (
      <ButtonTriggerModalStore
        id="mca"
        ref={this.modalRef}
        showButton={false}
        title="MCA"
      >
        {this.props.spectraFetching && (
          <Alert variant="info">
            <i className="fa fa-spin fa-spinner me-1" />
            Fetching point {this.props.pointer.point} from scan{' '}
            {this.props.pointer.scanid}
          </Alert>
        )}

        {this.props.spectraError && (
          <Alert variant="warning">
            Scan number {this.props.pointer.scanid} is no longer available
          </Alert>
        )}

        {!this.props.spectraError &&
          this.props.spectraFetched &&
          !this.props.spectraFetching && (
            <Container>
              <Row>
                Point {this.props.pointer.point + 1}/
                {this.props.spectra.npoints}
              </Row>
              <Row style={{ height: '40vh' }}>
                <Col>
                  <ScanPlot1d
                    fetching={this.props.spectraFetching}
                    selectable={false}
                    spectra={this.props.spectra}
                    data={{ scanid: null }}
                    layout={{
                      shapes,
                      annotations,
                      xaxis: {
                        title: 'Energy (eV)'
                      },
                      yaxis: {
                        title: 'Counts'
                      }
                    }}
                  />
                </Col>
              </Row>
            </Container>
          )}
      </ButtonTriggerModalStore>
    );
  }
}
