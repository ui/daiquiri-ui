import PropTypes from 'prop-types';
import { map } from 'lodash';

import TransitionChildren from 'components/common/TransitionChildren';

function OverlayMessage(props) {
  const messages = map(props.messages, (message, type) => (
    <TransitionChildren key={type} timeout={message.timeout}>
      {message.text}
    </TransitionChildren>
  ));

  return <div className="overlay-messages">{messages}</div>;
}

OverlayMessage.propTypes = {
  messages: PropTypes.objectOf(
    PropTypes.shape({
      text: PropTypes.node.isRequired,
      timeout: PropTypes.number
    })
  ).isRequired
};

export default OverlayMessage;
