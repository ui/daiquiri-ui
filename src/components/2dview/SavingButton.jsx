import { Component } from 'react';
import PropTypes from 'prop-types';
import { map } from 'lodash';

import { Form, Row, Col } from 'react-bootstrap';

import ButtonTriggerModalStore from 'connect/layout/ButtonTriggerModalStore';

export default class SavingButton extends Component {
  static defaultProps = {
    disabled: false
  };

  static propTypes = {
    actions: PropTypes.shape({
      fetchSaving: PropTypes.func.isRequired
    }).isRequired,
    saving: PropTypes.shape({
      arguments: PropTypes.shape({})
    }).isRequired,
    disabled: PropTypes.bool
  };

  onSubmit = (formData, e, doSubmit) =>
    doSubmit(formData).then(() => {
      this.props.actions.fetchSaving();
    });

  render() {
    return (
      <ButtonTriggerModalStore
        id="saving"
        button={<i className="fa fa-cog" />}
        buttonProps={{
          className: 'me-1 float-end',
          disabled: this.props.disabled
        }}
        title="Saving Configuration"
      >
        <Form>
          {map(this.props.saving.arguments, (v, k) => (
            <Form.Group as={Row} key={k}>
              <Form.Label column sm={3}>
                {k}
              </Form.Label>
              <Col>{v}</Col>
            </Form.Group>
          ))}
        </Form>
      </ButtonTriggerModalStore>
    );
  }
}
