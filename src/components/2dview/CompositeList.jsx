import { createRef, Component } from 'react';
import PropTypes from 'prop-types';

import { each, debounce } from 'lodash';
import { Button, OverlayTrigger, Popover, Form } from 'react-bootstrap';

import Table from 'components/table';
import DeleteButton from 'components/common/DeleteButton';

class HideShowButton extends Component {
  static propTypes = {
    compositeid: PropTypes.number.isRequired,
    opacity: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired
  };

  onClick = () => {
    this.props.onChange({
      compositeid: this.props.compositeid,
      opacity: this.props.opacity ? 0 : 1
    });
  };

  render() {
    return (
      <Button
        className="me-1"
        title={this.props.opacity ? 'Hide' : 'Show'}
        size="sm"
        onClick={this.onClick}
        variant={this.props.opacity ? 'primary' : 'secondary'}
      >
        <i className={`fa fa-${this.props.opacity ? 'eye' : 'eye-slash'}`} />
      </Button>
    );
  }
}

class EditButton extends Component {
  static propTypes = {
    compositeid: PropTypes.number.isRequired,
    rroi: PropTypes.string.isRequired,
    groi: PropTypes.string.isRequired,
    broi: PropTypes.string.isRequired,
    ropacity: PropTypes.number.isRequired,
    gopacity: PropTypes.number.isRequired,
    bopacity: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.orefs = {};
    each(['r', 'g', 'b'], c => {
      this.orefs[c] = createRef();
    });

    this.updateCompositeMap = debounce(this.updateCompositeMap.bind(this), 100);
  }

  componentDidUpdate() {
    each(['r', 'g', 'b'], c => {
      if (this.orefs[c].current) {
        this.orefs[c].current.value = this.props[`${c}opacity`] * 100;
      }
    });
  }

  onChange = e => {
    this.updateCompositeMap({
      compositeid: this.props.compositeid,
      [`${e.target.name}opacity`]: e.target.value / 100.0
    });
  };

  updateCompositeMap(props) {
    this.props.onChange(props);
  }

  render() {
    return (
      <OverlayTrigger
        trigger="click"
        placement="left"
        rootClose
        overlay={
          <Popover>
            <Popover.Header>Composite Controls</Popover.Header>
            <Popover.Body>
              <Form.Group>
                <Form.Label>Red [{this.props.rroi}]</Form.Label>
                <Form.Control
                  type="range"
                  min="0"
                  max="100"
                  defaultValue={this.props.ropacity * 100}
                  name="r"
                  onChange={this.onChange}
                  ref={this.orefs.r}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Green [{this.props.groi}]</Form.Label>
                <Form.Control
                  type="range"
                  min="0"
                  max="100"
                  defaultValue={this.props.gopacity * 100}
                  name="g"
                  onChange={this.onChange}
                  ref={this.orefs.g}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Blue [{this.props.broi}]</Form.Label>
                <Form.Control
                  type="range"
                  min="0"
                  max="100"
                  defaultValue={this.props.bopacity * 100}
                  name="b"
                  onChange={this.onChange}
                  ref={this.orefs.b}
                />
              </Form.Group>
            </Popover.Body>
          </Popover>
        }
      >
        <Button title="Composite Controls" size="sm">
          <i className="fa fa-sun-o" />
        </Button>
      </OverlayTrigger>
    );
  }
}

function ActionCell(cell, row, rowIndex, formatExtraData) {
  return (
    <>
      <HideShowButton {...row} onChange={formatExtraData.updateCompositeMap} />
      <EditButton {...row} onChange={formatExtraData.updateCompositeMap} />
      <DeleteButton
        id={row.compositeid}
        onClick={formatExtraData.deleteCompositeMap}
        confirm
      />
    </>
  );
}

export default class CompositeList extends Component {
  static propTypes = {
    composites: PropTypes.arrayOf(
      PropTypes.shape({
        compositeid: PropTypes.number.isRequired,
        r: PropTypes.number.isRequired,
        g: PropTypes.number.isRequired,
        b: PropTypes.number.isRequired,
        opacity: PropTypes.number.isRequired
      })
    ).isRequired,
    actions: PropTypes.shape({
      updateCompositeMap: PropTypes.func.isRequired,
      deleteCompositeMap: PropTypes.func.isRequired
    }).isRequired
  };

  render() {
    const columns = [
      { dataField: 'compositeid', text: 'Id' },
      { dataField: 'r', text: 'Red' },
      { dataField: 'rroi', text: 'ROI', classes: 'text-break' },
      { dataField: 'g', text: 'Green' },
      { dataField: 'groi', text: 'ROI', classes: 'text-break' },
      { dataField: 'b', text: 'Blue' },
      { dataField: 'broi', text: 'ROI', classes: 'text-break' },
      {
        dataField: 'action',
        text: '',
        formatter: ActionCell,
        formatExtraData: {
          updateCompositeMap: this.props.actions.updateCompositeMap,
          deleteCompositeMap: this.props.actions.deleteCompositeMap
        },
        classes: 'text-end text-nowrap'
      }
    ];

    return (
      <Table
        keyField="compositeid"
        data={this.props.composites || []}
        columns={columns}
        noDataIndication="No composite maps for this object"
      />
    );
  }
}
