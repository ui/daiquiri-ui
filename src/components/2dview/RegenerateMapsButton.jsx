import PropTypes from 'prop-types';

import { Button } from 'react-bootstrap';

export default function RegenerateButton(props) {
  const onClick = () => {
    const data = {
      subsampleid: props.subsampleid
    };
    if (props.datacollectionid) {
      data.datacollectionid = props.datacollectionid;
    }

    props.actions.regenerateMaps(data);
  };
  return (
    <Button onClick={onClick} title="Regenerate Maps" {...props.buttonProps}>
      <i className="fa fa-refresh" />
    </Button>
  );
}

RegenerateButton.defaultProps = {
  datacollectionid: null,
  buttonProps: {}
};

RegenerateButton.propTypes = {
  subsampleid: PropTypes.number.isRequired,
  datacollectionid: PropTypes.number,
  buttonProps: PropTypes.shape({}),
  actions: PropTypes.shape({
    regenerateMaps: PropTypes.func.isRequired
  }).isRequired
};
