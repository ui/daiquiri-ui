/* eslint-disable promise/no-promise-in-callback */
import type { PropsWithChildren } from 'react';
import { Component } from 'react';

import ErrorStackParser from 'error-stack-parser';
import StackTraceGPS from 'stacktrace-gps';
import StackFrame from 'stackframe';
import { Container, Alert, Button } from 'react-bootstrap';
import RestService from 'services/RestService';

interface State {
  hasError: boolean;
  message: string;
  trace: string;
  error?: Error;
}

type Props = PropsWithChildren<{ resetKey?: any }>;

export function ResolvedErrorMessage(props: {
  message: string;
  trace: string;
  error: Error | undefined;
}) {
  return (
    <Alert variant="danger">
      {props.message}
      <br />
      {props.trace}
    </Alert>
  );
}

export function baseComponentDidCatch(
  error: Error,
  setStateCallback: (data: Record<string, any>) => void
) {
  const stackframes = ErrorStackParser.parse(error);
  const stackframe = new StackFrame(stackframes[0]);
  const gps = new StackTraceGPS();

  gps.pinpoint(stackframe).then(
    (frame) => {
      RestService.post('/logging/ui', {
        status: 'resolved',
        message: error.message,
        frame: frame.toString(),
      });
      setStateCallback({
        trace: frame.toString(),
      });
    },
    (frameError) => {
      RestService.post('/logging/ui', {
        status: 'error',
        message: error.message,
        error: frameError,
      });
      setStateCallback({
        trace: error.toString(), // eslint-disable-line @typescript-eslint/no-base-to-string
      });
    }
  );
}

const INITIAL_STATE = {
  hasError: false,
  message: '',
  trace: '',
  error: undefined,
};

export default class ErrorBoundary extends Component<Props, State> {
  public constructor(props: Props) {
    super(props);
    this.state = INITIAL_STATE;
  }

  public static getDerivedStateFromError() {
    return { hasError: true };
  }

  private readonly reload = () => {
    window.location.reload();
  };

  public componentDidCatch(error: Error) {
    baseComponentDidCatch(error, (data) => {
      this.setState({
        hasError: true,
        message: error.message,
        error,
        ...data,
      });
    });
  }

  // https://github.com/bvaughn/react-error-boundary/blob/23a4d779744f3079dfe0960acaa4bdfd5dede87b/src/ErrorBoundary.ts#L53
  public componentDidUpdate(prevProps: Props, prevState: State) {
    if (
      this.state.hasError &&
      prevState.error !== undefined &&
      prevProps.resetKey !== this.props.resetKey
    ) {
      this.setState(INITIAL_STATE);
    }
  }

  public render() {
    if (this.state.hasError) {
      return (
        <Container className="mt-2">
          <h4>Something went wrong</h4>
          <ResolvedErrorMessage
            message={this.state.message}
            trace={this.state.trace}
            error={this.state.error}
          />
          <p>
            This error has been logged. Please{' '}
            <Button size="sm" onClick={this.reload}>
              <i className="fa fa-refresh" /> Reload
            </Button>{' '}
            the application
          </p>
        </Container>
      );
    }
    return this.props.children;
  }
}
