import { createRef, Component } from 'react';
import PropTypes from 'prop-types';
import { SvgLoader, SvgProxy } from 'react-svgmt';

import { map, filter } from 'lodash';

import { Alert } from 'react-bootstrap';

import HardwareGroup from 'components/hardware/HardwareGroup';
import DraggablePopup from 'components/layout/DraggablePopup';
import ButtonTriggerModalStore from 'connect/layout/ButtonTriggerModalStore';

export default class Synoptic extends Component {
  static defaultProps = {
    sid: 0
  };

  static propTypes = {
    actions: PropTypes.shape({
      fetchHardware: PropTypes.func.isRequired,
      fetchHardwareGroups: PropTypes.func.isRequired,
      fetchSVG: PropTypes.func.isRequired
    }).isRequired,
    synoptic: PropTypes.shape({
      synoptics: PropTypes.arrayOf(
        PropTypes.shape({
          elements: PropTypes.arrayOf(
            PropTypes.shape({
              name: PropTypes.string,
              value: PropTypes.string,
              group: PropTypes.string
            })
          ),
          synoptic: PropTypes.string
        })
      )
    }).isRequired,
    hardwareGroups: PropTypes.shape({}).isRequired,
    hardwareObjects: PropTypes.shape({}).isRequired,

    /** The synoptic id to show */
    sid: PropTypes.number
  };

  constructor(props) {
    super(props);
    this.buttonRefs = {};
    this.rectRefs = {};
    this.state = {
      svg: undefined
    };

    if (this.validSynoptic()) {
      this.props.actions.fetchSVG(this.props.sid).then(resp => {
        this.setState({ svg: resp.data });
      });
    }
  }

  componentDidMount() {
    this.props.actions.fetchHardware();
    this.props.actions.fetchHardwareGroups();
  }

  onClick = (name, group) => {
    if (group in this.buttonRefs) {
      this.buttonRefs[group].current.showModal();
    }
  };

  validSynoptic() {
    return (
      this.props.sid < this.props.synoptic.synoptics.length &&
      this.props.sid > -1
    );
  }

  render() {
    if (!this.validSynoptic()) {
      return (
        <Alert variant="warning">
          Could not find synoptic &quot;{this.props.sid}&quot;
        </Alert>
      );
    }

    const synoptic = this.props.synoptic.synoptics[this.props.sid];

    const groups = map(synoptic.elements, el => {
      const group = el.group ? this.props.hardwareGroups[el.group] : null;

      let value = null;
      if (el.value) {
        const path = el.value.split('.');
        const obj = this.props.hardwareObjects[path[0]];
        if (obj) {
          value = `${obj.properties[path[1]]} ${obj.properties.unit || ''}`;
        }
      }

      const cls = el.name.toLowerCase();
      this.rectRefs[el.group] = createRef();

      return [
        <SvgProxy
          key={`${el.group}state`}
          selector={`.${cls} .status`}
          class={`$ORIGINAL ${group && (group.state ? 'ok' : 'error')}`}
        />,
        <SvgProxy key={`${el.group}value`} selector={`.${cls} .value`}>
          {value}
        </SvgProxy>,
        <SvgProxy
          key={`${el.group}handler`}
          selector={`.${cls} > rect`}
          onClick={() => this.onClick(cls, el.group)}
          ref={this.rectRefs[el.group]}
        />,
        <SvgProxy
          key={`${el.group}comp`}
          class={`$ORIGINAL ${group ? 'active' : ''}`}
          selector={`.${cls}`}
        />
      ];
    });

    const hwgroups = filter(synoptic.elements, e => e.group);
    const popups = map(hwgroups, g => {
      this.buttonRefs[g.group] = createRef();
      const popperInterface = () => {
        if (!this.rectRefs[g.group].current) return;
        return this.rectRefs[g.group].current.elemRefs[0];
      };

      return (
        <ButtonTriggerModalStore
          id={`${g.group}pop`}
          key={`${g.group}pop`}
          ref={this.buttonRefs[g.group]}
          target={popperInterface}
          showButton={false}
          modalClass={DraggablePopup}
          popoverid={g.group}
          title={
            (this.props.hardwareGroups[g.group] &&
              this.props.hardwareGroups[g.group].name) ||
            ''
          }
          button={
            <>
              <i className="fa fa-plus" />
              show
            </>
          }
        >
          <HardwareGroup
            even
            objects={
              this.props.hardwareGroups[g.group] &&
              map(this.props.hardwareGroups[g.group].objects, id => ({ id }))
            }
          />
        </ButtonTriggerModalStore>
      );
    });

    return (
      <div>
        <SvgLoader width="100%" svgXML={this.state.svg} className="synoptic">
          {groups}
        </SvgLoader>
        {popups}
      </div>
    );
  }
}
