import { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { trim, throttle } from 'lodash';
import { Button } from 'react-bootstrap';

export default function LogViewer(props) {
  const [offset, setOffset] = useState(0);
  const lines = trim(props.log).split('\n');

  const onScroll = useCallback(
    throttle(dir => {
      setOffset(currentOffset => {
        if (dir > 0) {
          if (currentOffset < lines.length - props.noLines) {
            return currentOffset + 1;
          }
        } else if (currentOffset > 0) {
          return currentOffset - 1;
        }

        return currentOffset;
      });
    }, 50),
    [lines.length]
  );

  const canScrollUp = offset < lines.length - props.noLines;
  const canScrollDown = offset > 0;

  const outputLines = lines.slice(
    lines.length - props.noLines - offset,
    lines.length - offset
  );
  if (outputLines[outputLines.length - 1] === '') {
    outputLines[outputLines.length - 1] = '\n';
  }

  return (
    <div className="log-viewer-wrap" onWheel={e => onScroll(-e.deltaY)}>
      <div className="log-viewer">{outputLines.join('\n')}</div>
      <div className="log-viewer-scroll">
        <Button
          className="scroll-up"
          size="sm"
          onClick={() => onScroll(1)}
          disabled={!canScrollUp}
        >
          <i className="fa fa-caret-up" />
        </Button>
        <Button
          className="scroll-down"
          size="sm"
          onClick={() => onScroll(-1)}
          disabled={!canScrollDown}
        >
          <i className="fa fa-caret-down" />
        </Button>
      </div>
    </div>
  );
}

LogViewer.defaultProps = {
  log: '',
  noLines: 5
};

LogViewer.propTypes = {
  log: PropTypes.string,
  noLines: PropTypes.number
};
