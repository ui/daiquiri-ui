import Alert from 'react-bootstrap/Alert';
import ServerWarning from 'components/ServerWarning';
import ScanStatus from 'connect/scans/ScanStatus';
import type { ScanSchema } from '../types/Scan';

interface Props {
  inline?: boolean;
  actions: {
    removeAlert: (id: string) => void;
  };
  alerts?: {
    id: string;
    type: string;
    message: string;
  }[];
  networkError?: boolean;
  ready?: boolean;
  runningScan?: ScanSchema;
}

export default function Alerts(props: Props) {
  const {
    networkError = false,
    ready = false,
    inline = false,
    runningScan,
    alerts = [],
    actions,
  } = props;
  return (
    <div className="alerts-wrapper">
      {alerts.length === 0 && !networkError && !runningScan && !inline && (
        <Alert variant="light" className="border-secondary">
          <i className="fa fa-bell-o me-1" />
          No Alerts
        </Alert>
      )}
      <ServerWarning show={networkError && ready} />
      {alerts.map((alert) => (
        <Alert
          variant={alert.type}
          key={alert.id}
          onClose={() => actions.removeAlert(alert.id)}
        >
          {alert.message}
        </Alert>
      ))}
      <ScanStatus />
    </div>
  );
}
