import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';

import Tooltip from 'components/common/Tooltip';

export default function ControlButton(props) {
  const icon = props.operator ? 'unlock' : 'lock';
  return (
    <Tooltip
      tooltip={`This session ${
        props.operator ? 'has' : 'does not have'
      } control. Click to ${props.operator ? 'yield' : 'request'}`}
      bottom
    >
      <Button
        size="sm"
        className="me-2 btn-fw"
        variant={props.operator ? 'success' : 'warning'}
        onClick={
          props.operator
            ? props.actions.yieldControl
            : props.actions.requestControl
        }
      >
        <i className={`fa fa-${icon}`} />
      </Button>
    </Tooltip>
  );
}

ControlButton.propTypes = {
  operator: PropTypes.bool.isRequired,
  actions: PropTypes.shape({
    requestControl: PropTypes.func.isRequired,
    yieldControl: PropTypes.func.isRequired
  }).isRequired
};
