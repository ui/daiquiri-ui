import Form from '@rjsf/core';
import type { TomoScanTask } from 'types/Tomo';
import {
  ObjectFieldTemplate,
  GroupTemplates,
} from 'components/form-widgets/GroupedFields';
import {
  TemplatedFieldTemplate,
  TemplatedObjectFieldTemplate,
  TemplatedArrayFieldTemplate,
} from 'components/form-widgets/CustomTemplates';

const schema = {
  title: 'Tomo scan description',
  type: 'object',
  properties: {
    global_kind: {
      title: 'Kind of sequence',
      type: 'string',
      enum: ['default', 'zseries', 'helical'],
      enumNames: ['Default tomo scan', 'Z-series', 'Helical'],
      default: 'default',
    },
    scan_type: {
      title: 'Kind of scan',
      type: 'string',
      enum: ['step', 'continuous', 'sweep', 'interlaced'],
      enumNames: ['Step by step', 'Continuous', 'Sweep', 'Interlaced'],
      default: 'step',
    },
    latency_time: {
      title: 'Latency time',
      type: 'number',
      default: 0,
    },
    return_to_start_pos: {
      title: 'Return to start pos',
      type: 'boolean',
      default: 0,
    },
    flat_on: {
      title: 'Enable flat',
      type: 'boolean',
      default: true,
    },
    dark_on: {
      title: 'Enable dark',
      type: 'boolean',
      default: true,
    },
    start_pos: {
      title: 'Start angle position',
      type: 'number',
      default: 0,
    },
    range: {
      title: 'Range for the rotation',
      type: 'number',
      default: 360,
    },
    tomo_n: {
      title: 'Number of projections',
      type: 'integer',
      default: 0,
    },
    flat_n: {
      title: 'Number of flat',
      type: 'integer',
      default: 0,
    },
    dark_n: {
      title: 'Number of dark',
      type: 'integer',
      default: 0,
    },
    exposure_time: {
      title: 'Exposure time',
      type: 'number',
      default: 0,
    },
    comment: {
      title: 'Comment',
      type: 'string',
      default: '',
    },
  },
};

const uiGroups = [
  {
    Trajectory: ['global_kind', 'scan_type', 'return_to_start_pos'],
    'ui:minwidth': 12,
  },
  { Acquisition: ['exposure_time', 'latency_time'] },
  { Rotation: ['start_pos', 'range', 'tomo_n'], 'ui:minwidth': 12 },
  { Dark: ['dark_on', 'dark_n'], 'ui:minwidth': 12 },
  { Flat: ['flat_on', 'flat_n'], 'ui:minwidth': 12 },
  'comment',
];

const uiSchema = {
  comment: {
    'ui:widget': 'textarea',
  },
  'ui:groups': uiGroups,
  'ui:template': ObjectFieldTemplate,
};

export interface TomoScanTaskFormOptions {
  [key: string]: never;
}

interface Props {
  task: TomoScanTask;
  options: TomoScanTaskFormOptions;
}

export default function TomoScanTaskForm(props: Props) {
  const { task } = props;
  return (
    <>
      <Form
        // @ts-expect-error
        schema={schema}
        formData={task.scanParameters}
        FieldTemplate={TemplatedFieldTemplate as any}
        ObjectFieldTemplate={TemplatedObjectFieldTemplate as any}
        ArrayFieldTemplate={TemplatedArrayFieldTemplate as any}
        uiSchema={uiSchema}
        onChange={() => console.log('changed')}
        onSubmit={() => console.log('submitted')}
        onError={() => console.log('errors')}
        formContext={{
          templates: GroupTemplates,
          groups: uiGroups,
        }}
      />
    </>
  );
}
