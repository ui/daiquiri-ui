import { Form } from 'react-bootstrap';
import type { ChangeEvent, KeyboardEvent } from 'react';
import { useEffect, useRef, useState } from 'react';
import classNames from 'classnames';

export function InputNumberOrNull(props: {
  value: number | null;
  onValidate: (value: number | null) => Promise<void> | void;
  onAbort?: () => void;
  readOnly?: boolean;
  precision?: number;
  step?: number;
  coef?: number;
}) {
  const { coef = 1 } = props;
  const valueRef = useRef<HTMLInputElement>(null);
  const [edited, setEdited] = useState(false);
  const [error, setError] = useState(false);

  function normalizeValue(value: number | null): string {
    if (value === null) {
      return '';
    }
    const v = value * coef;
    if (props.precision !== undefined) {
      return v.toFixed(props.precision);
    }
    return v.toString();
  }

  useEffect(() => {
    if (valueRef.current) {
      valueRef.current.value = normalizeValue(props.value);
      setEdited(false);
    }
  }, [props.value, props.precision]);

  function updateRef(value: any) {
    const newValue = normalizeValue(value);
    if (valueRef?.current) {
      valueRef.current.value = newValue;
    }
  }

  function onKeyDown(e: KeyboardEvent<HTMLInputElement>) {
    function readValue(): number | null {
      try {
        // @ts-expect-error
        return Number.parseFloat(e.target.value) / coef;
      } catch {
        return null;
      }
    }

    switch (e.key) {
      case 'Enter': {
        setEdited(false);
        const value = readValue();
        const promise = props.onValidate(value);
        if (promise) {
          promise.catch(() => {
            setError(true);
            setTimeout(() => {
              updateRef(props.value);
              setError(false);
            }, 2000);
          });
        }
        e.preventDefault();
        e.stopPropagation();
        break;
      }
      case 'Esc': // IE/Edge specific value
      case 'Escape':
        if (edited) {
          setError(false);
          setEdited(false);
          updateRef(props.value);
        }
        // @ts-expect-error
        e.target.blur();
        e.preventDefault();
        e.stopPropagation();
        break;
    }
  }

  function onChange(e: ChangeEvent<HTMLInputElement>) {
    setEdited(!(e.target.value === props.value?.toString()));
  }

  return (
    <Form.Control
      className={classNames({
        'form-control-edited': edited,
        'form-control-error': error,
      })}
      type="number"
      ref={valueRef}
      onChange={onChange}
      onKeyDown={onKeyDown}
      disabled={props.readOnly}
      step={props.step}
    />
  );
}
