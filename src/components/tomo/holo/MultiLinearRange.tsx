import { Form } from 'react-bootstrap';
import { useMemo } from 'react';
import type { TomoHoloDistance } from '../../hardware/tomoholo';
import { clamp } from 'lodash';

/**
 * Use a Range to display a position between multiple postion of linear distances
 */
export function MultiLinearRange(props: {
  className?: string;
  thumbClassName?: string;
  style?: Record<string, any>;
  thumbStyle?: Record<string, any>;
  position: number | null;
  distances: TomoHoloDistance[];
  variant?: string;
}) {
  const {
    style,
    position,
    distances,
    className,
    thumbClassName,
    thumbStyle = {},
  } = props;

  const mappingPositions = useMemo(() => {
    const positions: number[] = [];
    if (distances.length > 1) {
      const d1 = distances[0].position;
      const d2 = distances[1].position;
      positions.push(d1 - (d2 - d1) * 0.5);
    }
    for (const d of distances) {
      positions.push(d.position);
    }
    if (distances.length > 1) {
      const d1 = distances[distances.length - 2].position;
      const d2 = distances[distances.length - 1].position;
      positions.push(d2 + (d2 - d1) * 0.5);
    }
    return positions;
  }, [distances]);

  const mappedPosition = useMemo(() => {
    if (position === null) {
      return null;
    }
    let ipos = mappingPositions.length;
    for (const [i, p] of mappingPositions.entries()) {
      if (position < p) {
        ipos = i - 1;
        break;
      }
    }
    if (ipos <= -1) {
      return 0.5;
    }
    if (ipos >= mappingPositions.length) {
      return mappingPositions.length + 0.5;
    }
    const before = mappingPositions[ipos];
    const after = mappingPositions[ipos + 1];
    const coef = (position - before) / (after - before);

    const beforeCoef = ipos === 0 ? -0.5 : ipos;
    const afterCoef =
      ipos === mappingPositions.length - 2
        ? mappingPositions.length - 0.5
        : ipos + 1;
    return beforeCoef * (1 - coef) + afterCoef * coef;
  }, [mappingPositions, position]);

  if (mappedPosition === null) {
    return <></>;
  }

  const percent =
    clamp((mappedPosition - 0.5) / (mappingPositions.length - 2), 0, 1) * 100;

  return (
    <div className={`multi-linear-range ${className}`} style={style}>
      <i
        className={`${thumbClassName}`}
        style={{ ...thumbStyle, left: `${percent}%` }}
      />
    </div>
  );
}
