import debug from 'debug';
import { Alert, Button, Col, Container, Row } from 'react-bootstrap';
import {
  useHolotomoHardware,
  useLimaHardware,
  useMotorHardware,
  useTomoConfig,
  useTomoConfigHardware,
  useTomoDetectorHardware,
  useTomoSampleStageHardware,
  useTomoSampleStageRefs,
} from '../utils/hooks';
import { useHardwareChangeActions, useOperator } from '../../utils/hooks';
import type { TomoDetectorSchema } from '../../hardware/tomodetector';
import { useMemo } from 'react';
import { useEffect, useState } from 'react';
import type { TomoHoloDistance, TomoHoloSchema } from '../../hardware/tomoholo';
import { TomoHoloState } from '../../hardware/tomoholo/State';
import type { MotorSchema } from '@esrf/daiquiri-lib';
import type { TomoConfigSchema } from '../../hardware/tomoconfig';
import { useHover } from './hoverHook';
import { InputNumberOrNull } from './InputNumberOrNull';
import { NbDistances } from './NbDistances';
import { MultiLinearRange } from './MultiLinearRange';
import Qty from 'js-quantities';
import { getAxisName } from '../utils/geometry';

const logger = debug('daiquiri.components.tomo.TomoDetector');

export interface TomoHoloOptions {
  /** Identifier of the tomoconfig hardware object */
  tomoconfig: string;
}

interface Props {
  options: TomoHoloOptions;
}

function useFormattedQuantity(
  value: number | null,
  unit: string,
  displayUnit: string,
  digits: number
): string {
  return useMemo(() => {
    function normUnit(u: string): string {
      if (u === 'um') {
        return 'μm';
      }
      return u;
    }
    function normValue(v: number): string {
      return v.toFixed(digits).replace(/\.?0+$/, '');
    }
    if (value === null) {
      return '∅';
    }
    if (unit === displayUnit) {
      return `${normValue(value)} ${normUnit(displayUnit)}`;
    }
    const q = Qty(value, unit);
    return `${normValue(q.to(displayUnit).scalar)} ${normUnit(displayUnit)}`;
  }, [value, unit, displayUnit, digits]);
}

function hardwareRefToId(ref?: string | null): string | null {
  if (ref === undefined || ref === null) {
    return null;
  }
  return ref.replace('hardware:', '');
}

function Distance(props: {
  distance: LocalDistance;
  tomoDetector: TomoDetectorSchema | null;
  moveToPosition: (pos: string) => void;
  sx: MotorSchema | null;
  disabled?: boolean;
  gridColumn: number | string;
  stabilizing?: boolean;
  onEnter?: () => void;
  onLeave?: () => void;
}) {
  const {
    distance,
    tomoDetector,
    moveToPosition,
    disabled = false,
    gridColumn,
    stabilizing = false,
    sx,
  } = props;
  const [hoverRef, isHovered] = useHover<HTMLButtonElement>();
  const { onEnter, onLeave } = props;

  const detector = useLimaHardware(
    hardwareRefToId(tomoDetector?.properties.detector ?? null)
  );
  const detectorSize = detector?.properties.size ?? [0, 0];
  const fov = distance.pixel_size * detectorSize[0];
  const tolerance = sx?.properties?.tolerance ?? 0.001;
  const digits = sx?.properties?.display_digits ?? 3;
  const position = sx?.properties?.position ?? null;
  const limits = sx?.properties?.limits ?? [0, 1];
  const selected =
    position !== null
      ? Math.abs(distance.position - position) < tolerance
      : false;

  const inRange =
    distance.position >= limits[0] && distance.position <= limits[1];

  const isFocal = distance.distanceName === 'focal';
  const variant = selected
    ? stabilizing
      ? 'warning'
      : 'primary'
    : inRange
    ? 'secondary'
    : 'danger';
  const positionMessage = inRange
    ? ''
    : 'Position is outside of the axis limits';

  function Title() {
    if (isFocal) {
      return <h5 className="py-1">Focal</h5>;
    }
    return (
      <h5 className={`rounded-4 bg-${variant} text-bg-${variant} py-1`}>
        {`Distance ${distance.distanceNum}`}
      </h5>
    );
  }
  useEffect(() => {
    if (isHovered) {
      onEnter?.();
    } else {
      onLeave?.();
    }
  }, [isHovered]);

  const fdistance = useFormattedQuantity(distance.z1, 'mm', 'um', digits - 3);
  const fpx = useFormattedQuantity(distance.pixel_size, 'um', 'nm', 3);
  const ffov = useFormattedQuantity(fov, 'um', 'um', 3);
  const fposition = useFormattedQuantity(
    distance.position,
    'mm',
    'um',
    digits - 3
  );

  return (
    <>
      <div
        style={{ gridColumn, gridRow: 1 }}
        className="text-center"
        title={positionMessage}
      >
        <Title />
      </div>
      <div style={{ gridColumn, gridRow: 2 }} className="text-center">
        {fdistance}
      </div>
      <div style={{ gridColumn, gridRow: 3 }} className="text-center">
        {!isFocal && `${fpx}`}
      </div>
      <div style={{ gridColumn, gridRow: 4 }} className="text-center">
        {!isFocal && `${ffov}`}
      </div>
      <div
        style={{ gridColumn, gridRow: 6 }}
        title={positionMessage}
        className={`text-center ${inRange ? '' : 'text-danger'}`}
      >
        {fposition}
      </div>
      <div
        style={{ gridColumn, gridRow: 7 }}
        title={`${inRange ? '' : 'Position is outside of the axis limits'}${
          inRange && stabilizing ? 'Waiting for stabilization' : ''
        }`}
      >
        <Button
          ref={hoverRef}
          className="w-100"
          variant={
            selected ? (stabilizing ? 'warning' : 'primary') : 'secondary'
          }
          onClick={() => {
            moveToPosition(distance.distanceName);
          }}
          disabled={disabled || !inRange || stabilizing}
        >
          Move to
        </Button>
      </div>
    </>
  );
}

function DetectorDistance(props: {
  tomoDetector: TomoDetectorSchema | null;
  gridColumn: number;
}) {
  const { tomoDetector, gridColumn } = props;
  if (tomoDetector === null) {
    return <></>;
  }
  function getBlissLimaName(): string {
    const name = tomoDetector?.properties.detector ?? '';
    return name.replace('hardware:', '');
  }

  const limaName = getBlissLimaName();
  const distance = tomoDetector.properties.source_distance;
  return (
    <>
      <div style={{ gridColumn, gridRow: 1 }} className="text-center">
        <h5 className="py-1">Detector</h5>
      </div>
      <div style={{ gridColumn, gridRow: 2 }} className="text-center">
        {distance && <>{distance.toFixed(2)} mm</>}
      </div>
      <div
        style={{ gridColumn, gridRow: 5, zIndex: 1 }}
        className="text-center text-middle my-auto"
      >
        <img
          className="mx-auto my-auto"
          src="resources/tomo/holo-detector.png"
          alt="Detector"
          style={{
            gridColumn: 7,
            gridRow: 2,
            width: '92px',
            height: '92px',
            zIndex: 0,
            imageRendering: 'pixelated',
          }}
        />
        <br />
        {limaName}
      </div>
    </>
  );
}

function asStyle(spriteSheet: string): React.CSSProperties {
  const rendering = spriteSheet.includes('_hd.') ? 'default' : 'pixelated';
  return {
    '--sprite-sheet': `url(${spriteSheet})`,
    '--spritesheet-rendering': rendering,
  } as React.CSSProperties;
}

interface LocalDistance extends TomoHoloDistance {
  distanceName: string;
  distanceNum: number | null;
}

/**
 * Returns the holo distances including the focal location
 */
function useDistances(
  tomoConfig: TomoConfigSchema | null,
  holotomo: TomoHoloSchema | null
): LocalDistance[] {
  const sampleStage = useTomoSampleStageHardware(
    tomoConfig?.properties.sample_stage?.slice(9) ?? null
  );
  const x_axis_focal_pos = sampleStage?.properties.x_axis_focal_pos ?? null;
  const distances = holotomo?.properties.distances ?? [];
  return useMemo(() => {
    const res = [];
    if (x_axis_focal_pos) {
      res.push({
        distanceName: 'focal',
        distanceNum: -1,
        pixel_size: 0,
        position: x_axis_focal_pos,
        z1: 0,
      });
    }
    for (const [i, d] of distances.entries()) {
      res.push({ ...d, distanceName: `dist${i + 1}`, distanceNum: i + 1 });
    }
    return res;
  }, [x_axis_focal_pos, distances]);
}

export default function TomoHolo(props: Props) {
  const { options } = props;
  const { tomoconfig: tomoConfigId } = options;
  const tomoConfig = useTomoConfigHardware(tomoConfigId ?? '');
  const sampleStageRefs = useTomoSampleStageRefs(tomoConfigId ?? '');
  const holotomo = useHolotomoHardware(sampleStageRefs?.holotomo ?? null);
  const holoActions = useHardwareChangeActions(holotomo?.id ?? null);
  const tomoDetector = useTomoDetectorHardware(
    sampleStageRefs?.tomoDetector ?? null
  );
  const operator = useOperator();
  const sx = useMotorHardware(sampleStageRefs?.sx ?? null);
  const sxActions = useHardwareChangeActions(sx?.id ?? null);
  const sxMoving = sx?.properties.state[0] === 'MOVING';

  const distances = useDistances(tomoConfig, holotomo);
  const [hover, setHover] = useState(false);

  const tomoConf = useTomoConfig();
  const spriteSheet =
    tomoConf.avatar?.sprite_sheet ?? 'resources/tomo/fox-yellow.png';

  if (!holotomo) {
    return <Alert variant="warning">No holotomo object found</Alert>;
  }

  function moveToPosition(distanceId: string) {
    holoActions.call('move', distanceId);
  }
  const standUp = hover;

  function getAvatarClassName() {
    if (sx === null) {
      return 'tomo-avatar';
    }
    if (!sxMoving) {
      if (standUp) {
        return 'tomo-avatar stand';
      }
      return 'tomo-avatar';
    }
    const properties = sx.properties;
    if (properties.position === null || properties.target === null) {
      return 'tomo-avatar';
    }
    if (properties.position < properties.target) {
      return 'tomo-avatar walk-right';
    }
    if (properties.position > properties.target) {
      return 'tomo-avatar walk-left';
    }
    return 'tomo-avatar';
  }
  const avatarClassName = getAvatarClassName();
  const sxName = getAxisName(sx, 'sx');

  return (
    <div
      className="w-100"
      style={{
        flex: '1 1 0%',
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <h2>Configuration</h2>
      <Row className="g-0">
        <Col className="p-1 text-nowrap my-auto" xs={2}>
          <div>State</div>
        </Col>
        <Col className="p-1" xs={3}>
          <TomoHoloState hardware={holotomo} />
        </Col>
      </Row>
      <Row className="g-0">
        <Col className="p-1 text-nowrap my-auto" xs={2}>
          <div>Nb distances</div>
        </Col>
        <Col className="p-1" xs={3}>
          <NbDistances
            value={holotomo.properties?.nb_distances}
            onValueChanged={(value: number | null) => {
              holoActions.setProperty('nb_distances', value);
            }}
            disabled={!operator}
          />
        </Col>
      </Row>
      <Row className="g-0">
        <Col className="p-1 text-nowrap my-auto" xs={2}>
          <div>Pixel size</div>
        </Col>
        <Col className="p-1" xs={3}>
          <InputNumberOrNull
            value={holotomo.properties?.pixel_size}
            onValidate={(value: number | null) => {
              holoActions.setProperty('pixel_size', value);
            }}
            coef={1000}
            readOnly={!operator}
          />
        </Col>
        <Col className="p-1 text-nowrap my-auto" xs={1}>
          <div title="Nanometer">nm</div>
        </Col>
        <Col className="p-1" xs={1}>
          <Button
            variant="secondary"
            onClick={() => {
              holoActions.setProperty('pixel_size', null);
            }}
            disabled={
              !operator || (holotomo.properties?.pixel_size ?? null) === null
            }
          >
            <i className="fa fa-trash" />
          </Button>
        </Col>
      </Row>
      <Row className="g-0">
        <Col className="p-1 text-nowrap my-auto" xs={2}>
          <div>Sample position ({sxName})</div>
        </Col>
        <Col className="p-1 text-nowrap my-auto" xs={3}>
          <div>
            {sx?.properties?.position?.toFixed(
              sx?.properties?.display_digits ?? 3
            )}
          </div>
        </Col>
        <Col className="p-1 text-nowrap my-auto" xs={1}>
          <div title="Micrometer">{sx?.properties?.unit}</div>
        </Col>
        <Col className="p-1" xs={1}>
          <Button
            style={{ visibility: sxMoving ? 'visible' : 'hidden' }}
            variant="danger"
            onClick={() => {
              sxActions.call('stop');
            }}
            disabled={!operator}
          >
            <i className="fa fa-xmark" />
          </Button>
        </Col>
      </Row>
      <Row className="g-0">
        <Col className="p-1 text-nowrap my-auto" xs={2}>
          <div>Settle time</div>
        </Col>
        <Col className="p-1" xs={3}>
          <InputNumberOrNull
            value={holotomo.properties?.settle_time}
            onValidate={(value: number | null) => {
              holoActions.setProperty('settle_time', value);
            }}
            readOnly={!operator}
          />
        </Col>
        <Col className="p-1 text-nowrap my-auto" xs={1}>
          <div title="Second">s</div>
        </Col>
      </Row>
      <h2>Distances</h2>
      {distances.length === 0 && (
        <Alert variant="warning">No distances are setup</Alert>
      )}
      {distances && distances.length > 0 && (
        <Container
          style={{
            display: 'grid',
            gridGap: 5,
            gridTemplateColumns: `auto 1fr 1fr ${'2fr '.repeat(
              distances.length
            )}`,
          }}
        >
          <div style={{ gridColumn: 1, gridRow: 1 }}> </div>
          <div
            style={{ gridColumn: 1, gridRow: 2 }}
            className="text-end"
            title="Distance from the focal plan"
          >
            Distance
          </div>
          <div style={{ gridColumn: 1, gridRow: 3 }} className="text-end">
            Pixel size
          </div>
          <div style={{ gridColumn: 1, gridRow: 4 }} className="text-end">
            Field of view
          </div>
          <div
            style={{ gridColumn: 1, gridRow: 6 }}
            className="text-end"
            title="Position in the x-translation axis"
          >
            Position
          </div>
          {distances.map((d, i) => (
            <Distance
              gridColumn={`${i === 0 ? i + 2 : i + 3} / ${i + 4}`}
              key={i}
              distance={d}
              tomoDetector={tomoDetector}
              moveToPosition={moveToPosition}
              disabled={!operator || holotomo.properties.state !== 'READY'}
              sx={sx}
              stabilizing={holotomo.properties.state === 'STABILIZING'}
              onEnter={() => setHover(true)}
              onLeave={() => setHover(false)}
            />
          ))}
          <img
            className="w-100 my-auto"
            alt="Beam"
            src="resources/tomo/holo-beam.svg"
            style={{
              gridColumn: `3 / ${distances.length + 4}`,
              gridRow: 5,
              height: '8em',
              zIndex: 0,
              objectFit: 'fill',
            }}
          />
          <MultiLinearRange
            thumbClassName={`${avatarClassName}`}
            style={{
              gridColumn: `2 / ${distances.length + 3}`,
              gridRow: 5,
              zIndex: 1,
            }}
            thumbStyle={asStyle(spriteSheet)}
            variant={sxMoving ? 'info' : 'primary'}
            position={sx?.properties.position ?? null}
            distances={distances}
          />
          <DetectorDistance
            tomoDetector={tomoDetector}
            gridColumn={distances.length + 3}
          />
        </Container>
      )}
    </div>
  );
}
