import { InputGroup, Form, Button } from 'react-bootstrap';

export function NbDistances(props: {
  value: number | null;
  onValueChanged: (value: number | null) => void;
  disabled?: boolean;
}) {
  const { value, onValueChanged, disabled = false } = props;

  return (
    <InputGroup>
      <Form.Control value={`${value ?? ''}`} disabled={disabled} readOnly />

      <Button
        variant={value === 2 ? 'primary' : 'secondary'}
        onClick={() => {
          onValueChanged(2);
        }}
        disabled={disabled}
      >
        2
      </Button>
      <Button
        variant={value === 3 ? 'primary' : 'secondary'}
        onClick={() => {
          onValueChanged(3);
        }}
        disabled={disabled}
      >
        3
      </Button>
      <Button
        variant={value === 4 ? 'primary' : 'secondary'}
        onClick={() => {
          onValueChanged(4);
        }}
        disabled={disabled}
      >
        4
      </Button>
      <Button
        variant={value === 5 ? 'primary' : 'secondary'}
        onClick={() => {
          onValueChanged(5);
        }}
        disabled={disabled}
      >
        5
      </Button>
    </InputGroup>
  );
}
