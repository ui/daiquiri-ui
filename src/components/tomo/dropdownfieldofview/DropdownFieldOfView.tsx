import { useEffect } from 'react';

import tomo from 'providers/tomo';
import { DropdownButton, Dropdown } from 'react-bootstrap';
import type { TomoAvailableFieldOfView } from 'types/Tomo';

export interface FieldOfViewSelection {
  detector: string;
  magnification: number | null;
  fov: [number, number];
  pixelSize: [number, number];
}

interface Props {
  as: any;
  value: FieldOfViewSelection | null;
  onSelect: (value: FieldOfViewSelection | null) => void;
  availableSelections: TomoAvailableFieldOfView[];
  variant?: string;
}

function getSampleFov(
  data: TomoAvailableFieldOfView,
  px: [number, number]
): [number, number] {
  return [px[0] * data.size[0] * 0.001, px[1] * data.size[1] * 0.001];
}

function getSamplePixelSize(data: TomoAvailableFieldOfView): [number, number] {
  if (data.userPixelSize !== undefined) {
    return [data.userPixelSize, data.userPixelSize];
  }
  if (data.autoPixelSize !== undefined) {
    const m = data.autoPixelSize.magnification;
    const px = data.autoPixelSize.cameraPixelSize;
    return [px[0] / m, px[1] / m];
  }
  throw new Error('Unsupported TomoAvailableFieldOfView state');
}

function Item(props: {
  eventKey: number;
  isSelected: boolean;
  data: TomoAvailableFieldOfView;
}) {
  const { eventKey, isSelected, data } = props;
  const px = getSamplePixelSize(data);
  const fov = getSampleFov(data, px);
  function getPixelLabel() {
    if (data.userPixelSize !== undefined) {
      return 'user';
    }
    if (data.autoPixelSize !== undefined) {
      const m = data.autoPixelSize.magnification;
      return `×${m}`;
    }
    throw new Error('Unsupported TomoAvailableFieldOfView state');
  }
  return (
    <Dropdown.Item eventKey={eventKey}>
      <div>
        {data.detector} {getPixelLabel()}
      </div>
      <div className="font-weight-bold">
        {fov[0]}mm {px[0]}µm
      </div>
    </Dropdown.Item>
  );
}

export default function DropdownFieldOfView(props: Props) {
  const {
    as = undefined,
    onSelect,
    value,
    availableSelections,
    variant,
  } = props;

  useEffect(() => {
    // fetch the detectors state if it was not already done
    tomo.getNamespace('detectors').fetch(null, true);
    // FIXME: We should wait until this stuff is reached
    // See hardware object to check how it works
  }, []);

  function formatSelection(value: FieldOfViewSelection | null) {
    if (value === null) {
      return 'No detector/optic selected';
    }
    function getPixelLabel() {
      if (value === null) {
        return '';
      }
      if (value.magnification === null) {
        return 'user';
      }
      const m = value.magnification;
      return `×${m}`;
    }
    return `${value.detector} ${getPixelLabel()} ${value.pixelSize[0]}µm ${
      value.fov[0]
    }mm`;
  }

  const label = formatSelection(value);

  function onEventKeySelect(eventKey: string | null) {
    if (eventKey === null) {
      onSelect(null);
      return;
    }
    const index = Number.parseInt(eventKey);
    const dataSelection = availableSelections[index];

    const size = dataSelection.size;
    const px = getSamplePixelSize(dataSelection);
    const fov = getSampleFov(dataSelection, px);

    const selection: FieldOfViewSelection = {
      detector: dataSelection.detector,
      magnification: dataSelection.autoPixelSize?.magnification ?? null,
      fov,
      pixelSize: px,
    };
    onSelect(selection);
  }

  return (
    <DropdownButton
      as={as}
      title={label}
      id="bg-vertical-dropdown-3"
      onSelect={onEventKeySelect}
      variant={variant}
    >
      {availableSelections.map((data, index) => {
        const isSelected: boolean =
          value !== null &&
          data.detector === value.detector &&
          (data.autoPixelSize?.magnification ?? null) === value.magnification;
        return (
          <Item
            key={index}
            eventKey={index}
            isSelected={isSelected}
            data={data}
          />
        );
      })}
    </DropdownButton>
  );
}
