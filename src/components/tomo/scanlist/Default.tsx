import type { MouseEvent } from 'react';
import {
  Table,
  Button,
  Container,
  OverlayTrigger,
  Tooltip,
} from 'react-bootstrap';
import type { TomoScanTask, TomoScanTaskActions } from 'types/Tomo';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

interface Props {
  tasks: TomoScanTask[];
  actions: TomoScanTaskActions;
}

function ScanInfoTooltipContent({ task }: { task: TomoScanTask }) {
  const roi = task.roi;
  const acq = task.acquisition;
  return (
    <div>
      <h5>Acquisition</h5>
      {acq && <div>{acq.detector}</div>}
      {acq && <div>×{acq.magnification}</div>}
      {acq && <div>Fov: {acq.fov[0]} mm</div>}
      {acq && <div>Px: {acq.pixelSize[0]} µm</div>}
      <h5>ROI</h5>
      {task.roi && <div>X: {roi.center.x} mm</div>}
      {task.roi && <div>Y: {roi.center.y} mm</div>}
      {task.roi && <div>Z: {roi.centerZ} mm</div>}
      {task.roi && roi.width && <div>Width: {roi.width} mm</div>}
      {task.roi && roi.height && <div>Height: {roi.height} mm</div>}
    </div>
  );
}

function ScanInfo({
  task,
  actions,
}: {
  task: TomoScanTask;
  actions: TomoScanTaskActions;
}) {
  return (
    <>
      <td>{task.id}</td>
      <td>{task.kind}</td>
      <td>{task.estimationTime}</td>
      <td>
        <div className="d-flex">
          <OverlayTrigger
            placement="left"
            overlay={
              <Tooltip id="button-tooltip">
                <ScanInfoTooltipContent task={task} />
              </Tooltip>
            }
          >
            <Button
              size="sm"
              onClick={() => {
                console.warn('not yet implemented');
              }}
            >
              <i className="fa fa-fw fa-info" />
            </Button>
          </OverlayTrigger>
          <Button
            size="sm"
            variant="danger"
            onClick={(event: MouseEvent) => {
              event.preventDefault();
              event.stopPropagation();
              actions.removeScanTask(task.id);
            }}
          >
            <i className="fa fa-fw fa-trash" />
          </Button>
        </div>
      </td>
    </>
  );
}

export default function TomoScanList(props: Props) {
  const { tasks, actions } = props;
  function handleDrop(droppedItem: any) {
    if (!droppedItem.destination) {
      return;
    }
    actions.reorderScanTask(
      droppedItem.source.index,
      droppedItem.destination.index
    );
  }

  function getTaskClass(task: TomoScanTask) {
    if (task.active) {
      return 'table-info';
    }
    return 'bg-light';
  }

  return (
    <Container>
      <div>
        <h4>Scan planning</h4>
      </div>
      <Table responsive>
        <thead>
          <tr>
            <th>#</th>
            <th>Kind</th>
            <th>Estimation</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <DragDropContext onDragEnd={handleDrop}>
          <Droppable droppableId="table">
            {(provided) => (
              <tbody {...provided.droppableProps} ref={provided.innerRef}>
                {tasks.map((item, index) => (
                  <Draggable key={item.id} draggableId={item.id} index={index}>
                    {(provided) => (
                      <tr
                        className={getTaskClass(item)}
                        ref={provided.innerRef}
                        {...provided.dragHandleProps}
                        {...provided.draggableProps}
                        onClick={(event: MouseEvent) => {
                          event.preventDefault();
                          event.stopPropagation();
                          actions.activateScanTask(item.id);
                        }}
                      >
                        <ScanInfo task={item} actions={actions} />
                      </tr>
                    )}
                  </Draggable>
                ))}
                {provided.placeholder}
              </tbody>
            )}
          </Droppable>
        </DragDropContext>
      </Table>
    </Container>
  );
}
