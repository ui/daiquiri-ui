import { useEffect, useState, useMemo } from 'react';
import type { BeamlineNotification } from 'types/Events';

/**
 * Try to get the last autoprocprogramid from events.
 *
 * The specified `type` is supposed to be fixed.
 *
 * FIXME: this have to be done in the provider. Here we basically lose what we need, which is to know if a job have finished.
 * FIXME: it should be done with a LRU cache
 */
export function useLastAutoProcProgramId(props: {
  type: string;
  datacollectionid?: number;
  datacollectiongroupid?: number;
  event: BeamlineNotification | undefined;
}): number | undefined {
  const { event } = props;
  const [last, setLast] = useState<Record<string, number>>({});

  useEffect(() => {
    if (event) {
      if (event.type === props.type) {
        const { datacollectionid, autoprocprogramid } = event;
        console.log('event', event);
        if (datacollectionid !== undefined && autoprocprogramid !== undefined) {
          setLast((state) => {
            return {
              ...state,
              [`${datacollectionid}`]: autoprocprogramid,
            };
          });
        }
      }
    }
  }, [props.event]);

  return useMemo(() => {
    if (props.datacollectionid === undefined) {
      return undefined;
    }
    const result = last[`${props.datacollectionid}`];
    if (result !== undefined) {
      // FIXME: the datacollectiongroupid is not part of the event
      //        so we have to cheat
      if (last[`group${props.datacollectiongroupid}`] !== result) {
        setLast((state) => {
          return {
            ...state,
            [`group${props.datacollectiongroupid}`]: result,
          };
        });
      }
    }
    return result ?? last[`group${props.datacollectiongroupid}`];
  }, [last, props.datacollectionid]);
}
