import type { TomoConfigHardware } from 'connect/tomo/utils';
import { posInMM, fovInMM } from '../utils/geometry';
import { Circle } from 'components/h5web/items/shapes/Circle';
import Label from 'components/h5web/items/Label';
import { Vector2 } from 'three';

export function ActualScanRegion(props: { sampleStage: TomoConfigHardware }) {
  const { sampleStage } = props;
  const sampu = sampleStage.sampu ? posInMM(sampleStage.sampu) : null;
  const sampv = sampleStage.sampv ? posInMM(sampleStage.sampv) : null;
  const { tomoDetector, detector } = sampleStage;
  if (
    tomoDetector === null ||
    detector === null ||
    sampu === null ||
    sampv === null
  ) {
    return <></>;
  }
  const fov = fovInMM(tomoDetector, detector);
  if (fov === null) {
    return <></>;
  }
  const center = new Vector2(sampv, sampu);
  const fullRadius = fov[0] * 0.5;
  const halfRadius = fov[0];

  function computeActualRadius(): number | null {
    const sy = sampleStage.sy ? posInMM(sampleStage.sy) : null;
    if (sy === null || fov === null) {
      return null;
    }
    return fov[0] * 0.5 + Math.abs(sy);
  }

  const actualRadius = computeActualRadius();
  return (
    <>
      <Circle
        center={[center.x, center.y]}
        radius={fullRadius}
        color="#000000"
        gapColor="#FF0000"
        opacity={0.5}
        dashSize={5}
        gapSize={5}
        lineWidth={1}
      />
      <Label
        datapos={[center.x - fullRadius * 0.95, center.y - fullRadius * 0.35]}
        color="#FF0000"
        text="Full"
      />
      <Circle
        center={[center.x, center.y]}
        radius={halfRadius}
        color="#000000"
        gapColor="#FF0000"
        opacity={0.5}
        dashSize={5}
        gapSize={5}
        lineWidth={1}
      />
      <Label
        datapos={[center.x - halfRadius * 0.95, center.y - halfRadius * 0.35]}
        color="#FF0000"
        text="Half"
      />
      {actualRadius !== null && (
        <>
          <Circle
            center={[center.x, center.y]}
            radius={actualRadius}
            color="#000000"
            gapColor="#FF0000"
            dashSize={5}
            gapSize={5}
            lineWidth={1}
          />
          <Label
            datapos={[center.x - actualRadius, center.y - actualRadius * 0.175]}
            color="#FF0000"
            text="Actual region"
          />
        </>
      )}
    </>
  );
}
