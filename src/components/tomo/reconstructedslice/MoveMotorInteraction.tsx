import { useCallback } from 'react';
import type { CanvasEvent } from '@h5web/lib';
import { useLinearVisCanvas } from 'components/h5web/LinearVisCanvas2';
import PointerClick from 'components/h5web/SelectionPoint';
import type { RequestMoveSampleAxisAtPosition } from './models';
import debug from 'debug';

const logger = debug(
  'daiquiri.components.tomo.reconstructedslice.MoveMotorInteraction'
);

/**
 * Plot component handling mouse interaction used to move the motors
 */
export function MoveMotorInteraction(props: {
  disabled: boolean;
  requestMoveSampleAxisAtPosition: RequestMoveSampleAxisAtPosition;
}) {
  const linearVisCanvas = useLinearVisCanvas();
  const { mouseMode } = linearVisCanvas ?? {};

  const { disabled, requestMoveSampleAxisAtPosition } = props;
  const onClick = useCallback(
    (evt: CanvasEvent<MouseEvent>) => {
      const y = evt.dataPt.x;
      const x = evt.dataPt.y;
      requestMoveSampleAxisAtPosition(x, y);
    },
    [requestMoveSampleAxisAtPosition]
  );

  if (disabled || mouseMode !== 'move-sample-to-axis') {
    logger('MoveMotorInteraction disabled.');
    return <></>;
  }
  logger('MoveMotorInteraction enabled.');
  return <PointerClick onClick={onClick} />;
}
