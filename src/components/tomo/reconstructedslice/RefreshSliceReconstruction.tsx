import { Button } from 'react-bootstrap';
import type { RequestSliceReconstruction } from '../utils/types';

export function RefreshSliceReconstruction(props: {
  datacollectionid: number | undefined;
  actions: { requestSliceReconstruction: RequestSliceReconstruction };
}) {
  function invalidate() {
    if (props.datacollectionid !== undefined) {
      props.actions.requestSliceReconstruction({
        datacollectionid: props.datacollectionid,
      });
    }
  }

  return (
    <Button
      variant="secondary"
      title="Recompute the slice from the sinogram"
      disabled={props.datacollectionid === null}
      onClick={invalidate}
    >
      <i className="fa fa-rotate-right fa-solid" />
    </Button>
  );
}
