import Qty from 'js-quantities';
import type { NdArray, TypedArray } from 'ndarray';
import type ndarray from 'ndarray';
import {
  assertDisplayableNdArray,
  assertNdArrayTypedArray,
} from 'helpers/ndarray';
import type { H5groveGroup } from 'services/H5GroveService';
import {
  readAsNumber,
  readAsStringList,
  readAsScalarQuantity,
  readAsString,
} from 'services/H5GroveService';
import type {
  ArrayHistogram,
  ArrayStatistics,
} from 'components/h5web/colormap';

export type MouseModes = 'zoom' | 'pan' | 'move-sample-to-axis';

/**
 * Exception returned whenever the parsing of the data do not result on any plot
 */
export class NoPlotCanBeParsed extends Error {}

export type RequestMoveSampleAxisAtPosition = (sx: number, sy: number) => void;

export interface ReconstructedInfo {
  imageArray: ndarray.NdArray<any>;
  sampxPosition: Qty | null;
  sampyPosition: Qty | null;
  syPosition: Qty | null;
  axisPosition: Qty | null;
  samplePixelSize: Qty | null;
  estimatedAxisPosition: Qty | null;
  deltaBeta: number | null;
  backend: string | null;
  corBackend: string | null;
  histogram: ArrayHistogram | undefined;
  stats: ArrayStatistics | undefined;
}

/**
 * Extra expected info from NXdata group structure
 */
export function extractInfo(
  group: H5groveGroup | undefined
): ReconstructedInfo | null {
  if (!group) {
    return null;
  }
  const signal = readAsStringList(group.attrs.signal);
  if (!signal) {
    throw new NoPlotCanBeParsed('No signal found in NXdata');
  }
  if (signal.length !== 1) {
    throw new NoPlotCanBeParsed(
      `Expect one and only one signal, found ${signal.length}`
    );
  }

  function getHistogram(
    group: H5groveGroup
  ): { values: NdArray<TypedArray>; bins: NdArray<TypedArray> } | undefined {
    try {
      const hgroup = group.groups.histo;
      const values = hgroup.datasets.counts;
      const bins = hgroup.datasets.bin_edges;
      const valuesArray = values.data;
      const binsArray = bins.data;
      assertNdArrayTypedArray(valuesArray);
      assertNdArrayTypedArray(binsArray);
      return { values: valuesArray, bins: binsArray };
    } catch (error) {
      console.error(error);
      return undefined;
    }
  }

  function getStatistics(group: H5groveGroup): ArrayStatistics | undefined {
    try {
      const statgroup = group.groups.stats;
      if (statgroup === undefined) {
        return undefined;
      }
      return {
        min: readAsNumber(statgroup.datasets.min),
        max: readAsNumber(statgroup.datasets.max),
        minPositive: readAsNumber(statgroup.datasets.min_positive),
        mean: readAsNumber(statgroup.datasets.mean),
        std: readAsNumber(statgroup.datasets.std),
      };
    } catch {
      return undefined;
    }
  }

  const image = group.datasets[signal[0]];
  if (!image) {
    throw new NoPlotCanBeParsed(
      `Dataset from signal ${signal[0]} does not exists`
    );
  }

  if (image.shape.length !== 2) {
    throw new NoPlotCanBeParsed(
      `Dataset from signal ${signal[0]} must be 2 ndim (found ${image.shape.length})`
    );
  }

  const imageArray = image.data;
  assertDisplayableNdArray(imageArray);
  const sampxPosition = readAsScalarQuantity(group.datasets.sample_x_axis);
  const sampyPosition = readAsScalarQuantity(group.datasets.sample_y_axis);
  const syPosition = readAsScalarQuantity(group.datasets.y_axis);
  const samplePixelSize = readAsScalarQuantity(
    group.datasets.sample_pixel_size
  );
  const axisPosition =
    readAsScalarQuantity(group.datasets.used_axis_position) || new Qty(0, 'px');
  const estimatedAxisPosition =
    readAsScalarQuantity(group.datasets.estimated_axis_position) ||
    new Qty(0, 'px');
  const deltaBeta = readAsNumber(group.datasets.delta_beta);
  const backend = readAsString(group.datasets.backend);
  const corBackend = readAsString(group.datasets.cor_backend);

  return {
    histogram: getHistogram(group),
    imageArray,
    sampxPosition,
    sampyPosition,
    syPosition,
    axisPosition,
    samplePixelSize,
    estimatedAxisPosition,
    deltaBeta,
    backend,
    corBackend,
    stats: getStatistics(group),
  };
}
