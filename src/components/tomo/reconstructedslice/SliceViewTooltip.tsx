import { Col, Container, Row } from 'react-bootstrap';

export function SliceViewTooltip(props: {
  px: number;
  py: number;
  pixel: number | undefined;
  inMotorSpace: boolean;
}) {
  /**
   * Apply `toFixed` only if the input is a floating point.
   */
  function toFixedIfNeeded(val: number, digits: number): string {
    if (Math.round(val) === val) {
      return val.toFixed(0);
    }
    return val.toFixed(digits);
  }

  const N_DIGITS = 4;
  return (
    <Container style={{ width: '150px' }}>
      <Row>
        <Col xs={3}>u</Col>
        <Col xs={8}>{props.py.toFixed(N_DIGITS)}</Col>
        <Col xs={1}>{props.inMotorSpace ? 'mm' : 'px'}</Col>
      </Row>
      <Row>
        <Col xs={3}>v</Col>
        <Col xs={8}>{props.px.toFixed(N_DIGITS)}</Col>
        <Col xs={1}>{props.inMotorSpace ? 'mm' : 'px'}</Col>
      </Row>
      {props.pixel && (
        <Row>
          <Col xs={3}>pixel</Col>
          <Col xs={8}>{toFixedIfNeeded(props.pixel, N_DIGITS)}</Col>
          <Col xs={1} />
        </Row>
      )}
    </Container>
  );
}
