import { Alert } from 'react-bootstrap';
import type { H5groveResult } from 'services/H5GroveService';

export function SliceReconstructionMessage(props: {
  datacollectionid: number | undefined;
  autoprocprogramid: number | undefined;
  fetchedResult: H5groveResult;
  parsingError: string | undefined;
}) {
  const { fetchedResult } = props;
  if (props.datacollectionid === undefined) {
    return <Alert variant="secondary">Not yet datacollection</Alert>;
  }
  if (
    props.autoprocprogramid === undefined &&
    fetchedResult.group === undefined
  ) {
    // FIXME: autoprocprogramid === undefined still mean to reach the last job
    // So it doesn't mean the is not yet job
    return (
      <Alert variant="secondary">
        Not yet reconstruction for datacollection {props.datacollectionid}
      </Alert>
    );
  }
  if (fetchedResult.loading) {
    return <Alert variant="warning">Waiting for data</Alert>;
  }
  if (fetchedResult.error) {
    return (
      <Alert variant="danger">
        <p>Error during fetching:</p>
        <p>{fetchedResult.error}</p>
      </Alert>
    );
  }
  if (props.parsingError) {
    return (
      <Alert variant="danger">
        <p>Data format unsupported:</p>
        <p>{props.parsingError}</p>
      </Alert>
    );
  }
  return <></>;
}
