import { useEffect, useRef, useMemo } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { HardwareInputNumber } from '@esrf/daiquiri-lib';
import Toolbar, { Separator } from 'components/common/Toolbar';
import AutoProcessingState from 'connect/dc/AutoProcessingState';
import {
  requestMoveSampleStage,
  requestSliceReconstruction,
} from 'services/TomoService';
import Qty from 'js-quantities';
import type { LinearVisCanvasRef } from 'components/h5web/LinearVisCanvas2';
import DefaultMouseModeOptions from 'components/h5web/bootstrap/DefaultMouseModeOptions';
import debug from 'debug';
import type {
  TomoDataCollectionMeta,
  TomoDataCollectionMetaActions,
} from 'types/Tomo';
import HistogramDomainSlider from 'components/h5web/bootstrap/HistogramDomainSlider';
import { useHdf5Tree } from 'services/H5GroveService';
import { useSliceReconstructionConfig } from '../config/SliceReconstruction';
import AutoScaleOption from 'components/h5web/bootstrap/AutoscaleOption';
import { useSyncScaleDomainFromStatistics } from 'components/h5web/colormap';
import DropdownSliceViewConfig from '../sliceview/DropdownSliceViewConfig';
import { useLastNotification, useOperator } from 'components/utils/hooks';
import { useTomoConfigRelations, useTomoSampleStageRefs } from '../utils/hooks';
import { useMouseModeInteraction } from 'components/h5web/UseMouseModeInteraction';
import type { MouseModes, ReconstructedInfo } from './models';
import { extractInfo, NoPlotCanBeParsed } from './models';
import { useLastAutoProcProgramId } from './hooks';
import { SyncCorConfig } from './SyncCorConfig';
import { RefreshSliceReconstruction } from './RefreshSliceReconstruction';
import { SliceReconstructionInfo } from './SliceReconstructionInfo';
import { TomoReconstructedSlicePlot } from './TomoReconstructedSlicePlot';
import { SliceReconstructionMessage } from './SliceReconstructionMessage';

const logger = debug('daiquiri.components.tomo.TomoReconstructedSinogram');

export interface TomoReconstructedSinogramOptions {
  /** Identifier of the tomoconfig hardware object */
  tomoconfig: string;
  /** Event types from celery which have to be monitored */
  events: string[];
  /** Collection kind to monitor */
  datacollectionid: number | 'last';
  /** Data path to retried from the collection */
  uri: string;
}

interface Props {
  /** Datacollection selected by the view */
  datacollectionid?: number;
  datacollectiongroupid?: number;
  actions: TomoDataCollectionMetaActions;
  datacollectionMeta: TomoDataCollectionMeta | undefined;
  options: TomoReconstructedSinogramOptions;
}

export default function TomoReconstructedSinogram(props: Props) {
  const refreshThread = useRef<any>();
  const { actions, datacollectionid, options } = props;
  const { uri, tomoconfig: tomoConfigId, events, ...unknownOptions } = options;
  const operator = useOperator();

  const plotRef = useRef<LinearVisCanvasRef>(null);
  const mouseModeInteraction = useMouseModeInteraction<MouseModes>('zoom');
  const { mouseMode } = mouseModeInteraction;

  const config = useSliceReconstructionConfig();

  const sampleStageRefs = useTomoSampleStageRefs(tomoConfigId ?? '');
  const sampleStage = useTomoConfigRelations(sampleStageRefs);

  const event = useLastNotification(props.options.events);

  function resetZoom() {
    if (plotRef.current) {
      plotRef.current.actions.resetZoom();
    }
  }

  const autoprocprogramid = useLastAutoProcProgramId({
    type: 'tomo-sinogram-reconstruction',
    datacollectionid: props.datacollectionid,
    datacollectiongroupid: props.datacollectiongroupid,
    event: event ?? undefined,
  });

  // NOTE: autoprocprogramid===undefined only means the event dont know the last value
  // a request will undefined will returns the last

  useEffect(() => {
    return () => {
      if (refreshThread.current) clearTimeout(refreshThread.current);
    };
  }, []);

  const fetchedResult = useHdf5Tree({
    datacollectionid: props.datacollectionid,
    autoprocprogramid,
    type: 'processing',
    path: uri,
    supportsFloat16Array: true,
  });

  const [reconstructedInfo, parsingError]: [
    ReconstructedInfo | null,
    string | undefined
  ] = useMemo(() => {
    try {
      return [extractInfo(fetchedResult.group), undefined];
    } catch (error) {
      if (error instanceof NoPlotCanBeParsed) {
        return [null, error.message];
      }
      throw error;
    }
  }, [fetchedResult]);

  function requestMoveSampleAxisAtPosition(newx: number, newy: number) {
    mouseModeInteraction.resetMouseMode();
    if (reconstructedInfo === null) {
      logger('Move cancelled: reconstructedInfo is null');
      return;
    }
    const sampu = Qty(newx, 'mm');
    const sampv = Qty(newy, 'mm');
    requestMoveSampleStage({ sampu, sampv });
  }

  useSyncScaleDomainFromStatistics({
    statistics: reconstructedInfo?.stats,
    ...config,
  });

  return (
    <div
      className="plot2d-container w-100 h-100"
      style={{
        flex: '1 1 0%',
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <Toolbar align="center">
        <SyncCorConfig
          reconstructionInfo={reconstructedInfo}
          datacollectionid={datacollectionid}
          actions={actions}
        />
        <DefaultMouseModeOptions mouseModeInteraction={mouseModeInteraction} />
        <Button
          title="Reset zoom (sample stage overview)"
          variant="secondary"
          onClick={() => {
            resetZoom();
          }}
        >
          <i className="fa fa-expand fa-fw fa-lg" />
        </Button>
        <Separator />
        <Button
          title={
            operator
              ? 'Move the motors to set rotation axis on the sample'
              : 'You have to get the control on the session to move motors'
          }
          disabled={!operator}
          variant={mouseMode === 'move-sample-to-axis' ? 'danger' : 'secondary'}
          onClick={() => {
            mouseModeInteraction.setOrResetMouseMode('move-sample-to-axis');
          }}
        >
          <i className="fa fam-arrow-h-over-rot fa-fw fa-lg" />
        </Button>
        <AutoProcessingState
          name="Worker"
          description="Automatic slice reconstruction from sinogram"
          datacollectionid={props.datacollectionid}
          programs="tomo-sinogram-reconstruction"
          providers={{
            metadata: {
              autoprocprograms: { namespace: 'tomosinogramreconstruction' },
            },
          }}
        />
        <RefreshSliceReconstruction
          datacollectionid={props.datacollectionid}
          actions={{ requestSliceReconstruction }}
        />
      </Toolbar>
      <Toolbar align="center">
        <AutoScaleOption
          disabled={reconstructedInfo?.stats === undefined}
          {...config}
        />
        <HistogramDomainSlider
          disabled={reconstructedInfo === undefined}
          histogram={reconstructedInfo?.histogram}
          {...config}
        />
        <SliceReconstructionInfo
          fetchedResult={fetchedResult}
          reconstructedInfo={reconstructedInfo}
        />
        <DropdownSliceViewConfig config={config} />
      </Toolbar>
      <Container>
        <Row className="g-0 align-items-center">
          <Col
            xs="3"
            title="Location of the rotation axis from the left side of the sinogram (in pixel)"
          >
            Axis position:
          </Col>
          <Col xs="9">
            <HardwareInputNumber
              hardwareValue={reconstructedInfo?.axisPosition?.scalar ?? 0}
              hardwareIsDisabled={reconstructedInfo === null}
              onMoveRequested={(value: number) => {
                if (props.datacollectionid !== undefined) {
                  requestSliceReconstruction({
                    datacollectionid: props.datacollectionid,
                    axisposition: value,
                    deltabeta: config.deltaBeta,
                  });
                }
                return null;
              }}
            />
          </Col>
        </Row>
        <Row className="g-0 align-items-center">
          <Col xs="3" title="Delta/beta ratio for the Paganin filter">
            Delta/beta:
          </Col>
          <Col xs="9">
            <HardwareInputNumber
              hardwareValue={config.deltaBeta}
              hardwareIsDisabled={false}
              onMoveRequested={(value: number) => {
                config.setDeltaBeta(value);
                if (props.datacollectionid !== undefined) {
                  requestSliceReconstruction({
                    datacollectionid: props.datacollectionid,
                    axisposition: reconstructedInfo?.axisPosition?.scalar,
                    deltabeta: value,
                  });
                }
                return null;
              }}
            />
          </Col>
        </Row>
      </Container>
      <SliceReconstructionMessage
        datacollectionid={props.datacollectionid}
        autoprocprogramid={autoprocprogramid}
        fetchedResult={fetchedResult}
        parsingError={parsingError}
      />
      <TomoReconstructedSlicePlot
        config={config}
        imageColorMap={config.colorMap}
        imageInvertColorMap={config.invertColorMap}
        imageDomain={config.scaleDomain}
        imageScaleType={config.scaleType}
        reconstructedInfo={reconstructedInfo}
        className="flex-grow-1"
        sampleStage={sampleStage}
        mouseMode={mouseMode}
        plotRef={plotRef}
        requestMoveSampleAxisAtPosition={requestMoveSampleAxisAtPosition}
      />
    </div>
  );
}
