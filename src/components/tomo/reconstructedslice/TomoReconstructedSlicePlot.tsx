import { useRef } from 'react';
import type { Domain, ScaleType, ColorMap } from '@h5web/lib';
import { TooltipMesh } from '@h5web/lib';
import type { MotorSchema } from '@esrf/daiquiri-lib';
import type { TomoConfigHardware } from 'connect/tomo/utils';
import type { LinearVisCanvasRef } from 'components/h5web/LinearVisCanvas2';
import { LinearVisCanvas } from 'components/h5web/LinearVisCanvas2';
import Cross from 'components/h5web/items/shapes/Cross';
import ImageMesh from 'components/h5web/items/ImageMesh';
import { SceneScale } from 'components/h5web/items/SceneScale';
import type { SliceReconstructionConfig } from '../config/SliceReconstruction';
import type { ImageMeshRef } from 'components/h5web/items/ImageMesh';
import type {
  ReconstructedInfo,
  RequestMoveSampleAxisAtPosition,
} from './models';
import { useOperator } from 'components/utils/hooks';
import { SliceViewTooltip } from './SliceViewTooltip';
import { ActualScanRegion } from './ActualScanRegion';
import { ActualAxisPosition } from './ActualAxisPosition';
import { ActualBeamDirection } from './ActualBeamDirection';
import { MoveMotorInteraction } from './MoveMotorInteraction';
import { isMotorClockWise } from 'components/hardware/motor/utils';

export function TomoReconstructedSlicePlot(props: {
  config: SliceReconstructionConfig;
  reconstructedInfo: ReconstructedInfo | null;
  className?: string;
  sampleStage: TomoConfigHardware;
  mouseMode: string;
  plotRef: React.RefObject<LinearVisCanvasRef>;
  imageDomain: Domain;
  imageScaleType: ScaleType;
  imageColorMap: ColorMap;
  imageInvertColorMap: boolean;
  requestMoveSampleAxisAtPosition: RequestMoveSampleAxisAtPosition;
}) {
  const { sampleStage, mouseMode } = props;
  const reconstructedArray = props.reconstructedInfo?.imageArray;
  const imageRef = useRef<ImageMeshRef>(null);
  const operator = useOperator();

  if (!props.reconstructedInfo || reconstructedArray === undefined) {
    return <></>;
  }

  // FIXME: For now we assume the reconstruction was done with this somega state
  //        but this orientation should be provided by the reconstructed data
  const isClockWise =
    sampleStage.somega !== null ? isMotorClockWise(sampleStage.somega) : true;
  const sign = isClockWise ? 1 : -1;

  function computeMotorRange(info: ReconstructedInfo): {
    inMotorSpace: boolean;
    xRange: Domain;
    yRange: Domain;
    center: [number, number, number];
    scale: [number, number, number];
    width: number;
    height: number;
  } {
    const width = reconstructedArray?.shape[1] ?? 0;
    const height = reconstructedArray?.shape[0] ?? 0;
    if (
      info.samplePixelSize === null ||
      info.sampxPosition === null ||
      info.sampyPosition === null ||
      info.imageArray === null
    ) {
      return {
        inMotorSpace: false,
        xRange: [0, height],
        yRange: [0, width],
        center: [width / 2, height / 2, 0],
        scale: [1, 1, 1],
        height,
        width,
      };
    }
    const px = info.samplePixelSize.to('mm').scalar;
    const sampx = info.sampxPosition.to('mm').scalar;
    const sampy = info.sampyPosition.to('mm').scalar;
    const halfWidth = info.imageArray.shape[1] * 0.5;
    const halfHeight = info.imageArray.shape[0] * 0.5;

    return {
      inMotorSpace: true,
      xRange: [sampy - halfWidth * px, sampy + halfWidth * px],
      yRange: [sampx - halfHeight * px, sampx + halfHeight * px],
      center: [sampy, sampx, 0],
      scale: [-px, -sign * px, 1],
      width: halfWidth * px * 2,
      height: halfHeight * px * 2,
    };
  }
  const motorRange = computeMotorRange(props.reconstructedInfo);

  function motorTitle(motor: MotorSchema | null, alternative: string): string {
    if (motor === null || !motorRange.inMotorSpace) {
      return `${alternative} (px)`;
    }
    return `${motor.alias ?? motor.name} (mm)`;
  }

  return (
    <div
      style={{
        flex: '1 1 auto',
        display: 'flex',
        margin: 0,
        minHeight: 0,
      }}
      className={props.className}
    >
      <LinearVisCanvas
        plotRef={props.plotRef}
        abscissaConfig={{
          visDomain: motorRange.xRange,
          label: motorTitle(sampleStage.sampv, 'lateral-axis'),
          flip: true,
        }}
        ordinateConfig={{
          visDomain: motorRange.yRange,
          label: motorTitle(sampleStage.sampu, 'x-axis'),
        }}
        mouseMode={mouseMode}
        aspect="equal"
        showAxes={props.config.displayAxes}
      >
        <ImageMesh
          ref={imageRef}
          values={reconstructedArray}
          domain={props.imageDomain}
          colorMap={props.imageColorMap}
          invertColorMap={props.imageInvertColorMap}
          position={motorRange.center}
          scale={motorRange.scale}
          scaleType={props.imageScaleType}
          size={{
            width: reconstructedArray.shape[1],
            height: reconstructedArray.shape[0],
          }}
        />
        <TooltipMesh
          guides={props.config.crossHair ? 'both' : undefined}
          renderTooltip={(x, y) => {
            const p = imageRef.current?.pick(x, y);
            return (
              <SliceViewTooltip
                px={x}
                py={y}
                pixel={p}
                inMotorSpace={motorRange.inMotorSpace}
              />
            );
          }}
        />
        <Cross
          centerX={motorRange.center[0]}
          centerY={motorRange.center[1]}
          width={motorRange.width}
          height={motorRange.height}
          color="white"
          gapColor="black"
          dashSize={5}
          gapSize={5}
          lineWidth={1}
        />
        {motorRange.inMotorSpace && (
          <>
            <ActualScanRegion sampleStage={sampleStage} />
            <ActualAxisPosition sampleStage={sampleStage} />
            <ActualBeamDirection
              sampleStage={sampleStage}
              viewRange={[motorRange.xRange, motorRange.yRange]}
            />
          </>
        )}
        <MoveMotorInteraction
          disabled={!operator}
          requestMoveSampleAxisAtPosition={
            props.requestMoveSampleAxisAtPosition
          }
        />
        <SceneScale unit={motorRange.inMotorSpace ? 'mm' : 'px'} />
      </LinearVisCanvas>
    </div>
  );
}
