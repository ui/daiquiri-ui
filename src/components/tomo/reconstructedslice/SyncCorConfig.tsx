import { useEffect } from 'react';
import type { TomoDataCollectionMetaActions } from 'types/Tomo';
import type { ReconstructedInfo } from './models';

/**
 * Synchonize with other widgets the estimated center of rotation
 */
export function SyncCorConfig(props: {
  reconstructionInfo: ReconstructedInfo | null;
  datacollectionid?: number;
  actions: TomoDataCollectionMetaActions;
}) {
  const { datacollectionid, actions } = props;
  const estimatedCor =
    props.reconstructionInfo?.estimatedAxisPosition?.scalar ?? undefined;
  const actualCor = props.reconstructionInfo?.axisPosition?.scalar ?? undefined;

  useEffect(() => {
    if (estimatedCor !== undefined) {
      if (datacollectionid !== undefined) {
        actions?.updateDataCollectionMeta(datacollectionid, {
          estimatedCor,
        });
      }
    }
  }, [estimatedCor]);

  useEffect(() => {
    if (estimatedCor !== undefined) {
      if (datacollectionid !== undefined) {
        actions?.updateDataCollectionMeta(datacollectionid, {
          actualCor,
          requestedCor: null,
        });
      }
    }
  }, [actualCor]);
  return <></>;
}
