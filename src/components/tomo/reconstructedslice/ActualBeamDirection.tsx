import type { Domain } from '@h5web/lib';
import type { TomoConfigHardware } from 'connect/tomo/utils';
import { posInMM, cwPosInRad } from '../utils/geometry';
import { ArrowMarker } from 'components/h5web/items/shapes/ArrowMarker';
import Label from 'components/h5web/items/Label';
import { Vector2 } from 'three';

export function ActualBeamDirection(props: {
  sampleStage: TomoConfigHardware;
  viewRange: [Domain, Domain];
}) {
  const { sampleStage } = props;
  const sampu = sampleStage.sampu ? posInMM(sampleStage.sampu) : null;
  const sampv = sampleStage.sampv ? posInMM(sampleStage.sampv) : null;
  const cwAngleRad = sampleStage.somega
    ? -cwPosInRad(sampleStage.somega)
    : null;
  if (cwAngleRad === null || sampu === null || sampv === null) {
    return <></>;
  }
  const cwAngleDeg = (cwAngleRad / Math.PI) * 180;
  const dataDist =
    (props.viewRange[0][1] - props.viewRange[0][0]) * 0.5 +
    (props.viewRange[1][1] - props.viewRange[1][0]) * 0.5;
  const direction = new Vector2(0, 1).rotateAround(
    new Vector2(0, 0),
    cwAngleRad
  );
  const norm = new Vector2(-direction.y, direction.x).multiplyScalar(
    0.3 * dataDist
  );
  const center = new Vector2(sampv, sampu);
  const midPoint = center
    .clone()
    .add(direction.clone().multiplyScalar(0.375 * dataDist));
  const centerX = sampv;
  const centerY = sampu;
  const p1 = midPoint.clone().add(norm);
  const p2 = midPoint;
  const p3 = midPoint.clone().sub(norm);
  return (
    <>
      <ArrowMarker
        x={p1.x}
        y={p1.y}
        sizeInScreen={80}
        angle={90 - cwAngleDeg}
        color="red"
        lineWidth={3}
      />
      <ArrowMarker
        x={p2.x}
        y={p2.y}
        sizeInScreen={80}
        angle={90 - cwAngleDeg}
        color="red"
        lineWidth={3}
      />
      <ArrowMarker
        x={p3.x}
        y={p3.y}
        sizeInScreen={80}
        angle={90 - cwAngleDeg}
        color="red"
        lineWidth={3}
        /* headSize={0.05 * dataDist}*/
      />
      <Label
        datapos={[p2.x, p2.y]}
        color="#FF0000"
        text="Beam"
        anchor="bottom"
      />
    </>
  );
}
