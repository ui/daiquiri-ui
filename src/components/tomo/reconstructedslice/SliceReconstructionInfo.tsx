import { Col, Container, Popover, Row } from 'react-bootstrap';
import { Button, OverlayTrigger } from 'react-bootstrap';
import type { H5groveResult } from 'services/H5GroveService';
import type { ReconstructedInfo } from './models';

export function SliceReconstructionInfo(props: {
  fetchedResult: H5groveResult;
  reconstructedInfo: ReconstructedInfo | null;
}) {
  const { fetchedResult, reconstructedInfo } = props;

  function popup(props: any) {
    if (reconstructedInfo === null) {
      return (
        <Popover id="reconstructed-slice-info" {...props}>
          <Popover.Header as="h3">Displayed data info</Popover.Header>
          <Popover.Body>No data</Popover.Body>
        </Popover>
      );
    }
    return (
      <Popover id="reconstructed-slice-info" {...props}>
        <Popover.Header as="h3">Displayed data info</Popover.Header>
        <Popover.Body>
          <Container style={{ width: '17em' }}>
            {fetchedResult.request && (
              <>
                <Row className="g-0">
                  <Col>Collection ID</Col>
                  <Col>{fetchedResult?.request?.datacollectionid}</Col>
                </Row>
                <Row className="g-0">
                  <Col>Program ID</Col>
                  <Col>
                    {fetchedResult?.request?.autoprocprogramid ?? 'last'}
                  </Col>
                </Row>
              </>
            )}
            <Row className="g-0">
              <Col>Backend</Col>
              <Col>{reconstructedInfo?.backend}</Col>
            </Row>
            <Row className="g-0">
              <Col>COR backend</Col>
              <Col>{reconstructedInfo?.corBackend}</Col>
            </Row>
            <Row className="g-0">
              <Col>Axis position</Col>
              <Col>{reconstructedInfo.axisPosition?.scalar.toFixed(2)} px</Col>
            </Row>
            <Row className="g-0">
              <Col>Delta/beta</Col>
              <Col>{reconstructedInfo.deltaBeta?.toFixed(2)}</Col>
            </Row>
          </Container>
        </Popover.Body>
      </Popover>
    );
  }

  return (
    <OverlayTrigger
      trigger={['hover', 'focus']}
      key="arg"
      placement="bottom"
      overlay={popup}
    >
      <Button variant="secondary" disabled={reconstructedInfo === null}>
        <i className="fa-solid fa-info fa-fw fa-lg" />
      </Button>
    </OverlayTrigger>
  );
}
