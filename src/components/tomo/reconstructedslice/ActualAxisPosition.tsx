import type { TomoConfigHardware } from 'connect/tomo/utils';
import { posInMM } from '../utils/geometry';
import Label from 'components/h5web/items/Label';
import { CrossMarker } from 'components/h5web/items/shapes/CrossMarker';

export function ActualAxisPosition(props: { sampleStage: TomoConfigHardware }) {
  const { sampleStage } = props;
  const sampu = sampleStage.sampu ? posInMM(sampleStage.sampu) : null;
  const sampv = sampleStage.sampv ? posInMM(sampleStage.sampv) : null;
  const sy = sampleStage.sy ? posInMM(sampleStage.sy) : null;
  if (sy === null || sampu === null || sampv === null) {
    return <></>;
  }
  return (
    <>
      <CrossMarker
        x={sampv}
        y={sampu}
        color="red"
        sizeInScreen={30}
        lineWidth={2.5}
      />
      <Label
        datapos={[sampv, sampu]}
        color="#FF0000"
        anchor="top-right"
        text="Actual axis position"
      />
    </>
  );
}
