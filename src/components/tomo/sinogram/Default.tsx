import { useCallback, useEffect, useRef, useState } from 'react';
import { Alert, Button } from 'react-bootstrap';
import SinogramPlot from './SinogramPlot';
import type { RenderMode, Sinogram } from './models';
import RenderModeDropdown from './RenderModeDropdown';
import AxisRotationPositionInteraction from './AxisRotationPositionInteraction';
import RotationPosition from './RotationPosition';
import type { MouseModes } from './models';
import debug from 'debug';
import type { LinearVisCanvasRef } from '../../h5web/LinearVisCanvas2';
import DefaultMouseModeOptions from '../../h5web/bootstrap/DefaultMouseModeOptions';
import Toolbar, { Separator } from '../../common/Toolbar';
import { useScanData } from 'services/ScanDataService';
import { requestSliceReconstruction } from 'services/TomoService';
import type {
  TomoDataCollectionMeta,
  TomoDataCollectionMetaActions,
} from '../../../types/Tomo';
import { useSinogramViewConfig } from '../config/SinogramView';
import DropdownSinogramViewConfig from './DropdownSinogramViewConfig';
import ndarray from 'ndarray';
import { useOperator } from '../../utils/hooks';
import { useTomoConfigRelations, useTomoSampleStageRefs } from '../utils/hooks';
import { useMouseModeInteraction } from '../../h5web/UseMouseModeInteraction';

const logger = debug('daiquiri.components.tomo.TomoSinogram');

export interface TomoSinogramOptions {
  /** Identifier of the tomoconfig hardware object */
  tomoconfig: string;
  /** Collection kind to monitor */
  datacollectionid: number | 'last';
}

interface Props {
  actions: TomoDataCollectionMetaActions;
  selectedScan: number | null;
  sinogram: Sinogram | undefined;
  datacollectionid: number;
  datacollectionMeta: TomoDataCollectionMeta | undefined;
  options: TomoSinogramOptions;
}

export default function TomoSinogram(props: Props) {
  const {
    sinogram,
    selectedScan: scanId,
    datacollectionid,
    datacollectionMeta,
    options,
  } = props;
  const { tomoconfig: tomoConfigId } = options;

  const actions = useRef(props.actions);
  const viewConfig = useSinogramViewConfig();
  const nbPoints = viewConfig.displayedSinogram
    ? sinogram?.actualnbpoints ?? 0
    : 0;
  const [renderMode, setRenderMode] = useState<RenderMode>('solid');
  const operator = useOperator();

  const sampleStageRefs = useTomoSampleStageRefs(tomoConfigId ?? '');
  const sampleStage = useTomoConfigRelations(sampleStageRefs);

  useEffect(() => {
    if (datacollectionid === undefined) {
      actions?.current.updateDataCollectionMeta(datacollectionid, {
        estimatedCor: null,
        requestedCor: null,
        actualCor: null,
      });
    }
  }, [datacollectionid]);

  const sinogramResult = useScanData({
    scanId: sinogram ? scanId ?? undefined : undefined,
    channelNames: ['sinogram', 'rotation', 'translation'],
    start: 0,
    stop: nbPoints,
  });

  const plotRef = useRef<LinearVisCanvasRef>(null);
  const mouseModeInteraction = useMouseModeInteraction<MouseModes>('zoom');
  const { mouseMode } = mouseModeInteraction;

  const empty = ndarray(new Float32Array([]), [0]);
  const xdata = sinogramResult.data.translation;
  const ydata = sinogramResult.data.rotation;
  const vdata = sinogramResult.data.sinogram;

  function getMessage(): { danger?: string; warning?: string; info?: string } {
    if (scanId === null) {
      return { info: 'No scan' };
    }

    if (!sinogram) {
      return { info: 'No sinogram defined' };
    }

    if (!xdata || !ydata || !vdata) {
      if (sinogramResult.loading) {
        return { warning: 'Waiting for data...' };
      }
      if (!xdata) {
        return { danger: 'Data for x-axis are missing' };
      }
      if (!ydata) {
        return { danger: 'Data for y-axis are missing' };
      }
      if (!vdata) {
        return { danger: 'Data for intensity are missing' };
      }
    }
    const dataLength = Math.min(xdata.size, ydata.size, vdata.size);

    if (dataLength === 0) {
      return { warning: 'Waiting for data...' };
    }
    return {};
  }

  const message = getMessage();
  const detectorWidth = props.sinogram?.translationrange?.[1];

  const setRequestedCor = useCallback(
    (requestedCor: number, intermediate?: boolean) => {
      if (!intermediate && props.datacollectionid !== null) {
        requestSliceReconstruction({
          datacollectionid: props.datacollectionid,
          axisposition: requestedCor,
        });
      }
      actions?.current.updateDataCollectionMeta(datacollectionid, {
        requestedCor,
      });
    },
    [datacollectionid]
  );

  return (
    <div
      className="plot2d-container w-100 h-100"
      style={{
        flex: '1 1 0%',
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <Toolbar align="center">
        <DefaultMouseModeOptions mouseModeInteraction={mouseModeInteraction} />
        <Button
          title="Reset zoom (sample stage overview)"
          variant="secondary"
          onClick={() => {
            plotRef?.current?.actions.resetZoom();
          }}
        >
          <i className="fa fa-expand fa-fw fa-lg" />
        </Button>
        <Separator />
        <Button
          title={
            operator
              ? 'Select another center of rotation for the slice reconstruction'
              : 'You have to get the control on the session to set the center of rotation'
          }
          disabled={!operator}
          variant={mouseMode === 'set-axis-rotation' ? 'warning' : 'secondary'}
          onClick={() => {
            mouseModeInteraction.setOrResetMouseMode('set-axis-rotation');
          }}
        >
          <i className="fad fam-tomo-cor fa-fw fa-lg" />
        </Button>
        <Separator />
        <RenderModeDropdown value={renderMode} onSelect={setRenderMode} />
        <DropdownSinogramViewConfig config={viewConfig} />
      </Toolbar>
      {!viewConfig.displayedSinogram && (
        <Alert variant="warning">Sinogram was disabled by user</Alert>
      )}

      {viewConfig.displayedSinogram && (
        <SinogramPlot
          viewConfig={viewConfig}
          message={message}
          mouseMode={mouseMode}
          xdata={xdata ?? empty}
          ydata={ydata ?? empty}
          vdata={vdata ?? empty}
          sinogram={sinogram}
          renderMode={renderMode}
          plotRef={plotRef}
          datacollectionMeta={datacollectionMeta}
          setRequestedCor={setRequestedCor}
        >
          <AxisRotationPositionInteraction
            operator={operator}
            resetMouseMode={mouseModeInteraction.resetMouseMode}
            setRequestedCor={setRequestedCor}
          />
          <RotationPosition
            rotationHardware={sampleStage.somega}
            detectorWidth={detectorWidth}
          />
        </SinogramPlot>
      )}
    </div>
  );
}
