import { Dropdown, DropdownButton } from 'react-bootstrap';
import type { RenderMode } from './models';

interface Props {
  value: RenderMode;
  onSelect: (value: RenderMode) => void;
}

function RenderModeDropdown(props: Props) {
  const { onSelect, value } = props;
  const labels: Record<RenderMode, string> = {
    marker: 'Marker',
    solid: 'Solid',
  };
  const label = labels[value];

  return (
    <DropdownButton
      title={label}
      id="bg-vertical-dropdown-4"
      variant="secondary"
      onSelect={(eventKey: string | null) => {
        if (eventKey !== null) onSelect(eventKey as RenderMode);
      }}
    >
      <Dropdown.Item eventKey="marker">{labels.marker}</Dropdown.Item>
      <Dropdown.Item eventKey="solid">{labels.solid}</Dropdown.Item>
    </DropdownButton>
  );
}

export default RenderModeDropdown;
