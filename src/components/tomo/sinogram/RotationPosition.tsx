import type { MotorSchema } from '@esrf/daiquiri-lib';
import { HSegment } from '../../h5web/items/shapes/HSegment';
import Label from '../../h5web/items/Label';

export default function RotationPosition(props: {
  rotationHardware: MotorSchema | null;
  detectorWidth: number | undefined;
}) {
  const { rotationHardware, detectorWidth } = props;
  const srot = rotationHardware?.properties?.position;
  if (srot === undefined || srot === null || detectorWidth === undefined) {
    return <></>;
  }
  const srotName = rotationHardware?.name;

  return (
    <>
      <HSegment
        y={srot}
        x1={0}
        x2={detectorWidth}
        color="red"
        gapColor="write"
        dashSize={10}
        gapSize={10}
        lineWidth={3}
      />
      <Label
        datapos={[10, srot]}
        color="#FF0000"
        anchor="top"
        vmargin={10}
        text={srotName ?? 'srot'}
      />
    </>
  );
}
