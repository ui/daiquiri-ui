import type { Domain } from '@h5web/lib';

export interface SinogramRanges {
  rotationrange: Domain;
  translationrange: Domain;
}

export interface Sinogram extends SinogramRanges {
  actualnbpoints: number;
  expectednbpoints: number;
  translationaxispoints: number;
  rotationaxispoints: number;
}

export type RenderMode = 'marker' | 'solid';

export type MouseModes = 'pan' | 'zoom' | 'set-axis-rotation';
