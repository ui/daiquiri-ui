import type { MouseModes, RenderMode, Sinogram } from './models';
import { getAxesDomains } from './utils';
import { ColorBar, ScaleType, useDomain } from '@h5web/lib';
import type { LinearVisCanvasRef } from '../../h5web/LinearVisCanvas2';
import { LinearVisCanvas } from '../../h5web/LinearVisCanvas2';
import { VSegment } from '../../h5web/items/shapes/VSegment';
import Label from '../../h5web/items/Label';
import debug from 'debug';
import type { PropsWithChildren } from 'react';
import type { TomoDataCollectionMeta } from '../../../types/Tomo';
import type { NdArray, TypedArray } from 'ndarray';
import { useSlicedArray } from '../../../helpers/ndarray';
import type { SinogramViewConfig } from '../config/SinogramView';
import { LoadingMessage } from '../../h5web/items/LoadingMessage';
import { RegularScatter } from '../../h5web/items/RegularScatter';
import { VLineRoi } from '../../h5web/items/rois/VLineRoi';

const logger = debug('daiquiri.components.tomo.SinogramPlot');

interface Props {
  xdata: NdArray<TypedArray>;
  ydata: NdArray<TypedArray>;
  vdata: NdArray<TypedArray>;
  sinogram?: Sinogram;
  renderMode: RenderMode;
  plotRef: React.RefObject<LinearVisCanvasRef>;
  mouseMode: string;
  operator?: boolean;
  datacollectionMeta: TomoDataCollectionMeta | undefined;
  viewConfig?: SinogramViewConfig;
  message?: {
    danger?: string;
    warning?: string;
    info?: string;
  };
  setRequestedCor: (position: number, dragging: boolean) => void;
}

function CenterOfRotation(props: {
  datacollectionMeta: TomoDataCollectionMeta;
  setRequestedCor: (position: number, dragging: boolean) => void;
}) {
  const { datacollectionMeta } = props;

  const requestedMode =
    datacollectionMeta.requestedCor !== null &&
    datacollectionMeta.requestedCor !== datacollectionMeta.actualCor;

  return (
    <>
      {datacollectionMeta.estimatedCor !== null &&
        datacollectionMeta.estimatedCor !== datacollectionMeta.actualCor && (
          <>
            <VSegment
              x={datacollectionMeta.estimatedCor}
              y1={0}
              y2={360}
              color="#505050"
              gapColor="#909090"
              dashSize={10}
              gapSize={10}
            />
            <Label
              datapos={[datacollectionMeta.estimatedCor, 10]}
              color="#A0A0A0"
              text="Estimated CoR"
            />
          </>
        )}
      {datacollectionMeta.actualCor !== null && (
        <>
          <VLineRoi
            geometry={{
              x:
                datacollectionMeta.requestedCor ?? datacollectionMeta.actualCor,
            }}
            onGeometryChanged={(geometry, dragging) => {
              props.setRequestedCor(geometry.x, dragging);
            }}
          />
          <Label
            datapos={[
              datacollectionMeta.requestedCor ?? datacollectionMeta.actualCor,
              requestedMode ? 40 : 25,
            ]}
            color="#FFFFFF"
            text={requestedMode ? 'Requested CoR' : 'Actual CoR'}
          />
        </>
      )}
      {requestedMode && datacollectionMeta.actualCor !== null && (
        <>
          <VSegment
            x={datacollectionMeta.actualCor}
            y1={0}
            y2={360}
            color="#000000"
            gapColor="#FFFFFF"
            dashSize={10}
            gapSize={10}
          />
          <Label
            datapos={[datacollectionMeta.actualCor, 25]}
            color="#FFFFFF"
            text="Actual CoR"
          />
        </>
      )}
    </>
  );
}

function SinogramPlot(props: PropsWithChildren<Props>) {
  const {
    xdata,
    ydata,
    vdata,
    sinogram,
    renderMode,
    datacollectionMeta,
    mouseMode,
  } = props;

  const { translationrange, rotationrange } = getAxesDomains(
    sinogram,
    renderMode
  );

  const domain = useDomain(vdata);

  const dataLength = Math.min(xdata.size, ydata.size, vdata.size);
  const xdataNorm = useSlicedArray(xdata, 0, dataLength);
  const ydataNorm = useSlicedArray(ydata, 0, dataLength);
  const vdataNorm = useSlicedArray(vdata, 0, dataLength);

  return (
    <div
      style={{
        flex: '1 1 auto',
        display: 'flex',
        margin: 0,
        minHeight: 0,
      }}
    >
      <LinearVisCanvas
        plotRef={props.plotRef}
        showAxes={props.viewConfig?.displayAxes ?? true}
        title="Sinogram"
        abscissaConfig={{
          visDomain: translationrange,
          showGrid: true,
          label: 'Profile',
        }}
        ordinateConfig={{
          visDomain: rotationrange,
          showGrid: true,
          label: 'Rotation',
          flip: true,
        }}
        mouseMode={mouseMode}
      >
        {sinogram && domain && (
          <RegularScatter
            xData={xdataNorm}
            yData={ydataNorm}
            vData={vdataNorm}
            domain={domain}
            colorMap="Viridis"
            xAxisSize={sinogram.translationaxispoints}
            yAxisSize={sinogram.rotationaxispoints}
            xRange={sinogram.translationrange}
            yRange={sinogram.rotationrange}
            renderMode={renderMode}
          />
        )}
        {datacollectionMeta !== undefined && (
          <CenterOfRotation
            setRequestedCor={props.setRequestedCor}
            datacollectionMeta={datacollectionMeta}
          />
        )}
        <LoadingMessage {...props.message} />
        {props.children}
      </LinearVisCanvas>
      {(props.viewConfig?.colorbarVisible ?? true) && (
        <ColorBar
          domain={domain ?? [0, 0]}
          withBounds
          colorMap="Viridis"
          invertColorMap={false}
          scaleType={ScaleType.Linear}
        />
      )}
    </div>
  );
}

export default SinogramPlot;
