import type { Dispatch, PropsWithChildren, SetStateAction } from 'react';
import {
  Dropdown,
  ButtonGroup,
  Button,
  Container,
  Row,
  Col,
} from 'react-bootstrap';
import type { SinogramViewConfig } from '../config/SinogramView';

interface Props {
  variant?: string;
  as?: any;
  config: SinogramViewConfig;
}

function selectedVariant<T>(
  value: T,
  expected: T,
  variantWhenSelected: string | undefined
) {
  if (value !== expected) {
    return 'secondary';
  }
  if (variantWhenSelected) {
    return variantWhenSelected;
  }
  return 'primary';
}

function SelectedButton<T>(
  props: PropsWithChildren<{
    var: T;
    value: T;
    setter: Dispatch<SetStateAction<T>>;
    variantWhenSelected?: string;
  }>
) {
  return (
    <Button
      variant={selectedVariant(
        props.var,
        props.value,
        props.variantWhenSelected
      )}
      onClick={() => {
        props.setter(props.value);
      }}
      className="text-nowrap"
      size="sm"
    >
      {props.children}
    </Button>
  );
}

export default function DropdownSinogramViewConfig(props: Props) {
  const { as = undefined, variant = 'secondary', config } = props;

  return (
    <Dropdown as={as}>
      <Dropdown.Toggle
        id="dropdown-basic"
        variant={variant}
        className="d-flex align-items-center"
      >
        <i className="fa fa-sliders fa-fw fa-lg" />
      </Dropdown.Toggle>
      <Dropdown.Menu className="dropdown-menu-center">
        <div className="ms-1 me-1" style={{ minWidth: '300px' }}>
          <Container>
            <Row>
              <Col className="my-auto">Sinogram</Col>
              <Col xs={7}>
                <ButtonGroup>
                  <SelectedButton
                    var={config.displayedSinogram}
                    value
                    setter={config.setDisplayedSinogram}
                  >
                    Displayed
                  </SelectedButton>
                  <SelectedButton
                    var={config.displayedSinogram}
                    value={false}
                    setter={config.setDisplayedSinogram}
                    variantWhenSelected="warning"
                  >
                    Hide
                  </SelectedButton>
                </ButtonGroup>
              </Col>
            </Row>
            <Row className="mt-1">
              <Col className="my-auto">Display axes</Col>
              <Col xs={7}>
                <ButtonGroup>
                  <SelectedButton
                    var={config.displayAxes}
                    value
                    setter={config.setDisplayAxes}
                  >
                    Yes
                  </SelectedButton>
                  <SelectedButton
                    var={config.displayAxes}
                    value={false}
                    setter={config.setDisplayAxes}
                  >
                    No
                  </SelectedButton>
                </ButtonGroup>
              </Col>
            </Row>
            <Row className="mt-1">
              <Col className="my-auto">Color bar</Col>
              <Col xs={7}>
                <ButtonGroup>
                  <SelectedButton
                    var={config.colorbarVisible}
                    value
                    setter={config.setColorbarVisible}
                  >
                    Displayed
                  </SelectedButton>
                  <SelectedButton
                    var={config.colorbarVisible}
                    value={false}
                    setter={config.setColorbarVisible}
                  >
                    Hide
                  </SelectedButton>
                </ButtonGroup>
              </Col>
            </Row>
          </Container>
        </div>
      </Dropdown.Menu>
    </Dropdown>
  );
}
