import { extendDomain } from '@h5web/lib';
import type { ChannelData } from '../utils/models';
import type { RenderMode, Sinogram, SinogramRanges } from './models';

export function getKeyByValue<T>(object: { [index: string]: T }, value: T) {
  return Object.keys(object).find((key) => object[key] === value);
}

export function getData(
  scanData: Record<string, ChannelData>,
  name: string | undefined
): null | number[] {
  if (!name) {
    return null;
  }
  const d = scanData[name];
  if (!d) {
    return null;
  }
  return d.data;
}

const EXTENSION_FACTOR = 0.1;

export function getAxesDomains(
  sinogram: Sinogram | undefined,
  renderMode: RenderMode
): SinogramRanges {
  if (!sinogram) {
    return {
      translationrange: extendDomain([0, 1024], EXTENSION_FACTOR),
      rotationrange: extendDomain([0, 360], EXTENSION_FACTOR),
    };
  }

  const { rotationrange, translationrange } = sinogram;

  if (renderMode === 'marker') {
    return {
      translationrange: extendDomain(translationrange, EXTENSION_FACTOR),
      rotationrange: extendDomain(rotationrange, EXTENSION_FACTOR),
    };
  }

  if (renderMode === 'solid') {
    const { rotationaxispoints } = sinogram;
    const rotationStep =
      Math.abs(rotationrange[1] - rotationrange[0]) / rotationaxispoints;

    return {
      translationrange,
      rotationrange: [
        rotationrange[0] - rotationStep / 2,
        rotationrange[1] - rotationStep / 2,
      ],
    };
  }

  throw new Error(
    `Expected "marker" or "solid" render mode. Got ${renderMode}`
  );
}
