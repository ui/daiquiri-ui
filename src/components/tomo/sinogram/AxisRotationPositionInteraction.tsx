import PointerClick from '../../h5web/SelectionPoint';
import debug from 'debug';
import { useCallback } from 'react';
import type { CanvasEvent } from '@h5web/lib';
import { useLinearVisCanvas } from '../../h5web/LinearVisCanvas2';

const logger = debug(
  'daiquiri.components.tomo.sinogram.AxisRotationPositionInteraction'
);

/**
 * Plot component handling mouse interaction to select the location of the axis position
 */
export default function AxisRotationPositionInteraction(props: {
  operator?: boolean;
  resetMouseMode: () => void;
  setRequestedCor: (position: number) => void;
}) {
  const { operator, resetMouseMode } = props;
  const linearVisCanvas = useLinearVisCanvas();
  const { mouseMode, actions } = linearVisCanvas ?? {};

  const onClick = useCallback(
    (evt: CanvasEvent<MouseEvent>) => {
      const x = evt.dataPt.x;
      resetMouseMode();
      props.setRequestedCor(x);
    },
    [props.setRequestedCor, actions]
  );

  if (!operator) {
    logger('disabled: not operator');
    return <></>;
  }
  if (mouseMode !== 'set-axis-rotation') {
    logger('disabled: mouseMode dont match');
    return <></>;
  }
  logger('enabled');
  return <PointerClick onClick={onClick} />;
}
