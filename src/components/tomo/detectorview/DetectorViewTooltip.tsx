import type { TomoConfigHardware } from '../../../connect/tomo/utils';
import { posInMMElseNull } from '../utils/geometry';
import { getAxisName } from '../utils/geometry';

export function DetectorViewTooltip(props: {
  sampleStage: TomoConfigHardware;
  px: number;
  py: number;
  pixel: number | undefined;
}) {
  /**
   * Apply `toFixed` only if the input is a floating point.
   */
  function toFixedIfNeeded(val: number, digits: number): string {
    if (Math.round(val) === val) {
      return val.toFixed(0);
    }
    return val.toFixed(digits);
  }
  const { sampleStage } = props;

  const sy = posInMMElseNull(sampleStage.sy);
  const sz = posInMMElseNull(sampleStage.sz);
  const sampy = posInMMElseNull(sampleStage.sampy);

  const syName = getAxisName(sampleStage.sy, 'sy');
  const sampyName = getAxisName(sampleStage.sampy, 'sampy');
  const szName = getAxisName(sampleStage.sz, 'sz');
  const N_DIGITS = 2;

  return (
    <table>
      {sampy === null && sy !== null && (
        <tr className="flex-nowrap text-nowrap">
          <td>{syName}</td>
          <td className="text-end">{props.px.toFixed(N_DIGITS)}</td>
          <td className="text-start">mm</td>
        </tr>
      )}
      {sampy !== null && sy !== null && (
        <tr className="flex-nowrap text-nowrap">
          <td>{syName}</td>
          <td className="text-end">
            {(sy + sampy - props.px).toFixed(N_DIGITS)}
          </td>
          <td className="text-start">mm</td>
        </tr>
      )}
      {sampy !== null && sy !== null && (
        <tr className="flex-nowrap text-nowrap">
          <td>{sampyName}</td>
          <td className="text-end">{(props.px - sy).toFixed(N_DIGITS)}</td>
          <td className="text-start">mm</td>
        </tr>
      )}
      <tr className="flex-nowrap text-nowrap">
        <td style={{ minWidth: '7em' }}>{szName}</td>
        <td style={{ minWidth: '4em' }} className="text-end">
          {props.py.toFixed(N_DIGITS)}
        </td>
        <td className="text-start">mm</td>
      </tr>
      {props.pixel !== undefined && (
        <tr className="flex-nowrap text-nowrap">
          <td>pixel</td>
          <td className="text-end">{toFixedIfNeeded(props.pixel, N_DIGITS)}</td>
          <td className="text-start" />
        </tr>
      )}
    </table>
  );
}
