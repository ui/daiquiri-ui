import { Dropdown, ButtonGroup, Button } from 'react-bootstrap';
import type { MotorSchema } from '@esrf/daiquiri-lib';
import { useOperator } from '../../utils/hooks';
import type { MouseModes } from './models';

interface Props {
  mouseMode: MouseModes;
  setOrResetMouseMode?: (value: MouseModes) => void;
  className?: string;
  sy: MotorSchema | null;
  sz: MotorSchema | null;
  sampy: MotorSchema | null;
}

/**
 * Widget to select the mouse mode for the tomo view
 */
export default function MouseMode(props: Props) {
  const { mouseMode, setOrResetMouseMode } = props;
  const operator = useOperator();

  function getDeviceName(device: MotorSchema | null, alias: string) {
    if (!device) {
      return alias;
    }
    return device.alias ?? device.name;
  }

  function getDisabledCause(device: MotorSchema | null) {
    if (!operator) {
      return 'You must have the control to the session to move motors';
    }
    if (!device) {
      return 'This motor is not available';
    }
    if (!device.online) {
      return 'This motor is not online';
    }
    const { state } = device.properties;
    if (!state.includes('READY')) {
      return `This motor is not ready (found ${device.properties.state})`;
    }
    return undefined;
  }

  const syDisabledCause = getDisabledCause(props.sy);
  const szDisabledCause = getDisabledCause(props.sz);
  const sampyDisabledCause = getDisabledCause(props.sampy);

  const syName = getDeviceName(props.sy, 'sy');
  const szName = getDeviceName(props.sz, 'sz');
  const sampyName = getDeviceName(props.sampy, 'sampy');

  return (
    <Dropdown as={ButtonGroup} className={props.className}>
      {props.sampy !== null && (
        <Button
          variant={mouseMode === 'sampy' ? 'danger' : 'secondary'}
          onClick={() => {
            setOrResetMouseMode?.('sampy');
          }}
          disabled={sampyDisabledCause !== undefined}
          title={sampyDisabledCause}
        >
          <i className="fa fam-arrow-h-over-rot" /> {sampyName}
        </Button>
      )}
      <Button
        variant={mouseMode === 'sy' ? 'danger' : 'secondary'}
        onClick={() => {
          setOrResetMouseMode?.('sy');
        }}
        disabled={syDisabledCause !== undefined}
        title={syDisabledCause}
      >
        <i className="fa fam-arrow-h-under-rot" /> {syName}
      </Button>

      <Button
        variant={mouseMode === 'sz' ? 'danger' : 'secondary'}
        onClick={() => {
          setOrResetMouseMode?.('sz');
        }}
        disabled={szDisabledCause !== undefined}
        title={szDisabledCause}
      >
        <i className="fa fa-arrows-v" /> {szName}
      </Button>
    </Dropdown>
  );
}
