import type { TomoConfigHardware } from 'connect/tomo/utils';
import type { TomoDetectorArray } from 'services/TomoDetectorService';
import type { DetectorViewConfig } from '../config/DetectorView';
import { fovInMM, posInMM, posInMMElseNull } from '../utils/geometry';

export type MouseModes =
  | 'pan'
  | 'zoom'
  | 'sz'
  | 'sampy'
  | 'sy'
  | 'measure-distance'
  | 'measure-ortho'
  | 'measure-angle';

export interface SampleStageGeometry {
  detectorCenter: [number, number];
  minDetectorPos: [number, number];
  maxDetectorPos: [number, number];
  viewCenter: [number, number];
  minViewPos: [number, number];
  maxViewPos: [number, number];
}

export function computeSampleStageGeometry(
  tomoHardware: TomoConfigHardware,
  imageArray: TomoDetectorArray | null,
  config: DetectorViewConfig
): SampleStageGeometry | null {
  const detectorLocation = getDetectorLocationInScene(tomoHardware);
  if (detectorLocation === null) {
    return null;
  }
  const { y, z, fov } = detectorLocation;
  if (fov === null) {
    return null;
  }

  function getViewCenter(): [number, number] {
    if (imageArray === null || config.follow === 'motors') {
      return [y, z];
    }
    const frameLocation = getFrameLocationInScene(tomoHardware, imageArray);
    if (frameLocation === null) {
      return [0, 0];
    }
    return [frameLocation.y, frameLocation.z];
  }

  const [vy, vz] = getViewCenter();

  return {
    detectorCenter: [y, z],
    minDetectorPos: [y - fov[0] * 0.5, z - fov[1] * 0.5],
    maxDetectorPos: [y + fov[0] * 0.5, z + fov[1] * 0.5],
    viewCenter: [vy - fov[0] * 0.5, vz - fov[1] * 0.5 * config.fieldOfView],
    minViewPos: [vy - fov[0] * 0.5, vz - fov[1] * 0.5 * config.fieldOfView],
    maxViewPos: [vy + fov[0] * 0.5, vz + fov[1] * 0.5 * config.fieldOfView],
  };
}

export function getDetectorLocationInScene(
  tomoHardware: TomoConfigHardware
): { y: number; z: number; fov: [number, number] | null } | null {
  const { sy, sz, sampy, tomoDetector, detector } = tomoHardware;

  if (sy === null || sz === null) {
    return null;
  }
  const szPos = posInMM(sz);
  // If no translation over the rotation, there is no y location
  const sampyPos = posInMMElseNull(sampy) ?? 0;
  const syPos = posInMM(sy);
  const fov = fovInMM(tomoDetector, detector);

  const y = syPos + sampyPos;
  const z = szPos;
  return { y, z, fov };
}

export function getFrameLocationInScene(
  tomoHardware: TomoConfigHardware,
  detectorArray: TomoDetectorArray
): { y: number; z: number; px: number } | null {
  const {
    imagePixelSize,
    sampyPosition,
    syPosition,
    szPosition,
    detectorCenterY,
    detectorCenterZ,
  } = detectorArray;

  if (imagePixelSize === null || imagePixelSize.units() === 'pixel') {
    return null;
  }

  if (syPosition === null || szPosition === null) {
    return null;
  }

  const { sampleStage } = tomoHardware;
  const detectorCenter = sampleStage?.properties.detector_center ?? [0, 0];
  const curDetY = detectorCenter[0] ?? 0;
  const curDetZ = detectorCenter[1] ?? 0;

  const detY = detectorCenterY?.to('mm').scalar ?? 0;
  const detZ = detectorCenterZ?.to('mm').scalar ?? 0;
  const px = imagePixelSize.to('mm').scalar;
  const sy = syPosition.to('mm').scalar;
  const sz = szPosition.to('mm').scalar;
  // If no translation over the rotation, there is no y location
  const sampy = sampyPosition ? sampyPosition.to('mm').scalar : 0;
  const y = sy + sampy + (curDetY - detY);
  const z = sz + (curDetZ - detZ);

  return { y, z, px };
}
