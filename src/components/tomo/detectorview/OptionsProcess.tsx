import type { Dispatch, PropsWithChildren, SetStateAction } from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';
import type { TomoDetector } from 'types/Tomo';
import { TomoProcess } from 'services/TomoDetectorService';

interface Props {
  as: any;
  value: TomoProcess;
  onSelect: Dispatch<SetStateAction<TomoProcess>>;
  detector: TomoDetector | null;
}

function selectedVariant<T>(value: T, expected: T) {
  return value === expected ? 'primary' : 'secondary';
}

function SelectedButton<T>(props: {
  var: T;
  value: T;
  setter: Dispatch<SetStateAction<T>>;
  title: string;
  warningMessage?: string;
  iconName: string;
}) {
  const { warningMessage, iconName } = props;
  return (
    <Button
      variant={selectedVariant(props.var, props.value)}
      onClick={() => {
        props.setter(props.value);
      }}
      className="text-nowrap"
      size="sm"
      title={warningMessage ?? props.title}
    >
      <span className="fa-stack fa-2x">
        <i className={`${iconName} fa-stack-1x`} />
        {props.warningMessage && (
          <i
            className="fa-solid fa-xmark fa-stack-1x text-danger"
            title={warningMessage}
          />
        )}
      </span>
    </Button>
  );
}

export default function OptionsProcess(props: Props) {
  return (
    <ButtonGroup>
      <SelectedButton
        var={props.value}
        value={TomoProcess.Flatfield}
        setter={props.onSelect}
        title="Display the last projection with flatfield correction"
        iconName="fad fam-picture-dinosaur"
      />
      <SelectedButton
        var={props.value}
        value={TomoProcess.Proj}
        setter={props.onSelect}
        title="Display the last projection"
        warningMessage={
          props.detector?.data?.frame_no === undefined
            ? 'No projection yet available'
            : undefined
        }
        iconName="fad fam-picture-flatfield-raw"
      />
      <SelectedButton
        var={props.value}
        value={TomoProcess.Flat}
        setter={props.onSelect}
        title="Display the flat used for the correction"
        warningMessage={
          props.detector?.flat === undefined
            ? 'No flat yet available'
            : undefined
        }
        iconName="fad fam-picture-flatfield-flat"
      />
      <SelectedButton
        var={props.value}
        value={TomoProcess.Dark}
        setter={props.onSelect}
        title="Display the dark used for the correction"
        warningMessage={
          props.detector?.dark === undefined
            ? 'No dark yet available'
            : undefined
        }
        iconName="fad fam-picture-flatfield-dark"
      />
    </ButtonGroup>
  );
}
