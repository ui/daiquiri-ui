import type { TomoConfigHardware } from '../../../connect/tomo/utils';
import Label from 'components/h5web/items/Label';
import { CornerMarker } from 'components/h5web/items/shapes/CornerMarker';
import { CrossMarker } from 'components/h5web/items/shapes/CrossMarker';
import sassVariables from 'scss/variables.module.scss';
import debug from 'debug';
import { getDetectorLocationInScene } from './models';

const logger = debug(
  'daiquiri.components.tomo.detectorview.ActualDetectorLocation'
);

export function DetectorCorners(props: {
  fov: [number, number];
  x: number;
  y: number;
}) {
  const { x, y, fov } = props;
  const hfovx = fov[0] * 0.5;
  const hfovy = fov[1] * 0.5;
  const sizeX = 40;
  const sizeY = 40;

  return (
    <>
      <CornerMarker
        x={x + hfovx}
        y={y + hfovy}
        sizeXInScreen={-sizeX}
        sizeYInScreen={-sizeY}
        color={sassVariables.danger}
        lineWidth={4}
        inner
      />
      <CornerMarker
        x={x - hfovx}
        y={y - hfovy}
        sizeXInScreen={sizeX}
        sizeYInScreen={sizeY}
        color={sassVariables.danger}
        lineWidth={4}
        inner
      />
      <CornerMarker
        x={x - hfovx}
        y={y + hfovy}
        sizeXInScreen={sizeX}
        sizeYInScreen={-sizeY}
        color={sassVariables.danger}
        lineWidth={4}
        inner
      />
      <CornerMarker
        x={x + hfovx}
        y={y - hfovy}
        sizeXInScreen={-sizeX}
        sizeYInScreen={sizeY}
        color={sassVariables.danger}
        lineWidth={4}
        inner
      />
    </>
  );
}

export function ActualDetectorLocation(props: {
  tomoHardware: TomoConfigHardware;
}) {
  const { tomoHardware } = props;
  const { detector } = tomoHardware;
  const detectorLocation = getDetectorLocationInScene(tomoHardware);
  if (detectorLocation === null) {
    logger('Skipped: one of sy, sz is missing.');
    return <></>;
  }
  const { y, z, fov } = detectorLocation;
  return (
    <>
      <CrossMarker
        x={y}
        y={z}
        color={sassVariables.danger}
        sizeInScreen={40}
        lineWidth={3}
        zIndex={0.5}
      />
      <Label
        datapos={[y, z]}
        color={sassVariables.danger}
        text="Beam"
        anchor="top-right"
      />
      {fov !== null && (
        <>
          <Label
            datapos={[y - fov[0] * 0.5, z - fov[1] * 0.5]}
            color={sassVariables.danger}
            text={detector?.name ?? 'Detector'}
            anchor="top-right"
          />
          <DetectorCorners x={y} y={z} fov={fov} />
        </>
      )}
    </>
  );
}
