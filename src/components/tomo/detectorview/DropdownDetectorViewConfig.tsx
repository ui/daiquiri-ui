import type { Dispatch, PropsWithChildren, SetStateAction } from 'react';
import {
  Dropdown,
  ButtonGroup,
  Button,
  Container,
  Row,
  Col,
} from 'react-bootstrap';
import type { DetectorViewConfig } from '../config/DetectorView';

interface Props {
  variant?: string;
  as?: any;
  config: DetectorViewConfig;
}

function selectedVariant<T>(value: T, expected: T) {
  return value === expected ? 'primary' : 'secondary';
}

function SelectedButton<T>(
  props: PropsWithChildren<{
    var: T;
    value: T;
    setter: Dispatch<SetStateAction<T>>;
  }>
) {
  return (
    <Button
      variant={selectedVariant(props.var, props.value)}
      onClick={() => {
        props.setter(props.value);
      }}
      className="text-nowrap"
      size="sm"
    >
      {props.children}
    </Button>
  );
}

export default function DropdownDetectorViewConfig(props: Props) {
  const { as = undefined, variant = 'secondary', config } = props;

  return (
    <Dropdown as={as}>
      <Dropdown.Toggle
        id="dropdown-basic"
        variant={variant}
        className="d-flex align-items-center"
      >
        <i className="fa fa-sliders fa-fw fa-lg" />
      </Dropdown.Toggle>
      <Dropdown.Menu className="dropdown-menu-center">
        <div className="ms-1 me-1" style={{ minWidth: '300px' }}>
          <Container>
            <Row>
              <Col className="my-auto">Display axes</Col>
              <Col xs={7}>
                <ButtonGroup>
                  <SelectedButton
                    var={config.displayAxes}
                    value
                    setter={config.setDisplayAxes}
                  >
                    Yes
                  </SelectedButton>
                  <SelectedButton
                    var={config.displayAxes}
                    value={false}
                    setter={config.setDisplayAxes}
                  >
                    No
                  </SelectedButton>
                </ButtonGroup>
              </Col>
            </Row>
            <Row className="mt-1">
              <Col className="my-auto">Follow</Col>
              <Col xs={7}>
                <ButtonGroup>
                  <SelectedButton
                    var={config.follow}
                    value="motors"
                    setter={config.setFollow}
                  >
                    Motors
                  </SelectedButton>
                  <SelectedButton
                    var={config.follow}
                    value="last-image"
                    setter={config.setFollow}
                  >
                    Last image
                  </SelectedButton>
                </ButtonGroup>
              </Col>
            </Row>
            <Row className="mt-1">
              <Col className="my-auto">Field of view</Col>
              <Col xs={7}>
                <ButtonGroup>
                  <SelectedButton
                    var={config.fieldOfView}
                    value={1}
                    setter={config.setFieldOfView}
                  >
                    ×1
                  </SelectedButton>
                  <SelectedButton
                    var={config.fieldOfView}
                    value={1.5}
                    setter={config.setFieldOfView}
                  >
                    ×1.5
                  </SelectedButton>
                  <SelectedButton
                    var={config.fieldOfView}
                    value={2}
                    setter={config.setFieldOfView}
                  >
                    ×2
                  </SelectedButton>
                  <SelectedButton
                    var={config.fieldOfView}
                    value={3}
                    setter={config.setFieldOfView}
                  >
                    ×3
                  </SelectedButton>
                </ButtonGroup>
              </Col>
            </Row>
            <Row className="mt-1">
              <Col className="my-auto">Crosshair</Col>
              <Col xs={7}>
                <ButtonGroup>
                  <SelectedButton
                    var={config.crossHair}
                    value
                    setter={config.setCrossHair}
                  >
                    Yes
                  </SelectedButton>
                  <SelectedButton
                    var={config.crossHair}
                    value={false}
                    setter={config.setCrossHair}
                  >
                    No
                  </SelectedButton>
                </ButtonGroup>
              </Col>
            </Row>
            <Row className="mt-1">
              <Col className="my-auto">Mean level</Col>
              <Col xs={7}>
                <ButtonGroup>
                  <SelectedButton
                    var={config.displayMean}
                    value
                    setter={config.setDisplayMean}
                  >
                    Show
                  </SelectedButton>
                  <SelectedButton
                    var={config.displayMean}
                    value={false}
                    setter={config.setDisplayMean}
                  >
                    Hide
                  </SelectedButton>
                </ButtonGroup>
              </Col>
            </Row>
            <Row className="mt-1">
              <Col className="my-auto">Histogram</Col>
              <Col xs={7}>
                <ButtonGroup>
                  <SelectedButton
                    var={config.displayHistogram}
                    value
                    setter={config.setDisplayHistogram}
                  >
                    Show
                  </SelectedButton>
                  <SelectedButton
                    var={config.displayHistogram}
                    value={false}
                    setter={config.setDisplayHistogram}
                  >
                    Hide
                  </SelectedButton>
                </ButtonGroup>
              </Col>
            </Row>
          </Container>
        </div>
      </Dropdown.Menu>
    </Dropdown>
  );
}
