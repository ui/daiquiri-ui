import debug from 'debug';
import type { TomoConfigHardware } from '../../../connect/tomo/utils';
import { posInMM } from '../utils/geometry';
import Label from 'components/h5web/items/Label';
import { Vector2 } from 'three';
import { VSegment } from '../../h5web/items/shapes/VSegment';
import type { SampleStageGeometry } from './models';

const logger = debug(
  'daiquiri.components.tomo.detectorview.ActualRotationAxisLocation'
);

export function RotationAxisLocation(props: {
  tomoHardware: TomoConfigHardware;
  sampleStageGeometry: SampleStageGeometry;
}) {
  const { tomoHardware, sampleStageGeometry } = props;
  if (tomoHardware.sy === null) {
    return <></>;
  }
  const curDetY =
    tomoHardware.sampleStage?.properties.detector_center?.[0] ?? 0;

  const rotationAxis =
    sampleStageGeometry.detectorCenter[0] + posInMM(tomoHardware.sy) - curDetY;
  const bottom = sampleStageGeometry.maxDetectorPos[1];
  const height =
    sampleStageGeometry.maxDetectorPos[1] -
    sampleStageGeometry.minDetectorPos[1];
  const top = bottom - height * 0.1;

  const endPoint = new Vector2(rotationAxis, top);
  const dash = 10;
  return (
    <>
      <VSegment
        x={rotationAxis}
        y1={bottom}
        y2={top}
        color="red"
        gapColor="white"
        dashSize={dash}
        gapSize={dash}
        lineWidth={2}
        zIndex={1.5}
      />
      <Label
        datapos={[endPoint.x, endPoint.y]}
        color="#FF0000"
        text="Theorical rotation axis"
      />
    </>
  );
}
