import debug from 'debug';
import type { RefObject } from 'react';
import type { TomoDetectorArray } from 'services/TomoDetectorService';
import { Annotation } from '@h5web/lib';
import type { ColorMap, ScaleType, Domain } from '@h5web/lib';
import Cross from '../../h5web/items/shapes/Cross';
import type { DetectorViewConfig } from '../config/DetectorView';
import type { ImageMeshRef } from '../../h5web/items/ImageMesh';
import ImageMesh from '../../h5web/items/ImageMesh';
import type { TomoConfigHardware } from '../../../connect/tomo/utils';
import { getFrameLocationInScene } from './models';

const logger = debug(
  'daiquiri.components.tomo.detectorview.DetectorPositionMesh'
);

export function DetectorProjectionMesh(props: {
  tomoHardware: TomoConfigHardware;
  detectorArray: TomoDetectorArray;
  lut: ColorMap;
  config: DetectorViewConfig;
  isColorMapInverted: boolean;
  scale: ScaleType;
  imageRef: RefObject<ImageMeshRef>;
  domain: Domain;
}) {
  const { config } = props;
  const { displayArray, statistics, detectorSize } = props.detectorArray;

  const frameLocation = getFrameLocationInScene(
    props.tomoHardware,
    props.detectorArray
  );

  if (frameLocation === null) {
    return null;
  }
  const { y, z, px } = frameLocation;
  return (
    <>
      <ImageMesh
        ref={props.imageRef}
        values={displayArray}
        domain={props.domain}
        colorMap={props.lut}
        invertColorMap={props.isColorMapInverted}
        scaleType={props.scale}
        position={[y, z, 0]}
        scale={[-px, px, px]}
        size={detectorSize}
      />
      {config.displayMean && statistics?.mean && (
        <Annotation
          x={y + detectorSize.width * px * 0.5}
          y={z - detectorSize.height * px * 0.5}
        >
          <div className="ms-2 mt-2" title="Mean grey level">
            <div className="bg-dark text-light p-1 rounded">
              <i className="fa-solid fa-circle-half-stroke fa-fw fa-lg" />{' '}
              {statistics.mean.toFixed(2)}
            </div>
          </div>
        </Annotation>
      )}
      <Cross
        centerX={y}
        centerY={z}
        width={detectorSize.width * px}
        height={detectorSize.height * px}
        color="white"
        gapColor="black"
        dashSize={5}
        gapSize={5}
        lineWidth={1}
      />
    </>
  );
}
