import debug from 'debug';
import { useState, useRef, useCallback } from 'react';
import { ButtonGroup } from 'react-bootstrap';
import OptionsProcess from './OptionsProcess';
import Toolbar, { Separator } from 'components/common/Toolbar';
import MoveSampleStageOptions from './MoveSampleStageOptions';
import type { MouseModes } from './models';
import { computeSampleStageGeometry } from './models';
import { TomoDataKind } from 'services/TomoDetectorService';
import { TomoProcess } from 'services/TomoDetectorService';
import { useSyncScaleDomainFromStatistics } from 'components/h5web/colormap';
import { TooltipMesh } from '@h5web/lib';
import DefaultMouseModeOptions from '../../h5web/bootstrap/DefaultMouseModeOptions';
import { useActiveDetectorService } from '../utils/TomoDetector';
import { Button } from 'react-bootstrap';
import type { LinearVisCanvasRef } from '../../h5web/LinearVisCanvas2';
import { LinearVisCanvas } from '../../h5web/LinearVisCanvas2';
import DropdownDetector from '../tiling/DropdownDetector';
import DropdownDetectorViewConfig from './DropdownDetectorViewConfig';
import { useDetectorConfig } from '../config/Detector';
import { getAxisName } from '../utils/geometry';
import { SceneScale } from 'components/h5web/items/SceneScale';
import PointerClick from '../../h5web/SelectionPoint';
import type { CanvasEvent } from '@h5web/lib';
import Qty from 'js-quantities';
import { useDetectorViewConfig } from '../config/DetectorView';
import type { ImageMeshRef } from '../../h5web/items/ImageMesh';
import { LoadingMessage } from '../../h5web/items/LoadingMessage';
import AutoScaleOption from '../../h5web/bootstrap/AutoscaleOption';
import HistogramDomainSlider from '../../h5web/bootstrap/HistogramDomainSlider';
import {
  useTomoConfigRelations,
  useTomoDetectors,
  useTomoSampleStageRefs,
} from '../utils/hooks';
import RulerSelectionTool from '../../h5web/RulerSelectionTool';
import { useMouseModeInteraction } from '../../h5web/UseMouseModeInteraction';
import { RulerButton } from '../../h5web/bootstrap/RulerButton';
import { requestMoveSampleStage } from '../../../services/TomoService';
import { ActualDetectorLocation } from './ActualDetectorLocation';
import { DetectorProjectionMesh } from './DetectorProjectionMesh';
import { DetectorViewTooltip } from './DetectorViewTooltip';
import { RotationAxisLocation } from './ActualRotationAxisLocation';

const logger = debug('daiquiri.components.tomo.detectorview.Default');

function normalizeBool(value: any, defaultValue: boolean): boolean {
  if (value === null || value === undefined) {
    return defaultValue;
  }
  return value;
}

export interface TomoDetectorViewOptions {
  /** Identifier of the tomoconfig hardware object */
  tomoconfig: string;
  /** If true the UI provides actions to move motors, else (the default) any move are disabled */
  moveenabled?: boolean;
}

export default function TomoDetectorView(props: {
  options: TomoDetectorViewOptions;
}) {
  const { options } = props;
  const detectors = useTomoDetectors();
  const sampleStageRefs = useTomoSampleStageRefs(options.tomoconfig ?? '');
  const sampleStage = useTomoConfigRelations(sampleStageRefs);
  const detectorId = sampleStage.detector?.id ?? '';
  const detector = detectors[detectorId];
  const detectorConfig = useDetectorConfig();

  const moveenabled = normalizeBool(options.moveenabled, true);

  const config = useDetectorViewConfig();

  const [process, setProcess] = useState(TomoProcess.Flatfield);

  const activeDetectorService = useActiveDetectorService({
    detector,
    sampleStage,
    profile: detectorConfig.profile,
    normalization: detectorConfig.scaleType,
    autoscale: detectorConfig.autoscale,
    autoscaleMode: detectorConfig.autoscaleMode,
    domain: detectorConfig.scaleDomain,
    histogram: config.displayHistogram,
    enabled: true,
    minRefreshPeriod: 0.5,
    processing: process,
  });
  const detectorArray = activeDetectorService.data;

  useSyncScaleDomainFromStatistics({
    statistics: detectorArray?.statistics,
    ...detectorConfig,
  });

  const plotRef = useRef<LinearVisCanvasRef>(null);
  const imageRef = useRef<ImageMeshRef>(null);

  const mouseModeInteraction = useMouseModeInteraction<MouseModes>('zoom');
  const { mouseMode, setMouseMode } = mouseModeInteraction;

  const sampleStageGeometry = computeSampleStageGeometry(
    sampleStage,
    detectorArray,
    config
  );

  function getAxisPosition(name: 'sy' | 'sampy' | 'somega' | 'sz') {
    if (!sampleStage) {
      return null;
    }
    const axis = sampleStage[name] || null;
    if (!axis) {
      return null;
    }
    const position = axis.properties.position;
    if (position === null) {
      return null;
    }
    return new Qty(position, axis.properties.unit);
  }

  const sampyPosition = getAxisPosition('sampy');
  const syPosition = getAxisPosition('sy');

  const onClick = useCallback(
    (evt: CanvasEvent<MouseEvent>) => {
      const y = evt.dataPt.x;
      const z = evt.dataPt.y;
      // Positive move aim the left
      // const dx = event.pixelSize.mul(-(event.x - cx));
      // Positive move aim the bottom
      // const dy = event.pixelSize.mul(event.y - cy);
      // const imageData = event.detectorData;
      switch (mouseMode) {
        case 'pan':
          break;
        case 'zoom':
          break;
        case 'sampy':
          // move sampy in order to use this location as sample axis
          setMouseMode('pan');
          if (syPosition !== null) {
            const pos = new Qty(y, 'mm').sub(syPosition);
            requestMoveSampleStage({ sampy: pos });
          } else {
            console.error('sy axis is not defined');
          }
          break;
        case 'sy':
          // move sy in order to move this pixel in the center of the screen
          setMouseMode('pan');
          if (syPosition !== null) {
            if (sampyPosition !== null) {
              const pos = syPosition.add(sampyPosition).sub(new Qty(y, 'mm'));
              requestMoveSampleStage({ sy: pos });
            } else {
              const pos = new Qty(y, 'mm');
              requestMoveSampleStage({ sy: pos });
            }
          } else {
            console.error('sy axis is not defined');
          }
          break;
        case 'sz':
          // move sz in order to move this pixel in the center of the screen
          setMouseMode('pan');
          requestMoveSampleStage({ sz: new Qty(z, 'mm') });
          break;
        default:
          console.error(`Unexpected ${mouseMode}`);
      }
    },
    [
      mouseMode,
      sampyPosition,
      syPosition,
      requestMoveSampleStage,
      detectorArray,
    ]
  );
  const statistics = detectorArray?.statistics;
  const unusablePixelSize =
    !detectorArray?.imagePixelSize ||
    detectorArray?.imagePixelSize?.units() === 'pixel';

  function getDataDescription(datakind?: TomoDataKind) {
    if (!datakind) {
      return undefined;
    }
    const messages: Record<TomoDataKind, string | undefined> = {
      [TomoDataKind.Dark]: 'Dark',
      [TomoDataKind.Flat]: 'Flat',
      [TomoDataKind.Proj]: 'Proj',
      [TomoDataKind.Unknown]: 'Unknown data',
      [TomoDataKind.FlatfieldNorm]: undefined, // That's the default data we expect
      [TomoDataKind.ProjNorm]: 'Only normalized proj',
      [TomoDataKind.ProjDarkNorm]: 'Only normalized proj-dark',
      [TomoDataKind.ProjFlatNorm]: 'Only normalized proj/flat',
    };
    return messages[datakind];
  }

  const dataDescription = getDataDescription(detectorArray?.datakind);

  return (
    <div
      className="plot2d-container w-100 h-100"
      style={{
        flex: '1 1 0%',
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <Toolbar align="center">
        <DefaultMouseModeOptions mouseModeInteraction={mouseModeInteraction} />
        <Button
          title="Reset zoom (sample stage overview)"
          variant="secondary"
          onClick={() => {
            plotRef?.current?.actions.resetZoom();
          }}
        >
          <i className="fa fa-expand fa-fw fa-lg" />
        </Button>
        <Separator />
        <RulerButton mouseModeInteraction={mouseModeInteraction} />
        {moveenabled && (
          <MoveSampleStageOptions
            mouseMode={mouseMode}
            setOrResetMouseMode={mouseModeInteraction.setOrResetMouseMode}
            sy={sampleStage.sy}
            sz={sampleStage.sz}
            sampy={sampleStage.sampy}
          />
        )}
        <Separator />
        <DropdownDetector
          showDisplayed={false}
          variant="secondary"
          isInverted={detectorConfig.invertColorMap}
          onSelectInvertion={detectorConfig.setInvertColorMap}
          isDisplayed
          onSelectDisplayed={() => {}}
          lut={detectorConfig.colorMap}
          onSelectLut={detectorConfig.setColorMap}
          norm={detectorConfig.scaleType}
          onSelectNorm={detectorConfig.setScaleType}
          autoscale={detectorConfig.autoscale}
          onSelectAutoscale={detectorConfig.setAutoscale}
          autoscaleMode={detectorConfig.autoscaleMode}
          onSelectAutoscaleMode={detectorConfig.setAutoscaleMode}
          profile={detectorConfig.profile}
          onSelectProfile={detectorConfig.setProfile}
          array={detectorArray}
        />
        <AutoScaleOption
          disabled={statistics === undefined}
          {...detectorConfig}
        />
        <HistogramDomainSlider
          disabled={statistics === undefined}
          statistics={detectorArray?.statistics}
          histogram={detectorArray?.histogram}
          {...detectorConfig}
        />
        <OptionsProcess
          as={ButtonGroup}
          value={process}
          onSelect={setProcess}
          detector={detector}
        />
        <Separator />
        <DropdownDetectorViewConfig config={config} />
      </Toolbar>
      <div
        style={{
          flex: '1 1 auto',
          display: 'flex',
          margin: 0,
          minHeight: 0,
        }}
      >
        {sampleStageGeometry !== null && (
          <LinearVisCanvas
            plotRef={plotRef}
            abscissaConfig={{
              visDomain: [
                sampleStageGeometry.minViewPos[0],
                sampleStageGeometry.maxViewPos[0],
              ],
              label: `${getAxisName(sampleStage.sy, 'sy')} + ${getAxisName(
                sampleStage.sampy,
                'sampy'
              )} (mm)`,
              flip: true,
            }}
            ordinateConfig={{
              visDomain: [
                sampleStageGeometry.minViewPos[1],
                sampleStageGeometry.maxViewPos[1],
              ],
              label: `${getAxisName(sampleStage.sz, 'sz')} (mm)`,
              flip: true,
            }}
            showAxes={config.displayAxes}
            mouseMode={mouseMode}
            aspect="equal"
          >
            <LoadingMessage
              loading={activeDetectorService.loading}
              info={dataDescription}
              warning={detectorArray === null ? 'No data' : undefined}
              danger={activeDetectorService.error}
            />
            <RulerSelectionTool mouseMode={mouseMode} plotUnit="mm" />
            {detectorArray && (
              <DetectorProjectionMesh
                tomoHardware={sampleStage}
                config={config}
                detectorArray={detectorArray}
                lut={detectorConfig.colorMap}
                isColorMapInverted={detectorConfig.invertColorMap}
                scale={detectorConfig.scaleType}
                domain={detectorConfig.scaleDomain}
                imageRef={imageRef}
              />
            )}
            <RotationAxisLocation
              tomoHardware={sampleStage}
              sampleStageGeometry={sampleStageGeometry}
            />
            <ActualDetectorLocation tomoHardware={sampleStage} />
            <TooltipMesh
              guides={config.crossHair ? 'both' : undefined}
              renderTooltip={(x, y) => {
                const p = imageRef.current?.pick(x, y);
                return (
                  <DetectorViewTooltip
                    sampleStage={sampleStage}
                    px={x}
                    py={y}
                    pixel={p}
                  />
                );
              }}
            />
            {(mouseMode === 'sz' ||
              mouseMode === 'sampy' ||
              mouseMode === 'sy') && <PointerClick onClick={onClick} />}
            <SceneScale unit="mm" />
          </LinearVisCanvas>
        )}
        {sampleStageGeometry === null && (
          <LinearVisCanvas
            plotRef={plotRef}
            abscissaConfig={{
              visDomain: [0, detectorArray?.detectorSize?.width ?? 1],
              label: `width (px)`,
              flip: true,
            }}
            ordinateConfig={{
              visDomain: [0, detectorArray?.detectorSize?.height ?? 1],
              label: `height (px)`,
              flip: true,
            }}
            showAxes={config.displayAxes}
            mouseMode={mouseMode}
          >
            <LoadingMessage
              loading={activeDetectorService.loading}
              info={dataDescription}
              warning={
                detectorArray === null
                  ? 'No data'
                  : unusablePixelSize
                  ? 'No sample pixel size defined'
                  : undefined
              }
              danger={activeDetectorService.error}
            />
            {detectorArray && (
              <>
                <DetectorProjectionMesh
                  tomoHardware={sampleStage}
                  config={config}
                  detectorArray={detectorArray}
                  lut={detectorConfig.colorMap}
                  isColorMapInverted={detectorConfig.invertColorMap}
                  scale={detectorConfig.scaleType}
                  domain={detectorConfig.scaleDomain}
                  imageRef={imageRef}
                />
              </>
            )}
            <SceneScale unit="px" />
            <RulerSelectionTool mouseMode={mouseMode} plotUnit="px" />
          </LinearVisCanvas>
        )}
      </div>
    </div>
  );
}
