import { DropdownButton, Dropdown } from 'react-bootstrap';
import { TomoProcess } from 'services/TomoDetectorService';

interface Props {
  as: any;
  value: TomoProcess;
  onSelect: (value: TomoProcess) => void;
}

export default function DropdownProcess(props: Props) {
  const { as = undefined, onSelect, value } = props;
  const labels: Record<TomoProcess, string> = {
    [TomoProcess.Dark]: 'Last dark',
    [TomoProcess.Flat]: 'Last flat',
    [TomoProcess.Proj]: 'Last projection',
    [TomoProcess.Flatfield]: 'Flatfield correction',
  };
  const label = labels[value];

  function createItem(key: TomoProcess) {
    const icon =
      value === key ? 'fa fa-fw fa-dot-circle-o' : 'fa fa-fw fa-circle-o';
    return (
      <Dropdown.Item
        onSelect={() => {
          onSelect(key);
        }}
      >
        <i className={icon} />
        &nbsp; {labels[key]}
      </Dropdown.Item>
    );
  }

  return (
    <DropdownButton as={as} title={label} id="bg-vertical-dropdown-4">
      {createItem(TomoProcess.Flatfield)}
      <Dropdown.Divider />
      {createItem(TomoProcess.Proj)}
      {createItem(TomoProcess.Flat)}
      {createItem(TomoProcess.Dark)}
    </DropdownButton>
  );
}
