import type { PropsWithChildren } from 'react';
import { Button, ButtonGroup, Dropdown, SplitButton } from 'react-bootstrap';
import { DisplayMode } from './CompareMesh';

export function SelectMixedDisplayMode(props: {
  value: DisplayMode;
  onChanged: (value: DisplayMode) => void;
  mixedMode: DisplayMode;
  onMixedModeChanged: (value: DisplayMode) => void;
}) {
  const { value, onChanged, mixedMode, onMixedModeChanged } = props;

  function isMixedMode(displayMode: number): boolean {
    return displayMode >= 100 && displayMode < 300;
  }

  function DisplayModeButton(props: PropsWithChildren<{ value: DisplayMode }>) {
    return (
      <Button
        variant={value === props.value ? 'primary' : 'secondary'}
        onClick={() => {
          onChanged(props.value);
          onMixedModeChanged(props.value);
        }}
      >
        {props.children}
      </Button>
    );
  }

  return (
    <SplitButton
      title={
        <i title="Blend the two images" className="fa fam-compare-mode fa-lg" />
      }
      variant={isMixedMode(value) ? 'primary' : 'secondary'}
      align={{ lg: 'start' }}
      autoClose={false}
      onClick={() => {
        onChanged(mixedMode);
      }}
    >
      <Dropdown.Header>
        <h5>
          <i className="fa fam-compare-mode" /> Grey scale
        </h5>
        <ButtonGroup>
          <DisplayModeButton value={DisplayMode.MIN}>Min</DisplayModeButton>
          <DisplayModeButton value={DisplayMode.MAX}>Max</DisplayModeButton>
          <DisplayModeButton value={DisplayMode.DIFF}>Diff</DisplayModeButton>
          <DisplayModeButton value={DisplayMode.ERROR}>Error</DisplayModeButton>
          <DisplayModeButton value={DisplayMode.MUL}>A×B</DisplayModeButton>
          <DisplayModeButton value={DisplayMode.HSPLITTER}>
            Split
          </DisplayModeButton>
        </ButtonGroup>
      </Dropdown.Header>
      <Dropdown.Header>
        <h5>
          <span className="fa-stack">
            <i
              className="fa fam-compare-mode-left fa-stack-1x"
              style={{ color: 'cyan' }}
            />
            <i className="fa fam-compare-mode-center fa-stack-1x" />
            <i
              className="fa fam-compare-mode-right fa-stack-1x"
              style={{ color: 'lime' }}
            />
          </span>{' '}
          Green & Cyan
        </h5>
        <ButtonGroup>
          <DisplayModeButton value={DisplayMode.MIN_COLOR}>
            Min
          </DisplayModeButton>
          <DisplayModeButton value={DisplayMode.MAX_COLOR}>
            Max
          </DisplayModeButton>
          <DisplayModeButton value={DisplayMode.DIFF_COLOR}>
            Diff
          </DisplayModeButton>
          <DisplayModeButton value={DisplayMode.ERROR_COLOR}>
            Error
          </DisplayModeButton>
          <DisplayModeButton value={DisplayMode.MUL_COLOR}>
            A×B
          </DisplayModeButton>
          <DisplayModeButton value={DisplayMode.HSPLITTER_COLOR}>
            Split
          </DisplayModeButton>
        </ButtonGroup>
      </Dropdown.Header>
    </SplitButton>
  );
}
