import React from 'react';
import { useState, useRef } from 'react';
import { Stack, Button } from 'react-bootstrap';

export function TextArea(props: {
  defaultValue?: string;
  onChange?: (value: string) => void;
}) {
  const valueRef = useRef(props.defaultValue ?? '');
  const [invalidated, setInvalidated] = useState(false);

  function update(v: string) {
    valueRef.current = v;
    setInvalidated(true);
  }

  function send() {
    props.onChange?.(valueRef.current);
    setInvalidated(false);
  }

  const keydownHandler = (e: KeyboardEvent) => {
    if (e.key === 'Enter' && e.ctrlKey) send();
  };
  React.useEffect(() => {
    document.addEventListener('keydown', keydownHandler);
    return () => {
      document.removeEventListener('keydown', keydownHandler);
    };
  }, []);

  return (
    <Stack direction="vertical" gap={1} className="mx-auto">
      <textarea
        rows={5}
        cols={40}
        defaultValue={valueRef.current}
        onChange={(e) => {
          update(e.target.value);
        }}
      />
      <Button
        variant={invalidated ? 'primary' : 'secondary'}
        onClick={() => {
          send();
        }}
      >
        Apply (Ctrl+Return)
      </Button>
    </Stack>
  );
}
