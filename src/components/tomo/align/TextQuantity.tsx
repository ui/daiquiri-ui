import { useMemo } from 'react';
import Qty from 'js-quantities';

function normalizeUnit(u: string): string {
  if (u === 'um') {
    return 'μm';
  }
  return u;
}

function useFormattedQuantity(
  value: number | null,
  unit: string,
  displayUnit: string,
  decimals: number
): [string, string] {
  return useMemo(() => {
    function normUnit(u: string): string {
      if (u === 'um') {
        return 'μm';
      }
      return u;
    }
    function normValue(v: number): string {
      return v.toFixed(decimals).replace(/\.?0+$/, '');
    }
    if (value === null) {
      return ['∅', ''];
    }
    if (unit === displayUnit) {
      return [normValue(value), normUnit(displayUnit)];
    }
    const q = Qty(value, unit);
    return [normValue(q.to(displayUnit).scalar), normUnit(displayUnit)];
  }, [value, unit, displayUnit]);
}

export function TextQuantity(props: {
  value?: number;
  unit: string;
  displayUnit?: string;
  style?: Record<string, any>;
  decimals?: number;
  className?: string;
}) {
  const {
    value = null,
    unit,
    displayUnit,
    style,
    className,
    decimals = 2,
  } = props;
  const [formattedValue, formattedUnit] = useFormattedQuantity(
    value,
    unit,
    displayUnit ?? unit,
    decimals
  );
  return (
    <span style={style} className={className} title={`${value} ${unit}`}>
      {formattedValue} {normalizeUnit(formattedUnit)}
    </span>
  );
}
