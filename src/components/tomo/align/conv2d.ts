export enum FilterMode {
  NONE,
  LAPLACIAN_3x3,
  LAPLACIAN_9x9,
  SHARPENING_3x3,
  UNSHARPENING_3x3,
  UNSHARPENING_5x5,
  SMOOTH_3x3,
  SMOOTH_5x5,
  SMOOTH_7x7,
}

export const FilterDefinitions: Record<
  FilterMode,
  { name: string; size: number; coefs: Float32Array }
> = {
  [FilterMode.NONE]: {
    name: 'No filter',
    size: 0,
    coefs: Float32Array.from([0, 0, 0, 0, 0, 0, 0, 0, 0]),
  },
  [FilterMode.LAPLACIAN_3x3]: {
    name: 'Laplacian 3×3',
    size: 3,
    coefs: Float32Array.from([0, -1, 0, -1, 4, -1, 0, -1, 0]),
  },
  [FilterMode.LAPLACIAN_9x9]: {
    name: 'Laplacian 9×9',
    size: 9,
    coefs: Float32Array.from([
      0, 1, 1, 2, 2, 2, 1, 1, 0, 1, 2, 4, 5, 5, 5, 4, 2, 1, 1, 4, 5, 3, 0, 3, 5,
      4, 1, 2, 5, 3, -12, -24, -12, 3, 5, 2, 2, 5, 0, -24, -40, -24, 0, 5, 2, 2,
      5, 3, -12, -24, -12, 3, 5, 2, 1, 4, 5, 3, 0, 3, 5, 4, 1, 1, 2, 4, 5, 5, 5,
      4, 2, 1, 0, 1, 1, 2, 2, 2, 1, 1, 0,
    ]),
  },
  [FilterMode.SHARPENING_3x3]: {
    name: 'Sharpening 3×3',
    size: 3,
    coefs: Float32Array.from([-1, -1, -1, -1, 9, -1, -1, -1, -1]),
  },
  [FilterMode.UNSHARPENING_3x3]: {
    name: 'Unsharpening 3×3',
    size: 3,
    coefs: Float32Array.from([0, -1, 0, -1, 5, -1, 0, -1, 0]),
  },
  [FilterMode.UNSHARPENING_5x5]: {
    name: 'Unsharpening 5×5',
    size: 5,
    coefs: Float32Array.from([
      1 / 256,
      4 / 256,
      6 / 256,
      4 / 256,
      1 / 256,
      4 / 256,
      16 / 256,
      24 / 256,
      16 / 256,
      4 / 256,
      6 / 256,
      24 / 256,
      -476 / 256,
      24 / 256,
      6 / 256,
      4 / 256,
      16 / 256,
      24 / 256,
      16 / 256,
      4 / 256,
      1 / 256,
      4 / 256,
      6 / 256,
      4 / 256,
      1 / 256,
    ]),
  },
  [FilterMode.SMOOTH_3x3]: {
    name: 'Gaussian 3×3',
    size: 3,
    coefs: Float32Array.from([
      1 / 16,
      2 / 16,
      1 / 16,
      2 / 16,
      4 / 16,
      2 / 16,
      1 / 16,
      2 / 16,
      1 / 16,
    ]),
  },
  [FilterMode.SMOOTH_5x5]: {
    name: 'Gaussian 5×5',
    size: 5,
    coefs: Float32Array.from([
      1 / 273,
      4 / 273,
      7 / 273,
      4 / 273,
      1 / 273,
      4 / 273,
      16 / 273,
      26 / 273,
      16 / 273,
      4 / 273,
      7 / 273,
      26 / 273,
      41 / 273,
      26 / 273,
      7 / 273,
      4 / 273,
      16 / 273,
      26 / 273,
      16 / 273,
      4 / 273,
      1 / 273,
      4 / 273,
      7 / 273,
      4 / 273,
      1 / 273,
    ]),
  },
  [FilterMode.SMOOTH_7x7]: {
    name: 'Gaussian 7×7',
    size: 7,
    coefs: Float32Array.from([
      1 / 4096,
      6 / 4096,
      15 / 4096,
      20 / 4096,
      15 / 4096,
      6 / 4096,
      1 / 4096,

      6 / 4096,
      36 / 4096,
      90 / 4096,
      120 / 4096,
      90 / 4096,
      36 / 4096,
      6 / 4096,

      15 / 4096,
      90 / 4096,
      225 / 4096,
      300 / 4096,
      225 / 4096,
      90 / 4096,
      15 / 4096,

      20 / 4096,
      120 / 4096,
      300 / 4096,
      400 / 4096,
      300 / 4096,
      120 / 4096,
      20 / 4096,

      15 / 4096,
      90 / 4096,
      225 / 4096,
      300 / 4096,
      225 / 4096,
      90 / 4096,
      15 / 4096,

      6 / 4096,
      36 / 4096,
      90 / 4096,
      120 / 4096,
      90 / 4096,
      36 / 4096,
      6 / 4096,

      1 / 4096,
      6 / 4096,
      15 / 4096,
      20 / 4096,
      15 / 4096,
      6 / 4096,
      1 / 4096,
    ]),
  },
};
