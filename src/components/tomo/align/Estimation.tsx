import { Col, Row, Container } from 'react-bootstrap';
import type { NabuCorrectionInfo } from '.';
import { TextQuantity } from './TextQuantity';

export function Estimation(props: {
  nabuCorrection: NabuCorrectionInfo | null;
  decimals?: number;
  lateralUnit?: string;
}) {
  const { nabuCorrection, decimals = 2, lateralUnit = 'mm' } = props;

  if (nabuCorrection === null) {
    return <></>;
  }

  function formatNumber(
    value: number | null | undefined,
    unit?: string
  ): string {
    if (value === null || value === undefined) {
      return 'n.a.';
    }
    if (unit === undefined) {
      return value.toFixed(6);
    }
    return `${value.toFixed(6)} ${unit}`;
  }

  const erroMessage = nabuCorrection.exception?.message;

  return (
    <Container>
      <Row>
        <Col xs={4}>Estimator</Col>
        <Col xs={8}>{nabuCorrection.method ? 'nabu' : 'none'}</Col>
      </Row>
      <Row>
        <Col xs={4}>Method</Col>
        <Col xs={8}>{nabuCorrection.method}</Col>
      </Row>
      <Row>
        <Col xs={4}>Lateral</Col>
        <Col xs={8}>
          <TextQuantity
            value={nabuCorrection?.lateral_corr ?? 0}
            unit="mm"
            displayUnit={lateralUnit}
            decimals={decimals}
          />
        </Col>
      </Row>
      <Row>
        <Col xs={4}>Tilt</Col>
        <Col xs={8}>
          <TextQuantity
            value={nabuCorrection?.camera_tilt ?? 0}
            unit="deg"
            decimals={decimals}
          />
        </Col>
      </Row>
      {erroMessage && (
        <Row>
          <Col xs={4}>Failure?</Col>
          <Col xs={8}>{erroMessage}</Col>
        </Row>
      )}
    </Container>
  );
}
