import { useCallback, useEffect, useMemo } from 'react';
import { useState, useRef } from 'react';
import { InputGroup, Form } from 'react-bootstrap';
import Qty from 'js-quantities';

/**
 * Convert a quantity value into another unit.
 *
 * @param value
 * @param from: One of 'mm', 'um', 'nm', 'deg', 'rad', 'px'
 * @param to: One of 'mm', 'um', 'nm', 'deg', 'rad', 'px'
 * @param pixelSize: In px/mm
 * @returns
 */
function convert(
  value: number,
  from: string,
  to: string,
  pixelSize: Qty | undefined
): number {
  if (from === to) {
    return value;
  }
  if (from === 'rad' && to === 'deg') {
    return (180 * value) / Math.PI;
  }
  if (from === 'deg' && to === 'rad') {
    return (Math.PI * value) / 180;
  }
  if (from === 'px') {
    if (pixelSize !== undefined) {
      const px = pixelSize.to('mm').scalar;
      return convert(value * px, 'mm', to, undefined);
    }
    return Number.NaN;
  }
  if (to === 'px') {
    if (pixelSize !== undefined) {
      const v = convert(value, from, 'mm', undefined);
      const px = pixelSize.to('mm').scalar;
      return v / px;
    }
    return Number.NaN;
  }
  try {
    return Qty(value, from).to(to).scalar;
  } catch {
    console.error(`Convertion ${from}->${to} is not provided`);
    return Number.NaN;
  }
}

function normalizeUnit(u: string): string {
  if (u === 'um') {
    return 'μm';
  }
  return u;
}

export function InputQuantity(props: {
  value?: number;
  unit: string;
  displayUnit?: string;
  preferedDisplayUnit?: string;
  step: number;
  pixelSize?: Qty;
  onChange?: (value: number) => void;
  onIntermediateChange?: (value: number) => void;
  style?: Record<string, any>;
  decimals?: number;
  className?: string;
}) {
  const { style, className, decimals = 2 } = props;
  const inputRef = useRef<HTMLInputElement>(null);
  const localInputRef = useRef<number | undefined>(undefined);
  const [invalidated, setInvalidated] = useState(false);

  const displayUnit = useMemo(() => {
    if (
      props.preferedDisplayUnit !== undefined &&
      (props.pixelSize === undefined ||
        props.unit === 'px' ||
        props.preferedDisplayUnit === 'px')
    ) {
      return props.preferedDisplayUnit;
    }
    return props.displayUnit ?? props.unit;
  }, [
    props.displayUnit,
    props.unit,
    props.preferedDisplayUnit,
    props.pixelSize,
  ]);

  const convertFromDisplay = useCallback(
    (value: number | undefined) => {
      if (value === undefined || displayUnit === undefined) {
        return undefined;
      }
      return convert(value, displayUnit, props.unit, props.pixelSize);
    },
    [props.unit, displayUnit, props.pixelSize]
  );

  const convertToDisplay = useCallback(
    (value: number | undefined) => {
      if (value === undefined || displayUnit === undefined) {
        return undefined;
      }
      return convert(value, props.unit, displayUnit, props.pixelSize);
    },
    [props.unit, displayUnit, props.pixelSize]
  );

  function validitate() {
    if (localInputRef.current === undefined) return;
    const v = localInputRef.current;
    const i = convertFromDisplay(v);
    localInputRef.current = undefined;
    if (inputRef.current) {
      inputRef.current.value = v.toFixed(decimals);
    }
    if (i !== undefined) {
      props.onChange?.(i);
    }
    setInvalidated(false);
  }

  function cancel() {
    if (localInputRef.current === undefined) return;
    localInputRef.current = undefined;
    if (inputRef.current) {
      inputRef.current.value =
        convertToDisplay(props.value)?.toFixed(decimals) ?? '';
    }
    setInvalidated(false);
  }

  useEffect(() => {
    if (!localInputRef.current) {
      if (inputRef.current) {
        inputRef.current.value =
          convertToDisplay(props.value)?.toFixed(decimals) ?? '';
      }
    }
  }, [props.value]);

  return (
    <InputGroup className={className} style={style}>
      <Form.Control
        ref={inputRef}
        step={props.step}
        type="number"
        style={{ backgroundColor: invalidated ? '#FFFF80' : 'transparent' }}
        onChange={(e) => {
          setInvalidated(true);
          localInputRef.current = Number.parseFloat(e.target.value);
        }}
        onBlur={() => {
          cancel();
        }}
        onKeyDown={(e) => {
          if (/^[ a-z]$/i.test(e.key)) {
            e.preventDefault();
            return false;
          }
          e.stopPropagation();
          if (e.key === 'Enter') {
            validitate();
          }
          return true;
        }}
      />
      <InputGroup.Text>{normalizeUnit(displayUnit)}</InputGroup.Text>
    </InputGroup>
  );
}
