import React from 'react';
import { Col, Row, Container } from 'react-bootstrap';
import type { DescriptionInfo } from '.';

export function Description(props: { description: DescriptionInfo | null }) {
  const { description } = props;

  if (description === null) {
    return <></>;
  }

  return (
    <Container>
      <Row>
        <Col xs={4}>Lateral axis</Col>
        <Col xs={8}>{description.lateral_motor?.name}</Col>
      </Row>
      <Row>
        <Col xs={4}>Tilt axis</Col>
        <Col xs={8}>{description.camera_tilt_motor?.name}</Col>
      </Row>
    </Container>
  );
}
