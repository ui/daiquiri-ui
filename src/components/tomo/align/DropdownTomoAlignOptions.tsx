import {
  Button,
  Dropdown,
  ButtonGroup,
  DropdownButton,
  Stack,
} from 'react-bootstrap';
import { AutoscaleMode } from '../../h5web/colormap';
import type { AlignConfig } from '../config/Align';

export default function DropdownTomoAlignOptions(props: {
  config: AlignConfig;
}) {
  const { config } = props;
  return (
    <DropdownButton
      title={<i title="Extra options" className="fa fa-sliders fa-fw fa-lg" />}
    >
      <Dropdown.Header>
        <h5 style={{ width: '8em' }}>Auto contrast</h5>
        <ButtonGroup>
          <Button
            variant={
              config.autoscaleMode === AutoscaleMode.None
                ? 'primary'
                : 'secondary'
            }
            onClick={() => {
              config.setAutoscaleMode(AutoscaleMode.None);
            }}
          >
            None
          </Button>
          <Button
            variant={
              config.autoscaleMode === AutoscaleMode.Minmax
                ? 'primary'
                : 'secondary'
            }
            onClick={() => {
              config.setAutoscaleMode(AutoscaleMode.Minmax);
            }}
          >
            Min/max
          </Button>
          <Button
            variant={
              config.autoscaleMode === AutoscaleMode.StdDev3
                ? 'primary'
                : 'secondary'
            }
            onClick={() => {
              config.setAutoscaleMode(AutoscaleMode.StdDev3);
            }}
          >
            3×std
          </Button>
        </ButtonGroup>
      </Dropdown.Header>
      <Dropdown.Header>
        <Stack direction="horizontal">
          <h5 style={{ width: '8em' }}>Vertical line</h5>
          <Button
            title="Show/hide a vertical line"
            variant={config.displayVLine ? 'primary' : 'secondary'}
            onClick={() => {
              config.setDisplayVLine((b) => !b);
            }}
          >
            <i
              className={`fa ${
                config.displayVLine ? 'fa-eye' : 'fa-eye-slash'
              } fa-fw fa-lg`}
            />
          </Button>
        </Stack>
      </Dropdown.Header>
      <Dropdown.Header>
        <Stack direction="horizontal">
          <h5 style={{ width: '8em' }}>Horizontal line</h5>
          <Button
            title="Show/hide an horizontal line"
            variant={config.displayHLine ? 'primary' : 'secondary'}
            onClick={() => {
              config.setDisplayHLine((b) => !b);
            }}
          >
            <i
              className={`fa ${
                config.displayHLine ? 'fa-eye' : 'fa-eye-slash'
              } fa-fw fa-lg`}
            />
          </Button>
        </Stack>
      </Dropdown.Header>
      <Dropdown.Header>
        <Stack direction="horizontal">
          <h5 style={{ width: '8em' }}>Show axes</h5>
          <Button
            title="Show/hide the axes"
            variant={config.displayAxes ? 'primary' : 'secondary'}
            onClick={() => {
              config.setDisplayAxes(!config.displayAxes);
            }}
          >
            <i
              className={`fa ${
                config.displayAxes ? 'fa-eye' : 'fa-eye-slash'
              } fa-fw fa-lg`}
            />
          </Button>
        </Stack>
      </Dropdown.Header>
      <Dropdown.Header>
        <Stack direction="horizontal">
          <h5 style={{ width: '8em' }}>Axis unit</h5>
          <ButtonGroup>
            <Button
              title="Display axis as pixel"
              variant={config.preferedUnit === 'px' ? 'primary' : 'secondary'}
              onClick={() => {
                config.setPreferedUnit('px');
              }}
            >
              px
            </Button>
            <Button
              title="Display axis as milimeter if possible"
              variant={config.preferedUnit === 'mm' ? 'primary' : 'secondary'}
              onClick={() => {
                config.setPreferedUnit('mm');
              }}
            >
              mm
            </Button>
          </ButtonGroup>
        </Stack>
      </Dropdown.Header>
    </DropdownButton>
  );
}
