import { Dropdown, SplitButton } from 'react-bootstrap';
import { TextArea } from './TextArea';
import { DisplayMode } from './CompareMesh';

export function SelectCustomDisplayMode(props: {
  value: DisplayMode;
  onChanged: (value: DisplayMode) => void;
  customFunc: string;
  onCustomFuncChanged: (value: string) => void;
}) {
  const { value, onChanged, customFunc, onCustomFuncChanged } = props;

  return (
    <SplitButton
      title={
        <i
          title="Advanced blending based on custom webgl code"
          className="fa fa-square-root-variable fa-fw fa-lg"
        />
      }
      variant={value === DisplayMode.CUSTOM ? 'primary' : 'secondary'}
      align={{ lg: 'start' }}
      autoClose={false}
      onClick={() => {
        onChanged(DisplayMode.CUSTOM);
      }}
    >
      <Dropdown.Header>
        <TextArea
          defaultValue={customFunc}
          onChange={(v) => {
            onCustomFuncChanged(v);
          }}
        />
      </Dropdown.Header>
    </SplitButton>
  );
}
