import type {
  ExceptionFromProcedure,
  HardwareFromProcedure,
  ScanFromProcedure,
} from 'components/hardware/procedure';

/**
 * Parameters as defined in the python code
 */
export interface AlignParameters {
  // Motor controlling the translation under the rotation
  lateral_motor: HardwareFromProcedure;

  // Motor controlling the detector tilt
  camera_tilt_motor: HardwareFromProcedure | null;

  // Method used by nabu
  nabu_method?: string;

  // Translation for the motor under the rotation, in mm
  nabu_lateral_corr?: number | null;

  // Rotation for the tilt of the detector, in deg
  nabu_camera_tilt?: number | null;

  // Error message in case of failure of nabu
  nabu_exception?: ExceptionFromProcedure;

  // Translation for the motor under the rotation, in mm
  lateral_corr: number | null;

  // Rotation for the tilt of the detector, in deg
  camera_tilt: number | null;

  // Identify the location of the data inside the scans
  detector_channel: string;

  pixel_size_mm?: number;

  // Scan storing flat
  flat_scan?: ScanFromProcedure;

  // Scan storing dark
  dark_scan?: ScanFromProcedure;

  // Scan storing proj as 0 and 180
  proj_scan?: ScanFromProcedure;

  // Scan storing normalized data for display
  data_scan?: ScanFromProcedure;
}

export interface DescriptionInfo {
  // Motor controlling the translation under the rotation
  lateral_motor: HardwareFromProcedure;

  // Motor controlling the detector tilt
  camera_tilt_motor: HardwareFromProcedure | null;
}

export interface NabuCorrectionInfo {
  // Method used by nabu
  method?: string;

  // Translation for the motor under the rotation, in mm
  lateral_corr?: number | null;

  // Rotation for the tilt of the detector, in deg
  camera_tilt?: number | null;

  // Error message in case of failure of nabu
  exception?: ExceptionFromProcedure;
}

export function extractNabuCorrection(
  parameters: AlignParameters
): NabuCorrectionInfo | null {
  const {
    nabu_method: method,
    nabu_lateral_corr: lateral_corr,
    nabu_camera_tilt: camera_tilt,
    nabu_exception: exception,
  } = parameters;
  if (!method && !lateral_corr && !camera_tilt && !exception) {
    return null;
  }
  return {
    method,
    lateral_corr,
    camera_tilt,
    exception,
  };
}

export function extractDescription(
  parameters: AlignParameters
): DescriptionInfo | null {
  const { lateral_motor, camera_tilt_motor } = parameters;
  if (!lateral_motor && !camera_tilt_motor) {
    return null;
  }
  return {
    lateral_motor,
    camera_tilt_motor,
  };
}
