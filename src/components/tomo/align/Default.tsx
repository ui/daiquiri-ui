import debug from 'debug';
import { useHardware } from '../utils/hooks';
import { useScanData } from 'services/ScanDataService';
import { asFloat32Array } from '../../../helpers/ndarray';
import type { NdArray } from 'ndarray';
import Qty from 'js-quantities';
import type { ArrayStatistics } from '../../h5web/colormap';
import { TomoAlign } from './TomoAlign';
import { assertDisplayableArray2dOrUndefined } from '../../h5web/utils';
import { useMemo } from 'react';
import { useAlignConfig } from '../config/Align';
import type { AlignParameters } from '.';
import { Alert } from 'react-bootstrap';

const logger = debug('daiquiri.components.tomo.TomoAlign');

export interface TomoAlignOptions {
  /** Identifier of the procedure hardware object */
  procedure: string;

  /** If true, the tilt is not handled. No tilt correction will be displayed or changed */
  ignoretilt?: boolean;

  /** Specify a prefered number of decimal to display */
  decimals?: number;

  /** Specify a prefered unit to display the lateral lateral correction */
  lateralunit?: string;
}

interface Props {
  options: TomoAlignOptions;
}

export default function Default(props: Props) {
  const { options } = props;
  const config = useAlignConfig();
  const {
    procedure: procedureId,
    ignoretilt: ignoreTilt = false,
    decimals,
    lateralunit: lateralUnit,
  } = options;
  const procedure = useHardware(procedureId ?? '');
  const parameters: AlignParameters = procedure?.properties?.parameters ?? {};
  const dataId = parameters.data_scan?.mmh3;

  const pixelSize = useMemo(() => {
    if (parameters.pixel_size_mm === undefined) {
      return undefined;
    }
    return new Qty(parameters.pixel_size_mm, 'mm');
  }, [parameters.pixel_size_mm]);

  const dataResult = useScanData({
    scanId: dataId,
    channelNames: [
      'proj0',
      'proj0_min',
      'proj0_max',
      'proj0_min_positive',
      'proj0_mean',
      'proj0_std',
      'proj180',
      'proj180_min',
      'proj180_max',
      'proj180_min_positive',
      'proj180_mean',
      'proj180_std',
    ],
    start: 0,
    stop: 1,
  });

  function getImage(name: string): NdArray<Float32Array> | undefined {
    if (dataResult?.request?.scanId !== dataId) {
      return undefined;
    }
    const array = dataResult.data[name]?.pick(0, null, null);
    return array && asFloat32Array(array);
  }

  function getNumber(name: string): number | null | undefined {
    const array = dataResult.data[name]?.pick(0, null, null);
    if (array === undefined) {
      return undefined;
    }
    const floats = asFloat32Array(array);
    return floats.data[0];
  }

  function getStatisticsFromScan(name: string): ArrayStatistics | undefined {
    const min = getNumber(`${name}_min`);
    const max = getNumber(`${name}_max`);
    const minPositive = getNumber(`${name}_min_positive`);
    const mean = getNumber(`${name}_mean`);
    const std = getNumber(`${name}_std`);
    if (
      min === undefined ||
      max === undefined ||
      minPositive === undefined ||
      mean === undefined ||
      std === undefined
    ) {
      return undefined;
    }
    return {
      min,
      max,
      mean,
      std,
      minPositive,
    };
  }

  const proj0Array = getImage('proj0');
  const proj180Array = getImage('proj180');

  const proj0State = getStatisticsFromScan('proj0');
  const proj180State = getStatisticsFromScan('proj180');

  function useDebounceImageWithState(
    array: NdArray<Float32Array> | undefined,
    state: ArrayStatistics | undefined
  ): [NdArray<Float32Array>, ArrayStatistics] | undefined {
    return useMemo(() => {
      if (array === undefined || state === undefined) {
        return undefined;
      }
      return [array, state];
    }, [array, state]);
  }

  const proj0 = useDebounceImageWithState(proj0Array, proj0State);
  const proj180 = useDebounceImageWithState(proj180Array, proj180State);

  try {
    assertDisplayableArray2dOrUndefined(proj0Array);
    assertDisplayableArray2dOrUndefined(proj180Array);
  } catch {
    return <>Image data unsupported</>;
  }

  if (procedure === null) {
    return (
      <Alert variant="danger">Procedure {procedureId} is not available</Alert>
    );
  }

  return (
    <TomoAlign
      procedureId={dataId?.toString()}
      config={config}
      procedure={procedure}
      parameters={parameters}
      pixelSize={pixelSize}
      imageA={proj0}
      imageB={proj180}
      loading={dataResult.loading}
      ignoreTilt={ignoreTilt}
      decimals={decimals}
      lateralUnit={lateralUnit}
      style={{
        flexGrow: 1,
        flexShrink: 1,
        minWidth: 0,
        minHeight: 0,
        alignSelf: 'stretch',
        width: '100%',
        height: '100%',
      }}
    />
  );
}
