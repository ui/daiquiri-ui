import { Button, ButtonGroup } from 'react-bootstrap';
import { DisplayMode } from './CompareMesh';

export function SelectDisplayMode(props: {
  value: DisplayMode;
  onChanged: (value: DisplayMode) => void;
}) {
  const { value, onChanged } = props;
  return (
    <ButtonGroup>
      <Button
        variant={value === DisplayMode.IMAGE_A ? 'primary' : 'secondary'}
        title="Only display the image A"
        onClick={() => {
          onChanged(DisplayMode.IMAGE_A);
        }}
      >
        <i className="fa fa-a fa-fw fa-lg" />
      </Button>
      <Button
        variant={value === DisplayMode.IMAGE_B ? 'primary' : 'secondary'}
        title="Only display the image B"
        onClick={() => {
          onChanged(DisplayMode.IMAGE_B);
        }}
      >
        <i className="fa fa-b fa-fw fa-lg" />
      </Button>
    </ButtonGroup>
  );
}
