import React from 'react';
import { DropdownButton, Dropdown } from 'react-bootstrap';
import { FilterMode, FilterDefinitions } from './conv2d';

interface Props {
  as?: any;
  value: FilterMode;
  onSelect: (value: FilterMode) => void;
}

export default function DropdownFilterMode(props: Props) {
  const { as, onSelect, value } = props;

  function createItem(key: FilterMode) {
    const { name } = FilterDefinitions[key];
    const icon =
      key === FilterMode.NONE
        ? 'fa fa-fw fa-xmark'
        : value === key
        ? 'fa fa-fw fa-circle-dot'
        : 'fa fa-fw fa-circle';
    return (
      <Dropdown.Item
        eventKey={key}
        onClick={() => {
          onSelect(key);
        }}
      >
        <i className={icon} />
        &nbsp; {name}
      </Dropdown.Item>
    );
  }

  const { name } = FilterDefinitions[value];
  const title =
    value === FilterMode.NONE
      ? 'Apply a convolution filter'
      : `Filter ${name} enabled`;
  return (
    <DropdownButton
      variant={value !== FilterMode.NONE ? 'primary' : 'secondary'}
      as={as}
      title={<i title={title} className="fa fa-filter fa-fw fa-lg" />}
      id="bg-vertical-dropdown-6"
    >
      {createItem(FilterMode.NONE)}
      {createItem(FilterMode.LAPLACIAN_3x3)}
      {createItem(FilterMode.LAPLACIAN_9x9)}
      {createItem(FilterMode.SHARPENING_3x3)}
      {createItem(FilterMode.UNSHARPENING_3x3)}
      {createItem(FilterMode.UNSHARPENING_5x5)}
      {createItem(FilterMode.SMOOTH_3x3)}
      {createItem(FilterMode.SMOOTH_5x5)}
      {createItem(FilterMode.SMOOTH_7x7)}
    </DropdownButton>
  );
}
