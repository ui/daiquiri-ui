import { useRef } from 'react';
import { Form } from 'react-bootstrap';
import type Qty from 'js-quantities';

export function InputQuantityRange(props: {
  value?: number;
  min: number;
  max: number;
  step: number;
  onChange?: (value: number) => void;
  onIntermediateChange?: (value: number) => void;
  style?: Record<string, any>;
  className?: string;
}) {
  const { style, className } = props;
  const draggingState = useRef({ value: 0, dragging: false });

  return (
    <Form.Range
      className={className}
      style={style}
      min={props.min}
      max={props.max}
      value={props.value}
      step={props.step}
      onChange={(e) => {
        const value = Number.parseFloat(e.target.value);
        draggingState.current.value = value;
        if (draggingState.current.dragging) {
          props.onIntermediateChange?.(value);
        } else {
          props.onChange?.(value);
        }
      }}
      onMouseDown={(e) => {
        draggingState.current.dragging = true;
      }}
      onMouseUp={(e) => {
        draggingState.current.dragging = false;
        props.onChange?.(draggingState.current.value);
      }}
    />
  );
}
