import { useEffect, useMemo, useRef } from 'react';
import { Suspense, useState } from 'react';
import { Stack, Button, ButtonGroup, Collapse } from 'react-bootstrap';
import Toolbar, { Separator } from 'components/common/Toolbar';
import Form from 'react-bootstrap/Form';
import { FilterMode } from './conv2d';
import { CompareMesh, DisplayMode } from './CompareMesh';
import { InputQuantity } from './InputQuantity';
import { InputQuantityRange } from './InputQuantityRange';
import { VLineRoi } from '../../h5web/items/rois/VLineRoi';
import { HLineRoi } from '../../h5web/items/rois/HLineRoi';
import type { LinearVisCanvasRef } from '../../h5web/LinearVisCanvas2';
import { LinearVisCanvas } from '../../h5web/LinearVisCanvas2';
import DropdownFilterMode from './DropdownFilterMode';
import { SceneScale } from '../../h5web/items/SceneScale';
import useUndoableWithIntermediate from './useUndoableWithIntermediate';
import type Qty from 'js-quantities';
import type { ArrayStatistics } from '../../h5web/colormap';
import type { NdArray } from 'ndarray';
import type { NdFloat16Array } from '../../h5web/NdFloat16Array';
import RulerSelectionTool from '../../h5web/RulerSelectionTool';
import type { NdNormalizedUint8Array } from '../../h5web/NdNormalizedUint8Array';
import DropdownTomoAlignOptions from './DropdownTomoAlignOptions';
import { useUndoableViewpoint } from '../../h5web/hooks';
import type { AlignConfig } from '../config/Align';
import DefaultMouseModeOptions from '../../h5web/bootstrap/DefaultMouseModeOptions';
import { SelectDisplayMode } from './SelectDisplayMode';
import { SelectMixedDisplayMode } from './SelectMixedDisplayMode';
import { SelectCustomDisplayMode } from './SelectCustomDisplayMode';
import { RulerButton } from '../../h5web/bootstrap/RulerButton';
import { useMouseModeInteraction } from '../../h5web/UseMouseModeInteraction';
import {
  extractNabuCorrection,
  type AlignParameters,
  extractDescription,
} from '.';
import type { Hardware } from '@esrf/daiquiri-lib';
import { useHardwareChangeActions } from '../../utils/hooks';
import { Estimation } from './Estimation';
import { Description } from './Description';
import { LoadingMessage } from '../../h5web/items/LoadingMessage';

type MouseModes =
  | 'pan'
  | 'zoom'
  | 'measure-distance'
  | 'measure-ortho'
  | 'measure-angle'
  | 'move-lateral';

export function TomoAlign(props: {
  style?: Record<string, any>;
  procedureId: string | undefined;
  procedure: Hardware;
  parameters: AlignParameters;
  pixelSize?: Qty;
  loading?: boolean;
  ignoreTilt?: boolean;
  decimals?: number;
  lateralUnit?: string;
  imageA?: [
    NdArray<Float32Array> | NdFloat16Array | NdNormalizedUint8Array,
    ArrayStatistics
  ];
  imageB?: [
    NdArray<Float32Array> | NdFloat16Array | NdNormalizedUint8Array,
    ArrayStatistics
  ];
  config: AlignConfig;
}) {
  const {
    imageA,
    imageB,
    pixelSize,
    config,
    parameters,
    procedure,
    ignoreTilt = false,
    decimals = 2,
    lateralUnit = 'um',
  } = props;
  const [mixedMode, setMixedMode] = useState(DisplayMode.MIN_COLOR);
  const [displayMode, setDisplayMode] = useState(DisplayMode.MIN_COLOR);
  const plotRef = useRef<LinearVisCanvasRef>(null);
  const [roiX, setRoiX] = useState(0);
  const [roiY, setRoiY] = useState(0);
  const mouseModeInteraction = useMouseModeInteraction<MouseModes>('zoom');
  const { mouseMode } = mouseModeInteraction;
  const [mouseOverlay, setMouseOverlay] = useState<string | undefined>(
    undefined
  );
  const [filterMode, setFilterMode] = useState(FilterMode.NONE);
  const [verticalSeparator, setVerticalSeparator] = useState(0);
  const [customFunc, setCustomFunc] = useState(`\
// a: Intensity from image A, [0..1], -1 = out
// b: Intensity from image B, [0..1], -1 = out
// return: R, G, B, alpha values, [0..1]
return vec4(a, b, 0.0, 1.0);`);

  const undoableViewpoint = useUndoableViewpoint();

  const actions = useHardwareChangeActions(procedure.id);

  const [correction, setCorrection, history] = useUndoableWithIntermediate({
    lateral: 0,
    tilt: 0,
  });

  useEffect(() => {
    history.reset({
      lateral: parameters.nabu_lateral_corr ?? 0,
      tilt:
        (ignoreTilt ? 0 : parameters.nabu_camera_tilt ?? 0) * (Math.PI / 180),
    });
  }, [parameters.nabu_lateral_corr ?? 0, parameters.nabu_camera_tilt ?? 0]);

  const { tilt, lateral } = correction;

  const pixelSizeMm = useMemo(() => {
    if (pixelSize === undefined) {
      return undefined;
    }
    if (config.preferedUnit === 'px') {
      return undefined;
    }
    return pixelSize.to('mm').scalar;
  }, [config.preferedUnit, pixelSize]);

  function setIntermediateTilt(value: number) {
    setCorrection({ tilt: value }, true);
  }

  function setIntermediateLateral(value: number) {
    setCorrection({ lateral: value }, true);
  }

  function setTilt(value: number) {
    setCorrection({ tilt: value }, false);
  }

  function setLateral(value: number) {
    setCorrection({ lateral: value }, false);
  }

  useEffect(() => {
    plotRef.current?.actions.clearLater();
  }, [imageA, imageB]);

  const [height, width] = imageA?.[0]?.shape ?? [1024, 1024];

  const { abscissaConfig, ordinateConfig } = useMemo(() => {
    const unit = pixelSizeMm ? 'mm' : 'px';
    const coef = pixelSizeMm ?? 1;

    return {
      abscissaConfig: {
        visDomain: [-width * coef * 0.8, width * coef * 0.8] as [
          number,
          number
        ],
        label: `x (${unit})`,
      },
      ordinateConfig: {
        visDomain: [-height * coef * 0.6, height * coef * 0.6] as [
          number,
          number
        ],
        label: `y (${unit})`,
      },
    };
  }, [width, height, pixelSizeMm]);

  const waitingUserInput = procedure.properties.state === 'AWAITING_USER_INPUT';
  const aborting = procedure.properties.state === 'ABORTING';
  const isRunning =
    procedure.properties.state === 'RUNNING' || waitingUserInput || aborting;
  const nabuCorrection = extractNabuCorrection(parameters);
  const description = extractDescription(parameters);

  useEffect(() => {
    if (
      !waitingUserInput &&
      mouseModeInteraction.mouseMode === 'move-lateral'
    ) {
      // Disable the mous einteraction because it is not reachable anymore
      mouseModeInteraction.resetMouseMode();
    }
  }, [waitingUserInput]);

  return (
    <Stack
      direction="horizontal"
      gap={2}
      onKeyDown={(e) => {
        if (e.key === ' ') {
          setDisplayMode((mode) => {
            if (mode === DisplayMode.IMAGE_A) {
              return DisplayMode.IMAGE_B;
            }
            return DisplayMode.IMAGE_A;
          });
          e.preventDefault();
        }
        if (e.key === 'ArrowLeft') {
          setLateral(lateral - Number(props.pixelSize?.scalar ?? 0) * 0.5);
          e.preventDefault();
        }
        if (e.key === 'ArrowRight') {
          setLateral(lateral + Number(props.pixelSize?.scalar ?? 0) * 0.5);
          e.preventDefault();
        }
      }}
      tabIndex={-1}
      style={props.style}
    >
      <Stack
        direction="vertical"
        style={{ alignSelf: 'stretch', flexGrow: 4, minWidth: 0 }}
      >
        <Toolbar align="center" style={{ zIndex: 1 }}>
          <DefaultMouseModeOptions
            mouseModeInteraction={mouseModeInteraction}
          />
          <ButtonGroup>
            <Button
              title="Reset zoom"
              variant="secondary"
              onClick={() => {
                plotRef?.current?.actions.resetZoom();
              }}
            >
              <i className="fa fa-expand fa-fw fa-lg" />
            </Button>
            <Button
              disabled={!undoableViewpoint[2].canUndo}
              title="Undo the last changes applied to the corrections"
              onClick={() => {
                undoableViewpoint[2].undo();
              }}
            >
              <i className="fa fa-reply fa-fw fa-lg" />
            </Button>
            <Button
              disabled={!undoableViewpoint[2].canRedo}
              title="Redo the last reverted changes applied to the corrections"
              onClick={() => {
                undoableViewpoint[2].redo();
              }}
            >
              <i className="fa fa-share fa-fw fa-lg" />
            </Button>
          </ButtonGroup>
          <Separator />
          <RulerButton mouseModeInteraction={mouseModeInteraction} />
          <SelectDisplayMode
            value={displayMode}
            onChanged={(v) => {
              setDisplayMode(v);
            }}
          />
          <SelectMixedDisplayMode
            value={displayMode}
            onChanged={(v) => {
              setDisplayMode(v);
            }}
            mixedMode={mixedMode}
            onMixedModeChanged={setMixedMode}
          />
          <SelectCustomDisplayMode
            value={displayMode}
            onChanged={(v) => {
              setDisplayMode(v);
            }}
            customFunc={customFunc}
            onCustomFuncChanged={(v) => {
              setCustomFunc(v);
            }}
          />
          <DropdownFilterMode
            value={filterMode}
            onSelect={(v) => {
              setFilterMode(v);
            }}
          />
          <Separator />
          <DropdownTomoAlignOptions config={config} />
        </Toolbar>
        <Suspense fallback={null}>
          <LinearVisCanvas
            undoableViewpoint={undoableViewpoint}
            plotRef={plotRef}
            abscissaConfig={abscissaConfig}
            ordinateConfig={ordinateConfig}
            aspect="equal"
            showAxes={config.displayAxes}
            onMouseInteractionOverlayChanged={setMouseOverlay}
            cursors={{ 'move-lateral': 'ew-resize' }}
            mouseMode={mouseMode}
          >
            <SceneScale unit={pixelSizeMm === undefined ? 'px' : 'mm'} />
            <LoadingMessage
              loading={props.loading}
              warning={
                imageA === undefined || imageB === undefined
                  ? 'No data'
                  : undefined
              }
            />
            {imageA && imageB && (
              <CompareMesh
                imageA={imageA[0]}
                imageB={imageB[0]}
                stateA={imageA[1]}
                stateB={imageB[1]}
                displayMode={displayMode}
                lateral={(-lateral * 2) / (props.pixelSize?.scalar ?? 0)}
                pixelSize={
                  config.preferedUnit !== 'px' ? props.pixelSize : undefined
                }
                onLateralChanged={(v) => {
                  setLateral(-v * (props.pixelSize?.scalar ?? 0));
                }}
                onLateralIntermediateChanged={(v) => {
                  setIntermediateLateral(-v * (props.pixelSize?.scalar ?? 0));
                }}
                tilt={tilt}
                autoScale={config.autoscaleMode}
                verticalSeparator={verticalSeparator}
                customFunc={customFunc}
                filterMode={filterMode}
                lateralInteration={
                  mouseOverlay === undefined && mouseMode === 'move-lateral'
                }
              />
            )}
            <VLineRoi
              geometry={{ x: roiX }}
              onGeometryChanged={(p) => {
                setRoiX(p.x);
              }}
              visible={config.displayVLine}
            />
            <HLineRoi
              geometry={{ y: roiY }}
              onGeometryChanged={(p) => {
                setRoiY(p.y);
              }}
              visible={config.displayHLine}
            />
            <HLineRoi
              geometry={{ y: verticalSeparator }}
              color="red"
              onGeometryChanged={(p) => {
                setVerticalSeparator(p.y);
              }}
              visible={
                displayMode === DisplayMode.HSPLITTER ||
                displayMode === DisplayMode.HSPLITTER_COLOR
              }
            />
            <RulerSelectionTool
              mouseMode={mouseMode}
              disabled={mouseOverlay !== undefined}
              plotUnit={pixelSizeMm === undefined ? 'px' : 'mm'}
            />
          </LinearVisCanvas>
        </Suspense>
      </Stack>
      <Stack
        direction="vertical"
        gap={2}
        className="m-1"
        style={{ width: '20em' }}
      >
        <Stack
          style={{ minWidth: '20em' }}
          className="mt-4"
          direction="vertical"
          gap={2}
        >
          <Collapse in={!!description} dimension="height">
            <div>
              <h4>Description</h4>
              <Description description={description} />
            </div>
          </Collapse>
          <Collapse in={!!nabuCorrection} dimension="height">
            <div>
              <h4>Estimation</h4>
              <Estimation
                nabuCorrection={nabuCorrection}
                lateralUnit={lateralUnit}
                decimals={decimals}
              />
            </div>
          </Collapse>
          <Collapse in={waitingUserInput} dimension="height">
            <div>
              <h4>Correction</h4>
              <Stack className="mb-2" direction="horizontal">
                <Button
                  className="me-1"
                  variant={
                    mouseMode === 'move-lateral' ? 'primary' : 'secondary'
                  }
                  title="Shift the two images to compare and edit the ROIs"
                  onClick={() => {
                    mouseModeInteraction.setOrResetMouseMode('move-lateral');
                  }}
                >
                  <i className="fa fa-arrow-right-arrow-left fa-fw fa-lg" />
                </Button>
                <ButtonGroup>
                  <Button
                    disabled={!history.canUndo}
                    title="Undo the last changes applied to the corrections"
                    onClick={() => {
                      history.undo();
                    }}
                  >
                    <i className="fa fa-reply fa-fw fa-lg" />
                  </Button>
                  <Button
                    disabled={!history.canRedo}
                    title="Redo the last reverted changes applied to the corrections"
                    onClick={() => {
                      history.redo();
                    }}
                  >
                    <i className="fa fa-share fa-fw fa-lg" />
                  </Button>
                </ButtonGroup>
              </Stack>
              <Stack direction="horizontal" gap={2}>
                <Form.Label style={{ width: '5em' }}>Lateral</Form.Label>
                <Stack direction="vertical" gap={2}>
                  <InputQuantity
                    value={lateral}
                    unit="mm"
                    preferedDisplayUnit={lateralUnit}
                    step={0.01}
                    decimals={decimals}
                    onChange={(value) => {
                      setLateral(value);
                    }}
                    onIntermediateChange={(value) => {
                      setIntermediateLateral(value);
                    }}
                  />
                  <InputQuantity
                    value={lateral}
                    unit="mm"
                    decimals={decimals}
                    preferedDisplayUnit="px"
                    pixelSize={props.pixelSize}
                    step={0.01}
                    onChange={(value) => {
                      setLateral(value);
                    }}
                    onIntermediateChange={(value) => {
                      setIntermediateLateral(value);
                    }}
                  />
                  <InputQuantityRange
                    min={-200 * (props.pixelSize?.scalar ?? 0)}
                    max={200 * (props.pixelSize?.scalar ?? 0)}
                    value={lateral}
                    step={0.01}
                    onChange={(value) => {
                      setLateral(value);
                    }}
                    onIntermediateChange={(value) => {
                      setIntermediateLateral(value);
                    }}
                  />
                </Stack>
              </Stack>
              {!ignoreTilt && (
                <Stack direction="horizontal" gap={2}>
                  <Form.Label style={{ width: '5em' }}>Tilt</Form.Label>
                  <Stack direction="vertical" gap={2}>
                    <InputQuantity
                      step={0.01}
                      value={tilt}
                      unit="rad"
                      decimals={decimals}
                      displayUnit="deg"
                      onChange={(value) => {
                        setTilt(value);
                      }}
                      onIntermediateChange={(value) => {
                        setIntermediateTilt(value);
                      }}
                    />
                    <InputQuantityRange
                      min={-1}
                      max={1}
                      step={0.01}
                      value={tilt}
                      onChange={(value) => {
                        setTilt(value);
                      }}
                      onIntermediateChange={(value) => {
                        setIntermediateTilt(value);
                      }}
                    />
                  </Stack>
                </Stack>
              )}
              <Stack
                className="mx-auto mt-4 justify-content-center"
                direction="horizontal"
                gap={2}
              >
                <Button
                  variant="danger"
                  size="lg"
                  disabled={!waitingUserInput}
                  onClick={() => {
                    actions.call('validate', {
                      lateral_corr: lateral,
                      camera_tilt: ignoreTilt ? null : (tilt * 180) / Math.PI,
                    });
                  }}
                  title="This will apply the correction to the axes of the beamline"
                >
                  Apply corrections
                </Button>
                <Button
                  size="lg"
                  onClick={() => {
                    actions.call('abort');
                  }}
                  disabled={!isRunning}
                >
                  Abort
                </Button>
              </Stack>
            </div>
          </Collapse>
        </Stack>
      </Stack>
    </Stack>
  );
}
