import { useMemo, useState } from 'react';
import useUndoable from 'use-undoable';

/**
 * A useUndoable hook featuring an intermediate present state
 * which is not yet archived in the history.
 *
 * This simplifies a lot interaction between multiple components.
 */
export default function useUndoableWithIntermediate<T>(props: T): [
  T,
  (state: Partial<T>, intermediate?: boolean) => void,
  {
    past: T[];
    future: T[];
    undo: () => void;
    canUndo: boolean;
    redo: () => void;
    canRedo: boolean;
    reset: (initialState?: T) => void;
    resetInitialState: (newInitialState: T) => void;
  }
] {
  const [present, setPresent, actions] = useUndoable<T>(props, {
    behavior: 'destroyFuture',
  });
  const [intermediate, setIntermediate] = useState<Partial<T> | undefined>(
    undefined
  );

  const state = useMemo(() => {
    return { ...present, ...intermediate };
  }, [present, intermediate]);

  function setState(value: Partial<T>, intermediate?: boolean) {
    if (intermediate) {
      setIntermediate((state) => {
        return { ...state, ...value };
      });
    } else {
      setPresent((state: T) => {
        return { ...state, ...value };
      });
      setIntermediate(() => undefined);
    }
  }

  return [state, setState, actions];
}
