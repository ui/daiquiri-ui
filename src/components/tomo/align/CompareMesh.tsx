import React, { useCallback, useMemo } from 'react';
import { useState, useEffect, useRef } from 'react';
import { useResourceAsProgram, useDataTexture } from '../../h5web/shaderUtils';
import { FilterMode, FilterDefinitions } from './conv2d';
import type { NdArray } from 'ndarray';
import type { Vector3 } from 'three';
import { ShaderMaterial } from 'three';
import type { CanvasEvent } from '@h5web/lib';
import { useCanvasEvents } from '@h5web/lib';
import { extend, invalidate } from '@react-three/fiber';
import type Qty from 'js-quantities';
import type { NdFloat16Array } from '../../h5web/NdFloat16Array';
import { NdNormalizedUint8Array } from '../../h5web/NdNormalizedUint8Array';
import { AutoscaleMode } from '../../h5web/colormap';
import type { ArrayStatistics } from '../../h5web/colormap';
import debug from 'debug';

const logger = debug('daiquiri.components.tomo.TomoAlign');

export enum DisplayMode {
  // Render the image A
  IMAGE_A = 0,
  // Render the image B
  IMAGE_B = 1,
  // Render mixed result as intensity
  DIFF = 100,
  ERROR = 101,
  MAX = 102,
  MIN = 103,
  MUL = 104,
  HSPLITTER = 105,
  // Render mixed result as channel conbination
  DIFF_COLOR = 200,
  ERROR_COLOR = 201,
  MAX_COLOR = 202,
  MIN_COLOR = 203,
  MUL_COLOR = 204,
  HSPLITTER_COLOR = 205,
  // Render a custom inhected shader function
  CUSTOM = 300,
}

class CompareMaterial extends ShaderMaterial {
  public constructor() {
    super({
      uniforms: {
        textureA: { value: null },
        normA: { value: { min: 0, max: 0, mode: 0 } },
        textureB: { value: null },
        normB: { value: { min: 0, max: 0, mode: 0 } },
        mode: { value: 0 },
        sizeCoef: { value: 1 },
        lateral: { value: 0 },
        tilt: { value: 0 },
        domain: { value: { min: 0, max: 1 } },
        verticalSeparator: { value: 0 },
        kernelCoefs: { value: Float32Array.from([0, 0, 0, 0, 0, 0, 0, 0, 0]) },
        kernelSize: { value: 0 },
      },
    });
  }
}

extend({ CompareMaterial });

function directionFromMode(
  dataPt: Vector3,
  displayMode: DisplayMode,
  verticalSeparator: number
): number {
  if (displayMode === DisplayMode.IMAGE_A) {
    return -1;
  }
  if (
    displayMode === DisplayMode.HSPLITTER ||
    displayMode === DisplayMode.HSPLITTER_COLOR
  ) {
    if (dataPt.y < verticalSeparator) {
      return -1;
    }
  }
  return 1;
}

export function DragLateral(props: {
  threshold?: number;
  lateral: number;
  verticalSeparator: number;
  displayMode: DisplayMode;
  onLateralChanged: (value: number) => void;
  onLateralIntermediateChanged: (value: number) => void;
  disabled?: boolean;
}) {
  const {
    lateral,
    threshold = 4,
    displayMode,
    verticalSeparator,
    disabled,
  } = props;
  const gestureRef = useRef<
    | {
        lateral: number;
        startData: Vector3;
        startScreen: Vector3;
        gestureStarted: boolean;
        coef: number;
      }
    | undefined
  >(undefined);

  const onPointerDown = useCallback(
    (event: CanvasEvent<PointerEvent>) => {
      const coef = directionFromMode(
        event.dataPt,
        displayMode,
        verticalSeparator
      );
      gestureRef.current = {
        lateral,
        startData: event.dataPt,
        startScreen: event.htmlPt,
        gestureStarted: false,
        coef,
      };
    },
    [lateral, displayMode, verticalSeparator]
  );

  const isCloseToStart = useCallback(
    (evt: CanvasEvent<PointerEvent>) => {
      const gesture = gestureRef.current;
      if (gesture === undefined) {
        return false;
      }
      const { htmlPt } = evt;
      const dist = Math.max(
        Math.abs(htmlPt.x - gesture.startScreen.x),
        Math.abs(htmlPt.y - gesture.startScreen.y)
      );
      return dist <= threshold;
    },
    [threshold]
  );

  const onPointerMove = useCallback(
    (evt: CanvasEvent<PointerEvent>) => {
      const gesture = gestureRef.current;
      if (gesture === undefined) {
        return;
      }
      if (!gesture.gestureStarted) {
        if (isCloseToStart(evt)) {
          return;
        }
        gestureRef.current = { ...gesture, gestureStarted: true };
      }
      const delta = evt.dataPt.x - gesture.startData.x;
      props.onLateralIntermediateChanged(
        gesture.lateral + delta * 2 * gesture.coef
      );
    },
    [isCloseToStart, props.onLateralIntermediateChanged]
  );

  const onPointerUp = useCallback(
    (evt: CanvasEvent<PointerEvent>) => {
      const gesture = gestureRef.current;
      if (gesture) {
        const delta = evt.dataPt.x - gesture.startData.x;
        props.onLateralChanged(gesture.lateral + delta * 2 * gesture.coef);
      }
      gestureRef.current = undefined;
    },
    [isCloseToStart, props.onLateralChanged]
  );

  // Hold the hook inside a component to  be able to drop it
  function Foo() {
    useCanvasEvents({ onPointerDown, onPointerMove, onPointerUp });
    return <></>;
  }

  return <>{!disabled && <Foo />}</>;
}

export function CompareMesh(props: {
  imageA:
    | NdArray<Float32Array>
    | NdFloat16Array
    | NdNormalizedUint8Array
    | undefined;
  imageB:
    | NdArray<Float32Array>
    | NdFloat16Array
    | NdNormalizedUint8Array
    | undefined;
  stateA: ArrayStatistics | undefined;
  stateB: ArrayStatistics | undefined;
  displayMode?: DisplayMode;
  zoom?: number;
  lateral?: number;
  onLateralChanged?: (value: number) => void;
  onLateralIntermediateChanged?: (value: number) => void;
  tilt?: number;
  autoScale?: AutoscaleMode;
  verticalSeparator?: number;
  customFunc?: string;
  filterMode?: FilterMode;
  lateralInteration?: boolean;
  pixelSize?: Qty;
}) {
  const materialRef = useRef<CompareMaterial>(null);
  const {
    displayMode = 1,
    lateral = 0,
    tilt = 0,
    autoScale = AutoscaleMode.None,
    stateA,
    stateB,
    verticalSeparator = 0,
    customFunc,
    filterMode = FilterMode.NONE,
    lateralInteration = false,
  } = props;
  const vertexShader = useResourceAsProgram('compare.vert.glsl')?.data;
  const rawFragmentShader = useResourceAsProgram('compare.frag.glsl')?.data;
  const [version, setVersion] = useState(0);

  const sizeCoef = useMemo(() => {
    if (props.pixelSize === undefined) {
      return 1;
    }
    return props.pixelSize.to('mm').scalar;
  }, [props.pixelSize]);

  const fragmentShader = useMemo(() => {
    if (customFunc === undefined) {
      return rawFragmentShader;
    }
    if (rawFragmentShader === undefined) {
      return undefined;
    }
    return rawFragmentShader.replace('// CUSTOM FUNC INJECTION //', customFunc);
  }, [rawFragmentShader, customFunc]);

  const textureA = useDataTexture(props.imageA, { magNearest: true });
  const textureB = useDataTexture(props.imageB, { magNearest: true });

  const [kernelSize, kernelCoefs] = useMemo(() => {
    if (filterMode in FilterDefinitions) {
      const { size, coefs } = FilterDefinitions[filterMode];
      return [size, coefs];
    }
    console.log(`Unsupported ${filterMode}`);
    const { size, coefs } = FilterDefinitions[FilterMode.NONE];
    return [size, coefs];
  }, [filterMode]);

  // Track the version of the shader programs in order to invalidate the object
  useEffect(() => {
    setVersion((v) => v + 1);
  }, [fragmentShader, vertexShader]);

  const domain = useMemo(() => {
    if (stateA === undefined || stateB === undefined) {
      return { min: 0, max: 1 };
    }
    switch (autoScale) {
      case AutoscaleMode.None:
        return { min: 0, max: 1 };
      case AutoscaleMode.Minmax:
        return {
          min: Math.min(stateA.min ?? 0, stateB.min ?? 0),
          max: Math.max(stateA.max ?? 1, stateB.max ?? 1),
        };
      case AutoscaleMode.StdDev3: {
        const min = Math.min(stateA.min ?? 0, stateB.min ?? 0);
        const max = Math.max(stateA.max ?? 1, stateB.max ?? 1);
        const mean = ((stateA.mean ?? 0.5) + (stateB.mean ?? 0.5)) * 0.5;
        const std = ((stateA.std ?? 0.5) + (stateB.std ?? 0.5)) * 0.5;
        return {
          min: Math.max(min, mean - 3 * std),
          max: Math.min(max, mean + 3 * std),
        };
      }
      default:
        console.warn(`Unsupported norm ${autoScale}`);
    }
    return { min: 0, max: 1 };
  }, [stateA, stateB, autoScale]);

  useEffect(() => {
    if (materialRef.current) {
      const uniforms = materialRef.current.uniforms;
      uniforms.mode.value = displayMode;
      uniforms.sizeCoef.value = sizeCoef;
      uniforms.tilt.value = tilt;
      uniforms.lateral.value = lateral;
      uniforms.domain.value = domain;
      uniforms.verticalSeparator.value = verticalSeparator;
      uniforms.kernelCoefs.value = kernelCoefs;
      uniforms.kernelSize.value = kernelSize;
      invalidate();
    }
  }, [
    materialRef.current,
    displayMode,
    sizeCoef,
    lateral,
    tilt,
    stateA,
    stateB,
    domain,
    verticalSeparator,
    kernelCoefs,
    kernelSize,
    version,
  ]);

  function getNorm(image: any) {
    if (image instanceof NdNormalizedUint8Array) {
      return {
        min: image.transfer.min,
        max: image.transfer.max,
        mode: 1,
      };
    }
    return { min: 0, max: 0, mode: 0 };
  }

  useEffect(() => {
    if (materialRef.current) {
      const uniforms = materialRef.current.uniforms;
      uniforms.textureA.value = textureA;
      uniforms.normA.value = getNorm(props.imageA);
      invalidate();
    }
  }, [version, textureA, materialRef.current]);

  useEffect(() => {
    if (materialRef.current) {
      const uniforms = materialRef.current.uniforms;
      uniforms.textureB.value = textureB;
      uniforms.normB.value = getNorm(props.imageB);
      invalidate();
    }
  }, [version, textureB, materialRef.current]);

  const [height, width] = props.imageA?.shape ?? [1024, 1024];

  useEffect(() => {
    logger(
      `Debug vertexShader?:${vertexShader !== undefined} fragmentShader?:${
        fragmentShader !== undefined
      } textureA?:${textureA !== undefined} textureB?:${textureB !== undefined}`
    );
  }, [vertexShader, fragmentShader, textureA, textureB]);

  if (
    vertexShader === undefined ||
    fragmentShader === undefined ||
    textureA === undefined ||
    textureB === undefined
  ) {
    return <></>;
  }

  return (
    <mesh>
      <planeGeometry
        attach="geometry"
        args={[width * sizeCoef * 2, height * sizeCoef * 2, 1, 1]}
      />
      <DragLateral
        lateral={lateral * sizeCoef}
        onLateralChanged={(v) => {
          props.onLateralChanged?.((0.5 * v) / sizeCoef);
        }}
        onLateralIntermediateChanged={(v) => {
          props.onLateralIntermediateChanged?.((0.5 * v) / sizeCoef);
        }}
        threshold={0}
        displayMode={displayMode}
        verticalSeparator={verticalSeparator}
        disabled={!lateralInteration}
      />
      {/* @ts-expect-error */}
      <compareMaterial
        key={version}
        attach="material"
        ref={materialRef}
        vertexShader={vertexShader}
        fragmentShader={fragmentShader}
      />
    </mesh>
  );
}
