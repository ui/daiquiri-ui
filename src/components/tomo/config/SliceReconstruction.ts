import type { ColorMap } from '@h5web/lib';
import type { ScaleType } from '@h5web/lib';
import useUserPreference from 'components/utils/UseUserPreference';
import type { Dispatch, SetStateAction } from 'react';
import type { AutoscaleMode } from 'components/h5web/colormap';
import { useColorMapConfig } from './ColorMap';

export interface SliceReconstructionConfig {
  setDeltaBeta: Dispatch<SetStateAction<number>>;
  deltaBeta: number;
  setAutoscale: Dispatch<SetStateAction<boolean>>;
  autoscale: boolean;
  setAutoscaleMode: Dispatch<SetStateAction<AutoscaleMode>>;
  autoscaleMode: AutoscaleMode;
  setScaleDomain: Dispatch<SetStateAction<[number, number]>>;
  scaleDomain: [number, number];
  setInvertColorMap: Dispatch<SetStateAction<boolean>>;
  invertColorMap: boolean;
  setColorMap: Dispatch<SetStateAction<ColorMap>>;
  colorMap: ColorMap;
  scaleType: ScaleType;
  setDisplayAxes: Dispatch<SetStateAction<boolean>>;
  displayAxes: boolean;
  setCrossHair: Dispatch<SetStateAction<boolean>>;
  crossHair: boolean;
}

/**
 * Returns the main user configuration related to the slice reconstruction view display
 */
export function useSliceReconstructionConfig(): SliceReconstructionConfig {
  const colormap = useColorMapConfig({ name: 'tomo/slicecolormap' });
  const [deltaBeta, setDeltaBeta] = useUserPreference<number>(
    'tomo/slicereconstruction/delta-beta',
    200
  );
  const [displayAxes, setDisplayAxes] = useUserPreference<boolean>(
    'tomo/sliceview/axes',
    true
  );
  const [crossHair, setCrossHair] = useUserPreference<boolean>(
    'tomo/sliceview/crosshair',
    false
  );

  return {
    deltaBeta,
    setDeltaBeta,
    ...colormap,
    displayAxes,
    setDisplayAxes,
    crossHair,
    setCrossHair,
  };
}
