import type { ColorMap } from '@h5web/lib';
import type { ScaleType } from '@h5web/lib';
import useUserPreference from 'components/utils/UseUserPreference';
import type { Dispatch, SetStateAction } from 'react';
import type { AutoscaleMode } from '../../h5web/colormap';
import { useColorMapConfig } from './ColorMap';

type LayoutViews = 'front' | 'side' | 'front-side';

type ToolbarLocation = 'top' | 'bottom';

export interface TilingViewConfig {
  setLayoutView: Dispatch<SetStateAction<LayoutViews>>;
  layoutView: LayoutViews;
  setColorbarVisible: Dispatch<SetStateAction<boolean>>;
  colorbarVisible: boolean;
  setDisplayAxes: Dispatch<SetStateAction<boolean>>;
  displayAxes: boolean;
  setCrossHair: Dispatch<SetStateAction<boolean>>;
  crossHair: boolean;
  setAutoscale: Dispatch<SetStateAction<boolean>>;
  autoscale: boolean;
  setAutoscaleMode: Dispatch<SetStateAction<AutoscaleMode>>;
  autoscaleMode: AutoscaleMode;
  setScaleDomain: Dispatch<SetStateAction<[number, number]>>;
  scaleDomain: [number, number];
  setInvertColorMap: Dispatch<SetStateAction<boolean>>;
  invertColorMap: boolean;
  setColorMap: Dispatch<SetStateAction<ColorMap>>;
  colorMap: ColorMap;
  setScaleType: Dispatch<SetStateAction<ScaleType>>;
  scaleType: ScaleType;
  setToolbarLocation: Dispatch<SetStateAction<ToolbarLocation>>;
  toolbarLocation: ToolbarLocation;
  setDetectorDisplayed: Dispatch<SetStateAction<boolean>>;
  isDetectorDisplayed: boolean;
}

/**
 * Returns the main user configuration related to the tiling view display
 */
export function useTilingViewConfig(): TilingViewConfig {
  const colormap = useColorMapConfig({
    name: 'tomo/tilingcolormap',
    lut: 'Greys',
  });
  const [layoutView, setLayoutView] = useUserPreference<LayoutViews>(
    'tomo/tilingview/layout-view',
    'front-side'
  );
  const [displayAxes, setDisplayAxes] = useUserPreference<boolean>(
    'tomo/tilingview/axes',
    true
  );
  const [colorbarVisible, setColorbarVisible] = useUserPreference<boolean>(
    'tomo/tilingview/colorbar-visible',
    true
  );
  const [crossHair, setCrossHair] = useUserPreference<boolean>(
    'tomo/tilingview/crosshair',
    false
  );
  const [toolbarLocation, setToolbarLocation] =
    useUserPreference<ToolbarLocation>(
      'tomo/tilingview/toolbarlocation',
      'top'
    );
  const [isDetectorDisplayed, setDetectorDisplayed] =
    useUserPreference<boolean>('tomo/tilingview/detectordisplayed', false);

  return {
    layoutView,
    setLayoutView,
    colorbarVisible,
    setColorbarVisible,
    displayAxes,
    setDisplayAxes,
    crossHair,
    setCrossHair,
    toolbarLocation,
    setToolbarLocation,
    ...colormap,
    isDetectorDisplayed,
    setDetectorDisplayed,
  };
}
