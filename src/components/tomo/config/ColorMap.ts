import { ScaleType } from '@h5web/lib';
import type { ColorMap, Domain } from '@h5web/lib';
import useUserPreference from 'components/utils/UseUserPreference';
import { ImageProfile } from 'services/TomoDetectorService';
import { AutoscaleMode } from 'components/h5web/colormap';

/**
 * User preferences for a colormap
 */
export function useColorMapConfig(props: {
  name: string;
  lut?: ColorMap;
  autoscale?: boolean;
  autoscaleMode?: AutoscaleMode;
}) {
  const [colorMap, setColorMap] = useUserPreference<ColorMap>(
    `${props.name}/lut`,
    props.lut ?? 'Cividis'
  );
  const [scaleDomain, setScaleDomain] = useUserPreference<Domain>(
    `${props.name}/levels`,
    [-0.1, 0.1]
  );
  const [invertColorMap, setInvertColorMap] = useUserPreference<boolean>(
    `${props.name}/inverted`,
    false
  );
  const [scaleType, setScaleType] = useUserPreference<ScaleType>(
    `${props.name}/scale`,
    ScaleType.Linear
  );
  const [autoscale, setAutoscale] = useUserPreference<boolean>(
    `${props.name}/autoscale`,
    props.autoscale ?? true
  );
  const [autoscaleMode, setAutoscaleMode] = useUserPreference<AutoscaleMode>(
    `${props.name}/autoscalemode`,
    props.autoscaleMode ?? AutoscaleMode.StdDev3
  );
  const [profile, setProfile] = useUserPreference<ImageProfile>(
    `${props.name}/profile`,
    ImageProfile.Raw
  );

  return {
    colorMap,
    setColorMap,
    scaleType,
    setScaleType,
    autoscale,
    setAutoscale,
    autoscaleMode,
    setAutoscaleMode,
    scaleDomain,
    setScaleDomain,
    invertColorMap,
    setInvertColorMap,
    profile,
    setProfile,
  };
}
