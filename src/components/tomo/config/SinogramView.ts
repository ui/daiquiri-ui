import useUserPreference from 'components/utils/UseUserPreference';
import type { Dispatch, SetStateAction } from 'react';

export interface SinogramViewConfig {
  setColorbarVisible: Dispatch<SetStateAction<boolean>>;
  colorbarVisible: boolean;
  setDisplayAxes: Dispatch<SetStateAction<boolean>>;
  displayAxes: boolean;
  setCrossHair: Dispatch<SetStateAction<boolean>>;
  crossHair: boolean;
  setDisplayedSinogram: Dispatch<SetStateAction<boolean>>;
  displayedSinogram: boolean;
}

/**
 * Returns the main user configuration related to the sinogram view display
 */
export function useSinogramViewConfig(): SinogramViewConfig {
  const [displayAxes, setDisplayAxes] = useUserPreference<boolean>(
    'tomo/sinogramview/axes',
    true
  );
  const [colorbarVisible, setColorbarVisible] = useUserPreference<boolean>(
    'tomo/sinogramview/colorbar-visible',
    false
  );
  const [crossHair, setCrossHair] = useUserPreference<boolean>(
    'tomo/sinogramview/crosshair',
    false
  );
  const [displayedSinogram, setDisplayedSinogram] = useUserPreference<boolean>(
    'tomo/sinogramview/displayed',
    true
  );

  return {
    colorbarVisible,
    setColorbarVisible,
    displayAxes,
    setDisplayAxes,
    crossHair,
    setCrossHair,
    displayedSinogram,
    setDisplayedSinogram,
  };
}
