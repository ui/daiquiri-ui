import useUserPreference from 'components/utils/UseUserPreference';
import type { Dispatch, SetStateAction } from 'react';
import { AutoscaleMode } from '../../h5web/colormap';

export interface AlignConfig {
  autoscaleMode: AutoscaleMode;
  setAutoscaleMode: Dispatch<SetStateAction<AutoscaleMode>>;
  displayAxes: boolean;
  setDisplayAxes: Dispatch<SetStateAction<boolean>>;
  displayVLine: boolean;
  setDisplayVLine: Dispatch<SetStateAction<boolean>>;
  displayHLine: boolean;
  setDisplayHLine: Dispatch<SetStateAction<boolean>>;
  setCrossHair: Dispatch<SetStateAction<boolean>>;
  crossHair: boolean;
  setPreferedUnit: Dispatch<SetStateAction<string>>;
  preferedUnit: string;
}

/**
 * Returns the main user configuration related to the detector view display
 */
export function useAlignConfig(): AlignConfig {
  const [autoscaleMode, setAutoscaleMode] = useUserPreference<AutoscaleMode>(
    'tomo/align/colormap/autoscalemode',
    AutoscaleMode.StdDev3
  );
  const [displayAxes, setDisplayAxes] = useUserPreference<boolean>(
    'tomo/align/axes',
    true
  );
  const [displayVLine, setDisplayVLine] = useUserPreference<boolean>(
    'tomo/align/vline',
    false
  );
  const [displayHLine, setDisplayHLine] = useUserPreference<boolean>(
    'tomo/align/hline',
    false
  );
  const [crossHair, setCrossHair] = useUserPreference<boolean>(
    'tomo/align/crosshair',
    false
  );
  const [preferedUnit, setPreferedUnit] = useUserPreference<string>(
    'tomo/align/preferedunit',
    'mm'
  );

  return {
    autoscaleMode,
    setAutoscaleMode,
    displayAxes,
    setDisplayAxes,
    displayVLine,
    setDisplayVLine,
    displayHLine,
    setDisplayHLine,
    crossHair,
    setCrossHair,
    preferedUnit,
    setPreferedUnit,
  };
}
