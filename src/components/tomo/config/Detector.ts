import { useColorMapConfig } from './ColorMap';

/**
 * Returns the main user configuration related to the active detector display
 */
export function useDetectorConfig() {
  const colormap = useColorMapConfig({ name: 'tomo/defaultcolormap' });

  return {
    ...colormap,
  };
}
