import useUserPreference from 'components/utils/UseUserPreference';
import type { Dispatch, SetStateAction } from 'react';

type Follow = 'motors' | 'last-image';

export interface DetectorViewConfig {
  setFollow: Dispatch<SetStateAction<Follow>>;
  follow: Follow;
  setDisplayAxes: Dispatch<SetStateAction<boolean>>;
  displayAxes: boolean;
  setFieldOfView: Dispatch<SetStateAction<number>>;
  fieldOfView: number;
  setCrossHair: Dispatch<SetStateAction<boolean>>;
  crossHair: boolean;
  setDisplayMean: Dispatch<SetStateAction<boolean>>;
  displayMean: boolean;
  setDisplayHistogram: Dispatch<SetStateAction<boolean>>;
  displayHistogram: boolean;
}

/**
 * Returns the main user configuration related to the detector view display
 */
export function useDetectorViewConfig(): DetectorViewConfig {
  const [follow, setFollow] = useUserPreference<Follow>(
    'tomo/detectorview/follow',
    'last-image'
  );
  const [displayAxes, setDisplayAxes] = useUserPreference<boolean>(
    'tomo/detectorview/axes',
    true
  );
  const [fieldOfView, setFieldOfView] = useUserPreference<number>(
    'tomo/detectorview/field-of-view',
    1
  );
  const [crossHair, setCrossHair] = useUserPreference<boolean>(
    'tomo/detectorview/crosshair',
    false
  );
  const [displayMean, setDisplayMean] = useUserPreference<boolean>(
    'tomo/detectorview/mean',
    true
  );
  const [displayHistogram, setDisplayHistogram] = useUserPreference<boolean>(
    'tomo/detectorview/histogram',
    false
  );

  return {
    follow,
    setFollow,
    displayAxes,
    setDisplayAxes,
    fieldOfView,
    setFieldOfView,
    crossHair,
    setCrossHair,
    displayMean,
    setDisplayMean,
    displayHistogram,
    setDisplayHistogram,
  };
}
