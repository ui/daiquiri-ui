import { useRef } from 'react';
import type { MouseModes } from '../sinogram/models';
import debug from 'debug';
import DefaultMouseModeOptions from '../../h5web/bootstrap/DefaultMouseModeOptions';
import Toolbar, { Separator } from '../../common/Toolbar';
import { useSyncScaleDomainFromStatistics } from 'components/h5web/colormap';
import {
  useActiveDetectorAcqInfo,
  useActiveDetectorService,
} from '../utils/TomoDetector';
import { Button } from 'react-bootstrap';
import type { LinearVisCanvasRef } from '../../h5web/LinearVisCanvas2';
import { LinearVisCanvas } from '../../h5web/LinearVisCanvas2';
import DropdownDetector from '../tiling/DropdownDetector';
import { useDetectorConfig } from '../config/Detector';
import AutoScaleOption from '../../h5web/bootstrap/AutoscaleOption';
import HistogramDomainSlider from '../../h5web/bootstrap/HistogramDomainSlider';
import { useTomoConfigRelations, useTomoSampleStageRefs } from '../utils/hooks';
import { useMouseModeInteraction } from '../../h5web/UseMouseModeInteraction';
import { DetectorMesh } from './DetectorMesh';
import { ActualRotationAxisLocation } from './ActualRotationAxisLocation';

const logger = debug('daiquiri.components.tomo.detector.Default');

export interface TomoDetectorOptions {
  /** Identifier of the tomoconfig hardware object */
  tomoconfig: string;
}

interface Props {
  options: TomoDetectorOptions;
}

export default function TomoDetectorWidget(props: Props) {
  const { options } = props;
  const { tomoconfig: tomoConfigId } = options;
  const detectorConfig = useDetectorConfig();
  const sampleStageRefs = useTomoSampleStageRefs(tomoConfigId ?? '');
  const tomoHardware = useTomoConfigRelations(sampleStageRefs);

  const detectorInfo = useActiveDetectorAcqInfo({ sampleStage: tomoHardware });

  const activeDetectorService = useActiveDetectorService({
    detector: detectorInfo,
    sampleStage: tomoHardware,
    profile: detectorConfig.profile,
    normalization: detectorConfig.scaleType,
    autoscale: detectorConfig.autoscale,
    autoscaleMode: detectorConfig.autoscaleMode,
    domain: detectorConfig.scaleDomain,
    enabled: true,
    minRefreshPeriod: 0.5,
  });
  const detectorArray = activeDetectorService.data;

  const plotRef = useRef<LinearVisCanvasRef>(null);

  const mouseModeInteraction = useMouseModeInteraction<MouseModes>('zoom');
  const { mouseMode } = mouseModeInteraction;

  const detectorSize = detectorArray?.detectorSize ?? { width: 0, height: 0 };
  const statistics = detectorArray?.statistics;

  useSyncScaleDomainFromStatistics({
    statistics: detectorArray?.statistics,
    ...detectorConfig,
  });

  return (
    <div
      className="plot2d-container w-100 h-100"
      style={{
        flex: '1 1 0%',
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <Toolbar align="center">
        <DefaultMouseModeOptions mouseModeInteraction={mouseModeInteraction} />
        <Button
          title="Reset zoom (sample stage overview)"
          variant="secondary"
          onClick={() => {
            plotRef?.current?.actions.resetZoom();
          }}
        >
          <i className="fa fa-expand fa-fw fa-lg" />
        </Button>
        <Separator />
        <DropdownDetector
          showDisplayed={false}
          variant="secondary"
          isInverted={detectorConfig.invertColorMap}
          onSelectInvertion={detectorConfig.setInvertColorMap}
          isDisplayed
          onSelectDisplayed={() => {}}
          lut={detectorConfig.colorMap}
          onSelectLut={detectorConfig.setColorMap}
          norm={detectorConfig.scaleType}
          onSelectNorm={detectorConfig.setScaleType}
          autoscale={detectorConfig.autoscale}
          onSelectAutoscale={detectorConfig.setAutoscale}
          autoscaleMode={detectorConfig.autoscaleMode}
          onSelectAutoscaleMode={detectorConfig.setAutoscaleMode}
          profile={detectorConfig.profile}
          onSelectProfile={detectorConfig.setProfile}
          array={detectorArray}
        />
        <AutoScaleOption
          disabled={statistics === undefined}
          {...detectorConfig}
        />
        <HistogramDomainSlider
          disabled={statistics === undefined}
          statistics={detectorArray?.statistics}
          {...detectorConfig}
        />
      </Toolbar>
      <div
        style={{
          flex: '1 1 auto',
          display: 'flex',
          margin: 0,
          minHeight: 0,
        }}
      >
        <LinearVisCanvas
          plotRef={plotRef}
          abscissaConfig={{
            visDomain: [0, detectorSize.width],
            label: 'px',
          }}
          ordinateConfig={{
            visDomain: [0, detectorSize.height],
            label: 'px',
            flip: true,
          }}
          mouseMode={mouseMode}
          aspect="equal"
        >
          {detectorArray && (
            <>
              <DetectorMesh
                detectorArray={detectorArray}
                lut={detectorConfig.colorMap}
                isColorMapInverted={detectorConfig.invertColorMap}
                scale={detectorConfig.scaleType}
                domain={detectorConfig.scaleDomain}
              />
              <ActualRotationAxisLocation
                tomoHardware={tomoHardware}
                detectorArray={detectorArray}
              />
            </>
          )}
        </LinearVisCanvas>
      </div>
    </div>
  );
}
