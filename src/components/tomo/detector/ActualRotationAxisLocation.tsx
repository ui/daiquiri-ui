import type { TomoDetectorArray } from 'services/TomoDetectorService';
import type { TomoConfigHardware } from '../../../connect/tomo/utils';
import { posInMM } from '../utils/geometry';
import Label from 'components/h5web/items/Label';
import { Vector2 } from 'three';
import { VSegment } from '../../h5web/items/shapes/VSegment';

export function ActualRotationAxisLocation(props: {
  tomoHardware: TomoConfigHardware;
  detectorArray: TomoDetectorArray;
}) {
  const { tomoHardware, detectorArray } = props;
  const imagePs = detectorArray.imagePixelSize?.to('mm')?.scalar;
  const sy = tomoHardware.sy ? posInMM(tomoHardware.sy) : null;
  if (sy === null || imagePs === undefined) {
    return <></>;
  }
  const imageWidth = detectorArray.detectorSize.width;
  const { sampleStage } = tomoHardware;
  const detectorCenterY = sampleStage?.properties.detector_center?.[0] ?? 0;
  const imageHeight = detectorArray.detectorSize.height;
  const axisY = imageWidth * 0.5 + sy / imagePs - detectorCenterY;

  const startPoint = new Vector2(axisY, imageHeight * 0.75);
  const dash = imageHeight / 50;
  return (
    <>
      <VSegment
        x={axisY}
        y1={imageHeight}
        y2={imageHeight * 0.75}
        color="red"
        gapColor="white"
        dashSize={dash}
        gapSize={dash}
        lineWidth={2}
      />
      <Label
        datapos={[startPoint.x, startPoint.y]}
        color="#FF0000"
        text="Theorical rotation axis"
      />
    </>
  );
}
