import type { ColorMap, Domain, ScaleType } from '@h5web/lib';
import type { TomoDetectorArray } from 'services/TomoDetectorService';
import ImageMesh from '../../h5web/items/ImageMesh';

export function DetectorMesh(props: {
  detectorArray: TomoDetectorArray;
  lut: ColorMap;
  isColorMapInverted: boolean;
  scale: ScaleType;
  domain: Domain;
}) {
  const { domain } = props;
  const { displayArray, detectorSize } = props.detectorArray;

  return (
    <ImageMesh
      values={displayArray}
      domain={domain}
      colorMap={props.lut}
      invertColorMap={props.isColorMapInverted}
      scaleType={props.scale}
      position={[detectorSize.width * 0.5, detectorSize.height * 0.5, 0]}
      scale={[1, 1, 1]}
      size={detectorSize}
    />
  );
}
