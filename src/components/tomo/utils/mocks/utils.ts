import { createHardwareMotor } from 'components/hardware/motor/mocks/utils';

export function createTomoHardware() {
  return {
    sy: createHardwareMotor('sy', 10),
    sz: createHardwareMotor('sz', 10),
    sampy: createHardwareMotor('sampy', 10),
    sampu: createHardwareMotor('sampy', 10),
    sampv: createHardwareMotor('sampy', 10),
    somega: createHardwareMotor('somega', 10),
    detector: {
      id: 'detector',
      type: 'unknown',
      name: 'MyDetector',
    },
    tomodetector: {
      id: 'tomodetector',
      type: 'tomodetector',
      properties: { actual_size: [1000, 1000], sample_pixel_size: 10 },
    },
    detectors: {
      id: 'detectors',
      type: 'tomodetectors',
      properties: { active_detector: 'hardware:tomodetector' },
    },
    tomo: {
      id: 'tomo',
      type: 'tomoconfig',
      properties: {
        sx: 'hardware:sx',
        sy: 'hardware:sy',
        sz: 'hardware:sz',
        sampx: 'hardware:sampx',
        sampy: 'hardware:sampy',
        sampu: 'hardware:sampu',
        sampv: 'hardware:sampv',
        somega: 'hardware:somega',
        detectors: 'hardware:detectors',
      },
    },
  };
}

export function getStore(state: any) {
  return {
    getState: () => {
      return state;
    },
    subscribe: () => {},
    dispatch: () => {},
  };
}
