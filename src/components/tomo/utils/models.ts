/**
 * Channel data as exposed by the scandata Redux provider
 */
export interface ChannelData {
  name: string;
  shape: any;
  data: number[];
}

/**
 * Scan data as exposed by the scandata Redux provider
 */
export interface ScanData {
  axes: any;
  info: any;
  data: {
    [channelid: string]: ChannelData;
  };
}
