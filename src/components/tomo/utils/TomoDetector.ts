import { useEffect, useCallback, useMemo } from 'react';
import {
  TomoProcess,
  useTomoDetectorService,
} from 'services/TomoDetectorService';
import type {
  ImageProfile,
  TomoDetectorResult,
} from 'services/TomoDetectorService';
import type { TomoDetector } from 'types/Tomo';
import { AutoscaleMode } from 'components/h5web/colormap';
import debug from 'debug';
import type {
  TomoConfigHardware,
  TomoSampleStageRefs,
} from 'connect/tomo/utils';
import { debounce } from 'lodash';
import type { Domain } from '@h5web/lib';
import { ScaleType } from '@h5web/lib';
import { useTomoDetectors } from './hooks';

const logger = debug('daiquiri.components.tomo.utils.TomoDetector');

export function useActiveDetectorAcqInfo(props: {
  sampleStage?: TomoConfigHardware;
  sampleStageRefs?: TomoSampleStageRefs | null;
}): TomoDetector | undefined {
  const detectors = useTomoDetectors();
  return useMemo(() => {
    function getDetectorId() {
      if (props.sampleStage) {
        const { detector } = props.sampleStage;
        return detector ? detector.id : '';
      }
      return props.sampleStageRefs?.detector ?? '';
    }
    const detectorId = getDetectorId();
    return detectors[detectorId];
  }, [props.sampleStage, detectors]);
}

/**
 * Get an updated version of the detector data with flatfield correction.
 *
 * The min refresh period is in second.
 */
export function useActiveDetectorService(props: {
  detector: TomoDetector | undefined;
  sampleStage?: TomoConfigHardware;
  sampleStageRefs?: TomoSampleStageRefs | null;
  autoscale: boolean;
  autoscaleMode: AutoscaleMode;
  domain: Domain;
  normalization: ScaleType;
  profile: ImageProfile;
  histogram?: boolean;
  enabled: boolean;
  minRefreshPeriod: number;
  processing?: TomoProcess;
}): TomoDetectorResult {
  const tomoDetectorService = useTomoDetectorService();
  const fetchDetectorData = tomoDetectorService.requestUpdate;

  const debounceFetchDetectorData = useCallback(
    debounce(fetchDetectorData, props.minRefreshPeriod * 1000),
    [props.minRefreshPeriod, fetchDetectorData]
  );

  useEffect(() => {
    if (props.profile !== 'u8') {
      if (props.enabled && props.detector) {
        debounceFetchDetectorData({
          detector: props.detector,
          lut: 'Cividis', // Place holder
          autoscale: AutoscaleMode.StdDev3, // Place holder
          normalization: ScaleType.Linear, // Place holder
          process: props.processing ?? TomoProcess.Flatfield,
          profiles: [props.profile],
          histogram: props.histogram,
        });
      }
    }
  }, [
    props.detector,
    debounceFetchDetectorData,
    props.enabled,
    props.profile,
    props.processing,
    props.histogram,
  ]);

  function scale() {
    if (props.autoscale) {
      if (props.autoscaleMode !== AutoscaleMode.None) {
        return {
          autoscale: props.autoscaleMode,
        };
      }
    }
    return {
      vmin: props.domain[0],
      vmax: props.domain[1],
      autoscale: AutoscaleMode.None,
    };
  }

  useEffect(() => {
    if (props.profile === 'u8') {
      if (props.enabled && props.detector) {
        debounceFetchDetectorData({
          detector: props.detector,
          lut: 'Cividis', // Place holder
          normalization: props.normalization,
          process: props.processing ?? TomoProcess.Flatfield,
          profiles: [props.profile],
          histogram: props.histogram,
          ...scale(),
        });
      }
    }
  }, [
    props.detector,
    debounceFetchDetectorData,
    props.enabled,
    props.profile,
    props.processing,
    props.autoscale,
    props.autoscaleMode,
    props.normalization,
    props.domain[0],
    props.domain[1],
    props.histogram,
  ]);

  return tomoDetectorService;
}
