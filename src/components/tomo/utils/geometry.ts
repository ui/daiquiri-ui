import type { MotorSchema } from '@esrf/daiquiri-lib';
import type { TomoDetectorSchema } from 'components/hardware/tomodetector';
import Qty from 'js-quantities';
import type { LimaSchema } from '../../hardware/lima';
import { isMotorClockWise } from '../../hardware/motor/utils';

/**
 * Returns the position of a motor in millimeter.
 */
export function posInMM(motor: MotorSchema): number {
  if (!motor.properties.unit) {
    throw new Error(`${motor.name} don't have unit`);
  }
  const position = motor.properties.position;
  if (position === null) {
    throw new Error(`${motor.name} don't have position`);
  }
  return new Qty(position, motor.properties.unit).to('mm').scalar;
}

/**
 * Returns the position of a motor in millimeter.
 */
export function posInMMElseNull(motor: MotorSchema | null): number | null {
  if (motor === null) {
    return null;
  }
  if (!motor.properties.unit) {
    throw new Error(`${motor.name} don't have unit`);
  }
  const position = motor.properties.position;
  if (position === null) {
    return null;
  }
  return new Qty(position, motor.properties.unit).to('mm').scalar;
}

/**
 * Returns the position of a motor in degree.
 */
export function posInDeg(motor: MotorSchema): number | null {
  if (!motor.properties.unit) {
    throw new Error(`${motor.name} don't have unit`);
  }
  const position = motor.properties.position;
  if (position === null) {
    throw new Error(`${motor.name} don't have position`);
  }
  return new Qty(position, motor.properties.unit).to('deg').scalar;
}

/**
 * Returns the position of a motor in radian.
 */
export function posInRad(motor: MotorSchema): number {
  if (!motor.properties.unit) {
    throw new Error(`${motor.name} don't have unit`);
  }
  const position = motor.properties.position;
  if (position === null) {
    throw new Error(`${motor.name} don't have position`);
  }
  return new Qty(position, motor.properties.unit).to('rad').scalar;
}

/**
 * Returns the position of a motor in radian and clock wise.
 */
export function cwPosInRad(motor: MotorSchema): number {
  if (!motor.properties.unit) {
    throw new Error(`${motor.name} don't have unit`);
  }
  const sign = isMotorClockWise(motor) ? 1 : -1;
  const position = motor.properties.position;
  if (position === null) {
    throw new Error(`${motor.name} don't have position`);
  }
  return new Qty(sign * position, motor.properties.unit).to('rad').scalar;
}

/**
 * Returns the range of a motor in millimeter.
 */
export function rangeInMM(motor: MotorSchema | null): [number, number] {
  if (motor === null) {
    return [0, 0];
  }
  const { limits, unit } = motor.properties;
  if (!unit) {
    throw new Error(`${motor.name} don't have unit`);
  }
  if (limits === null || limits === undefined) {
    return [0, 0];
  }
  const [min, max] = limits;
  return [
    new Qty(min, unit).to('mm').scalar,
    new Qty(max, unit).to('mm').scalar,
  ];
}

/**
 * Returns the field of view of a detector in millimeter.
 *
 * If the detector is null, or if some propertes like the size or the pixel size
 * are not defined, null is returned.
 *
 * @returns A tuple containing the width and the height, else null.
 */
export function fovInMM(
  tomoDetector: TomoDetectorSchema | null,
  detector: LimaSchema | null
): [number, number] | null {
  if (tomoDetector === null || detector === null) {
    return null;
  }
  const sample_pixel_size = tomoDetector.properties.sample_pixel_size;
  const size = detector.properties.size;
  if (size === null || sample_pixel_size === null) {
    return null;
  }
  const px = sample_pixel_size / 1000;
  return [size[0] * px, size[1] * px];
}

/**
 * Convert a position in millimeter into the motor unit.
 *
 * If the motor is null, the position is returned in millimeter.
 *
 * @returns A tuple with the position and the unit.
 */
export function toMotorPos(
  motor: MotorSchema | null,
  position: number
): [number, string] {
  if (motor === null) {
    return [position, 'mm'];
  }
  const unit = motor.properties.unit;
  return [new Qty(position, 'mm').to(unit).scalar, unit];
}

export function getAxisName(motor: MotorSchema | null, defaultName: string) {
  if (motor === null) {
    return defaultName;
  }
  const axisName = motor.alias ?? motor.name;
  if (!axisName) {
    return defaultName;
  }
  return axisName;
}
