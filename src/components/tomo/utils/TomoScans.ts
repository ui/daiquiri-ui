export function getLabelFromScanState(state: string | null): [string, string] {
  if (!state || state === 'NONE') {
    return ['NONE', 'secondary'];
  }
  // BLISS
  if (state === 'IDLE') {
    return ['IDLE', 'secondary'];
  }
  if (state === 'PREPARING') {
    return ['RUNNING', 'warning'];
  }
  if (state === 'STARTING') {
    return ['RUNNING', 'warning'];
  }
  if (state === 'STOPPING') {
    return ['FINISHED', 'success'];
  }
  if (state === 'DONE') {
    return ['FINISHED', 'success'];
  }
  if (state === 'USER_ABORTED') {
    return ['ABORTED', 'secondary'];
  }
  if (state === 'KILLED') {
    return ['ERROR', 'danger'];
  }
  // Daiquiri
  if (state === 'IDLE') {
    return ['IDLE', 'secondary'];
  }
  if (state === 'RUNNING') {
    return ['RUNNING', 'warning'];
  }
  if (state === 'FINISHED') {
    return ['FINISHED', 'success'];
  }
  if (state === 'ABORTED') {
    return ['ABORTED', 'secondary'];
  }
  if (state === 'ERROR') {
    return ['ERROR', 'danger'];
  }
  return [state, 'danger'];
}
