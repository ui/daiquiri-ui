import type {
  TomoConfigHardware,
  TomoSampleStageRefs,
} from 'connect/tomo/utils';
import { shallowEqual, useSelector } from 'react-redux';
import type { HardwareTypes, MotorSchema } from '@esrf/daiquiri-lib';
import type { ReduxState } from '../../../types/ReduxStore';
import { getObjFromId, getObjFromRef } from 'connect/hardware/utils';
import type { TomoDetectorSchema } from 'components/hardware/tomodetector';
import { isTomoDetectorHardware } from 'components/hardware/tomodetector';
import type {
  TilingTomoScan,
  TomoConfigStore,
  TomoDetector,
  TomoScanInfo,
} from '../../../types/Tomo';
import tomo from 'providers/tomo';
import { useEffect } from 'react';
import type { TomoHoloSchema } from '../../hardware/tomoholo';
import { isTomoHoloHardware } from '../../hardware/tomoholo';
import type { TomoConfigSchema } from '../../hardware/tomoconfig';
import { isTomoConfigHardware } from '../../hardware/tomoconfig';
import { isMotorHardware } from '../../hardware/motor';
import { isTomoDetectorsHardware } from '../../hardware/tomodetectors';
import type { LimaSchema } from '../../hardware/lima';
import { isLimaHardware } from '../../hardware/lima';
import type { TomoSampleStageSchema } from '../../hardware/tomosamplestage';
import { isTomoSampleStageHardware } from '../../hardware/tomosamplestage';
import type { TomoFlatMotionSchema } from '../../hardware/tomoflatmotion';
import { isTomoFlatMotionHardware } from '../../hardware/tomoflatmotion';

function getMotorFromRef(
  hardwareMap: Record<string, HardwareTypes.Hardware>,
  ref: string
) {
  const obj = getObjFromRef(hardwareMap, ref);
  if (!obj) {
    return null;
  }
  if (!isMotorHardware(obj)) {
    return null;
  }
  return obj;
}

function getMotorFromId(
  hardwareMap: Record<string, HardwareTypes.Hardware>,
  id: string
) {
  const obj = getObjFromId(hardwareMap, id);
  if (!obj) {
    return null;
  }
  if (!isMotorHardware(obj)) {
    return null;
  }
  return obj;
}

function getTomoConfigFromId(
  hardwareMap: Record<string, HardwareTypes.Hardware>,
  id: string
) {
  const obj = getObjFromId(hardwareMap, id);
  if (!obj) {
    return null;
  }
  if (!isTomoConfigHardware(obj)) {
    return null;
  }
  return obj;
}

function getTomoDetectors(
  hardwareMap: Record<string, HardwareTypes.Hardware>,
  ref: string
) {
  const obj = getObjFromRef(hardwareMap, ref);
  if (!obj) {
    return null;
  }
  if (!isTomoDetectorsHardware(obj)) {
    return null;
  }
  return obj;
}

function getTomoDetector(
  hardwareMap: Record<string, HardwareTypes.Hardware>,
  ref: string
) {
  const obj = getObjFromRef(hardwareMap, ref);
  if (!obj) {
    return null;
  }
  if (!isTomoDetectorHardware(obj)) {
    return null;
  }
  return obj;
}

function getTomoDetectorFromId(
  hardwareMap: Record<string, HardwareTypes.Hardware>,
  id: string
) {
  const obj = getObjFromId(hardwareMap, id);
  if (!obj) {
    return null;
  }
  if (!isTomoDetectorHardware(obj)) {
    return null;
  }
  return obj;
}

function getTomoSampleStageFromId(
  hardwareMap: Record<string, HardwareTypes.Hardware>,
  id: string
) {
  const obj = getObjFromId(hardwareMap, id);
  if (!obj) {
    return null;
  }
  if (!isTomoSampleStageHardware(obj)) {
    return null;
  }
  return obj;
}

function getTomoFlatMotionFromId(
  hardwareMap: Record<string, HardwareTypes.Hardware>,
  id: string
) {
  const obj = getObjFromId(hardwareMap, id);
  if (!obj) {
    return null;
  }
  if (!isTomoFlatMotionHardware(obj)) {
    return null;
  }
  return obj;
}

/**
 * Expose every hardware id from the main tomo config object
 */
export function useTomoSampleStageRefs(
  name: string
): TomoSampleStageRefs | null {
  return useSelector<ReduxState, TomoSampleStageRefs | null>((store) => {
    const hardwareMap = store.hardware.ns_hardware.default.results;
    const tomoConfig = getTomoConfigFromId(hardwareMap, name);
    if (!tomoConfig) {
      return null;
    }
    const detectors = getTomoDetectors(
      hardwareMap,
      tomoConfig.properties.detectors
    );
    const tomoDetector = getTomoDetector(
      hardwareMap,
      detectors?.properties.active_detector ?? ''
    );
    const detector = getObjFromRef(
      hardwareMap,
      tomoDetector?.properties.detector ?? ''
    );
    const holotomo = getObjFromRef(
      hardwareMap,
      tomoConfig?.properties.holotomo ?? ''
    );
    const sampleStage = getObjFromRef(
      hardwareMap,
      tomoConfig?.properties.sample_stage ?? ''
    );

    return {
      sampleStage: sampleStage?.id ?? null,
      sx: getMotorFromRef(hardwareMap, sampleStage?.properties.sx)?.id ?? null,
      sy: getMotorFromRef(hardwareMap, sampleStage?.properties.sy)?.id ?? null,
      sz: getMotorFromRef(hardwareMap, sampleStage?.properties.sz)?.id ?? null,
      somega:
        getMotorFromRef(hardwareMap, sampleStage?.properties.somega)?.id ??
        null,
      sampx:
        getMotorFromRef(hardwareMap, sampleStage?.properties.sampx)?.id ?? null,
      sampy:
        getMotorFromRef(hardwareMap, sampleStage?.properties.sampy)?.id ?? null,
      sampu:
        getMotorFromRef(hardwareMap, sampleStage?.properties.sampu)?.id ?? null,
      sampv:
        getMotorFromRef(hardwareMap, sampleStage?.properties.sampv)?.id ?? null,
      detector: detector?.id ?? null,
      tomoDetector: tomoDetector?.id ?? null,
      holotomo: holotomo?.id ?? null,
    };
  }, shallowEqual);
}

/**
 * Use an holotomo hardware from it's name.
 */
export function useHolotomoHardware(
  name: string | null
): TomoHoloSchema | null {
  return useSelector<ReduxState, TomoHoloSchema | null>((store) => {
    const hardwareMap = store.hardware.ns_hardware.default.results;
    if (!name) {
      return null;
    }
    const holotomo = getObjFromId(hardwareMap, name ?? '');
    if (!holotomo) {
      return null;
    }
    if (!isTomoHoloHardware(holotomo)) {
      return null;
    }
    return holotomo;
  }, shallowEqual);
}

export function useHardware(
  name: string | null
): HardwareTypes.Hardware | null {
  return useSelector<ReduxState, HardwareTypes.Hardware | null>((store) => {
    if (name === null) {
      return null;
    }
    const hardwareMap = store.hardware.ns_hardware.default.results;
    return getObjFromId(hardwareMap, name);
  }, shallowEqual);
}

export function useMotorHardware(name: string | null): MotorSchema | null {
  return useSelector<ReduxState, MotorSchema | null>((store) => {
    if (name === null) {
      return null;
    }
    const hardwareMap = store.hardware.ns_hardware.default.results;
    return getMotorFromId(hardwareMap, name);
  }, shallowEqual);
}

export function useLimaHardware(name: string | null): LimaSchema | null {
  return useSelector<ReduxState, LimaSchema | null>((store) => {
    if (name === null) {
      return null;
    }
    const hardwareMap = store.hardware.ns_hardware.default.results;
    const obj = getObjFromId(hardwareMap, name);
    if (!obj) {
      return null;
    }
    if (!isLimaHardware(obj)) {
      return null;
    }
    return obj;
  }, shallowEqual);
}

export function useTomoDetectorHardware(
  name: string | null
): TomoDetectorSchema | null {
  return useSelector<ReduxState, TomoDetectorSchema | null>((store) => {
    if (name === null) {
      return null;
    }
    const hardwareMap = store.hardware.ns_hardware.default.results;
    return getTomoDetectorFromId(hardwareMap, name);
  }, shallowEqual);
}

export function useTomoSampleStageHardware(
  name: string | null
): TomoSampleStageSchema | null {
  return useSelector<ReduxState, TomoSampleStageSchema | null>((store) => {
    if (name === null) {
      return null;
    }
    const hardwareMap = store.hardware.ns_hardware.default.results;
    return getTomoSampleStageFromId(hardwareMap, name);
  }, shallowEqual);
}

export function useTomoFlatMotionHardware(
  name: string | null
): TomoFlatMotionSchema | null {
  return useSelector<ReduxState, TomoFlatMotionSchema | null>((store) => {
    if (name === null) {
      return null;
    }
    const hardwareMap = store.hardware.ns_hardware.default.results;
    return getTomoFlatMotionFromId(hardwareMap, name);
  }, shallowEqual);
}

export function useTomoConfigHardware(
  name: string | null
): TomoConfigSchema | null {
  return useSelector<ReduxState, TomoConfigSchema | null>((store) => {
    if (name === null) {
      return null;
    }
    const hardwareMap = store.hardware.ns_hardware.default.results;
    return getTomoConfigFromId(hardwareMap, name);
  }, shallowEqual);
}

export function useTomoConfigRelations(
  ref: TomoSampleStageRefs | null
): TomoConfigHardware {
  const sy = useMotorHardware(ref?.sy ?? null);
  const sz = useMotorHardware(ref?.sz ?? null);
  const sampx = useMotorHardware(ref?.sampx ?? null);
  const sampy = useMotorHardware(ref?.sampy ?? null);
  const sampu = useMotorHardware(ref?.sampu ?? null);
  const sampv = useMotorHardware(ref?.sampv ?? null);
  const somega = useMotorHardware(ref?.somega ?? null);
  const detector = useLimaHardware(ref?.detector ?? null);
  const tomoDetector = useTomoDetectorHardware(ref?.tomoDetector ?? null);
  const sampleStage = useTomoSampleStageHardware(ref?.sampleStage ?? null);
  return {
    sampleStage,
    sy,
    sampx,
    sampy,
    sampu,
    sampv,
    sz,
    somega,
    detector,
    tomoDetector,
  };
}

export function useTomoConfig(): TomoConfigStore {
  return useSelector<ReduxState, TomoConfigStore>((state) => {
    return state.app.ns_config.default.results.tomo ?? {};
  }, shallowEqual);
}

export function useLastTilingScan(): TilingTomoScan | null {
  useEffect(() => {
    // fetch the scaninfo state if it was not already done
    // The following updates are event based
    tomo.getNamespace('scaninfo').fetch(null, true);
  }, [tomo]);

  return useSelector<ReduxState, TilingTomoScan | null>((state) => {
    const lastTiling = state.tomo?.ns_scaninfo.default.results.lasttiling;
    if (!lastTiling) {
      return null;
    }
    if (!lastTiling.scanid) {
      return null;
    }
    return lastTiling as TilingTomoScan;
  }, shallowEqual);
}

export function useLastScanGroup(): TomoScanInfo | null {
  useEffect(() => {
    // fetch the scaninfo state if it was not already done
    // The following updates are event based
    tomo.getNamespace('scaninfo').fetch(null, true);
  }, [tomo]);

  return useSelector<ReduxState, TomoScanInfo | null>((state) => {
    const lastGroup = state.tomo?.ns_scaninfo.default.results.lastgroup;
    if (!lastGroup) {
      return null;
    }
    if (!lastGroup.scanid) {
      return null;
    }
    return lastGroup as TomoScanInfo;
  }, shallowEqual);
}

export function useLastTomovisId(): string | null {
  return useSelector<ReduxState, string | null>((state) => {
    const lastTiling = state.tomo?.ns_scaninfo.default.results.lasttiling;
    if (!lastTiling) {
      return null;
    }
    if (!lastTiling.tomovisId) {
      return null;
    }
    return lastTiling.tomovisId;
  });
}

export function useTomoDetectors(): Record<string, TomoDetector> {
  const detectors = useSelector<ReduxState, Record<string, TomoDetector>>(
    (state) => {
      return state.tomo?.ns_detectors.default.results ?? {};
    }
  );

  useEffect(() => {
    // fetch the detectors state if it was not already done
    // The following updates are event based
    tomo.getNamespace('detectors').fetch(null, true);
  }, [tomo]);

  return detectors;
}
