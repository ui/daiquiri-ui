import type Qty from 'js-quantities';

export type RequestMoveSampleStage = (props: {
  sz?: Qty;
  sy?: Qty;
  sampx?: Qty;
  sampy?: Qty;
  sampu?: Qty;
  sampv?: Qty;
}) => void;

export type RequestSliceReconstruction = (props: {
  datacollectionid: number;
  axisposition?: number;
  deltabeta?: number;
}) => void;
