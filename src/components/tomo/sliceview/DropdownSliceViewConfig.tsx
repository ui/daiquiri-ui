import type { Dispatch, PropsWithChildren, SetStateAction } from 'react';
import {
  Dropdown,
  ButtonGroup,
  Button,
  Container,
  Row,
  Col,
} from 'react-bootstrap';
import ColormapButtonGroup from '../../h5web/bootstrap/ColormapButtonGroup';
import type { SliceReconstructionConfig } from '../config/SliceReconstruction';

interface Props {
  variant?: string;
  as?: any;
  dropDirection?: 'up' | 'down';
  config: SliceReconstructionConfig;
}

function selectedVariant<T>(value: T, expected: T) {
  return value === expected ? 'primary' : 'secondary';
}

function SelectedButton<T>(
  props: PropsWithChildren<{
    var: T;
    value: T;
    setter: Dispatch<SetStateAction<T>>;
  }>
) {
  return (
    <Button
      variant={selectedVariant(props.var, props.value)}
      onClick={() => {
        props.setter(props.value);
      }}
      className="text-nowrap"
      size="sm"
    >
      {props.children}
    </Button>
  );
}

export default function DropdownTilingViewConfig(props: Props) {
  const { as = undefined, variant = 'secondary', config } = props;

  return (
    <Dropdown
      as={as}
      className={`${props.dropDirection === 'up' ? 'dropup' : ''}`}
    >
      <Dropdown.Toggle
        id="dropdown-basic"
        variant={variant}
        className="d-flex align-items-center"
      >
        <i className="fa fa-sliders fa-fw fa-lg" />
      </Dropdown.Toggle>
      <Dropdown.Menu className="dropdown-menu-center">
        <div className="ms-1 me-1" style={{ minWidth: '300px' }}>
          <Container>
            <Row>
              <Col className="my-auto">Colormap</Col>
              <Col xs={7}>
                <ColormapButtonGroup
                  lut={config.colorMap}
                  onSelectLut={config.setColorMap}
                />
              </Col>
            </Row>
            <Row className="mt-1">
              <Col className="my-auto">Display axes</Col>
              <Col xs={7}>
                <ButtonGroup>
                  <SelectedButton
                    var={config.displayAxes}
                    value
                    setter={config.setDisplayAxes}
                  >
                    Yes
                  </SelectedButton>
                  <SelectedButton
                    var={config.displayAxes}
                    value={false}
                    setter={config.setDisplayAxes}
                  >
                    No
                  </SelectedButton>
                </ButtonGroup>
              </Col>
            </Row>
            <Row className="mt-1">
              <Col className="my-auto">Crosshair</Col>
              <Col xs={7}>
                <ButtonGroup>
                  <SelectedButton
                    var={config.crossHair}
                    value
                    setter={config.setCrossHair}
                  >
                    Yes
                  </SelectedButton>
                  <SelectedButton
                    var={config.crossHair}
                    value={false}
                    setter={config.setCrossHair}
                  >
                    No
                  </SelectedButton>
                </ButtonGroup>
              </Col>
            </Row>
          </Container>
        </div>
      </Dropdown.Menu>
    </Dropdown>
  );
}
