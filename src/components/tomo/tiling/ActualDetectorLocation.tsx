import type { MotorSchema } from '@esrf/daiquiri-lib';
import type { TomoConfigHardware } from 'connect/tomo/utils';
import Label from 'components/h5web/items/Label';
import { Vector3 } from 'three';
import sassVariables from 'scss/variables.module.scss';
import { fovInMM, posInMM, posInMMElseNull } from '../utils/geometry';
import { Rect } from 'components/h5web/items/shapes/Rect';
import debug from 'debug';
import { CrossMarker } from 'components/h5web/items/shapes/CrossMarker';
import { VSegment } from 'components/h5web/items/shapes/VSegment';
import { isMotorClockWise } from '../../hardware/motor/utils';

const logger = debug('daiquiri.components.tomo.tiling.ActualDetectorLocation');

function Detector(props: {
  projRotation: number;
  fov: [number, number];
  x: number;
  y: number;
  rotation: number;
}) {
  const rot = ((props.rotation + props.projRotation) / 180) * Math.PI;
  const { x, y, fov } = props;
  const hfovx = Math.abs(Math.cos(rot) * fov[0] * 0.5);
  const hfovy = fov[1] * 0.5;

  const boundingStartPoint: [number, number] = [
    x - fov[0] * 0.5,
    y - fov[1] * 0.5,
  ];
  const boundingEndPoint: [number, number] = [
    x + fov[0] * 0.5,
    y + fov[1] * 0.5,
  ];
  const roundingRect = (
    <>
      <Rect
        startPoint={boundingStartPoint}
        endPoint={boundingEndPoint}
        lineColor={sassVariables.danger}
        gapColor={sassVariables.light}
        dashSize={4}
        gapSize={4}
        lineWidth={1}
        zIndex={2}
      />
    </>
  );

  if (hfovx < 0.001) {
    return (
      <>
        {roundingRect}
        <VSegment
          x={x}
          y1={y - hfovy}
          y2={y + hfovy}
          color={sassVariables.danger}
          lineWidth={2}
          zIndex={2}
        />
      </>
    );
  }
  return (
    <>
      {roundingRect}
      <Rect
        startPoint={[x - hfovx, y - hfovy]}
        endPoint={[x + hfovx, y + hfovy]}
        lineColor={sassVariables.danger}
        lineWidth={2}
        zIndex={2}
      />
    </>
  );
}

function RotationAxis(props: {
  xAxisDelta: number;
  y: number;
  fov: [number, number] | null;
  detectorX: number;
}) {
  const { xAxisDelta, y, fov, detectorX } = props;
  const axisSize = fov ? fov[1] * 0.5 : 50;

  function AxisLine(props: { startPoint: Vector3; endPoint: Vector3 }) {
    const { startPoint, endPoint } = props;
    return (
      <VSegment
        x={startPoint.x}
        y1={startPoint.y}
        y2={endPoint.y}
        dashSize={8}
        gapSize={4}
        color={sassVariables.danger}
        gapColor={sassVariables.light}
      />
    );
  }

  const x = detectorX + xAxisDelta;

  if (fov === null) {
    const axisStartPoint = new Vector3(x, y - axisSize);
    const axisEndPoint = new Vector3(x, y + axisSize);
    return (
      <>
        <Label
          datapos={[x + 1, y - axisSize]}
          color={sassVariables.danger}
          text="Axis"
        />
        <AxisLine startPoint={axisStartPoint} endPoint={axisEndPoint} />
      </>
    );
  }

  if (Math.abs(xAxisDelta) * 2 > fov[0]) {
    const halfFov = fov[0] * 0.5;
    const axisStartPoint = new Vector3(x, y - halfFov - axisSize);
    const axisEndPoint = new Vector3(x, y + halfFov + axisSize);
    return (
      <>
        <Label
          datapos={[x + 1, y - axisSize]}
          color={sassVariables.danger}
          text="Axis"
        />
        <AxisLine startPoint={axisStartPoint} endPoint={axisEndPoint} />
      </>
    );
  }

  const halfDetector = fov[1] * 0.5;
  const axisStartPoint1 = new Vector3(x, y - halfDetector);
  const axisEndPoint1 = new Vector3(x, y - halfDetector - axisSize);
  const axisStartPoint2 = new Vector3(x, y + halfDetector);
  const axisEndPoint2 = new Vector3(x, y + halfDetector + axisSize);
  return (
    <>
      <Label
        datapos={[x + 1, y - halfDetector - axisSize]}
        color={sassVariables.danger}
        text="Axis"
      />
      <AxisLine startPoint={axisStartPoint1} endPoint={axisEndPoint1} />
      <AxisLine startPoint={axisStartPoint2} endPoint={axisEndPoint2} />
    </>
  );
}

interface Props {
  projRotation: number;
  tomoHardware: TomoConfigHardware;
}

function ActualDetectorLocation(props: Props) {
  const { tomoHardware } = props;
  const { sy, sz, somega, sampu, sampv, tomoDetector, detector, sampleStage } =
    tomoHardware;

  if (sy === null || sz === null) {
    logger('Skipped: one of sy, sz is missing.');
    return <></>;
  }

  /**
   * Optional clock wise position in degree
   */
  function optionalCwPosInDeg(motor: MotorSchema | null) {
    if (motor === null) {
      return null;
    }
    const { position } = motor.properties;
    if (position === null) {
      return position;
    }
    const sign = isMotorClockWise(motor) ? 1 : -1;
    return position * sign;
  }
  const detectorCenter = sampleStage?.properties.detector_center ?? [0, 0];
  const detectorCenterY = detectorCenter[0] ?? 0;
  const detectorCenterZ = detectorCenter[1] ?? 0;
  const szPos = posInMM(sz);
  // If no translation over the rotation, there is no u/v location
  const sampuPos = posInMMElseNull(sampu) ?? 0;
  const sampvPos = posInMMElseNull(sampv) ?? 0;
  const syPos = posInMM(sy);
  const somegaPos = optionalCwPosInDeg(somega);
  const fov = fovInMM(tomoDetector, detector);

  function computeAxisLocation() {
    const radProj = (props.projRotation * Math.PI) / 180;
    return -Math.sin(radProj) * sampuPos + Math.cos(radProj) * sampvPos;
  }

  function computeDetectorCenter() {
    const radProj = (props.projRotation * Math.PI) / 180;
    return -Math.sin(radProj) * sampuPos + Math.cos(radProj) * sampvPos + syPos;
  }

  const x = computeDetectorCenter() - detectorCenterY;
  const y = szPos - detectorCenterZ;
  const xAxisDelta = syPos - detectorCenterY;

  return (
    <>
      <CrossMarker
        x={x}
        y={y}
        color={sassVariables.danger}
        sizeInScreen={20}
        lineWidth={2}
        zIndex={2}
      />
      <Label
        datapos={[
          x - (fov ? fov[0] * 0.5 : 0) + 1,
          y - (fov ? fov[1] * 0.5 : 0),
        ]}
        color={sassVariables.danger}
        text={detector?.name ?? 'Detector'}
      />
      {somegaPos !== null && fov !== null && (
        <Detector
          x={x}
          y={y}
          fov={fov}
          rotation={somegaPos}
          projRotation={props.projRotation}
        />
      )}
      <RotationAxis xAxisDelta={xAxisDelta} y={y} fov={fov} detectorX={x} />
    </>
  );
}

export default ActualDetectorLocation;
