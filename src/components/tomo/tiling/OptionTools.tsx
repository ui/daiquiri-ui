import { useState, useEffect, useRef } from 'react';
import { Button, Overlay, Popover } from 'react-bootstrap';
import type { MouseModes } from './types';
import { MarkerSelectionButton } from './MarkerOptions';
import { useOperator } from '../../utils/hooks';
import type { MouseModeInteraction } from '../../h5web/UseMouseModeInteraction';

interface Props {
  mouseModeInteraction: MouseModeInteraction<MouseModes>;
  showRoiTools?: boolean;
  className?: string;
  markerName: string;
  onSelectMarkerName: (name: string) => void;
}

/**
 * Widget to select one of the tools
 */
export default function OptionTools(props: Props) {
  const { mouseModeInteraction, showRoiTools = true } = props;
  const { mouseMode, setOrResetMouseMode } = mouseModeInteraction;
  const markerRef = useRef(null);
  const isMarkerMode = mouseMode === 'mark-position';
  const [showMarkers, setShowMarkers] = useState(false);
  const operator = useOperator();

  useEffect(() => {
    setShowMarkers(isMarkerMode);
  }, [isMarkerMode]);

  return (
    <>
      <Button
        ref={markerRef}
        variant={mouseMode === 'mark-position' ? 'primary' : 'secondary'}
        title="Pin a position in the sample stage"
        onClick={() => {
          setOrResetMouseMode('mark-position');
        }}
      >
        <i className="fa fa-map-marker fa-fw fa-lg" />
      </Button>
      <Button
        variant={mouseMode === 'move-motors' ? 'danger' : 'secondary'}
        title="Click on the scene to move the active detector at a specific location"
        disabled={!operator}
        onClick={() => {
          setOrResetMouseMode('move-motors');
        }}
        size="sm"
      >
        <i className="fad fam-move-samplestage fa-fw fa-2x" />
      </Button>
      {showRoiTools && (
        <Button
          variant={mouseMode === 'create' ? 'primary' : 'secondary'}
          title="Click on the scene to create a new scan ROI"
          onClick={() => {
            setOrResetMouseMode('create');
          }}
          size="sm"
        >
          <i className="fa fam-roi-new fa-fw fa-lg" />
        </Button>
      )}
      <Overlay
        show={showMarkers}
        placement="bottom"
        container={markerRef.current}
        target={markerRef.current}
        containerPadding={20}
      >
        <Popover id="popover-contained" body>
          <MarkerSelectionButton
            value={props.markerName}
            onSelect={props.onSelectMarkerName}
          />
        </Popover>
      </Overlay>
    </>
  );
}
