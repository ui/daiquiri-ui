import type { Rect } from '@h5web/lib';
import type { TomoConfigHardware } from 'connect/tomo/utils';
import { fovInMM, getDetectorCenter } from '../geometry';
import debug from 'debug';
import type { RectRoiGeometry } from 'components/h5web/items/rois/geometries';

const logger = debug('daiquiri.components.tomo.Point3DRoi');

export interface VSegment3DRoiGeometry {
  x: number;
  y: number;
  zstart: number;
  zstop: number;
}

/**
 * Project the ROI into a 2D geometry for display.
 */
export function getGeometryFromRoi(
  roi: VSegment3DRoiGeometry | null,
  projRotation: number
): { x: number; ystart: number; ystop: number } {
  if (roi === null) {
    return { x: 0, ystart: 0, ystop: 0 };
  }
  if (projRotation === 0) {
    return { x: roi.y, ystart: roi.zstart, ystop: roi.zstop };
  } else if (projRotation === -90) {
    return { x: roi.x, ystart: roi.zstart, ystop: roi.zstop };
  }
  console.error(`Unimplemented projRotation ${projRotation}`);
  return { x: 0, ystart: 0, ystop: 0 };
}

/**
 * Update the ROI from an update of a 2D geometry.
 */
export function updateRoiFromGeometry(
  previousRoi: VSegment3DRoiGeometry,
  geometry: RectRoiGeometry,
  projRotation: number
): VSegment3DRoiGeometry {
  const x = (geometry.x1 + geometry.x2) * 0.5;
  const y = (geometry.y1 + geometry.y2) * 0.5;
  if (projRotation === 0) {
    return { x: previousRoi.x, y: x, zstart: geometry.y1, zstop: geometry.y2 };
  } else if (projRotation === -90) {
    return { x, y: previousRoi.y, zstart: geometry.y1, zstop: geometry.y2 };
  }
  console.error(`Unimplemented projRotation ${projRotation}`);
  return { x: 0, y: 0, zstart: 0, zstop: 0 };
}

/**
 * Update the ROI from selection interaction.
 */
export function updateRoiFromSelectionTool(
  previousRoi: VSegment3DRoiGeometry,
  selection: Rect,
  projRotation: number
): VSegment3DRoiGeometry {
  const { x, y } = selection[0];
  if (projRotation === 0) {
    return {
      x: previousRoi.x,
      y: x,
      zstart: selection[0].y,
      zstop: selection[1].y,
    };
  } else if (projRotation === -90) {
    return {
      x,
      y: previousRoi.y,
      zstart: selection[0].y,
      zstop: selection[1].y,
    };
  }
  console.error(`Unexpected projRotation ${projRotation}`);
  return previousRoi;
}

/**
 * Create an initial version of the ROI based on the sample stage state
 */
export function createRoiFromSampleStage(
  sampleStage: TomoConfigHardware
): VSegment3DRoiGeometry | null {
  const center = getDetectorCenter(sampleStage);
  const { tomoDetector, detector } = sampleStage;
  const fov = fovInMM(tomoDetector, detector);

  if (!center) {
    logger('Roi reset skipped: center is undefined.');
    return null;
  }
  if (fov === null) {
    logger('Roi reset skipped: fov is null.');
    return null;
  }
  return {
    x: center.x,
    y: center.y,
    zstart: center.z - fov[0] * 0.5,
    zstop: center.z - fov[0] * 0.5,
  };
}
