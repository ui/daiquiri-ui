import { SelectionTool } from '@h5web/lib';
import { useLinearVisCanvas } from 'components/h5web/LinearVisCanvas2';
import type { TomoConfigHardware } from 'connect/tomo/utils';
import type { VSegment3DRoiGeometry } from './VSegment3DRoi';
import {
  createRoiFromSampleStage,
  updateRoiFromSelectionTool,
} from './VSegment3DRoi';

export default function ZseriesScanRoiSelectionTool(props: {
  geometry: VSegment3DRoiGeometry | null;
  onGeometryChanged: (geometry: VSegment3DRoiGeometry) => void;
  disabled: boolean;
  projRotation: 0 | -90;
  sampleStage: TomoConfigHardware;
  id: string;
  resetMouseMode: () => void;
}) {
  const {
    disabled,
    projRotation,
    geometry,
    onGeometryChanged,
    sampleStage,
    resetMouseMode,
  } = props;

  const linearVisCanvas = useLinearVisCanvas();

  return (
    <SelectionTool
      id={props.id}
      disabled={disabled || linearVisCanvas?.overlayCursor !== undefined}
      onSelectionStart={() => {
        linearVisCanvas?.actions.captureMouseInteraction();
      }}
      onSelectionEnd={() => {
        linearVisCanvas?.actions.releaseMouseInteraction();
        resetMouseMode();
      }}
      onSelectionChange={(selection) => {
        if (selection === undefined) {
          return;
        }

        function getDefaultGeometry() {
          if (geometry) {
            return geometry;
          }
          return createRoiFromSampleStage(sampleStage);
        }

        const base = getDefaultGeometry();
        if (base === null) {
          return;
        }

        const roi = updateRoiFromSelectionTool(
          base,
          selection.data,
          projRotation
        );
        onGeometryChanged(roi);
      }}
      onValidSelection={(selection) => {
        if (geometry === null) {
          return;
        }
        const roi = updateRoiFromSelectionTool(
          geometry,
          selection.data,
          projRotation
        );
        onGeometryChanged(roi);
      }}
    >
      {(selection) => <></>}
    </SelectionTool>
  );
}
