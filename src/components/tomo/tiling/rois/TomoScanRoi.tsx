import { useCallback, useMemo } from 'react';
import type { Point3DRoiGeometry } from './Point3DRoi';
import { getGeometryFromRoi } from './Point3DRoi';
import { updateRoiFromGeometry } from './Point3DRoi';
import { RectRoi } from 'components/h5web/items/rois/RectRoi';
import Label from 'components/h5web/items/Label';
import type { TomoDetectorSchema } from 'components/hardware/tomodetector';
import type { LimaSchema } from 'components/hardware/lima';
import { fovInMM } from '../../utils/geometry';
import sassVariables from 'scss/variables.module.scss';
import type { RectRoiGeometry } from '../../../h5web/items/rois/geometries';

interface Props {
  roi: Point3DRoiGeometry;
  readOnly?: boolean;
  onDragMove?: (roi: Point3DRoiGeometry) => void;
  onDragEnd?: (roi: Point3DRoiGeometry) => void;
  projRotation: 0 | -90;
  color?: string;
  label?: string;
  cellSize?: [number, number];
}

function Point3DRoi(props: Props) {
  const { roi, projRotation, cellSize = [0, 0] } = props;

  const onGeometryChanged = useCallback(
    (geometry: RectRoiGeometry, dragging: boolean) => {
      const newRoi = updateRoiFromGeometry(roi, geometry, projRotation);
      if (dragging) {
        if (props.onDragMove) {
          props.onDragMove(newRoi);
        }
      } else {
        if (props.onDragEnd) {
          props.onDragEnd(newRoi);
        }
      }
    },
    [roi, props.onDragMove, props.onDragEnd, projRotation]
  );

  const projRoi = getGeometryFromRoi(roi, projRotation);

  return (
    <>
      <Label
        datapos={[projRoi.x, projRoi.y - cellSize[1] * 0.5]}
        text={props.label ?? ''}
        color={props.color}
        anchor="bottom"
      />
      <RectRoi
        geometry={{
          x1: projRoi.x - cellSize[0] * 0.5,
          y1: projRoi.y - cellSize[1] * 0.5,
          x2: projRoi.x + cellSize[0] * 0.5,
          y2: projRoi.y + cellSize[1] * 0.5,
        }}
        readOnly={props.readOnly}
        onGeometryChanged={onGeometryChanged}
        color={props.color}
      />
    </>
  );
}

export function TomoScanRoi(props: {
  roi: Point3DRoiGeometry;
  tomoDetector: TomoDetectorSchema | null;
  detector: LimaSchema | null;
  readOnly?: boolean;
  onDragMove?: (roi: Point3DRoiGeometry) => void;
  onDragEnd?: (roi: Point3DRoiGeometry) => void;
  projRotation: 0 | -90;
}) {
  const fov = useMemo(() => {
    return fovInMM(props.tomoDetector, props.detector);
  }, [props.tomoDetector, props.detector]);
  return (
    <Point3DRoi
      roi={props.roi}
      cellSize={fov ?? undefined}
      onDragEnd={props.onDragEnd}
      onDragMove={props.onDragMove}
      projRotation={props.projRotation}
      color={sassVariables.success}
      label="Tomo"
      readOnly={props.readOnly}
    />
  );
}
