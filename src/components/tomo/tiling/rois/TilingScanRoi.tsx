import { useCallback, useMemo } from 'react';
import { Vector3 } from 'three';
import { Vector2 } from 'three';
import { RectRoi } from 'components/h5web/items/rois/RectRoi';
import Label from 'components/h5web/items/Label';
import { Grid } from 'components/h5web/items/shapes/Grid';
import type { CubeRoiGeometry } from './CubeRoi';
import { getGeometryFromRoi, updateRoiFromGeometry } from './CubeRoi';
import type { TomoDetectorSchema } from '../../../hardware/tomodetector';
import type { LimaSchema } from 'components/hardware/lima';
import { fovInMM } from '../../utils/geometry';
import sassVariables from 'scss/variables.module.scss';
import type { RectRoiGeometry } from '../../../h5web/items/rois/geometries';

interface Props {
  roi: CubeRoiGeometry;
  readOnly?: boolean;
  onDragMove?: (roi: CubeRoiGeometry) => void;
  onDragEnd?: (roi: CubeRoiGeometry) => void;
  projRotation: 0 | -90;
  color?: string;
  label?: string;
  grid?: Vector3;
  maxCellSize?: [number, number];
}

function CubeRoi(props: Props) {
  const { roi, projRotation, maxCellSize = [0, 0] } = props;

  const onGeometryChanged = useCallback(
    (geometry: RectRoiGeometry, dragging: boolean) => {
      const newRoi = updateRoiFromGeometry(roi, geometry, projRotation);
      if (dragging) {
        if (props.onDragMove) {
          props.onDragMove(newRoi);
        }
      } else {
        if (props.onDragEnd) {
          props.onDragEnd(newRoi);
        }
      }
    },
    [roi, props.onDragMove, props.onDragEnd, projRotation]
  );

  const { box, nbRow, nbCol } = getGeometryFromRoi(
    roi,
    projRotation,
    props.grid
  );
  const size: Vector2 = new Vector2();
  box.getSize(size);
  const center: Vector2 = new Vector2();
  box.getCenter(center);

  function computeOverlap(): [number, number] | undefined {
    if (props.readOnly) {
      return undefined;
    }
    const [cellSizeX, cellSizeY] = maxCellSize;
    const nbCellX = Math.ceil(size.x / cellSizeX);
    const nbCellY = Math.ceil(size.y / cellSizeY);
    const overlapSizeX = (cellSizeX * nbCellX - size.x) / (nbCellX - 1);
    const overlapSizeY = (cellSizeY * nbCellY - size.y) / (nbCellY - 1);
    const overlapX = nbCellX <= 1 ? 0 : overlapSizeX / cellSizeX;
    const overlapY = nbCellY <= 1 ? 0 : overlapSizeY / cellSizeY;
    return [overlapX, overlapY];
  }

  const overlap = computeOverlap();

  return (
    <>
      <Label
        datapos={[(box.min.x + box.max.x) * 0.5, box.min.y]}
        text={props.label ?? ''}
        color={props.color}
        anchor="bottom"
      />
      <Grid
        center={[center.x, center.y]}
        size={[size.x, size.y]}
        color={props.color}
        opacity={0.25}
        nbCells={[nbCol ?? 1, nbRow ?? 1]}
        overlap={overlap}
        lineWidth={1}
        zIndex={0.5}
      />
      <RectRoi
        geometry={{
          x1: box.min.x,
          y1: box.min.y,
          x2: box.max.x,
          y2: box.max.y,
        }}
        readOnly={props.readOnly}
        onGeometryChanged={onGeometryChanged}
        color={props.color}
      />
    </>
  );
}

function useGridRoi(
  tilingRoi: CubeRoiGeometry,
  tomoDetector: TomoDetectorSchema | null,
  detector: LimaSchema | null
): [Vector3, [number, number]] | undefined {
  return useMemo(() => {
    const fov = fovInMM(tomoDetector, detector);
    if (fov === null) {
      return undefined;
    }
    const size = new Vector3();
    size.add(tilingRoi.start);
    size.sub(tilingRoi.stop);

    return [
      new Vector3(
        Math.ceil(Math.abs(size.x) / fov[0]),
        Math.ceil(Math.abs(size.y) / fov[0]),
        Math.ceil(Math.abs(size.z) / fov[1])
      ),
      fov,
    ];
  }, [tilingRoi, tomoDetector]);
}

export function TilingScanRoi(props: {
  roi: CubeRoiGeometry;
  tomoDetector: TomoDetectorSchema | null;
  detector: LimaSchema | null;
  readOnly?: boolean;
  onDragMove?: (roi: CubeRoiGeometry) => void;
  onDragEnd?: (roi: CubeRoiGeometry) => void;
  projRotation: 0 | -90;
}) {
  const [roiGrid, detFov] = useGridRoi(
    props.roi,
    props.tomoDetector,
    props.detector
  ) ?? [undefined, undefined];
  return (
    <CubeRoi
      roi={props.roi}
      grid={roiGrid}
      maxCellSize={detFov}
      onDragEnd={props.onDragEnd}
      onDragMove={props.onDragMove}
      projRotation={props.projRotation}
      color={sassVariables.success}
      label="Tiling"
      readOnly={props.readOnly}
    />
  );
}
