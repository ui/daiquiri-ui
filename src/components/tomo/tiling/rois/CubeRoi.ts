import { Vector3 } from 'three';
import { Box2, Vector2 } from 'three';
import debug from 'debug';

import type { RectRoiGeometry } from 'components/h5web/items/rois/geometries';
import type { TomoConfigHardware } from 'connect/tomo/utils';
import { getDetectorCenter } from '../geometry';
import { fovInMM } from '../../utils/geometry';
import type { Rect } from '@h5web/lib';

const logger = debug('daiquiri.components.tomo.tiling.rois.CubeRoi');

export interface CubeRoiGeometry {
  start: Vector3;
  stop: Vector3;
}

function getProjectedCoord(
  coord: Vector3,
  projRotation: number
): Vector2 | null {
  if (projRotation === 0) {
    return new Vector2(coord.y, coord.z);
  } else if (projRotation === -90) {
    return new Vector2(coord.x, coord.z);
  }
  console.error(`Unimplemented projRotation ${projRotation}`);
  return null;
}

function getRowCol(
  projRotation: number,
  grid?: Vector3
): { nbRow?: number; nbCol?: number } {
  if (grid === undefined) {
    return {};
  }
  if (projRotation === 0) {
    return { nbRow: grid.z, nbCol: grid.y };
  } else if (projRotation === -90) {
    return { nbRow: grid.z, nbCol: grid.x };
  }
  return {};
}

/**
 * Project the ROI into a 2D geometry for display.
 */
export function getGeometryFromRoi(
  roi: CubeRoiGeometry | null,
  projRotation: number,
  grid?: Vector3
): { box: Box2; nbRow?: number; nbCol?: number } {
  if (roi === null) {
    return { box: new Box2() };
  }
  const start = getProjectedCoord(roi.start, projRotation);
  const stop = getProjectedCoord(roi.stop, projRotation);
  if (start === null || stop === null) {
    return { box: new Box2() };
  }
  const rowCol = getRowCol(projRotation, grid);
  return { box: new Box2().setFromPoints([start, stop]), ...rowCol };
}

/**
 * Update the ROI from an update of a 2D geometry.
 */
export function updateRoiFromGeometry(
  previous: CubeRoiGeometry,
  box: RectRoiGeometry,
  projRotation: number
): CubeRoiGeometry {
  function getVector3(
    previous: Vector3,
    position: Vector2,
    projRotation: number
  ): Vector3 {
    if (projRotation === 0) {
      return new Vector3(previous?.x ?? 0, position.x, position.y);
    } else if (projRotation === -90) {
      return new Vector3(position.x, previous?.y ?? 0, position.y);
    }
    console.error(`Unexpected projRotation ${projRotation}`);
    return previous;
  }

  return {
    start: getVector3(
      previous.start,
      new Vector2(box.x1, box.y1),
      projRotation
    ),
    stop: getVector3(previous.stop, new Vector2(box.x2, box.y2), projRotation),
  };
}

/**
 * Update the ROI from selection interaction.
 */
export function updateRoiFromSelectionTool(
  previousRoi: CubeRoiGeometry,
  selection: Rect,
  projRotation: number
): CubeRoiGeometry {
  const rect = {
    x1: selection[0].x,
    y1: selection[0].y,
    x2: selection[1].x,
    y2: selection[1].y,
  };
  return updateRoiFromGeometry(previousRoi, rect, projRotation);
}

/**
 * Create an initial version of the ROI based on the sample stage state
 */
export function createRoiFromSampleStage(
  sampleStage: TomoConfigHardware
): CubeRoiGeometry | null {
  const center = getDetectorCenter(sampleStage);
  const { tomoDetector, detector } = sampleStage;
  const fov = fovInMM(tomoDetector, detector);

  if (!center) {
    logger('Roi reset skipped: center is undefined.');
    return null;
  }
  if (fov === null) {
    logger('Roi reset skipped: fov is null.');
    return null;
  }
  return {
    start: new Vector3(
      center.x - fov[0] * 1.4,
      center.y - fov[0] * 1.4,
      center.z - fov[1] * 1.4
    ),
    stop: new Vector3(
      center.x + fov[0] * 1.4,
      center.y + fov[0] * 1.4,
      center.z + fov[1] * 1.4
    ),
  };
}
