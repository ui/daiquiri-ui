import { Button, ButtonGroup, Overlay, Popover, Stack } from 'react-bootstrap';
import { Formatting } from '@esrf/daiquiri-lib';
import type { MouseModes, ScanRoiKind } from '../types';
import type { CubeRoiGeometry } from './CubeRoi';
import type { Point3DRoiGeometry } from './Point3DRoi';
import NewScanButton from 'connect/samples/NewScanButton';
import type { Dispatch, SetStateAction } from 'react';
import { useCallback, useRef } from 'react';
import type { MouseModeInteraction } from 'components/h5web/UseMouseModeInteraction';
import { getTomoHardware } from 'connect/tomo/utils';
import store from 'store';
import type { VSegment3DRoiGeometry } from './VSegment3DRoi';
import { fovInMM } from '../geometry';

interface Props {
  mouseModeInteraction: MouseModeInteraction<MouseModes>;
  editScanRoi: ScanRoiKind | null;
  setEditScanRoi: Dispatch<SetStateAction<ScanRoiKind | null>>;
  className?: string;
  clearTilingRoi: () => void;
  tilingRoi: CubeRoiGeometry | null;
  clearScanRoi: () => void;
  scanRoi: Point3DRoiGeometry | null;
  clearZseriesRoi: () => void;
  zseriesRoi: VSegment3DRoiGeometry | null;
  tomoConfigId: string;
}

/**
 * Widget to select one of the tools
 */
function TilingSelectionButtons(props: Props) {
  const {
    tilingRoi: roi,
    clearTilingRoi: clearRoi,
    mouseModeInteraction,
    tomoConfigId,
  } = props;
  const { mouseMode, setOrResetMouseMode } = mouseModeInteraction;
  const isCreatingTilingRoiMode = mouseMode === 'create-tiling-roi';

  const getRoiData = useCallback(() => {
    if (roi === null) {
      return undefined;
    }
    const state = store.getState();
    const hardwareMap = state.hardware.ns_hardware.default.results;
    const tomoHardware = getTomoHardware(hardwareMap, tomoConfigId);
    const { sampleStage } = tomoHardware;
    const detectorCenter = sampleStage?.properties.detector_center ?? [0, 0];
    const detectorCenterY = detectorCenter[0] ?? 0;
    const detectorCenterZ = detectorCenter[1] ?? 0;

    return {
      start_x: Formatting.round(roi.start.x + detectorCenterY, 3),
      end_x: Formatting.round(roi.stop.x + detectorCenterY, 3),
      start_y: Formatting.round(roi.start.y + detectorCenterY, 3),
      end_y: Formatting.round(roi.stop.y + detectorCenterY, 3),
      start_z: Formatting.round(roi.start.z + detectorCenterZ, 3),
      end_z: Formatting.round(roi.stop.z + detectorCenterZ, 3),
    };
  }, [roi, tomoConfigId]);

  return (
    <Stack direction="horizontal">
      <div className="text-dark p-2">Tiling:</div>
      <ButtonGroup className={props.className}>
        <Button
          variant={isCreatingTilingRoiMode ? 'primary' : 'secondary'}
          title="Initialize the location of the ROI"
          onClick={() => {
            setOrResetMouseMode('create-tiling-roi');
          }}
        >
          <i className="fa fam-roi-new fa-fw" />
        </Button>
        <Button
          variant="secondary"
          title="Initialize the location of the ROI"
          onClick={() => {
            clearRoi();
          }}
          disabled={roi === null}
        >
          <i className="fa fa-trash-can fa-fw" />
        </Button>
        <NewScanButton
          button={
            <span className="text-nowrap">
              <i className="fa fa-plus" title="Launch a new tiling scan" /> New
              Scan
            </span>
          }
          disabled={roi === null}
          options={{
            tags: ['tomo-tiling-roi'],
            providers: {
              metadata: {
                datacollections: {
                  namespace: 'tilingview',
                },
              },
            },
          }}
          additionalFormData={getRoiData}
        />
      </ButtonGroup>
    </Stack>
  );
}

/**
 * Widget to select one of the tools
 */
function ScanSelectionButtons(props: Props) {
  const {
    scanRoi: roi,
    clearScanRoi: clearRoi,
    mouseModeInteraction,
    tomoConfigId,
  } = props;
  const { mouseMode, setOrResetMouseMode } = mouseModeInteraction;
  const isCreatingTilingRoiMode = mouseMode === 'create-tiling-roi';

  const getRoiData = useCallback(() => {
    if (roi === null) {
      return undefined;
    }
    const state = store.getState();
    const hardwareMap = state.hardware.ns_hardware.default.results;
    const tomoHardware = getTomoHardware(hardwareMap, tomoConfigId);
    const { sampleStage } = tomoHardware;
    const detectorCenter = sampleStage?.properties.detector_center ?? [0, 0];
    const detectorCenterY = detectorCenter[0] ?? 0;
    const detectorCenterZ = detectorCenter[1] ?? 0;

    return {
      x: Formatting.round(roi.x + detectorCenterY, 3),
      y: Formatting.round(roi.y + detectorCenterY, 3),
      z: Formatting.round(roi.z + detectorCenterZ, 3),
    };
  }, [roi, tomoConfigId]);

  return (
    <Stack direction="horizontal">
      <div className="text-dark p-2">Tomo:</div>
      <ButtonGroup className={props.className}>
        <Button
          variant={isCreatingTilingRoiMode ? 'primary' : 'secondary'}
          title="Initialize the location of the ROI"
          onClick={() => {
            setOrResetMouseMode('create-tiling-roi');
          }}
        >
          <i className="fa fam-roi-new fa-fw" />
        </Button>
        <Button
          variant="secondary"
          title="Initialize the location of the ROI"
          onClick={() => {
            clearRoi();
          }}
          disabled={roi === null}
        >
          <i className="fa fa-trash-can fa-fw" />
        </Button>
        <NewScanButton
          button={
            <span className="text-nowrap">
              <i className="fa fa-plus" title="Launch a new tiling scan" /> New
              Scan
            </span>
          }
          disabled={roi === null}
          options={{
            tags: ['tomo-tomo-roi'],
            providers: {
              metadata: {
                datacollections: {
                  namespace: 'tilingview',
                },
              },
            },
          }}
          additionalFormData={getRoiData}
        />
      </ButtonGroup>
    </Stack>
  );
}

/**
 * Widget to select one of the tools
 */
function ZseriesSelectionButtons(props: Props) {
  const {
    zseriesRoi: roi,
    clearZseriesRoi: clearRoi,
    mouseModeInteraction,
    tomoConfigId,
  } = props;
  const { mouseMode, setOrResetMouseMode } = mouseModeInteraction;
  const isCreatingTilingRoiMode = mouseMode === 'create-tiling-roi';

  const getRoiData = useCallback(() => {
    if (roi === null) {
      return undefined;
    }
    const state = store.getState();
    const hardwareMap = state.hardware.ns_hardware.default.results;
    const tomoHardware = getTomoHardware(hardwareMap, tomoConfigId);
    const { sampleStage, tomoDetector, detector } = tomoHardware;
    const detectorCenter = sampleStage?.properties.detector_center ?? [0, 0];
    const detectorCenterY = detectorCenter[0] ?? 0;
    const detectorCenterZ = detectorCenter[1] ?? 0;
    const direction = Math.sign(roi.zstop - roi.zstart);
    const fov = fovInMM(tomoDetector, detector);
    if (fov === null) {
      return undefined;
    }
    const halfvfov = fov[1] * 0.5;
    const zStart = roi.zstart + detectorCenterZ + direction * halfvfov;
    const zStop = roi.zstop + detectorCenterZ - direction * halfvfov;
    const height = zStop - zStart;

    return {
      x: Formatting.round(roi.x + detectorCenterY, 3),
      y: Formatting.round(roi.y + detectorCenterY, 3),
      z_start: Formatting.round(zStart, 3),
      z_stop: Formatting.round(zStop, 3),
      z_height: Formatting.round(Math.abs(height) + fov[1], 3),
      z_direction: direction > 0 ? 'Down' : 'Up',
    };
  }, [roi, tomoConfigId]);

  return (
    <Stack direction="horizontal">
      <div className="text-dark p-2">Zseries:</div>
      <ButtonGroup className={props.className}>
        <Button
          variant={isCreatingTilingRoiMode ? 'primary' : 'secondary'}
          title="Initialize the location of the ROI"
          onClick={() => {
            setOrResetMouseMode('create-tiling-roi');
          }}
        >
          <i className="fa fam-roi-new fa-fw" />
        </Button>
        <Button
          variant="secondary"
          title="Initialize the location of the ROI"
          onClick={() => {
            clearRoi();
          }}
          disabled={roi === null}
        >
          <i className="fa fa-trash-can fa-fw" />
        </Button>
        <NewScanButton
          button={
            <span className="text-nowrap">
              <i className="fa fa-plus" title="Launch a new tiling scan" /> New
              Scan
            </span>
          }
          disabled={roi === null}
          options={{
            tags: ['tomo-zseries-roi'],
            providers: {
              metadata: {
                datacollections: {
                  namespace: 'tilingview',
                },
              },
            },
          }}
          additionalFormData={getRoiData}
        />
      </ButtonGroup>
    </Stack>
  );
}

/**
 * Widget to select one of the tools
 */
export default function OptionTilingScan(props: Props) {
  const { editScanRoi, setEditScanRoi } = props;
  const tilingRef = useRef(null);
  const scanRef = useRef(null);
  const zseriesRef = useRef(null);

  function setOrUnsetScanRoi(expected: ScanRoiKind | null) {
    setEditScanRoi((s) => {
      return s === expected ? null : expected;
    });
  }

  return (
    <>
      <ButtonGroup className={props.className}>
        <Button
          ref={tilingRef}
          variant={editScanRoi === 'tiling' ? 'primary' : 'secondary'}
          title="ROI for tiling scan"
          onClick={() => {
            setOrUnsetScanRoi('tiling');
          }}
        >
          <i className="fa fam-roi-rect-tile fa-fw fa-lg" />
        </Button>
        <Button
          ref={scanRef}
          variant={editScanRoi === 'scan' ? 'primary' : 'secondary'}
          title="ROI for tomo scan"
          onClick={() => {
            setOrUnsetScanRoi('scan');
          }}
        >
          <i className="fa fam-roi-rect fa-fw fa-lg" />
        </Button>
        <Button
          ref={zseriesRef}
          variant={editScanRoi === 'zseries' ? 'primary' : 'secondary'}
          title="ROI for zseries scan"
          onClick={() => {
            setOrUnsetScanRoi('zseries');
          }}
        >
          <i className="fa fam-roi-rect-tile-v fa-fw fa-lg" />
        </Button>
      </ButtonGroup>
      <Overlay
        show={editScanRoi === 'tiling'}
        placement="bottom"
        container={tilingRef.current}
        target={tilingRef.current}
        containerPadding={20}
      >
        <Popover id="popover-tiling-contained">
          <TilingSelectionButtons {...props} className={undefined} />
        </Popover>
      </Overlay>
      <Overlay
        show={editScanRoi === 'scan'}
        placement="bottom"
        container={scanRef.current}
        target={scanRef.current}
        containerPadding={20}
      >
        <Popover id="popover-scan-contained">
          <ScanSelectionButtons {...props} className={undefined} />
        </Popover>
      </Overlay>
      <Overlay
        show={editScanRoi === 'zseries'}
        placement="bottom"
        container={zseriesRef.current}
        target={zseriesRef.current}
        containerPadding={20}
      >
        <Popover id="popover-zseries-contained">
          <ZseriesSelectionButtons {...props} className={undefined} />
        </Popover>
      </Overlay>
    </>
  );
}
