import type { Rect } from '@h5web/lib';
import type { TomoConfigHardware } from 'connect/tomo/utils';
import { fovInMM, getDetectorCenter } from '../geometry';
import debug from 'debug';
import type { RectRoiGeometry } from 'components/h5web/items/rois/geometries';

const logger = debug('daiquiri.components.tomo.Point3DRoi');

export interface Point3DRoiGeometry {
  x: number;
  y: number;
  z: number;
}

/**
 * Project the ROI into a 2D geometry for display.
 */
export function getGeometryFromRoi(
  roi: Point3DRoiGeometry | null,
  projRotation: number
): { x: number; y: number } {
  if (roi === null) {
    return { x: 0, y: 0 };
  }
  if (projRotation === 0) {
    return { x: roi.y, y: roi.z };
  } else if (projRotation === -90) {
    return { x: roi.x, y: roi.z };
  }
  console.error(`Unimplemented projRotation ${projRotation}`);
  return { x: 0, y: 0 };
}

/**
 * Update the ROI from an update of a 2D geometry.
 */
export function updateRoiFromGeometry(
  previousRoi: Point3DRoiGeometry,
  geometry: RectRoiGeometry,
  projRotation: number
): Point3DRoiGeometry {
  const x = (geometry.x1 + geometry.x2) * 0.5;
  const y = (geometry.y1 + geometry.y2) * 0.5;
  if (projRotation === 0) {
    return { x: previousRoi.x, y: x, z: y };
  } else if (projRotation === -90) {
    return { x, y: previousRoi.y, z: y };
  }
  console.error(`Unimplemented projRotation ${projRotation}`);
  return { x: 0, y: 0, z: 0 };
}

/**
 * Update the ROI from selection interaction.
 */
export function updateRoiFromSelectionTool(
  previousRoi: Point3DRoiGeometry,
  selection: Rect,
  projRotation: number
): Point3DRoiGeometry {
  const { x, y } = selection[1];
  if (projRotation === 0) {
    return { x: previousRoi.x, y: x, z: y };
  } else if (projRotation === -90) {
    return { x, y: previousRoi.y, z: y };
  }
  console.error(`Unexpected projRotation ${projRotation}`);
  return previousRoi;
}

/**
 * Create an initial version of the ROI based on the sample stage state
 */
export function createRoiFromSampleStage(
  sampleStage: TomoConfigHardware
): Point3DRoiGeometry | null {
  const center = getDetectorCenter(sampleStage);

  if (!center) {
    logger('Roi reset skipped: center is undefined.');
    return null;
  }

  return {
    x: center.x,
    y: center.y,
    z: center.z,
  };
}
