import { useCallback, useMemo } from 'react';
import type { VSegment3DRoiGeometry } from './VSegment3DRoi';
import { getGeometryFromRoi } from './VSegment3DRoi';
import { updateRoiFromGeometry } from './VSegment3DRoi';
import { RectRoi } from 'components/h5web/items/rois/RectRoi';
import Label from 'components/h5web/items/Label';
import { Grid } from 'components/h5web/items/shapes/Grid';
import sassVariables from 'scss/variables.module.scss';
import type { TomoDetectorSchema } from 'components/hardware/tomodetector';
import type { LimaSchema } from 'components/hardware/lima';
import { fovInMM } from '../../utils/geometry';
import type { RectRoiGeometry } from '../../../h5web/items/rois/geometries';

interface Props {
  roi: VSegment3DRoiGeometry;
  readOnly?: boolean;
  onDragMove?: (roi: VSegment3DRoiGeometry) => void;
  onDragEnd?: (roi: VSegment3DRoiGeometry) => void;
  projRotation: 0 | -90;
  color?: string;
  label?: string;
  cellSize?: [number, number];
}

function VSegmentRoi(props: Props) {
  const { roi, projRotation, cellSize = [0, 0] } = props;

  const onGeometryChanged = useCallback(
    (geometry: RectRoiGeometry, dragging: boolean) => {
      const newRoi = updateRoiFromGeometry(roi, geometry, projRotation);
      if (dragging) {
        if (props.onDragMove) {
          props.onDragMove(newRoi);
        }
      } else {
        if (props.onDragEnd) {
          props.onDragEnd(newRoi);
        }
      }
    },
    [roi, props.onDragMove, props.onDragEnd, projRotation]
  );

  const projRoi = getGeometryFromRoi(roi, projRotation);
  const sizeY = Math.abs(projRoi.ystop - projRoi.ystart);
  const centerY = (projRoi.ystart + projRoi.ystop) * 0.5;

  const cellSizeY = cellSize[1];
  const nbCellY = Math.ceil(sizeY / cellSizeY);

  function computeOverlap(): [number, number] | undefined {
    if (props.readOnly) {
      return undefined;
    }
    const overlapSizeY = (cellSizeY * nbCellY - sizeY) / (nbCellY - 1);
    const overlapY = nbCellY <= 1 ? 0 : overlapSizeY / cellSizeY;
    return [0, overlapY];
  }

  const overlap = computeOverlap();

  return (
    <>
      <Label
        datapos={[projRoi.x, Math.min(projRoi.ystart, projRoi.ystop)]}
        text={props.label ?? ''}
        color={props.color}
        anchor="bottom"
      />
      <Label
        datapos={[projRoi.x, projRoi.ystart]}
        text="Start"
        color={props.color}
        anchor="top"
      />
      <Label
        datapos={[projRoi.x, projRoi.ystop]}
        text="Stop"
        color={props.color}
        anchor="top"
      />
      <Grid
        center={[projRoi.x, centerY]}
        size={[cellSize[0], sizeY]}
        color={props.color}
        opacity={0.25}
        nbCells={[1, nbCellY ?? 1]}
        overlap={overlap}
        lineWidth={1}
        zIndex={0.5}
      />
      <RectRoi
        geometry={{
          x1: projRoi.x - cellSize[0] * 0.5,
          y1: projRoi.ystart,
          x2: projRoi.x + cellSize[0] * 0.5,
          y2: projRoi.ystop,
        }}
        readOnly={props.readOnly}
        onGeometryChanged={onGeometryChanged}
        color={props.color}
      />
    </>
  );
}

export function ZseriesScanRoi(props: {
  roi: VSegment3DRoiGeometry;
  tomoDetector: TomoDetectorSchema | null;
  detector: LimaSchema | null;
  readOnly?: boolean;
  onDragMove?: (roi: VSegment3DRoiGeometry) => void;
  onDragEnd?: (roi: VSegment3DRoiGeometry) => void;
  projRotation: 0 | -90;
}) {
  const fov = useMemo(() => {
    return fovInMM(props.tomoDetector, props.detector);
  }, [props.tomoDetector, props.detector]);
  return (
    <VSegmentRoi
      roi={props.roi}
      cellSize={fov ?? undefined}
      onDragEnd={props.onDragEnd}
      onDragMove={props.onDragMove}
      projRotation={props.projRotation}
      color={sassVariables.success}
      label="Zseries"
      readOnly={props.readOnly}
    />
  );
}
