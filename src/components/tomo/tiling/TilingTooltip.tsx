import type { TomoConfigHardware } from 'connect/tomo/utils';
import type { MotorSchema } from '@esrf/daiquiri-lib';
import { Container, Row, Col } from 'react-bootstrap';
import { toMotorPos, posInMM } from '../utils/geometry';

const N_DIGITS = 2;

function AnyRow(props: { title: string; value: number; unit: string }) {
  return (
    <Row className="flex-nowrap" xs={12}>
      <Col xs={3}>{props.title}</Col>
      <Col className="text-end" xs={6}>
        {props.value.toFixed(N_DIGITS)}
      </Col>
      <Col className="text-start" xs={3}>
        {props.unit}
      </Col>
    </Row>
  );
}

function MotorRow(props: {
  motor: MotorSchema | null;
  position: number;
  defaultName: string;
}) {
  const { motor } = props;
  const [motorPos, unit] = toMotorPos(motor, props.position);
  const name = motor?.alias ?? motor?.name ?? props.defaultName;
  return <AnyRow title={name} value={motorPos} unit={unit} />;
}

/** Tooltip displayed inside the tiling plots */
export default function TilingTooltip(props: {
  projRotation: number;
  tomoHardware: TomoConfigHardware;
  x?: number;
  y?: number;
  z: number;
  value: number;
}) {
  const { tomoHardware, value } = props;
  const { sampleStage } = tomoHardware;
  const detectorCenter = sampleStage?.properties.detector_center ?? [0, 0];
  const detectorCenterY = detectorCenter[0] ?? 0;
  const detectorCenterZ = detectorCenter[1] ?? 0;

  const sy = tomoHardware.sy ? posInMM(tomoHardware.sy) : 0;
  return (
    <Container>
      {props.x !== undefined && (
        <MotorRow
          motor={tomoHardware.sampu}
          position={props.x - sy + detectorCenterY}
          defaultName="sample-u-axis"
        />
      )}
      {props.y !== undefined && (
        <MotorRow
          motor={tomoHardware.sampv}
          position={props.y - sy + detectorCenterY}
          defaultName="sample-v-axis"
        />
      )}
      {props.z !== undefined && (
        <MotorRow
          motor={tomoHardware.sz}
          position={props.z + detectorCenterZ}
          defaultName="z-axis"
        />
      )}
      <AnyRow title="intensity" value={value} unit="" />
    </Container>
  );
}
