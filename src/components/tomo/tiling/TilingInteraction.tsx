import type { CanvasEvent } from '@h5web/lib';
import { useCallback } from 'react';
import type { TomoScanTaskActions } from 'types/Tomo';
import Qty from 'js-quantities';
import type { TomoConfigHardware } from 'connect/tomo/utils';
import RulerSelectionTool from '../../h5web/RulerSelectionTool';
import type { FieldOfViewSelection } from '../dropdownfieldofview/DropdownFieldOfView';
import PointerClick from '../../h5web/SelectionPoint';
import { MarkerInteraction } from './TilingMarkers';
import type { ComputeSampleStageAtPlotLocation } from './types';
import debug from 'debug';
import { requestMoveSampleStage } from '../../../services/TomoService';

const logger = debug('daiquiri.components.tomo.tiling.TilingInteraction');

/**
 * Plot component handling mouse interaction used to move the motors
 */
function MoveMotorInteraction(props: {
  disabled: boolean;
  projRotation: number;
  onActionRequested: () => void;
  computeSampleStageAtPlotLocation: ComputeSampleStageAtPlotLocation;
  sampleStage: TomoConfigHardware;
}) {
  const {
    disabled,
    onActionRequested,
    projRotation,
    computeSampleStageAtPlotLocation,
    sampleStage,
  } = props;
  const { sy } = sampleStage;

  const onClick = useCallback(
    (evt: CanvasEvent<MouseEvent>) => {
      onActionRequested();
      if (projRotation === 0) {
        const y = evt.dataPt.x;
        const z = evt.dataPt.y;
        const motion = computeSampleStageAtPlotLocation(null, y, z);
        requestMoveSampleStage(motion);
      } else if (projRotation === -90) {
        const x = evt.dataPt.x;
        const z = evt.dataPt.y;
        const motion = computeSampleStageAtPlotLocation(x, null, z);
        requestMoveSampleStage(motion);
      } else {
        console.error(`Unexpected projRotation ${projRotation}`);
      }
    },
    [sy, computeSampleStageAtPlotLocation, onActionRequested]
  );

  if (disabled) {
    logger('MoveMotorInteraction disabled.');
    return <></>;
  }
  return <PointerClick onClick={onClick} />;
}

/**
 * Plot component handling mouse interaction used to create a ROI
 */
function CreateRoiInteraction(props: {
  disabled: boolean;
  projRotation: number;
  roiFov: FieldOfViewSelection | null;
  actions: TomoScanTaskActions;
  onActionRequested: () => void;
}) {
  const { disabled, roiFov, projRotation, actions, onActionRequested } = props;

  const createScanTask = useCallback(
    (x: number, y: number, z: number) => {
      if (roiFov) {
        actions.createScanTask({
          x: new Qty(x, 'mm'),
          y: new Qty(y, 'mm'),
          z: new Qty(z, 'mm'),
          detector: roiFov.detector,
          magnification: roiFov.magnification,
          fov: [new Qty(roiFov.fov[0], 'mm'), new Qty(roiFov.fov[1], 'mm')],
          pixelSize: [
            new Qty(roiFov.pixelSize[0], 'um'),
            new Qty(roiFov.pixelSize[1], 'um'),
          ],
          active: true,
        });
      }
    },
    [roiFov, actions]
  );

  const onClick = useCallback(
    (evt: CanvasEvent<MouseEvent>) => {
      onActionRequested();

      if (projRotation === 0) {
        createScanTask(evt.dataPt.x, 0, evt.dataPt.y);
      } else if (projRotation === -90) {
        createScanTask(0, evt.dataPt.x, evt.dataPt.y);
      } else {
        console.error(`Unexpected projRotation ${projRotation}`);
      }
    },
    [createScanTask, onActionRequested, projRotation]
  );

  if (disabled || roiFov === null) {
    logger('CreateRoiInteraction disabled.');
    return <></>;
  }
  return <PointerClick onClick={onClick} />;
}

interface Props {
  projRotation: 0 | -90;
  mouseMode: string;
  resetMouseMode: () => void;
  actions: any;
  computeSampleStageAtPlotLocation: ComputeSampleStageAtPlotLocation;
  operator: boolean;
  sampleStage: any;
  roiFov: any;
  markerName: any;
  setMarker: any;
}

function TilingInteraction(props: Props) {
  const {
    projRotation,
    mouseMode,
    resetMouseMode,
    actions,
    computeSampleStageAtPlotLocation,
    operator,
    sampleStage,
    roiFov,
    markerName,
    setMarker,
  } = props;

  return (
    <>
      <RulerSelectionTool mouseMode={mouseMode} plotUnit="mm" />
      <CreateRoiInteraction
        projRotation={projRotation}
        disabled={mouseMode !== 'create'}
        roiFov={roiFov}
        actions={actions}
        onActionRequested={() => resetMouseMode()}
      />
      <MoveMotorInteraction
        projRotation={projRotation}
        disabled={!operator || mouseMode !== 'move-motors'}
        computeSampleStageAtPlotLocation={computeSampleStageAtPlotLocation}
        sampleStage={sampleStage}
        onActionRequested={() => resetMouseMode()}
      />
      <MarkerInteraction
        projRotation={projRotation}
        disabled={mouseMode !== 'mark-position'}
        selectedMarker={markerName}
        setMarker={setMarker}
      />
    </>
  );
}

export type { Props as TilingInteractionProps };
export default TilingInteraction;
