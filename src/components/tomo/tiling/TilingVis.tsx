import type { PropsWithChildren } from 'react';
import { useCallback, useMemo } from 'react';
import { capitalize } from 'lodash';
import type { Box2 } from 'three';
import type { TomoScanTask, TomoScanTaskActions } from 'types/Tomo';
import Qty from 'js-quantities';
import type { TomoSampleStageRefs } from 'connect/tomo/utils';
import ActualDetectorLocation from './ActualDetectorLocation';
import type { ColorMapProps } from './models';
import SampleStageLimits from './SampleStageLimits';
import { LinearVisCanvas } from '../../h5web/LinearVisCanvas2';
import TilingTooltip from './TilingTooltip';
import { expandBox2ByFactor, toBoxExtent } from './utils';
import { decodeFloat16 } from '../../h5web/NdFloat16Array';
import TomoScanTaskSelectRect from './TomoScanTaskSelectRect';
import type { FieldOfViewSelection } from '../dropdownfieldofview/DropdownFieldOfView';
import type { ComputeSampleStageAtPlotLocation, ScanRoiKind } from './types';
import type { CubeRoiGeometry } from './rois/CubeRoi';
import type { Point3DRoiGeometry } from './rois/Point3DRoi';
import type { VSegment3DRoiGeometry } from './rois/VSegment3DRoi';
import type { MouseModes } from './types';
import type { MarkersType, SetMarkerAction } from './TilingMarkers';
import { MarkersItem } from './TilingMarkers';
import { getSampleStageBounds } from './geometry';
import TilingInteraction from './TilingInteraction';
import TilingMesh from './TilingMesh';
import { TilingScanRoi } from './rois/TilingScanRoi';
import type { TomoTilesApi } from 'services/tiling/providers';
import { LayerDType } from 'services/tiling/providers';
import type { TilingViewConfig } from '../config/TilingView';
import { useTomoConfigRelations } from '../utils/hooks';
import TilingRoiSelectionTool from './rois/TilingScanRoiSelectionTool';
import type { MouseModeInteraction } from 'components/h5web/UseMouseModeInteraction';
import { FlatLocation } from './FlatLocation';
import type { TomoFlatMotionSchema } from 'components/hardware/tomoflatmotion';
import { TomoScanRoi } from './rois/TomoScanRoi';
import TomoRoiSelectionTool from './rois/TomoScanRoiSelectionTool';
import { ZseriesScanRoi } from './rois/ZseriesScanRoi';
import ZseriesScanRoiSelectionTool from './rois/ZseriesScanRoiSelectionTool';
import { OutsidePlotDomain } from 'components/h5web/items/shapes/OutsidePlotDomain';
import sassVariables from 'scss/variables.module.scss';

interface Props extends ColorMapProps {
  projRotation: 0 | -90;
  config: TilingViewConfig;
  api: TomoTilesApi | undefined;
  tilingBox: Box2;
  actions: TomoScanTaskActions;
  computeSampleStageAtPlotLocation: ComputeSampleStageAtPlotLocation;
  tasks: TomoScanTask[];
  sampleStage: TomoSampleStageRefs | null;
  operator: boolean;
  roiFov: FieldOfViewSelection | null;
  markerName: string;
  mouseModeInteraction: MouseModeInteraction<MouseModes>;
  markers: MarkersType;
  setMarker: SetMarkerAction;
  tilingRoi: CubeRoiGeometry | null;
  setTilingRoi: (roi: CubeRoiGeometry | null) => void;
  scanRoi: Point3DRoiGeometry | null;
  setScanRoi: (roi: Point3DRoiGeometry | null) => void;
  zseriesRoi: VSegment3DRoiGeometry | null;
  setZseriesRoi: (roi: VSegment3DRoiGeometry | null) => void;
  editScanRoi: ScanRoiKind | null;
  tomoFlatMotion: TomoFlatMotionSchema | null;
}

function useTilingDomain(
  sampleStageRef: TomoSampleStageRefs | null,
  projRotation: 0 | -90
) {
  const sampleStage = useTomoConfigRelations(sampleStageRef);
  return useMemo(() => {
    const { maxBox } = getSampleStageBounds(sampleStage, projRotation);
    return toBoxExtent(expandBox2ByFactor(maxBox, 0.1));
  }, [sampleStage]);
}

function TilingVis(props: PropsWithChildren<Props>) {
  const {
    projRotation,
    config,
    api,
    tilingBox,
    actions,
    tasks,
    computeSampleStageAtPlotLocation,
    operator,
    roiFov,
    markerName,
    children,
    mouseModeInteraction,
    setMarker,
    markers,
    editScanRoi,
    tomoFlatMotion,
    ...colorMapProps
  } = props;

  const plotDomain = useTilingDomain(props.sampleStage, projRotation);
  const { mouseMode, resetMouseMode } = mouseModeInteraction;
  const tomoHardware = useTomoConfigRelations(props.sampleStage);
  const { tomoDetector, detector } = tomoHardware;

  const dragTomoScanRoi = useCallback(
    (id: TomoScanTask['id'], x: number, y: number, z: number) =>
      actions.moveScanTaskRoi(
        id,
        new Qty(x, 'mm'),
        new Qty(y, 'mm'),
        new Qty(z, 'mm')
      ),
    []
  );

  const view = projRotation === 0 ? 'front' : 'side';

  const convertTooltipValue = useCallback(
    (v: number) =>
      api?.layerDType === LayerDType.Float16 ? decodeFloat16(v) : v,
    [api]
  );

  const cursors: Record<string, string> = {
    'create-tiling-roi': 'crosshair',
    'move-motors': 'crosshair',
    create: 'crosshair',
    'measure-distance': 'crosshair',
    'mark-position': 'crosshair',
    'edit-tiling-roi': 'crosshair',
  };

  return (
    <LinearVisCanvas
      abscissaConfig={{
        visDomain: plotDomain.x,
        // the front have to be flipped
        // the side, have to be consistent with detector view rotation icons:
        //           right now it is displayed on the right side of the front view
        flip: view === 'front',
      }}
      ordinateConfig={{
        visDomain: plotDomain.y,
        flip: true,
      }}
      aspect="equal"
      title={`${capitalize(view)} (in mm)`}
      showAxes={config.displayAxes}
      mouseMode={mouseMode}
      cursors={cursors}
    >
      <OutsidePlotDomain
        domainX={plotDomain.x}
        domainY={plotDomain.y}
        color={sassVariables.light}
      />
      {props.tilingRoi && (
        <TilingScanRoi
          roi={props.tilingRoi}
          tomoDetector={tomoDetector}
          detector={detector}
          onDragMove={props.setTilingRoi}
          onDragEnd={props.setTilingRoi}
          projRotation={projRotation}
          readOnly={editScanRoi !== 'tiling'}
        />
      )}
      {props.scanRoi && (
        <TomoScanRoi
          roi={props.scanRoi}
          tomoDetector={tomoDetector}
          detector={detector}
          onDragMove={props.setScanRoi}
          onDragEnd={props.setScanRoi}
          projRotation={projRotation}
          readOnly={editScanRoi !== 'scan'}
        />
      )}
      {props.zseriesRoi && (
        <ZseriesScanRoi
          roi={props.zseriesRoi}
          tomoDetector={tomoDetector}
          detector={detector}
          onDragMove={props.setZseriesRoi}
          onDragEnd={props.setZseriesRoi}
          projRotation={projRotation}
          readOnly={editScanRoi !== 'zseries'}
        />
      )}
      <TilingRoiSelectionTool
        id="tiling-roi-selection-tool"
        disabled={mouseMode !== 'create-tiling-roi' || editScanRoi !== 'tiling'}
        geometry={props.tilingRoi}
        onGeometryChanged={props.setTilingRoi}
        projRotation={projRotation}
        sampleStage={tomoHardware}
        resetMouseMode={resetMouseMode}
      />
      <TomoRoiSelectionTool
        id="scan-roi-selection-tool"
        disabled={mouseMode !== 'create-tiling-roi' || editScanRoi !== 'scan'}
        geometry={props.scanRoi}
        onGeometryChanged={props.setScanRoi}
        projRotation={projRotation}
        sampleStage={tomoHardware}
        resetMouseMode={resetMouseMode}
      />
      <ZseriesScanRoiSelectionTool
        id="zseries-roi-selection-tool"
        disabled={
          mouseMode !== 'create-tiling-roi' || editScanRoi !== 'zseries'
        }
        geometry={props.zseriesRoi}
        onGeometryChanged={props.setZseriesRoi}
        projRotation={projRotation}
        sampleStage={tomoHardware}
        resetMouseMode={resetMouseMode}
      />
      <TilingInteraction
        projRotation={projRotation}
        mouseMode={mouseMode}
        resetMouseMode={mouseModeInteraction.resetMouseMode}
        roiFov={roiFov}
        actions={actions}
        computeSampleStageAtPlotLocation={computeSampleStageAtPlotLocation}
        sampleStage={tomoHardware}
        markerName={markerName}
        operator={operator}
        setMarker={setMarker}
      />
      <TilingMesh
        api={api}
        box={tilingBox}
        renderTooltip={(x, y, v) => {
          return (
            <TilingTooltip
              projRotation={projRotation}
              x={view === 'side' ? x : undefined}
              y={view === 'front' ? x : undefined}
              z={y}
              tomoHardware={tomoHardware}
              value={convertTooltipValue(v)}
            />
          );
        }}
        {...colorMapProps}
      />
      {tasks.map((task) => (
        <TomoScanTaskSelectRect
          key={task.id}
          task={task}
          view={view}
          readOnly={mouseMode !== 'pan' && mouseMode !== 'zoom'}
          dragTomoScanRoi={dragTomoScanRoi}
        />
      ))}
      {tomoHardware && (
        <>
          <SampleStageLimits
            projRotation={projRotation}
            tomoHardware={tomoHardware}
          />
          <ActualDetectorLocation
            projRotation={projRotation}
            tomoHardware={tomoHardware}
          />
          <FlatLocation
            projRotation={projRotation}
            tomoHardware={tomoHardware}
            tomoFlatMotion={tomoFlatMotion}
          />
        </>
      )}
      <MarkersItem
        projRotation={projRotation}
        tomoHardware={tomoHardware}
        markers={markers}
        setMarker={setMarker}
        computeSampleStageAtPlotLocation={computeSampleStageAtPlotLocation}
      />
      {children}
    </LinearVisCanvas>
  );
}

export type { Props as TilingVisProps };
export default TilingVis;
