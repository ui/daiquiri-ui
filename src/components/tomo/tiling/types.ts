import type Qty from 'js-quantities';
import type { Vector3 } from 'three';

export type MouseModes =
  | 'zoom'
  | 'pan'
  | 'move-motors'
  | 'create'
  | 'measure-distance'
  | 'measure-ortho'
  | 'measure-angle'
  | 'create-tiling-roi'
  | 'mark-position';

export type ScanRoiKind = 'tiling' | 'zseries' | 'scan';

export type RequestMoveSampleStageAtPlotLocation = (
  x: number | null,
  y: number | null,
  z: number | null
) => void;

export type ComputeSampleStageAtPlotLocation = (
  x: number | null,
  y: number | null,
  z: number | null
) => { sz?: Qty; sampu?: Qty; sampv?: Qty };

export interface TilingPlotActions {
  resetZoom: () => void;
  setScale: (scale: number) => void;
}
