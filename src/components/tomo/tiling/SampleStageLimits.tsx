import type { TomoConfigHardware } from 'connect/tomo/utils';
import { getSampleStageBounds } from './geometry';
import sassVariables from 'scss/variables.module.scss';
import Label from 'components/h5web/items/Label';
import { Rect } from 'components/h5web/items/shapes/Rect';

interface Props {
  projRotation: number;
  tomoHardware: TomoConfigHardware;
}

function SampleStageLimits(props: Props) {
  const { projRotation, tomoHardware } = props;

  const { motorBox, visibleBox } = getSampleStageBounds(
    tomoHardware,
    projRotation
  );

  return (
    <>
      {!motorBox.isEmpty() && (
        <>
          <Label
            datapos={visibleBox.min}
            color={sassVariables.info}
            text="Visible limit"
          />
          <Rect
            startPoint={[visibleBox.min.x, visibleBox.min.y]}
            endPoint={[visibleBox.max.x, visibleBox.max.y]}
            lineColor={sassVariables.info}
            dashSize={8}
            gapSize={4}
          />
        </>
      )}
      {!visibleBox.isEmpty() && (
        <>
          <Label
            datapos={motorBox.min}
            color={sassVariables.info}
            text="Motor limit"
          />
          <Rect
            startPoint={[motorBox.min.x, motorBox.min.y]}
            endPoint={[motorBox.max.x, motorBox.max.y]}
            lineColor={sassVariables.info}
            dashSize={2}
            gapSize={2}
          />
        </>
      )}
    </>
  );
}

export default SampleStageLimits;
