import type { Dispatch, PropsWithChildren, SetStateAction } from 'react';
import {
  Dropdown,
  ButtonGroup,
  Button,
  Container,
  Row,
  Col,
} from 'react-bootstrap';
import type { TilingViewConfig } from '../config/TilingView';

interface Props {
  variant?: string;
  as?: any;
  dropDirection?: 'up' | 'down';
  config: TilingViewConfig;
}

function selectedVariant<T>(value: T, expected: T) {
  return value === expected ? 'primary' : 'secondary';
}

function SelectedButton<T>(
  props: PropsWithChildren<{
    var: T;
    value: T;
    setter: Dispatch<SetStateAction<T>>;
  }>
) {
  return (
    <Button
      variant={selectedVariant(props.var, props.value)}
      onClick={() => {
        props.setter(props.value);
      }}
      className="text-nowrap"
      size="sm"
    >
      {props.children}
    </Button>
  );
}

export default function DropdownTilingViewConfig(props: Props) {
  const { as = undefined, variant = 'secondary', config } = props;

  return (
    <Dropdown
      as={as}
      className={`${props.dropDirection === 'up' ? 'dropup' : ''}`}
    >
      <Dropdown.Toggle
        id="dropdown-basic"
        variant={variant}
        className="d-flex align-items-center"
      >
        <i className="fa fa-sliders fa-fw fa-lg" />
      </Dropdown.Toggle>
      <Dropdown.Menu className="dropdown-menu-center">
        <div className="ms-1 me-1" style={{ minWidth: '300px' }}>
          <Container>
            <Row>
              <Col className="my-auto">Display axes</Col>
              <Col xs={7}>
                <ButtonGroup>
                  <SelectedButton
                    var={config.displayAxes}
                    value
                    setter={config.setDisplayAxes}
                  >
                    Yes
                  </SelectedButton>
                  <SelectedButton
                    var={config.displayAxes}
                    value={false}
                    setter={config.setDisplayAxes}
                  >
                    No
                  </SelectedButton>
                </ButtonGroup>
              </Col>
            </Row>
            <Row className="mt-1">
              <Col className="my-auto">Layout</Col>
              <Col xs={7}>
                <ButtonGroup>
                  <SelectedButton
                    var={config.layoutView}
                    value="front"
                    setter={config.setLayoutView}
                  >
                    Front
                  </SelectedButton>
                  <SelectedButton
                    var={config.layoutView}
                    value="side"
                    setter={config.setLayoutView}
                  >
                    Side
                  </SelectedButton>
                  <SelectedButton
                    var={config.layoutView}
                    value="front-side"
                    setter={config.setLayoutView}
                  >
                    Both
                  </SelectedButton>
                </ButtonGroup>
              </Col>
            </Row>
            <Row className="mt-1">
              <Col className="my-auto">Color bar</Col>
              <Col xs={7}>
                <ButtonGroup>
                  <SelectedButton
                    var={config.colorbarVisible}
                    value
                    setter={config.setColorbarVisible}
                  >
                    Displayed
                  </SelectedButton>
                  <SelectedButton
                    var={config.colorbarVisible}
                    value={false}
                    setter={config.setColorbarVisible}
                  >
                    Hide
                  </SelectedButton>
                </ButtonGroup>
              </Col>
            </Row>
            <Row className="mt-1">
              <Col className="my-auto">Crosshair</Col>
              <Col xs={7}>
                <ButtonGroup>
                  <SelectedButton
                    var={config.crossHair}
                    value
                    setter={config.setCrossHair}
                  >
                    Yes
                  </SelectedButton>
                  <SelectedButton
                    var={config.crossHair}
                    value={false}
                    setter={config.setCrossHair}
                  >
                    No
                  </SelectedButton>
                </ButtonGroup>
              </Col>
            </Row>
            <Row className="mt-1">
              <Col className="my-auto">Toolbar</Col>
              <Col xs={7}>
                <ButtonGroup>
                  <SelectedButton
                    var={config.toolbarLocation}
                    value="top"
                    setter={config.setToolbarLocation}
                  >
                    Top
                  </SelectedButton>
                  <SelectedButton
                    var={config.toolbarLocation}
                    value="bottom"
                    setter={config.setToolbarLocation}
                  >
                    Bottom
                  </SelectedButton>
                </ButtonGroup>
              </Col>
            </Row>
          </Container>
        </div>
      </Dropdown.Menu>
    </Dropdown>
  );
}
