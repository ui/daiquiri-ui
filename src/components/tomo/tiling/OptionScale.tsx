import { Dropdown, Button, ButtonGroup } from 'react-bootstrap';
import type { TilingPlotActions } from './types';

interface Props {
  resetZoomEnabled: boolean;
  actions: TilingPlotActions;
}

/**
 * Widget to select the scale mode for the tomo view
 */
export default function OptionScale(props: Props) {
  const { resetZoomEnabled, actions } = props;

  return (
    <Dropdown as={ButtonGroup}>
      <Button
        title="Reset zoom (sample stage overview)"
        variant="secondary"
        disabled={!resetZoomEnabled}
        onClick={() => {
          actions.resetZoom();
        }}
      >
        <i className="fa fa-expand fa-fw fa-lg" />
      </Button>
      <Dropdown.Toggle split id="dropdown-split-basic" variant="secondary" />
      <Dropdown.Menu>
        <Dropdown.Item
          onClick={() => {
            actions.setScale(1);
          }}
        >
          ×1 (overview)
        </Dropdown.Item>
        <Dropdown.Item
          onClick={() => {
            actions.setScale(2);
          }}
        >
          ×2
        </Dropdown.Item>
        <Dropdown.Item
          onClick={() => {
            actions.setScale(10);
          }}
        >
          ×10
        </Dropdown.Item>
        <Dropdown.Item
          onClick={() => {
            actions.setScale(100);
          }}
        >
          ×100
        </Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  );
}
