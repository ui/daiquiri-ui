import type { ColorMap } from '@h5web/lib';
import { ScaleType } from '@h5web/lib';
import {
  Dropdown,
  Button,
  Container,
  Row,
  Col,
  ButtonGroup,
} from 'react-bootstrap';
import type { TomoDetectorArray } from 'services/TomoDetectorService';
import { ImageProfile } from 'services/TomoDetectorService';
import { AutoscaleMode } from 'components/h5web/colormap';
import {
  LutIcon,
  AutoscaleIcon,
  ScaleIcon,
} from 'components/h5web/bootstrap/Icons';

interface Props {
  as?: any;
  variant?: string;
  dropDirection?: 'up' | 'down';
  showDisplayed?: boolean;
  isDisplayed: boolean;
  lut: ColorMap;
  norm: ScaleType;
  autoscale: boolean;
  autoscaleMode: AutoscaleMode;
  isInverted: boolean;
  profile: string;
  onSelectDisplayed: (value: boolean) => void;
  onSelectLut: (value: ColorMap) => void;
  onSelectAutoscale: (value: boolean) => void;
  onSelectAutoscaleMode: (value: AutoscaleMode) => void;
  onSelectNorm: (value: ScaleType) => void;
  onSelectInvertion: (value: boolean) => void;
  onSelectProfile: (value: ImageProfile) => void;
  array?: TomoDetectorArray | null;
}

function Info(props: { array: TomoDetectorArray | null | undefined }) {
  if (!props.array || !props.array.info) {
    return <></>;
  }
  function formatMemory(bytes: number) {
    if (bytes < 1000) {
      return `${bytes} B`;
    }
    if (bytes < 100_000) {
      return `${(bytes / 1000).toFixed(1)} KB`;
    }
    return `${(bytes / 1_000_000).toFixed(1)} MB`;
  }
  function formatShape(shape: number[]) {
    return shape.map((s) => `${s}`).join(' × ');
  }

  const info = props.array.info;
  return (
    <>
      <Row>
        <h6 className="flex-grow-1 text-center">Info</h6>
      </Row>
      <Row>
        <Col>
          <h6>Shape</h6>
        </Col>
        <Col>{formatShape(props.array.array.shape)}</Col>
      </Row>
      <Row>
        <Col>
          <h6>Profile</h6>
        </Col>
        <Col>{info.profile}</Col>
      </Row>
      <Row>
        <Col>
          <h6>Size</h6>
        </Col>
        <Col>{formatMemory(info.byteLength)}</Col>
      </Row>
      <Row>
        <Col>
          <h6 title="Data type of the source">Type</h6>
        </Col>
        <Col>{info.dtype}</Col>
      </Row>
    </>
  );
}

export default function DropdownDetector(props: Props) {
  const { as = undefined, showDisplayed = true } = props;

  function createLutItem(key: ColorMap, title: string) {
    const active = props.lut === key;
    const variant = active ? 'primary' : 'secondary';
    return (
      <Button
        variant={variant}
        onClick={() => {
          props.onSelectLut(key);
        }}
        className="text-nowrap"
        title={title}
        size="sm"
      >
        <LutIcon name={key} />
      </Button>
    );
  }

  function createAutoscaleModeItem(autoscaleMode: AutoscaleMode) {
    const active = props.autoscaleMode === autoscaleMode;
    const variant =
      active && props.autoscale ? 'primary' : active ? 'warning' : 'secondary';
    const titles: Record<AutoscaleMode, string> = {
      [AutoscaleMode.Minmax]: 'Minmax',
      [AutoscaleMode.StdDev3]: '3×std',
      [AutoscaleMode.None]: 'Manual',
    };
    const title = titles[autoscaleMode];
    return (
      <Button
        variant={variant}
        onClick={() => {
          props.onSelectAutoscaleMode(autoscaleMode);
          props.onSelectAutoscale(true);
        }}
        size="sm"
      >
        {title}
      </Button>
    );
  }

  function createEnabledAutoscaleItem() {
    const variant = props.autoscale ? 'primary' : 'secondary';
    return (
      <Button
        variant={variant}
        onClick={() => {
          props.onSelectAutoscale(!props.autoscale);
        }}
        size="sm"
      >
        <i className="fa-solid fa-circle-half-stroke fa-fw fa-lg" /> Auto scale
        enabled
      </Button>
    );
  }

  function createInvertedItem() {
    const variant = props.isInverted ? 'primary' : 'secondary';
    return (
      <Button
        variant={variant}
        onClick={() => {
          props.onSelectInvertion(!props.isInverted);
        }}
        size="sm"
      >
        <i className="fa fa-fw fa-arrows-alt-v" /> Inverted LUT
      </Button>
    );
  }

  function createNormalizationItem(key: ScaleType, title: string) {
    const active = props.norm === key;
    const variant = active ? 'primary' : 'secondary';
    return (
      <Button
        variant={variant}
        onClick={() => {
          props.onSelectNorm(key);
        }}
        size="sm"
      >
        {title}
      </Button>
    );
  }

  function createDisplayedItem() {
    const variant = props.isDisplayed ? 'primary' : 'secondary';
    return (
      <Button
        variant={variant}
        onClick={() => {
          props.onSelectDisplayed(!props.isDisplayed);
        }}
        size="sm"
      >
        Display detector data
      </Button>
    );
  }

  function createProfileItem(key: ImageProfile, label: string, title: string) {
    const active = props.profile === key;
    const variant = active ? 'primary' : 'secondary';
    return (
      <Button
        variant={variant}
        onClick={() => {
          props.onSelectProfile(key);
        }}
        size="sm"
        title={title}
      >
        {label}
      </Button>
    );
  }

  return (
    <Dropdown
      as={as}
      className={`${props.dropDirection === 'up' ? 'dropup' : ''}`}
    >
      <Dropdown.Toggle
        id="dropdown-basic"
        variant={props.variant}
        className="d-flex align-items-center"
      >
        <i className="fa fam-hardware-camera fa-lg me-1" />
        {props.isDisplayed && (
          <>
            <LutIcon name={props.lut} />
            <ScaleIcon scale={props.norm} />
            {props.autoscale && (
              <AutoscaleIcon autoscale={props.autoscaleMode} />
            )}
          </>
        )}
      </Dropdown.Toggle>
      <Dropdown.Menu className="dropdown-menu-center">
        <div className="ms-1 me-1">
          <Container>
            {showDisplayed && (
              <>
                <Row>
                  <h6 className="flex-grow-1 text-center">Detector</h6>
                </Row>
                <Row>
                  <ButtonGroup className="flex-grow-1 mt-1 mb-2">
                    {createDisplayedItem()}
                  </ButtonGroup>
                </Row>
              </>
            )}
            <Row>
              <h6 className="flex-grow-1 text-center">Lookup table</h6>
            </Row>
            <Row>
              <ButtonGroup className="flex-grow-1">
                {createLutItem('Greys', 'Gray')}
                {createLutItem('Viridis', 'Viridis')}
                {createLutItem('Cividis', 'Cividis')}
                {createLutItem('Magma', 'Magma')}
                {createLutItem('Inferno', 'Inferno')}
                {createLutItem('Plasma', 'Plasma')}
              </ButtonGroup>
            </Row>
            <Row>
              <ButtonGroup className="flex-grow-1 mt-1 mb-2">
                {createInvertedItem()}
              </ButtonGroup>
            </Row>
            <Row>
              <h6 className="flex-grow-1 text-center">Auto scale</h6>
            </Row>
            <Row>
              <ButtonGroup className="flex-grow-1 mt-1">
                {createAutoscaleModeItem(AutoscaleMode.Minmax)}
                {createAutoscaleModeItem(AutoscaleMode.StdDev3)}
              </ButtonGroup>
            </Row>
            <Row>
              <ButtonGroup className="flex-grow-1 mt-1 mb-2">
                {createEnabledAutoscaleItem()}
              </ButtonGroup>
            </Row>
            <Row>
              <h6 className="flex-grow-1 text-center">Normalization</h6>
            </Row>
            <Row>
              <ButtonGroup className="flex-grow-1 mt-1 mb-2">
                {createNormalizationItem(ScaleType.Linear, 'Linear')}
                {createNormalizationItem(ScaleType.Log, 'Log')}
                {createNormalizationItem(ScaleType.SymLog, 'SymLog')}
              </ButtonGroup>
            </Row>
            <Row>
              <h6
                className="flex-grow-1 text-center"
                title="How to transfer the data"
              >
                Profile
              </h6>
            </Row>
            <Row>
              <ButtonGroup className="flex-grow-1 mt-1 mb-2">
                {createProfileItem(
                  ImageProfile.Raw,
                  'Raw',
                  'Transfer data as it is provided by the detector or the processing'
                )}
                {createProfileItem(
                  ImageProfile.F16,
                  'Float 16b',
                  'Transfer data as float 16-bits'
                )}
                {createProfileItem(
                  ImageProfile.U8,
                  '8-bits',
                  'Transfer data as 8-bits'
                )}
              </ButtonGroup>
            </Row>
            <Info array={props.array} />
          </Container>
        </div>
      </Dropdown.Menu>
    </Dropdown>
  );
}
