import { useCallback } from 'react';
import { Vector2 } from 'three';
import sassVariables from 'scss/variables.module.scss';
import type { TomoScanTask } from '../../../types/Tomo';
import { RectRoi } from 'components/h5web/items/rois/RectRoi';
import type { RectRoiGeometry } from 'components/h5web/items/rois/geometries';

interface Props {
  task: TomoScanTask;
  view: 'front' | 'side';
  readOnly?: boolean;
  dragTomoScanRoi: (
    id: TomoScanTask['id'],
    x: number,
    y: number,
    z: number
  ) => void;
}

function TomoScanTaskSelectRect(props: Props) {
  const { task, view, dragTomoScanRoi, readOnly = false } = props;
  const { active, roi } = task;
  const { centerZ, width, height } = roi;
  const { x: centerX, y: centerY } = roi.center;

  const onGeometryChanged = useCallback(
    (geometry: RectRoiGeometry, dragging: boolean) => {
      const newCenterX = (geometry.x1 + geometry.x2) * 0.5;
      const newCenterY = (geometry.y1 + geometry.y2) * 0.5;
      dragTomoScanRoi(
        task.id,
        view === 'front' ? newCenterX : centerX,
        view === 'front' ? centerY : newCenterX,
        newCenterY
      );
    },
    [view, dragTomoScanRoi, centerX, centerY, centerZ]
  );

  if (width === null || height === null) {
    return null;
  }

  if (width === null || height === null) {
    return null;
  }
  const center = view === 'front' ? centerX : centerY;
  const startPoint = new Vector2(center - width / 2, centerZ - height / 2);
  const endPoint = new Vector2(center + width / 2, centerZ + height / 2);

  return (
    <RectRoi
      geometry={{
        x1: startPoint.x,
        y1: startPoint.y,
        x2: endPoint.x,
        y2: endPoint.y,
      }}
      color={sassVariables.info}
      // dashSize={4}
      // gapSize={2}
      lineWidth={2}
      readOnly={!active}
      onGeometryChanged={onGeometryChanged}
    />
  );
}

export type { Props as TomoScanTaskSelectRect };
export default TomoScanTaskSelectRect;
