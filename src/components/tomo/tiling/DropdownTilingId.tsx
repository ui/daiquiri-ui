import { Dropdown, Badge, Button } from 'react-bootstrap';
import type {
  TomovisService,
  ImageTilingItem,
} from '../../../services/TomovisService';
import { humanReadableEvent } from 'helpers/datetime';

interface Props {
  selected: string | null;
  onSelect: (selection: string | null) => void;
  tomovisService: TomovisService;
  as?: any;
  variant?: string;
  dropDirection?: 'up' | 'down';
}

function Item(props: {
  isSelected: boolean;
  imageTiling: ImageTilingItem;
  onDelete: (ids: string) => void;
}) {
  const { imageTiling, isSelected } = props;
  const icon = isSelected ? 'fa fa-fw fa-dot-circle-o' : 'fa fa-fw fa-circle-o';
  const stateVariant = imageTiling.status === 'done' ? 'success' : 'warning';
  const date = new Date(Date.parse(imageTiling.create_time));
  return (
    <Dropdown.Item
      className="d-flex"
      style={{ alignItems: 'center' }}
      eventKey={imageTiling.id}
      active={isSelected}
    >
      <i className={icon} />{' '}
      <span className="ms-2 me-auto pr-2" title={date.toLocaleString()}>
        {humanReadableEvent(date)}
      </span>{' '}
      <Badge className="me-2" bg={stateVariant}>
        {imageTiling.status.toUpperCase()}
      </Badge>{' '}
      <Button
        size="sm"
        variant="danger"
        onClick={(event) => {
          event.stopPropagation();
          props.onDelete(imageTiling.id);
        }}
      >
        <i className="fa fa-fw fa-trash" />
      </Button>
    </Dropdown.Item>
  );
}

export default function DropdownTilingId(props: Props) {
  const { as = undefined, onSelect, selected, variant, tomovisService } = props;
  const imageTilings = tomovisService.imageTilings;

  return (
    <Dropdown
      className={`me-1 ${props.dropDirection === 'up' ? 'dropup' : ''}`}
      as={as}
      onSelect={(eventKey) => {
        onSelect(eventKey);
      }}
      onToggle={(show) => {
        if (show) {
          tomovisService.updateImageTilings();
        }
      }}
    >
      <Dropdown.Toggle
        id="dropdown-basic"
        variant={variant}
        title={selected ? `Tiling selected: ${selected}` : 'No tiling selected'}
      >
        <i className="fa fa-th fa-fw" />
        {(selected === null || imageTilings.length === 0) && (
          <div className="indicator">
            <i
              className="fa fa-xs fa-circle text-warning"
              title="No tiling selected"
            />
          </div>
        )}
      </Dropdown.Toggle>
      <Dropdown.Menu className="dropdown-menu-center">
        {imageTilings.length === 0 && (
          <Dropdown.Item disabled>No tiling available</Dropdown.Item>
        )}
        {imageTilings.map((imageTiling, index) => {
          const isActive = selected === imageTiling.id;
          return (
            <Item
              key={index}
              isSelected={isActive}
              imageTiling={imageTiling}
              onDelete={(id) => {
                tomovisService.deleteImageTiling(id);
                if (isActive) {
                  onSelect(null);
                }
              }}
            />
          );
        })}
      </Dropdown.Menu>
    </Dropdown>
  );
}
