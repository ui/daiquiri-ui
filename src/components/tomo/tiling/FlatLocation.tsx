import type { MotorSchema } from '@esrf/daiquiri-lib';
import type { TomoConfigHardware } from 'connect/tomo/utils';
import Label from 'components/h5web/items/Label';
import sassVariables from 'scss/variables.module.scss';
import { fovInMM, posInMMElseNull } from '../utils/geometry';
import { Rect } from 'components/h5web/items/shapes/Rect';
import debug from 'debug';
import type {
  MotionStep,
  TomoFlatMotionSchema,
} from '../../hardware/tomoflatmotion';
import { useMemo } from 'react';

const logger = debug('daiquiri.components.tomo.tiling.FlatLocation');

function Detector(props: { fov: [number, number]; x: number; y: number }) {
  const { x, y, fov } = props;
  const hfovx = fov[0] * 0.5;
  const hfovy = fov[1] * 0.5;

  return (
    <Rect
      startPoint={[x - hfovx, y - hfovy]}
      endPoint={[x + hfovx, y + hfovy]}
      lineColor={sassVariables.secondary}
      lineWidth={2}
      zIndex={2}
    />
  );
}

function useMotionStep(
  motion: MotionStep[] | undefined,
  motor: MotorSchema | null
): MotionStep | null {
  return useMemo(() => {
    if (motor === null) {
      return null;
    }
    if (motion === undefined) {
      return null;
    }
    for (const m of motion) {
      const ref = m.axisRef.slice(9);
      if (ref === motor.alias || ref === motor.id || ref === motor.name) {
        return m;
      }
    }
    return null;
  }, [motion, motor?.alias, motor?.name, motor?.id]);
}

/**
 * Display the location of the flat based on known axes.
 *
 * Else, display nothing.
 */
export function FlatLocation(props: {
  projRotation: number;
  tomoHardware: TomoConfigHardware;
  tomoFlatMotion: TomoFlatMotionSchema | null;
}) {
  const { tomoHardware, tomoFlatMotion } = props;
  const { sy, sz, sampu, sampv, tomoDetector, detector, sampleStage } =
    tomoHardware;

  const usedSy: MotionStep | null = useMotionStep(
    tomoFlatMotion?.properties.motion,
    sy
  );
  const usedSz: MotionStep | null = useMotionStep(
    tomoFlatMotion?.properties.motion,
    sz
  );

  if (usedSy === null && usedSz === null) {
    return <></>;
  }

  const detectorCenter = sampleStage?.properties.detector_center ?? [0, 0];
  const detectorCenterY = detectorCenter[0] ?? 0;
  const detectorCenterZ = detectorCenter[1] ?? 0;
  const szPos = posInMMElseNull(sz);
  // If no translation over the rotation, there is no u/v location
  const sampuPos = posInMMElseNull(sampu) ?? 0;
  const sampvPos = posInMMElseNull(sampv) ?? 0;
  const syPos = posInMMElseNull(sy);
  const fov = fovInMM(tomoDetector, detector);

  if (fov === null) {
    return <></>;
  }

  function computeDetectorCenter(): [number, number] | null {
    const radProj = (props.projRotation * Math.PI) / 180;
    if (szPos === null || syPos === null) {
      return null;
    }
    return [
      -Math.sin(radProj) * sampuPos +
        Math.cos(radProj) * sampvPos +
        syPos -
        detectorCenterY,
      szPos - detectorCenterZ,
    ];
  }

  const absSz: number | null = usedSz?.absolute_position ?? null;
  const relSz: number | null = usedSz?.relative_position ?? null;
  const absSy: number | null = usedSy?.absolute_position ?? null;
  const relSy: number | null = usedSy?.relative_position ?? null;

  function computeFlatCenter() {
    if (absSy !== null && absSz !== null) {
      return [absSy, absSz];
    }
    const det = computeDetectorCenter();
    if (det === null) {
      return null;
    }
    const x = absSy ?? det[0] + (relSy ?? 0);
    const y = absSz ?? det[1] + (relSz ?? 0);
    return [x, y];
  }

  const pos = computeFlatCenter();
  if (pos === null) {
    return <></>;
  }

  const [x, y] = pos;

  return (
    <>
      <Label
        vmargin={0}
        hmargin={0}
        anchor="center"
        datapos={[x, y]}
        color={sassVariables.primary}
        text="Flat"
      />
      <Detector x={x} y={y} fov={fov} />
    </>
  );
}
