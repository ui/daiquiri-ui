import type { AutoscaleMode } from 'components/h5web/colormap';
import type { TomoDetectorArray } from 'services/TomoDetectorService';
import debug from 'debug';
import type { ColorMap, Domain, ScaleType } from '@h5web/lib';
import ImageMesh from '../../h5web/items/ImageMesh';
import { useSyncScaleDomainFromStatistics } from 'components/h5web/colormap';
import type { Dispatch, SetStateAction } from 'react';

const logger = debug('daiquiri.components.tomo.tiling.ActualDetectorLocation');

interface Props {
  projRotation: 0 | -90;
  clockWiseRotation: boolean;
  detectorArray: TomoDetectorArray | null;
  detectorConfig: {
    scaleDomain: Domain;
    setScaleDomain: Dispatch<SetStateAction<Domain>>;
    scaleType: ScaleType;
    autoscale: boolean;
    autoscaleMode: AutoscaleMode;
    colorMap: ColorMap;
    invertColorMap: boolean;
  };
}

export function TomoDetectorArrayMesh(props: Props) {
  const { detectorConfig, projRotation, clockWiseRotation } = props;

  useSyncScaleDomainFromStatistics({
    statistics: props.detectorArray?.statistics,
    ...detectorConfig,
  });

  if (props.detectorArray === null) {
    return <></>;
  }
  const {
    displayArray,
    imagePosition,
    detectorSize,
    imagePixelSize,
    szPosition,
    imageSomegaPosition,
    detectorCenterY,
    detectorCenterZ,
  } = props.detectorArray;

  if (
    szPosition === null ||
    detectorSize === null ||
    imagePixelSize === null ||
    imageSomegaPosition === null
  ) {
    logger(
      'Skipped: one of imagePosition, szPosition, detectorSize, imagePixelSize, imageSomegaPosition is missing.'
    );
    return <></>;
  }
  const detcy = detectorCenterY?.to('mm').scalar ?? 0;
  const detcz = detectorCenterZ?.to('mm').scalar ?? 0;
  const z = szPosition.to('mm').scalar;
  // If no translation over the rotation, there is no y location
  const y = imagePosition ? imagePosition.to('mm').scalar : 0;
  const rot = imageSomegaPosition.scalar * (clockWiseRotation ? 1 : -1);
  const px = imagePixelSize.to('mm').scalar;

  const epsilon = 0.01;

  function angularDistance(rot1: number, rot2: number): number {
    const delta = (rot1 - rot2) % 360;
    return delta < 0 ? 360 + delta : delta;
  }

  const dist = angularDistance(rot, projRotation);
  const straight = dist < epsilon || dist > 360 - epsilon;
  const inverted = dist > 180 - epsilon && dist < 180 + epsilon;

  if (!straight && !inverted) {
    // We dont have u/v motors to handle intermediate projections
    logger(
      `Skipped: rot ${rot} does not match to the projRotation ${projRotation}.`
    );
    return <></>;
  }

  const flipy = straight ? -1 : 1;
  return (
    <ImageMesh
      values={displayArray}
      domain={detectorConfig.scaleDomain}
      colorMap={detectorConfig.colorMap}
      invertColorMap={detectorConfig.invertColorMap}
      scaleType={detectorConfig.scaleType}
      position={[-flipy * (y - detcy), z - detcz, 2]}
      scale={[flipy * px, px, 1]}
      size={detectorSize}
    />
  );
}
