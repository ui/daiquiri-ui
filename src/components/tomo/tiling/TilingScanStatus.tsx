import { getLabelFromScanState } from '../utils/TomoScans';
import StackedNameState from 'components/common/StackedNameState';
import { useLastTilingScan } from '../utils/hooks';

export default function TilingScanStatus(props: { className?: string }) {
  const tilingScan = useLastTilingScan() ?? { state: 'NONE' };
  const [label, variant] = getLabelFromScanState(tilingScan.state);
  return (
    <StackedNameState
      className={props.className}
      name="Scan"
      state={label}
      stateVariant={variant}
      description="Tiling scan state"
    />
  );
}
