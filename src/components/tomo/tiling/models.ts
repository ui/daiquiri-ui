import type { ColorMap, Domain, VisScaleType } from '@h5web/lib';

export interface ColorMapProps {
  domain: Domain;
  scaleType: VisScaleType;
  colorMap: ColorMap;
  invertColorMap: boolean;
}
