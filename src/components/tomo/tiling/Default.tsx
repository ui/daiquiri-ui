import { useCallback, useEffect, useRef, useState, useMemo } from 'react';
import { ColorBar } from '@h5web/lib';
import Toolbar, { Separator } from 'components/common/Toolbar';
import { Button } from 'react-bootstrap';
import type { TomoScanTask, TomoScanTaskActions } from 'types/Tomo';
import { getTomoHardware } from 'connect/tomo/utils';
import DropdownColormap from 'components/h5web/bootstrap/DropdownColormap';
import { useTomovisTilingService } from 'services/tiling/TomovisTilingService';
import { useTomovisAxiosInstance } from 'services/tiling/TomovisAxiosInstance';
import { useTomovisService } from '../../../services/TomovisService';
import TilingVis from './TilingVis';
import TomovisStatus from './TomovisStatus';
import TilingScanStatus from './TilingScanStatus';
import { toScaleType } from 'components/h5web/utils';
import DefaultMouseModeOptions from '../../h5web/bootstrap/DefaultMouseModeOptions';
import OptionTools from './OptionTools';
import OptionTilingScan from './rois/OptionScan';
import VisSynchroniser from '../../h5web/VisSynchroniser';
import type { VisSynchroniserRef } from '../../h5web/VisSynchroniser';
import DropdownFieldOfView from 'connect/tomo/DropdownFieldOfView';
import type { FieldOfViewSelection } from '../dropdownfieldofview/DropdownFieldOfView';
import DropdownTilingId from './DropdownTilingId';
import DropdownDetector from './DropdownDetector';
import type { MouseModes, TilingPlotActions, ScanRoiKind } from './types';
import type { Point3DRoiGeometry } from './rois/Point3DRoi';
import type { CubeRoiGeometry } from './rois/CubeRoi';
import { useMarkers } from './TilingMarkers';
import { Vector3 } from 'three';
import useUserPreference from '../../utils/UseUserPreference';
import Qty from 'js-quantities';
import { getDetectorCenter } from './geometry';
import { posInMM } from '../utils/geometry';
import {
  useActiveDetectorAcqInfo,
  useActiveDetectorService,
} from '../utils/TomoDetector';
import { TomoDetectorArrayMesh } from './TomoDetectorArrayItem';
import OptionScale from './OptionScale';
import { useDetectorConfig } from '../config/Detector';
import { useTilingViewConfig } from '../config/TilingView';
import DropdownTilingViewConfig from './DropdownTilingViewConfig';
import { useSyncScaleDomainFromStatistics } from '../../h5web/colormap';
import { useStatisticsFromHistogram } from '../../h5web/colormap';
import AutoScaleOption from '../../h5web/bootstrap/AutoscaleOption';
import HistogramDomainSlider from '../../h5web/bootstrap/HistogramDomainSlider';
import VisViewpointRestore from '../../h5web/VisViewpointRestore';
import { useTilingViewpoint } from './state';
import WithToolbar from '../../common/WithToolbar';
import {
  useLastTomovisId,
  useMotorHardware,
  useTomoConfig,
  useTomoConfigRelations,
  useTomoFlatMotionHardware,
  useTomoSampleStageRefs,
} from '../utils/hooks';
import { useStore } from 'react-redux';
import type { ReduxState } from '../../../types/ReduxStore';
import { useOperator } from '../../utils/hooks';
import debug from 'debug';
import { useMouseModeInteraction } from '../../h5web/UseMouseModeInteraction';
import { RulerButton } from '../../h5web/bootstrap/RulerButton';
import { isMotorClockWise } from '../../hardware/motor/utils';
import type { VSegment3DRoiGeometry } from './rois/VSegment3DRoi';

const logger = debug('daiquiri.components.tomo.TomoTilingtiling');

export interface TomoTilingOptions {
  /** Identifier of the tomoconfig hardware object */
  tomoconfig: string;
  /** If true show the ROIs on the UI, else none (the default) */
  showrois?: boolean;
  /** If true provides an action to launch a new tiling scan, else none (the default) */
  showlaunchscan?: boolean;
}

interface Props {
  options: TomoTilingOptions;
  actions: TomoScanTaskActions;
  tasks: TomoScanTask[];
}

function useTilingPlotModel() {
  const frontRef = useRef<VisSynchroniserRef>(null);
  const sideRef = useRef<VisSynchroniserRef>(null);

  function handleFrameSide(center: Vector3, scale: Vector3) {
    if (frontRef.current) {
      const { getCameraViewpoint, updateCameraViewpoint } = frontRef.current;
      const viewpoint = getCameraViewpoint();
      updateCameraViewpoint({
        center: new Vector3(viewpoint.center.x, center.y),
        scale,
      });
    }
  }

  function handleFrameFront(center: Vector3, scale: Vector3) {
    if (sideRef.current) {
      const { getCameraViewpoint, updateCameraViewpoint } = sideRef.current;
      const viewpoint = getCameraViewpoint();
      updateCameraViewpoint({
        center: new Vector3(viewpoint.center.x, center.y),
        scale,
      });
    }
  }

  const actions = useMemo<TilingPlotActions>(() => {
    return {
      resetZoom: () => {
        frontRef.current?.resetZoom();
        sideRef.current?.resetZoom();
      },
      setScale: (scale: number) => {
        frontRef.current?.setScale(1 / scale);
        sideRef.current?.setScale(1 / scale);
      },
    };
  }, [frontRef, sideRef]);

  return {
    handleFrameFront,
    handleFrameSide,
    frontRef,
    sideRef,
    actions,
  };
}

function TomoTiling(props: Props) {
  const { actions, tasks, options } = props;
  const {
    showrois = true,
    showlaunchscan = true,
    tomoconfig: tomoConfigId,
  } = options;
  const lastTomovisId = useLastTomovisId();
  const [tomovisId, setTomovisId] = useState<string | null>(null);
  const operator = useOperator();

  const config = useTilingViewConfig();

  const sampleStageRefs = useTomoSampleStageRefs(tomoConfigId ?? '');

  const [tilingRoi, setTilingRoi] = useUserPreference<CubeRoiGeometry | null>(
    'tomo/tilingroi/roi',
    null
  );

  const [scanRoi, setScanRoi] = useUserPreference<Point3DRoiGeometry | null>(
    'tomo/scanroi/roi',
    null
  );

  const [zseriesRoi, setZseriesRoi] =
    useUserPreference<VSegment3DRoiGeometry | null>(
      'tomo/zseriesroi/roi',
      null
    );

  const [refreshPeriod, setRefreshPeriod] = useUserPreference<number>(
    'tomo/tilingview/refreshperiod',
    30
  );

  const { frontViewpoint, setFrontViewpoint, sideViewpoint, setSideViewpoint } =
    useTilingViewpoint();

  const tomoConfig = useTomoConfig();
  const { tomovis_url: tomovisUrl, tomovis_proxy_url: tomovisProxyUrl } =
    tomoConfig;

  const tomovisRestService = useTomovisAxiosInstance(
    tomovisUrl,
    tomovisProxyUrl
  );

  const detectorConfig = useDetectorConfig();

  const tomovisService = useTomovisService(
    tomovisRestService,
    lastTomovisId,
    30_000
  );

  const {
    apiFront,
    apiSide,
    histogram,
    tilingBoxFront,
    tilingBoxSide,
    invalidate,
    lazyRefresh,
  } = useTomovisTilingService(tomovisRestService, tomovisId);

  const statistics = useStatisticsFromHistogram(histogram);

  useSyncScaleDomainFromStatistics({
    statistics,
    ...config,
  });

  useEffect(() => {
    let refreshInterval: NodeJS.Timeout | null = null;
    if (tomovisService.state === 'READY') {
      lazyRefresh();
    } else if (tomovisService.state === 'PROCESSING') {
      if (refreshPeriod) {
        refreshInterval = setInterval(() => {
          lazyRefresh();
        }, refreshPeriod * 1000);
      }
    }
    return () => {
      if (refreshInterval) {
        clearInterval(refreshInterval);
      }
    };
  }, [tomovisService.state, refreshPeriod]);

  const mouseModeInteraction = useMouseModeInteraction<MouseModes>('zoom');
  const { mouseMode, resetMouseMode } = mouseModeInteraction;

  const store = useStore<ReduxState>();

  const getCenter = useCallback(() => {
    // FIXME: Use the center of the view instead of the detector location
    const state = store.getState();
    const hardwareMap = state.hardware.ns_hardware.default.results;
    const sampleStage = getTomoHardware(hardwareMap, tomoConfigId);
    return getDetectorCenter(sampleStage);
  }, [tomoConfigId]);

  const tomoFlatMotion = useTomoFlatMotionHardware(
    `${tomoConfigId}.flat_motion`
  );

  const [markers, setMarker] = useMarkers({ getCenter });

  const detectorInfo = useActiveDetectorAcqInfo({
    sampleStageRefs,
  });

  const activeDetectorService = useActiveDetectorService({
    detector: detectorInfo,
    sampleStageRefs,
    profile: detectorConfig.profile,
    normalization: detectorConfig.scaleType,
    autoscale: detectorConfig.autoscale,
    autoscaleMode: detectorConfig.autoscaleMode,
    domain: detectorConfig.scaleDomain,
    enabled: config.isDetectorDisplayed,
    minRefreshPeriod: 0.5,
  });
  const detectorArray = activeDetectorService.data;

  useEffect(() => {
    // when the last tomovis id change, select it
    if (tomovisService.lastReadyImageTiling) {
      setTomovisId(tomovisService.lastReadyImageTiling);
    }
  }, [tomovisService.lastReadyImageTiling]);

  useEffect(() => {
    if (!operator && mouseMode === 'move-motors') {
      // reset the move for safety
      resetMouseMode();
    }
  }, [mouseMode, operator]);

  const [roiFov, setRoiFov] = useUserPreference<FieldOfViewSelection | null>(
    'tomo/tilingroi/fov',
    null
  );
  const [markerName, setMarkerName] = useUserPreference<string>(
    'tomo/tilingmarker/selection',
    'primary'
  );
  const [editScanRoi, setEditScanRoi] = useState<ScanRoiKind | null>(null);

  const tilingPlotModel = useTilingPlotModel();

  const computeSampleStageAtPlotLocation = useCallback(
    (x: number | null, y: number | null, z: number | null) => {
      const state = store.getState();
      const hardwareMap = state.hardware.ns_hardware.default.results;
      const tomoHardware = getTomoHardware(hardwareMap, tomoConfigId);
      const { sampleStage } = tomoHardware;
      const detectorCenter = sampleStage?.properties.detector_center ?? [0, 0];
      const detectorCenterY = detectorCenter[0] ?? 0;
      const detectorCenterZ = detectorCenter[1] ?? 0;
      const yrot = tomoHardware.sy ? posInMM(tomoHardware.sy) : 0;

      function getSampuvMotion() {
        if (tomoHardware.sampu === null || tomoHardware.sampv === null) {
          // If no translation over the rotation, we can't move it
          return [undefined, undefined];
        }
        return [
          x === null ? undefined : new Qty(x - yrot + detectorCenterY, 'mm'),
          y === null ? undefined : new Qty(y - yrot + detectorCenterY, 'mm'),
        ];
      }

      const [xx, yy] = getSampuvMotion();
      const zz = z === null ? undefined : new Qty(z + detectorCenterZ, 'mm');
      return { sz: zz, sampu: xx, sampv: yy };
    },
    []
  );

  const dropDirection = config.toolbarLocation === 'top' ? 'down' : 'up';

  const somega = useMotorHardware(sampleStageRefs?.somega ?? null);
  const clockWiseRotation = somega ? isMotorClockWise(somega) : true;

  return (
    <WithToolbar
      className="w-100 h-100"
      style={{
        flex: '1 1 0%',
        display: 'flex',
        flexDirection: 'column',
      }}
      toolbarLocation={config.toolbarLocation}
      toolbar={
        <Toolbar align="center" style={{ zIndex: 1 }}>
          <DefaultMouseModeOptions
            mouseModeInteraction={mouseModeInteraction}
          />
          <OptionScale resetZoomEnabled actions={tilingPlotModel.actions} />
          <Separator />
          <RulerButton mouseModeInteraction={mouseModeInteraction} />
          <OptionTools
            mouseModeInteraction={mouseModeInteraction}
            showRoiTools={showrois}
            markerName={markerName}
            onSelectMarkerName={setMarkerName}
          />
          <DropdownDetector
            variant={config.isDetectorDisplayed ? 'primary' : 'secondary'}
            isInverted={detectorConfig.invertColorMap}
            onSelectInvertion={detectorConfig.setInvertColorMap}
            isDisplayed={config.isDetectorDisplayed}
            onSelectDisplayed={config.setDetectorDisplayed}
            lut={detectorConfig.colorMap}
            onSelectLut={detectorConfig.setColorMap}
            norm={detectorConfig.scaleType}
            onSelectNorm={detectorConfig.setScaleType}
            autoscale={detectorConfig.autoscale}
            onSelectAutoscale={detectorConfig.setAutoscale}
            autoscaleMode={detectorConfig.autoscaleMode}
            onSelectAutoscaleMode={detectorConfig.setAutoscaleMode}
            profile={detectorConfig.profile}
            onSelectProfile={detectorConfig.setProfile}
            array={detectorArray}
            dropDirection={dropDirection}
          />
          {showrois && (
            <DropdownFieldOfView
              value={roiFov}
              onSelect={setRoiFov}
              variant={
                mouseMode === 'create'
                  ? roiFov === null
                    ? 'danger'
                    : 'primary'
                  : 'secondary'
              }
            />
          )}
          <Separator />
          <TilingScanStatus className="me-1 d-none d-lg-block" />
          {showlaunchscan && (
            <OptionTilingScan
              className="d-none d-lg-block"
              mouseModeInteraction={mouseModeInteraction}
              editScanRoi={editScanRoi}
              setEditScanRoi={setEditScanRoi}
              tomoConfigId={tomoConfigId}
              tilingRoi={tilingRoi}
              clearTilingRoi={() => {
                setTilingRoi(null);
              }}
              zseriesRoi={zseriesRoi}
              clearZseriesRoi={() => {
                setZseriesRoi(null);
              }}
              scanRoi={scanRoi}
              clearScanRoi={() => {
                setScanRoi(null);
              }}
            />
          )}
          <Separator />
          <TomovisStatus
            className="me-1 d-none d-lg-block"
            tomovisRestService={tomovisRestService}
            tomovisService={tomovisService}
          />
          <DropdownTilingId
            variant="secondary"
            tomovisService={tomovisService}
            selected={tomovisId}
            onSelect={setTomovisId}
            dropDirection={dropDirection}
          />
          <Button
            variant="secondary"
            disabled={tomovisId === null}
            title={
              tomovisService.state === 'PROCESSING'
                ? `Auto refresh the tiling every ${refreshPeriod.toFixed(
                    1
                  )} seconds`
                : 'Reload current tiling'
            }
            onClick={invalidate}
          >
            <i
              className={`fa fa-rotate-right fa-solid ${
                tomovisService.state === 'PROCESSING' ? 'fa-spin' : ''
              }`}
            />
          </Button>
          <Separator />
          <DropdownColormap
            lut={config.colorMap}
            onSelectLut={config.setColorMap}
            norm={toScaleType(config.scaleType)}
            autoscale={config.autoscale}
            autoscaleMode={config.autoscaleMode}
            onSelectNorm={config.setScaleType}
            isInverted={config.invertColorMap}
            onSelectInvertion={config.setInvertColorMap}
            onSelectAutoscale={config.setAutoscale}
            onSelectAutoscaleMode={config.setAutoscaleMode}
            dropDirection={dropDirection}
          />
          <AutoScaleOption disabled={statistics === undefined} {...config} />
          <HistogramDomainSlider
            disabled={statistics === undefined}
            histogram={histogram}
            {...config}
          />
          <Separator />
          <DropdownTilingViewConfig
            config={config}
            dropDirection={dropDirection}
          />
        </Toolbar>
      }
    >
      <div
        style={{
          flex: '1 1 auto',
          display: 'flex',
          margin: 0,
          minHeight: 0,
        }}
      >
        {(config.layoutView === 'front' ||
          config.layoutView === 'front-side') && (
          <TilingVis
            config={config}
            projRotation={0}
            api={apiFront}
            tilingBox={tilingBoxFront}
            actions={actions}
            computeSampleStageAtPlotLocation={computeSampleStageAtPlotLocation}
            tasks={tasks}
            sampleStage={sampleStageRefs}
            operator={operator}
            domain={config.scaleDomain}
            colorMap={config.colorMap}
            invertColorMap={config.invertColorMap}
            scaleType={config.scaleType}
            roiFov={roiFov}
            markerName={markerName}
            mouseModeInteraction={mouseModeInteraction}
            markers={markers}
            setMarker={setMarker}
            tilingRoi={tilingRoi}
            setTilingRoi={setTilingRoi}
            scanRoi={scanRoi}
            setScanRoi={setScanRoi}
            zseriesRoi={zseriesRoi}
            setZseriesRoi={setZseriesRoi}
            editScanRoi={editScanRoi}
            tomoFlatMotion={tomoFlatMotion}
          >
            {config.isDetectorDisplayed && (
              <TomoDetectorArrayMesh
                clockWiseRotation={clockWiseRotation}
                projRotation={0}
                detectorArray={detectorArray}
                detectorConfig={detectorConfig}
              />
            )}
            <VisSynchroniser
              ref={tilingPlotModel.frontRef}
              onVisViewpointChange={tilingPlotModel.handleFrameFront}
            />
            <VisViewpointRestore
              viewpoint={frontViewpoint}
              setViewpoint={setFrontViewpoint}
            />
          </TilingVis>
        )}
        {config.layoutView === 'front-side' && !config.displayAxes && (
          <div style={{ minWidth: '1em' }} className="p-2 bg-light" />
        )}
        {(config.layoutView === 'side' ||
          config.layoutView === 'front-side') && (
          <TilingVis
            config={config}
            projRotation={-90}
            api={apiSide}
            tilingBox={tilingBoxSide}
            actions={actions}
            computeSampleStageAtPlotLocation={computeSampleStageAtPlotLocation}
            tasks={tasks}
            sampleStage={sampleStageRefs}
            operator={operator}
            domain={config.scaleDomain}
            colorMap={config.colorMap}
            invertColorMap={config.invertColorMap}
            scaleType={config.scaleType}
            roiFov={roiFov}
            markerName={markerName}
            mouseModeInteraction={mouseModeInteraction}
            markers={markers}
            setMarker={setMarker}
            tilingRoi={tilingRoi}
            setTilingRoi={setTilingRoi}
            scanRoi={scanRoi}
            setScanRoi={setScanRoi}
            zseriesRoi={zseriesRoi}
            setZseriesRoi={setZseriesRoi}
            editScanRoi={editScanRoi}
            tomoFlatMotion={tomoFlatMotion}
          >
            {config.isDetectorDisplayed && (
              <TomoDetectorArrayMesh
                clockWiseRotation={clockWiseRotation}
                projRotation={-90}
                detectorArray={detectorArray}
                detectorConfig={detectorConfig}
              />
            )}
            <VisSynchroniser
              ref={tilingPlotModel.sideRef}
              onVisViewpointChange={tilingPlotModel.handleFrameSide}
            />
            <VisViewpointRestore
              viewpoint={sideViewpoint}
              setViewpoint={setSideViewpoint}
            />
          </TilingVis>
        )}
        {config.colorbarVisible && (
          <ColorBar
            domain={config.scaleDomain}
            scaleType={config.scaleType}
            colorMap={config.colorMap}
            invertColorMap={config.invertColorMap}
            withBounds
          />
        )}
      </div>
    </WithToolbar>
  );
}

export type { Props as TomoTilingProps };
export default TomoTiling;
