import type { Vector3 } from 'three';
import { forwardRef, useState, useRef, useEffect } from 'react';
import type { TomoConfigHardware } from 'connect/tomo/utils';
import { Annotation } from '@h5web/lib';
import type { MotorSchema } from '@esrf/daiquiri-lib';
import {
  Container,
  Row,
  Col,
  Popover,
  Badge,
  OverlayTrigger,
  Button,
  ButtonGroup,
} from 'react-bootstrap';
import { toMotorPos, posInMM } from '../utils/geometry';
import debug from 'debug';
import { useCoordState } from 'components/h5web/h5webhelper';

const logger = debug('daiquiri.components.tomo.tiling.MotorMarker');

interface Props {
  projRotation: number;
  tomoHardware: TomoConfigHardware;
  coords: [number | null, number | null, number | null];
  variant?: string;
  onDelete?: (name: string) => void;
  onMove?: (name: string) => void;
  onCopyToClipboard?: (name: string) => void;
}

const UpdatingPopover = forwardRef(
  (
    props: {
      popper?: any;
      children: any;
      id: string;
      show: boolean;
      displayed: boolean;
      displayedCoords: Vector3 | null;
    },
    ref: any
  ) => {
    const {
      popper,
      children,
      show,
      displayed,
      displayedCoords,
      ...otherProps
    } = props;

    useEffect(() => {
      if (popper) {
        popper.scheduleUpdate();
      }
    }, [children, popper]);

    useEffect(() => {
      if (popper) {
        popper.scheduleUpdate();
      }
    }, [displayedCoords, popper]);

    return (
      <Popover ref={ref} body {...otherProps}>
        {children}
      </Popover>
    );
  }
);

function MotorRow(props: {
  motor: MotorSchema | null;
  position: number;
  defaultName: string;
}) {
  const digits = 2;
  const { motor } = props;
  const [motorPos, unit] = toMotorPos(motor, props.position);
  const name = motor?.alias ?? motor?.name ?? props.defaultName;
  return (
    <Row className="flex-nowrap text-nowrap" xs={12}>
      <Col xs={3}>{name}</Col>
      <Col className="text-end" xs={6}>
        {motorPos.toFixed(digits)}
      </Col>
      <Col className="text-start" xs={3}>
        <span className="text-nowrap">{unit}</span>
      </Col>
    </Row>
  );
}

function MotorMarker(props: Props) {
  const { tomoHardware, projRotation, variant = 'primary' } = props;
  const [show, setShow] = useState(false);
  const target = useRef(null);

  function getProjectedCoord(): [number | null, number | null] {
    if (projRotation === 0) {
      const [_, y, z] = props.coords;
      return [y, z];
    } else if (projRotation === -90) {
      const [x, _, z] = props.coords;
      return [x, z];
    }
    console.error(`Unimplemented projRotation ${projRotation}`);
    return [null, null];
  }

  const [px, py] = getProjectedCoord();
  const coordState = useCoordState(px, py);

  if (px === null || py === null) {
    logger('Skipped: projected pixel size is null.');
    return <></>;
  }

  const [x, y, z] = props.coords;

  const sy = tomoHardware.sy ? posInMM(tomoHardware.sy) : 0;
  const { sampleStage } = tomoHardware;
  const detectorCenter = sampleStage?.properties.detector_center ?? [0, 0];
  const detectorCenterY = detectorCenter[0] ?? 0;
  const detectorCenterZ = detectorCenter[1] ?? 0;

  const content = (
    <>
      <div className="text-end mb-2">
        <Button
          size="sm"
          variant="light"
          onClick={() => {
            if (props.onDelete) {
              props.onDelete(variant);
            }
          }}
          title="Remove the marker"
        >
          <i className="fa fa-fw fa-trash fa-lg" />
        </Button>
      </div>
      <Container>
        {x !== null && (
          <MotorRow
            motor={tomoHardware.sampu}
            position={x - sy + detectorCenterY}
            defaultName="sample-u-axis"
          />
        )}
        {y !== null && (
          <MotorRow
            motor={tomoHardware.sampv}
            position={y - sy + detectorCenterY}
            defaultName="sample-v-axis"
          />
        )}
        {z !== null && (
          <MotorRow
            motor={tomoHardware.sz}
            position={z + detectorCenterZ}
            defaultName="z-axis"
          />
        )}
        {(props.onDelete || props.onMove) && (
          <Row className="g-0 mt-2 justify-content-center">
            <ButtonGroup>
              <Button
                size="sm"
                variant="danger"
                title="Move the sample stage at this location"
                onClick={() => {
                  if (props.onMove) {
                    props.onMove(variant);
                  }
                }}
              >
                Move motors
              </Button>
              <Button
                size="sm"
                variant="secondary"
                title="Copy bliss command to clipboard"
                onClick={() => {
                  if (props.onCopyToClipboard) {
                    props.onCopyToClipboard(variant);
                  }
                }}
              >
                <i className="fa fa-fw fa-clipboard" />
              </Button>
            </ButtonGroup>
          </Row>
        )}
      </Container>
    </>
  );
  return (
    <>
      <Annotation x={px} y={py}>
        <OverlayTrigger
          trigger="click"
          rootClose
          overlay={
            <UpdatingPopover
              id="popover-contained"
              show={show && !coordState?.isVisible}
              displayed={show}
              displayedCoords={coordState?.displayedCoords ?? null}
            >
              {content}
            </UpdatingPopover>
          }
        >
          <Badge
            bg="none"
            onClick={() => setShow(!show)}
            style={{
              pointerEvents: 'auto',
              top: '50%',
              left: '50%',
              position: 'relative',
              transform: 'translate(-100%, -100%)',
            }}
          >
            <i
              ref={target}
              style={{ cursor: 'pointer' }}
              className={`fa fa-map-marker fa-3x text-${variant}`}
            />
          </Badge>
        </OverlayTrigger>
      </Annotation>
    </>
  );
}

export default MotorMarker;
