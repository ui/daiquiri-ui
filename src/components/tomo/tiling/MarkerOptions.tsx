import { DropdownButton, ButtonGroup, Button } from 'react-bootstrap';

interface Props {
  as?: any;
  value: string;
  onSelect: (value: string) => void;
  variant?: string;
}

export function MarkerSelectionButton(props: Props) {
  const { as = undefined, onSelect, value, variant } = props;

  const availableSelections = [
    'primary',
    'success',
    'info',
    'warning',
    'danger',
    'dark',
  ];

  return (
    <ButtonGroup as={as} id="bg-vertical-dropdown-3" variant={variant}>
      {availableSelections.map((data, index) => {
        const isSelected: boolean = value !== null && data === value;
        return (
          <Button
            key={index}
            variant={isSelected ? 'primary' : 'secondary'}
            onClick={() => {
              onSelect(data);
            }}
          >
            <i
              className={`fa fa-map-marker fa-lg text-${
                isSelected && data === 'primary' ? 'secondary' : data
              }`}
            />
          </Button>
        );
      })}
    </ButtonGroup>
  );
}

export default function MarkerOptions(props: Props) {
  const { as = undefined, onSelect, value, variant } = props;

  // Make sure the marker will be readable (most of the time)
  const displayedVariant = value !== variant ? value : 'light';
  const label = <i className={`fa fa-map-marker text-${displayedVariant}`} />;

  function onEventKeySelect(eventKey: string | null) {
    if (eventKey === null) {
      return;
    }
    onSelect(eventKey);
  }

  const availableSelections = [
    'primary',
    'secondary',
    'success',
    'info',
    'warning',
    'danger',
  ];

  return (
    <DropdownButton
      as={as}
      title={label}
      id="bg-vertical-dropdown-3"
      onSelect={onEventKeySelect}
      variant={variant}
    >
      <MarkerSelectionButton {...props} />
    </DropdownButton>
  );
}
