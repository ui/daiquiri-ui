import { Vector3 } from 'three';
import { create } from 'zustand';
import { createJSONStorage, persist } from 'zustand/middleware';
import type { VisViewpoint } from '../../h5web/models';

interface State {
  frontViewpoint?: VisViewpoint;
  setFrontViewpoint: (viewpoint: VisViewpoint) => void;

  sideViewpoint?: VisViewpoint;
  setSideViewpoint: (viewpoint: VisViewpoint) => void;
}

interface StoredState {
  x?: number;
  y?: number;
  z?: number;
  scale?: number;

  setFrontViewpoint: (viewpoint: VisViewpoint) => void;

  setSideViewpoint: (viewpoint: VisViewpoint) => void;
}

const useStore = create<StoredState>()(
  persist(
    (set) => ({
      setFrontViewpoint: (viewpoint) => {
        const { center, scale } = viewpoint;
        set({
          y: center.x,
          z: center.y,
          scale: scale.x,
        });
      },
      setSideViewpoint: (viewpoint) => {
        const { center, scale } = viewpoint;
        set({
          x: center.x,
          z: center.y,
          scale: scale.x,
        });
      },
    }),
    {
      name: 'tomotiling-viewpoint',
      storage: createJSONStorage(() => sessionStorage),
    }
  )
);

export function useTilingViewpoint(): State {
  const { x, y, z, scale, setFrontViewpoint, setSideViewpoint } = useStore();

  let frontViewpoint;
  if (Number.isFinite(y) && Number.isFinite(z) && Number.isFinite(scale)) {
    frontViewpoint = {
      center: new Vector3(y, z, 0),
      scale: new Vector3(scale, scale, 1),
    };
  }
  let sideViewpoint;
  if (Number.isFinite(x) && Number.isFinite(z) && Number.isFinite(scale)) {
    sideViewpoint = {
      center: new Vector3(x, z, 0),
      scale: new Vector3(scale, scale, 1),
    };
  }
  return { frontViewpoint, setFrontViewpoint, sideViewpoint, setSideViewpoint };
}
