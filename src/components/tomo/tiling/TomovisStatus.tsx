import type { AxiosInstance } from 'axios';
import StackedNameState from 'components/common/StackedNameState';
import type { TomovisService } from '../../../services/TomovisService';

export default function TomovisStatus(props: {
  className?: string;
  tomovisRestService: AxiosInstance | undefined;
  tomovisService?: TomovisService;
}) {
  const { tomovisRestService, tomovisService } = props;

  function getData(): [string, string, string] {
    if (!tomovisRestService) {
      return [
        'secondary',
        'UNDECLARED',
        'Tomovis service not declared at the Daiquiri server config. No tiling can be retrieved',
      ];
    }
    const baseURL = tomovisRestService.defaults.baseURL;

    if (!tomovisService) {
      return ['danger', 'FAILED', `Tomovis service can be setup at ${baseURL}`];
    }
    const variant = {
      READY: 'success',
      FAILED: 'danger',
      UNKNOWN: 'secondary',
      PROCESSING: 'warning',
    }[tomovisService.state];

    return [
      variant,
      tomovisService.state,
      `Tomovis service is reachable at ${baseURL}`,
    ];
  }

  const [variant, label, description] = getData();

  return (
    <StackedNameState
      className={props.className}
      name="Tomovis"
      state={label}
      stateVariant={variant}
      description={description}
    />
  );
}
