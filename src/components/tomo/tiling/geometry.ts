import type { TomoConfigHardware } from 'connect/tomo/utils';
import type { TomoDetectorSchema } from 'components/hardware/tomodetector';
import { Vector3 } from 'three';
import { Box2, Vector2 } from 'three';
import type { LimaSchema } from '../../hardware/lima';
import { posInMM, posInMMElseNull, rangeInMM } from '../utils/geometry';

/**
 * Returns the field of view of a detector in millimeter.
 *
 * If the detector is null, or if some propertes like the size or the pixel size
 * are not defined, null is returned.
 *
 * @returns A tuple containing the width and the height, else null.
 */
export function fovInMM(
  tomoDetector: TomoDetectorSchema | null,
  detector: LimaSchema | null
): [number, number] | null {
  if (tomoDetector === null || detector === null) {
    return null;
  }
  const sample_pixel_size = tomoDetector.properties.sample_pixel_size;
  const size = detector.properties.size;
  if (size === null || sample_pixel_size === null) {
    return null;
  }
  const px = sample_pixel_size / 1000;
  return [size[0] * px, size[1] * px];
}

/**
 * Compute the area of the sample stage that can be view in the detector
 *
 * @param tomoHardware Set of hardware used by the tomo
 * @param projRotation Angle in degrees
 * @returns Bounding box in sample coordinates
 */
export function getSampleStageBounds(
  tomoHardware: TomoConfigHardware,
  projRotation: number
): { motorBox: Box2; visibleBox: Box2; maxBox: Box2 } {
  const { sy, sz, sampu, sampv, tomoDetector, detector } = tomoHardware;

  if (sz === null) {
    return {
      motorBox: new Box2(),
      visibleBox: new Box2(),
      maxBox: new Box2(),
    }; // empty bounds
  }

  const radProj = (projRotation * Math.PI) / 180;
  const syPos = sy ? posInMM(sy) : 0;
  const [minSy, maxSy] = rangeInMM(sy);
  const [minU, maxU] = rangeInMM(sampu);
  const [minV, maxV] = rangeInMM(sampv);
  const [minZ, maxZ] = rangeInMM(sz);
  const minX = -Math.sin(radProj) * minU + Math.cos(radProj) * minV + syPos;
  const maxX = -Math.sin(radProj) * maxU + Math.cos(radProj) * maxV + syPos;

  const fov = fovInMM(tomoDetector, detector);
  const [hfovh, hfovv] = fov || [0, 0];

  const { sampleStage } = tomoHardware;
  const detectorCenter = sampleStage?.properties.detector_center ?? [0, 0];
  const detectorCenterY = detectorCenter[0] ?? 0;
  const detectorCenterZ = detectorCenter[1] ?? 0;

  const motorBox = new Box2().setFromPoints([
    new Vector2(minX - detectorCenterY, minZ - detectorCenterZ),
    new Vector2(maxX - detectorCenterY, maxZ - detectorCenterZ),
  ]);
  const visibleBox = motorBox
    .clone()
    .expandByVector(new Vector2(hfovh * 0.5, hfovv * 0.5));

  const maxBox = new Box2()
    .setFromPoints([
      new Vector2(
        minU + minV + minSy - detectorCenterY,
        minZ - detectorCenterZ
      ),
      new Vector2(
        maxU + maxV + maxSy - detectorCenterY,
        maxZ - detectorCenterZ
      ),
    ])
    .expandByVector(new Vector2(hfovh * 0.5, hfovv * 0.5));

  return { motorBox, visibleBox, maxBox };
}

export function getDetectorCenter(
  tomoHardware: TomoConfigHardware
): Vector3 | undefined {
  const { sz, sampu, sampv } = tomoHardware;

  if (sz === null) {
    return undefined;
  }

  // If no translation over the rotation, there is no u/v location
  const sampuPos = posInMMElseNull(sampu) ?? 0;
  const sampvPos = posInMMElseNull(sampv) ?? 0;
  const szPos = posInMM(sz);

  const { sampleStage } = tomoHardware;
  const detectorCenter = sampleStage?.properties.detector_center ?? [0, 0];
  const detectorCenterY = detectorCenter[0] ?? 0;
  const detectorCenterZ = detectorCenter[1] ?? 0;

  return new Vector3(
    sampuPos - detectorCenterY,
    sampvPos - detectorCenterY,
    szPos - detectorCenterZ
  );
}
