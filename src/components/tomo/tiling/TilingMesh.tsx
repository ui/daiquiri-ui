import type { TilesApi } from '@h5web/lib';
import { TiledTooltipMesh } from '@h5web/lib';
import { TiledHeatmapMesh } from '@h5web/lib';
import type { ReactElement } from 'react';
import type { Box2 } from 'three';
import { Vector2 } from 'three';

import type { ColorMapProps } from './models';

interface Props extends ColorMapProps {
  api: TilesApi | undefined;
  box: Box2;
  renderTooltip?: (x: number, y: number, v: number) => ReactElement | undefined;
}

/**
 * A TiledHeatmapMesh component that is positioned with a Box2
 */
function TilingMesh(props: Props) {
  const { api, box, renderTooltip, ...colorMapProps } = props;

  if (!api || box.isEmpty()) {
    return null;
  }

  const size = box.getSize(new Vector2());
  const center = box.getCenter(new Vector2());

  return (
    <group position={[center.x, center.y, 0]}>
      <TiledHeatmapMesh
        api={api}
        size={{ width: size.x, height: size.y }}
        {...colorMapProps}
      />
      {renderTooltip && (
        <TiledTooltipMesh size={size} renderTooltip={renderTooltip} />
      )}
    </group>
  );
}

export type { Props as TilingMeshProps };
export default TilingMesh;
