import type { Domain } from '@h5web/lib';
import type { Box2 } from 'three';
import { Vector2 } from 'three';

export function expandBox2ByFactor(box: Box2, factor: number): Box2 {
  return box.expandByVector(
    box.getSize(new Vector2()).multiplyScalar(factor * 0.5)
  );
}

export function toBoxExtent(box: Box2): { x: Domain; y: Domain } {
  return { x: [box.min.x, box.max.x], y: [box.min.y, box.max.y] };
}
