import { useCallback } from 'react';
import { map } from 'lodash';
import type { TomoConfigHardware } from 'connect/tomo/utils';
import MotorMarker from './MotorMarker';
import type { CanvasEvent } from '@h5web/lib';
import type { ComputeSampleStageAtPlotLocation } from './types';
import PointerClick from '../../h5web/SelectionPoint';
import useUserPreference from '../../utils/UseUserPreference';
import type { Vector3 } from 'three';
import { requestMoveSampleStage } from '../../../services/TomoService';
import type { MotorSchema } from '@esrf/daiquiri-lib';
import type Qty from 'js-quantities';

type MarkerType = [number | null, number | null, number | null];

export type MarkersType = Record<string, MarkerType>;

export type SetMarkerAction = (
  name: string,
  x: number | null,
  y: number | null,
  z: number | null
) => void;

export function useMarkers(props: {
  getCenter: () => Vector3 | undefined;
}): [MarkersType, SetMarkerAction] {
  const { getCenter } = props;
  const [markers, setMarkers] = useUserPreference<MarkersType>(
    'tomo/tiling/markers',
    {}
  );
  const setMarker = useCallback(
    (name: string, x: number | null, y: number | null, z: number | null) => {
      const defc = getCenter() ?? { x: null, y: null, z: null };
      const m = markers[name] ?? [null, null, null];
      const d: MarkersType = {
        ...markers,
      };
      const isDelete = x === null && y === null && z === null;
      // FIXME: It would be better to expose a dedicated API to delete a marker
      d[name] = isDelete
        ? [null, null, null]
        : [x ?? m[0] ?? defc.x, y ?? m[1] ?? defc.y, z ?? m[2] ?? defc.z];
      setMarkers(d);
    },
    [setMarkers, markers]
  );
  return [markers, setMarker];
}

export function MarkersItem(props: {
  projRotation: number;
  markers: MarkersType;
  setMarker: SetMarkerAction;
  tomoHardware: TomoConfigHardware;
  computeSampleStageAtPlotLocation: ComputeSampleStageAtPlotLocation;
}) {
  return (
    <>
      {map(props.markers, (m, key) => (
        <MotorMarker
          key={key}
          projRotation={props.projRotation}
          tomoHardware={props.tomoHardware}
          variant={key}
          coords={m}
          onDelete={(name: string) => {
            props.setMarker(name, null, null, null);
          }}
          onMove={(name: string) => {
            const p = props.markers[name];
            const motion = props.computeSampleStageAtPlotLocation(
              p[0],
              p[1],
              p[2]
            );
            requestMoveSampleStage(motion);
          }}
          onCopyToClipboard={(name: string) => {
            const p = props.markers[name];
            const motion = props.computeSampleStageAtPlotLocation(
              p[0],
              p[1],
              p[2]
            );
            function pushMotorPosition(
              params: string[],
              motor: MotorSchema | null,
              position: Qty | undefined
            ) {
              if (position === undefined || motor === null) {
                return;
              }
              const scalar = position.to(motor.properties.unit).scalar;
              params.push(
                `${motor.alias ?? motor.name}`,
                `${scalar.toFixed(4)}`
              );
            }

            const params: string[] = [];
            pushMotorPosition(params, props.tomoHardware.sz, motion.sz);
            pushMotorPosition(params, props.tomoHardware.sampu, motion.sampu);
            pushMotorPosition(params, props.tomoHardware.sampv, motion.sampv);
            navigator.clipboard.writeText(`umv(${params.join(', ')})`);
          }}
        />
      ))}
    </>
  );
}

export function MarkerInteraction(props: {
  disabled: boolean;
  projRotation: number;
  selectedMarker: string;
  setMarker: SetMarkerAction;
}) {
  const { disabled, projRotation, selectedMarker, setMarker } = props;

  const onClick = useCallback(
    (evt: CanvasEvent<MouseEvent>) => {
      if (disabled) {
        console.error(`Unexpected click action`);
        return;
      }
      if (projRotation === 0) {
        setMarker(selectedMarker, null, evt.dataPt.x, evt.dataPt.y);
      } else if (projRotation === -90) {
        setMarker(selectedMarker, evt.dataPt.x, null, evt.dataPt.y);
      } else {
        console.error(`Unexpected projRotation ${projRotation}`);
      }
    },
    [disabled, projRotation, setMarker, selectedMarker]
  );

  return <>{!disabled && <PointerClick onClick={onClick} />}</>;
}
