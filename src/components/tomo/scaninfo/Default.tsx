import { useMemo, useState } from 'react';
import { Badge, ProgressBar } from 'react-bootstrap';
import { map } from 'lodash';
import { Panel } from '@esrf/daiquiri-lib';
import type * as TomoTypes from 'types/Tomo';
import debug from 'debug';
import { useSelector } from 'react-redux';
import type { ReduxState } from '../../../types/ReduxStore';
import type { DataCollection } from '../../../types/DataCollection';
import { humanReadableDuration, parseDDMMYYYY } from '../../scans/utils';
import { useIntervalEffect } from '@react-hookz/web';
import { useLastScanGroup } from '../utils/hooks';
import { getLabelFromScanState } from '../utils/TomoScans';

const logger = debug('daiquiri.components.tomo.TomoScanInfo');

function getLabelDescription(type: string): [string, string] {
  if (type === 'tomo:dark') {
    return ['Dark', 'Acquire dark projection'];
  }
  if (type === 'tomo:flat') {
    return ['Flat', 'Acquire flat projection'];
  }
  if (type === 'tomo:return_ref') {
    return [
      'Return',
      'Return to origin and acquiring few projection as reference',
    ];
  }
  if (type === 'tomo:step') {
    return ['Step scan', 'Tomo scan using rotation as step motor'];
  }
  if (type === 'tomo:continuous') {
    return ['Continuous scan', 'Tomo scan using rotation as continuous motor'];
  }
  return [type, 'No description'];
}

function TomoProgressSequence() {
  const lastgroup = useLastScanGroup();
  if (!lastgroup) {
    logger('Skipped: lastgroup missing.');
    return <></>;
  }
  const { subscans } = lastgroup;
  if (!subscans) {
    logger('Skipped: subscans missing.');
    return <></>;
  }
  const { activesubscan } = lastgroup;

  function indexToScanState(index: number) {
    if (!lastgroup) {
      return '';
    }

    if (index < activesubscan) {
      return 'DONE';
    }

    if (index === activesubscan) {
      // Returns the actual state of the scan group
      // If the group fail, probably this subscan have failed
      return lastgroup.state;
    }
    return 'IDLE';
  }

  const sumCharacters = subscans.reduce((total, subscan) => {
    const [name] = getLabelDescription(subscan);
    return total + name.length;
  }, 0);

  function createSubscanProgressBar(
    index: number,
    state: string,
    type: string
  ) {
    function capitalized(str: string) {
      return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
    }
    const [normalizedState, variant] = getLabelFromScanState(state);
    const [name, description] = getLabelDescription(type);
    const animated = normalizedState === 'RUNNING';

    return (
      <ProgressBar
        animated={animated}
        variant={variant}
        label={`${capitalized(name)}`}
        title={description}
        now={name.length}
        max={sumCharacters}
        key={index}
      />
    );
  }

  return (
    <ProgressBar>
      {map(subscans, (subscan, index) => {
        const state = indexToScanState(index);
        return createSubscanProgressBar(index, state, subscan);
      })}
    </ProgressBar>
  );
}

function LiveDuration(props: { starttime: string }) {
  const [timestamp, setTimestamp] = useState(0);
  useIntervalEffect(() => {
    setTimestamp(Date.now());
  }, 5000);
  const duration = useMemo(() => {
    if (timestamp <= 0) {
      return 0;
    }
    const start = parseDDMMYYYY(props.starttime);
    const duration = (timestamp - start.getTime()) / 1000;
    if (duration < 0) {
      return 0;
    }
    return duration;
  }, [props.starttime, timestamp]);
  return (
    <>
      {humanReadableDuration(duration)}{' '}
      <i className="fa-solid fa-spinner fa-spin-pulse" />
    </>
  );
}

export default function TomoScanInfo(props: Record<string, never>) {
  const lastgroup = useLastScanGroup();

  const dataCollection = useSelector<ReduxState, DataCollection | undefined>(
    (state) => {
      if (!lastgroup) {
        return undefined;
      }
      return state.metadata.ns_datacollections.default.results[
        lastgroup.datacollectionid
      ];
    }
  );

  if (!lastgroup) {
    return (
      <Panel>
        <Panel.Contents>No scan</Panel.Contents>
      </Panel>
    );
  }

  const [state, variant] = getLabelFromScanState(lastgroup.state);

  function getDescription(scanGroup: TomoTypes.TomoScanInfo) {
    const { subscans } = scanGroup;
    if (!subscans) {
      return '';
    }
    const { activesubscan } = scanGroup;

    if (activesubscan < 0) {
      return 'Not yet started';
    }

    if (
      activesubscan + 1 === subscans.length &&
      scanGroup.state !== 'STARTING'
    ) {
      return 'Done';
    }

    const activetype = subscans[activesubscan];
    const [, description] = getLabelDescription(activetype);
    return `${activesubscan + 1}/${subscans.length}: ${description}`;
  }

  const { starttime, endtime, duration } = dataCollection ?? {};
  const description = getDescription(lastgroup);

  return (
    <Panel>
      <Panel.Contents>
        <ul>
          {lastgroup.datacollectionid && (
            <li>
              <b>Data collection</b>: {lastgroup.datacollectionid}
            </li>
          )}
          <li>
            <b>Scan</b>: {lastgroup.scanid}
          </li>
          <li>
            <b>Start</b>:{' '}
            {starttime ? parseDDMMYYYY(starttime).toLocaleTimeString() : ''}
          </li>
          <li>
            <b>Status</b>:{' '}
            <Badge bg={variant} title={`BLISS state: ${lastgroup.state}`}>
              {state}
            </Badge>
          </li>
          <li>
            <b>Progress</b>: <TomoProgressSequence />
          </li>
          <li>
            <b>Description</b>: {description}
          </li>
          {endtime && (
            <li>
              <b>End</b>: {parseDDMMYYYY(endtime).toLocaleTimeString()}
            </li>
          )}
          {starttime && (
            <li>
              <b>Duration</b>:{' '}
              {duration ? (
                humanReadableDuration(duration)
              ) : (
                <LiveDuration starttime={starttime} />
              )}
            </li>
          )}
        </ul>
      </Panel.Contents>
    </Panel>
  );
}
