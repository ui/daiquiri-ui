import { createRef, Component } from 'react';
import PropTypes from 'prop-types';
import {
  cloneDeep,
  forEach,
  each,
  debounce,
  mapValues,
  map,
  filter,
  values,
  reduce,
  isEqual,
  isFunction
} from 'lodash';

import { Alert, Button, ListGroup } from 'react-bootstrap';

import RestService from 'services/RestService';
import FormWithCache from 'components/form-widgets/FormWithCache';
import FormPresets from 'components/form-widgets/FormPresets';
import OptionalParams from 'components/form-widgets/OptionalParams';
import ArrayTableField from 'components/form-widgets/ArrayTableField';
import CustomCheckbox from 'components/form-widgets/CustomCheckbox';
import ConnectedSelect from 'components/form-widgets/ConnectedSelect';
import SampleId from 'components/form-widgets/SampleId';
import DatasetName from 'components/form-widgets/DatasetName';
import Plot0d from 'components/form-widgets/Plot0d';
import {
  TemplatedFieldTemplate,
  TemplatedObjectFieldTemplate,
  TemplatedArrayFieldTemplate
} from 'components/form-widgets/CustomTemplates';

import {
  ObjectFieldTemplate,
  GroupTemplates
} from 'components/form-widgets/GroupedFields';

// https://github.com/fuhrysteve/marshmallow-jsonschema/issues/40
// https://github.com/mozilla-services/react-jsonschema-form/issues/961
// https://github.com/epoberezkin/ajv#formats
// Sigh :(
const SUPPORTED_SCHEMA_FORMATS = [
  'data-url',
  'date',
  'date-time',
  'email',
  'hostname',
  'ipv4',
  'ipv6',
  'uri'
];

const stripSchemaFormats = (schema, whitelist) => {
  const newSchema = cloneDeep(schema);

  const strip = schema => {
    if (schema.format && whitelist.indexOf(schema.format) === -1) {
      delete schema.format;
    }

    // urgh :(
    if (schema.exclusiveMinimum) delete schema.exclusiveMinimum;
    if (schema.exclusiveMaximum) delete schema.exclusiveMaximum;

    if (schema.properties) {
      forEach(schema.properties, subSchema => {
        strip(subSchema);
      });
    }
  };

  strip(newSchema);
  return newSchema;
};

export const stripWithWhitelist = schema =>
  stripSchemaFormats(schema, SUPPORTED_SCHEMA_FORMATS);

function derefData(additionalFormData) {
  return isFunction(additionalFormData)
    ? additionalFormData()
    : additionalFormData;
}

function ErrorListTemplate(props) {
  const hasErrors =
    filter(values(props.errors), v => v.stack.includes('schema')).length > 0;
  return (
    <>
      {hasErrors && (
        <div className="errors mt-1">
          <ListGroup>
            {props.errors.map(error => {
              if (error.stack.startsWith('schema')) {
                return (
                  <ListGroup.Item key={error.stack} className="text-danger">
                    <i className="fa fa-exclamation-circle" />{' '}
                    {error.stack.replace('schema: ', '')}
                  </ListGroup.Item>
                );
              }
              return <></>;
            })}
          </ListGroup>
        </div>
      )}
    </>
  );
}

class WarningListTemplate extends Component {
  static propTypes = {
    warnings: PropTypes.object
  };

  render() {
    const hasWarnings = Object.keys(this.props.warnings).length > 0;
    return (
      <>
        {hasWarnings && (
          <div className="warnings">
            <ListGroup>
              {map(this.props.warnings, (warning, key) => (
                <ListGroup.Item key={key} className="text-warning">
                  <i className="fa fa-warning" /> {warning}
                </ListGroup.Item>
              ))}
            </ListGroup>
          </div>
        )}
      </>
    );
  }
}

export default class SchemaForm extends Component {
  static propTypes = {
    actions: PropTypes.shape({
      alert: PropTypes.func.isRequired,
      savePreset: PropTypes.func.isRequired
    }),
    schema: PropTypes.string.isRequired,
    schemas: PropTypes.object.isRequired,
    url: PropTypes.string,
    additionalFormData: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
    onAsyncValidate: PropTypes.func,
    button: PropTypes.func,
    requestCloseModal: PropTypes.func, // unused?
    requireOperator: PropTypes.bool,
    submitText: PropTypes.string,
    allowSubmitOnEnter: PropTypes.bool
  };

  constructor(props) {
    super(props);

    this.submitButton = createRef();

    this.state = {
      asyncErrors: {},
      asyncWarnings: {},
      submitting: false,
      fetched: false,
      reset: false,
      formData: {
        ...this.props.formData
      }
    };

    this.updateFetched = debounce(this.setFetched.bind(this), 300);
    this.asyncValidate = debounce(this.asyncValidate.bind(this), 200);
    this.setFormStore = debounce(this.setFormStore.bind(this), 200);
    this.setAsyncTimeout = null;
  }

  setFetched(fetched) {
    this.setState({ fetched });
  }

  componentDidMount() {
    this.props.actions.fetch();
    this.setRemoteButton();
  }

  componentDidUpdate(lastProps, lastState) {
    const lastDisabled =
      (!lastProps.operator && lastProps.requireOperator) ||
      lastProps.runningScan ||
      lastState.submitting;

    if (lastDisabled !== this.disabled()) {
      this.setRemoteButton();
    }

    if (
      (this.props.fetched != lastProps.fetched ||
        this.props.fetching != lastProps.fetching) &&
      this.props.fetched &&
      !this.props.fetching
    ) {
      this.updateFetched(true);
    }
  }

  setRemoteButton() {
    if (this.props.button) {
      this.props.button(this.disabled(), this.externalSubmit);
    }
  }

  disabled() {
    return (
      (!this.props.operator && this.props.requireOperator) ||
      this.props.runningScan ||
      this.state.submitting
    );
  }

  preventSubmit = event => {
    if (this.props.allowSubmitOnEnter) return;
    if (event.keyCode === 13 && event.target.tagName !== 'TEXTAREA') {
      event.preventDefault();
    }
  };

  onError = e => {
    console.log('onError', e);
  };

  onSubmit = ({ formData }, e) => {
    if (Object.keys(this.state.asyncErrors).length) return;

    this.setState({ submitting: true });
    if (this.props.onSubmit) {
      this.props
        .onSubmit(formData, e, this.doSubmit.bind(this))
        .then(() => this.setState({ submitting: false }));
      return;
    }

    this.doSubmit(formData).then(() => this.setState({ submitting: false }));
  };

  doSubmit(formData) {
    const rt = this.props.schemas[this.props.schema];
    const url = this.props.url || rt.url;

    const method = this.props.method || rt.method;

    return RestService[method](url, formData)
      .then(response => {
        this.setState({ reset: true });
      })
      .catch(error => {
        console.log(error);
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.error) ||
          error.message;
        this.props.actions.alert({
          message: `Could not submit form: ${message || error}`,
          type: 'danger'
        });
      });
  }

  asyncValidate(formData) {
    this.props.actions
      .validate({
        name: this.props.schema,
        data: {
          ...formData,
          ...derefData(this.props.additionalFormData)
        }
      })
      .then(resp => {
        const { warnings, errors, calculated, ...rest } = resp.data;
        const newFormData = {
          ...this.state.formData,
          ...calculated
        };
        this.setState(state => ({
          formData: newFormData,
          asyncWarnings: warnings,
          asyncErrors: mapValues(errors, v => ({ __errors: v })),
          additionalAsync: rest
        }));

        this.setFormStore(newFormData);

        if (this.props.onAsyncValidate) {
          this.props.onAsyncValidate({
            formData: newFormData,
            additionalAsync: rest
          });
        }
      });
  }

  setFormStore(formData) {
    this.props.actions.setForm(formData);
  }

  diffFormData(newFormData) {
    const diffs = reduce(
      this.state.formData,
      (result, value, key) => {
        return isEqual(value, newFormData[key]) ? result : result.concat(key);
      },
      []
    );
    return Object.fromEntries(diffs.map(field => [field, newFormData[field]]));
  }

  onChange = form => {
    const formData = JSON.parse(JSON.stringify(form.formData));
    const diff = this.diffFormData(formData);
    const keys = Object.keys(diff);

    // we have emptied a single field, dont revalidate immediately
    // -> debounce by 2.5s, this allows user to enter a value or reset if left
    if (keys.length === 1 && diff[keys[0]] === undefined) {
      this.setAndAsyncValidate(formData, 2500);
    } else {
      this.setAndAsyncValidate(formData);
    }
  };

  setAndAsyncValidate(formData, wait = null) {
    clearTimeout(this.setAsyncTimeout);
    this.setState({ formData }, () => {
      if (this.props.schemas[this.props.schema].asyncValidate) {
        if (wait) {
          this.setAsyncTimeout = setTimeout(() => {
            this.asyncValidate(formData);
          }, wait);
        } else {
          this.asyncValidate(formData);
        }
      }
    });
    this.setFormStore(formData);
  }

  externalSubmit = (enqueue = null) => {
    const updatedEnqueue = previousState => {
      return { formData: { ...previousState.formData, enqueue } };
    };

    if (enqueue !== null) {
      this.setState(updatedEnqueue, () => {
        this.submitButton.current.click();
      });
    } else {
      this.submitButton.current.click();
    }
  };

  render() {
    const rt = this.props.schemas[this.props.schema];
    if (!rt) {
      return (
        <div>
          Could not load schema
          {this.props.schema}
        </div>
      );
    }

    if (rt.exception) {
      return (
        <Alert variant="danger">
          <Alert.Heading>Error loading schema</Alert.Heading>
          <p>{rt.exception}</p>
          <hr />
          <pre>{rt.traceback}</pre>
        </Alert>
      );
    }

    const sch = {
      definitions: {},
      $ref: rt.$ref,
      name: this.props.schema,
      cache: rt.cache,
      presets: rt.presets,
      save_presets: rt.save_presets
    };
    each(rt.definitions, (d, k) => {
      sch.definitions[k] = stripSchemaFormats(d, SUPPORTED_SCHEMA_FORMATS);
    });

    const uiSchema = {
      'ui:order': rt.uiorder,
      'ui:groups': rt.uigroups,
      'ui:template': ObjectFieldTemplate,
      ...rt.uischema,
      ...this.props.uiSchema
    };

    const fields = {
      optionalParams: OptionalParams,
      arrayTable: ArrayTableField,
      plot0d: Plot0d
    };

    const widgets = {
      CheckboxWidget: CustomCheckbox,
      connectedSelect: ConnectedSelect,
      SampleId,
      DatasetName
    };

    const submitText = this.props.submitText || 'Submit';
    return (
      <div
        className={`schema-form ${this.state.fetched ? 'fetched' : ''}`}
        onKeyDown={this.preventSubmit}
      >
        {this.props.fetching && (
          <p>
            <i className="fa fa-spinner fa-pulse" /> Loading...
          </p>
        )}

        {this.state.fetched && (
          <>
            <WarningListTemplate warnings={this.state.asyncWarnings} />
            <FormPresets
              onChange={this.onChange}
              presets={sch.presets}
              savePresets={sch.save_presets}
              autoLoadPreset={sch.auto_load_preset}
              savePreset={this.props.actions.savePreset}
              schema={this.props.schema}
              formData={this.state.formData}
              fetchSchema={this.props.actions.fetch}
            />
            <FormWithCache
              schema={cloneDeep(sch)}
              FieldTemplate={TemplatedFieldTemplate}
              ObjectFieldTemplate={TemplatedObjectFieldTemplate}
              ArrayFieldTemplate={TemplatedArrayFieldTemplate}
              ErrorList={ErrorListTemplate}
              widgets={widgets}
              uiSchema={uiSchema}
              fields={fields}
              liveValidate
              disabled={this.disabled()}
              omitExtraData
              showErrorList
              extraErrors={this.state.asyncErrors}
              onError={this.onError}
              reset={this.state.reset}
              formData={
                this.props.mirrorid ? this.props.formStore : this.state.formData
              }
              additionalFormData={this.props.additionalFormData}
              onChange={this.onChange}
              onSubmit={this.onSubmit}
              formContext={{
                templates: GroupTemplates,
                groups: rt.uigroups
              }}
            >
              <Button
                type="submit"
                disabled={this.disabled()}
                ref={this.submitButton}
                className={this.props.button ? 'hidden' : ''}
              >
                {submitText}
              </Button>
            </FormWithCache>
          </>
        )}
      </div>
    );
  }
}
