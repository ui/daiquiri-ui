import { useEffect } from 'react';
import moment from 'moment';
import { Alert, ProgressBar } from 'react-bootstrap';
import { humanReadableDuration } from './utils';
import type { ScanSchema } from 'types/Scan';

interface Props {
  running: {
    scanid: number;
    progress: number;
  };
  runningScan?: ScanSchema;
  actions: {
    fetch: () => Promise<void>;
  };
}

function ScanStatus(props: Props) {
  const { actions, runningScan, running } = props;

  useEffect(() => {
    actions.fetch().catch(() => console.log('Couldnt fetch scan status'));
  }, [actions]);

  if (!runningScan) {
    return null;
  }

  const started = moment.unix(runningScan.start_timestamp ?? 0);
  const duration = moment.duration(moment().diff(started)).humanize();

  const estimate = humanReadableDuration(runningScan.estimated_time);

  const { scanid, progress } = running;
  return (
    <Alert variant="info">
      {`Scan ${scanid} running, started at ${started.format(
        'HH:mm:ss'
      )}, running
      for ${duration}`}
      {estimate && ` (estimated ${estimate})`}
      <ProgressBar variant="info" animated now={progress} />
    </Alert>
  );
}

export default ScanStatus;
