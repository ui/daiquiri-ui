import { useEffect } from 'react';
import { FullSizer } from '@esrf/daiquiri-lib';

import momentCell from 'components/table/cells/momentCell';
import RemountOnResize from 'components/utils/RemountOnResize';

import { Button, Badge, OverlayTrigger, Popover } from 'react-bootstrap';
import SelectableTable from 'components/table/Selectable';
import type { Scan } from './models';
import type {
  PagedActions,
  PagedType,
  SelectableActions,
} from '../table/models';

const statusMap = {
  CREATED: 'light',
  PREPARING: 'info',
  RUNNING: 'info',
  FINISHED: 'success',
  ABORTED: 'warning',
  FAILED: 'danger',
};

function StatusCell(cell: keyof typeof statusMap) {
  return <Badge bg={statusMap[cell]}>{cell}</Badge>;
}

function FileNameCell(cell: string) {
  const popover = (
    <Popover id="popover-filename">
      <Popover.Header as="h3">File Name</Popover.Header>
      <Popover.Body>{cell}</Popover.Body>
    </Popover>
  );
  return cell ? (
    <OverlayTrigger
      trigger="click"
      placement="left"
      overlay={popover}
      rootClose
    >
      <Button size="sm">
        <i className="fa fa-file" />
      </Button>
    </OverlayTrigger>
  ) : null;
}

interface Actions extends PagedActions, SelectableActions {
  fetchData: (payload: unknown) => Promise<void>;
}

interface Props extends PagedType {
  actions: Actions;
  scans: Record<string, Scan>;
  selected?: number;
  selectedItems: number[];
}

function ScanTable(props: Props) {
  const { scans, actions, selected } = props;
  const tableData = Object.values(scans);

  useEffect(() => {
    actions.setPageSize(10);
    actions.fetch();
  }, [actions]);

  useEffect(() => {
    if (selected) actions.fetchData(selected);
  }, [selected]);

  const columns = [
    {
      dataField: 'scan_number',
      text: '#',
    },
    { dataField: 'title', text: 'Title', classes: 'text-break' },
    {
      dataField: 'start_timestamp',
      text: 'Start',
      formatter: momentCell,
      formatExtraData: { format: 'DD-MM-YYYY HH:mm:ss' },
      classes: 'text-nowrap',
    },
    {
      dataField: 'end_timestamp',
      text: 'End',
      formatter: momentCell,
      formatExtraData: { format: 'DD-MM-YYYY HH:mm:ss' },
      classes: 'text-nowrap',
    },
    { dataField: 'npoints', text: 'Points' },
    { dataField: 'count_time', text: 'Count Time' },
    { dataField: 'status', text: 'Status', formatter: StatusCell },
    { dataField: 'filename', text: 'File', formatter: FileNameCell },
  ];

  const pages = {
    page: props.page,
    per_page: props.per_page,
    total: props.total,
    setPage: props.actions.setPage,
    setPageSize: props.actions.setPageSize,
    fetch: props.actions.fetch,
  };

  return (
    <RemountOnResize>
      <FullSizer>
        <Button
          size="sm"
          onClick={() => actions.resetSelection?.()}
          disabled={props.selected === null}
          className="mb-1"
        >
          Follow
        </Button>
        <SelectableTable
          keyField="scanid"
          data={tableData}
          pages={pages}
          columns={columns}
          bordered={false}
          classes="table-sm scan-table"
          noDataIndication="No scans available"
          loading={props.fetching}
          overlay
          multiple
          selectedItems={props.selectedItems}
          page={props.page}
          actions={{
            addSelection: props.actions.addSelection,
            removeSelection: props.actions.removeSelection,
            resetSelection: props.actions.resetSelection,
          }}
        />
      </FullSizer>
    </RemountOnResize>
  );
}

export default ScanTable;
