import { useEffect } from 'react';
import { Row, Col, Badge } from 'react-bootstrap';
import { dynamicOp } from '@esrf/daiquiri-lib';
import type { Scan, PagedScanData } from './models';
import { findLastScanId } from './utils';

export interface ScanPlot0dValueOptions {
  /** Selected data series to display */
  series: string;
  /** Name for the value (overrides `series` name) */
  name?: string;
  /** Round the value to n digits (if float) */
  round?: number;
  /** Comparison to show value as ok */
  comparison?: string;
  /** Comparator to use */
  comparator?: string;
}

interface Props {
  scans: Record<string, Scan>;
  data: Record<string, PagedScanData>;
  actions: {
    setPageSize: (size: number) => void;
    setPage: (pageNumber?: number, fetch?: boolean) => void;
    setParams: (
      p: { scalars?: string[]; scanid?: number },
      fetch?: boolean
    ) => void;
    setFollow: () => void;
  };
  selectedScan: number;
  currentPoint: number;
  options: ScanPlot0dValueOptions;
}

export default function ScanPlot0dValue(props: Props) {
  const { options, selectedScan, currentPoint, data, scans, actions } = props;
  const { series } = options;

  const lastScanId = findLastScanId(scans);

  useEffect(() => {
    actions.setFollow();
    actions.setPage(undefined);
    actions.setParams({
      scalars: [series],
    });
    if (lastScanId) {
      actions.setParams(
        {
          scanid: lastScanId,
        },
        true
      );
    }
  }, []);

  useEffect(() => {
    let scanid = selectedScan;
    if (!scanid) {
      // Follow latest scan
      if (lastScanId) scanid = lastScanId;
    }

    if (scanid) {
      actions.setParams(
        {
          scanid,
        },
        true
      );
    }
  }, [selectedScan]);

  if (!series) {
    return (
      <div className="bg-warning rounded p-1">
        <i className="fa fa-warning m-1" />
        Please specify a `series` in the config
      </div>
    );
  }

  const name = options.name || series;

  if (lastScanId === null) {
    return <span>{name}: No scan loaded.</span>;
  }

  const scanid = selectedScan || lastScanId;
  const obj = data[scanid] || {};
  if (!obj.data) {
    return <span>{name}: Waiting for scan data</span>;
  }

  if (!obj.axes.ys.scalars.includes(series)) {
    return (
      <div className="bg-warning rounded p-1">
        Scan does not include `{series}`
      </div>
    );
  }

  const y = obj.data[series];
  if (!y) {
    return (
      <div className="bg-warning rounded p-1">No y data yet for `{series}`</div>
    );
  }

  if (!y.data) {
    return (
      <div className="bg-warning rounded p-1">
        Data not yet loaded for `{series}`
      </div>
    );
  }

  // Follow latest point if `currentPoint` is null (not undefined!)
  const point = currentPoint !== null ? currentPoint : y.data.length - 1;
  const value = y.data[point];

  let checkClass = 'primary';
  if (options.comparator && options.comparison) {
    const check = dynamicOp(options.comparator, value, options.comparison);
    checkClass = check ? 'success' : 'danger';
  }

  let valueString = String(value);
  if (options.round !== undefined) {
    if (value !== undefined && !Number.isNaN(value)) {
      valueString = value.toFixed(options.round);
    }
  }

  return (
    <div className="scanplot0d-value">
      <Row>
        <Col>{name}:</Col>
        <Col>
          <Badge bg={checkClass}>{valueString}</Badge>
          <span className="invisible">
            [{point}, {obj.page}]
          </span>
        </Col>
      </Row>
    </div>
  );
}
