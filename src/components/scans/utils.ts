import { Formatting } from '@esrf/daiquiri-lib';
import type { Scan } from './models';

export function humanReadableDuration(
  duration: number | undefined
): string | null {
  if (duration === undefined) {
    return null;
  }
  if (duration < 60) {
    return `${duration.toFixed(0)}s`;
  }
  return Formatting.toHoursMins(duration);
}

export function findLastScanId(scanRecord: Record<number, Scan>) {
  const scans = Object.values(scanRecord);
  if (scans.length === 0) {
    return null;
  }

  const sortedScans = scans.sort(
    (a, b) => b.start_timestamp - a.start_timestamp
  );

  return sortedScans[0].scanid;
}

/**
 * Parse `DD-MM-YYYY hh:mm:ss` as Date.
 */
export function parseDDMMYYYY(date: string) {
  const d = `${date.slice(6, 10)}-${date.slice(3, 5)}-${date.slice(
    0,
    2
  )}${date.slice(10)}`;
  return new Date(d);
}
