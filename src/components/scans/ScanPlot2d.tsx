import type { ChangeEvent, CSSProperties, RefObject } from 'react';
import { createRef, Component } from 'react';
import type { DebouncedFunc } from 'lodash';
import { debounce } from 'lodash';

import config from 'config/config';

import ZoomPanCanvas from 'components/utils/ZoomPanCanvas';
import RemountOnResize from 'components/utils/RemountOnResize';
import { Form, InputGroup, Alert, Container, Row, Col } from 'react-bootstrap';
import DropdownLut from 'components/h5web/bootstrap/DropdownLut';
import DropdownNormalization from 'components/h5web/bootstrap/DropdownNormalization';
import DropdownAutoscale from 'components/h5web/bootstrap/DropdownAutoscale';
import type { Scan, ScanData } from './models';
import { findLastScanId } from './utils';

interface Props {
  actions: {
    selectPoint: (point: number | string) => void;
    fetchScanSpectra: (payload: unknown) => Promise<void>;
  };
  scans: Record<number, Scan>;
  data: Record<number, ScanData>;
  currentPoint?: number;
  selectedScan?: number;
  style?: CSSProperties;
  url?: string;
}

interface State {
  loadingMessage: string;
  zoom: number;
  url: string | undefined;
  lut: string;
  autoscale: string;
  norm: string;
  error: string | undefined;
}

export default class ScanPlot2d extends Component<Props, State> {
  public max: number;
  public selectRef: RefObject<HTMLSelectElement>;
  public point: RefObject<HTMLInputElement>;
  public loadImage: DebouncedFunc<() => void>;

  public constructor(props: Props) {
    super(props);

    this.state = {
      loadingMessage: '',
      zoom: 1,
      url: undefined,
      lut: 'gray',
      autoscale: 'minmax',
      norm: 'linear',
      error: undefined,
    };

    this.loadImage = debounce(this.rawLoadImage.bind(this), 1000);
    this.max = 0;
    this.selectRef = createRef();
    this.point = createRef();
  }

  public componentDidMount() {
    this.loadImage();
  }

  public componentDidUpdate(prevProps: Props) {
    const data = this.data();
    if (!data || !data.scanid) return;
    const max = data.npoints_avail;

    if (
      prevProps.currentPoint !== this.props.currentPoint ||
      prevProps.selectedScan !== this.props.selectedScan ||
      this.max !== max
    ) {
      this.loadImage();
    }

    this.max = max;
  }

  public componentWillUnmount() {
    this.loadImage.cancel();
  }

  public onChangeNode = () => {
    this.loadImage();
    // this.props.dispatch(selectImageNode(e.target.value))
  };

  public onChangePoint = (e: ChangeEvent<HTMLInputElement>) => {
    const { value: valueAsStr } = e.target;
    const value = Number.parseInt(valueAsStr, 10);
    if (value < this.max) {
      this.props.actions.selectPoint(value);
    }
  };

  public onReady = () => {
    this.loadImage();
  };

  public onError = (status: unknown, event: unknown, message: string) => {
    this.setState({ error: message });
  };

  public onZoomChange = (zoom: number) => {
    this.setState({ zoom });
  };

  public onLoadProgress = (pc: number) => {
    if (pc === 100) this.setState({ loadingMessage: '' });
    else this.setState({ loadingMessage: `Loading ${pc}%` });
  };

  public onSelectLut = (lut: string | null) => {
    if (lut === null) {
      return;
    }
    this.setState({ lut });
    this.loadImage();
  };

  public onSelectAutoscale = (autoscale: string | null) => {
    if (autoscale === null) {
      return;
    }
    this.setState({ autoscale });
    this.loadImage();
  };

  public onSelectNorm = (norm: string | null) => {
    if (norm === null) {
      return;
    }
    this.setState({ norm });
    this.loadImage();
  };

  public getNodeOptions() {
    const data = this.data();
    if (!data) return null;
    return data.axes.ys.images.map((i) => <option key={i}>{i}</option>);
  }

  public data(): ScanData | undefined {
    const selectedScan = this.selectedScan();
    if (selectedScan === null) {
      return undefined;
    }
    return this.props.data[selectedScan];
  }

  public selectedScan() {
    return this.props.selectedScan || findLastScanId(this.props.scans);
  }

  public imageURL(
    scanid: number,
    imageNo: number,
    lut: string,
    autoscale: string,
    norm: string
  ) {
    if (!this.selectRef.current?.value) {
      return undefined;
    }

    return `${config.baseUrl}/scans/image/${scanid}?node_name=${this.selectRef.current.value}&image_no=${imageNo}&lut=${lut}&autoscale=${autoscale}&norm=${norm}`;
  }

  public rawLoadImage() {
    this.setState({ error: undefined });
    const data = this.data();
    const imageNo = this.props.currentPoint || this.max - 1;

    if (imageNo < 0) return;

    if (data?.scanid) {
      if (this.point.current) this.point.current.value = `${imageNo}`;

      this.setState((prevState) => ({
        ...prevState,
        loadingMessage: `Loading Image ${imageNo}`,
        url:
          this.props.url ||
          this.imageURL(
            data.scanid,
            imageNo,
            prevState.lut,
            prevState.autoscale,
            prevState.norm
          ),
      }));
    }
  }

  public render() {
    return (
      <div className="plot2d-container" style={this.props.style}>
        <div className="controls mb-1">
          <Container>
            <Row>
              <Col xs="auto" className="gx-1">
                <DropdownLut
                  value={this.state.lut}
                  onSelect={this.onSelectLut}
                />
              </Col>
              <Col xs="auto" className="gx-1">
                <DropdownAutoscale
                  value={this.state.autoscale}
                  onSelect={this.onSelectAutoscale}
                />
              </Col>
              <Col xs="auto" className="gx-1">
                <DropdownNormalization
                  value={this.state.norm}
                  onSelect={this.onSelectNorm}
                />
              </Col>
              <Col xs="auto" className="gx-1">
                <InputGroup>
                  <InputGroup.Text>Node</InputGroup.Text>
                  <Form.Control
                    as="select"
                    onChange={this.onChangeNode}
                    ref={this.selectRef}
                  >
                    {this.getNodeOptions()}
                  </Form.Control>
                </InputGroup>
              </Col>
              <Col xs="auto" className="gx-1">
                <InputGroup>
                  <InputGroup.Text>Point</InputGroup.Text>
                  <Form.Control
                    type="number"
                    ref={this.point}
                    onChange={this.onChangePoint}
                    min="0"
                    max={this.max}
                  />
                </InputGroup>
              </Col>
            </Row>
          </Container>
          <div className="message">{this.state.loadingMessage}</div>
          <div className="zoom">
            Zoom:
            {(this.state.zoom * 100).toFixed(0)}%
          </div>
        </div>
        <RemountOnResize>
          <ZoomPanCanvas
            className="scanimage"
            url={this.state.url}
            onZoomChange={this.onZoomChange}
            onLoadProgress={this.onLoadProgress}
            onReady={this.onReady}
            onError={this.onError}
            maxZoom={4}
          >
            <div className="plot-error">
              {this.state.error && (
                <Alert variant="danger">
                  Error loading image: {this.state.error}
                </Alert>
              )}
            </div>
          </ZoomPanCanvas>
        </RemountOnResize>
      </div>
    );
  }
}
