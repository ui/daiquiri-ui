import ErrorBoundary from 'components/ErrorBoundary';
import PlotEnhancer from 'components/PlotEnhancer';
import RemountOnResize from 'components/utils/RemountOnResize';
import { debounce, each, range } from 'lodash';
import moment from 'moment';
import type { ChangeEvent, RefObject } from 'react';
import { Component, createRef } from 'react';
import { Button, Form, InputGroup, Row, Col, Container } from 'react-bootstrap';

import H5Web0dPlot from '../plotting/H5Web0dPlot';
import type { PlotData } from '../plotting/models';
import AxesSelect from './controls/AxesSelect';
import SeriesSelect from './controls/SeriesSelect';
import type {
  Options,
  PlotlyClickEvent,
  Scan,
  AxesParams,
  PagedScanData,
  FieldData,
} from './models';
import { findLastScanId } from './utils';

export interface Options0d extends Options {
  /** Enforce a backend */
  backend?: 'plotly' | 'h5web';
  /** Pre-select an x axis */
  preSelectedXAxis?: string;
  /** Pre-select a data series */
  preSelectedSeries?: string | string[];
}

export interface ScanPlot0dActions {
  selectPoint: (index: string | number) => void;
  setPageSize: (size: number) => void;
  setPage: (pageNumber: number, fetch?: boolean) => void;
  setParams: (p: { scalars: string[] }, fetch?: boolean) => void;
  fetchScanSpectra: (payload: unknown) => Promise<void>;
  selectSeries: (s: string) => void;
  unselectSeries: (s: string) => void;
  resetSeries: (payload?: unknown) => void;
  selectAxes: (axisParams: AxesParams) => void;
  unselectAxes: (payload: unknown) => void;
  resetAxes: (payload?: unknown) => void;
  setFollow: () => void;
}

interface Props {
  scans: Record<string, Scan>;
  data: Record<string, PagedScanData>;
  fetching: boolean;
  actions: ScanPlot0dActions;
  selectedSeries: Record<string, boolean>;
  selectedAxes: { x: string; y: string };
  selectedScans: number[];
  selectedScan: number;
  pageSize: number;
  options: Options0d;
}

function clampDataLength(
  xData: number[],
  yData: number[]
): { xData: number[]; yData: number[] } {
  if (xData.length === yData.length) {
    return {
      xData,
      yData,
    };
  }

  if (xData.length > yData.length) {
    return {
      xData: xData.slice(0, yData.length),
      yData,
    };
  }
  return {
    xData,
    yData: yData.slice(0, xData.length),
  };
}

export default class ScanPlot0d extends Component<Props> {
  public doSetPageSize: (size: number) => void;
  public doSetPage: (pageNumber: number, b?: boolean) => void;
  public pageRef: RefObject<HTMLInputElement>;

  public constructor(props: Props) {
    super(props);

    this.doSetPageSize = debounce(this.props.actions.setPageSize, 500);
    this.doSetPage = debounce(this.props.actions.setPage, 500);

    this.pageRef = createRef();
  }

  public componentDidMount() {
    const { options = {}, actions } = this.props;
    const { preSelectedXAxis, preSelectedSeries } = options;

    actions.setFollow();

    // Select pre-selected axes
    if (preSelectedXAxis) {
      actions.selectAxes({
        type: 'x',
        axis: preSelectedXAxis,
      });
    }
    if (preSelectedSeries) {
      if (Array.isArray(preSelectedSeries)) {
        preSelectedSeries.forEach((series) => actions.selectSeries(series));
      } else {
        actions.selectSeries(preSelectedSeries);
      }
    }
  }

  public componentDidUpdate(prevProps: Props) {
    const cur = this.props.data[this.props.selectedScan] || {};
    const prev = prevProps.data[prevProps.selectedScan] || {};
    if (cur.page !== prev.page && this.pageRef.current) {
      this.pageRef.current.value = `${cur.page || 1}`;
    }

    if (prevProps.selectedSeries !== this.props.selectedSeries) {
      this.props.actions.setParams(
        {
          scalars: [
            ...(this.props.selectedAxes.x ? [this.props.selectedAxes.x] : []),
            ...(this.props.selectedAxes.y ? [this.props.selectedAxes.y] : []),
            ...Object.keys(this.props.selectedSeries),
          ],
        },
        true
      );
    }

    if (prevProps.selectedAxes !== this.props.selectedAxes) {
      this.props.actions.setParams(
        {
          scalars: [
            ...(this.props.selectedAxes.x ? [this.props.selectedAxes.x] : []),
            ...(this.props.selectedAxes.y ? [this.props.selectedAxes.y] : []),
            ...Object.keys(this.props.selectedSeries),
          ],
        },
        true
      );
    }
  }

  public onClick = (index: string | number | undefined) => {
    if (index !== undefined) {
      this.props.actions.selectPoint(index);
    }
  };

  public setPageSize = (e: ChangeEvent<HTMLInputElement>) => {
    this.doSetPage(1);
    this.doSetPageSize(Number.parseInt(e.target.value, 10));
  };

  public setPage = (e: ChangeEvent<HTMLInputElement>) => {
    this.doSetPage(Number.parseInt(e.target.value, 10), true);
  };

  public selectSeries = (series: string, checked: boolean) => {
    if (checked) {
      this.props.actions.selectSeries(series);
    } else {
      this.props.actions.unselectSeries(series);
    }
  };

  public render() {
    const lastScanId = findLastScanId(this.props.scans);

    if (lastScanId === null) {
      return <span>No scan loaded.</span>;
    }

    const selectedScan = this.props.selectedScan || lastScanId;
    const obj = this.props.data[selectedScan] || {};
    if (!obj.data) {
      return <span>Waiting for scan data</span>;
    }

    const selected =
      Object.keys(this.props.selectedSeries).length > 0
        ? Object.keys(this.props.selectedSeries)
        : obj.axes.ys.scalars.filter((s) => !s.startsWith('timer')).slice(0, 2);

    const data: PlotData[] = [];
    const isScatterData =
      (obj.axes.xs.length === 2 && !this.props.selectedAxes.x) ||
      (this.props.selectedAxes.x && this.props.selectedAxes.y);

    if (isScatterData) {
      let x: number[] = [];
      let y: number[] = [];
      if (this.props.selectedAxes.x && this.props.selectedAxes.y) {
        const xind = obj.data[this.props.selectedAxes.x];
        const yind = obj.data[this.props.selectedAxes.y];

        if (!xind || !yind) {
          return (
            <span>
              The selected axes are not valid for the current scan{' '}
              <Button size="sm" onClick={() => this.props.actions.resetAxes()}>
                Reset
              </Button>
            </span>
          );
        }

        x = xind.data;
        y = yind.data;
      } else {
        const xchan = obj.data[obj.axes.xs[0]];
        const ychan = obj.data[obj.axes.xs[1]];
        x = xchan ? xchan.data : [];
        y = ychan ? ychan.data : [];
      }

      each(selected, (s) => {
        if (!obj.axes.ys.scalars.includes(s)) {
          return;
        }

        const zchan = obj.data[s];
        if (zchan) {
          const sc = {
            x,
            y,
            type: 'scattergl',
            name: zchan.name,
            mode: 'markers',
            text: zchan.data?.map((v) => v.toPrecision(4)),
            marker: {
              size: 10,
              // Warning: Plotly does not play nice with SeamlessImmutable's array for colour
              // data. We therefore make a copy to a normal array with a spread
              color: zchan.data && [...zchan.data],
              colorscale: 'Viridis' as const,
            },
          };
          data.push(sc);
        }
      });
    } else {
      const allScans = this.props.selectedScans
        ? [
            ...Object.values(this.props.selectedScans).map(
              (sid) => this.props.data[sid]
            ),
          ]
        : [obj];

      try {
        each(allScans, (d) => {
          let x: FieldData;
          if (d.axes.xs.length === 0 && !this.props.selectedAxes.x) {
            x = { data: [0], name: 'index', shape: [1] };
          } else {
            const xaxis = this.props.selectedAxes.x || d.axes.xs[0];
            x = d.data[xaxis];
          }
          const scan: Scan | undefined = this.props.scans[d.scanid];

          if (!x) {
            throw new Error(
              'The selected axes are not valid for the current scan(s)'
            );
          }

          if (x.data.length === 0) {
            throw new Error('Waiting for data for the selected axes');
          }

          each(selected, (s) => {
            if (!d.axes.ys.scalars.includes(s)) {
              return;
            }

            const y = d.data[s];
            if (!y) {
              return;
            }

            if (!y.data) {
              console.log(
                `Data for ${d.scanid} series ${y.name} not loaded yet`
              );
              return;
            }

            if (x.name === 'index') x.data = range(y.data.length);
            const { xData, yData } = clampDataLength(x.data, y.data);
            const sc = {
              x: [...xData],
              y: yData,
              // yaxis: 'y'+(i+1),
              type: 'scattergl',
              name:
                d.scanid === selectedScan && scan
                  ? y.name
                  : `${moment
                      .unix(scan.start_timestamp)
                      .format('DD-MM-YYYY HH:mm:ss')}: ${y.name}`,
            };
            data.push(sc);
          });
        });
      } catch (error: unknown) {
        if (error instanceof Error) {
          return (
            <span>
              {error.message}
              <Button
                style={{ marginLeft: '1rem' }}
                size="sm"
                onClick={() => this.props.actions.resetAxes()}
              >
                Reset
              </Button>
            </span>
          );
        }
      }
    }
    const { options } = this.props;
    const { preSelectedXAxis, preSelectedSeries } = options;
    const { backend = 'h5web' } = options;

    return (
      <div className="plot1d-container">
        <Container>
          <Row>
            <Col xs="auto" className="gx-1">
              <AxesSelect
                onChange={(d) => this.props.actions.selectAxes(d)}
                series={[...obj.axes.xs, ...obj.axes.ys.scalars]}
                selected={this.props.selectedAxes}
                resetAxes={this.props.actions.resetAxes}
                unselectAxes={this.props.actions.unselectAxes}
              />
            </Col>
            <Col xs="auto" className="gx-1">
              <SeriesSelect
                onChange={this.selectSeries}
                series={obj.axes.ys.scalars}
                selected={this.props.selectedSeries}
                resetSeries={this.props.actions.resetSeries}
              />
            </Col>
            <Col xs="auto" className="gx-1">
              <InputGroup>
                <InputGroup.Text>Points</InputGroup.Text>
                <Form.Control
                  as="select"
                  onChange={this.setPageSize}
                  value={this.props.pageSize}
                >
                  {[5, 10, 25, 100, 1000].map((p) =>
                    obj.npoints === null || p < obj.npoints ? (
                      <option key={p} value={p}>
                        {p}
                      </option>
                    ) : null
                  )}
                  {obj.npoints !== null && (
                    <option
                      value={obj.npoints}
                    >{`All (${obj.npoints})`}</option>
                  )}
                </Form.Control>
              </InputGroup>
            </Col>
            <Col xs="auto" className="gx-1">
              <InputGroup>
                <InputGroup.Text>Page</InputGroup.Text>
                <Button
                  size="sm"
                  onClick={() => this.doSetPage(obj.page - 1, true)}
                  disabled={obj.page === 1}
                >
                  <i className="fa fa-caret-left" />
                </Button>
                <Form.Control
                  ref={this.pageRef}
                  type="number"
                  onChange={this.setPage}
                  defaultValue={obj.page}
                  max={obj.pages}
                  min={1}
                />
                <Button
                  size="sm"
                  onClick={() => this.doSetPage(obj.page + 1, true)}
                  disabled={obj.page === obj.pages}
                >
                  <i className="fa fa-caret-right" />
                </Button>
              </InputGroup>
            </Col>

            {this.props.fetching && (
              <Col xs="auto">
                <i className="fa fa-spin fa-spinner" />
              </Col>
            )}
          </Row>
        </Container>

        <RemountOnResize>
          {backend === 'h5web' ? (
            <ErrorBoundary>
              <H5Web0dPlot data={data} onClick={this.onClick} />
            </ErrorBoundary>
          ) : (
            <PlotEnhancer
              data={data}
              layout={{}}
              onClick={(e: PlotlyClickEvent) =>
                this.onClick(e.points[0]?.pointNumber)
              }
            />
          )}
        </RemountOnResize>
        {(preSelectedXAxis || preSelectedSeries) && (
          <span>
            {preSelectedSeries} vs. {preSelectedXAxis}
          </span>
        )}
      </div>
    );
  }
}
