import PlotEnhancer from 'components/PlotEnhancer';
import H5WebCurvePlot from 'components/plotting/H5WebCurvePlot';
import RemountOnResize from 'components/utils/RemountOnResize';
import { each, range } from 'lodash';
import type { ChangeEvent, RefObject } from 'react';
import { createRef, Component } from 'react';
import { Form, Row, Col, Button, Container } from 'react-bootstrap';
import type { PlotData } from '../plotting/models';

import { findLastScanId } from './utils';
import type {
  Layout,
  Options,
  Scan,
  ScanData,
  SpectraData,
  SpectraContainer,
  PlotSeries,
} from './models';

export interface Options1d extends Options {
  /** Enforce a backend */
  backend?: 'plotly' | 'h5web';
  /** Pre-select an x axis */
  preSelectedXAxis?: string;
  /** Pre-select a data series */
  preSelectedSeries?: string | string[];
  /** Title for this plot */
  plotTitle?: string;
  /** Inset the legend for h5web plot */
  insetLegend?: boolean;
}

interface Props {
  actions: {
    selectPoint: (v: string | number) => void;
    clearPoint: () => void;
    fetchScanSpectra: (p: { scanid: number; point: number }) => Promise<void>;
    setPage: (pageNumber: number) => void;
  };
  scans: Scan[];
  data: Record<number, ScanData>;
  spectra: SpectraContainer;
  currentPoint?: number;
  selectable?: boolean;
  fetching: boolean;
  layout?: Layout;
  selectedScan?: number;
  options: Options1d;
}

export default class ScanPlot1d extends Component<Props> {
  private readonly point: RefObject<HTMLInputElement>;
  private max: number;
  private lastScanId?: number;

  public constructor(props: Props) {
    super(props);
    this.point = createRef<HTMLInputElement>();
    this.max = 0;
  }

  public componentDidUpdate(prevProps: Props) {
    const { currentPoint } = this.props;

    const { scanid, npoints_avail } = this.data();
    if (!scanid) {
      return;
    }

    const fetchNewScan = this.lastScanId !== scanid;
    this.lastScanId = scanid;

    const fetch = this.max !== npoints_avail;
    this.max = npoints_avail;

    const point = currentPoint ?? this.max - 1;
    if (this.point.current) {
      this.point.current.value = `${point}`;
    }

    if (point === -1) return;

    if (
      prevProps.currentPoint !== currentPoint &&
      currentPoint !== null &&
      currentPoint !== undefined
    ) {
      this.fetchScanSpectra(currentPoint);
    }

    if (
      currentPoint === null &&
      (fetchNewScan || fetch || prevProps.currentPoint !== null)
    ) {
      this.fetchScanSpectra(point);
    }
  }

  public onChangePoint = (e: ChangeEvent<{ value: string }>) => {
    const value = Number(e.target.value);
    if (value < this.max) {
      this.props.actions.selectPoint(value);
    }
  };

  public clearSelection = () => {
    this.props.actions.clearPoint();
  };

  public data() {
    return this.props.data[this.selectedScan()] || {};
  }

  public selectedScan(): number {
    if (this.props.selectedScan) {
      return this.props.selectedScan;
    }

    return findLastScanId(this.props.scans) || 0;
  }

  public async fetchScanSpectra(point: number) {
    if (!this.data().scanid) {
      return;
    }
    if (point < 0 && this.props.currentPoint && this.props.currentPoint < 0) {
      return;
    }
    if (this.props.fetching) {
      return;
    }

    try {
      await this.props.actions.fetchScanSpectra({
        scanid: this.selectedScan(),
        point,
      });
    } catch (error) {
      console.log("Couldn't load scan spectra", error);
    }
  }

  private generateXData(yData: number[], obj: SpectraContainer): number[] {
    const { conversion } = obj;
    return conversion
      ? range(yData.length).map((v) => v * conversion.scale + conversion.zero)
      : range(yData.length);
  }

  private generateSeries(
    series: SpectraData,
    obj: SpectraContainer,
    preSelectedXAxis: string | undefined = undefined
  ): PlotSeries | undefined {
    if (!series) return undefined;
    const [yd] = series.data;
    if (!yd) return undefined;

    const x = preSelectedXAxis
      ? obj.data[preSelectedXAxis].data[0]
      : this.generateXData(yd, obj);

    return {
      x: [...x],
      y: [...yd],
      type: 'scattergl',
      name: series.name,
    };
  }

  public render() {
    if (Object.keys(this.props.spectra).length === 0) {
      return <span>Waiting for scan spectra</span>;
    }

    const { selectable = true, options = {}, spectra: obj } = this.props;
    const {
      backend = 'h5web',
      preSelectedXAxis,
      preSelectedSeries,
      plotTitle,
      insetLegend,
    } = options;

    const data: PlotData[] = [];

    if (preSelectedSeries) {
      const selectedSeries = Array.isArray(preSelectedSeries)
        ? preSelectedSeries
        : [preSelectedSeries];
      selectedSeries.forEach((series) => {
        const s = obj.data[series];
        const plotSeries = this.generateSeries(s, obj, preSelectedXAxis);
        if (plotSeries) data.push(plotSeries);
      });
    } else {
      each(obj.data, (s) => {
        const plotSeries = this.generateSeries(s, obj);
        if (plotSeries) data.push(plotSeries);
      });
    }

    return (
      <div className="plot1d-container">
        {selectable && (
          <Container>
            <Form.Group as={Row}>
              <Form.Label column sm={3}>
                Point
              </Form.Label>
              <Col>
                <Form.Control
                  type="number"
                  ref={this.point}
                  onChange={this.onChangePoint}
                  min="0"
                  max={this.max}
                />
              </Col>
              {this.props.currentPoint !== null && (
                <Col xs={2}>
                  <Button onClick={this.clearSelection}>Follow</Button>
                </Col>
              )}
              {this.props.fetching && (
                <Col xs={1}>
                  <div className="my-auto ms-2">
                    <i className="fa fa-spin fa-spinner" />
                  </div>
                </Col>
              )}
            </Form.Group>
          </Container>
        )}
        <RemountOnResize>
          {backend === 'h5web' ? (
            <H5WebCurvePlot
              insetLegend={insetLegend}
              data={data}
              layout={this.props.layout}
            />
          ) : (
            <PlotEnhancer
              className="plot"
              data={data}
              layout={(this.props.layout ?? {}) as Partial<Plotly.Layout>}
            />
          )}
        </RemountOnResize>
        {(preSelectedXAxis || preSelectedSeries || plotTitle) && (
          <span>
            {plotTitle || `${preSelectedSeries} vs. ${preSelectedXAxis}`}
          </span>
        )}
      </div>
    );
  }
}
