import type { ChangeEvent } from 'react';
import { Form, Container, DropdownButton, Button } from 'react-bootstrap';

interface SeriesSelectProps {
  onChange: (name: string, checked: boolean) => void;
  resetSeries: () => void;
  series: string[];
  selected: Record<string, boolean>;
}

function SeriesSelect(props: SeriesSelectProps) {
  const selectSeries = (e: ChangeEvent<HTMLInputElement>) => {
    props.onChange(e.target.name, e.target.checked);
  };

  return (
    <DropdownButton title="Series">
      <Container className="series-select">
        <div className="text-end">
          <Button size="sm" onClick={() => props.resetSeries()}>
            Reset
          </Button>
        </div>
        {props.series.map((series) => (
          <Form.Check
            key={series}
            className="justify-content-start"
            checked={series in props.selected}
            type="checkbox"
            id={`${series}-check`}
            label={series}
            onChange={selectSeries}
            name={series}
          />
        ))}
      </Container>
    </DropdownButton>
  );
}

export default SeriesSelect;
