import type { ChangeEvent } from 'react';
import {
  Form,
  Container,
  DropdownButton,
  Button,
  Row,
  Col,
} from 'react-bootstrap';
import type { AxesParams } from '../models';

interface AxesSelectProps {
  onChange: (p: AxesParams) => void;
  unselectAxes: (axis: 'x' | 'y') => void;
  resetAxes: () => void;
  series: string[];
  selected: { x: string; y: string };
}

function AxesSelect(props: AxesSelectProps) {
  const selectAxis = (e: ChangeEvent<HTMLInputElement>) => {
    props.onChange({ type: e.target.name, axis: e.target.value });
  };

  return (
    <DropdownButton title="Axes">
      <Container className="series-select">
        <Row>
          <Col xs={1}>
            <Button
              size="sm"
              onClick={() => props.unselectAxes('x')}
              className="mx-auto"
            >
              x
            </Button>
          </Col>
          <Col xs={1}>
            <Button
              size="sm"
              onClick={() => props.unselectAxes('y')}
              className="mx-auto"
            >
              y
            </Button>
          </Col>
          <Col className="text-end">
            <Button size="sm" onClick={() => props.resetAxes()}>
              Reset
            </Button>
          </Col>
        </Row>
        {props.series.map((series) => (
          <Row key={series}>
            <Col xs={1}>
              <Form.Check
                checked={series === props.selected.x}
                name="x"
                type="radio"
                onChange={selectAxis}
                value={series}
                className="my-auto"
              />
            </Col>
            <Col xs={1}>
              <Form.Check
                checked={series === props.selected.y}
                name="y"
                type="radio"
                onChange={selectAxis}
                value={series}
                className="my-auto"
              />
            </Col>

            <Form.Label column className="justify-content-start col-10">
              {series}
            </Form.Label>
          </Row>
        ))}
      </Container>
    </DropdownButton>
  );
}

export default AxesSelect;
