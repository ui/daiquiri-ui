import { useEffect } from 'react';
import { Row, Col } from 'react-bootstrap';
import type { Scan } from './models';
import { findLastScanId } from './utils';

export interface ScanValueOptions {
  /** Selected scan key to display */
  scanKey: string;
  /** Name for the value (overrides `scanKey` as name) */
  name?: string;
}

interface Props {
  scans: Record<string, Scan>;
  actions: {
    setFollow: () => void;
  };
  selectedScan: number;
  options: ScanValueOptions;
}

export default function ScanValue(props: Props) {
  const { options, selectedScan, scans, actions } = props;
  const { scanKey, name } = options;

  const lastScanId = findLastScanId(scans);

  useEffect(() => {
    actions.setFollow();
  }, []);

  const fieldName = name ?? scanKey;
  if (lastScanId === null) {
    return (
      <div className="scan-value">
        <Row>
          <Col>{fieldName}:</Col>
          <Col>No scan loaded</Col>
        </Row>
      </div>
    );
  }

  const scanid = selectedScan || lastScanId;
  const scanObj = scans[scanid] || {};
  const value: any = scanObj[scanKey as keyof typeof scanObj];
  if (!value) {
    return (
      <div className="scan-value">
        <Row>
          <Col>{fieldName}:</Col>
          <Col>
            <i className="fa fa-warning me-1" />
            No data for this scan
          </Col>
        </Row>
      </div>
    );
  }

  return (
    <div className="scan-value">
      <Row>
        <Col>{fieldName}:</Col>
        <Col>{value}</Col>
      </Row>
    </div>
  );
}
