export type Backend = 'plotly' | 'h5web';

export interface Options {
  backend?: Backend;
}

export interface Scan {
  scanid: number;
  start_timestamp: number;
  title: string;
  filename: string;
  scan_number: string;
}

export interface FieldData {
  data: number[];
  name: string;
  shape: number[];
}

export interface ScanData {
  scanid: number;
  npoints_avail: number;
  data: Record<string, FieldData>;
  axes: {
    xs: string[];
    ys: {
      images: [];
      scalars: string[];
      spectra: string[];
    };
  };
}

export interface PagedScanData extends ScanData {
  page: number;
  pages: number;
  npoints: number;
}

export interface SpectraData {
  data: [number[]];
  name: string;
}

export interface SpectraContainer {
  data: Record<string, SpectraData>;
  conversion?: {
    scale: number;
    zero: number;
  };
}

export interface PlotSeries {
  x: number[];
  y: number[];
  type: string;
  name: string;
}

export interface PlotlyClickEvent {
  points: { pointNumber: number }[];
}

export interface AxesParams {
  type: string;
  axis: string;
}

export interface Shape {
  type: string;
  xref: string;
  yref: string;
  x0: number;
  x1: number;
  y0: number;
  y1: number;
  fillcolor: string;
  opacity?: number;
  line?: {
    width: number;
  };
}

export interface Annotation {
  xref: string;
  yref: string;
  x: number;
  y: number;
  text: string;
  showarrow: boolean;
}

export interface Layout {
  shapes?: Shape[];
  annotations?: Annotation[];
  xaxis?: {
    title: string;
  };
  yaxis?: {
    title: string;
  };
}
