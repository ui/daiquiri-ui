import { useSelector } from 'react-redux';
import { MonitorPanelItem } from '@esrf/daiquiri-lib';
import type { Monitor } from '@esrf/daiquiri-lib';
import type { ReduxState } from 'types/ReduxStore';

export function MonitorSample(props: { monitor: Monitor }) {
  const { monitor } = props;
  const sampleName = useSelector<ReduxState, string | undefined>((state) => {
    function getSampleId() {
      const currentId = state.metadata.samples.current;
      if (currentId !== undefined) {
        return currentId;
      }
      if (!state.metadata.samples.selection) {
        return undefined;
      }
      return state.metadata.samples.selection[0];
    }
    const sampleId = getSampleId();
    if (sampleId === undefined) {
      return undefined;
    }
    return state.metadata.ns_sample.default.results[sampleId]?.name;
  });
  return (
    <MonitorPanelItem>
      <h1>{monitor.name}</h1>
      <div
        className="text-truncate"
        style={{ maxWidth: '10em' }}
        title={sampleName}
      >
        {sampleName ?? '-'}
      </div>
    </MonitorPanelItem>
  );
}
