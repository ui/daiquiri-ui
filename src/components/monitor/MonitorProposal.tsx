import { useSelector } from 'react-redux';
import { MonitorPanelItem } from '@esrf/daiquiri-lib';
import type { Monitor } from '@esrf/daiquiri-lib';
import type { ReduxState } from 'types/ReduxStore';

export function MonitorProposal(props: { monitor: Monitor }) {
  const { monitor } = props;
  const blsession = useSelector<ReduxState, string>(
    (state) => state.session.current.data.blsession
  );
  const proposal = blsession.split('-', 1)[0];
  return (
    <MonitorPanelItem>
      <h1>{monitor.name}</h1>
      <div>{proposal}</div>
    </MonitorPanelItem>
  );
}
