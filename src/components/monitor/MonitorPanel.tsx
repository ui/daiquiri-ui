import {
  registerMonitorComponent,
  registerRuntimeHook,
} from '@esrf/daiquiri-lib';
import { useHardware } from '../utils/hooks';
import { MonitorProposal } from './MonitorProposal';
import { MonitorSample } from './MonitorSample';

registerRuntimeHook('useHardware', useHardware);
registerMonitorComponent('MonitorProposal', MonitorProposal);
registerMonitorComponent('MonitorSample', MonitorSample);

export { MonitorPanel as default } from '@esrf/daiquiri-lib';
