import type { Vector3 } from 'three';

export interface VisViewpoint {
  center: Vector3;
  scale: Vector3;
}
