import { useVisCanvasContext } from '@h5web/lib';
import type { HistogramParams as Histogram } from '@h5web/lib';
import { sum } from 'lodash';
import { useCallback, useMemo } from 'react';
import { useThree } from '@react-three/fiber';
import { Vector3 } from 'three';
import type { UseUndoable } from 'use-undoable';
import useUndoable from 'use-undoable';

import { stripHistogram } from './utils';
import type { VisViewpoint } from './models';

export function useStrippedHistogram(histogram: Histogram): Histogram {
  return useMemo(() => stripHistogram(histogram), [histogram]);
}

export function useVisViewpointToCamera(): (viewpoint: VisViewpoint) => {
  position: Vector3;
  scale: Vector3;
} {
  const camera = useThree((state) => state.camera);
  const { dataToWorld, abscissaConfig, ordinateConfig, visSize } =
    useVisCanvasContext();
  const { width, height } = visSize;

  return useCallback(
    (viewpoint: VisViewpoint) => {
      const { center, scale } = viewpoint;
      const worldCenter = dataToWorld(center);
      const sx =
        (scale.x * width) /
        Math.abs(abscissaConfig.visDomain[1] - abscissaConfig.visDomain[0]);
      const sy =
        (scale.y * height) /
        Math.abs(ordinateConfig.visDomain[1] - ordinateConfig.visDomain[0]);
      return {
        position: new Vector3(worldCenter.x, worldCenter.y, camera.position.z),
        scale: new Vector3(sx, sy, camera.scale.z),
      };
    },
    [camera, dataToWorld, abscissaConfig, ordinateConfig, width, height]
  );
}

export function useGetVisViewpoint(): () => VisViewpoint {
  const { width, height } = useThree((state) => state.size);
  const camera = useThree((state) => state.camera);
  const { getVisibleDomains } = useVisCanvasContext();

  return () => {
    const { xVisibleDomain, yVisibleDomain } = getVisibleDomains(camera);

    const center = new Vector3(
      sum(xVisibleDomain),
      sum(yVisibleDomain)
    ).multiplyScalar(0.5);

    const scale = new Vector3(
      Math.abs(xVisibleDomain[1] - xVisibleDomain[0]) / width,
      Math.abs(yVisibleDomain[1] - yVisibleDomain[0]) / height,
      1
    );

    return { center, scale };
  };
}

export type UseUndoableViewpoint = UseUndoable<
  | {
      centerX: number;
      centerY: number;
      scale: number;
    }
  | undefined
>;

export function useUndoableViewpoint(): UseUndoableViewpoint {
  return useUndoable<
    | {
        centerX: number;
        centerY: number;
        scale: number;
      }
    | undefined
  >(undefined, {
    behavior: 'destroyFuture',
    historyLimit: 50,
    ignoreIdenticalMutations: true,
  });
}
