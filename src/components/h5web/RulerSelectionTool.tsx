import { useMemo, useState } from 'react';
import {
  Annotation,
  SelectionTool,
  useCameraState,
  useVisCanvasContext,
} from '@h5web/lib';
import type { Selection } from '@h5web/lib';
import { toHumanReadableUnit } from '../../helpers/QtyHelper';
import Qty from 'js-quantities';
import { LineWithDataDash } from './items/shapes/LineWithDataDash';
import { Vector2, Vector3 } from 'three';
import { AngleRoi } from './items/rois/AngleRoi';
import type { AngleRoiGeometry } from './items/rois/geometries';
import { useLinearVisCanvas } from './LinearVisCanvas2';

/**
 * Find a closed smaller number which is much more human readable.
 *
 * Select a number like 1, 2.5, 5
 */
function normalizeNumber(value: number): number {
  const digits = Math.ceil(Math.log10(value));
  const pow = 10 ** digits;
  const norm = value / pow;
  if (norm >= 1) {
    return Number(pow);
  }
  if (norm >= 0.5) {
    return 0.5 * pow;
  }
  if (norm >= 0.25) {
    return 0.25 * pow;
  }
  if (norm >= 0.1) {
    return 0.1 * pow;
  }
  return value;
}

/**
 * Display line selection together with the distance value.
 *
 * If a `plotUnit` is specified the value is normalized and displayed
 * together with a convinient unit.
 *
 * If `plotUnit` is `px`, the distance is displayed without normalization.
 */
function DistanceSelectionLine(props: Selection & { plotUnit?: string }) {
  const { data, plotUnit } = props;
  const [dataBegin, dataEnd] = data;

  const visCanvasContext = useVisCanvasContext();
  const { width } = visCanvasContext.canvasSize;

  const dataWidth = useCameraState(
    (camera) => {
      const visibleDomain = visCanvasContext.getVisibleDomains(camera);
      const dataWidth =
        visibleDomain.xVisibleDomain[1] - visibleDomain.xVisibleDomain[0];
      return Math.abs(dataWidth);
    },
    [width]
  );

  const gapWidth = useMemo(() => {
    return normalizeNumber(dataWidth / 15);
  }, [dataWidth]);

  const distance = dataBegin.distanceTo(dataEnd);
  const center = dataBegin.clone().add(dataEnd).multiplyScalar(0.5);

  function getText(distance: number, unit?: string) {
    if (!unit) {
      return distance.toFixed(2);
    }
    if (unit === 'px') {
      return `${distance.toFixed(2)} px`;
    }
    return toHumanReadableUnit(new Qty(distance, unit)).toPrec(0.01).toString();
  }

  const text = getText(distance, plotUnit);
  return (
    <>
      <LineWithDataDash
        p1={[dataBegin.x, dataBegin.y]}
        p2={[dataEnd.x, dataEnd.y]}
        color="red"
        gapColor="#F0F0F0"
        dashSize={gapWidth}
        gapSize={gapWidth}
        lineWidth={2}
        zIndex={3}
        opacity={0.75}
      />
      <Annotation x={center.x} y={center.y} style={{ zIndex: 100 }} center>
        <div className="bg-dark text-light p-1 rounded">{text}</div>
      </Annotation>
    </>
  );
}

/**
 * Display orthogonal lines selection together with the distance value.
 *
 * If a `plotUnit` is specified the value is normalized and displayed
 * together with a convinient unit.
 *
 * If `plotUnit` is `px`, the distance is displayed without normalization.
 */
function OrthoSelectionLine(props: Selection & { plotUnit?: string }) {
  const { data, plotUnit } = props;
  const [dataBegin, dataEnd] = data;

  const visCanvasContext = useVisCanvasContext();
  const { width } = visCanvasContext.canvasSize;

  const dataWidth = useCameraState(
    (camera) => {
      const visibleDomain = visCanvasContext.getVisibleDomains(camera);
      const dataWidth =
        visibleDomain.xVisibleDomain[1] - visibleDomain.xVisibleDomain[0];
      return Math.abs(dataWidth);
    },
    [width]
  );

  const gapWidth = useMemo(() => {
    return normalizeNumber(dataWidth / 15);
  }, [dataWidth]);

  const dataBeginX = dataBegin;
  const dataEndX = new Vector3(dataEnd.x, dataBegin.y);
  const dataBeginY = dataEndX;
  const dataEndY = dataEnd;

  const distanceX = Math.abs(dataBeginX.x - dataEndX.x);
  const distanceY = Math.abs(dataBeginY.y - dataEndY.y);
  const centerX = dataBeginX.clone().add(dataEndX).multiplyScalar(0.5);
  const centerY = dataBeginY.clone().add(dataEndY).multiplyScalar(0.5);

  function getText(distance: number, unit?: string) {
    if (!unit) {
      return distance.toFixed(2);
    }
    if (unit === 'px') {
      return `${distance.toFixed(2)} px`;
    }
    return toHumanReadableUnit(new Qty(distance, unit)).toPrec(0.01).toString();
  }

  const textX = getText(distanceX, plotUnit);
  const textY = getText(distanceY, plotUnit);
  const displayX = distanceX > distanceY * 0.05;
  const displayY = distanceY > distanceX * 0.05;
  return (
    <>
      {displayX && (
        <LineWithDataDash
          p1={[dataBeginX.x, dataBeginX.y]}
          p2={[dataEndX.x, dataEndX.y]}
          color="red"
          gapColor="#F0F0F0"
          dashSize={gapWidth}
          gapSize={gapWidth}
          lineWidth={2}
          zIndex={3}
          opacity={0.75}
        />
      )}
      {displayY && (
        <LineWithDataDash
          p1={[dataBeginY.x, dataBeginY.y]}
          p2={[dataEndY.x, dataEndY.y]}
          color="red"
          gapColor="#F0F0F0"
          dashSize={gapWidth}
          gapSize={gapWidth}
          lineWidth={2}
          zIndex={3}
          opacity={0.75}
        />
      )}
      {displayX && (
        <Annotation x={centerX.x} y={centerX.y} style={{ zIndex: 100 }} center>
          <div className="bg-dark text-light p-1 rounded">{textX}</div>
        </Annotation>
      )}
      {displayY && (
        <Annotation x={centerY.x} y={centerY.y} style={{ zIndex: 100 }} center>
          <div className="bg-dark text-light p-1 rounded">{textY}</div>
        </Annotation>
      )}
    </>
  );
}

function createAngleRoiFromRect(
  start: Vector3,
  end: Vector3
): AngleRoiGeometry {
  const v = new Vector2(end.x - start.x, end.y - start.y);
  const angle = (Math.atan2(v.x, v.y) * 180) / Math.PI;
  return {
    x: start.x,
    y: start.y,
    radius: v.length(),
    angle,
    angleRange: 30,
  };
}

/**
 * @param mouseMode The following values are supported
 *  - 'measure-distance': The mouse behave as a ruler for distance
 *  - 'measure-ortho': The mouse behave as a ruler for orthogonal distances
 *  - 'measure-angle': The mouse behave to create an interactive protractor
 *  - else nothing is displayed
 */
export default function RulerSelectionTool(props: {
  mouseMode: string;
  disabled?: boolean;
  plotUnit: string;
}) {
  const { mouseMode, disabled = false, plotUnit } = props;
  const measureAngle = mouseMode === 'measure-angle';
  const [angleGeometry, setAngleGeometry] = useState({
    x: 0,
    y: 0,
    radius: 0,
    angle: 0,
    angleRange: 0,
  });

  const linearVisCanvas = useLinearVisCanvas();

  return (
    <>
      <SelectionTool
        id="ruler3-selection-tool"
        disabled={mouseMode !== 'measure-angle' || disabled}
        onSelectionChange={(selection) => {
          if (selection === undefined) {
            return;
          }
          const g = createAngleRoiFromRect(
            selection.data[0],
            selection.data[1]
          );
          setAngleGeometry(g);
        }}
        onSelectionStart={() => {
          linearVisCanvas?.actions.captureMouseInteraction();
        }}
        onSelectionEnd={() => {
          linearVisCanvas?.actions.releaseMouseInteraction();
        }}
        onValidSelection={(selection) => {
          const g = createAngleRoiFromRect(
            selection.data[0],
            selection.data[1]
          );
          setAngleGeometry(g);
        }}
      >
        {(selection) => <></>}
      </SelectionTool>
      {measureAngle && (
        <AngleRoi
          geometry={angleGeometry}
          onGeometryChanged={(g) => {
            setAngleGeometry(g);
          }}
          color="red"
          lineWidth={2}
          zIndex={2}
          opacity={0.75}
        />
      )}
      <SelectionTool
        id="ruler-selection-tool"
        disabled={mouseMode !== 'measure-distance' || disabled}
        onSelectionStart={() => {
          linearVisCanvas?.actions.captureMouseInteraction();
        }}
        onSelectionEnd={() => {
          linearVisCanvas?.actions.releaseMouseInteraction();
        }}
      >
        {(selection) => (
          <DistanceSelectionLine {...selection} plotUnit={plotUnit} />
        )}
      </SelectionTool>
      <SelectionTool
        id="ruler2-selection-tool"
        disabled={mouseMode !== 'measure-ortho' || disabled}
        onSelectionStart={() => {
          linearVisCanvas?.actions.captureMouseInteraction();
        }}
        onSelectionEnd={() => {
          linearVisCanvas?.actions.releaseMouseInteraction();
        }}
      >
        {(selection) => (
          <OrthoSelectionLine {...selection} plotUnit={plotUnit} />
        )}
      </SelectionTool>
    </>
  );
}
