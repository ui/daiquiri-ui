import type { NdArray } from 'ndarray';
import { useEffect, useMemo, useState } from 'react';
import type { TextureDataType, TextureFilter } from 'three';
import { UnsignedIntType, UnsignedShortType } from 'three';
import {
  ClampToEdgeWrapping,
  DataTexture,
  FloatType,
  HalfFloatType,
  LinearFilter,
  NearestFilter,
  RedFormat,
  UnsignedByteType,
  UVMapping,
} from 'three';
import axios from 'axios';
import type { NdNormalizedUint8Array } from './NdNormalizedUint8Array';
import type { NdFloat16Array } from './NdFloat16Array';

const TEXTURE_TYPES: Record<string, TextureDataType> = {
  float32: FloatType,
  float16: HalfFloatType,
  uint8: UnsignedByteType,
  uint16: UnsignedShortType,
  uint32: UnsignedIntType,
  uint8_clamped: UnsignedByteType,
};

interface DataTextureOptions {
  magNearest?: boolean;
}

/**
 * Create a red texture to be used with shader programs
 *
 * TODO: Make sure the ndarray is an image
 */
export function getDataTexture(
  values:
    | NdArray<Float32Array>
    | NdFloat16Array
    | NdNormalizedUint8Array
    | undefined,
  options?: DataTextureOptions
): DataTexture | undefined {
  if (values === undefined) {
    return undefined;
  }
  const [rows, cols] = values.shape;
  const magFilter: TextureFilter = options?.magNearest
    ? NearestFilter
    : LinearFilter;
  const texture = new DataTexture(
    values.data,
    cols,
    rows,
    RedFormat,
    TEXTURE_TYPES[values.dtype],
    UVMapping,
    ClampToEdgeWrapping,
    ClampToEdgeWrapping,
    magFilter
  );
  texture.needsUpdate = true;

  return texture;
}

/**
 * Use a red texture from an ndarray to be used with shader programs
 */
export function useDataTexture(
  values:
    | NdArray<Float32Array>
    | NdFloat16Array
    | NdNormalizedUint8Array
    | undefined,
  options?: DataTextureOptions
): DataTexture | undefined {
  return useMemo(() => getDataTexture(values, options), [values, options]);
}

/**
 * Load a shader program by name
 *
 * @param name: Name of the resource to load (actually located inside `resources/shaders/`)
 * @param id: User identifier, which can be used for informal reference
 */
export function useResourceAsProgram(
  name: string,
  id?: string
): { name: string; data: string; id: string | undefined } | undefined {
  const url = `resources/shaders/${name}`;
  const [program, setProgram] = useState<{
    name: string;
    data: string;
    id: string | undefined;
  }>();
  useEffect(() => {
    axios.get(url).then((res) => setProgram({ name, data: res.data, id }));
  }, [url]);
  return program;
}
