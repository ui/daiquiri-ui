import { useCallback, useEffect, useState } from 'react';

export type DefaultMouseMode = 'pan' | 'zoom';

/**
 * State and interaction to handle a mouse mode.
 */
export interface MouseModeInteraction<T> {
  // Actual mouse mode
  mouseMode: T | DefaultMouseMode;
  // Mouse mode which is applied when resetMouseMode is called
  defaultMouseMode: T | DefaultMouseMode;
  // Set a mouse mode
  setMouseMode: (mouseMode: T | DefaultMouseMode) => void;
  // Reset the mouse mode to the default mouse mode
  resetMouseMode: () => void;
  // Set the mouse mode if it is not yet the right one, else reset the mode
  setOrResetMouseMode: (mouseMode: T | DefaultMouseMode) => void;
  // Reset the default mouse mode (the one used if there is no special interaction)
  setDefaultMouseMode: (mouseMode: DefaultMouseMode) => void;
}

/**
 * Provide a shared mouse mode with it's API.
 *
 * It contains a defaultMouseMode to be able to reset the mouse mode to
 * a common mode.
 */
export function useMouseModeInteraction<T>(
  initialMouseMode: T | DefaultMouseMode
): MouseModeInteraction<T> {
  const [mouseMode, setMouseMode] = useState<T | DefaultMouseMode>(
    initialMouseMode
  );
  const [defaultMouseMode, setDefaultMouseMode] = useState<
    T | DefaultMouseMode
  >('zoom');

  useEffect(() => {
    if (mouseMode === 'pan' || mouseMode === 'zoom') {
      setDefaultMouseMode(mouseMode);
    }
  }, [mouseMode, setDefaultMouseMode]);

  const resetMouseMode = useCallback(() => {
    setMouseMode(defaultMouseMode);
  }, [defaultMouseMode]);

  const setOrResetMouseMode = useCallback(
    (newMouseMode: T | DefaultMouseMode) => {
      if (newMouseMode !== mouseMode) {
        setMouseMode(newMouseMode);
      } else {
        setMouseMode(defaultMouseMode);
      }
    },
    [mouseMode, defaultMouseMode, setMouseMode]
  );

  return {
    mouseMode,
    setMouseMode,
    resetMouseMode,
    setOrResetMouseMode,
    defaultMouseMode,
    setDefaultMouseMode,
  };
}
