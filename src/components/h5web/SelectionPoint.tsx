import type { CanvasEvent } from '@h5web/lib';
import { useCanvasEvents, useVisCanvasContext } from '@h5web/lib';
import { useCallback, useState } from 'react';
import type { Vector3 } from 'three';

interface Props {
  onClick: (evt: CanvasEvent<PointerEvent>) => void;
  threshold?: number;
}

function PointerClick(props: Props) {
  const { onClick, threshold = 4 } = props;

  const { worldToData } = useVisCanvasContext();

  const [startPoint, setStartPoint] = useState<Vector3 | null>(null);

  const onPointerDown = useCallback(
    (event: CanvasEvent<PointerEvent>) => {
      const { htmlPt } = event;
      setStartPoint(htmlPt);
    },
    [setStartPoint]
  );

  const isCloseToStart = useCallback(
    (evt: CanvasEvent<PointerEvent>) => {
      if (!startPoint) {
        return false;
      }
      const { htmlPt } = evt;
      const dist = Math.max(
        Math.abs(htmlPt.x - startPoint.x),
        Math.abs(htmlPt.y - startPoint.y)
      );
      return dist <= threshold;
    },
    [startPoint, threshold]
  );

  const onPointerMove = useCallback(
    (evt: CanvasEvent<PointerEvent>) => {
      if (!isCloseToStart(evt)) {
        setStartPoint(null);
      }
    },
    [isCloseToStart, setStartPoint]
  );

  const onPointerUp = useCallback(
    (evt: CanvasEvent<PointerEvent>) => {
      if (!isCloseToStart(evt)) {
        return;
      }
      onClick(evt);
    },
    [isCloseToStart, onClick, onClick, worldToData]
  );

  useCanvasEvents({ onPointerDown, onPointerMove, onPointerUp });

  return null;
}

export type { Props as PointerClickProps };
export default PointerClick;
