import { Vector3 } from 'three';
import type { Domain } from '@h5web/lib';
import { useCameraState, useVisCanvasContext } from '@h5web/lib';

/**
 * True if a number is inside a domain.
 */
export function isInside(v: number, range: Domain) {
  if (range[0] < range[1]) {
    return range[0] <= v && v <= range[1];
  }
  return range[1] <= v && v <= range[0];
}

/**
 * True if a data coord is inside a x/y domain.
 */
export function isCoordVisible(
  coord: Vector3,
  visibleDomain: {
    xVisibleDomain: Domain;
    yVisibleDomain: Domain;
  }
) {
  return (
    isInside(coord.x, visibleDomain.xVisibleDomain) &&
    isInside(coord.y, visibleDomain.yVisibleDomain)
  );
}

/**
 * Returns displayed position and visibility from data coords.
 */
export function useCoordState(x: number | null, y: number | null) {
  const coord = x === null || y === null ? null : new Vector3(x, y);

  const { dataToHtml, getVisibleDomains } = useVisCanvasContext();

  return useCameraState(
    (camera) => {
      if (coord === null) {
        return null;
      }
      const visibleDomain = getVisibleDomains(camera);
      return {
        isVisible: isCoordVisible(coord, visibleDomain),
        displayedCoords: dataToHtml(camera, coord),
      };
    },
    [x, y]
  );
}
