import { MouseButton, Pan, SelectToZoom, Zoom } from '@h5web/lib';

/**
 * Define the default interaction which have to be provided for every plots
 *
 * There is 2 modes: `pan` and `zoom`:
 *
 * - In zoom mode
 *     - Left button: zoom on select region
 *     - Middle button: pan
 * - In pan mode
 *     - Left button: pan
 *     - Middle button: pan
 * - Without mode
 *     - Middle button: pan
 */
export default function DefaultMouseInteraction(props: {
  zoomMode: boolean;
  panMode: boolean;
  disabled: boolean;
}) {
  if (props.zoomMode) {
    return (
      <>
        <Pan disabled={props.disabled} button={MouseButton.Middle} />
        <SelectToZoom disabled={props.disabled} />
        <Zoom />
      </>
    );
  }
  if (props.panMode) {
    return (
      <>
        <Pan
          disabled={props.disabled}
          button={[MouseButton.Left, MouseButton.Middle]}
        />
        <Zoom />
      </>
    );
  }
  return (
    <>
      <Pan disabled={props.disabled} button={MouseButton.Middle} />
      <Zoom />
    </>
  );
}
