import type { PropsWithChildren, RefObject } from 'react';
import { useLayoutEffect } from 'react';
import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { useImperativeHandle } from 'react';
import type { Aspect, AxisConfig } from '@h5web/lib';
import { VisCanvas } from '@h5web/lib';
import type { VisSynchroniserRef } from './VisSynchroniser';
import VisSynchroniser from './VisSynchroniser';
import { MouseInteractionOverlayManager } from './MouseInteractionOverlayManager';
import DefaultMouseInteraction from './DefaultMouseInteraction';
import { LinearVisGroup } from './LinearVisGroup';
import type { UseUndoableViewpoint } from './hooks';
import { Vector3 } from 'three';
import { debounce } from 'lodash';
import type { DefaultMouseMode } from './UseMouseModeInteraction';

/**
 * Properties provided by the component
 */
interface Props<MouseMode> {
  className?: string;
  title?: string;
  aspect?: Aspect;
  abscissaConfig: AxisConfig;
  ordinateConfig: AxisConfig;
  raycasterThreshold?: number;
  showAxes?: boolean;
  mouseMode?: MouseMode | DefaultMouseMode;
  onMouseInteractionOverlayChanged?: (uid: string | undefined) => void;
  onKeyDown?: (event: KeyboardEvent) => void;
  tabIndex?: number;
  undoableViewpoint?: UseUndoableViewpoint;
  // Cursor to be used per mouse mode
  // FIXME: Use a subset of MouseMode when ever it is possible
  cursors?: Record<string, string>;
}

/**
 * Actions exposed by the context and the imperative API
 */
interface LinearVisCanvasActions {
  // Wait until the viewport and history can be cleared
  clearLater: () => void;
  // Reset the zoom according to it's content
  resetZoom: () => void;
  // True if the zoom can be reset
  isZoomed: () => boolean;
  // Set a custom scale to the viewpoint
  setScale: (scale: number) => void;
  // Set a dedicated mouse cursor, until `undefined` is set
  setMouseInteractionCursor: (cursor: string | undefined) => void;
  // Inform the component that the mouse is about to be used
  captureMouseInteraction: () => void;
  // Inform the component that the mouse is released
  releaseMouseInteraction: () => void;
}

/**
 * Inperative API exposed by the component reference
 */
interface LinearVisCanvasRef {
  actions: LinearVisCanvasActions;
  // Object holding overlay interaction
  mouseInteractionOverlay: MouseInteractionOverlayManager;
}

/**
 * Context exposed by the component
 */
interface LinearVisCanvasContext {
  actions: LinearVisCanvasActions;
  // Object holding overlay interaction
  mouseInteractionOverlay: MouseInteractionOverlayManager;
  // Mouse cursor for overlay. If defined, the actual mouse mode is overload by an overlay interaction
  overlayCursor: string | undefined;
  // Actual mouse mode
  mouseMode: string;
}

const LinearVisCanvasContextCmp = createContext<LinearVisCanvasContext | null>(
  null
);

export function useLinearVisCanvas(): LinearVisCanvasContext | undefined {
  try {
    const context = useContext(LinearVisCanvasContextCmp);
    if (context === null) {
      console.error('LinearVisCanvasContext not found');
      return undefined;
    }
    return context;
  } catch {
    console.error('LinearVisCanvasContext not found');
    return undefined;
  }
}

export const useDebouncedEffect = (
  effect: () => void,
  deps: any[],
  delay: number
) => {
  useEffect(() => {
    const handler = setTimeout(() => effect(), delay);

    return () => clearTimeout(handler);
  }, [...(deps || []), delay]);
};

export function LinearVisCanvas<MouseMode extends string>(
  props: PropsWithChildren<Props<MouseMode>> & {
    plotRef?: RefObject<LinearVisCanvasRef>;
  }
) {
  const {
    className,
    title,
    children,
    mouseMode = 'zoom',
    onMouseInteractionOverlayChanged,
    plotRef,
    showAxes,
    abscissaConfig,
    ordinateConfig,
    ...rest
  } = props;
  const cursors: Record<string, string> = {
    zoom: 'crosshair',
    pan: 'all-scroll',
    ...props.cursors,
  };
  const syncRef = useRef<VisSynchroniserRef>(null);

  const [mouseInteractionCursor, setMouseInteractionCursor] = useState<
    string | undefined
  >();
  const [overlayCursor, setOverlayCursor] = useState<string | undefined>();

  useEffect(() => {
    const cursor = cursors[mouseMode];
    setMouseInteractionCursor(cursor);
  }, [cursors, mouseMode]);

  const onMouseInteractionOverlayChangedRef = useCallback(
    (uid: string | undefined) => {
      onMouseInteractionOverlayChanged?.(uid);
    },
    [onMouseInteractionOverlayChanged]
  );

  const mouseInteractionOverlayRef = useRef(
    new MouseInteractionOverlayManager(
      onMouseInteractionOverlayChangedRef,
      setOverlayCursor
    )
  );

  const cursor = useMemo(() => {
    /**
     * Normalize the cursor according to the flip of the plot
     */
    function getFinalOverlayCursor() {
      const flip = abscissaConfig.flip !== ordinateConfig.flip;
      if (!flip) {
        return overlayCursor;
      }
      if (overlayCursor === undefined) {
        return overlayCursor;
      }
      const flipped: Record<string, string> = {
        'nesw-resize': 'nwse-resize',
        'nwse-resize': 'nesw-resize',
      };
      return flipped[overlayCursor] ?? overlayCursor;
    }

    const finalOverlayCursor = getFinalOverlayCursor();

    return finalOverlayCursor ?? mouseInteractionCursor ?? 'default';
  }, [
    mouseInteractionCursor,
    overlayCursor,
    abscissaConfig.flip,
    ordinateConfig.flip,
  ]);

  const [undoableState, undoableSetState, undoableActions] =
    props.undoableViewpoint ?? [undefined, undefined, undefined];

  const [mustClear, setMustClear] = useState(false);

  useLayoutEffect(() => {
    if (!undoableSetState) {
      return;
    }
    if (!syncRef.current || !mustClear) {
      return;
    }
    syncRef.current.resetZoom();
    const { center, scale } = syncRef.current.getCameraViewpoint();

    // This combination reset/resetInitialState/reset works
    // I have no idea why
    undoableActions?.reset();
    undoableActions?.resetInitialState({
      centerX: center.x,
      centerY: center.y,
      scale: scale.x,
    });
    undoableActions?.reset();

    setMustClear(false);
  }, [syncRef.current, mustClear]);

  const clearLater = useCallback(() => {
    setMustClear(true);
  }, []);

  const resetZoom = useCallback(() => {
    if (!syncRef.current) {
      return;
    }
    syncRef.current.resetZoom();
  }, []);

  const isZoomed = useCallback(() => {
    if (!syncRef.current) {
      return false;
    }
    return syncRef.current.isZoomed();
  }, []);

  const setScale = useCallback((scale: number) => {
    if (!syncRef.current) {
      return;
    }
    syncRef.current.setScale(scale);
  }, []);

  const captureMouseInteraction = useCallback(() => {
    mouseInteractionOverlayRef.current.setDisabled(true);
  }, []);

  const releaseMouseInteraction = useCallback(() => {
    mouseInteractionOverlayRef.current.setDisabled(false);
  }, []);

  const actions = useRef<LinearVisCanvasActions>({
    clearLater,
    resetZoom,
    isZoomed,
    setScale,
    setMouseInteractionCursor,
    captureMouseInteraction,
    releaseMouseInteraction,
  });

  const refFigure = useRef<HTMLElement>(null);

  React.useEffect(() => {
    if (props.onKeyDown === undefined || refFigure.current === null) {
      return () => {};
    }

    function wrapped(event: Event) {
      if (event instanceof KeyboardEvent) {
        props.onKeyDown?.(event);
      }
    }

    refFigure.current.addEventListener('keydown', wrapped);
    return () => {
      if (refFigure.current !== null) {
        refFigure.current.removeEventListener('keydown', wrapped);
      }
    };
  }, [refFigure.current, props.onKeyDown]);

  useEffect(() => {
    // FIXME: Would be good to have a real accessor
    const canvas = refFigure.current?.children[0].children[0].children[0];
    if (canvas === undefined || !(canvas instanceof HTMLElement)) {
      console.error(
        'h5web canvas not reachable. This h5web version is maybe not supported'
      );
      return;
    }
    // Set the canvas cursor only so what it is not used over the axes
    canvas.style.cursor = cursor;
  }, [cursor]);

  useImperativeHandle(
    plotRef,
    () => {
      return {
        actions: actions.current,
        mouseInteractionOverlay: mouseInteractionOverlayRef.current,
      };
    },
    []
  );

  const viewportRef = useRef<{
    userInteraction: boolean;
    ignoreNextCallback: boolean;
  }>({
    userInteraction: false,
    ignoreNextCallback: false,
  });

  // This have to be debounced as workaround cause it can be called twice
  const debouncedUndoableSetState = useCallback(
    debounce(
      (event: { center: Vector3; scale: Vector3 }) => {
        if (undoableSetState !== undefined) {
          viewportRef.current.userInteraction = true;
          undoableSetState({
            centerX: event.center.x,
            centerY: event.center.y,
            scale: event.scale.x,
          });
          viewportRef.current.userInteraction = false;
        }
      },
      500,
      { maxWait: 3000, trailing: true }
    ),
    []
  );

  const onVisViewpointChange = useCallback(
    (center: Vector3, scale: Vector3) => {
      if (viewportRef.current.ignoreNextCallback) {
        viewportRef.current.ignoreNextCallback = false;
        return;
      }
      debouncedUndoableSetState({ center, scale });
    },
    [debouncedUndoableSetState]
  );

  useEffect(() => {
    if (viewportRef.current.userInteraction) {
      // During a user interaction we dont need to resync
      // This avoid reentrant events
      return;
    }
    if (undoableState !== undefined) {
      const { centerX, centerY, scale } = undoableState;
      syncRef.current?.updateCameraViewpoint({
        center: new Vector3(centerX, centerY, 0),
        scale: new Vector3(scale, scale, 1),
      });
      viewportRef.current.ignoreNextCallback = true;
    }
  }, [undoableState]);

  return (
    <figure
      ref={refFigure}
      className={`linearviscanvas ${showAxes ? 'show-axes' : ''} ${className}`}
      tabIndex={props.tabIndex}
      aria-label={title}
      data-keep-canvas-colors
    >
      <VisCanvas
        title={title}
        showAxes={showAxes}
        abscissaConfig={abscissaConfig}
        ordinateConfig={ordinateConfig}
        {...rest}
      >
        <LinearVisCanvasContextCmp.Provider
          value={{
            overlayCursor,
            mouseMode,
            mouseInteractionOverlay: mouseInteractionOverlayRef.current,
            actions: actions.current,
          }}
        >
          <VisSynchroniser
            ref={syncRef}
            onVisViewpointChange={onVisViewpointChange}
          />
          <DefaultMouseInteraction
            zoomMode={mouseMode === 'zoom'}
            panMode={mouseMode === 'pan'}
            disabled={
              overlayCursor !== undefined ||
              (mouseMode !== 'pan' && mouseMode !== 'zoom')
            }
          />
          <LinearVisGroup>{children}</LinearVisGroup>
        </LinearVisCanvasContextCmp.Provider>
      </VisCanvas>
    </figure>
  );
}

export type { Props as VisSynchroniserProps, LinearVisCanvasRef };
