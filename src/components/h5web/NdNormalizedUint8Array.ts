import { ScaleType } from '@h5web/lib';
import type { AutoscaleMode } from './colormap';

interface NdNormalizedUint8ArrayTransfer {
  // Scale used between min and max
  normalization: ScaleType;
  // Informative kind of scale which was used to choose min and max
  autoscale: AutoscaleMode;
  // Min data value from the transfer function
  min: number;
  // Max data value from the transfer function
  max: number;
}

/**
 * NdArray stored as a Uint8Array normalized with a transfer function.
 */
export class NdNormalizedUint8Array {
  public readonly shape: number[];

  public readonly dimension: number;

  public readonly data: Uint8Array;

  public readonly dtype: string;

  public readonly transfer: NdNormalizedUint8ArrayTransfer;

  public constructor(
    data: Uint8Array,
    shape: number[],
    transfer: NdNormalizedUint8ArrayTransfer
  ) {
    if (shape.length !== 2) {
      throw new Error(
        `Unsupported dimension ${shape.length} with NdNormalizedUint8Array`
      );
    }
    this.data = data;
    this.transfer = transfer;
    this.shape = shape;
    this.dimension = shape.length;
    this.dtype = 'uint8';
  }

  public index(...args: number[]): number {
    if (args.length !== this.dimension) {
      throw new Error(`Expected ${this.dimension} for indexing`);
    }
    return args[1] + args[0] * this.shape[1];
  }

  public getRaw(...args: number[]): number {
    const index = this.index(...args);
    return this.data[index];
  }

  public get(...args: number[]): number {
    const normv = this.getRaw(...args);
    const { min, max, normalization } = this.transfer;
    const v = (normv * (max - min)) / 255 + min;
    switch (normalization) {
      case ScaleType.Linear:
        return v;
      case ScaleType.Log:
        return Math.exp(v);
      case ScaleType.Sqrt:
        return v ** 2;
      default:
        return Number.NaN;
    }
  }
}
