import React, { useRef } from 'react';
import { ScaleType, useVisCanvasContext } from '@h5web/lib';
import type { PropsWithChildren } from 'react';

import { createContext, useContext } from 'react';
import { Matrix4 } from 'three';
import { useFrame } from '@react-three/fiber';

interface Props {}

interface ILinearVisGroupContext {
  sx: number;
  sy: number;
  centerX: number;
  centerY: number;
  matrix: Matrix4;
}

const defaultLinearVisGroupContext: ILinearVisGroupContext = {
  sx: 0,
  sy: 0,
  centerX: 0,
  centerY: 0,
  matrix: new Matrix4().identity(),
};

const LinearVisGroupContext =
  createContext<React.MutableRefObject<ILinearVisGroupContext> | null>(null);

export function useLinearVisGroup(): ILinearVisGroupContext {
  const context = useContext(LinearVisGroupContext);
  if (!context) {
    throw new Error('No LinearVisGroup context are avaialble');
  }
  return context.current;
}

export function LinearVisGroup(props: PropsWithChildren<Props>) {
  const state = useRef<ILinearVisGroupContext>(defaultLinearVisGroupContext);
  const { children } = props;
  const { abscissaConfig, ordinateConfig, visSize } = useVisCanvasContext();
  const { width, height } = visSize;
  const { scaleType: abscissaScaleType = ScaleType.Linear } = abscissaConfig;
  const { scaleType: ordinateScaleType = ScaleType.Linear } = ordinateConfig;

  if (
    abscissaScaleType !== ScaleType.Linear ||
    ordinateScaleType !== ScaleType.Linear
  ) {
    throw new Error('VisGroup supports only linear axes');
  }

  useFrame(({ camera }) => {
    const sx =
      ((abscissaConfig.flip ? -1 : 1) * width) /
      (abscissaConfig.visDomain[1] - abscissaConfig.visDomain[0]);
    const sy =
      ((ordinateConfig.flip ? -1 : 1) * height) /
      (ordinateConfig.visDomain[1] - ordinateConfig.visDomain[0]);
    const centerX =
      0.5 * (abscissaConfig.visDomain[0] + abscissaConfig.visDomain[1]);
    const centerY =
      0.5 * (ordinateConfig.visDomain[0] + ordinateConfig.visDomain[1]);

    state.current.sx = sx;
    state.current.sy = sy;
    state.current.centerX = centerX;
    state.current.centerY = centerY;
    state.current.matrix = state.current.matrix.makeScale(sx, sy, 0);
  }, -100);

  const sx =
    ((abscissaConfig.flip ? -1 : 1) * width) /
    (abscissaConfig.visDomain[1] - abscissaConfig.visDomain[0]);
  const sy =
    ((ordinateConfig.flip ? -1 : 1) * height) /
    (ordinateConfig.visDomain[1] - ordinateConfig.visDomain[0]);
  const centerX =
    0.5 * (abscissaConfig.visDomain[0] + abscissaConfig.visDomain[1]);
  const centerY =
    0.5 * (ordinateConfig.visDomain[0] + ordinateConfig.visDomain[1]);

  state.current.sx = sx;
  state.current.sy = sy;
  state.current.centerX = centerX;
  state.current.centerY = centerY;
  state.current.matrix = state.current.matrix.makeScale(sx, sy, 0);

  return (
    <LinearVisGroupContext.Provider value={state}>
      <group position={[-centerX * sx, -centerY * sy, 0]} scale={[sx, sy, 1]}>
        {children}
      </group>
    </LinearVisGroupContext.Provider>
  );
}

export type { Props as LinearVisGroupProps };
