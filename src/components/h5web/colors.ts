import { useMemo } from 'react';
import { Color, Vector4 } from 'three';

/**
 * Color factory supporting transparent channel.
 *
 * Based on https ://github.com/mrdoob/three.js/blob/master/src/math/Color.js|src/math/Color.js
 *
 * @param name: Color from three plus 'transparent' and '#808080??'.
 * @param opacity: A number between 0..1
 */
export function color4(
  name: Color | string | number,
  opacity?: number
): Vector4 {
  if (typeof name === 'string') {
    if (name === '') {
      return new Vector4(0, 0, 0, 0);
    }
    if (name === 'transparent') {
      return new Vector4(0, 0, 0, 0);
    }
    if (name.startsWith('#') && name.length === 9) {
      const r = Number(`0x${name.slice(1, 3)}`) / 255;
      const g = Number(`0x${name.slice(3, 5)}`) / 255;
      const b = Number(`0x${name.slice(5, 7)}`) / 255;
      const a = Number(`0x${name.slice(7, 9)}`) / 255;
      return new Vector4(r, g, b, a * (opacity ?? 1));
    }
  }
  const c = new Color(name);
  return new Vector4(c.r, c.g, c.b, opacity ?? 1);
}

/**
 * Hook to memo a color factory supporting transparent channel.
 *
 * @param name: Color from three plus 'transparent' and '#808080xx'.
 * @param opacity: A number between 0..1
 */
export function useColor4(
  name: Color | string | number,
  opacity?: number
): Vector4 {
  return useMemo(() => {
    return color4(name, opacity);
  }, [name, opacity]);
}
