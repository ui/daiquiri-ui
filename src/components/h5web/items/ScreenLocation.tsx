import type {
  ForwardRefExoticComponent,
  PropsWithoutRef,
  RefAttributes,
} from 'react';
import * as React from 'react';
import type { Group } from 'three';
import { useFrame } from '@react-three/fiber';
import { useVisCanvasContext } from '@h5web/lib';

type ForwardRefComponent<P, T> = ForwardRefExoticComponent<
  PropsWithoutRef<P> & RefAttributes<T>
>;

export type ScreenLocationProps = {
  percentLocation: [number, number];
} & JSX.IntrinsicElements['group'];

export const ScreenLocation: ForwardRefComponent<ScreenLocationProps, Group> =
  /* @__PURE__ */ React.forwardRef<Group, ScreenLocationProps>(
    ({ children, percentLocation }, ref) => {
      const localRef = React.useRef<Group>(null);

      // @ts-expect-error
      React.useImperativeHandle(ref, () => localRef.current, []);

      const visCanvasContext = useVisCanvasContext();

      useFrame(({ camera }) => {
        const group = localRef.current;
        if (!group) {
          return;
        }
        const visibleDomain = visCanvasContext.getVisibleDomains(camera);

        const x =
          visibleDomain.xVisibleDomain[0] +
          (visibleDomain.xVisibleDomain[1] - visibleDomain.xVisibleDomain[0]) *
            (percentLocation[0] / 100);
        const y =
          visibleDomain.yVisibleDomain[0] +
          (visibleDomain.yVisibleDomain[1] - visibleDomain.yVisibleDomain[0]) *
            (1 - percentLocation[1] / 100);
        group.position.x = x;
        group.position.y = y;
      });

      return (
        <group ref={localRef}>
          <group>{children}</group>
        </group>
      );
    }
  );
