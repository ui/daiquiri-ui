import type {
  ForwardRefExoticComponent,
  PropsWithoutRef,
  RefAttributes,
} from 'react';
import * as React from 'react';
import type { Group } from 'three';
import { useFrame } from '@react-three/fiber';
import { useLinearVisGroup } from '../LinearVisGroup';

type ForwardRefComponent<P, T> = ForwardRefExoticComponent<
  PropsWithoutRef<P> & RefAttributes<T>
>;

type ScreenScaleProps = {
  scaleX?: boolean;
  scaleY?: boolean;
  // If true, the default, the screen direction is conserved (top-down, left-right)
  screenDirection?: boolean;
} & JSX.IntrinsicElements['group'];

/**
 * Change the scale to a screen scale, in pixel.
 *
 * X direction is left-right, Y direction is top-down.
 */
export const ScreenScale: ForwardRefComponent<ScreenScaleProps, Group> =
  React.forwardRef<Group, ScreenScaleProps>((props, ref) => {
    const {
      scaleX = true,
      scaleY = true,
      screenDirection = true,
      children,
    } = props;
    const localRef = React.useRef<Group>(null);

    // @ts-expect-error
    React.useImperativeHandle(ref, () => localRef.current, []);

    const linearVisGroup = useLinearVisGroup();

    useFrame(({ camera }) => {
      const group = localRef.current;
      if (!group) {
        return;
      }
      const sx = camera.scale.x / linearVisGroup.sx;
      const sy = camera.scale.y / linearVisGroup.sy;

      if (scaleX) {
        group.scale.x = screenDirection ? sx : Math.abs(sx);
      }
      if (scaleY) {
        group.scale.y = screenDirection ? -sy : Math.abs(sy);
      }
    });

    return <group ref={localRef}>{children}</group>;
  });
