import { useEffect } from 'react';
import { Alert } from 'react-bootstrap';
import { Overlay } from '@h5web/lib';
import { useDebouncedState } from '@react-hookz/web';

export function LoadingMessage(props: {
  loading?: boolean;
  warning?: string;
  danger?: string;
  info?: string;
}) {
  const [state, setState] = useDebouncedState(
    {
      loading: props.loading,
      info: props.info,
      warning: props.warning,
      danger: props.danger,
    },
    500
  );

  useEffect(() => {
    setState({
      loading: props.loading,
      warning: props.warning,
      danger: props.danger,
      info: props.info,
    });
  }, [props.loading, props.info, props.warning, props.danger]);

  if (!state.loading && !state.danger && !state.warning && !state.info) {
    return <></>;
  }

  return (
    <Overlay style={{ zIndex: 200 }}>
      <div style={{ textAlign: 'center' }}>
        {state.loading && (
          <Alert
            variant="warning"
            style={{
              marginLeft: 'auto',
              marginRight: 'auto',
              marginTop: '2em',
              display: 'inline-block',
            }}
          >
            <i className="fa fa-spinner fa-solid fa-pulse" /> Loading
          </Alert>
        )}
        {!state.loading && state.danger && (
          <Alert
            variant="danger"
            style={{
              marginLeft: 'auto',
              marginRight: 'auto',
              marginTop: '2em',
              display: 'inline-block',
            }}
          >
            {state.danger}
          </Alert>
        )}
        {!state.loading && !state.danger && state.warning && (
          <Alert
            variant="warning"
            style={{
              marginLeft: 'auto',
              marginRight: 'auto',
              marginTop: '2em',
              display: 'inline-block',
            }}
          >
            {state.warning}
          </Alert>
        )}
        {state.info && (
          <Alert
            variant="info"
            style={{
              marginLeft: 'auto',
              marginRight: 'auto',
              marginTop: '2em',
              display: 'inline-block',
            }}
          >
            {state.info}
          </Alert>
        )}
      </div>
    </Overlay>
  );
}
