import { Annotation } from '@h5web/lib';
import type { Vector2 } from 'three';
import { asVector2 } from '../utils';
import { useMemo } from 'react';

function colorHexToRgb(hex: string): [number, number, number] | null {
  const result = /^#([\da-f]{2})([\da-f]{2})([\da-f]{2})$/gis.exec(hex);
  if (result === null) {
    return null;
  }
  return [
    Number.parseInt(result[1], 16),
    Number.parseInt(result[2], 16),
    Number.parseInt(result[3], 16),
  ];
}

function computeOutlineStroke(color: string) {
  const rgb = colorHexToRgb(color);
  if (rgb === null) {
    return undefined;
  }
  const grey = 0.299 * rgb[0] + 0.587 * rgb[1] + 0.114 * rgb[2];
  if (grey > 128) {
    return '#00000080';
  }
  return '#FFFFFF80';
}

/**
 * Display a label at a data position in the plot.
 *
 * An anchor can be defined. It is the point relative to the label which
 * will be placed at the specified data position.
 *
 * The anchor can be one of:
 *
 * - `text-right`: To put the anchor at the right of the bottom of the text
 * - `bottom`: The anchor is located at the bottom center of the text box
 * - `top-right`: The anchor is located at top-right of the text box
 * - `center`: The anchor is located at center of the text box
 */
export default function Label(props: {
  text: string;
  color?: string;
  datapos: [number, number] | Vector2;
  outline?: string;
  vmargin?: number;
  hmargin?: number;
  anchor?: 'text-right' | 'center' | 'bottom' | 'top' | 'top-right';
}) {
  const {
    vmargin = 5,
    hmargin = 5,
    anchor = 'text-right',
    color = 'black',
  } = props;

  const outline = useMemo(() => {
    return props.outline ?? computeOutlineStroke(color);
  }, [props.outline]);
  const datapos = asVector2(props.datapos);

  function createBaseStyle(): React.CSSProperties {
    switch (anchor) {
      case 'bottom':
        return {
          top: '50%',
          left: '50%',
          position: 'relative',
          transform: 'translate(-100%, -100%)',
        };
      case 'top':
        return {
          top: '50%',
          left: '50%',
          position: 'relative',
          transform: 'translate(-100%, 0%)',
        };
      case 'top-right':
        return { marginLeft: hmargin, marginTop: vmargin };
      case 'text-right':
        return {
          top: '50%',
          position: 'relative',
          transform: 'translate(0%, -100%)',
          marginLeft: hmargin,
          marginTop: vmargin,
        };
      case 'center':
        return {
          top: '50%',
          left: '50%',
          position: 'relative',
          transform: 'translate(-100%, -50%)',
        };
    }
    return {};
  }
  const baseStyle = createBaseStyle();

  return (
    <Annotation x={datapos.x} y={datapos.y} style={{ color }}>
      <div
        style={{
          ...baseStyle,
          borderRadius: 5,
          paddingLeft: 5,
          paddingRight: 5,
          backgroundColor: outline,
          fontWeight: 'bold',
          fontSize: '100%',
          zIndex: 100,
        }}
      >
        {props.text}
      </div>
    </Annotation>
  );
}
