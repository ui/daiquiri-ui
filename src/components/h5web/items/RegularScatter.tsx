import { useMemo } from 'react';
import { ScatterPoints } from '@h5web/lib';
import { HeatmapMesh, ScaleType } from '@h5web/lib';
import type { NdArray, TypedArray } from 'ndarray';
import ndarray from 'ndarray';
import type { ColorMap, Domain } from '@h5web/lib';
import { WorldScale } from './WorldScale';

function useAsTextureSafeTypedArray(array: NdArray<TypedArray> | undefined) {
  return useMemo(() => {
    if (array === undefined) {
      return undefined;
    }
    const normArray =
      array instanceof Float32Array ? array : Float32Array.from(array.data);
    return ndarray(normArray, array.shape);
  }, [array]);
}

/**
 * Regular scatter item.
 *
 * It's basically a scatter that can be displayed like an image.
 * The indexes follow the same structure as an image.
 *
 * `xData` amd `yData` are just ignored.
 *
 * FIXME: This could be generalized with Y as fast axis.
 */
function SolidRegularScatter(props: {
  xData?: NdArray<TypedArray>;
  yData?: NdArray<TypedArray>;
  vData?: NdArray<TypedArray>;
  xRange: [number, number];
  // Number of point expected in the x-axis
  xAxisSize: number;
  yRange: [number, number];
  // Number of point expected in the y-axis
  yAxisSize: number;
  domain: Domain;
  colorMap: ColorMap;
  invertColorMap: boolean;
  scaleType: ScaleType;
}) {
  const {
    vData,
    xRange,
    xAxisSize,
    yRange,
    yAxisSize,
    colorMap,
    scaleType,
    invertColorMap,
    domain,
  } = props;

  const yAxisActualSize = useMemo(() => {
    const nb = vData?.shape[0] ?? 0;
    return (nb - (nb % xAxisSize)) / xAxisSize;
  }, [vData, xAxisSize]);

  const imageArray = useMemo(() => {
    if (vData === undefined) {
      return undefined;
    }
    if (yAxisActualSize === 0) {
      return undefined;
    }
    const clampedArray = vData.data.subarray(0, yAxisActualSize * xAxisSize);
    return ndarray(clampedArray, [yAxisActualSize, xAxisSize]);
  }, [yAxisActualSize, xAxisSize]);

  const textureArray = useAsTextureSafeTypedArray(imageArray);

  if (textureArray === undefined) {
    return <></>;
  }

  const yLength = Math.abs(yRange[1] - yRange[0]);
  const xLength = Math.abs(xRange[1] - xRange[0]);

  const psx = xLength / xAxisSize;
  const psy = yLength / yAxisSize;

  return (
    <HeatmapMesh
      position={[
        (xRange[0] + xRange[1]) * 0.5,
        yRange[0] + yLength * (textureArray.shape[0] / yAxisSize) * 0.5,
        0,
      ]}
      size={{ width: textureArray.shape[1], height: textureArray.shape[0] }}
      scale={[psx, psy, 1]}
      values={textureArray}
      domain={domain}
      colorMap={colorMap}
      scaleType={scaleType}
      invertColorMap={invertColorMap}
    />
  );
}

/**
 * Regular scatter item.
 *
 * It's basically a scatter that can be displayed like an image.
 * The indexes follow the same structure as an image.
 *
 * `xData` amd `yData` are just ignored.
 *
 * FIXME: This could be generalized with Y as fast axis.
 */
export function RegularScatter(props: {
  xData?: NdArray<TypedArray>;
  yData?: NdArray<TypedArray>;
  vData?: NdArray<TypedArray>;
  xRange: [number, number];
  // Number of point expected in the x-axis
  xAxisSize: number;
  yRange: [number, number];
  // Number of point expected in the y-axis
  yAxisSize: number;
  domain: Domain;
  colorMap?: ColorMap;
  invertColorMap?: boolean;
  scaleType?: ScaleType;
  markerSize?: number;
  renderMode: 'solid' | 'marker';
}) {
  const {
    xData,
    yData,
    vData,
    domain,
    colorMap = 'Viridis',
    invertColorMap = false,
    scaleType = ScaleType.Linear,
    markerSize = 8,
    renderMode,
    ...others
  } = props;

  if (xData === undefined || yData === undefined || vData === undefined) {
    return <></>;
  }

  if (renderMode === 'marker') {
    return (
      <WorldScale>
        <ScatterPoints
          abscissas={xData.data}
          ordinates={yData.data}
          data={vData.data}
          size={markerSize}
          domain={domain}
          scaleType={scaleType}
          invertColorMap={false}
          colorMap={colorMap}
        />
      </WorldScale>
    );
  }

  return (
    <SolidRegularScatter
      xData={xData}
      yData={yData}
      vData={vData}
      domain={domain}
      scaleType={scaleType}
      invertColorMap={false}
      colorMap={colorMap}
      {...others}
    />
  );
}
