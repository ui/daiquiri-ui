import type {
  ForwardRefExoticComponent,
  PropsWithoutRef,
  RefAttributes,
} from 'react';
import * as React from 'react';
import type { Group } from 'three';
import { useFrame } from '@react-three/fiber';
import { useLinearVisGroup } from '../LinearVisGroup';

type ForwardRefComponent<P, T> = ForwardRefExoticComponent<
  PropsWithoutRef<P> & RefAttributes<T>
>;

type Props = {
  scaleX?: boolean;
  scaleY?: boolean;
  // If true, the default, the screen direction is conserved (top-down, left-right)
  screenDirection?: boolean;
} & JSX.IntrinsicElements['group'];

/**
 * Change the scale to a screen scale, in pixel.
 *
 * X direction is left-right, Y direction is top-down.
 */
export const WorldScale: ForwardRefComponent<Props, Group> = React.forwardRef<
  Group,
  Props
>((props, ref) => {
  const { children } = props;
  const localRef = React.useRef<Group>(null);

  // @ts-expect-error
  React.useImperativeHandle(ref, () => localRef.current, []);

  const linearVisGroup = useLinearVisGroup();

  useFrame(({ camera }) => {
    const group = localRef.current;
    if (!group) {
      return;
    }
    group.position.x = linearVisGroup.centerX;
    group.position.y = linearVisGroup.centerY;
    group.scale.x = 1 / linearVisGroup.sx;
    group.scale.y = 1 / linearVisGroup.sy;
  });

  return <group ref={localRef}>{children}</group>;
});
