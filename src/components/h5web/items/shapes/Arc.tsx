import { extend, invalidate, useFrame } from '@react-three/fiber';
import React, { useEffect, useRef } from 'react';
import { ShaderMaterial, Vector4 } from 'three';
import { useLinearVisGroup } from '../../LinearVisGroup';
import { color4 } from '../../colors';

class ArcDashMaterial extends ShaderMaterial {
  public constructor() {
    super({
      uniforms: {
        color: { value: new Vector4() },
        gapColor: { value: new Vector4() },
        radius: { value: 0 },
        startAngle: { value: 0 },
        angleRange: { value: 0 },
        dashSize: { value: 0 },
        gapSize: { value: 0 },
        scaleX: { value: 0 },
        scaleY: { value: 0 },
        lineWidth: { value: 0 },
      },
      vertexShader: `
uniform float scaleX; // signed scale
uniform float scaleY; // signed scale
out vec2 pixelCoord;
// out vec2 dataCoord;

void main() {
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
    vec2 pixelScale = abs(vec2(scaleX, scaleY));
    // dataCoord = vec2(position.x, position.y);
    pixelCoord = vec2(position.x, position.y) / pixelScale;
}
      `,
      fragmentShader: `
uniform vec4 color;
uniform vec4 gapColor;
uniform float gapSize;
uniform float dashSize;
uniform float lineWidth;
uniform float scaleX; // signed scale
uniform float scaleY; // signed scale
uniform float radius;
uniform float startAngle;
uniform float angleRange;
in vec2 pixelCoord;

const float M_PI = 3.1415926535897932384626433832795;
const float AA_DASH = 0.5;

/**
 * Antialiasing following the line of dash and gap.
 *
 * The distances have to be in pixel.
 */
vec4 aa_dash_gap(float dist, float alpha, float dashSize, float gapSize) {
  dist = mod(dist, (gapSize + dashSize));
  float sd1 = (dist < dashSize) ? dist : dist - (dashSize + gapSize);
  float sd2 = -(dist - dashSize);
  if (abs(sd1) <= AA_DASH || abs(sd2) <= AA_DASH) {
    if (abs(sd1) < abs(sd2)) {
      float c = (sd1 + AA_DASH) * (0.5 / AA_DASH);
      return (
        vec4(color.r, color.g, color.b, alpha * color.a) * c
        + vec4(gapColor.r, gapColor.g, gapColor.b, alpha * gapColor.a) * (1.0 - c)
      );
    } else {
      float c = (sd2 + AA_DASH) * (0.5 / AA_DASH);
      return (
        vec4(color.r, color.g, color.b, alpha * color.a) * c
        + vec4(gapColor.r, gapColor.g, gapColor.b, alpha * gapColor.a) * (1.0 - c)
      );
    }
  }
  if (dist <= dashSize) {
    return vec4(color.r, color.g, color.b, alpha * color.a);
  }
  return vec4(gapColor.r, gapColor.g, gapColor.b, alpha * gapColor.a);
}

void main() {
    float r = length(pixelCoord);
    float s = (abs(scaleX) + abs(scaleY)) * 0.5;
    float ss = 1.0 / s;
    float d = abs(r - radius * ss) - lineWidth * 0.5;
    if (d > 1.5) {
      discard;
    }

    float sAngleRange = sign(angleRange);
    float nAngleRange = abs(angleRange);
    float rawA = atan(pixelCoord.x, pixelCoord.y);
    float aa = mod(sAngleRange * (rawA - startAngle) + 8.0 * M_PI, 2.0 * M_PI);

    if (nAngleRange < 2.0 * M_PI && aa < 0.0 || aa > nAngleRange) {
      discard;
    }

    float alpha;
    if (lineWidth >= 1.0) {
      alpha = smoothstep(0.25, 0.0, d);
    } else {
      // simulate line thiner than 1px with alpha
      alpha = lineWidth;
    }

    if (dashSize == 0.0) {
      gl_FragColor = vec4(color.r, color.g, color.b, alpha * color.a);
      return;
    }

    float dist = aa * radius;
    gl_FragColor = aa_dash_gap(dist * ss, alpha, dashSize * ss, gapSize * ss);
}
      `,
    });
  }
}

extend({ ArcDashMaterial });

/**
 * Display a line in data coord.
 *
 * The dash have the specificity to be in data unit.
 *
 * @param center: Center of the circle, in data coord
 * @param radius: Radius of the circle, in data scale
 * @param zIndex: Layer for the display. Bigger number is over smaller numbers.
 * @param lineWidth: Width of the line, in pixel coord
 * @param color: Color of the line
 * @param gapColor: Color of the gap
 * @param dashSize: Size of the dash, in data coord
 * @param gapSize: Size of the dash, in data coord
 * @param opacity: Level of opacity, from 0.0 (transparent) to 1.0 (opaque)
 */
export function Arc(props: {
  center: [number, number];
  radius: number;
  startAngle: number;
  angleRange: number;
  zIndex?: number;
  lineWidth?: number;
  color?: string;
  gapColor?: string;
  dashSize?: number;
  gapSize?: number;
  opacity?: number;
}) {
  const {
    center,
    radius,
    startAngle,
    angleRange,
    lineWidth = 1,
    color = 'black',
    gapColor,
    dashSize = 0,
    gapSize = dashSize,
    opacity = 1,
    zIndex = 0,
  } = props;

  const materialRef = useRef<ArcDashMaterial>(null);

  const linearVisGroup = useLinearVisGroup();

  useEffect(() => {
    const vColor = color4(color, opacity);
    const vGapColor = color4(gapColor ?? 'transparent', opacity);
    if (materialRef.current) {
      const uniforms = materialRef.current.uniforms;
      uniforms.color.value = vColor;
      uniforms.radius.value = radius;
      uniforms.startAngle.value =
        ((((startAngle + 180) % 360) - 180) * Math.PI) / 180;
      uniforms.angleRange.value = (angleRange * Math.PI) / 180;
      uniforms.gapColor.value = vGapColor;
      uniforms.dashSize.value = dashSize;
      uniforms.gapSize.value = gapSize;
      uniforms.lineWidth.value = lineWidth;
      invalidate();
    }
  }, [
    color,
    radius,
    startAngle,
    angleRange,
    gapColor,
    dashSize,
    gapSize,
    opacity,
    lineWidth,
  ]);

  useFrame(({ camera }) => {
    if (materialRef.current === null) {
      return;
    }
    const scaleX = camera.scale.x / linearVisGroup.sx;
    const scaleY = camera.scale.y / linearVisGroup.sy;
    const uniforms = materialRef.current.uniforms;
    uniforms.scaleX.value = scaleX;
    uniforms.scaleY.value = scaleY;
  });

  return (
    <group position={[center[0], center[1], zIndex]}>
      <mesh>
        <ambientLight />
        <planeGeometry
          attach="geometry"
          args={[(radius + lineWidth) * 2, (radius + lineWidth) * 2, 1, 1]}
        />
        {/* @ts-expect-error */}
        <arcDashMaterial attach="material" transparent ref={materialRef} />
      </mesh>
    </group>
  );
}
