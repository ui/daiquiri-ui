import { extend, invalidate } from '@react-three/fiber';
import React, { useEffect, useRef } from 'react';
import { ShaderMaterial, Vector4 } from 'three';
import { color4 } from '../../colors';
import { ScreenScale } from '../ScreenScale';

class CornerMarkerMaterial extends ShaderMaterial {
  private constructor() {
    super({
      uniforms: {
        color: { value: new Vector4() },
        lineWidth: { value: 0 },
        sizeXInScreen: { value: 0 },
        sizeYInScreen: { value: 0 },
        inner: { value: 0 },
      },
      vertexShader: `
out vec2 pixelCoord;

void main() {
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
    pixelCoord = vec2(position.x, position.y);
}
      `,
      fragmentShader: `
uniform vec4 color;
uniform float lineWidth;
uniform float sizeXInScreen;  // signed size
uniform float sizeYInScreen;  // signed size
uniform int inner;            // 0 or 1

in vec2 pixelCoord;

/**
 * Exact signed distance function to compute an
 * orthogonal rectangle.
 *
 * @param pos: Pixel position
 * @param center: Center of the rectangle
 * @param size: Size of the rectangle
 */
float sdf_rect(vec2 pos, vec2 center, vec2 size) {
    vec2 d = abs(pos - center) - size * 0.5;
    return length(max(d, 0.0)) + min(max(d.x, d.y), 0.0);
}

void main() {
  float is_inner = float(inner);
  float d1 = sdf_rect(
    pixelCoord,
    vec2(
      (sizeXInScreen - (1.0 - is_inner) * lineWidth * sign(sizeXInScreen)) * 0.5,
      is_inner * lineWidth * sign(sizeYInScreen) * 0.5
    ),
    vec2(abs(sizeXInScreen), lineWidth)
  );
  float d2 = sdf_rect(
    pixelCoord,
    vec2(
      is_inner * lineWidth * sign(sizeXInScreen) * 0.5,
      (sizeYInScreen - (1.0 - is_inner) * lineWidth * sign(sizeYInScreen)) * 0.5
    ),
    vec2(lineWidth, abs(sizeYInScreen))
  );
  float d = min(d1, d2);

  if (d > 0.2) {
    discard;
  }

  float alpha;
  if (lineWidth >= 1.0) {
    alpha = smoothstep(0.2, 0.0, d);
  } else {
    // simulate line thiner than 1px with alpha
    alpha = lineWidth;
  }

  gl_FragColor = vec4(color.r, color.g, color.b, alpha * color.a);
}
      `,
    });
  }
}

extend({ CornerMarkerMaterial });

/**
 * Display a corner in data coord.
 *
 * The position is the intersection of the 2 lines.
 *
 * @param sizeXInScreen: signed width of the corner, in pixel
 * @param sizeYInScreen: signed height of the corner, in pixel
 * @param inner: If true the position of the marker is in the corner of the line weight,
 *               else the position is in the center of the line weight
 */
export function CornerMarker(props: {
  x: number;
  y: number;
  sizeXInScreen: number;
  sizeYInScreen: number;
  color: string;
  opacity?: number;
  lineWidth?: number;
  zIndex?: number;
  inner?: boolean;
}) {
  const {
    x,
    y,
    color = 'black',
    opacity,
    sizeXInScreen,
    sizeYInScreen,
    lineWidth = 1,
    inner = false,
    zIndex = 0,
  } = props;
  const materialRef = useRef<CornerMarkerMaterial>(null);

  useEffect(() => {
    const vColor = color4(color, opacity);
    if (materialRef.current) {
      const uniforms = materialRef.current.uniforms;
      uniforms.color.value = vColor;
      uniforms.lineWidth.value = lineWidth;
      uniforms.sizeXInScreen.value = sizeXInScreen;
      uniforms.sizeYInScreen.value = sizeYInScreen;
      uniforms.inner.value = inner ? 1 : 0;
      invalidate();
    }
  }, [color, opacity, lineWidth, sizeXInScreen, sizeYInScreen, inner]);

  return (
    <group position={[x, y, zIndex]}>
      <ScreenScale screenDirection={false}>
        <mesh>
          <ambientLight />
          <planeGeometry
            attach="geometry"
            args={[
              Math.abs(sizeXInScreen) * 2 + lineWidth,
              Math.abs(sizeYInScreen) * 2 + lineWidth,
              1,
              1,
            ]}
          />
          {/* @ts-expect-error */}
          <cornerMarkerMaterial
            attach="material"
            transparent
            ref={materialRef}
          />
        </mesh>
      </ScreenScale>
    </group>
  );
}
