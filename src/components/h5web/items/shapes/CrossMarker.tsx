import { extend, invalidate } from '@react-three/fiber';
import React, { useEffect, useRef } from 'react';
import { ShaderMaterial, Vector4 } from 'three';
import { color4 } from '../../colors';
import { ScreenScale } from '../ScreenScale';

class CrossMarkerMaterial extends ShaderMaterial {
  private constructor() {
    super({
      uniforms: {
        color: { value: new Vector4() },
        lineWidth: { value: 0 },
        sizeInScreen: { value: 0 },
      },
      vertexShader: `
out vec2 pixelCoord;

void main() {
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
    pixelCoord = vec2(position.x, position.y);
}
      `,
      fragmentShader: `
uniform vec4 color;
uniform float lineWidth;
uniform float sizeInScreen;
in vec2 pixelCoord;

/**
 * Exact signed distance function to compute an
 * orthogonal rectangle.
 *
 * @param pos: Pixel position
 * @param center: Center of the rectangle
 * @param size: Size of the rectangle
 */
float sdf_rect(vec2 pos, vec2 center, vec2 size) {
    vec2 d = abs(pos - center) - size * 0.5;
    return length(max(d, 0.0)) + min(max(d.x, d.y), 0.0);
}

void main() {
  float d1 = sdf_rect(
    pixelCoord,
    vec2(0.0, 0.0),
    vec2(abs(sizeInScreen), lineWidth)
  );
  float d2 = sdf_rect(
    pixelCoord,
    vec2(0.0, 0.0),
    vec2(lineWidth, abs(sizeInScreen))
  );
  float d = min(d1, d2);

  if (d > 0.2) {
    discard;
  }

  float alpha;
  if (lineWidth >= 1.0) {
    alpha = smoothstep(0.2, 0.0, d);
  } else {
    // simulate line thiner than 1px with alpha
    alpha = lineWidth;
  }

  gl_FragColor = vec4(color.r, color.g, color.b, alpha * color.a);
}
      `,
    });
  }
}

extend({ CrossMarkerMaterial });

/**
 * Display a corner in data coord, expect the size is related to the screen.
 *
 * @param sizeInScreen: width and height of the corner, in pixel
 */
export function CrossMarker(props: {
  x: number;
  y: number;
  sizeInScreen: number;
  color: string;
  opacity?: number;
  lineWidth?: number;
  zIndex?: number;
}) {
  const {
    x,
    y,
    color = 'black',
    opacity,
    sizeInScreen,
    lineWidth = 1,
    zIndex = 0,
  } = props;
  const materialRef = useRef<CrossMarkerMaterial>(null);

  useEffect(() => {
    const vColor = color4(color, opacity);
    if (materialRef.current) {
      const uniforms = materialRef.current.uniforms;
      uniforms.color.value = vColor;
      uniforms.lineWidth.value = lineWidth;
      uniforms.sizeInScreen.value = sizeInScreen;
      invalidate();
    }
  }, [color, opacity, lineWidth, sizeInScreen]);

  return (
    <group position={[x, y, zIndex]}>
      <ScreenScale>
        <mesh>
          <ambientLight />
          <planeGeometry
            attach="geometry"
            args={[sizeInScreen + lineWidth, sizeInScreen + lineWidth, 1, 1]}
          />
          {/* @ts-expect-error */}
          <crossMarkerMaterial
            attach="material"
            transparent
            ref={materialRef}
          />
        </mesh>
      </ScreenScale>
    </group>
  );
}
