import { HSegment } from './HSegment';
import { VSegment } from './VSegment';

/**
 * Draw a cross in data coordinate of the plot
 *
 * The geometry is defined by a center, a width and a height.
 *
 * Uses an optional `gapColor` for the gap when a dash is used.
 *
 * The shape is draw from the center with 4 lines in order to have a fine symmetry
 * with the dashes.
 */
export default function CrossItem(props: {
  centerX: number;
  centerY: number;
  width: number;
  height: number;
  color: string;
  gapColor?: string;
  dashSize?: number;
  gapSize?: number;
  lineWidth?: number;
  zIndex?: number;
}) {
  const { centerX, centerY, width, height } = props;
  const halfWidth = width * 0.5;
  const halfHeight = height * 0.5;

  return (
    <>
      <HSegment
        y={centerY}
        x1={centerX}
        x2={centerX - halfWidth}
        color={props.color}
        gapColor={props.gapColor}
        dashSize={props.dashSize}
        gapSize={props.gapSize}
        lineWidth={props.lineWidth}
        zIndex={props.zIndex}
      />
      <HSegment
        y={centerY}
        x1={centerX}
        x2={centerX + halfWidth}
        color={props.color}
        gapColor={props.gapColor}
        dashSize={props.dashSize}
        gapSize={props.gapSize}
        lineWidth={props.lineWidth}
        zIndex={props.zIndex}
      />
      <VSegment
        x={centerX}
        y1={centerY}
        y2={centerY + halfHeight}
        color={props.color}
        gapColor={props.gapColor}
        dashSize={props.dashSize}
        gapSize={props.gapSize}
        lineWidth={props.lineWidth}
        zIndex={props.zIndex}
      />
      <VSegment
        x={centerX}
        y1={centerY}
        y2={centerY - halfHeight}
        color={props.color}
        gapColor={props.gapColor}
        dashSize={props.dashSize}
        gapSize={props.gapSize}
        lineWidth={props.lineWidth}
        zIndex={props.zIndex}
      />
    </>
  );
}
