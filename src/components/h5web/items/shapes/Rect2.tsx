import { extend, invalidate, useFrame } from '@react-three/fiber';
import React, { useEffect, useRef } from 'react';
import { ShaderMaterial, Vector2, Vector4 } from 'three';
import { useLinearVisGroup } from '../../LinearVisGroup';
import { color4 } from '../../colors';

class Rect2DashMaterial extends ShaderMaterial {
  public constructor() {
    super({
      uniforms: {
        color: { value: new Vector4() },
        gapColor: { value: new Vector4() },
        size: { value: new Vector2() },
        dashSize: { value: 0 },
        gapSize: { value: 0 },
        scaleX: { value: 0 },
        scaleY: { value: 0 },
        lineWidth: { value: 0 },
      },
      vertexShader: `
uniform float scaleX; // signed scale
uniform float scaleY; // signed scale
out vec2 pixelCoord;
// out vec2 dataCoord;

void main() {
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
    vec2 pixelScale = abs(vec2(scaleX, scaleY));
    // dataCoord = vec2(position.x, position.y);
    pixelCoord = vec2(position.x, position.y) / pixelScale;
}
      `,
      fragmentShader: `
uniform vec4 color;
uniform vec4 gapColor;
uniform float gapSize;
uniform float dashSize;
uniform float lineWidth;
uniform float scaleX; // signed scale
uniform float scaleY; // signed scale
uniform vec2 size;
in vec2 pixelCoord;

/**
 * Exact signed distance function to compute an
 * orthogonal rectangle.
 *
 * @param pos: Pixel position
 * @param center: Center of the rectangle
 * @param size: Size of the rectangle
 */
float sdf_rect(vec2 pos, vec2 center, vec2 size) {
    vec2 d = abs(pos - center) - size * 0.5;
    return length(max(d, 0.0)) + min(max(d.x, d.y), 0.0);
}

void main() {
    vec2 pixelScale = vec2(scaleX, scaleY);
    float d = sdf_rect(pixelCoord, vec2(0.0, 0.0), size / pixelScale);
    if (abs(d) > 1.5) {
      discard;
    }
    float alpha;
    if (lineWidth >= 1.0) {
        alpha = smoothstep(0.5, 0.0, abs(d) - lineWidth * 0.5);
    } else {
        // simulate line thiner than 1px with alpha
        alpha = lineWidth;
    }
    gl_FragColor = vec4(color.rgb, color.a * alpha);
}
      `,
    });
  }
}

extend({ Rect2DashMaterial });

/**
 * Display a line in data coord.
 *
 * The dash have the specificity to be in data unit.
 *
 * @param center: Center of the circle, in data coord
 * @param radius: Radius of the circle, in data scale
 * @param zIndex: Layer for the display. Bigger number is over smaller numbers.
 * @param lineWidth: Width of the line, in pixel coord
 * @param color: Color of the line
 * @param gapColor: Color of the gap
 * @param dashSize: Size of the dash, in data coord
 * @param gapSize: Size of the dash, in data coord
 * @param opacity: Level of opacity, from 0.0 (transparent) to 1.0 (opaque)
 */
export function Rect2(props: {
  center: [number, number];
  size: [number, number];
  zIndex?: number;
  lineWidth?: number;
  color?: string;
  gapColor?: string;
  dashSize?: number;
  gapSize?: number;
  opacity?: number;
}) {
  const {
    center,
    size,
    lineWidth = 1,
    color = 'black',
    gapColor,
    dashSize = 0,
    gapSize = dashSize,
    opacity = 1,
    zIndex = 0,
  } = props;

  const materialRef = useRef<Rect2DashMaterial>(null);

  const linearVisGroup = useLinearVisGroup();

  useEffect(() => {
    const vColor = color4(color, opacity);
    const vGapColor = color4(gapColor ?? 'transparent', opacity);
    if (materialRef.current) {
      const uniforms = materialRef.current.uniforms;
      uniforms.color.value = vColor;
      uniforms.size.value = size;
      uniforms.gapColor.value = vGapColor;
      uniforms.dashSize.value = dashSize;
      uniforms.gapSize.value = gapSize;
      uniforms.lineWidth.value = lineWidth;
      invalidate();
    }
  }, [color, size, gapColor, dashSize, gapSize, opacity, lineWidth]);

  useFrame(({ camera }) => {
    if (materialRef.current === null) {
      return;
    }
    const scaleX = camera.scale.x / linearVisGroup.sx;
    const scaleY = camera.scale.y / linearVisGroup.sy;
    const uniforms = materialRef.current.uniforms;
    uniforms.scaleX.value = scaleX;
    uniforms.scaleY.value = scaleY;
  });

  return (
    <group position={[center[0], center[1], zIndex]}>
      <mesh>
        <ambientLight />
        <planeGeometry
          attach="geometry"
          args={[size[0] + lineWidth, size[1] + lineWidth, 1, 1]}
        />
        {/* @ts-expect-error */}
        <rect2DashMaterial attach="material" transparent ref={materialRef} />
      </mesh>
    </group>
  );
}
