import { extend, invalidate, useFrame } from '@react-three/fiber';
import React, { useEffect, useRef } from 'react';
import { ShaderMaterial, Vector2, Vector4 } from 'three';
import { useLinearVisGroup } from '../../LinearVisGroup';
import { color4 } from '../../colors';

class GridDashMaterial extends ShaderMaterial {
  public constructor() {
    super({
      uniforms: {
        color: { value: new Vector4() },
        size: { value: new Vector2() },
        nbCells: { value: new Vector2() },
        overlap: { value: new Vector2() },
        scaleX: { value: 0 },
        scaleY: { value: 0 },
        lineWidth: { value: 0 },
      },
      vertexShader: `
uniform float scaleX; // signed scale
uniform float scaleY; // signed scale
out vec2 pixelCoord;
// out vec2 dataCoord;

void main() {
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
    vec2 pixelScale = abs(vec2(scaleX, scaleY));
    // dataCoord = vec2(position.x, position.y);
    pixelCoord = vec2(position.x, position.y) / pixelScale;
}
      `,
      fragmentShader: `
uniform vec4 color;
uniform vec4 gapColor;
uniform float gapSize;
uniform float dashSize;
uniform float lineWidth;
uniform float scaleX;  // signed scale
uniform float scaleY;  // signed scale
uniform vec2 size;
uniform vec2 nbCells;
uniform vec2 overlap;
in vec2 pixelCoord;

/**
 * @param position: pixel position
 * @param size: size of the rectangle
 * @param nbCells: number of cells
 * @param overlap: size of the overlap in ratio of a cell
 *                 (0.5 means same size with the cell)
 */
float sdf_grid(in vec2 position, in vec2 size, in vec2 nbCells, in vec2 overlap) {
    vec2 maxSize = size / (nbCells - overlap * (nbCells - 1.0));
    vec2 tableDist = min(abs(abs(position) - size * 0.5), maxSize);
    vec2 overlapSize = maxSize * overlap;
    vec2 cellSize = maxSize - overlapSize;
    vec2 p = position - cellSize * 0.5 * (1.0 - mod(nbCells, 2.0));
    vec2 gridDist = abs(abs(mod(p, cellSize)) - cellSize * 0.5);
    gridDist -= overlapSize * 0.5;
    gridDist = max(gridDist, cellSize  - tableDist);
    return min(gridDist.x, gridDist.y);
}

void main() {
    vec2 scale = abs(vec2(scaleX, scaleY));
    float d = sdf_grid(pixelCoord, size / scale, nbCells, overlap);

    //#define DEBUG_DIST
    #ifdef DEBUG_DIST
        vec2 n = size / nbCells / scale;
        float c = d / min(n.x, n.y);
        gl_FragColor = vec4(max(-c, 0.0), abs(c), max(c, 0.0), 1.0);
        return;
    #endif

    if (d > lineWidth) {
      discard;
    }
    float alpha;
    if (lineWidth >= 1.0) {
        alpha = smoothstep(1.0, 0.0, d - lineWidth * 0.5);
    } else {
        // simulate line thiner than 1px with alpha
        alpha = lineWidth;
    }
    gl_FragColor = vec4(color.rgb, color.a * alpha);
}
      `,
    });
  }
}

extend({ GridDashMaterial });

/**
 * Display a grid inside a rectangle.
 *
 * It supports hilight of overlap between cells.
 *
 * Geometry:
 *
 *     |----|---------|----|---------|----|
 *     |-------cell1-------|
 *                    |-------cell2-------|
 *            overlap |----|
 *
 * @param center: Center of the circle, in data coord
 * @param radius: Radius of the circle, in data scale
 * @param zIndex: Layer for the display. Bigger number is over smaller numbers.
 * @param lineWidth: Width of the line, in pixel coord
 * @param color: Color of the line
 * @param gapColor: Color of the gap
 * @param dashSize: Size of the dash, in data coord
 * @param gapSize: Size of the dash, in data coord
 * @param opacity: Level of opacity, from 0.0 (transparent) to 1.0 (opaque)
 */
export function Grid(props: {
  center: [number, number];
  size: [number, number];
  nbCells: [number, number];
  overlap?: [number, number];
  zIndex?: number;
  lineWidth?: number;
  color?: string;
  opacity?: number;
}) {
  const {
    center,
    size,
    nbCells,
    overlap = [0, 0],
    lineWidth = 1,
    color = 'black',
    opacity = 1,
    zIndex = 0,
  } = props;

  const materialRef = useRef<GridDashMaterial>(null);

  const linearVisGroup = useLinearVisGroup();

  useEffect(() => {
    const vColor = color4(color, opacity);
    if (materialRef.current) {
      const uniforms = materialRef.current.uniforms;
      uniforms.color.value = vColor;
      uniforms.size.value = size;
      uniforms.nbCells.value = nbCells;
      uniforms.overlap.value = overlap;
      uniforms.lineWidth.value = lineWidth;
      invalidate();
    }
  }, [color, size, nbCells, overlap, opacity, lineWidth]);

  useFrame(({ camera }) => {
    if (materialRef.current === null) {
      return;
    }
    const scaleX = camera.scale.x / linearVisGroup.sx;
    const scaleY = camera.scale.y / linearVisGroup.sy;
    const uniforms = materialRef.current.uniforms;
    uniforms.scaleX.value = scaleX;
    uniforms.scaleY.value = scaleY;
  });

  return (
    <group position={[center[0], center[1], zIndex]}>
      <mesh>
        <ambientLight />
        <planeGeometry attach="geometry" args={[size[0], size[1], 1, 1]} />
        {/* @ts-expect-error */}
        <gridDashMaterial attach="material" transparent ref={materialRef} />
      </mesh>
    </group>
  );
}
