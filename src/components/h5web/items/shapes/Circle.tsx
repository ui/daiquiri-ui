import { extend, invalidate, useFrame } from '@react-three/fiber';
import React, { useEffect, useRef } from 'react';
import { ShaderMaterial, Vector4 } from 'three';
import { useLinearVisGroup } from '../../LinearVisGroup';
import { color4 } from '../../colors';

class CircleDashMaterial extends ShaderMaterial {
  public constructor() {
    super({
      uniforms: {
        color: { value: new Vector4() },
        gapColor: { value: new Vector4() },
        radius: { value: 0 },
        dashSize: { value: 0 },
        gapSize: { value: 0 },
        scaleX: { value: 0 },
        scaleY: { value: 0 },
        lineWidth: { value: 0 },
      },
      vertexShader: `
uniform float scaleX; // signed scale
uniform float scaleY; // signed scale
out vec2 pixelCoord;
// out vec2 dataCoord;

void main() {
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
    vec2 pixelScale = abs(vec2(scaleX, scaleY));
    // dataCoord = vec2(position.x, position.y);
    pixelCoord = vec2(position.x, position.y) / pixelScale;
}
      `,
      fragmentShader: `
uniform vec4 color;
uniform vec4 gapColor;
uniform float gapSize;
uniform float dashSize;
uniform float lineWidth;
uniform float scaleX; // signed scale
uniform float scaleY; // signed scale
uniform float radius;
in vec2 pixelCoord;

const float M_PI = 3.1415926535897932384626433832795;
const float AA_DASH = 0.5;

/**
 * Antialiasing following the line of dash and gap
 */
vec4 aa_dash_gap(float dist, float alpha) {
  dist = mod(dist, (dashSize + gapSize));
  float sd1 = (dist < dashSize) ? dist : dist - (dashSize + gapSize);
  float sd2 = -(dist - dashSize);
  if (abs(sd1) <= AA_DASH || abs(sd2) <= AA_DASH) {
    if (abs(sd1) < abs(sd2)) {
      float c = (sd1 + AA_DASH) * (0.5 / AA_DASH);
      return (
        vec4(color.r, color.g, color.b, alpha * color.a) * c
        + vec4(gapColor.r, gapColor.g, gapColor.b, alpha * gapColor.a) * (1.0 - c)
      );
    } else {
      float c = (sd2 + AA_DASH) * (0.5 / AA_DASH);
      return (
        vec4(color.r, color.g, color.b, alpha * color.a) * c
        + vec4(gapColor.r, gapColor.g, gapColor.b, alpha * gapColor.a) * (1.0 - c)
      );
    }
  }
  if (dist <= dashSize) {
      return vec4(color.r, color.g, color.b, alpha * color.a);
  }
  return vec4(gapColor.r, gapColor.g, gapColor.b, alpha * gapColor.a);
}

void main() {
    float r = length(pixelCoord);
    float s = (abs(scaleX) + abs(scaleY)) * 0.5;
    float d = abs(r - radius / s) - lineWidth * 0.5;
    if (d > 1.5) {
      discard;
    }
    float alpha;
    if (lineWidth >= 1.0) {
        alpha = smoothstep(0.25, 0.0, d);
    } else {
        // simulate line thiner than 1px with alpha
        alpha = lineWidth;
    }

    if (dashSize == 0.0) {
        gl_FragColor = vec4(color.r, color.g, color.b, alpha * color.a);
        return;
    }

    // try to fit a fixed integer number of dashes + gaps
    // could be computed in the vertex shader
    float full = 2.0 * M_PI * (radius / s) / (dashSize + gapSize);
    float coef = ceil(full) / full;

    float dist = 2.0 * atan(pixelCoord.y, pixelCoord.x) * (radius / s) * coef;
    gl_FragColor = aa_dash_gap(dist, alpha);
}
      `,
    });
  }
}

extend({ CircleDashMaterial });

/**
 * Display a line in data coord.
 *
 * The dash have the specificity to be in data unit.
 *
 * @param center: Center of the circle, in data coord
 * @param radius: Radius of the circle, in data scale
 * @param zIndex: Layer for the display. Bigger number is over smaller numbers.
 * @param lineWidth: Width of the line, in pixel coord
 * @param color: Color of the line
 * @param gapColor: Color of the gap
 * @param dashSize: Size of the dash, in data coord
 * @param gapSize: Size of the dash, in data coord
 * @param opacity: Level of opacity, from 0.0 (transparent) to 1.0 (opaque)
 */
export function Circle(props: {
  center: [number, number];
  radius: number;
  zIndex?: number;
  lineWidth?: number;
  color?: string;
  gapColor?: string;
  dashSize?: number;
  gapSize?: number;
  opacity?: number;
}) {
  const {
    center,
    radius,
    lineWidth = 1,
    color = 'black',
    gapColor,
    dashSize = 0,
    gapSize = dashSize,
    opacity = 1,
    zIndex = 0,
  } = props;

  const materialRef = useRef<CircleDashMaterial>(null);

  const linearVisGroup = useLinearVisGroup();

  useEffect(() => {
    const vColor = color4(color, opacity);
    const vGapColor = color4(gapColor ?? 'transparent', opacity);
    if (materialRef.current) {
      const uniforms = materialRef.current.uniforms;
      uniforms.color.value = vColor;
      uniforms.radius.value = radius;
      uniforms.gapColor.value = vGapColor;
      uniforms.dashSize.value = dashSize;
      uniforms.gapSize.value = gapSize;
      uniforms.lineWidth.value = lineWidth;
      invalidate();
    }
  }, [color, radius, gapColor, dashSize, gapSize, opacity, lineWidth]);

  useFrame(({ camera }) => {
    if (materialRef.current === null) {
      return;
    }
    const scaleX = camera.scale.x / linearVisGroup.sx;
    const scaleY = camera.scale.y / linearVisGroup.sy;
    const uniforms = materialRef.current.uniforms;
    uniforms.scaleX.value = scaleX;
    uniforms.scaleY.value = scaleY;
  });

  return (
    <group position={[center[0], center[1], zIndex]}>
      <mesh>
        <ambientLight />
        <planeGeometry
          attach="geometry"
          args={[(radius + lineWidth) * 2, (radius + lineWidth) * 2, 1, 1]}
        />
        {/* @ts-expect-error */}
        <circleDashMaterial attach="material" transparent ref={materialRef} />
      </mesh>
    </group>
  );
}
