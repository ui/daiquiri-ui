import React from 'react';
import { HSegment } from './HSegment';
import { VSegment } from './VSegment';

export function Rect(props: {
  startPoint: [number, number];
  endPoint: [number, number];
  zIndex?: number;
  lineWidth?: number;
  lineColor?: string;
  gapColor?: string;
  dashSize?: number;
  gapSize?: number;
  opacity?: number;
}) {
  const {
    startPoint,
    endPoint,
    lineWidth = 1,
    lineColor = 'black',
    gapColor,
    gapSize,
    dashSize,
    opacity = 1,
    zIndex = 1,
  } = props;

  return (
    <>
      <HSegment
        y={startPoint[1]}
        x1={startPoint[0]}
        x2={endPoint[0]}
        zIndex={zIndex}
        color={lineColor}
        gapColor={gapColor}
        gapSize={gapSize}
        dashSize={dashSize}
        opacity={opacity}
        lineWidth={lineWidth}
      />
      <HSegment
        y={endPoint[1]}
        x1={startPoint[0]}
        x2={endPoint[0]}
        zIndex={zIndex}
        color={lineColor}
        gapColor={gapColor}
        gapSize={gapSize}
        dashSize={dashSize}
        opacity={opacity}
        lineWidth={lineWidth}
      />
      <VSegment
        x={startPoint[0]}
        y1={startPoint[1]}
        y2={endPoint[1]}
        zIndex={zIndex}
        color={lineColor}
        gapColor={gapColor}
        gapSize={gapSize}
        dashSize={dashSize}
        opacity={opacity}
        lineWidth={lineWidth}
      />
      <VSegment
        x={endPoint[0]}
        y1={startPoint[1]}
        y2={endPoint[1]}
        zIndex={zIndex}
        color={lineColor}
        gapColor={gapColor}
        gapSize={gapSize}
        dashSize={dashSize}
        opacity={opacity}
        lineWidth={lineWidth}
      />
    </>
  );
}
