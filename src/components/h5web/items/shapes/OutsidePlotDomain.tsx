import React from 'react';

/**
 * Set a plain color outside of a rect.
 */
export function OutsidePlotDomain(props: {
  domainX: [number, number];
  domainY: [number, number];
  color: string;
  zIndex?: number;
}) {
  const { color = 'black', domainX, domainY, zIndex = 0 } = props;

  const sizeX = Math.abs(domainX[1] - domainX[0]);
  const x = (domainX[0] + domainX[1]) * 0.5;
  const sizeY = Math.abs(domainY[1] - domainY[0]);
  const y = (domainY[0] + domainY[1]) * 0.5;

  return (
    <>
      <mesh position={[x - sizeX, y, zIndex]}>
        <ambientLight />
        <planeGeometry attach="geometry" args={[sizeX, sizeY * 10, 1, 1]} />
        <meshBasicMaterial attach="material" color={color} opacity={1} />
      </mesh>
      <mesh position={[x + sizeX, y, zIndex]}>
        <ambientLight />
        <planeGeometry attach="geometry" args={[sizeX, sizeY * 10, 1, 1]} />
        <meshBasicMaterial attach="material" color={color} opacity={1} />
      </mesh>
      <mesh position={[x, y - sizeY, zIndex]}>
        <ambientLight />
        <planeGeometry attach="geometry" args={[sizeX * 10, sizeY, 1, 1]} />
        <meshBasicMaterial attach="material" color={color} opacity={1} />
      </mesh>
      <mesh position={[x, y + sizeY, zIndex]}>
        <ambientLight />
        <planeGeometry attach="geometry" args={[sizeX * 10, sizeY, 1, 1]} />
        <meshBasicMaterial attach="material" color={color} opacity={1} />
      </mesh>
    </>
  );
}
