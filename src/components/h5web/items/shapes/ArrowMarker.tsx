import { extend, invalidate } from '@react-three/fiber';
import React, { useEffect, useRef } from 'react';
import { ShaderMaterial, Vector4 } from 'three';
import { color4 } from '../../colors';
import { ScreenScale } from '../ScreenScale';

class ArrowMarkerMaterial extends ShaderMaterial {
  private constructor() {
    super({
      uniforms: {
        color: { value: new Vector4() },
        lineWidth: { value: 0 },
        sizeInScreen: { value: 0 },
        angle: { value: 0 },
      },
      vertexShader: `
out vec2 pixelCoord;

void main() {
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
    pixelCoord = vec2(position.x, position.y);
}
      `,
      fragmentShader: `
// Based on antialiased arrow fields
// Nicolas P. Rougier (http://www.loria.fr/~rougier)
// https://www.shadertoy.com/view/ldlSWj
// Released under BSD license.

uniform vec4 color;
uniform float lineWidth;
uniform float sizeInScreen;
uniform float angle;
in vec2 pixelCoord;

// Computes the signed distance from a line
float sdf_line(vec2 p, vec2 p1, vec2 p2) {
    vec2 center = (p1 + p2) * 0.5;
    float len = length(p2 - p1);
    vec2 dir = (p2 - p1) / len;
    vec2 rel_p = p - center;
    return dot(rel_p, vec2(dir.y, -dir.x));
}

// Computes the signed distance from a line segment
float sdf_segment(vec2 p, vec2 p1, vec2 p2) {
    vec2 center = (p1 + p2) * 0.5;
    float len = length(p2 - p1);
    vec2 dir = (p2 - p1) / len;
    vec2 rel_p = p - center;
    float dist1 = abs(dot(rel_p, vec2(dir.y, -dir.x)));
    float dist2 = abs(dot(rel_p, dir)) - 0.5*len;
    return max(dist1, dist2);
}

float sdf_arrow(vec2 texcoord,
  float body, float head, float height,
  float linewidth, float antialias
) {
  float d;
  float w = linewidth/2.0 + antialias;
  vec2 start = -vec2(body / 2.0, 0.0);
  vec2 end   = +vec2(body / 2.0, 0.0);

  // Arrow tip (beyond segment end)
  if( texcoord.x > body / 2.0) {
    // Head : 2 segments
    float d1 = sdf_line(texcoord, end, end - head*vec2(+1.0, -height));
    float d2 = sdf_line(texcoord, end - head*vec2(+1.0, +height), end);
    // Body : 1 segment
    float d3 = end.x - texcoord.x;
    d = max(max(d1, d2), d3);
  } else {
    // Head : 2 segments
    float d1 = sdf_segment(texcoord, end - head * vec2(+1.0, -height), end);
    float d2 = sdf_segment(texcoord, end - head * vec2(+1.0, +height), end);
    // Body : 1 segment
    float d3 = sdf_segment(texcoord, start, end - vec2(linewidth, 0.0));
    d = min(min(d1, d2), d3);
  }
  return d;
}

vec4 filled(float distance, float linewidth, float antialias, vec4 fill)
{
    vec4 frag_color;
    float t = linewidth / 2.0 - antialias;
    float signed_distance = distance;
    float border_distance = abs(signed_distance) - t;
    float alpha = border_distance / antialias;
    alpha = exp(-alpha * alpha);

    // Within linestroke
    if (border_distance < 0.0) {
        return fill;
    }
    // Within shape
    if (signed_distance < 0.0) {
      return fill;
    }
    // Outside shape
    if (border_distance > (linewidth / 2.0 + antialias)) {
        discard;
    }
    // Line stroke exterior border
    return vec4(fill.rgb, alpha);
}

void main() {
    const float M_PI = 3.1415926535897932384626433832795;

    float theta = angle * M_PI / 180.0;
    float cos_theta = cos(theta);
    float sin_theta = sin(theta);
    vec2 pixelCoord2 = vec2(
      cos_theta * pixelCoord.x - sin_theta * pixelCoord.y,
      sin_theta * pixelCoord.x + cos_theta * pixelCoord.y
    );

    const float antialias = 1.0;
    float body = sizeInScreen;
    float d = sdf_arrow(pixelCoord2, body, 0.30 * body, 0.8, lineWidth, antialias);
    gl_FragColor = filled(d, lineWidth, antialias, color);
}
    `,
    });
  }
}

extend({ ArrowMarkerMaterial });

/**
 * Display a corner in data coord, expect the size is related to the screen.
 *
 * @param sizeInScreen: width and height of the corner, in pixel
 */
export function ArrowMarker(props: {
  x: number;
  y: number;
  sizeInScreen: number;
  angle: number;
  color: string;
  opacity?: number;
  lineWidth?: number;
  zIndex?: number;
}) {
  const {
    x,
    y,
    color = 'black',
    opacity,
    sizeInScreen,
    angle,
    lineWidth = 1,
    zIndex = 0,
  } = props;
  const materialRef = useRef<ArrowMarkerMaterial>(null);

  useEffect(() => {
    const vColor = color4(color, opacity);
    if (materialRef.current) {
      const uniforms = materialRef.current.uniforms;
      uniforms.color.value = vColor;
      uniforms.lineWidth.value = lineWidth;
      uniforms.sizeInScreen.value = sizeInScreen;
      uniforms.angle.value = angle;
      invalidate();
    }
  }, [color, opacity, lineWidth, sizeInScreen, angle]);

  return (
    <group position={[x, y, zIndex]}>
      <ScreenScale screenDirection={false}>
        <mesh>
          <ambientLight />
          <planeGeometry
            attach="geometry"
            args={[sizeInScreen + lineWidth, sizeInScreen + lineWidth, 1, 1]}
          />
          {/* @ts-expect-error */}
          <arrowMarkerMaterial
            attach="material"
            transparent
            ref={materialRef}
          />
        </mesh>
      </ScreenScale>
    </group>
  );
}
