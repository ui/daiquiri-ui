import { extend, invalidate, useFrame } from '@react-three/fiber';
import React, { useEffect, useRef } from 'react';
import type { Group } from 'three';
import { ShaderMaterial, Vector4 } from 'three';
import { useLinearVisGroup } from '../../LinearVisGroup';
import { color4 } from '../../colors';

class VDashMaterial extends ShaderMaterial {
  public constructor() {
    super({
      uniforms: {
        color: { value: new Vector4() },
        gapColor: { value: new Vector4() },
        dashSize: { value: 0 },
        gapSize: { value: 0 },
        scaleX: { value: 0 },
        scaleY: { value: 0 },
        size: { value: 0 },
        lineWidth: { value: 0 },
      },
      vertexShader: `
uniform float scaleX;
uniform float scaleY;
uniform float size;
out vec2 pixelCoord;

void main() {
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
    vec2 pixelScale = vec2(scaleX, scaleY);
    pixelCoord = vec2(position.x, position.y + size * 0.5) / pixelScale;
}
      `,
      fragmentShader: `
uniform vec4 color;
uniform vec4 gapColor;
uniform float gapSize;
uniform float dashSize;
uniform float lineWidth;

in vec2 pixelCoord;

void main() {
    float alpha;
    if (lineWidth >= 1.0) {
        alpha = smoothstep(lineWidth * 0.5 + 0.01, lineWidth * 0.5 - 0.1, abs(pixelCoord.x));
    } else {
        // simulate line thiner than 1px with alpha
        alpha = lineWidth;
    }

    if (dashSize == 0.0) {
        gl_FragColor = vec4(color.r, color.g, color.b, alpha * color.a);
        return;
    }

    float dist = abs(pixelCoord.y);
    dist = mod(dist, (dashSize + gapSize));
    if (dist <= dashSize) {
        gl_FragColor = vec4(color.r, color.g, color.b, alpha * color.a);
    } else {
        gl_FragColor = vec4(gapColor.r, gapColor.g, gapColor.b, alpha * gapColor.a);
    }
}
      `,
    });
  }
}

extend({ VDashMaterial });

export function VSegment(props: {
  x: number;
  y1: number;
  y2: number;
  zIndex?: number;
  lineWidth?: number;
  color?: string;
  gapColor?: string;
  dashSize?: number;
  gapSize?: number;
  opacity?: number;
}) {
  const {
    x,
    y1,
    y2,
    lineWidth = 1,
    color = 'black',
    gapColor,
    dashSize = 0,
    gapSize = dashSize,
    opacity = 1,
    zIndex = 0,
  } = props;

  const materialRef = useRef<VDashMaterial>(null);

  const length = y2 - y1;
  const y0 = (y1 + y2) * 0.5;

  const localRef = React.useRef<Group>(null);

  const linearVisGroup = useLinearVisGroup();

  useEffect(() => {
    const vColor = color4(color, opacity);
    const vGapColor = color4(gapColor ?? 'transparent', opacity);
    if (materialRef.current) {
      const uniforms = materialRef.current.uniforms;
      uniforms.color.value = vColor;
      uniforms.gapColor.value = vGapColor;
      uniforms.dashSize.value = dashSize;
      uniforms.gapSize.value = gapSize;
      uniforms.size.value = length;
      uniforms.lineWidth.value = lineWidth;
      invalidate();
    }
  }, [color, gapColor, dashSize, gapSize, opacity, length, lineWidth]);

  useFrame(({ camera }) => {
    if (localRef.current === null || materialRef.current === null) {
      return;
    }
    const scaleX = camera.scale.x / linearVisGroup.sx;
    const scaleY = camera.scale.y / linearVisGroup.sy;
    localRef.current.scale.x = scaleX;

    const uniforms: any = materialRef.current.uniforms;
    uniforms.scaleX.value = 1;
    uniforms.scaleY.value = scaleY;
  });

  return (
    <group ref={localRef} position={[x, y0, zIndex]}>
      <mesh>
        <ambientLight />
        <planeGeometry
          attach="geometry"
          args={[lineWidth + 1, Math.abs(length), 1, 1]}
        />
        {/* @ts-expect-error */}
        <vDashMaterial attach="material" transparent ref={materialRef} />
      </mesh>
    </group>
  );
}
