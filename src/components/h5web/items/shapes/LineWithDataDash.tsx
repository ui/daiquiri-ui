import React, { useEffect, useMemo, useRef } from 'react';
import { extend, invalidate, useFrame } from '@react-three/fiber';
import { ShaderMaterial, Shape, Vector2, Vector4 } from 'three';
import { useLinearVisGroup } from '../../LinearVisGroup';
import { color4 } from '../../colors';

class LineWithDataDashMaterial extends ShaderMaterial {
  public constructor() {
    super({
      uniforms: {
        color: { value: new Vector4() },
        gapColor: { value: new Vector4() },
        dashSize: { value: 0 },
        gapSize: { value: 0 },
        scaleX: { value: 0 },
        scaleY: { value: 0 },
        origin: { value: new Vector2() },
        direction: { value: new Vector2() },
        lineWidth: { value: 0 },
      },
      vertexShader: `
uniform float scaleX;  // signed scale
uniform float scaleY;  // signed scale
uniform vec2 origin;
uniform vec2 direction;
out vec2 pixelCoord;

void main() {
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
    vec2 p = vec2(position.x, position.y) - origin;
    float l = dot(p, direction);
    float d = dot(p, vec2(-direction.y, direction.x));
    float scale = (abs(scaleX) + abs(scaleY)) * 0.5;
    pixelCoord = vec2(l, d / scale);
}
      `,
      fragmentShader: `
uniform vec4 color;
uniform vec4 gapColor;
uniform float gapSize;
uniform float dashSize;
uniform float lineWidth;

in vec2 pixelCoord;

void main() {
    float alpha;
    if (lineWidth >= 1.0) {
        alpha = smoothstep(0.5, 0.0, abs(pixelCoord.y) - lineWidth * 0.5);
    } else {
        // simulate line thiner than 1px with alpha
        alpha = lineWidth;
    }

    if (dashSize == 0.0) {
        gl_FragColor = vec4(color.r, color.g, color.b, alpha * color.a);
        return;
    }

    float dist = abs(pixelCoord.x);
    dist = mod(dist, (dashSize + gapSize));
    if (dist <= dashSize) {
        gl_FragColor = vec4(color.r, color.g, color.b, alpha * color.a);
    } else {
        gl_FragColor = vec4(gapColor.r, gapColor.g, gapColor.b, alpha * gapColor.a);
    }
}
      `,
    });
  }
}

extend({ LineWithDataDashMaterial });

/**
 * Display a line in data coord.
 *
 * The dash have the specificity to be in data unit.
 *
 * @param p1: Start point of the line, in data coord
 * @param p2: End point of the line, in data coord
 * @param zIndex: Layer for the display. Bigger number is over smaller numbers.
 * @param lineWidth: Width of the line, in pixel coord
 * @param color: Color of the line
 * @param gapColor: Color of the gap
 * @param dashSize: Size of the dash, in data coord
 * @param gapSize: Size of the dash, in data coord
 * @param opacity: Level of opacity, from 0.0 (transparent) to 1.0 (opaque)
 */
export function LineWithDataDash(props: {
  p1: [number, number];
  p2: [number, number];
  zIndex?: number;
  lineWidth?: number;
  color?: string;
  gapColor?: string;
  dashSize?: number;
  gapSize?: number;
  opacity?: number;
}) {
  const {
    p1,
    p2,
    lineWidth = 1,
    color = 'black',
    gapColor,
    dashSize = 0,
    gapSize = dashSize,
    opacity = 1,
    zIndex = 0,
  } = props;

  const materialRef = useRef<LineWithDataDashMaterial>(null);

  const linearVisGroup = useLinearVisGroup();

  const [shape, origin, direction] = useMemo(() => {
    const direction = new Vector2(p2[0] - p1[0], p2[1] - p1[1]).normalize();
    const n = new Vector2(-direction.y, direction.x);
    const n2 = n.multiplyScalar(lineWidth + 1);
    const shape = new Shape();
    shape.moveTo(p1[0] - n2.x, p1[1] - n2.y);
    shape.lineTo(p1[0] + n2.x, p1[1] + n2.y);
    shape.lineTo(p2[0] + n2.x, p2[1] + n2.y);
    shape.lineTo(p2[0] - n2.x, p2[1] - n2.y);
    shape.closePath();
    return [shape, new Vector2(p1[0], p1[1]), direction];
  }, [p1, p2, lineWidth]);

  useEffect(() => {
    const vColor = color4(color, opacity);
    const vGapColor = color4(gapColor ?? 'transparent', opacity);
    if (materialRef.current) {
      const uniforms = materialRef.current.uniforms;
      uniforms.color.value = vColor;
      uniforms.gapColor.value = vGapColor;
      uniforms.dashSize.value = dashSize;
      uniforms.gapSize.value = gapSize;
      uniforms.origin.value = origin;
      uniforms.direction.value = direction;
      uniforms.lineWidth.value = lineWidth;
      invalidate();
    }
  }, [
    color,
    gapColor,
    dashSize,
    gapSize,
    opacity,
    origin,
    direction,
    lineWidth,
  ]);

  useFrame(({ camera }) => {
    if (materialRef.current === null) {
      return;
    }
    const scaleX = camera.scale.x / linearVisGroup.sx;
    const scaleY = camera.scale.y / linearVisGroup.sy;
    const uniforms = materialRef.current.uniforms;
    uniforms.scaleX.value = scaleX;
    uniforms.scaleY.value = scaleY;
    invalidate();
  });

  return (
    <group position-z={zIndex}>
      <mesh>
        <ambientLight />
        <shapeGeometry attach="geometry" args={[shape]} />
        {/* @ts-expect-error */}
        <lineWithDataDashMaterial
          attach="material"
          transparent
          ref={materialRef}
        />
      </mesh>
    </group>
  );
}
