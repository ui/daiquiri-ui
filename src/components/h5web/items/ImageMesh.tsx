import type { NdArray } from 'ndarray';
import { forwardRef, useImperativeHandle, useRef } from 'react';
import type { TextureFilter } from 'three';
import type { ColorMap, Domain, VisScaleType } from '@h5web/lib';
import { ScaleType } from '@h5web/lib';
import { HeatmapMesh } from '@h5web/lib';
import { NdNormalizedUint8Array } from '../NdNormalizedUint8Array';
import { NdFloat16Array } from '../NdFloat16Array';
import ndarray from 'ndarray';

export interface ImageMeshRef {
  pick: (dataX: number, dataY: number) => number | undefined;
}

/**
 * FIXME: This have to be exposed by h5web
 */
declare type TextureSafeTypedArray =
  | Float32Array
  | Uint8Array
  | Uint8ClampedArray;

interface Props {
  values:
    | NdArray<TextureSafeTypedArray | Uint16Array>
    | NdNormalizedUint8Array
    | NdFloat16Array;
  domain: Domain;
  scaleType: VisScaleType;
  colorMap: ColorMap;
  invertColorMap?: boolean;
  magFilter?: TextureFilter;
  alphaValues?: NdArray<TextureSafeTypedArray | Uint16Array>;
  alphaDomain?: Domain;
  position: [number, number, number];
  scale: [number, number, number];
  size: { width: number; height: number };
}

function convertPositionToIndex(
  dataX: number,
  dataY: number,
  transformation: {
    position: [number, number, number];
    size: { width: number; height: number };
    scale: [number, number, number];
  }
): [number, number] | undefined {
  const cx = dataX - transformation.position[0];
  const cy = dataY - transformation.position[1];
  const px = Math.round(
    cx / transformation.scale[0] + transformation.size.width * 0.5 - 0.5
  );
  const py = Math.round(
    cy / transformation.scale[1] + transformation.size.height * 0.5 - 0.5
  );
  if (px < 0 || px >= transformation.size.width) {
    return undefined;
  }
  if (py < 0 || py >= transformation.size.width) {
    return undefined;
  }
  return [py, px];
}

const ImageMesh = forwardRef<ImageMeshRef, Props>((props, ref) => {
  const { values } = props;
  useImperativeHandle(
    ref,
    () => ({
      pick: (dataX: number, dataY: number) => {
        const index = convertPositionToIndex(dataX, dataY, props);
        if (index === undefined) {
          return undefined;
        }
        return values.get(...index);
      },
    }),
    [props.position, props.scale, values, props.size]
  );

  if (values instanceof NdNormalizedUint8Array) {
    const array = ndarray(values.data, values.shape);
    // FIXME: instead of this domain it is still possible to refine a normalized one
    return (
      <HeatmapMesh
        {...props}
        values={array}
        domain={[0, 255]}
        scaleType={ScaleType.Linear}
      />
    );
  }
  if (values instanceof NdFloat16Array) {
    const array = ndarray(values.data, values.shape);
    return <HeatmapMesh {...props} values={array} />;
  }
  return <HeatmapMesh {...props} values={values} />;
});

export default ImageMesh;
