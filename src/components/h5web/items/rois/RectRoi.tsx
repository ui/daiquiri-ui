import React from 'react';
import { Anchor } from './Anchor';
import { DraggableRect } from './DraggableRect';
import { HSegmentRoi } from './HSegmentRoi';
import { VSegmentRoi } from './VSegmentRoi';
import type { RectRoiGeometry } from './geometries';

export function RectRoi(props: {
  geometry: RectRoiGeometry;
  zIndex?: number;
  lineWidth?: number;
  color?: string;
  opacity?: number;
  readOnly?: boolean;
  visible?: boolean;
  onGeometryChanged?: (geometry: RectRoiGeometry, dragging: boolean) => void;
}) {
  const {
    geometry: pos,
    lineWidth = 2,
    color = 'blue',
    opacity = 1,
    zIndex = 1,
    visible = true,
    readOnly = props.onGeometryChanged === undefined,
  } = props;

  if (!visible) {
    return <></>;
  }

  const coord = pos.x1 < pos.x2 === pos.y1 < pos.y2;
  const pointer11 = coord ? 'nesw-resize' : 'nwse-resize';
  const pointer12 = !coord ? 'nesw-resize' : 'nwse-resize';

  function x1y1Changed(localPos: { x: number; y: number }, dragging: boolean) {
    const { x: x1, y: y1 } = localPos;
    props.onGeometryChanged?.({ ...pos, x1, y1 }, dragging);
  }

  function x1y2Changed(localPos: { x: number; y: number }, dragging: boolean) {
    const { x: x1, y: y2 } = localPos;
    props.onGeometryChanged?.({ ...pos, x1, y2 }, dragging);
  }

  function x2y1Changed(localPos: { x: number; y: number }, dragging: boolean) {
    const { x: x2, y: y1 } = localPos;
    props.onGeometryChanged?.({ ...pos, x2, y1 }, dragging);
  }

  function x2y2Changed(localPos: { x: number; y: number }, dragging: boolean) {
    const { x: x2, y: y2 } = localPos;
    props.onGeometryChanged?.({ ...pos, x2, y2 }, dragging);
  }

  function x1Changed(localPos: { x: number }, dragging: boolean) {
    const { x } = localPos;
    props.onGeometryChanged?.({ ...pos, x1: x }, dragging);
  }

  function x2Changed(localPos: { x: number }, dragging: boolean) {
    const { x } = localPos;
    props.onGeometryChanged?.({ ...pos, x2: x }, dragging);
  }

  function y1Changed(localPos: { y: number }, dragging: boolean) {
    const { y } = localPos;
    props.onGeometryChanged?.({ ...pos, y1: y }, dragging);
  }

  function y2Changed(localPos: { y: number }, dragging: boolean) {
    const { y } = localPos;
    props.onGeometryChanged?.({ ...pos, y2: y }, dragging);
  }

  function xyChanged(newPos: [number, number], dragging: boolean) {
    const { x1, x2, y1, y2 } = pos;
    props.onGeometryChanged?.(
      {
        x1: newPos[0],
        y1: newPos[1],
        x2: newPos[0] + (x2 - x1),
        y2: newPos[1] + (y2 - y1),
      },
      dragging
    );
  }

  const x0 = (pos.x1 + pos.x2) * 0.5;
  const y0 = (pos.y1 + pos.y2) * 0.5;
  const width = Math.abs(pos.x1 - pos.x2);
  const height = Math.abs(pos.y1 - pos.y2);

  return (
    <>
      <DraggableRect
        pos={[pos.x1, pos.y1]}
        displayPos={[x0, y0]}
        size={[width, height]}
        pointer="move"
        zIndex={zIndex}
        enabled={!readOnly}
        onPositionChanged={xyChanged}
      />
      <HSegmentRoi
        geometry={{
          y: pos.y1,
          x1: pos.x1,
          x2: pos.x2,
        }}
        zIndex={zIndex}
        color={color}
        opacity={opacity}
        readOnly={readOnly}
        lineWidth={lineWidth}
        onGeometryChanged={y1Changed}
      />
      <HSegmentRoi
        geometry={{
          y: pos.y2,
          x1: pos.x1,
          x2: pos.x2,
        }}
        zIndex={zIndex}
        color={color}
        opacity={opacity}
        readOnly={readOnly}
        lineWidth={lineWidth}
        onGeometryChanged={y2Changed}
      />
      <VSegmentRoi
        geometry={{
          x: pos.x1,
          y1: pos.y1,
          y2: pos.y2,
        }}
        zIndex={zIndex}
        color={color}
        opacity={opacity}
        readOnly={readOnly}
        lineWidth={lineWidth}
        onGeometryChanged={x1Changed}
      />
      <VSegmentRoi
        geometry={{
          x: pos.x2,
          y1: pos.y1,
          y2: pos.y2,
        }}
        zIndex={zIndex}
        color={color}
        opacity={opacity}
        readOnly={readOnly}
        lineWidth={lineWidth}
        onGeometryChanged={x2Changed}
      />
      {!readOnly && (
        <>
          <Anchor
            geometry={{ x: pos.x1, y: pos.y1 }}
            pointer={pointer11}
            zIndex={zIndex}
            color={color}
            opacity={opacity}
            readOnly={readOnly}
            onGeometryChanged={x1y1Changed}
          />
          <Anchor
            geometry={{ x: pos.x1, y: pos.y2 }}
            pointer={pointer12}
            zIndex={zIndex}
            color={color}
            opacity={opacity}
            readOnly={readOnly}
            onGeometryChanged={x1y2Changed}
          />
          <Anchor
            geometry={{ x: pos.x2, y: pos.y1 }}
            pointer={pointer12}
            zIndex={zIndex}
            color={color}
            opacity={opacity}
            readOnly={readOnly}
            onGeometryChanged={x2y1Changed}
          />
          <Anchor
            geometry={{ x: pos.x2, y: pos.y2 }}
            pointer={pointer11}
            zIndex={zIndex}
            color={color}
            opacity={opacity}
            readOnly={readOnly}
            onGeometryChanged={x2y2Changed}
          />
        </>
      )}
    </>
  );
}
