import React, { useState } from 'react';
import { ScreenScale } from '../ScreenScale';
import { SNAP, ACTIVE_LINE_WIDTH, ZINDEX_SHIFT_LINE } from './const';
import { DraggableRect } from './DraggableRect';
import type { HLineRoiGeometry } from './geometries';

export function HLineRoi(props: {
  geometry: HLineRoiGeometry;
  zIndex?: number;
  lineWidth?: number;
  color?: string;
  opacity?: number;
  readOnly?: boolean;
  visible?: boolean;
  onGeometryChanged?: (geometry: HLineRoiGeometry, dragging: boolean) => void;
}) {
  const {
    geometry,
    lineWidth = 2,
    color = 'blue',
    opacity = 1,
    zIndex = 1,
    visible = true,
    readOnly = props.onGeometryChanged === undefined,
  } = props;

  const [active, setActive] = useState(false);

  if (!visible) {
    return <></>;
  }

  return (
    <group position={[0, geometry.y, zIndex]}>
      <ScreenScale>
        <DraggableRect
          pos={[0, geometry.y]}
          size={[999_999, SNAP]}
          pointer="ns-resize"
          zIndex={zIndex + ZINDEX_SHIFT_LINE}
          enabled={!readOnly}
          onPositionChanged={(pos, dragging) => {
            props.onGeometryChanged?.({ y: pos[1] }, dragging);
          }}
          onEnter={() => {
            setActive(true);
          }}
          onLeave={() => {
            setActive(false);
          }}
        />
      </ScreenScale>
      <group>
        <ScreenScale>
          <mesh>
            <ambientLight />
            <planeGeometry
              attach="geometry"
              args={[999_999, active ? ACTIVE_LINE_WIDTH : lineWidth, 1, 1]}
            />
            <meshBasicMaterial
              attach="material"
              transparent
              color={color}
              opacity={opacity}
            />
          </mesh>
        </ScreenScale>
      </group>
    </group>
  );
}
