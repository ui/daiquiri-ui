export const SNAP = 10;

export const DEBUG = false;

export const ACTIVE_LINE_WIDTH = 3.5;

export const ANCHOR_SIZE = 10;

export const ZINDEX_SHIFT_ANCHOR = 0.03;
export const ZINDEX_SHIFT_SEGMENT = 0.02;
export const ZINDEX_SHIFT_LINE = 0.01;
