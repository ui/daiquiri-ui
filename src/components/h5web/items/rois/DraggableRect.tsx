import React, { useRef } from 'react';
import { Vector3 } from 'three';
import { useGesture } from '@use-gesture/react';
import { useLinearVisCanvas } from '../../LinearVisCanvas2';
import { useVisCanvasContext } from '@h5web/lib';
import { useThree } from '@react-three/fiber';
import { DEBUG } from './const';

/**
 * Virtual rectangle which is draggable.
 *
 * It was designed to be used to design ROIs and should not be used
 * in other context.
 *
 * @pos: An informative initial position for the gesture
 * @displayPos: A centered position for the region, usually `[0, 0]`.
 */
export function DraggableRect(props: {
  pos: [number, number];
  displayPos?: [number, number];
  zIndex: number;
  size: [number, number];
  enabled?: boolean;
  pointer?: string;
  onPositionChanged?: (pos: [number, number], dragging: boolean) => void;
  onEnter?: () => void;
  onLeave?: () => void;
}) {
  const {
    pos,
    size,
    zIndex,
    pointer = 'pointer',
    enabled = true,
    displayPos = [0, 0],
  } = props;

  const linearVisCanvas = useLinearVisCanvas();

  const gestureRef = useRef<
    | {
        // Offset in data space between the mouse and the object to drag
        dataOffset: [number, number];
        // Identifier of this interaction
        uid: string;
      }
    | undefined
  >(undefined);
  const { htmlToData } = useVisCanvasContext();
  const { camera } = useThree();

  const bind = useGesture(
    {
      onHover: ({ hovering }) => {
        if (!linearVisCanvas) return;
        if (hovering) {
          if (gestureRef.current?.uid) {
            // When a gesture move outside of the window
            // some weird hover events are reemitted
            return;
          }
          gestureRef.current = {
            uid: linearVisCanvas.mouseInteractionOverlay.register(
              zIndex,
              pointer,
              props.onEnter,
              props.onLeave
            ),
            dataOffset: [0, 0],
          };
        } else if (gestureRef.current) {
          const uid = gestureRef.current.uid;
          if (linearVisCanvas.mouseInteractionOverlay.isCurrent(uid)) {
            // When a gesture moves outside of the window
            // some weird hover events are reemitted
          } else {
            gestureRef.current = undefined;
            linearVisCanvas.mouseInteractionOverlay.unregister(uid);
          }
        }
      },
      onDragStart: ({ cancel, xy }) => {
        if (!linearVisCanvas) return;
        if (!gestureRef.current) {
          cancel();
          return;
        }
        const uid = gestureRef.current.uid;
        const captured = linearVisCanvas.mouseInteractionOverlay.capture(uid);
        if (!captured) {
          cancel();
          return;
        }
        const mouseData = htmlToData(camera, new Vector3(xy[0], xy[1], 0));
        gestureRef.current.dataOffset = [
          pos[0] - mouseData.x,
          pos[1] - mouseData.y,
        ];
      },
      onDrag: ({ cancel, xy, offset: [dx, dy], intentional }) => {
        if (!linearVisCanvas) return;
        if (!gestureRef.current) {
          cancel();
          return;
        }
        const uid = gestureRef.current.uid;
        if (!linearVisCanvas.mouseInteractionOverlay.isCurrent(uid)) {
          cancel();
          return;
        }
        const mouseData = htmlToData(camera, new Vector3(xy[0], xy[1], 0));
        props.onPositionChanged?.(
          [
            mouseData.x + gestureRef.current.dataOffset[0],
            mouseData.y + gestureRef.current.dataOffset[1],
          ],
          true
        );
      },
      onDragEnd: ({ cancel, xy }) => {
        if (!linearVisCanvas) return;
        const gesture = gestureRef.current;
        if (!gesture) {
          cancel();
          return;
        }
        if (!linearVisCanvas.mouseInteractionOverlay.isCurrent(gesture.uid)) {
          cancel();
          return;
        }
        linearVisCanvas.mouseInteractionOverlay.release(gesture.uid);
        const mouseData = htmlToData(camera, new Vector3(xy[0], xy[1], 0));
        props.onPositionChanged?.(
          [
            mouseData.x + gesture.dataOffset[0],
            mouseData.y + gesture.dataOffset[1],
          ],
          false
        );
      },
    },
    {
      drag: {
        pointer: {
          capture: true,
          buttons: [1],
        },
      },
    }
  );

  return (
    <mesh
      position={[displayPos[0], displayPos[1], zIndex]}
      {...(enabled ? (bind() as any) : {})}
    >
      <ambientLight />
      <planeGeometry attach="geometry" args={[size[0], size[1], 1, 1]} />
      <meshBasicMaterial
        attach="material"
        transparent
        color={DEBUG ? 'red' : undefined}
        opacity={DEBUG ? 0.25 : 0}
      />
    </mesh>
  );
}
