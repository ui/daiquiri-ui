import React, { useState } from 'react';
import { ScreenScale } from '../ScreenScale';
import { SNAP, ACTIVE_LINE_WIDTH, ZINDEX_SHIFT_LINE } from './const';
import { DraggableRect } from './DraggableRect';
import type { VLineRoiGeometry } from './geometries';

export function VLineRoi(props: {
  geometry: VLineRoiGeometry;
  zIndex?: number;
  lineWidth?: number;
  color?: string;
  opacity?: number;
  readOnly?: boolean;
  visible?: boolean;
  onGeometryChanged?: (geometry: VLineRoiGeometry, dragging: boolean) => void;
}) {
  const {
    geometry,
    lineWidth = 2,
    color = 'blue',
    opacity = 1,
    zIndex = 1,
    visible = true,
    readOnly = props.onGeometryChanged === undefined,
  } = props;

  const [active, setActive] = useState(false);

  if (!visible) {
    return <></>;
  }

  return (
    <group position={[geometry.x, 0, zIndex]}>
      <ScreenScale>
        <DraggableRect
          pos={[geometry.x, 0]}
          size={[SNAP, 999_999]}
          pointer="ew-resize"
          zIndex={zIndex + ZINDEX_SHIFT_LINE}
          enabled={!readOnly}
          onPositionChanged={(pos, dragging) => {
            props.onGeometryChanged?.({ x: pos[0] }, dragging);
          }}
          onEnter={() => {
            setActive(true);
          }}
          onLeave={() => {
            setActive(false);
          }}
        />
      </ScreenScale>
      <group>
        <ScreenScale>
          <mesh>
            <ambientLight />
            <planeGeometry
              attach="geometry"
              args={[active ? ACTIVE_LINE_WIDTH : lineWidth, 999_999, 1, 1]}
            />
            <meshBasicMaterial
              attach="material"
              transparent
              color={color}
              opacity={opacity}
            />
          </mesh>
        </ScreenScale>
      </group>
    </group>
  );
}
