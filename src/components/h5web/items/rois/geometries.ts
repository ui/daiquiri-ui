export interface RoiGeometry {}

export interface PointRoiGeometry extends RoiGeometry {
  x: number;
  y: number;
}

export interface VLineRoiGeometry extends RoiGeometry {
  x: number;
}

export interface HLineRoiGeometry extends RoiGeometry {
  y: number;
}

export interface PointRoiGeometry extends RoiGeometry {
  x: number;
  y: number;
}

export interface RectRoiGeometry extends RoiGeometry {
  x1: number;
  y1: number;
  x2: number;
  y2: number;
}

export interface AngleRoiGeometry {
  x: number;
  y: number;
  radius: number;
  /* Angle, centered to the arc, in degree */
  angle: number;
  /* Angle range, in degree */
  angleRange: number;
}
