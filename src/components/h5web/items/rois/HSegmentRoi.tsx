import React, { useState } from 'react';
import { ScreenScale } from '../ScreenScale';
import { SNAP, ACTIVE_LINE_WIDTH, ZINDEX_SHIFT_SEGMENT } from './const';
import { DraggableRect } from './DraggableRect';
import type { HLineRoiGeometry } from './geometries';

export function HSegmentRoi(props: {
  geometry: HLineRoiGeometry & {
    x1: number;
    x2: number;
  };
  zIndex?: number;
  lineWidth?: number;
  color?: string;
  opacity?: number;
  readOnly?: boolean;
  visible?: boolean;
  onGeometryChanged?: (geometry: HLineRoiGeometry, dragging: boolean) => void;
}) {
  const {
    geometry,
    lineWidth = 2,
    color = 'blue',
    opacity = 1,
    zIndex = 1,
    visible = true,
    readOnly = props.onGeometryChanged === undefined,
  } = props;
  const { y, x1, x2 } = geometry;
  const [active, setActive] = useState(false);
  const width = Math.abs(x1 - x2);
  const x0 = (x1 + x2) * 0.5;

  if (!visible) {
    return <></>;
  }

  return (
    <group position={[x0, y, zIndex]}>
      <ScreenScale scaleX={false}>
        <DraggableRect
          pos={[0, y]}
          size={[width, SNAP]}
          pointer="ns-resize"
          zIndex={zIndex + ZINDEX_SHIFT_SEGMENT}
          enabled={!readOnly}
          onPositionChanged={(pos, dragging) => {
            props.onGeometryChanged?.({ y: pos[1] }, dragging);
          }}
          onEnter={() => {
            setActive(true);
          }}
          onLeave={() => {
            setActive(false);
          }}
        />
      </ScreenScale>
      <group>
        <ScreenScale scaleX={false}>
          <mesh>
            <ambientLight />
            <planeGeometry
              attach="geometry"
              args={[width, active ? ACTIVE_LINE_WIDTH : lineWidth, 1, 1]}
            />
            <meshBasicMaterial
              attach="material"
              transparent
              color={color}
              opacity={opacity}
            />
          </mesh>
        </ScreenScale>
      </group>
    </group>
  );
}
