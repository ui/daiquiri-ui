import { useCallback } from 'react';
import { Annotation } from '@h5web/lib';
import { LineWithDataDash } from '../shapes/LineWithDataDash';
import { Vector2 } from 'three';
import { Anchor } from './Anchor';
import { Arc } from '../shapes/Arc';
import type { AngleRoiGeometry, PointRoiGeometry } from './geometries';
import { angularDist } from 'helpers/math';

export function AngleRoi(props: {
  geometry: AngleRoiGeometry;
  onGeometryChanged?: (geometry: AngleRoiGeometry, dragging: boolean) => void;
  zIndex?: number;
  lineWidth?: number;
  color?: string;
  opacity?: number;
  readOnly?: boolean;
  visible?: boolean;
}) {
  const {
    geometry,
    onGeometryChanged,
    zIndex,
    lineWidth = 1,
    color = 'blue',
    opacity = 1,
    readOnly = false,
    visible = true,
  } = props;
  const { x: x0, y: y0, angle, angleRange, radius } = geometry;

  const radAngle = (angle * Math.PI) / 180;
  const radAngle1 = ((angle - angleRange * 0.5) * Math.PI) / 180;
  const radAngle2 = ((angle + angleRange * 0.5) * Math.PI) / 180;

  const point1 = {
    x: x0 + Math.sin(radAngle1) * radius,
    y: y0 + Math.cos(radAngle1) * radius,
  };
  const point2 = {
    x: x0 + Math.sin(radAngle2) * radius,
    y: y0 + Math.cos(radAngle2) * radius,
  };
  const center = {
    x: x0 + Math.sin(radAngle) * radius * 0.5,
    y: y0 + Math.cos(radAngle) * radius * 0.5,
  };

  const text = `${angleRange.toFixed(2)} deg`;
  const deg10 = (10 * radius) / (Math.PI * 18);

  const onPoint1Change = useCallback(
    (anchor: PointRoiGeometry, dragging: boolean) => {
      const v = new Vector2(anchor.x - geometry.x, anchor.y - geometry.y);
      const angle = (Math.atan2(v.x, v.y) * 180) / Math.PI;
      const delta = angularDist(
        geometry.angle - geometry.angleRange * 0.5,
        angle
      );
      onGeometryChanged?.(
        {
          ...geometry,
          radius: v.length(),
          angle: geometry.angle + delta * 0.5,
          angleRange: geometry.angleRange - delta,
        },
        dragging
      );
    },
    [geometry, onGeometryChanged]
  );

  const onPoint2Change = useCallback(
    (anchor: PointRoiGeometry, dragging: boolean) => {
      const v = new Vector2(anchor.x - geometry.x, anchor.y - geometry.y);
      const angle = (Math.atan2(v.x, v.y) * 180) / Math.PI;
      const delta = angularDist(
        geometry.angle + geometry.angleRange * 0.5,
        angle
      );
      onGeometryChanged?.(
        {
          ...geometry,
          radius: v.length(),
          angle: geometry.angle + delta * 0.5,
          angleRange: geometry.angleRange + delta,
        },
        dragging
      );
    },
    [geometry, onGeometryChanged]
  );

  if (!visible) {
    return <></>;
  }

  return (
    <>
      <Arc
        center={[x0, y0]}
        radius={radius}
        startAngle={angle - angleRange * 0.5}
        angleRange={angleRange}
        lineWidth={lineWidth}
        color={color}
        opacity={opacity}
        gapColor="#F0F0F0"
        dashSize={deg10}
        gapSize={deg10}
        zIndex={zIndex}
      />
      <LineWithDataDash
        p1={[x0, y0]}
        p2={[point1.x, point1.y]}
        lineWidth={lineWidth}
        color={color}
        opacity={opacity}
        zIndex={zIndex}
      />
      <LineWithDataDash
        p1={[x0, y0]}
        p2={[point2.x, point2.y]}
        lineWidth={lineWidth}
        color={color}
        opacity={opacity}
        zIndex={zIndex}
      />
      <Anchor
        geometry={{ x: x0, y: y0 }}
        onGeometryChanged={(geo, dragging) => {
          onGeometryChanged?.(
            { ...props.geometry, x: geo.x, y: geo.y },
            dragging
          );
        }}
        color={color}
        readOnly={readOnly}
        opacity={opacity}
        zIndex={zIndex}
      />
      <Anchor
        geometry={point1}
        onGeometryChanged={onPoint1Change}
        color={color}
        readOnly={readOnly}
        opacity={opacity}
        zIndex={zIndex}
      />
      <Anchor
        geometry={point2}
        onGeometryChanged={onPoint2Change}
        color={color}
        readOnly={readOnly}
        opacity={opacity}
        zIndex={zIndex}
      />
      <Annotation x={center.x} y={center.y} style={{ zIndex: 100 }} center>
        <div
          className="text-light p-1 rounded"
          style={{ backgroundColor: '#000000A0' }}
        >
          {text}
        </div>
      </Annotation>
    </>
  );
}
