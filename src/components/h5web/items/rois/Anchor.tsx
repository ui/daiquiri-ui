import React, { useState } from 'react';
import { ScreenScale } from '../ScreenScale';
import {
  SNAP,
  ACTIVE_LINE_WIDTH,
  ANCHOR_SIZE,
  ZINDEX_SHIFT_ANCHOR,
} from './const';
import { DraggableRect } from './DraggableRect';
import type { PointRoiGeometry } from './geometries';

export function Anchor(props: {
  geometry: PointRoiGeometry;
  zIndex?: number;
  color?: string;
  opacity?: number;
  readOnly?: boolean;
  visible?: boolean;
  pointer?: string;
  onGeometryChanged?: (geometry: PointRoiGeometry, dragging: boolean) => void;
}) {
  const {
    geometry,
    color = 'blue',
    opacity = 1,
    zIndex = 1,
    visible = true,
    pointer = 'pointer',
    readOnly = props.onGeometryChanged === undefined,
  } = props;

  const [active, setActive] = useState(false);

  if (!visible) {
    return <></>;
  }

  return (
    <group position={[geometry.x, geometry.y, zIndex]}>
      <ScreenScale>
        <DraggableRect
          pos={[geometry.x, geometry.y]}
          size={[ANCHOR_SIZE + SNAP, ANCHOR_SIZE + SNAP]}
          pointer={pointer}
          zIndex={zIndex + ZINDEX_SHIFT_ANCHOR}
          enabled={!readOnly}
          onPositionChanged={(pos, dragging) => {
            props.onGeometryChanged?.({ x: pos[0], y: pos[1] }, dragging);
          }}
          onEnter={() => {
            setActive(true);
          }}
          onLeave={() => {
            setActive(false);
          }}
        />
      </ScreenScale>
      <group>
        <ScreenScale>
          <mesh>
            <ambientLight />
            <planeGeometry
              attach="geometry"
              args={[
                ANCHOR_SIZE + (active ? ACTIVE_LINE_WIDTH : 0),
                ANCHOR_SIZE + (active ? ACTIVE_LINE_WIDTH : 0),
                1,
                1,
              ]}
            />
            <meshBasicMaterial
              attach="material"
              transparent
              color={color}
              opacity={opacity}
            />
          </mesh>
        </ScreenScale>
      </group>
    </group>
  );
}
