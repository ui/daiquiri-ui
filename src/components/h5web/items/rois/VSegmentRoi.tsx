import React, { useState } from 'react';
import { ScreenScale } from '../ScreenScale';
import { SNAP, ACTIVE_LINE_WIDTH, ZINDEX_SHIFT_SEGMENT } from './const';
import { DraggableRect } from './DraggableRect';
import type { VLineRoiGeometry } from './geometries';

export function VSegmentRoi(props: {
  geometry: VLineRoiGeometry & {
    y1: number;
    y2: number;
  };
  zIndex?: number;
  lineWidth?: number;
  color?: string;
  opacity?: number;
  readOnly?: boolean;
  visible?: boolean;
  onGeometryChanged?: (geometry: VLineRoiGeometry, dragging: boolean) => void;
}) {
  const {
    geometry,
    lineWidth = 2,
    color = 'blue',
    opacity = 1,
    zIndex = 1,
    visible = true,
    readOnly = props.onGeometryChanged === undefined,
  } = props;

  const [active, setActive] = useState(false);
  const { x, y1, y2 } = geometry;
  const height = Math.abs(y1 - y2);
  const y0 = (y1 + y2) * 0.5;

  if (!visible) {
    return <></>;
  }

  return (
    <group position={[x, y0, zIndex]}>
      <ScreenScale scaleY={false}>
        <DraggableRect
          pos={[x, 0]}
          size={[SNAP, height]}
          pointer="ew-resize"
          zIndex={zIndex + ZINDEX_SHIFT_SEGMENT}
          enabled={!readOnly}
          onPositionChanged={(pos, dragging) => {
            props.onGeometryChanged?.({ x: pos[0] }, dragging);
          }}
          onEnter={() => {
            setActive(true);
          }}
          onLeave={() => {
            setActive(false);
          }}
        />
      </ScreenScale>
      <group>
        <ScreenScale scaleY={false}>
          <mesh>
            <ambientLight />
            <planeGeometry
              attach="geometry"
              args={[active ? ACTIVE_LINE_WIDTH : lineWidth, height, 1, 1]}
            />
            <meshBasicMaterial
              attach="material"
              transparent
              color={color}
              opacity={opacity}
            />
          </mesh>
        </ScreenScale>
      </group>
    </group>
  );
}
