import { useCameraState, useVisCanvasContext } from '@h5web/lib';
import React from 'react';
import { ScreenLocation } from './ScreenLocation';
import { ScreenScale } from './ScreenScale';
import { Annotation } from '@h5web/lib';
import Qty from 'js-quantities';
import { toHumanReadableUnit } from '../../../helpers/QtyHelper';

/**
 * Find a closed smaller number which is much more human readable.
 *
 * Select a number like 1, 2.5, 5
 */
function normalizeNumber(value: number): number {
  const digits = Math.ceil(Math.log10(value));
  const pow = 10 ** digits;
  const norm = value / pow;
  if (norm >= 1) {
    return Number(pow);
  }
  if (norm >= 0.5) {
    return 0.5 * pow;
  }
  if (norm >= 0.25) {
    return 0.25 * pow;
  }
  if (norm >= 0.1) {
    return 0.1 * pow;
  }
  return value;
}

export function SceneScale(props: {
  unit: string;
  percentWidth?: number;
  percentLocation?: number;
}) {
  const { unit, percentWidth = 30, percentLocation = 75 } = props;
  const visCanvasContext = useVisCanvasContext();
  const { width } = visCanvasContext.canvasSize;

  const state = useCameraState(
    (camera) => {
      const visibleDomain = visCanvasContext.getVisibleDomains(camera);
      const { xVisibleDomain, yVisibleDomain } = visibleDomain;
      const xDomain = xVisibleDomain[1] - xVisibleDomain[0];
      const sx = width / Math.abs(xDomain);
      const dataRange = normalizeNumber(
        (Math.abs(xDomain) * percentWidth) / 100
      );
      const pixelRange = dataRange * sx;
      const dataX = xVisibleDomain[0] + (xDomain * percentLocation) / 100;
      const dataY = yVisibleDomain[1];

      function formatUnit(dataRange: number, unit: string) {
        if (unit === 'px') {
          return `${dataRange} ${unit}`;
        }
        const q = toHumanReadableUnit(new Qty(dataRange, 'mm'));
        return `${q.toPrec(0.1)}`;
      }

      const text = formatUnit(dataRange, unit);
      return {
        pixelRange,
        dataRange,
        dataX,
        dataY,
        text,
      };
    },
    [unit, width, percentWidth, percentLocation]
  );

  const lineWidth = 2;
  const tickHeight = 10;
  return (
    <>
      <Annotation x={state.dataX} y={state.dataY} style={{ color: 'black' }}>
        <div
          style={{
            top: '50%',
            left: '50%',
            position: 'relative',
            transform: 'translate(-100%, 0%)',
            borderRadius: 5,
            paddingLeft: 5,
            paddingRight: 5,
            backgroundColor: '#FFFFFF80',
            fontWeight: 'bold',
            fontSize: '100%',
            zIndex: 100,
          }}
        >
          {state.text}
        </div>
      </Annotation>
      <ScreenLocation percentLocation={[percentLocation, 0]}>
        <ScreenScale>
          <group position-y={26}>
            <mesh position={[0, 0, 0]}>
              <ambientLight />
              <planeGeometry
                attach="geometry"
                args={[state.pixelRange, lineWidth + 2, 1, 1]}
              />
              <meshBasicMaterial
                attach="material"
                transparent
                color="white"
                opacity={1}
              />
            </mesh>
            <mesh position={[state.pixelRange * 0.5, 0, 0]}>
              <ambientLight />
              <planeGeometry
                attach="geometry"
                args={[lineWidth + 2, tickHeight + 2, 1, 1]}
              />
              <meshBasicMaterial
                attach="material"
                transparent
                color="white"
                opacity={1}
              />
            </mesh>
            <mesh position={[-state.pixelRange * 0.5, 0, 0]}>
              <ambientLight />
              <planeGeometry
                attach="geometry"
                args={[lineWidth + 2, tickHeight + 2, 1, 1]}
              />
              <meshBasicMaterial
                attach="material"
                transparent
                color="white"
                opacity={1}
              />
            </mesh>
            <mesh position={[0, 0, 0]}>
              <ambientLight />
              <planeGeometry
                attach="geometry"
                args={[state.pixelRange, lineWidth, 1, 1]}
              />
              <meshBasicMaterial
                attach="material"
                transparent
                color="black"
                opacity={1}
              />
            </mesh>
            <mesh position={[state.pixelRange * 0.5, 0, 0]}>
              <ambientLight />
              <planeGeometry
                attach="geometry"
                args={[lineWidth, tickHeight, 1, 1]}
              />
              <meshBasicMaterial
                attach="material"
                transparent
                color="black"
                opacity={1}
              />
            </mesh>
            <mesh position={[-state.pixelRange * 0.5, 0, 0]}>
              <ambientLight />
              <planeGeometry
                attach="geometry"
                args={[lineWidth, tickHeight, 1, 1]}
              />
              <meshBasicMaterial
                attach="material"
                transparent
                color="black"
                opacity={1}
              />
            </mesh>
          </group>
        </ScreenScale>
      </ScreenLocation>
    </>
  );
}
