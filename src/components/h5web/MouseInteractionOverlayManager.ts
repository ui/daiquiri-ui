export class MouseInteractionOverlayManager {
  /**
   * Counter to manage unique identifiers.
   */
  private counter: number;

  /**
   * Hold the list of available interactions.
   */
  private hoveredRois: Record<
    string,
    {
      priority: number;
      pointer: string;
      onEnter?: () => void;
      onLeave?: () => void;
    }
  >;

  /**
   * The curretn active mouse interaction.
   */
  private currentUid: string | undefined;

  /**
   * The top priority mouse interaction.
   */
  private topUid: string | undefined;

  /**
   * If true, the overview cursor is disabled.
   */
  private disabled: boolean;

  /**
   * Callback when the mouse interaction change.
   *
   * Returns the UID of the interaction.
   */
  private readonly onMouseInteractionOverlayChanged:
    | ((uid: string | undefined) => void)
    | undefined;

  /**
   * Callback to setup the reauired pointer.
   */
  private readonly setCursor:
    | ((pointer: string | undefined) => void)
    | undefined;

  public constructor(
    onMouseInteractionOverlayChanged:
      | ((uid: string | undefined) => void)
      | undefined,
    setCursor: ((pointer: string | undefined) => void) | undefined
  ) {
    this.counter = 1;
    this.currentUid = undefined;
    this.hoveredRois = {};
    this.onMouseInteractionOverlayChanged = onMouseInteractionOverlayChanged;
    this.setCursor = setCursor;
    this.disabled = false;
  }

  public setDisabled(disabled: boolean) {
    this.disabled = disabled;
    if (disabled && this.currentUid) {
      this.release(this.currentUid);
    }
    this.updateTopUid();
  }

  /**
   * Register an available interaction
   */
  public register(
    priority: number,
    pointer: string,
    onEnter?: () => void,
    onLeave?: () => void
  ): string {
    const uid = `interaction_${this.counter++}`;
    this.hoveredRois[uid] = {
      priority,
      pointer,
      onEnter,
      onLeave,
    };
    this.updateTopUid();
    return uid;
  }

  /**
   * Unregister an available interaction
   */
  public unregister(uid: string) {
    const desc = this.hoveredRois[uid];
    if (desc !== undefined) {
      delete this.hoveredRois[uid]; // eslint-disable-line @typescript-eslint/no-dynamic-delete
      if (this.topUid === uid) {
        desc.onLeave?.();
        this.topUid = undefined;
      }
      this.updateTopUid();
    }
  }

  private setCurrent(uid: string | undefined) {
    this.currentUid = uid;
    this.updateCursor();
  }

  /**
   * Update the top uid, and then update the cursor.
   */
  private updateTopUid() {
    const uid = this.disabled ? undefined : this.computeTopUid();
    if (this.topUid !== uid) {
      if (this.currentUid === undefined && this.topUid !== undefined) {
        const desc = this.hoveredRois[this.topUid];
        desc.onLeave?.();
      }
      this.topUid = uid;
      if (this.currentUid === undefined && this.topUid !== undefined) {
        const desc = this.hoveredRois[this.topUid];
        desc.onEnter?.();
      }
    }
    this.onMouseInteractionOverlayChanged?.(uid);
    this.updateCursor();
  }

  /**
   * Update the DOM based on the actual state.
   */
  private updateCursor() {
    const uid = this.currentUid ?? this.topUid;
    if (uid === undefined) {
      this.setCursor?.(undefined);
    } else {
      const desc = this.hoveredRois[uid];
      this.setCursor?.(desc.pointer);
    }
  }

  /**
   * True if a gesture is the current one.
   */
  public isCurrent(uid: string | undefined): boolean {
    if (this.currentUid === undefined) {
      return false;
    }
    return this.currentUid === uid;
  }

  private computeTopUid(): string | undefined {
    let topUid: string | undefined;
    let topPriority: number | undefined;
    for (const [uid, desc] of Object.entries(this.hoveredRois)) {
      if (topPriority === undefined || desc.priority > topPriority) {
        topUid = uid;
        topPriority = desc.priority;
      }
    }
    return topUid;
  }

  /**
   * Capture an interaction until a call to `release`.
   *
   * This returns true if the interaction was captured.
   *
   * The interaction can only be captured if no interaction
   * are already captured and the new one is the top priority
   * one.
   */
  public capture(uid: string) {
    if (this.disabled) {
      return false;
    }
    if (this.currentUid !== undefined) {
      return false;
    }
    if (this.topUid !== uid) {
      return false;
    }
    this.setCurrent(uid);
    return true;
  }

  /**
   * Release a captured interaction.
   */
  public release(uid: string) {
    // FIXME: This could raise if uid is not the actual one
    this.setCurrent(undefined);
  }
}
