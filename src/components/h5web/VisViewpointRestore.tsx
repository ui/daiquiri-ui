import { useMountEffect, useUnmountEffect } from '@react-hookz/web';
import { useThree } from '@react-three/fiber';

import { useVisViewpointToCamera, useGetVisViewpoint } from './hooks';
import type { VisViewpoint } from './models';

interface Props {
  viewpoint?: VisViewpoint;
  setViewpoint: (viewpoint: VisViewpoint) => void;
}

function VisViewpointRestore(props: Props) {
  const { viewpoint, setViewpoint } = props;
  const camera = useThree((state) => state.camera);
  const invalidate = useThree((state) => state.invalidate);
  const getVisViewpoint = useGetVisViewpoint();
  const visViewpointToCamera = useVisViewpointToCamera();

  useMountEffect(() => {
    if (!viewpoint) {
      return;
    }
    const { position, scale } = visViewpointToCamera(viewpoint);
    camera.position.copy(position);
    camera.scale.copy(scale);
    camera.updateMatrixWorld();
    invalidate();
  });

  useUnmountEffect(() => {
    setViewpoint(getVisViewpoint());
  });

  return null;
}

export default VisViewpointRestore;
export type { Props as VisViewpointRestoreProps };
