import { useState } from 'react';
import { Dropdown, SplitButton } from 'react-bootstrap';
import type {
  DefaultMouseMode,
  MouseModeInteraction,
} from '../UseMouseModeInteraction';

type MeasureMouseMode = 'measure-distance' | 'measure-ortho' | 'measure-angle';

export function RulerButton<T>(props: {
  mouseModeInteraction: MouseModeInteraction<T | MeasureMouseMode>;
  disabled?: boolean;
}) {
  const { mouseModeInteraction, disabled = false } = props;
  const { mouseMode, setOrResetMouseMode, setMouseMode } = mouseModeInteraction;
  const [selectedMode, setSelectedMode] =
    useState<MeasureMouseMode>('measure-distance');
  const selectedIcons: Record<MeasureMouseMode, string> = {
    'measure-distance': 'fa-ruler',
    'measure-ortho': 'fa-ruler-combined',
    'measure-angle': 'fam-protractor',
  };
  const selectedIcon = selectedIcons[selectedMode] ?? '';
  const selected =
    mouseMode === 'measure-distance' ||
    mouseMode === 'measure-ortho' ||
    mouseMode === 'measure-angle';

  return (
    <SplitButton
      title={<i className={`fa ${selectedIcon} fa-lg"`} />}
      variant={selected ? 'primary' : 'secondary'}
      align={{ lg: 'start' }}
      autoClose
      onClick={() => {
        setOrResetMouseMode(selectedMode);
      }}
      disabled={disabled}
    >
      <Dropdown.Item
        active={mouseMode === 'measure-distance'}
        onClick={() => {
          setSelectedMode('measure-distance');
          setMouseMode('measure-distance');
        }}
        title="Measure the distance between 2 points"
      >
        <i className="fa fa-ruler fa-lg" /> Measure distance
      </Dropdown.Item>
      <Dropdown.Item
        active={mouseMode === 'measure-ortho'}
        onClick={() => {
          setSelectedMode('measure-ortho');
          setMouseMode('measure-ortho');
        }}
        title="Measure the orthogonal distance between 2 points"
      >
        <i className="fa fa-ruler-combined fa-lg" /> Measure orthogonal
      </Dropdown.Item>
      <Dropdown.Item
        active={mouseMode === 'measure-angle'}
        onClick={() => {
          setSelectedMode('measure-angle');
          setMouseMode('measure-angle');
        }}
        title="Measure an angle"
      >
        <i className="fa fam-protractor fa-lg" /> Measure angle
      </Dropdown.Item>
    </SplitButton>
  );
}
