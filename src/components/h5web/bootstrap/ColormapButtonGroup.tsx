import type { ColorMap } from '@h5web/lib';
import { Button, ButtonGroup } from 'react-bootstrap';
import { LutIcon } from './Icons';

export default function ColormapButtonGroup(props: {
  as?: any;
  className?: string;
  lut: ColorMap;
  onSelectLut: (value: ColorMap) => void;
  disabled?: boolean;
}) {
  const { as = undefined, className } = props;

  function createLutItem(key: ColorMap, title: string) {
    const active = props.lut === key;
    const variant = active ? 'primary' : 'secondary';
    return (
      <Button
        disabled={props.disabled}
        variant={variant}
        onClick={() => {
          props.onSelectLut(key);
        }}
        className="text-nowrap"
        title={title}
        size="sm"
      >
        <LutIcon name={key} />
      </Button>
    );
  }

  return (
    <ButtonGroup as={as} className={className}>
      {createLutItem('Greys', 'Gray')}
      {createLutItem('Viridis', 'Viridis')}
      {createLutItem('Cividis', 'Cividis')}
      {createLutItem('Magma', 'Magma')}
      {createLutItem('Inferno', 'Inferno')}
      {createLutItem('Plasma', 'Plasma')}
    </ButtonGroup>
  );
}
