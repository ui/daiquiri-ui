import { Button, ButtonGroup } from 'react-bootstrap';
import type {
  DefaultMouseMode,
  MouseModeInteraction,
} from '../UseMouseModeInteraction';

/**
 * Widget to select the mouse mode for the tomo view
 */
export default function DefaultMouseModeOptions<T>(props: {
  mouseModeInteraction: MouseModeInteraction<T>;
}) {
  const { mouseModeInteraction } = props;
  const { mouseMode, setMouseMode } = mouseModeInteraction;

  return (
    <ButtonGroup>
      <Button
        variant={mouseMode === 'zoom' ? 'primary' : 'secondary'}
        title="Zoom on the scene and edit the ROIs"
        onClick={() => {
          setMouseMode('zoom');
        }}
      >
        <i className="fa fa-search fa-fw fa-lg" />
      </Button>
      <Button
        variant={mouseMode === 'pan' ? 'primary' : 'secondary'}
        title="Pan on the scene and edit the ROIs"
        onClick={() => {
          setMouseMode('pan');
        }}
      >
        <i className="fa fa-arrows fa-fw fa-lg" />
      </Button>
    </ButtonGroup>
  );
}
