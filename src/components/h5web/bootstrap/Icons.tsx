import type { ColorMap } from '@h5web/lib';
import { ScaleType } from '@h5web/lib';
import { Image } from 'react-bootstrap';
import { AutoscaleMode } from 'components/h5web/colormap';

const scaleIcons: Record<ScaleType, string> = {
  [ScaleType.Linear]: 'fam-norm-linear',
  [ScaleType.Log]: 'fam-norm-log',
  [ScaleType.Sqrt]: 'fam-norm-sqrt',
  [ScaleType.Gamma]: 'fam-norm-gamma',
  [ScaleType.SymLog]: 'fam-norm-arcsinh',
};

const autoscaleIcons: Record<AutoscaleMode, [string, string]> = {
  [AutoscaleMode.Minmax]: ['fam-scale-auto-minmax', 'Min-max'],
  [AutoscaleMode.StdDev3]: ['fam-scale-auto-3std', '3 × stddev'],
  [AutoscaleMode.None]: ['', 'Manual'],
};

const lutIconNames: Record<string, string> = {
  Greys: 'gray',
  Viridis: 'viridis',
  Cividis: 'cividis',
  Magma: 'magma',
  Inferno: 'inferno',
  Plasma: 'plasma',
};

export function LutIcon(props: { name: ColorMap }) {
  const filename = lutIconNames[props.name] ?? 'unknown';
  const imageSrc = `resources/icons/lut/${filename}.png`;
  return (
    <Image
      className="border"
      src={imageSrc}
      rounded
      width={16}
      height={16}
      title={props.name}
    />
  );
}

export function ScaleIcon(props: { scale: ScaleType }) {
  const label = props.scale.replace(/^\w/, (c) => c.toUpperCase());
  const icon = scaleIcons[props.scale];
  return <i className={`fad ${icon} fa-lg fa-fw`} title={label} />;
}

export function AutoscaleIcon(props: { autoscale: AutoscaleMode }) {
  const [icon, label] = autoscaleIcons[props.autoscale];
  if (icon === '') {
    return <></>;
  }
  return <i className={`fad ${icon} fa-lg fa-fw`} title={label} />;
}
