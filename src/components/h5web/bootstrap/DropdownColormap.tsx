import type { ColorMap } from '@h5web/lib';
import { ScaleType } from '@h5web/lib';
import { Dropdown, ButtonGroup, Button, Container, Row } from 'react-bootstrap';
import { AutoscaleMode } from '../colormap';
import { AutoscaleIcon, LutIcon, ScaleIcon } from './Icons';

interface Props {
  as?: any;
  variant?: string;
  dropDirection?: 'up' | 'down';
  lut: ColorMap;
  norm: ScaleType;
  isInverted: boolean;
  autoscale: boolean;
  autoscaleMode: AutoscaleMode;
  onSelectLut: (value: ColorMap) => void;
  onSelectNorm: (value: ScaleType) => void;
  onSelectInvertion: (value: boolean) => void;
  onSelectAutoscale?: (value: boolean) => void;
  onSelectAutoscaleMode?: (value: AutoscaleMode) => void;
}

export default function DropdownColormap(props: Props) {
  const { as = undefined, variant = 'secondary' } = props;

  function createLutItem(name: ColorMap, title: string) {
    const active = props.lut === name;
    const variant = active ? 'primary' : 'secondary';
    return (
      <Button
        variant={variant}
        onClick={() => {
          props.onSelectLut(name);
        }}
        className="text-nowrap"
        title={title}
        size="sm"
      >
        <LutIcon name={name} />
      </Button>
    );
  }

  function createInvertedItem() {
    const variant = props.isInverted ? 'primary' : 'secondary';
    return (
      <Button
        variant={variant}
        onClick={() => {
          props.onSelectInvertion(!props.isInverted);
        }}
        size="sm"
      >
        <i className="fa fa-fw fa-arrows-alt-v" /> Inverted LUT
      </Button>
    );
  }

  function createNormalizationItem(name: ScaleType, title: string) {
    const active = props.norm === name;
    const variant = active ? 'primary' : 'secondary';
    return (
      <Button
        variant={variant}
        onClick={() => {
          props.onSelectNorm(name);
        }}
        size="sm"
      >
        {title}
      </Button>
    );
  }

  function createAutoscaleModeItem(autoscaleMode: AutoscaleMode) {
    const active = props.autoscaleMode === autoscaleMode;
    const variant =
      active && props.autoscale ? 'primary' : active ? 'warning' : 'secondary';
    const titles: Record<AutoscaleMode, string> = {
      [AutoscaleMode.Minmax]: 'Minmax',
      [AutoscaleMode.StdDev3]: '3×std',
      [AutoscaleMode.None]: 'Manual',
    };
    const title = titles[autoscaleMode];
    return (
      <Button
        variant={variant}
        onClick={() => {
          props.onSelectAutoscaleMode?.(autoscaleMode);
          if (!props.autoscale) {
            props.onSelectAutoscale?.(true);
          }
        }}
        size="sm"
      >
        {title}
      </Button>
    );
  }

  function createEnabledAutoscaleItem() {
    const variant = props.autoscale ? 'primary' : 'secondary';
    return (
      <Button
        variant={variant}
        onClick={() => {
          props.onSelectAutoscale?.(!props.autoscale);
        }}
        size="sm"
      >
        <i className="fa-solid fa-circle-half-stroke fa-fw fa-lg" /> Auto scale
        enabled
      </Button>
    );
  }

  return (
    <Dropdown
      as={as}
      className={`${props.dropDirection === 'up' ? 'dropup' : ''}`}
    >
      <Dropdown.Toggle
        id="dropdown-basic"
        variant={variant}
        className="d-flex align-items-center"
      >
        <LutIcon name={props.lut} />
        <ScaleIcon scale={props.norm} />
        {props.autoscale && <AutoscaleIcon autoscale={props.autoscaleMode} />}
      </Dropdown.Toggle>
      <Dropdown.Menu className="dropdown-menu-center">
        <div className="ms-1 me-1">
          <Container>
            <Row>
              <h6 className="flex-grow-1 text-center">Lookup table</h6>
            </Row>
            <Row>
              <ButtonGroup className="flex-grow-1">
                {createLutItem('Greys', 'Gray')}
                {createLutItem('Viridis', 'Viridis')}
                {createLutItem('Cividis', 'Cividis')}
                {createLutItem('Magma', 'Magma')}
                {createLutItem('Inferno', 'Inferno')}
                {createLutItem('Plasma', 'Plasma')}
              </ButtonGroup>
            </Row>
            <Row>
              <ButtonGroup className="flex-grow-1 mt-1">
                {createInvertedItem()}
              </ButtonGroup>
            </Row>
            {props.onSelectAutoscale && (
              <>
                <Row className="mt-2">
                  <h6 className="flex-grow-1 text-center">Autoscale</h6>
                </Row>
                <Row>
                  <ButtonGroup className="flex-grow-1">
                    {createAutoscaleModeItem(AutoscaleMode.Minmax)}
                    {createAutoscaleModeItem(AutoscaleMode.StdDev3)}
                  </ButtonGroup>
                </Row>
                <Row>
                  <ButtonGroup className="flex-grow-1 mt-1 mb-2">
                    {createEnabledAutoscaleItem()}
                  </ButtonGroup>
                </Row>
              </>
            )}
            <Row className="mt-2">
              <h6 className="flex-grow-1 text-center">Normalization</h6>
            </Row>
            <Row>
              <ButtonGroup className="flex-grow-1">
                {createNormalizationItem(ScaleType.Linear, 'Linear')}
                {createNormalizationItem(ScaleType.Log, 'Log')}
                {createNormalizationItem(ScaleType.SymLog, 'SymLog')}
                {createNormalizationItem(ScaleType.Sqrt, 'Sqrt')}
              </ButtonGroup>
            </Row>
          </Container>
        </div>
      </Dropdown.Menu>
    </Dropdown>
  );
}
