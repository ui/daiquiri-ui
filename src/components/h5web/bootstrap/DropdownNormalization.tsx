import { DropdownButton, Dropdown } from 'react-bootstrap';

interface Props {
  as?: any;
  value: string;
  onSelect: (value: string | null) => void;
}

export default function DropdownNormalization(props: Props) {
  const { as, onSelect, value } = props;
  const label = value.replace(/^\w/, (c) => c.toUpperCase());

  function createItem(key: string, title: string) {
    const icon =
      value === key ? 'fa fa-fw fa-dot-circle-o' : 'fa fa-fw fa-circle-o';
    return (
      <Dropdown.Item eventKey={key}>
        <i className={icon} />
        &nbsp; {title}
      </Dropdown.Item>
    );
  }

  return (
    <DropdownButton
      as={as}
      title={label}
      id="bg-vertical-dropdown-4"
      onSelect={onSelect}
    >
      {createItem('linear', 'Linear')}
      {createItem('log', 'Log')}
      {createItem('arcsinh', 'ArcsinH')}
      {createItem('sqrt', 'Sqrt')}
    </DropdownButton>
  );
}
