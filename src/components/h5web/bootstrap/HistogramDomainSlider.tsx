import type { ArrayHistogram, ArrayStatistics } from '../../h5web/colormap';
import type { Dispatch, SetStateAction } from 'react';
import { useMemo } from 'react';
import {
  DomainSlider,
  getSafeDomain,
  getVisDomain,
  ScaleType,
  useDomain,
} from '@h5web/lib';
import type {
  CustomDomain,
  Domain,
  HistogramParams as Histogram,
  VisScaleType,
  ColorMap,
} from '@h5web/lib';
import { useEffect, useState } from 'react';

import { useStrippedHistogram } from '../hooks';
import { toScaleType } from '../utils';

const DEFAULT_DOMAIN: Domain = [0.1, 1];

interface Props {
  histogram: Histogram;
  scaleType: VisScaleType;
  onDomainChange: (domain: Domain) => void;
  domain?: Domain;
  dataMin?: number;
  dataMax?: number;
}

function HistogramDomainSlider2(props: Props) {
  const { histogram, scaleType, onDomainChange, domain = [0, 1] } = props;
  const strippedHistogram = useStrippedHistogram(histogram);
  const { bins } = strippedHistogram;

  const scale = toScaleType(scaleType);

  const [customDomain, setCustomDomain] = useState<CustomDomain>([null, null]);
  const histoDomain = useDomain(bins, scale);
  const dataDomain = useMemo<Domain>(() => {
    if (histoDomain) {
      return histoDomain;
    }
    if (props.dataMin !== undefined && props.dataMax !== undefined) {
      if (scale === ScaleType.Log) {
        if (props.dataMin <= 0 || props.dataMax <= 0) {
          return DEFAULT_DOMAIN;
        }
      }
      return [props.dataMin, props.dataMax];
    }
    return DEFAULT_DOMAIN;
  }, [histoDomain, props.dataMin, props.dataMax, scale]);

  useEffect(() => {
    setCustomDomain(domain);
  }, [domain[0], domain[1]]);

  // TODO add input for gamma factor
  return (
    <DomainSlider
      dataDomain={dataDomain}
      customDomain={customDomain}
      scaleType={scale}
      onCustomDomainChange={(customDomain) => {
        const visDomain = getVisDomain(customDomain, dataDomain);
        const [safeDomain] = getSafeDomain(visDomain, dataDomain, scale);
        onDomainChange(safeDomain);
      }}
      histogram={{
        ...strippedHistogram,
      }}
    />
  );
}

export default function HistogramDomainSlider(props: {
  histogram?: ArrayHistogram;
  statistics?: ArrayStatistics;
  disabled?: boolean;
  setAutoscale: Dispatch<SetStateAction<boolean>>;
  autoscale: boolean;
  scaleType: VisScaleType;
  setScaleDomain: Dispatch<SetStateAction<[number, number]>>;
  scaleDomain: [number, number];
  colorMap?: ColorMap;
  invertColorMap?: boolean;
}) {
  // FIXME: disabled state not yet supported by the sub component
  return (
    <HistogramDomainSlider2
      histogram={{
        values: props.histogram?.values.data ?? [],
        bins: props.histogram?.bins.data ?? [],
        colorMap: props.colorMap,
        invertColorMap: props.invertColorMap,
      }}
      dataMin={props.statistics?.min ?? undefined}
      dataMax={props.statistics?.max ?? undefined}
      scaleType={props.scaleType}
      domain={props.scaleDomain}
      onDomainChange={(domain) => {
        if (props.autoscale) {
          props.setAutoscale(false);
        }
        props.setScaleDomain(domain);
      }}
    />
  );
}
