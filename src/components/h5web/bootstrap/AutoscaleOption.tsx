import { Button } from 'react-bootstrap';
import type { AutoscaleMode } from 'components/h5web/colormap';
import type { Dispatch, SetStateAction } from 'react';

/**
 * Option widget to switch from autoscale to none scale
 */
export default function AutoScaleOption(props: {
  disabled?: boolean;
  setAutoscale: Dispatch<SetStateAction<boolean>>;
  autoscale: boolean;
  autoscaleMode: AutoscaleMode;
}) {
  const { disabled, autoscale, autoscaleMode, setAutoscale } = props;
  return (
    <Button
      title="Enable/disable auto scale on colormap"
      variant={autoscale ? 'primary' : 'secondary'}
      disabled={disabled}
      onClick={() => {
        setAutoscale((state) => {
          return !state;
        });
      }}
    >
      <i className="fa-solid fa-circle-half-stroke fa-fw fa-lg" />
    </Button>
  );
}
