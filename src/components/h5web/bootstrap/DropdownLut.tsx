import { DropdownButton, Dropdown, Image } from 'react-bootstrap';

interface LutImageProps {
  name: string;
  active: boolean;
}

function LutImage(props: LutImageProps) {
  const imageSrc = `resources/icons/lut/${props.name}.png`;

  const style = props.active
    ? { border: '2px solid black' }
    : { border: '2px solid transparent' };

  return <Image style={style} src={imageSrc} roundedCircle />;
}

interface Props {
  as?: any;
  value: string;
  onSelect: (value: string | null) => void;
}

export default function DropdownLut(props: Props) {
  const { as, onSelect, value } = props;
  const label = value
    .replace(/^\w/, (c) => c.toUpperCase())
    .replace('_r', ' (reversed)');

  function createItem(key: string, title: string) {
    const active = value === key;
    return (
      <Dropdown.Item eventKey={key}>
        <LutImage name={key} active={active} />
        &nbsp; {title}
      </Dropdown.Item>
    );
  }

  return (
    <DropdownButton
      as={as}
      title={label}
      id="bg-vertical-dropdown-7"
      onSelect={onSelect}
    >
      {createItem('gray', 'Gray')}
      {createItem('gray_r', 'Gray (reversed)')}
      {createItem('viridis', 'Viridis')}
      {createItem('cividis', 'Cividis')}
      {createItem('magma', 'Magma')}
      {createItem('inferno', 'Inferno')}
      {createItem('plasma', 'Plasma')}
    </DropdownButton>
  );
}
