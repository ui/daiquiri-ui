import { DropdownButton, Dropdown } from 'react-bootstrap';

interface StringDictionary {
  [index: string]: string;
}

interface Props {
  as?: any;
  value: string;
  onSelect: (value: string | null) => void;
}

export default function DropdownAutoscale(props: Props) {
  const { as, onSelect, value } = props;
  const labels: StringDictionary = {
    stddev3: '3×std',
  };
  const label = labels[value] || value.replace(/^\w/, (c) => c.toUpperCase());

  function createItem(key: string, title: string) {
    const icon =
      value === key ? 'fa fa-fw fa-dot-circle-o' : 'fa fa-fw fa-circle-o';
    return (
      <Dropdown.Item eventKey={key}>
        <i className={icon} />
        &nbsp; {title}
      </Dropdown.Item>
    );
  }

  return (
    <DropdownButton
      as={as}
      title={label}
      id="bg-vertical-dropdown-3"
      onSelect={onSelect}
    >
      {createItem('none', 'None')}
      {createItem('minmax', 'Minmax')}
      {createItem('stddev3', '3×std')}
    </DropdownButton>
  );
}
