import { VisCanvas } from '@h5web/lib';
import type { AxisConfig } from '@h5web/lib';
import type { PropsWithChildren } from 'react';
import { LinearVisGroup } from './LinearVisGroup';

interface Props {
  abscissaConfig: AxisConfig;
  ordinateConfig: AxisConfig;
  title?: string;
  showAxes?: boolean;
}

/** VisCanvas with same coordinate system as its axes
 *
 * The axes scales must be linear.
 */
function LinearVisCanvas(props: PropsWithChildren<Props>) {
  const { abscissaConfig, ordinateConfig, title, showAxes, children } = props;

  return (
    <VisCanvas
      abscissaConfig={abscissaConfig}
      ordinateConfig={ordinateConfig}
      aspect="equal"
      title={title}
      showAxes={showAxes}
    >
      <LinearVisGroup>{children}</LinearVisGroup>
    </VisCanvas>
  );
}

export type { Props as LinearVisCanvasProps };
export default LinearVisCanvas;
