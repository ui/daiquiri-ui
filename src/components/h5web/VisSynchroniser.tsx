import { useCounter } from '@react-hookz/web';
import { useFrame, useThree } from '@react-three/fiber';
import {
  forwardRef,
  useImperativeHandle,
  useLayoutEffect,
  useRef,
} from 'react';

import { Vector3 } from 'three';
import { useGetVisViewpoint, useVisViewpointToCamera } from './hooks';
import type { VisViewpoint } from './models';
import { isVecClose } from './utils';

interface Props {
  onVisViewpointChange?: (center: Vector3, scale: Vector3) => void;
}

interface VisSynchroniserRef {
  getCameraViewpoint: () => { center: Vector3; scale: Vector3 };
  updateCameraViewpoint: (viewpoint: VisViewpoint) => void;
  resetZoom: () => void;
  isZoomed: () => boolean;
  setScale: (scale: number) => void;
}

const VisSynchroniser = forwardRef<VisSynchroniserRef, Props>((props, ref) => {
  const { onVisViewpointChange } = props;

  const camera = useThree((state) => state.camera);
  const invalidate = useThree((state) => state.invalidate);
  const visViewpointToCamera = useVisViewpointToCamera();
  const getVisViewpoint = useGetVisViewpoint();

  const [updateCount, { inc: incUpdateCount }] = useCounter();
  const updateCountRef = useRef<number>(0);
  const lastVisViewpointRef = useRef<VisViewpoint>({
    center: new Vector3(),
    scale: new Vector3(),
  });
  useLayoutEffect(invalidate, [updateCount, invalidate]);

  useImperativeHandle(ref, () => ({
    getCameraViewpoint: () => {
      return getVisViewpoint();
    },
    updateCameraViewpoint: (viewpoint: VisViewpoint) => {
      const { position, scale } = visViewpointToCamera(viewpoint);

      if (
        !isVecClose(camera.position, position, 1e-10, 1e-10) ||
        !isVecClose(camera.scale, scale, 1e-10, 1e-10)
      ) {
        camera.position.copy(position);
        camera.scale.copy(scale);
        camera.updateMatrixWorld();
        incUpdateCount();
      }
    },
    setScale: (scale: number) => {
      camera.scale.x = scale;
      camera.scale.y = scale;
      camera.updateMatrixWorld();
      incUpdateCount();
    },
    isZoomed: () => camera.scale.x < 1 || camera.scale.y < 1,
    resetZoom: () => {
      camera.scale.x = 1;
      camera.scale.y = 1;
      camera.position.x = 0;
      camera.position.y = 0;
      camera.updateMatrixWorld();
      incUpdateCount();
    },
  }));

  useFrame(() => {
    if (updateCountRef.current !== updateCount) {
      updateCountRef.current = updateCount;
      return;
    }
    if (onVisViewpointChange !== undefined) {
      const { center, scale } = getVisViewpoint();
      const lastViewpoint = lastVisViewpointRef.current;
      lastVisViewpointRef.current = { center, scale };
      if (
        !isVecClose(lastViewpoint.center, center, 1e-10, 1e-10) ||
        !isVecClose(lastViewpoint.scale, scale, 1e-10, 1e-10)
      ) {
        onVisViewpointChange(center, scale);
      }
    }
  });

  return null;
});

export default VisSynchroniser;
export type { Props as VisSynchroniserProps, VisSynchroniserRef };
