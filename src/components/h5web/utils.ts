import type { VisScaleType, HistogramParams as Histogram } from '@h5web/lib';
import { ScaleType } from '@h5web/lib';
import { findLastIndex } from 'lodash';
import type { NdArray } from 'ndarray';
import type { Vector3 } from 'three';
import { Vector2 } from 'three';
import { NdFloat16Array } from './NdFloat16Array';
import { NdNormalizedUint8Array } from './NdNormalizedUint8Array';

function isScaleType(val: unknown): val is ScaleType {
  return (
    typeof val === 'string' && Object.values<string>(ScaleType).includes(val)
  );
}

export function toScaleType(scaleType: VisScaleType): ScaleType {
  return isScaleType(scaleType) ? scaleType : (scaleType[0] as ScaleType);
}

export function stripHistogram(histogram: Histogram): Histogram {
  const { values, bins } = histogram;

  const startIndex = values.findIndex((value) => value !== 0);
  if (startIndex === -1) {
    return {
      ...histogram,
      values: new Float64Array(),
      bins: new Float64Array(),
    };
  }
  const stopIndex = findLastIndex(values, (value) => value !== 0) + 1;
  if (startIndex === 0 && stopIndex === values.length) {
    return histogram; // copy-less fast-path
  }
  return {
    ...histogram,
    values: values.slice(startIndex, stopIndex),
    bins: bins.slice(startIndex, stopIndex + 1),
  };
}

export function isVector2(entity: unknown): entity is Vector2 {
  return entity instanceof Object && 'y' in entity;
}

export function asVector2(position: [number, number] | Vector2): Vector2 {
  if (isVector2(position)) {
    return position;
  }
  return new Vector2(position[0], position[1]);
}

function isClose(a: number, b: number, rtol = 1e-5, atol = 1e-8): boolean {
  // Same behavior as numpy.isclose
  return Math.abs(a - b) <= atol + rtol * Math.abs(b);
}

export function isVecClose(
  a: Vector3,
  b: Vector3,
  rtol = 1e-5,
  atol = 1e-8
): boolean {
  return (
    isClose(a.x, b.x, rtol, atol) &&
    isClose(a.y, b.y, rtol, atol) &&
    isClose(a.z, b.z, rtol, atol)
  );
}

export function assertFloat32Array(array: any): asserts array is Float32Array {
  if (array.constructore === Float32Array) {
    throw new Error('A Float32Array is expected');
  }
}

export function assertDisplayableArray2dOrUndefined(
  array: NdArray<any> | NdFloat16Array | NdNormalizedUint8Array | undefined
): asserts array is
  | NdArray<Float32Array>
  | NdFloat16Array
  | NdNormalizedUint8Array
  | undefined {
  if (array === undefined) {
    return;
  }
  if (array.dimension !== 2) {
    throw new Error('A 2d array is expected');
  }
  if (array instanceof NdNormalizedUint8Array) {
    return;
  }
  if (array instanceof NdFloat16Array) {
    return;
  }
  assertFloat32Array(array.data);
}
