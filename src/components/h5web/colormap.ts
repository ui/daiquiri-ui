import { sum } from 'lodash';
import { useEffect, useMemo } from 'react';
import type { Dispatch, SetStateAction } from 'react';
import type { NdArray, TypedArray } from 'ndarray';
import type { Domain } from '@h5web/lib';
import debug from 'debug';

const logger = debug('daiquiri.components.h5web.colormap');

/**
 *  Available modes for autoscale
 *
 * NOTE: This strings match the server side code
 */
export enum AutoscaleMode {
  Minmax = 'minmax',
  StdDev3 = 'stddev3',
  None = 'none',
}

/**
 * Statistics related to an array
 */
export interface ArrayStatistics {
  min: number | null;
  max: number | null;
  minPositive: number | null;
  mean: number | null;
  std: number | null;
}

/**
 * Histogram related to an array
 */
export interface ArrayHistogram {
  values: NdArray<TypedArray>;
  bins: NdArray<TypedArray>;
}

export function domainFromStatistics(
  autoscale: AutoscaleMode,
  statistics: ArrayStatistics
): Domain {
  const s = statistics;
  if (autoscale === AutoscaleMode.Minmax) {
    return [s.min ?? 0, s.max ?? 1];
  }
  if (autoscale === AutoscaleMode.StdDev3) {
    if (s.mean === null || s.std === null) {
      // Fallback with safe min/max
      return [s.min ?? 0, s.max ?? 1];
    }
    const mean = s.mean ?? 0;
    const std = s.std ?? 0;
    const vmin = s.min ?? mean - std * 10;
    const vmax = s.max ?? mean + std * 10;
    return [Math.max(mean - std * 3, vmin), Math.min(mean + std * 3, vmax)];
  }
  throw new Error(`Unsupported autoscale ${autoscale}`);
}

/**
 * Synchronize the scale domain from array statistics
 */
export function useSyncScaleDomainFromStatistics(props: {
  statistics: ArrayStatistics | undefined;
  autoscaleMode: AutoscaleMode;
  autoscale: boolean;
  setScaleDomain: Dispatch<SetStateAction<[number, number]>>;
  scaleDomain: [number, number];
}) {
  useEffect(() => {
    const s = props.statistics;
    if (s !== undefined && props.autoscale) {
      try {
        const domain = domainFromStatistics(props.autoscaleMode, s);
        props.setScaleDomain(domain);
      } catch (error) {
        const message = error instanceof Error ? error.message : String(error);
        logger(`Error while computing colormap level: ${message}`);
      }
    }
  }, [
    props.autoscale,
    props.autoscaleMode,
    props.setScaleDomain,
    props.statistics,
  ]);
}

/**
 * Using weighted mean and variance of the histogram bins centers to estimate
 * the mean and std of the inital data
 */
function getMeanStdFromHistogram(histogram: ArrayHistogram): [number, number] {
  const { values, bins } = histogram;

  const centers = bins.data
    .map((element, index, array) =>
      index === 0 ? 0 : 0.5 * (element + array[index - 1])
    )
    .subarray(1);

  const weights = values.data;
  const sumOfWeights = sum(weights);
  let mean = 0;
  for (let index = 0; index < centers.length; index++) {
    mean += weights[index] * centers[index];
  }
  mean /= sumOfWeights;
  let variance = 0;
  for (let index = 0; index < centers.length; index++) {
    variance += weights[index] * (centers[index] - mean) ** 2;
  }
  variance /= sumOfWeights;

  return [mean, Math.sqrt(variance)];
}

/**
 * Generate statistics from an histogram
 */
export function useStatisticsFromHistogram(
  histogram: ArrayHistogram | undefined
): ArrayStatistics | undefined {
  return useMemo(() => {
    if (!histogram) {
      return undefined;
    }

    const [mean, std] = getMeanStdFromHistogram(histogram);

    return {
      min: histogram.bins.get(0),
      max: histogram.bins.get(histogram.bins.size - 1),
      std,
      mean,
      minPositive: null,
    };
  }, [histogram]);
}
