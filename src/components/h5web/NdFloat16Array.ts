/**
 * Decode a binary uint16 as is it is a float16.
 *
 * https://stackoverflow.com/a/8796597
 */
export function decodeFloat16(value: number): number {
  const exponent = (value & 0x7c_00) >> 10; // eslint-disable-line no-bitwise
  const fraction = value & 0x03_ff; // eslint-disable-line no-bitwise
  return (
    (value >> 15 ? -1 : 1) * // eslint-disable-line no-bitwise
    (exponent
      ? exponent === 0x1f
        ? fraction
          ? Number.NaN
          : Infinity
        : 2 ** (exponent - 15) * (1 + fraction / 0x4_00)
      : 6.103_515_625e-5 * (fraction / 0x4_00))
  );
}

/**
 * NdArray stored as a Float16Array.
 *
 * `Float16Array` is not supported in Javascript.
 * That's why the data is stored as an opaque `Uint16Array`.
 * This kind of data still can be displayed in WebGL, that's why it is stayed as it is.
 *
 * The method `get` is anyway supported, with a custom binary transformation.
 */
export class NdFloat16Array {
  public readonly shape: number[];

  public readonly dimension: number;

  public readonly data: Uint16Array;

  public readonly dtype: string;

  public constructor(data: Uint16Array, shape: number[]) {
    if (shape.length !== 2) {
      throw new Error(
        `Unsupported dimension ${shape.length} with NdFloat16Array`
      );
    }
    this.data = data;
    this.shape = shape;
    this.dimension = shape.length;
    this.dtype = 'float16';
  }

  public index(...args: number[]): number {
    if (args.length !== this.dimension) {
      throw new Error(`Expected ${this.dimension} for indexing`);
    }
    return args[1] + args[0] * this.shape[1];
  }

  public getRaw(...args: number[]): number {
    const index = this.index(...args);
    return this.data[index];
  }

  public get(...args: number[]): number {
    const v = this.getRaw(...args);
    return decodeFloat16(v);
  }
}
