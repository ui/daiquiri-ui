import { Children, cloneElement, isValidElement, useState } from 'react';
import type { ReactNode } from 'react';
import { Col, Row, Button, ButtonGroup } from 'react-bootstrap';
import { map } from 'lodash';

interface StateList {
  /** Description for this set of props */
  description: string;
  /** Array of properties which will be passed to the sub components */
  props: any;
}

/**
 * Selector to custom the state of the hardware components on the documentation.
 */
export default function StateSelector(props: {
  children?: ReactNode;
  stateList: StateList[];
}) {
  const [selectedId, setSelectedId] = useState(0);
  const selected = props.stateList[selectedId];
  return (
    <Row>
      <Col style={{ maxWidth: '100%', flex: '1 0 50%' }}>
        {Children.map(props.children, (child) => {
          return isValidElement(child)
            ? cloneElement(child, { ...selected.props })
            : undefined;
        })}
      </Col>
      <Col md="auto">
        <h5>Device state</h5>
        <ButtonGroup vertical>
          {map(props.stateList, (element, id) => (
            <Button
              variant={id === selectedId ? 'primary' : 'secondary'}
              onClick={() => {
                setSelectedId(id);
              }}
            >
              {element.description}
            </Button>
          ))}
        </ButtonGroup>
      </Col>
    </Row>
  );
}

interface Props {
  stateList: StateList[];
  children: (props: any) => ReactNode;
}

export function StateSelectorRP(props: Props) {
  const [selectedId, setSelectedId] = useState(0);
  const selected = props.stateList[selectedId];
  return (
    <Row>
      <Col style={{ maxWidth: '100%', flex: '1 0 50%' }}>
        {props.children(selected.props)}
      </Col>
      <Col md="auto">
        <h5>Device state</h5>
        <ButtonGroup vertical>
          {map(props.stateList, (element, id) => (
            <Button
              variant={id === selectedId ? 'primary' : 'secondary'}
              onClick={() => {
                setSelectedId(id);
              }}
            >
              {element.description}
            </Button>
          ))}
        </ButtonGroup>
      </Col>
    </Row>
  );
}
