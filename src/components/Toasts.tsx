import { ToastContainer, Toast } from 'react-bootstrap';
import Holder from 'helpers/Holder';
import sassVariables from 'scss/variables.module.scss';
import { useEffect, useState } from 'react';

interface ToastData {
  id: number;
  time: string;
  title: string;
  type: string;
  text?: string;
}

interface ToastLocal {
  id: number;
  time: string;
  title: string;
  type: string;
  text?: string;
  addedTime: number;
  show: boolean;
}

interface Props {
  actions: {
    removeToast: (id: number) => void;
  };
  toasts: ToastData[];
}

function updateToastList(
  prevState: ToastLocal[],
  reduxData: ToastData[]
): ToastLocal[] {
  const prevIds = new Set(prevState.map((v) => v.id));
  const newToast = reduxData.filter((v) => !prevIds.has(v.id));
  const now = Date.now();
  const newToastLocal: ToastLocal[] = newToast.map((v) => {
    return { ...v, addedTime: now, show: true };
  });
  const remainingPrevState = prevState.filter((v) => {
    return now - v.addedTime < 6000;
  });
  return [...remainingPrevState, ...newToastLocal];
}

function hideToastItem(prevState: ToastLocal[], id: number) {
  return prevState.map((v) => {
    if (id === v.id) return { ...v, show: false };
    return v;
  });
}

export default function Toasts(props: Props) {
  const types: Record<string, string> = {
    error: sassVariables.danger,
    warning: sassVariables.warning,
    success: sassVariables.success,
    info: sassVariables.cyan,
  };

  const [toasts, setToasts] = useState<ToastLocal[]>([]);

  useEffect(() => {
    setToasts((prevState) => updateToastList(prevState, props.toasts));
  }, [props.toasts]);

  return (
    <ToastContainer>
      {toasts.map((toast) => {
        const bg = types[toast.type];
        return (
          <Toast
            key={toast.id}
            onClose={() => {
              props.actions.removeToast(toast.id);
              setToasts((prevState) => hideToastItem(prevState, toast.id));
            }}
            show={toast.show}
            autohide
            delay={3000}
          >
            <Toast.Header>
              <Holder
                width="20"
                height="20"
                text=" "
                className="rounded me-2"
                bg={bg}
              />
              <strong className="me-auto">{toast.title}</strong>
              <small>{toast.time}</small>
            </Toast.Header>
            <Toast.Body>{toast.text}</Toast.Body>
          </Toast>
        );
      })}
    </ToastContainer>
  );
}
