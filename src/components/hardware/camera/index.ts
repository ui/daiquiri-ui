import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { HardwareVariant } from '@esrf/daiquiri-lib';
import Camera from './Camera';

/**
 * Hardware as exposed by Redux
 */
export interface CameraSchema extends HardwareTypes.Hardware {
  properties: {
    state: string;
    // exposure time in second
    exposure: number;
    gain: number;
    mode: string;
    live: boolean;
    width: number;
    height: number;
  };
}

export default class Default extends HardwareVariant {
  public variants = {
    default: Camera,
  };
}
