import type { ChangeEvent } from 'react';
import {
  Badge,
  ToggleButton,
  InputGroup,
  Popover,
  OverlayTrigger,
  Form,
  Button,
  Row,
  Col,
} from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import {
  TypeIcon,
  HardwareInputNumber,
  HardwareNumericStep,
} from '@esrf/daiquiri-lib';
import type { CameraSchema } from '.';

export interface CameraOptions extends HardwareTypes.HardwareWidgetOptions {
  /** Default step size to use for up / down arrows */
  step?: number;
  /** Array of selectable step sizes */
  steps?: number[];
}

function CameraPropertiesDropdown(
  props: HardwareTypes.HardwareWidgetProps<CameraSchema, CameraOptions>
) {
  const { hardware, options = {} } = props;

  function onChangeGainRequested(value: number) {
    return hardware.actions.setProperty('gain', value);
  }

  return (
    <OverlayTrigger
      trigger="click"
      rootClose
      overlay={
        <Popover id="popid">
          <Popover.Header>{props.hardware.name} Properties</Popover.Header>
          <Popover.Body>
            <Form.Group as={Row}>
              <Form.Label column>Gain</Form.Label>
              <Col>
                <HardwareInputNumber
                  hardwareValue={hardware.properties.gain}
                  hardwareIsDisabled={props.disabled}
                  onMoveRequested={onChangeGainRequested}
                />
              </Col>
            </Form.Group>
          </Popover.Body>
        </Popover>
      }
    >
      <Button>
        <i className="fa fa-ellipsis-h" />
      </Button>
    </OverlayTrigger>
  );
}

export default function Camera(
  props: HardwareTypes.HardwareWidgetProps<CameraSchema, CameraOptions>
) {
  const { hardware, options = {} } = props;

  function onChangeExposureRequested(value: number) {
    return hardware.actions.setProperty('exposure', value);
  }

  function onChange(e: ChangeEvent<HTMLInputElement>) {
    hardware.actions.setProperty(
      e.target.name,
      e.target.checked !== undefined ? e.target.checked : e.target.value
    );
  }

  return (
    <div className="hw-component">
      <div className="hw-head">
        <TypeIcon
          name="Camera"
          icon="fam-hardware-camera"
          online={hardware.online}
        />
        <div className="name">{hardware.name}</div>
        <Badge
          bg={hardware.properties.state === 'ERROR' ? 'danger' : 'success'}
        >
          {hardware.properties.state}
        </Badge>
      </div>
      <div className="hw-content">
        <InputGroup>
          <HardwareNumericStep
            hardwareValue={hardware.properties.exposure}
            hardwareIsDisabled={props.disabled}
            hardwareIsReady
            hardwareIsMoving={false}
            onMoveRequested={onChangeExposureRequested}
            onAbortRequested={() => null}
            steps={options.steps}
            step={options.step}
          />
          <InputGroup.Text>s</InputGroup.Text>
          <ToggleButton
            id="live"
            name="live"
            type="checkbox"
            disabled={props.disabled}
            value="live"
            checked={hardware.properties.live}
            onChange={onChange}
          >
            Live
          </ToggleButton>
          <CameraPropertiesDropdown {...props} />
        </InputGroup>
      </div>
    </div>
  );
}
