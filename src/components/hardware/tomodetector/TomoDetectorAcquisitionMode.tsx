import {
  InputGroup,
  Dropdown,
  Container,
  Row,
  Col,
  ButtonGroup,
  Button,
} from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import {
  TypeIcon,
  HardwareTemplate,
  HardwareInputNumber,
} from '@esrf/daiquiri-lib';
import type { TomoDetectorSchema } from '.';
import { TomoDetectorState } from './State';
import type { LimaSchema } from '../lima';
import { isLimaHardware } from '../lima';
import { useHardware } from '../../tomo/utils/hooks';
import { useHardwareChangeActions } from '../../utils/hooks';
import ReadOnlyTextInput from '../../common/ReadOnlyTextInput';

/**
 * Widget to select the acquisition mode
 */
function AcquisitionMode(
  props: HardwareTypes.HardwareWidgetProps<
    TomoDetectorSchema,
    HardwareTypes.HardwareWidgetOptions
  > &
    HardwareTypes.HardwareWidgetOptions & { style: { [key: string]: any } }
) {
  const { hardware, options = {} } = props;
  function requestMode(value: string) {
    if (hardware.actions) {
      hardware.actions.setProperty('acq_mode', value);
    } else {
      console.error('Change request was triggered with:', value);
    }
    return null;
  }
  const mode = hardware.properties.acq_mode;

  return (
    <Dropdown style={props.style}>
      <ButtonGroup>
        <Button
          variant={mode === 'SINGLE' ? 'primary' : 'secondary'}
          onClick={() => {
            requestMode('SINGLE');
          }}
          disabled={props.disabled}
          title="Do acquisition in a single shot"
        >
          SINGLE
        </Button>
        <Button
          variant={mode === 'ACCUMULATION' ? 'primary' : 'secondary'}
          onClick={() => {
            requestMode('ACCUMULATION');
          }}
          disabled={props.disabled}
          title="Do acqisition in multiple sub frame with accumulation"
        >
          ACCU
        </Button>
      </ButtonGroup>
    </Dropdown>
  );
}

function InputAccMaxExpoTime(props: {
  hardware: LimaSchema;
  actions: HardwareTypes.HardwareChangeActions;
}) {
  const { hardware, actions } = props;
  const value = hardware.properties.acc_max_expo_time || 0;
  const { state } = hardware.properties;
  const hardwareReady = state === 'READY';

  function requestUpdate(value: number) {
    actions.setProperty('acc_max_expo_time', value);
    return null;
  }

  return (
    <HardwareInputNumber
      hardwareValue={value}
      onMoveRequested={requestUpdate}
      hardwareIsDisabled={false}
      hardwareIsReady={hardwareReady}
      hardwareIsMoving={false}
      readOnly={hardware.properties.state !== 'READY'}
    />
  );
}

function useLimaHardware(id: string): LimaSchema | null {
  const lima = useHardware(id);
  if (lima !== null && isLimaHardware(lima)) {
    return lima;
  }
  return null;
}

function DropdownAcquisitionMode(
  props: HardwareTypes.HardwareWidgetProps<
    TomoDetectorSchema,
    HardwareTypes.HardwareWidgetOptions
  >
) {
  const { hardware } = props;
  const { properties } = hardware;
  const limaId = properties.detector.slice(9);
  const lima = useLimaHardware(limaId);
  const limaActions = useHardwareChangeActions(limaId);

  return (
    <Dropdown>
      <Dropdown.Toggle disabled={props.disabled} variant="secondary" />
      <Dropdown.Menu>
        <Container>
          <Row>
            <Col className="text-center">Acquisition mode</Col>
          </Row>
          <Row>
            <Col>
              <AcquisitionMode {...props} style={{ width: '100%' }} />
            </Col>
          </Row>
          <Row>
            <Col className="text-center mt-1">Max sub frame exposition</Col>
          </Row>
          <Row>
            <Col>
              {lima && (
                <InputGroup>
                  <InputAccMaxExpoTime hardware={lima} actions={limaActions} />
                  <InputGroup.Text>um</InputGroup.Text>
                </InputGroup>
              )}
            </Col>
          </Row>
        </Container>
      </Dropdown.Menu>
    </Dropdown>
  );
}

export default function TomoDetectorAcquisitionMode(
  props: HardwareTypes.HardwareWidgetProps<
    TomoDetectorSchema,
    HardwareTypes.HardwareWidgetOptions
  >
) {
  const { hardware } = props;
  const widgetIcon = (
    <TypeIcon name="TomoDetector" icon="fa-cog" online={hardware.online} />
  );

  const widgetState = <TomoDetectorState hardware={hardware} />;

  const widgetContent = (
    <InputGroup className="flex-nowrap">
      <ReadOnlyTextInput value={hardware.properties.acq_mode} />
      <DropdownAcquisitionMode {...props} />
    </InputGroup>
  );

  const headerMode = props.options ? props.options.header : 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
