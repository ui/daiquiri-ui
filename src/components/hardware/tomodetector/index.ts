import type { Hardware, HardwareTypes } from '@esrf/daiquiri-lib';
import { HardwareVariant } from '@esrf/daiquiri-lib';
import TomoDetectorSamplePixelSize from './TomoDetectorSamplePixelSize';
import TomoDetectorAcquisitionMode from './TomoDetectorAcquisitionMode';

/**
 * Hardware as exposed by Redux
 */
export interface TomoDetectorSchema extends HardwareTypes.Hardware {
  properties: {
    state: string;
    user_sample_pixel_size: number | null;
    sample_pixel_size_mode: string;
    sample_pixel_size: number | null;
    field_of_view: number;
    detector: string;
    optic: string;
    source_distance: number | null;
    acq_mode: 'MANUAL' | 'SINGLE' | 'ACCUMULATION';
  };
}

export default class Default extends HardwareVariant {
  public variants = {
    samplepixelsize: TomoDetectorSamplePixelSize,
    acquisitionmode: TomoDetectorAcquisitionMode,
  };
}

export function isTomoDetectorHardware(
  entity: Hardware
): entity is TomoDetectorSchema {
  return entity.type === 'tomodetector';
}
