import { HardwareState } from '@esrf/daiquiri-lib';
import type { TomoDetectorSchema } from '.';

export function TomoDetectorState(props: { hardware: TomoDetectorSchema }) {
  const { hardware } = props;
  const { online, locked = null } = hardware;
  const state = online
    ? (locked !== null ? 'LOCKED' : hardware.properties.state) || 'UNKNOWN'
    : 'OFFLINE';

  const variants: { [state: string]: string } = {
    UNKNOWN: 'danger',
    READY: 'success',
    OFFLINE: 'secondary',
    INVALID: 'danger',
    MOUNTING: 'warning',
    UNMOUNTING: 'warning',
    LOCKED: 'warning',
  };
  const variant = variants[state] || variants.UNKNOWN;
  return (
    <HardwareState.HardwareState state={state} minWidth={6} variant={variant} />
  );
}
