import {
  InputGroup,
  Dropdown,
  Container,
  Row,
  Col,
  ButtonGroup,
  Button,
} from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import {
  TypeIcon,
  HardwareTemplate,
  HardwareInputNumber,
} from '@esrf/daiquiri-lib';
import type { TomoDetectorSchema } from '.';
import { TomoDetectorState } from './State';
import ReadOnlyTextInput from 'components/common/ReadOnlyTextInput';
import Qty from 'js-quantities';
import { useMemo } from 'react';

interface TomoDetectorSamplePixelSizeOptions
  extends HardwareTypes.HardwareWidgetOptions {
  /* The unit to be used to display the pixel size.

    FIXME: If undefined it is actually serialized as `null`.  In the future,
           this should be instead converted to `undefined`.
   */
  display_unit?: string | null;
}

/**
 * Widget to select the mouse mode for the tomo view
 */
function SelectPixelSizeMode(
  props: HardwareTypes.HardwareWidgetProps<
    TomoDetectorSchema,
    TomoDetectorSamplePixelSizeOptions
  > & { style: { [key: string]: any } }
) {
  const { hardware, options = {} } = props;
  function requestMode(value: string) {
    if (hardware.actions) {
      hardware.actions.setProperty('sample_pixel_size_mode', value);
    } else {
      console.error('Change request was triggered with:', value);
    }
    return null;
  }
  const mode = hardware.properties.sample_pixel_size_mode;

  return (
    <Dropdown style={props.style}>
      <ButtonGroup>
        <Button
          variant={mode === 'USER' ? 'primary' : 'secondary'}
          onClick={() => {
            requestMode('USER');
          }}
          disabled={props.disabled}
          title="Use a fixed pixel size defined by the user"
        >
          USER
        </Button>
        <Button
          variant={mode === 'AUTO' ? 'primary' : 'secondary'}
          onClick={() => {
            requestMode('AUTO');
          }}
          disabled={props.disabled}
          title="Compute the pixel size automatically based on the beamline geometry"
        >
          AUTO
        </Button>
      </ButtonGroup>
    </Dropdown>
  );
}

function InputUserPixelSize(
  props: HardwareTypes.HardwareWidgetProps<
    TomoDetectorSchema,
    TomoDetectorSamplePixelSizeOptions
  >
) {
  const { hardware, options = {} } = props;
  const userSamplePixelSize = hardware.properties.user_sample_pixel_size || 0;
  const { state } = hardware.properties;
  const hardwareReady = state === 'READY';

  function requestUserSamplePixelSize(value: number) {
    if (hardware.actions) {
      hardware.actions.setProperty('user_sample_pixel_size', value);
    } else {
      console.error('Change request was triggered with:', value);
    }
    return null;
  }

  return (
    <HardwareInputNumber
      hardwareValue={userSamplePixelSize}
      onMoveRequested={requestUserSamplePixelSize}
      hardwareIsDisabled={false}
      hardwareIsReady={hardwareReady}
      hardwareIsMoving={false}
      readOnly={hardware.properties.sample_pixel_size_mode !== 'USER'}
    />
  );
}

function DropdownPixelSize(
  props: HardwareTypes.HardwareWidgetProps<
    TomoDetectorSchema,
    TomoDetectorSamplePixelSizeOptions
  >
) {
  const { hardware, options = {} } = props;
  const hardwareReady = hardware.properties.state === 'READY';
  return (
    <Dropdown>
      <Dropdown.Toggle disabled={props.disabled} variant="secondary" />
      <Dropdown.Menu>
        <Container>
          <Row>
            <Col className="text-center">Mode</Col>
          </Row>
          <Row>
            <Col>
              <SelectPixelSizeMode {...props} style={{ width: '100%' }} />
            </Col>
          </Row>
          <Row>
            <Col className="text-center mt-1">User pixel size</Col>
          </Row>
          <Row>
            <Col>
              <InputGroup>
                <InputUserPixelSize {...props} />
                <InputGroup.Text>um</InputGroup.Text>
              </InputGroup>
            </Col>
          </Row>
        </Container>
      </Dropdown.Menu>
    </Dropdown>
  );
}

function useFormattedQuantity(
  value: number | null,
  unit: string,
  displayUnit: string
): [string, string] {
  return useMemo(() => {
    function normUnit(u: string): string {
      if (u === 'um') {
        return 'μm';
      }
      return u;
    }
    function normValue(v: number): string {
      return v.toFixed(2).replace(/\.?0+$/, '');
    }
    if (value === null) {
      return ['∅', ''];
    }
    if (unit === displayUnit) {
      return [normValue(value), normUnit(displayUnit)];
    }
    const q = Qty(value, unit);
    return [normValue(q.to(displayUnit).scalar), normUnit(displayUnit)];
  }, [value, unit, displayUnit]);
}

export default function TomoDetectorSamplePixelSize(
  props: HardwareTypes.HardwareWidgetProps<
    TomoDetectorSchema,
    TomoDetectorSamplePixelSizeOptions
  >
) {
  const { hardware, options = {} } = props;
  const displayUnit = options.display_unit ?? 'um';
  const widgetIcon = (
    <TypeIcon name="TomoDetector" icon="fa-cog" online={hardware.online} />
  );

  const widgetState = <TomoDetectorState hardware={hardware} />;

  const [formattedValue, unit] = useFormattedQuantity(
    hardware.properties.sample_pixel_size,
    'um',
    displayUnit
  );

  const widgetContent = (
    <InputGroup className="flex-nowrap">
      <InputGroup.Text
        className="justify-content-center"
        style={{ minWidth: '4em' }}
      >
        {hardware.properties.sample_pixel_size_mode}
      </InputGroup.Text>
      <ReadOnlyTextInput
        value={formattedValue}
        title={`${hardware.properties.sample_pixel_size} um`}
      />
      <InputGroup.Text>{unit}</InputGroup.Text>
      <DropdownPixelSize {...props} />
    </InputGroup>
  );

  const headerMode = props.options ? props.options.header : 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
