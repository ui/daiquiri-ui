import type { HardwareTypes, HWObject } from '@esrf/daiquiri-lib';
import { Info, Multiposition, NoObject, Property } from '@esrf/daiquiri-lib';

import DefaultObject from 'components/hardware/DefaultObject';

import AirBearing from 'components/hardware/airbearing';
import Motor from 'components/hardware/motor';
import Shutter from 'components/hardware/shutter';
import Camera from 'components/hardware/camera';
import Frontend from 'components/hardware/frontend';
import Wago from 'components/hardware/wago';
import Beamviewer from 'components/hardware/beamviewer';
import Gauge from 'components/hardware/gauge';
import Valve from 'components/hardware/valve';
import Pump from 'components/hardware/pump';
import Pusher from 'components/hardware/pusher';
import Laser from 'components/hardware/laser';
import LaserHeating from 'components/hardware/laserheating';
import Lima from 'components/hardware/lima';
import Light from 'components/hardware/light';
import Name from 'components/hardware/name';
import Procedure from 'components/hardware/procedure';
import State from 'components/hardware/state';
import TomoConfig from 'components/hardware/tomoconfig';
import TomoImaging from 'components/hardware/tomoimaging';
import TomoDetector from 'components/hardware/tomodetector';
import TomoDetectors from 'components/hardware/tomodetectors';
import TomoFlatMotion from 'components/hardware/tomoflatmotion';
import TomoReferencePosition from 'components/hardware/tomoreferenceposition';
import Optic from 'components/hardware/optic';
import TangoAttr from 'components/hardware/tangoattr';
import Service from 'components/hardware/service';
import { useHardwareChangeActions } from '../utils/hooks';

export const objMap: HWObject.ComponentMapping = {
  airbearing: AirBearing,
  motor: Motor,
  shutter: Shutter,
  multiposition: Multiposition,
  camera: Camera,
  wago: Wago,
  config: Wago,
  frontend: Frontend,
  beamviewer: Beamviewer,
  gauge: Gauge,
  valve: Valve,
  pump: Pump,
  pusher: Pusher,
  laser: Laser,
  laserheating: LaserHeating,
  light: Light,
  lima: Lima,
  procedure: Procedure,
  tomoconfig: TomoConfig,
  tomoimaging: TomoImaging,
  tomodetector: TomoDetector,
  tomodetectors: TomoDetectors,
  tomoflatmotion: TomoFlatMotion,
  tomoreferenceposition: TomoReferencePosition,
  optic: Optic,
  tangoattr: TangoAttr,
  service: Service,
};

export const globalVariants: HWObject.ComponentMapping = {
  property: Property,
  state: State,
  info: Info,
  name: Name,
};

export default function HardwareObject(
  props: HWObject.ResolvedHardwareObjectProps
) {
  const actions = useHardwareChangeActions(props.obj?.id ?? null);

  const { options = {} } = props;
  if (!props.obj) {
    return <NoObject id={props.id} options={options} />;
  }

  function getComp() {
    const { obj } = props;
    const { variant } = options;
    if (variant && variant in globalVariants) {
      return globalVariants[variant];
    }
    if (obj && obj.type in objMap) {
      return objMap[obj.type];
    }
    return DefaultObject;
  }

  const { obj, propertiesSchema, callablesSchema } = props;
  const schema = {
    properties: propertiesSchema,
    callables: callablesSchema,
  };

  const hardware = {
    ...obj,
    actions,
  };

  const Comp = getComp();
  return (
    // @ts-expect-error
    <Comp
      key={`hw${obj.id}`}
      hardware={hardware}
      schema={schema}
      options={options}
      operator={props.operator}
      disabled={
        !props.operator ||
        props.runningScan ||
        !props.obj.online ||
        props.obj.locked
      }
    />
  );
}
