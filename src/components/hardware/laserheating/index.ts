import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { HardwareVariant } from '@esrf/daiquiri-lib';
import LaserHeating from './LaserHeating';

/**
 * Hardware as exposed by Redux
 */
export interface LaserHeatingSchema extends HardwareTypes.Hardware {
  properties: {
    state: string;
    exposure_time: number;
    background_mode: 'ON' | 'OFF' | 'ALWAYS';
    fit_wavelength: number[];
    current_calibration: string;
  };
}

export default class Default extends HardwareVariant {
  public variants = {
    default: LaserHeating,
  };
}
