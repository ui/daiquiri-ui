import {
  Button,
  InputGroup,
  OverlayTrigger,
  Popover,
  Container,
  Row,
  Col,
} from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import {
  TypeIcon,
  HardwareTemplate,
  HardwareNumericStep,
  HardwareState,
} from '@esrf/daiquiri-lib';
import type { LaserHeatingSchema } from '.';
import { useState } from 'react';

interface LaserHeatingOverlayProps {
  hardware: HardwareTypes.EditableHardware<LaserHeatingSchema>;
  disabled: boolean;
}

function LaserHeatingOverlay(props: LaserHeatingOverlayProps) {
  const { hardware } = props;
  return (
    <OverlayTrigger
      trigger="click"
      rootClose
      overlay={
        <Popover id="laserheating_overlay">
          <Popover.Header>{hardware.name} details</Popover.Header>
          <Popover.Body>
            <Container>
              <Row>
                <Col>Background Mode</Col>
                <Col>{hardware.properties.background_mode}</Col>
              </Row>
              <Row>
                <Col>Calibration</Col>
                <Col>{hardware.properties.current_calibration}</Col>
              </Row>
              <Row>
                <Col>Fit Wavelength</Col>
                <Col>
                  {hardware.properties.fit_wavelength?.join(' - ')}{' '}
                  <span>nm</span>
                </Col>
              </Row>
            </Container>
          </Popover.Body>
        </Popover>
      }
    >
      <Button>
        <i className="fa fa-ellipsis-h" />
      </Button>
    </OverlayTrigger>
  );
}

export interface LaserHeatingOptions
  extends HardwareTypes.HardwareWidgetOptions {
  /** Default step size to use for up / down arrows */
  exposureStep?: number;
  /** Array of selectable step sizes */
  exposureSteps?: number[];
}

export default function LaserHeating(
  props: HardwareTypes.HardwareWidgetProps<
    LaserHeatingSchema,
    LaserHeatingOptions
  >
) {
  const { hardware, options = {}, disabled } = props;
  const [pending, setPending] = useState<boolean>(false);
  function requestExposureChange(value: number) {
    if (value <= 0) {
      return Promise.reject(new Error('exposure_time must be > 0'));
    }
    return hardware.actions.setProperty('exposure_time', value);
  }

  function requestMeasure() {
    setPending(true);
    const promise = hardware.actions.call('measure');

    if (promise !== undefined) {
      console.log('pending');
      promise
        .then(() => {
          setPending(false);
        })
        .catch((error) => {
          setPending(false);
        });
    }

    return promise;
  }

  const state = hardware.properties.state;

  const widgetIcon = (
    <TypeIcon name="Laser Heating" icon="fa-fire" online={hardware.online} />
  );

  const widgetState = (
    <HardwareState.HardwareState
      state={state}
      minWidth={6}
      variant={state === 'READY' ? 'success' : 'warning'}
    />
  );

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={
        <InputGroup>
          <HardwareNumericStep
            id={hardware.id}
            hardwareValue={hardware.properties.exposure_time}
            hardwareIsReady
            hardwareIsDisabled={props.disabled}
            hardwareIsMoving={false}
            onMoveRequested={requestExposureChange}
            onAbortRequested={() => null}
            precision={1}
            readOnly={false}
            step={options.exposureStep}
            steps={options.exposureSteps}
            incIcon={undefined}
            decIcon={undefined}
            swapIncDec={undefined}
          />
          <InputGroup.Text>s</InputGroup.Text>
          <Button
            size="sm"
            disabled={props.disabled || pending}
            onClick={() => {
              requestMeasure();
            }}
          >
            {pending ? (
              <i className="fa fa-spinner fa-spin" />
            ) : (
              <>
                <i className="fa fa-play me-1" />
                Measure
              </>
            )}
          </Button>
          <LaserHeatingOverlay hardware={hardware} disabled={props.disabled} />
        </InputGroup>
      }
    />
  );
}
