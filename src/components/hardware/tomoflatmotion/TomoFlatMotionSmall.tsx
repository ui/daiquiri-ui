import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import { InputGroup, Form, Stack, Alert } from 'react-bootstrap';
import { TomoFlatMotionState } from './State';
import type { MotionStep, TomoFlatMotionSchema } from '.';
import { useMotorHardware } from '../../tomo/utils/hooks';
import { useEffect, useRef, useState } from 'react';
import type { ChangeEvent, KeyboardEvent } from 'react';

function InputAxisMotion(props: {
  actions: HardwareTypes.HardwareChangeActions;
  motion: MotionStep;
  disabled?: boolean;
}) {
  const { motion, actions } = props;
  const axisRef = motion.axisRef;
  const position = motion.absolute_position ?? motion.relative_position ?? 0;

  const axisId = axisRef.slice(9);
  const axis = useMotorHardware(axisId);
  const disabled = props.disabled || !axis?.online;

  const formRef = useRef<HTMLInputElement>(null);
  const [edited, setEdited] = useState(false);
  const digits = 3;
  const unit = axis?.properties?.unit ?? '';
  const name = axis?.alias ?? axis?.name ?? axis?.id ?? 'none';

  function onKeyDown(e: KeyboardEvent) {
    if (!formRef || !formRef.current) {
      console.log('formRef not yet defined');
    } else if (e.key === 'Enter') {
      setEdited(false);
      const newPosition = Number.parseFloat(formRef.current.value);
      actions.call('update_axis', {
        axis_ref: axisRef,
        absolute_position:
          motion.absolute_position !== null ? newPosition : null,
        relative_position:
          motion.relative_position !== null ? newPosition : null,
      });
    } else if (e.key === 'Escape') {
      formRef.current.value = position.toFixed(digits);
      setEdited(false);
    } else {
      console.log(e.key);
    }
  }

  function onChange(event: ChangeEvent<HTMLInputElement>) {
    const { value } = event.target;
    if (value === undefined) {
      return;
    }
    const exp = Number.parseFloat(value);
    if (exp !== position && !edited) {
      setEdited(true);
    }
    if (exp === position && edited) {
      setEdited(false);
    }
  }

  useEffect(() => {
    if (!formRef || !formRef.current) {
      console.log('formRef not yet defined');
    } else if (!edited) {
      formRef.current.value = position.toFixed(digits);
    }
  }, [edited, position]);

  function getKind(): [string, string, string] {
    if (motion.absolute_position !== null) {
      return ['ABS', 'warning', 'Absolute displacement'];
    }
    if (motion.relative_position !== null) {
      return ['REL', 'none', 'Relative displacement'];
    }
    return ['ERR', 'danger', 'Unknown motion kind'];
  }
  const [kind, kindVariant, kindTitle] = getKind();

  return (
    <InputGroup className="w-100">
      <InputGroup.Text
        title="Displacement to apply on this axis to take a flat"
        style={{ width: '5em' }}
      >
        {name}
      </InputGroup.Text>
      <InputGroup.Text
        className={`bg-${kindVariant}`}
        title={kindTitle}
        style={{ width: '3em' }}
      >
        {kind}
      </InputGroup.Text>
      <Form.Control
        className={edited ? 'form-control-edited' : ''}
        type="number"
        ref={formRef}
        defaultValue={position.toFixed(digits)}
        onChange={onChange}
        disabled={disabled}
        onKeyDown={onKeyDown}
      />
      <InputGroup.Text>{unit}</InputGroup.Text>
    </InputGroup>
  );
}

export default function TomoFlatMotionSmall(
  props: HardwareTypes.HardwareWidgetProps<TomoFlatMotionSchema>
) {
  const { hardware, options = {} } = props;
  const { properties } = hardware;

  const widgetIcon = (
    <TypeIcon
      name="TomoFlatMotion"
      icon="fam-hardware-camera"
      online={hardware.online}
    />
  );

  const widgetState = <TomoFlatMotionState hardware={hardware} />;

  const widgetContent = (
    <>
      {properties.motion.length === 0 && (
        <Alert variant="danger" className="mb-0">
          No flat motion defined
        </Alert>
      )}
      {properties.motion.length > 0 && (
        <Stack direction="vertical">
          {properties.motion.map((motion, imotion) => {
            return (
              <InputAxisMotion
                key={motion.axisRef}
                actions={hardware.actions}
                motion={motion}
              />
            );
          })}
        </Stack>
      )}
    </>
  );

  const headerMode = options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
