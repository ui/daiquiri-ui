import { HardwareState } from '@esrf/daiquiri-lib';
import type { TomoFlatMotionSchema } from '.';

export function TomoFlatMotionState(props: { hardware: TomoFlatMotionSchema }) {
  const { hardware } = props;
  const state = hardware.online ? hardware.properties.state : 'OFFLINE';
  const variants: { [state: string]: string } = {
    UNKNOWN: 'danger',
    READY: 'success',
  };
  const variant = variants[state] || variants.UNKNOWN;
  return (
    <HardwareState.HardwareState state={state} minWidth={6} variant={variant} />
  );
}
