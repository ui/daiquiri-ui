import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import {
  Container,
  Row,
  Col,
  ButtonGroup,
  Button,
  InputGroup,
  Form,
} from 'react-bootstrap';
import { TomoFlatMotionState } from './State';
import type { MotionStep, TomoFlatMotionSchema } from '.';
import { useMotorHardware } from '../../tomo/utils/hooks';
import type { ChangeEvent, KeyboardEvent } from 'react';
import { useMemo } from 'react';
import { useEffect } from 'react';
import { useRef, useState } from 'react';

function AxisName(props: { motion: MotionStep; disabled?: boolean }) {
  const { motion } = props;
  const axisRef = motion.axisRef;
  const axisId = axisRef.slice(9);
  const axis = useMotorHardware(axisId);
  const name = axis?.alias ?? axis?.name ?? axis?.id ?? 'none';
  return <div className="my-auto">{name}</div>;
}

function DropAxis(props: {
  actions: HardwareTypes.HardwareChangeActions;
  motion: MotionStep;
  disabled?: boolean;
}) {
  const { motion, actions } = props;
  const axisRef = motion.axisRef;

  function requestDropAxis() {
    actions.call('update_axis', { axis_ref: axisRef });
  }

  return (
    <Button variant="danger" onClick={requestDropAxis}>
      <i className="fa fa-trash" />
    </Button>
  );
}

function AxisOnOff(props: {
  actions: HardwareTypes.HardwareChangeActions;
  axisRef: string;
  motion: MotionStep[];
  disabled?: boolean;
}) {
  const { motion, actions, axisRef } = props;

  const isUsed = useMemo(() => {
    for (const m of motion) {
      if (m.axisRef === axisRef) {
        return true;
      }
    }
    return false;
  }, [motion, axisRef]);

  const axisId = axisRef.slice(9);
  const axis = useMotorHardware(axisId);
  const name = axis?.alias ?? axis?.name ?? axis?.id ?? 'none';

  function requestAppendAxis() {
    actions.call('update_axis', { axis_ref: axisRef, relative_position: 0 });
  }

  return (
    <Button
      className="ms-1"
      variant={isUsed ? 'primary' : 'secondary'}
      onClick={() => {
        if (!isUsed) requestAppendAxis();
      }}
      title={
        isUsed
          ? 'This axis is already part of the motion'
          : 'Use this axis in the motion'
      }
    >
      {name}
    </Button>
  );
}

function InputAxisMotion(props: {
  actions: HardwareTypes.HardwareChangeActions;
  motion: MotionStep;
  disabled?: boolean;
}) {
  const { motion, actions } = props;
  const axisRef = motion.axisRef;
  const position = motion.absolute_position ?? motion.relative_position ?? 0;

  const axisId = axisRef.slice(9);
  const axis = useMotorHardware(axisId);
  const disabled = props.disabled || !axis?.online;

  const formRef = useRef<HTMLInputElement>(null);
  const [edited, setEdited] = useState(false);
  const digits = 3;
  const unit = axis?.properties?.unit ?? '';

  function onKeyDown(e: KeyboardEvent) {
    if (!formRef || !formRef.current) {
      console.log('formRef not yet defined');
    } else if (e.key === 'Enter') {
      setEdited(false);
      const newPosition = Number.parseFloat(formRef.current.value);
      actions.call('update_axis', {
        axis_ref: axisRef,
        absolute_position:
          motion.absolute_position !== null ? newPosition : null,
        relative_position:
          motion.relative_position !== null ? newPosition : null,
      });
    } else if (e.key === 'Escape') {
      formRef.current.value = position.toFixed(digits);
      setEdited(false);
    } else {
      console.log(e.key);
    }
  }

  function onChange(event: ChangeEvent<HTMLInputElement>) {
    const { value } = event.target;
    if (value === undefined) {
      return;
    }
    const exp = Number.parseFloat(value);
    if (exp !== position && !edited) {
      setEdited(true);
    }
    if (exp === position && edited) {
      setEdited(false);
    }
  }

  useEffect(() => {
    if (!formRef || !formRef.current) {
      console.log('formRef not yet defined');
    } else if (!edited) {
      formRef.current.value = position.toFixed(digits);
    }
  }, [edited, position]);

  return (
    <InputGroup className="w-100">
      <Form.Control
        className={edited ? 'form-control-edited' : ''}
        type="number"
        ref={formRef}
        defaultValue={position.toFixed(digits)}
        onChange={onChange}
        disabled={disabled}
        onKeyDown={onKeyDown}
      />
      <InputGroup.Text>{unit}</InputGroup.Text>
    </InputGroup>
  );
}

function MotionKind(props: {
  actions: HardwareTypes.HardwareChangeActions;
  motion: MotionStep;
}) {
  const { actions, motion } = props;

  function requestAbsoluteMotion() {
    actions.call('update_axis', {
      axis_ref: motion.axisRef,
      absolute_position: motion.absolute_position ?? motion.relative_position,
    });
  }

  function requestRelativeMotion() {
    actions.call('update_axis', {
      axis_ref: motion.axisRef,
      relative_position: motion.absolute_position ?? motion.relative_position,
    });
  }

  return (
    <ButtonGroup>
      <Button
        onClick={requestAbsoluteMotion}
        variant={motion.absolute_position !== null ? 'primary' : 'secondary'}
      >
        Abs
      </Button>
      <Button
        onClick={requestRelativeMotion}
        variant={motion.relative_position !== null ? 'primary' : 'secondary'}
      >
        Rel
      </Button>
    </ButtonGroup>
  );
}

export default function TomoFlatMotionDefault(
  props: HardwareTypes.HardwareWidgetProps<TomoFlatMotionSchema>
) {
  const { hardware, options = {} } = props;
  const { properties } = hardware;

  const widgetIcon = (
    <TypeIcon
      name="TomoFlatMotion"
      icon="fam-hardware-camera"
      online={hardware.online}
    />
  );

  const widgetState = <TomoFlatMotionState hardware={hardware} />;

  const widgetContent = (
    <Container>
      <Row className="mt-1">
        <Col xs={3}>Move in parallel</Col>
        <Col xs={9}>{properties.move_in_parallel ? 'true' : 'false'}</Col>
      </Row>
      <Row className="mt-1">
        <Col xs={3}>Switch power on/off</Col>
        <Col xs={9}>{properties.power_onoff ? 'true' : 'false'}</Col>
      </Row>
      <Row className="mt-1">
        <Col xs={3}>Settle time</Col>
        <Col xs={9}>
          {properties.settle_time.toFixed(2)} <span title="seconds">s</span>
        </Col>
      </Row>
      <Row className="mt-1">
        <Col xs={3}>Available axes</Col>
        <Col xs={9}>
          {properties.available_axes.map((axisRef) => {
            return (
              <AxisOnOff
                key={axisRef}
                axisRef={axisRef}
                motion={properties.motion}
                actions={hardware.actions}
              />
            );
          })}
        </Col>
      </Row>
      <Row className="mt-1">
        <Col xs={3}>Actual motion</Col>
        <Col xs={9}>
          <div
            style={{
              display: 'grid',
              gridGap: 5,
              gridTemplateColumns: 'min-content min-content auto min-content',
            }}
          >
            <div>Axis</div>
            <div>Kind</div>
            <div>Displacement</div>
            <div />
            {properties.motion.map((motion, imotion) => {
              return (
                <>
                  <AxisName key={`motion${imotion}-0`} motion={motion} />
                  <MotionKind
                    key={`motion${imotion}-1`}
                    motion={motion}
                    actions={hardware.actions}
                  />
                  <InputAxisMotion
                    key={`motion${imotion}-2`}
                    motion={motion}
                    actions={hardware.actions}
                  />
                  <DropAxis
                    key={`motion${imotion}-3`}
                    motion={motion}
                    actions={hardware.actions}
                  />
                </>
              );
            })}
          </div>
        </Col>
      </Row>
    </Container>
  );

  const headerMode = options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
