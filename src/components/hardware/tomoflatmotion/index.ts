import { HardwareVariant } from '@esrf/daiquiri-lib';
import TomoFlatMotionDefault from './TomoFlatMotionDefault';
import TomoFlatMotionSmall from './TomoFlatMotionSmall';
import type { HardwareTypes, Hardware } from '@esrf/daiquiri-lib';

export interface MotionStep {
  /** Axis reference, following the pattern `hardware:my_axis` */
  axisRef: string;
  absolute_position: number | null;
  relative_position: number | null;
}

/**
 * Hardware as exposed by Redux
 */
export interface TomoFlatMotionSchema extends HardwareTypes.Hardware {
  properties: {
    state: 'READY';
    available_axes: string[];
    motion: MotionStep[];
    move_in_parallel: boolean;
    power_onoff: boolean;
    settle_time: number;
  };
}

export default class Default extends HardwareVariant {
  public variants = {
    default: TomoFlatMotionDefault,
    small: TomoFlatMotionSmall,
  };
}

export function isTomoFlatMotionHardware(
  entity: Hardware
): entity is TomoFlatMotionSchema {
  return entity.type === 'tomoflatmotion';
}
