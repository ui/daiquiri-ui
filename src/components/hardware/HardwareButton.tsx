import { useEffect, useMemo, useState } from 'react';
import { Button, InputGroup, Form } from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import hardwarep from 'providers/hardware';

export interface HardwareButtonObject {
  /** The hardware id */
  id: string;
  /** The function to execute i.e. `move` */
  function?: string;
  /** The property to change, i.e. `speed` */
  property?: string;
  /** In absolute mode, the value to move to */
  value?: string;
  /** In relative mode, how much it should increment the value by */
  increment?: string;
  /** The reference value, i.e. `position` */
  reference?: string;
}

export interface HardwareButtonProps {
  objects: Record<string, HardwareTypes.Hardware>;
  operator: boolean;
  runningScan?: boolean;
  /** Button title */
  buttonTitle: string;
  /** or icon fa-arrow-up fam-... */
  icon?: string;
  /** List of objects and values */
  ids: HardwareButtonObject[];
  /** Selectable step sizes for relative mode (assumes all objects will get same step change) */
  steps?: number[];
}

/**
 * FIXME: It would be better to drop that API, if possible
 */
function useRequestChange() {
  return useMemo(() => {
    return (payload: any) => {
      return hardwarep.dispatch('REQUEST_HARDWARE_CHANGE', payload);
    };
  }, []);
}

function HardwareButton(props: HardwareButtonProps) {
  const [currentStep, setCurrentStep] = useState<number>(0);
  const [loading, setLoading] = useState<boolean>(false);
  const requestChange = useRequestChange();

  function onClick() {
    const pendingPromises: Promise<any>[] = [];

    setLoading(true);
    props.ids.forEach((obj) => {
      if (obj.reference && (obj.increment || props.steps)) {
        const object: HardwareTypes.Hardware = props.objects[obj.id];
        pendingPromises.push(
          requestChange({
            ...obj,
            value:
              object.properties[obj.reference] +
              (props.steps ? currentStep : obj.increment),
          })
        );
      } else if (obj.function || obj.property) {
        pendingPromises.push(requestChange(obj));
      }
    });

    Promise.all(pendingPromises).finally(() => {
      setLoading(false);
    });
  }

  useEffect(() => {
    if (props.steps) {
      setCurrentStep(props.steps[0]);
    }
  }, [props.steps]);

  const disabled = loading || !props.operator || props.runningScan;

  return (
    <InputGroup>
      <Button onClick={onClick} disabled={disabled}>
        {loading ? (
          <i className="fa fa-spinner fa-spin" />
        ) : (
          <>
            {props.icon ? (
              <i className={`fa ${props.icon}`} title={props.buttonTitle} />
            ) : (
              props.buttonTitle
            )}
          </>
        )}
      </Button>
      {props.steps && (
        <Form.Control
          as="select"
          className="step-size"
          defaultValue={currentStep}
          onChange={(e) => setCurrentStep(Number.parseFloat(e.target.value))}
        >
          {props.steps.map((step) => (
            <option value={step}>{step}</option>
          ))}
        </Form.Control>
      )}
    </InputGroup>
  );
}

export default HardwareButton;
