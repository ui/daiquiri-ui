import { Children, cloneElement, isValidElement, useState } from 'react';
import type { ReactChild } from 'react';
import { ListGroup, Button } from 'react-bootstrap';

interface Event {
  kind: 'call' | 'setProperty';
  event: any;
}

interface Props {
  showActions: boolean;
  children: ReactChild | ReactChild[];
}

/**
 * Wrap hardware widget passed as children with addToast and hardware
 * change actions triggering console log.
 */
export default function Wrapper(props: Props) {
  const { showActions = false } = props;

  const [actions, setActions] = useState<Event[]>([]);

  function call(name: string, value?: any) {
    const event = { type: 'call', name, value };
    if (showActions) {
      setActions([{ kind: 'call', event }, ...actions]);
    } else {
      console.log('Request change:', 'call', name, value);
    }
  }

  function setProperty(name: string, value: any) {
    const event = { type: 'setProperty', name, value };
    if (showActions) {
      setActions([{ kind: 'setProperty', event }, ...actions]);
    } else {
      console.log('Request change:', event);
    }
  }

  return (
    <>
      {Children.map(props.children, (child) => {
        return isValidElement(child)
          ? cloneElement(child, {
              ...child.props,
              hardware: {
                actions: {
                  setProperty,
                  call,
                },
                ...child.props.hardware,
              },
            })
          : undefined;
      })}
      {actions.length > 0 && (
        <div>
          <Button
            onClick={() => {
              setActions([]);
            }}
          >
            Clear
          </Button>
          <ListGroup variant="flush">
            {actions.map((v, index) => {
              return (
                <ListGroup.Item
                  variant="secondary"
                  className="text-monospace"
                  key={index}
                >
                  {v.kind} {JSON.stringify(v.event)}
                </ListGroup.Item>
              );
            })}
          </ListGroup>
        </div>
      )}
    </>
  );
}
