import { Frontend } from '@esrf/daiquiri-lib';
import { HardwareVariant } from '@esrf/daiquiri-lib';

export default class Default extends HardwareVariant {
  public variants = {
    default: Frontend,
  };
}
