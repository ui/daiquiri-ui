import type { Hardware, HardwareTypes } from '@esrf/daiquiri-lib';

export interface TomoHoloDistance {
  z1: number;
  position: number;
  pixel_size: number;
}

/**
 * Hardware as exposed by Redux
 */
export interface TomoHoloSchema extends HardwareTypes.Hardware {
  properties: {
    state: string;
    settle_time: number;
    pixel_size: number | null;
    nb_distances: number;
    distances: TomoHoloDistance[];
  };
}

export function isTomoHoloHardware(entity: Hardware): entity is TomoHoloSchema {
  return entity.type === 'tomoholo';
}
