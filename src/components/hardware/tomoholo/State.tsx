import { HardwareState } from '@esrf/daiquiri-lib';
import type { TomoHoloSchema } from '.';

export function TomoHoloState(props: { hardware: TomoHoloSchema }) {
  const { hardware } = props;
  const state = hardware.online ? hardware.properties.state : 'OFFLINE';
  const variants: { [state: string]: string } = {
    READY: 'success',
    MOVING: 'warning',
    STABILIZING: 'warning',
    INVALID: 'danger',
    INVALID_DETECTOR_BINNING_CHANGED: 'danger',
    INVALID_DETECTOR_DISTANCE_CHANGED: 'danger',
    INVALID_DETECTOR_CHANGED: 'danger',
  };
  const variant = variants[state] || variants.UNKNOWN;
  return (
    <HardwareState.HardwareState state={state} minWidth={6} variant={variant} />
  );
}
