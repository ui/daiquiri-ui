import type { Hardware, HardwareTypes } from '@esrf/daiquiri-lib';
import { HardwareVariant } from '@esrf/daiquiri-lib';

/**
 * Hardware as exposed by Redux
 */
export interface TomoSampleStageSchema extends HardwareTypes.Hardware {
  properties: {
    sx: string;
    sy: string;
    sz: string;
    somega: string;
    sampx: string;
    sampy: string;
    sampu: string;
    sampv: string;
    x_axis_focal_pos: number | null;
    detector_center: [number | null, number | null];
  };
}

export default class Default extends HardwareVariant {
  public variants = {};
}

export function isTomoSampleStageHardware(
  entity: Hardware
): entity is TomoSampleStageSchema {
  return entity.type === 'tomosamplestage';
}
