import { map } from 'lodash';

import {
  Badge,
  Button,
  InputGroup,
  Form,
  OverlayTrigger,
  Popover,
  Container,
  Row,
  Col,
} from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon } from '@esrf/daiquiri-lib';
import type { PumpSchema } from '.';

interface PumpWidgetOptions extends HardwareTypes.HardwareWidgetOptions {
  // Unit of pressure
  unit?: string;
  // Precision to show, default 3
  precision?: number;
}

function PumpOverlay(
  props: HardwareTypes.HardwareWidgetProps<PumpSchema, PumpWidgetOptions>
) {
  const { hardware } = props;
  const toggle = () => {
    hardware.actions.call(hardware.properties.state === 'ON' ? 'off' : 'on');
  };

  const attrs = {
    Voltage: `${hardware.properties.voltage.toFixed(0)} V`,
    Current: `${(hardware.properties.current * 1000).toFixed(2)} mA`,
  };

  return (
    <OverlayTrigger
      trigger="click"
      rootClose
      overlay={
        <Popover id="popid">
          <Popover.Header>{hardware.name} Details</Popover.Header>
          <Popover.Body>
            <h6>Status</h6>
            <pre>{hardware.properties.status}</pre>

            <h6>Properties</h6>
            <Container>
              {map(attrs, (prop, name) => (
                <Row key={name}>
                  <Col>{name}:</Col>
                  <Col>{prop}</Col>
                </Row>
              ))}
            </Container>

            <div className="d-grid gap-2">
              <Button
                disabled={props.disabled}
                onClick={toggle}
                variant={
                  hardware.properties.state === 'ON' ? 'danger' : 'success'
                }
              >
                {hardware.properties.state === 'ON' ? 'Off' : 'On'}
              </Button>
            </div>
          </Popover.Body>
        </Popover>
      }
    >
      <Button title="Pump Details">
        <i className="fa fa-ellipsis-h" />
      </Button>
    </OverlayTrigger>
  );
}

function Pump(
  props: HardwareTypes.HardwareWidgetProps<PumpSchema, PumpWidgetOptions>
) {
  const { hardware, options = {} } = props;
  return (
    <div className="hw-component">
      <div className="hw-head">
        <TypeIcon
          name="Pump"
          icon="fam-hardware-pump"
          online={hardware.online}
        />
        <div className="name">{hardware.name}</div>
        <Badge bg={hardware.properties.state === 'ON' ? 'success' : 'danger'}>
          {hardware.properties.state}
        </Badge>
      </div>
      <div className="hw-content">
        <InputGroup>
          <Form.Control
            value={hardware.properties.pressure.toExponential(
              options.precision || 3
            )}
            disabled
          />
          {(hardware.properties.unit || options.unit) && (
            <InputGroup.Text>
              {hardware.properties.unit || options.unit}
            </InputGroup.Text>
          )}
          <PumpOverlay {...props} />
        </InputGroup>
      </div>
    </div>
  );
}

export default Pump;
