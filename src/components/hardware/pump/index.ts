import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { HardwareVariant } from '@esrf/daiquiri-lib';
import Pump from './Pump';

/**
 * Hardware as exposed by Redux
 */
export interface PumpSchema extends HardwareTypes.Hardware {
  properties: {
    state: string;
    status: string;
    pressure: number;
    voltage: number;
    current: number;
    unit?: string; // FIXME: The server implementation is missing
  };
}

export default class Default extends HardwareVariant {
  public variants = {
    default: Pump,
  };
}
