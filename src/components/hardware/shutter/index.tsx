import { ShutterDefault, HardwareVariant } from '@esrf/daiquiri-lib';
import ShutterProtected from './ShutterProtected';

export type { ShutterSchema } from '@esrf/daiquiri-lib';

export default class Shutter extends HardwareVariant {
  public variants = {
    default: ShutterDefault,
    protected: ShutterProtected,
  };
}
