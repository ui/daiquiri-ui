import {
  InputGroup,
  Button,
  ButtonGroup,
  Form,
  Dropdown,
  Container,
  Row,
  Col,
} from 'react-bootstrap';
import type { HardwareTypes, ShutterSchema } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate, ShutterState } from '@esrf/daiquiri-lib';
import ReadOnlyTextInput from 'components/common/ReadOnlyTextInput';

function DropdownShutter(
  props: HardwareTypes.HardwareWidgetProps<
    ShutterSchema,
    HardwareTypes.HardwareWidgetOptions
  >
) {
  const { hardware, options = {} } = props;
  function requestOpen() {
    hardware.actions.call('open');
  }

  function requestClose() {
    hardware.actions.call('close');
  }

  const state = hardware.properties?.state ?? 'UNKNOWN';
  const hardwareMoving = state === 'MOVING';
  const hardwareDisabled = state === 'DISABLED' || hardwareMoving;

  return (
    <Dropdown>
      <Dropdown.Toggle
        variant="secondary"
        disabled={props.disabled || hardwareDisabled}
      />
      <Dropdown.Menu>
        <Container>
          <Row>
            <Col className="text-center">Actions</Col>
          </Row>
          <Row>
            <Col>
              <ButtonGroup>
                <Button
                  variant={hardwareMoving ? 'warning' : 'secondary'}
                  onClick={() => {
                    requestOpen();
                  }}
                  disabled={props.disabled || hardwareDisabled}
                  title="Open the shutter"
                >
                  Open
                </Button>
                <Button
                  variant={hardwareMoving ? 'warning' : 'secondary'}
                  onClick={() => {
                    requestClose();
                  }}
                  disabled={props.disabled || hardwareDisabled}
                  title="Close the shutter"
                >
                  Close
                </Button>
              </ButtonGroup>
            </Col>
          </Row>
        </Container>
      </Dropdown.Menu>
    </Dropdown>
  );
}

export default function ShutterProtected(
  props: HardwareTypes.HardwareWidgetProps<
    ShutterSchema,
    HardwareTypes.HardwareWidgetOptions
  >
) {
  const { hardware, options = {} } = props;
  const widgetIcon = (
    <TypeIcon
      name="Shutter"
      icon="fam-hardware-shutter"
      online={hardware.online}
    />
  );

  const widgetState = <ShutterState hardware={hardware} useReadyState />;

  function getState() {
    if (!hardware.properties) {
      return 'UNKNOWN';
    }
    return hardware.properties.state;
  }

  function getValue(s: string) {
    if (s === 'OPEN' || s === 'CLOSED') {
      return s;
    }
    if (s === 'DISABLED' || s === 'STANDBY' || s === 'FAULT') {
      return 'CLOSED';
    }
    if (s === 'MOVING') {
      return '...';
    }
    return 'UNKNOWN';
  }

  function capitalize(s: string) {
    return s.charAt(0).toUpperCase() + s.slice(1).toLowerCase();
  }

  const state = getState();
  const value = getValue(state);
  const hardwareDisabled = state === 'DISABLED' || state === 'MOVING';

  const variants: { [name: string]: string } = {
    OPEN: 'success',
    CLOSED: 'danger',
    MOVING: 'warning',
    DISABLED: 'secondary',
    STANDBY: 'success',
    FAULT: 'fatal',
    UNKNOWN: 'fatal',
  };
  const variant = variants[state];
  const valueName = capitalize(value);

  const widgetContent = (
    <InputGroup className="d-flex flex-nowrap">
      <ReadOnlyTextInput
        variant={variant}
        className="text-center"
        style={{ minWidth: '10em' }}
        value={valueName}
      />
      <DropdownShutter {...props} />
    </InputGroup>
  );

  const headerMode = options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
