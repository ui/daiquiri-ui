import type { HardwareTypes } from '@esrf/daiquiri-lib';

/**
 * Hardware as exposed by Redux
 */
export interface ActuatorSchema extends HardwareTypes.Hardware {
  properties: {
    state: string;
  };
}
