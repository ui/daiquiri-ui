import type { ComponentType } from 'react';
import { Badge } from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { ShutterState, MotorState } from '@esrf/daiquiri-lib';
import { OpticState } from 'components/hardware/optic/State';
import { ProcedureState } from 'components/hardware/procedure/State';
import { TomoImagingState } from 'components/hardware/tomoimaging/State';
import { TomoDetectorState } from 'components/hardware/tomodetector/State';
import { TomoDetectorsState } from 'components/hardware/tomodetectors/State';
import { ServiceState } from 'components/hardware/service/State';
import { LimaState } from 'components/hardware/lima/State';
import { PusherState } from 'components/hardware/pusher/State';
import { AirBearingState } from 'components/hardware/airbearing/State';

function AnyHardwareState(props: { hardware: HardwareTypes.Hardware }) {
  const { hardware } = props;
  const { online, locked = null } = hardware;
  const state = online
    ? (locked !== null ? 'LOCKED' : hardware.properties.state) || 'UNKNOWN'
    : 'OFFLINE';

  // Try to catch some common state name
  const variantMapping: { [name: string]: string } = {
    READY: 'success',
    OFFLINE: 'warning',
    MOVING: 'warning',
    DISABLED: 'secondary',
    UNKNOWN: 'danger',
    ERROR: 'danger',
    FAILED: 'danger',
    LOCKED: 'warning',
  };

  const variant = variantMapping[state] || 'success';

  return <Badge bg={variant}>{state}</Badge>;
}

export default function DefaultState(props: HardwareTypes.HardwareWidgetProps) {
  const { hardware } = props;
  const stateMapping: { [type: string]: ComponentType<any> } = {
    motor: MotorState,
    optic: OpticState,
    service: ServiceState,
    shutter: ShutterState,
    tomodetectors: TomoDetectorsState,
    tomodetector: TomoDetectorState,
    tomoimaging: TomoImagingState,
    procedure: ProcedureState,
    lima: LimaState,
    pusher: PusherState,
    airbearing: AirBearingState,
  };

  const Widget = stateMapping[hardware.type] || AnyHardwareState;

  return <Widget hardware={hardware} useReadyState />;
}
