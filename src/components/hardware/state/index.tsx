import { HardwareVariant } from '@esrf/daiquiri-lib';
import DefaultState from './StateDefault';

export default class Shutter extends HardwareVariant {
  public variants = {
    default: DefaultState,
  };
}
