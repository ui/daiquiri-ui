import { HardwareVariant } from '@esrf/daiquiri-lib';
import type { Hardware, HardwareTypes } from '@esrf/daiquiri-lib';
import LimaDefault from './LimaDefault';
import LimaSize from './LimaSize';
import LimaTransformation from './LimaTransformation';
import LimaBinning from './LimaBinning';
import LimaImageRoi from './LimaImageRoi';
import LimaDescription from './LimaDescription';

/**
 * Hardware as exposed by Redux
 */
export interface LimaSchema extends HardwareTypes.Hardware {
  properties: {
    state:
      | 'READY'
      | 'ACQUIRING'
      | 'CONFIGURATION'
      | 'FAULT'
      | 'UNKNOWN'
      | 'OFFLINE';

    static: {
      /* The lima core library version number*/
      lima_version: string;

      /* Name of the camera plugin: Maxipix, Pilatus, Frelon, Pco, Basler, Simulator...*/
      lima_type: string;

      /* Type of the camera as exposed by the camera plugin.*/
      camera_type: string;

      /* Model of the camera as exposed by the camera plugin: 5x1- TPX1*/
      camera_model: string;

      /* The camera pixel size in x and y dimension, in micron

      Despit the Lima Tango API, this value is returned in micron instead of meter.
      */
      camera_pixelsize: [number, number];

      /* Maximum image dimension, width and height in pixel*/
      image_max_dim: [number, number];
    } | null;

    /**
     * Pixel size of the detector (without binning).
     *
     * It's a read-only property.
     */
    pixel_size: [number, number];

    /**
     * Max exposition time of a single sub frame in accumulation.
     *
     * It is used (together with the exposure time) to determine the
     * nb of sub frames and the expo time of a single sub frame.
     * See `guessSubframes`.
     */
    acc_max_expo_time: number | null;

    /**
     * Flip applied to the resulting image
     */
    flip: [boolean, boolean];

    /**
     * Rotation applied to the resulting image.
     *
     * One of 0, 90, 180, 270
     */
    rotation: 0 | 90 | 180 | 270;

    /**
     * Binning applied to the resulting image
     */
    binning: [number, number];

    /**
     * Location and size of the ROI in the raw image.
     *
     * When binning=[1, 1] flip=[false, false], rotation=0
     */
    raw_roi: [number, number, number, number];

    /**
     * Location of the ROI in the transformed image.
     */
    roi: [number, number, number, number];

    /**
     * Final size of the detector. Including the crop of the ROI.
     */
    size: [number, number];
  };
}

export function isLimaHardware(entity: Hardware): entity is LimaSchema {
  return entity.type === 'lima';
}

/**
 * Compute the nb sub frames which will be used for a specific exposure time
 */
export function guessSubframes(
  exposureTime: number,
  accMaxExpoTime: number
): [number, number] {
  const nbSubFrames = Math.ceil(exposureTime / accMaxExpoTime);
  const accExpoTime = exposureTime / nbSubFrames;
  return [nbSubFrames, accExpoTime];
}

export default class Default extends HardwareVariant {
  public variants = {
    default: LimaDefault,
    size: LimaSize,
    transformation: LimaTransformation,
    binning: LimaBinning,
    imageroi: LimaImageRoi,
    description: LimaDescription,
  };
}
