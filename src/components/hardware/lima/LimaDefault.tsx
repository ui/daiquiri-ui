import { Stack } from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import { LimaState } from './State';
import type { LimaSchema } from '.';

function Content(props: HardwareTypes.HardwareWidgetProps<LimaSchema>) {
  const { hardware, options = {} } = props;

  return (
    <Stack className="bg-light rounded-2 ps-2 p-1" direction="horizontal">
      <div>{hardware.name}</div>
    </Stack>
  );
}

export default function LimaDefault(
  props: HardwareTypes.HardwareWidgetProps<LimaSchema>
) {
  const { hardware, options = {} } = props;
  const widgetIcon = (
    <TypeIcon name="Lima" icon="fam-hardware-camera" online={hardware.online} />
  );

  const widgetState = <LimaState hardware={hardware} />;

  const headerMode = options.header || 'top';
  if (headerMode === 'none') {
    return <Content {...props} />;
  }

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={<Content {...props} />}
      headerMode={headerMode}
    />
  );
}
