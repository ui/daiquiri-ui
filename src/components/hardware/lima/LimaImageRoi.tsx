import { InputGroup, Form, Badge } from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import type { LimaSchema } from '.';
import { LimaState } from './State';

function LimaRoiImageControl(props: { hardware: LimaSchema }) {
  const { hardware } = props;
  const { properties } = hardware;
  return (
    <InputGroup>
      <InputGroup.Text title="Rotation applied to the detector">
        <i className="fa fa-fw fa-crop-simple fa-2x" />
      </InputGroup.Text>
      <Form.Control value={`${properties.roi[0]}`} readOnly />
      <Form.Control value={`${properties.roi[1]}`} readOnly />
      <Form.Control value={`${properties.roi[2]}`} readOnly />
      <Form.Control value={`${properties.roi[3]}`} readOnly />
      <InputGroup.Text>px</InputGroup.Text>
    </InputGroup>
  );
}

export default function TomoDetectorSize(
  props: HardwareTypes.HardwareWidgetProps<
    LimaSchema,
    HardwareTypes.HardwareWidgetOptions
  >
) {
  const { hardware, options = {} } = props;
  const widgetIcon = (
    <TypeIcon name="Lima" icon="fam-hardware-camera" online={hardware.online} />
  );

  const widgetState = <LimaState hardware={hardware} />;

  const widgetContent = (
    <>
      <LimaRoiImageControl hardware={hardware} />
    </>
  );

  const headerMode = options.header ?? 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
