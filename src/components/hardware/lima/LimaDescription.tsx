import { InputGroup, Form } from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import type { LimaSchema } from '.';
import { LimaState } from './State';

function LimaMaxSizeControl(props: { hardware: LimaSchema }) {
  const { hardware } = props;
  const { properties } = hardware;
  const struct = properties.static;
  const [width, height] = struct?.image_max_dim ?? ['?', '?'];
  return (
    <InputGroup>
      <InputGroup.Text title="Max size of the detector">
        Max size
      </InputGroup.Text>
      <Form.Control value={`${width} × ${height}`} readOnly />
      <InputGroup.Text>px</InputGroup.Text>
    </InputGroup>
  );
}

function LimaLimaVersionControl(props: { hardware: LimaSchema }) {
  const { hardware } = props;
  const { properties } = hardware;
  const struct = properties.static;
  return (
    <InputGroup>
      <InputGroup.Text title="Version of the Lima server">
        Lima version
      </InputGroup.Text>
      <Form.Control value={`${struct?.lima_version}`} readOnly />
    </InputGroup>
  );
}

function LimaModelControl(props: { hardware: LimaSchema }) {
  const { hardware } = props;
  const { properties } = hardware;
  const struct = properties.static;
  return (
    <InputGroup>
      <InputGroup.Text title="Model of the detector">
        Type/model
      </InputGroup.Text>
      <Form.Control value={`${struct?.camera_type}`} readOnly />
      <Form.Control value={`${struct?.camera_model}`} readOnly />
    </InputGroup>
  );
}

export default function TomoDetectorSize(
  props: HardwareTypes.HardwareWidgetProps<
    LimaSchema,
    HardwareTypes.HardwareWidgetOptions
  >
) {
  const { hardware, options = {} } = props;
  const widgetIcon = (
    <TypeIcon name="Lima" icon="fam-hardware-camera" online={hardware.online} />
  );

  const widgetState = <LimaState hardware={hardware} />;

  const widgetContent = (
    <>
      <LimaModelControl hardware={hardware} />
      <LimaMaxSizeControl hardware={hardware} />
      <LimaLimaVersionControl hardware={hardware} />
    </>
  );

  const headerMode = options.header ?? 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
