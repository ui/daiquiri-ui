import { InputGroup, Form, Badge } from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import type { LimaSchema } from '.';
import { LimaState } from './State';

function LimaTransformationFlags(props: { hardware: LimaSchema }) {
  const { hardware } = props;
  const { properties } = hardware;

  const [flipH, flipV] = properties.flip;
  const rotation = properties.rotation ?? 0;
  const noTransformation = !flipH && !flipV && rotation === 0;
  return (
    <div>
      <InputGroup.Text>
        {flipH && (
          <Badge style={{ fontSize: '130%' }} className="me-1">
            Flip
            <i className="fa fa-fw fa-left-right" />
          </Badge>
        )}
        {flipV && (
          <Badge style={{ fontSize: '130%' }} className="me-1">
            Flip
            <i className="fa fa-fw fa-up-down" />
          </Badge>
        )}
        {rotation !== 0 && (
          <Badge style={{ fontSize: '130%' }} className="me-1">
            Rot {rotation.toFixed(0)}
            <i className="fa fa-fw fa-arrow-rotate-right" />
          </Badge>
        )}
        {noTransformation && (
          <Badge
            bg="secondary"
            style={{ fontSize: '130%' }}
            title="No transformation"
          >
            None
          </Badge>
        )}
      </InputGroup.Text>
    </div>
  );
}

export default function TomoDetectorSize(
  props: HardwareTypes.HardwareWidgetProps<
    LimaSchema,
    HardwareTypes.HardwareWidgetOptions
  >
) {
  const { hardware, options = {} } = props;
  const widgetIcon = (
    <TypeIcon name="Lima" icon="fam-hardware-camera" online={hardware.online} />
  );

  const widgetState = <LimaState hardware={hardware} />;

  const widgetContent = (
    <>
      <LimaTransformationFlags hardware={hardware} />
    </>
  );

  const headerMode = options.header ?? 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
