import { Stack } from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import type { LimaSchema } from '.';
import { LimaState } from './State';

function Content(
  props: HardwareTypes.HardwareWidgetProps<
    LimaSchema,
    HardwareTypes.HardwareWidgetOptions
  >
) {
  const { hardware, options = {} } = props;

  const [width, height] = hardware.properties.size ?? ['?', '?'];

  return (
    <Stack className="bg-light rounded-2 ps-2 p-1" direction="horizontal">
      <div>
        {width} × {height}
      </div>
      <div className="ms-auto">
        <small>
          <small>px</small>
        </small>
      </div>
    </Stack>
  );
}

export default function TomoDetectorSize(
  props: HardwareTypes.HardwareWidgetProps<
    LimaSchema,
    HardwareTypes.HardwareWidgetOptions
  >
) {
  const { hardware, options = {} } = props;
  const widgetIcon = (
    <TypeIcon name="Lima" icon="fam-hardware-camera" online={hardware.online} />
  );

  const widgetState = <LimaState hardware={hardware} />;

  const headerMode = options.header ?? 'top';
  if (headerMode === 'none') {
    return <Content {...props} />;
  }

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={<Content {...props} />}
      headerMode={headerMode}
    />
  );
}
