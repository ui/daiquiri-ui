import { InputGroup, Form, Badge, Button } from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import type { LimaSchema } from '.';
import { LimaState } from './State';

function LimaBinningControl(
  props: HardwareTypes.HardwareWidgetProps<
    LimaSchema,
    HardwareTypes.HardwareWidgetOptions
  >
) {
  const { hardware, disabled } = props;
  const { properties } = hardware;
  const [bh, bv] = properties.binning;
  const b = bh === bv ? bh : 0;

  function setBinning(value: number) {
    hardware.actions.setProperty('binning', [value, value]);
  }

  return (
    <InputGroup>
      <InputGroup.Text title="Binning applied to the detector">
        <i className="fa fa-fw fa-ruler-combined fa-2x" />
      </InputGroup.Text>
      <Form.Control value={`${properties.binning[0]}`} readOnly />
      <Form.Control value={`${properties.binning[1]}`} readOnly />
      <Button
        variant={b === 1 ? 'primary' : 'secondary'}
        onClick={() => {
          setBinning(1);
        }}
        disabled={disabled}
      >
        1
      </Button>
      <Button
        variant={b === 2 ? 'primary' : 'secondary'}
        onClick={() => {
          setBinning(2);
        }}
        disabled={disabled}
      >
        2
      </Button>
      <Button
        variant={b === 3 ? 'primary' : 'secondary'}
        onClick={() => {
          setBinning(3);
        }}
        disabled={disabled}
      >
        3
      </Button>
      <Button
        variant={b === 4 ? 'primary' : 'secondary'}
        onClick={() => {
          setBinning(4);
        }}
        disabled={disabled}
      >
        4
      </Button>
    </InputGroup>
  );
}

export default function LimaBinning(
  props: HardwareTypes.HardwareWidgetProps<
    LimaSchema,
    HardwareTypes.HardwareWidgetOptions
  >
) {
  const { hardware, options = {} } = props;
  const widgetIcon = (
    <TypeIcon name="Lima" icon="fam-hardware-camera" online={hardware.online} />
  );

  const widgetState = <LimaState hardware={hardware} />;

  const widgetContent = (
    <>
      <LimaBinningControl {...props} />
    </>
  );

  const headerMode = options.header ?? 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
