import { HardwareState } from '@esrf/daiquiri-lib';
import type { LimaSchema } from '.';

export function LimaState(props: { hardware: LimaSchema }) {
  const { hardware } = props;
  const { online, locked = null } = hardware;
  const state = hardware.properties.state ?? 'UNKNOWN';
  console.log(state);
  const finalState = online
    ? (state === 'ACQUIRING' ? state : locked !== null ? 'LOCKED' : state) ||
      'UNKNOWN'
    : 'OFFLINE';

  const variants: { [state: string]: string } = {
    UNKNOWN: 'critical',
    ACQUIRING: 'warning',
    READY: 'success',
    OFFLINE: 'secondary',
    LOCKED: 'warning',
  };
  const variant = variants[finalState] ?? variants.UNKNOWN;
  return (
    <HardwareState.HardwareState
      state={finalState}
      minWidth={6}
      variant={variant}
    />
  );
}
