import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { HardwareVariant } from '@esrf/daiquiri-lib';
import ProcedureManage from './ProcedureManage';
import ProcedureExecution from './ProcedureExecution';
import ProcedureValidate from './ProcedureValidate';
import ProcedureParameters from './ProcedureParameters';

export interface ProcedureSchema extends HardwareTypes.Hardware {
  properties: {
    state: string;
    previous_run_state: string;
    previous_run_exception: string | null;
    parameters: Record<string, any>;
  };
}

export default class Procedure extends HardwareVariant {
  public variants = {
    default: ProcedureManage,
    manage: ProcedureManage,
    execution: ProcedureExecution,
    validate: ProcedureValidate,
    parameters: ProcedureParameters,
  };
}

export interface HardwareFromProcedure {
  __type__: 'hardware';
  name: string;
}

export interface ScanFromProcedure {
  __type__: 'scan';
  mmh3: number;
  node?: string;
  key?: string;
}

export interface ExceptionFromProcedure {
  __type__: 'exception';
  class: string;
  message: string;
  traceback: Record<string, any>;
}
