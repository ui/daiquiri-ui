import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import { Container, Badge } from 'react-bootstrap';
import { ProcedureState } from './State';
import type { ProcedureSchema } from '.';

function State(props: {
  hardware: HardwareTypes.EditableHardware<ProcedureSchema>;
}) {
  const { hardware } = props;
  const awaitingUserInput = hardware.properties.state === 'AWAITING_USER_INPUT';
  const aborting = hardware.properties.state === 'ABORTING';
  const isRunning = hardware.properties.state === 'RUNNING';

  if (aborting) {
    return (
      <Badge bg="warning">
        <i className="fa-solid fa-heart-crack fa-beat" /> ABORTING
      </Badge>
    );
  }

  if (isRunning) {
    return (
      <Badge bg="info">
        <i className="fa-solid fa-heart fa-beat" /> RUNNING
      </Badge>
    );
  }

  if (awaitingUserInput) {
    return (
      <Badge bg="warning" title="A user input is expected">
        <i className="fa-solid fa-user fa-beat" /> AWAITING INPUT
      </Badge>
    );
  }

  return <FinishedState hardware={hardware} />;
}

function FinishedState(props: {
  hardware: HardwareTypes.EditableHardware<ProcedureSchema>;
}) {
  const { hardware } = props;
  const { previous_run_state: runState, previous_run_exception: runException } =
    hardware.properties;

  const variants: { [state: string]: [string, string | null] } = {
    NONE: ['secondary', null],
    ABORTED: ['warning', 'fa-heart-crack'],
    FAILED: ['danger', null],
    SUCCESSED: ['success', null],
    UNKNOWN: ['danger', null],
  };

  const definition = variants[runState] || variants.UNKNOWN;
  const [variant, icon] = definition;

  return (
    <Badge bg={variant} title={runException ?? undefined}>
      {icon && (
        <>
          <i className={`fa-solid ${icon}`} />{' '}
        </>
      )}
      {runState}
    </Badge>
  );
}

export default function ProcedureExecution(
  props: HardwareTypes.HardwareWidgetProps<ProcedureSchema>
) {
  const { hardware, options = {} } = props;

  const widgetIcon = (
    <TypeIcon name="Procedure" icon="fa-cog" online={hardware.online} />
  );

  const widgetState = <ProcedureState hardware={hardware} />;

  const widgetContent = (
    <Container>
      <div>
        <h3>
          <State hardware={hardware} />
        </h3>
      </div>
    </Container>
  );

  const headerMode = options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
