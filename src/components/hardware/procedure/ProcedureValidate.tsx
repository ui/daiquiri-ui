import type { MouseEvent } from 'react';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import { Button, ButtonGroup, Container, Badge } from 'react-bootstrap';
import { ProcedureState } from './State';
import type { ProcedureSchema } from '.';

function Actions(props: {
  hardware: HardwareTypes.EditableHardware<ProcedureSchema>;
  disabled: boolean;
}) {
  const { hardware } = props;
  const disabled = props.disabled || !hardware.online;
  const waitingUserInput = hardware.properties.state === 'AWAITING_USER_INPUT';

  function doValidate(event: MouseEvent) {
    hardware.actions.call('validate');
  }

  return (
    <ButtonGroup size="lg" className="w-100">
      <Button
        onClick={doValidate}
        disabled={disabled || !waitingUserInput}
        variant="primary"
      >
        Validate
      </Button>
    </ButtonGroup>
  );
}

export default function ProcedureValidate(
  props: HardwareTypes.HardwareWidgetProps<ProcedureSchema>
) {
  const { hardware, options = {} } = props;

  const widgetIcon = (
    <TypeIcon name="Procedure" icon="fa-cog" online={hardware.online} />
  );

  const widgetState = <ProcedureState hardware={hardware} />;

  const widgetContent = (
    <Container>
      <Actions hardware={hardware} disabled={!props.operator} />
    </Container>
  );

  const headerMode = options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
