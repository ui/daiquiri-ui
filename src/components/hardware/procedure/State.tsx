import { HardwareState } from '@esrf/daiquiri-lib';
import type { ProcedureSchema } from '.';

export function ProcedureState(props: { hardware: ProcedureSchema }) {
  const { hardware } = props;
  const state = hardware.online ? hardware.properties.state : 'OFFLINE';
  const variants: { [state: string]: string } = {
    STANDBY: 'success',
    DISABLED: 'secondary',
    OFFLINE: 'secondary',
    RUNNING: 'warning',
    ABORTING: 'warning',
    AWAITING_USER_INPUT: 'warning',
    UNKNOWN: 'fatal',
  };
  const variant = variants[state] || variants.UNKNOWN;
  return (
    <HardwareState.HardwareState state={state} minWidth={6} variant={variant} />
  );
}
