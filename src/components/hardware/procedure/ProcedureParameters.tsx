import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import { Container } from 'react-bootstrap';
import { ProcedureState } from './State';
import type { ProcedureSchema } from '.';
import Table from 'components/table';

function Parameters(props: {
  hardware: HardwareTypes.EditableHardware<ProcedureSchema>;
}) {
  const { hardware } = props;
  const parameters = hardware.properties.parameters ?? {};
  const columns = [
    { dataField: 'name', text: 'Name' },
    { dataField: 'type', text: 'Type' },
    { dataField: 'value', text: 'Content' },
  ];
  function getType(v: any): string {
    const t = typeof v;
    if (t === 'object' && v !== null && '__type__' in v) {
      return v.__type__;
    }
    return t;
  }
  const data = Object.entries(parameters).map(([key, value]) => {
    return { name: key, type: getType(value), value: JSON.stringify(value) };
  });
  return (
    <Table
      keyField="name"
      data={data}
      columns={columns}
      noDataIndication="No parameters"
    />
  );
}

export default function ProcedureProcedures(
  props: HardwareTypes.HardwareWidgetProps<ProcedureSchema>
) {
  const { hardware, options = {} } = props;

  const widgetIcon = (
    <TypeIcon name="Procedure" icon="fa-cog" online={hardware.online} />
  );

  const widgetState = <ProcedureState hardware={hardware} />;

  const widgetContent = (
    <Container>
      <Parameters hardware={hardware} />
    </Container>
  );

  const headerMode = options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
