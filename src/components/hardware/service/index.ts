import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { HardwareVariant } from '@esrf/daiquiri-lib';
import ServiceDefault from './ServiceDefault';
import ServiceProtected from './ServiceProtected';

/**
 * Hardware as exposed by Redux
 */
export interface ServiceSchema extends HardwareTypes.Hardware {
  properties: {
    state:
      | 'STOPPED'
      | 'STARTING'
      | 'RUNNING'
      | 'BACKOFF'
      | 'STOPPING'
      | 'EXITED'
      | 'FATAL'
      | 'UNKNOWN';
  };
}

export default class Shutter extends HardwareVariant {
  public variants = {
    default: ServiceDefault,
    protected: ServiceProtected,
  };
}
