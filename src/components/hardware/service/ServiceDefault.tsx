import { Form, Button, InputGroup } from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import { ServiceState } from './State';
import type { ServiceSchema } from '.';
import ReadOnlyTextInput from 'components/common/ReadOnlyTextInput';

export default function ServiceDefault(
  props: HardwareTypes.HardwareWidgetProps<
    ServiceSchema,
    HardwareTypes.HardwareWidgetOptions
  >
) {
  const { hardware, options = {} } = props;
  const widgetIcon = (
    <TypeIcon name="Shutter" icon="fa-cog" online={hardware.online} />
  );

  const widgetState = <ServiceState hardware={hardware} />;

  function requestStart() {
    hardware.actions.call('start');
  }

  function requestRestart() {
    hardware.actions.call('restart');
  }

  function requestStop() {
    hardware.actions.call('stop');
  }

  function getState() {
    if (!hardware.properties) {
      return 'UNKNOWN';
    }
    return hardware.properties.state;
  }

  function capitalize(s: string) {
    return s.charAt(0).toUpperCase() + s.slice(1).toLowerCase();
  }

  const state = getState();
  const label = capitalize(state);
  const variants: { [name: string]: string } = {
    STOPPED: 'danger',
    STARTING: 'warning',
    RUNNING: 'success',
    BACKOFF: 'warning',
    STOPPING: 'warning',
    EXITED: 'secondary',
    FATAL: 'fatal',
    UNKNOWN: 'secondary',
    OFFLINE: 'secondary',
  };
  const variant = variants[state] || 'danger';

  function getStartAction() {
    const restartEnabled = state === 'RUNNING' || state === 'STARTING';
    return {
      icon: restartEnabled ? 'fa fa-refresh fa-wp' : 'fa fa-play fa-wp',
      variant: props.disabled ? '' : 'success',
      callback: state === 'RUNNING' ? requestRestart : requestStart,
      disabled: props.disabled,
      title: !props.operator
        ? 'You dont have the control to the session'
        : restartEnabled
        ? 'Restart the service'
        : 'Start the serStaticTextInputvice',
    };
  }

  function getStopAction() {
    const stopEnabled = state === 'RUNNING' || state === 'STARTING';
    return {
      icon: 'fa fa-stop fa-wp',
      variant: props.disabled ? '' : !stopEnabled ? 'secondary' : 'danger',
      callback: requestStop,
      disabled: props.disabled,
      title: !props.operator
        ? 'You dont have the control to the session'
        : 'Stop the service',
    };
  }

  const startAction = getStartAction();
  const stopAction = getStopAction();

  const widgetContent = (
    <InputGroup className="d-flex flex-nowrap">
      <ReadOnlyTextInput
        variant={variant}
        className="text-center"
        style={{ minWidth: '8em' }}
        value={label}
      />
      <Button
        style={{ width: '2.5em' }}
        variant="light"
        className={`px-0 text-${startAction.variant}`}
        onClick={startAction.callback}
        disabled={startAction.disabled}
        title={startAction.title}
      >
        <i className={`${startAction.icon}`} />
      </Button>
      <Button
        style={{ width: '2.5em' }}
        variant="light"
        className={`px-0 text-${stopAction.variant}`}
        onClick={stopAction.callback}
        disabled={stopAction.disabled}
        title={stopAction.title}
      >
        <i className={`${stopAction.icon}`} />
      </Button>
    </InputGroup>
  );

  const headerMode = props.options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
