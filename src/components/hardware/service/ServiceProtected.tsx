import {
  InputGroup,
  Button,
  Form,
  Dropdown,
  Container,
  Row,
  Col,
} from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import { ServiceState } from './State';
import type { ServiceSchema } from '.';
import ReadOnlyTextInput from 'components/common/ReadOnlyTextInput';

function DropdownService(
  props: HardwareTypes.HardwareWidgetProps<
    ServiceSchema,
    HardwareTypes.HardwareWidgetOptions
  >
) {
  const { hardware, options = {} } = props;
  function requestStart() {
    hardware.actions.call('start');
  }

  function requestRestart() {
    hardware.actions.call('restart');
  }

  function requestStop() {
    hardware.actions.call('stop');
  }

  const state = hardware.properties?.state ?? 'UNKNOWN';
  const hardwareDisabled = state === 'STOPPING' || state === 'STARTING';

  return (
    <Dropdown>
      <Dropdown.Toggle disabled={props.disabled || hardwareDisabled} />
      <Dropdown.Menu>
        <Container>
          <Row>
            <Col className="text-center">Actions</Col>
          </Row>
          <Row>
            <Col>
              {state === 'RUNNING' && (
                <Button
                  className="me-1"
                  variant={state !== 'RUNNING' ? 'primary' : 'secondary'}
                  onClick={() => {
                    requestRestart();
                  }}
                  disabled={props.disabled}
                  title="Restart the service"
                >
                  Restart
                </Button>
              )}
              {state !== 'RUNNING' && (
                <Button
                  className="me-1"
                  variant="primary"
                  onClick={() => {
                    requestStart();
                  }}
                  disabled={props.disabled}
                  title="Start the service"
                >
                  Start
                </Button>
              )}
              <Button
                variant={state !== 'STOPPED' ? 'primary' : 'secondary'}
                onClick={() => {
                  requestStop();
                }}
                disabled={props.disabled}
                title="Stop the service"
              >
                Stop
              </Button>
            </Col>
          </Row>
        </Container>
      </Dropdown.Menu>
    </Dropdown>
  );
}

export default function ServiceProtected(
  props: HardwareTypes.HardwareWidgetProps<
    ServiceSchema,
    HardwareTypes.HardwareWidgetOptions
  >
) {
  const { hardware, options = {} } = props;
  const widgetIcon = (
    <TypeIcon name="Shutter" icon="fa-cog" online={hardware.online} />
  );

  const widgetState = <ServiceState hardware={hardware} />;

  function getState() {
    if (!hardware.properties) {
      return 'UNKNOWN';
    }
    return hardware.properties.state;
  }

  function capitalize(s: string) {
    return s.charAt(0).toUpperCase() + s.slice(1).toLowerCase();
  }

  const state = getState();
  const hardwareDisabled = state === 'STOPPING' || state === 'STARTING';

  const variants: { [name: string]: string } = {
    STOPPED: 'danger',
    STARTING: 'warning',
    RUNNING: 'success',
    BACKOFF: 'warning',
    STOPPING: 'warning',
    EXITED: 'secondary',
    FATAL: 'fatal',
    UNKNOWN: 'fatal',
    OFFLINE: 'secondary',
  };
  const variant = variants[state];
  const valueName = capitalize(state);

  const widgetContent = (
    <InputGroup className="d-flex flex-nowrap">
      <ReadOnlyTextInput
        variant={variant}
        style={{ minWidth: '10em' }}
        value={valueName}
        className="text-center"
      />
      <DropdownService {...props} />
    </InputGroup>
  );

  const headerMode = options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
