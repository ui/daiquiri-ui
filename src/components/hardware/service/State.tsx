import { HardwareState } from '@esrf/daiquiri-lib';
import type { ServiceSchema } from '.';

export function ServiceState(props: { hardware: ServiceSchema }) {
  const { hardware } = props;
  const state = hardware.online ? hardware.properties.state : 'OFFLINE';
  const variants: { [state: string]: string } = {
    STOPPED: 'danger',
    STARTING: 'warning',
    RUNNING: 'success',
    BACKOFF: 'warning',
    STOPPING: 'warning',
    EXITED: 'secondary',
    FATAL: 'fatal',
    UNKNOWN: 'fatal',
    OFFLINE: 'secondary',
  };
  const variant = variants[state] || variants.UNKNOWN;
  return (
    <HardwareState.HardwareState state={state} minWidth={6} variant={variant} />
  );
}
