import { Badge, OverlayTrigger, Popover, Button } from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, Formatting } from '@esrf/daiquiri-lib';
import StandaloneSchemaField from 'components/form-widgets/StandaloneSchemaField';

import { cloneDeep } from 'lodash';
import { useCallback, useState } from 'react';

export interface SecondaryPropertiesConfig {
  /** The property id */
  id: string;
}

export interface PropertiesConfig extends SecondaryPropertiesConfig {
  /** Set the property as read only */
  readonly?: boolean;
}

export interface DefaultObjectOptions
  extends HardwareTypes.HardwareWidgetOptions {
  /** Override the width of the component */
  width?: number;
  /** Set the label/field colspacing, default is 5 */
  colWidth?: number;
  /** Override which properties to display */
  properties?: PropertiesConfig[];
  /** Properties to display in secondary popover */
  secondaryProperties?: SecondaryPropertiesConfig[];
  /** Secondary overlay placement */
  secondaryPlacement?: 'top' | 'left' | 'bottom' | 'right';
}

function parseError(error: any) {
  // Validation error, convert to RJSF compatible `extraErrors`
  if (error.response.status === 422) {
    if (error?.response.data?.messages) {
      const mappedErrors = {};
      Object.entries(error.response.data.messages).forEach(([key, value]) => {
        // @ts-expect-error
        mappedErrors[key] = { __errors: value };
      });
      return mappedErrors;
    }
  }
  return error?.response?.data?.error || error.message;
}

export default function DefaultObject(
  props: HardwareTypes.HardwareWidgetProps<
    HardwareTypes.Hardware,
    DefaultObjectOptions
  >
) {
  const { hardware, schema, options = {} } = props;
  const [hasError, setHasError] = useState<string>();
  const [isPending, setIsPending] = useState<boolean>(false);
  const [isSuccess, setIsSuccess] = useState<boolean>(false);

  function updateState() {
    setIsPending(false);
    setIsSuccess(true);
    setTimeout(() => {
      setIsSuccess(false);
    }, 1000);
  }

  function updateError(error: any) {
    setHasError(parseError(error));
    setIsPending(false);
  }

  const onChange = useCallback(
    (prop: string, value: any) => {
      setIsPending(true);
      hardware.actions
        .setProperty(prop, value)
        .then(() => updateState())
        .catch((error) => updateError(error));
    },
    [hardware]
  );

  const onKeyPress = useCallback(
    (prop: string, value: any) => {
      setIsPending(true);
      hardware.actions
        .setProperty(prop, value)
        .then(() => updateState())
        .catch((error) => updateError(error));
    },
    [hardware]
  );

  const style: { [name: string]: any } = {};
  if (options.width) style.width = options.width;

  const state = hardware.properties.state ?? 'UNKNOWN';
  const stateColor =
    hardware.properties.state_ok !== undefined
      ? hardware.properties.state_ok
        ? 'success'
        : 'danger'
      : 'info';

  const schemaProperties = cloneDeep(schema.properties);
  const hiddenFields = ['state', 'state_ok'];
  const available = Object.entries(hardware.properties).map(
    ([name, value]) => name
  );
  if (options.properties) {
    const visible = new Set(options.properties.map((property) => property.id));
    const hidden = available.filter((property) => !visible.has(property));
    hiddenFields.push(...hidden);

    options.properties.forEach((property) => {
      if (property.readonly) {
        schemaProperties.properties[property.id].readOnly = true;
      }
    });
  }

  const secondaryHiddenFields = [];
  if (options.secondaryProperties) {
    const secondaryVisible = new Set(
      options.secondaryProperties.map((property) => property.id)
    );
    hiddenFields.push(...secondaryVisible);
    const hidden = available.filter(
      (property) => !secondaryVisible.has(property)
    );
    secondaryHiddenFields.push(...hidden);
  }

  const SecondarPopover = (
    <Popover id="popover-basic" style={{ minWidth: 350 }}>
      <Popover.Header as="h3">Additional Parameters</Popover.Header>
      <Popover.Body className="p-0">
        <div className="hw-component m-1">
          <div className="hw-content p-1">
            <StandaloneSchemaField
              extraErrors={hasError}
              schema={schemaProperties}
              formData={hardware.properties}
              disabled={props.disabled}
              hiddenFields={secondaryHiddenFields}
              onFieldKeyPress={onKeyPress}
              onFieldChange={onChange}
              colWidth={options.colWidth || 6}
              onChange={() => setHasError(undefined)}
            />
          </div>
        </div>
      </Popover.Body>
    </Popover>
  );

  return (
    <div className="hw-component">
      <div className="hw-head">
        <TypeIcon
          name={Formatting.ucfirst(hardware.type)}
          icon="fam-hardware-any"
          online={hardware.online}
        />
        <div className="name">{hardware.name}</div>
        <Badge bg={stateColor}>{state}</Badge>
        <i
          className={`ms-1 fa ${isPending ? 'fa-spin fa-spinner' : ''} ${
            isSuccess ? 'fa-regular fa-circle-check text-success' : ''
          }`}
        />
        {options.secondaryProperties && (
          <OverlayTrigger
            placement={options.secondaryPlacement || 'right'}
            trigger="click"
            overlay={SecondarPopover}
          >
            <Button size="sm" className="ms-1">
              ...
            </Button>
          </OverlayTrigger>
        )}
      </div>
      <div className="hw-content" style={style}>
        {schemaProperties && (
          <StandaloneSchemaField
            schema={schemaProperties}
            formData={hardware.properties}
            extraErrors={hasError}
            disabled={props.disabled}
            hiddenFields={hiddenFields}
            onFieldKeyPress={onKeyPress}
            onFieldChange={onChange}
            colWidth={options.colWidth || 5}
            onChange={() => setHasError(undefined)}
          />
        )}
        {!schema.properties && <span>No schema available</span>}
      </div>
    </div>
  );
}
