import type { HardwareTypes } from '@esrf/daiquiri-lib';

export interface NameOptions extends HardwareTypes.HardwareWidgetOptions {
  overrideName?: string;
  emptyifnone?: boolean;
}

export default function Name(
  props: HardwareTypes.HardwareWidgetProps<HardwareTypes.Hardware, NameOptions>
) {
  const { hardware, options } = props;
  function getName() {
    if (hardware.name) {
      return hardware.name;
    }
    return hardware.id;
  }

  function getLabel() {
    if (options.overrideName !== undefined) {
      return options.overrideName;
    }
    if (hardware.alias) {
      return hardware.alias;
    }
    if (hardware.name) {
      return hardware.name;
    }
    return hardware.id;
  }

  function getTitle() {
    if (options.overrideName !== undefined) {
      return `Real name: ${getName()}`;
    }
    if (hardware.alias) {
      return `Real name: ${hardware.name}`;
    }
    return undefined;
  }

  const name = getLabel();
  const title = getTitle();
  return <span title={title}>{name}</span>;
}
