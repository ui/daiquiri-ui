import { HardwareVariant } from '@esrf/daiquiri-lib';
import Name from './Name';

export default class Default extends HardwareVariant {
  public variants = {
    default: Name,
  };
}
