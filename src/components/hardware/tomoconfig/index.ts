import type { Hardware, HardwareTypes } from '@esrf/daiquiri-lib';
import { HardwareVariant } from '@esrf/daiquiri-lib';
import TomoConfigLatencyTime from './LatencyTime';

/**
 * Hardware as exposed by Redux
 */
export interface TomoConfigSchema extends HardwareTypes.Hardware {
  properties: {
    sample_stage: string;
    sxbeam: string;
    detectors: string;
    holotomo: string;
    latency_time: number;
  };
}

export default class Default extends HardwareVariant {
  public variants = {
    latencytime: TomoConfigLatencyTime,
  };
}

export function isTomoConfigHardware(
  entity: Hardware
): entity is TomoConfigSchema {
  return entity.type === 'tomoconfig';
}
