import { Button, ButtonGroup } from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import { AirBearingState } from './State';
import type { AirBearingSchema } from '.';

function RawAirBearing(props: {
  hardware: HardwareTypes.EditableHardware<AirBearingSchema>;
  options: HardwareTypes.HardwareWidgetOptions;
  disabled: boolean;
}) {
  const { hardware, options, disabled } = props;
  const { state } = hardware.properties ?? 'OFFLINE';
  const isOn = state === 'ON';
  const isOff = state === 'OFF';

  return (
    <div
      style={{
        display: 'grid',
        gridTemplateRows: 'min-content min-content',
        gridTemplateColumns: 'min-content auto',
        gridGap: '1px',
      }}
    >
      <ButtonGroup className="mx-auto" style={{ width: '12em' }}>
        <Button
          onClick={() => {
            hardware.actions.call('on');
          }}
          disabled={!isOn || disabled}
          style={{ width: '5em' }}
        >
          <span className="text-nowrap">On</span>
        </Button>
        <Button
          onClick={() => {
            hardware.actions.call('off');
          }}
          disabled={!isOff || disabled}
          style={{ width: '5em' }}
        >
          <span className="text-nowrap">Off</span>
        </Button>
      </ButtonGroup>
    </div>
  );
}

export default function AirBearing(
  props: HardwareTypes.HardwareWidgetProps<
    AirBearingSchema,
    HardwareTypes.HardwareWidgetOptions
  >
) {
  const { hardware, options = {} } = props;
  const widgetIcon = (
    <TypeIcon
      name="Air bearing"
      icon="fa-hand-point-right"
      online={hardware.online}
    />
  );

  const widgetState = <AirBearingState hardware={hardware} />;

  function getWidgetContent() {
    return <RawAirBearing {...props} />;
  }

  const headerMode = options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={getWidgetContent()}
      headerMode={headerMode}
    />
  );
}
