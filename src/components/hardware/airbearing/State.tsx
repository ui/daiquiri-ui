import { HardwareState } from '@esrf/daiquiri-lib';
import type { AirBearingSchema } from '.';

export function AirBearingState(props: { hardware: AirBearingSchema }) {
  const { hardware } = props;
  const state = hardware.online ? hardware.properties.state : 'OFFLINE';
  const variants: { [state: string]: string } = {
    ON: 'danger',
    OFF: 'success',
    MOVING_ON: 'warning',
    MOVING_OFF: 'warning',
    UNKNOWN: 'fatal',
    OFFLINE: 'warning',
  };
  const variant = variants[state] || variants.UNKNOWN;
  return (
    <HardwareState.HardwareState state={state} minWidth={6} variant={variant} />
  );
}
