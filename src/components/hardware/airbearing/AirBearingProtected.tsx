import {
  Button,
  ButtonGroup,
  Col,
  Container,
  Dropdown,
  InputGroup,
  Row,
} from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import { AirBearingState } from './State';
import type { AirBearingSchema } from '.';
import ReadOnlyTextInput from '../../common/ReadOnlyTextInput';
import type { PropsWithChildren } from 'react';

function ActionButton(
  props: PropsWithChildren<{
    icon: string;
    isActive?: boolean;
    wasActivated: boolean;
    disabled: boolean;
    onClick: () => void;
    title: string;
  }>
) {
  return (
    <Button
      variant={
        props.isActive
          ? 'primary'
          : props.wasActivated
          ? 'warning'
          : 'secondary'
      }
      onClick={props.onClick}
      disabled={props.disabled || props.wasActivated}
      title={props.title}
    >
      <Row className="gx-0">
        <i
          className={`col-sm-4 justify-content-center align-self-center ${props.icon}`}
        />
        <Col sm="8">{props.children}</Col>
      </Row>
    </Button>
  );
}

function DropdownAirBearing(props: {
  hardware: HardwareTypes.EditableHardware<AirBearingSchema>;
  options: HardwareTypes.HardwareWidgetOptions;
  disabled: boolean;
}) {
  const { hardware, options = {}, disabled } = props;
  function requestOn() {
    hardware.actions.call('on');
  }

  function requestOff() {
    hardware.actions.call('off');
  }

  const state = hardware.properties?.state ?? 'UNKNOWN';
  const hardwareDisabled = state === 'MOVING_ON' || state === 'MOVING_OFF';
  const isOn = state === 'ON';
  const isOff = state === 'OFF';
  const movingOn = state === 'MOVING_ON';
  const movingOff = state === 'MOVING_OFF';

  return (
    <Dropdown>
      <Dropdown.Toggle
        variant="secondary"
        disabled={disabled || hardwareDisabled}
      />
      <Dropdown.Menu>
        <Container>
          <Row>
            <Col className="text-center">Actions</Col>
          </Row>
          <Row>
            <Col>
              <ButtonGroup>
                <ActionButton
                  icon="fa fa-arrow-up"
                  wasActivated={movingOn}
                  onClick={() => {
                    requestOn();
                  }}
                  disabled={disabled || hardwareDisabled}
                  title="Turn on the air bearing"
                >
                  Turn
                  <br />
                  on
                </ActionButton>
                <ActionButton
                  icon="fa fa-arrow-down"
                  wasActivated={movingOff}
                  onClick={() => {
                    requestOff();
                  }}
                  disabled={disabled || hardwareDisabled}
                  title="Turn off the air bearing"
                >
                  Turn
                  <br />
                  off
                </ActionButton>
              </ButtonGroup>
            </Col>
          </Row>
        </Container>
      </Dropdown.Menu>
    </Dropdown>
  );
}

function RawAirBearing(props: {
  hardware: HardwareTypes.EditableHardware<AirBearingSchema>;
  options: HardwareTypes.HardwareWidgetOptions;
  disabled: boolean;
}) {
  const { hardware, options, disabled } = props;
  const { state } = hardware.properties ?? 'OFFLINE';

  const variants: { [state: string]: string } = {
    ON: 'danger',
    OFF: 'success',
    MOVING_ON: 'warning',
    MOVING_OFF: 'warning',
    UNKNOWN: 'fatal',
    OFFLINE: 'warning',
  };
  const variant = variants[state] ?? 'fatal';

  return (
    <InputGroup className="d-flex flex-nowrap">
      <ReadOnlyTextInput
        variant={variant}
        className="text-center"
        style={{ minWidth: '10em' }}
        value={state}
      />
      <DropdownAirBearing {...props} />
    </InputGroup>
  );
}

export default function AirBearingProtected(
  props: HardwareTypes.HardwareWidgetProps<
    AirBearingSchema,
    HardwareTypes.HardwareWidgetOptions
  >
) {
  const { hardware, options = {} } = props;
  const widgetIcon = (
    <TypeIcon
      name="Air bearing"
      icon="fa-hand-point-right"
      online={hardware.online}
    />
  );

  const widgetState = <AirBearingState hardware={hardware} />;

  function getWidgetContent() {
    return <RawAirBearing {...props} />;
  }

  const headerMode = options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={getWidgetContent()}
      headerMode={headerMode}
    />
  );
}
