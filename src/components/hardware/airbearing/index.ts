import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { HardwareVariant } from '@esrf/daiquiri-lib';
import AirBearing from './AirBearing';
import AirBearingProtected from './AirBearingProtected';

/**
 * Hardware as exposed by Redux
 */
export interface AirBearingSchema extends HardwareTypes.Hardware {
  properties: {
    state: string;
  };
}

export default class Default extends HardwareVariant {
  public variants = {
    default: AirBearing,
    protected: AirBearingProtected,
  };
}
