import { Fragment } from 'react';
import { map } from 'lodash';

import { Col, Row, Container } from 'react-bootstrap';
import HardwareObject from 'connect/hardware/HardwareObject';

export interface Props {
  /** Wrap each object in a `<Col>` so it fills the parent width and each object will be evenly spaced */
  even?: boolean;
  /** Disable wrapping of objects */
  nowrap?: boolean;
  objects: { [name: string]: any }[];
}

export default function HardwareGroup(props: Props) {
  const objs = map(props.objects, (obj) => {
    const { id, even = false, ...options } = obj;
    const wrapArgs: { [name: string]: any } = { key: `col${id}` };
    if (!even) {
      wrapArgs.lg = 'auto';
    }

    return (
      <Col {...wrapArgs} className="gx-0" key={`col${id}`}>
        <HardwareObject key={`chw${id}`} id={id} options={options} />
      </Col>
    );
  });

  const noWrap = props.nowrap ? 'flex-nowrap' : '';

  return (
    <Container fluid className="hw-group">
      <Row className={noWrap}>{objs}</Row>
    </Container>
  );
}
