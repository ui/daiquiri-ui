import { useEffect } from 'react';
import HardwareGroup from 'connect/hardware/HardwareGroup';

export interface HardwareDesc {
  id: string;
}

export type HardwareListDesc = HardwareDesc[];

interface Props {
  fetched: boolean;
  fetchedGroups: boolean;
  ids?: HardwareListDesc;
  groupid?: string;
  actions?: {
    fetch: () => void;
    fetchGroups: () => void;
  };
  even?: boolean;
  nowrap: boolean;
}

export default function YmlHardwareGroup(props: Props) {
  useEffect(() => {
    if (props.actions) {
      props.actions.fetch();
      props.actions.fetchGroups();
    }
  }, []);

  if (!props.fetched || !props.fetchedGroups) {
    return <span>Fetching</span>;
  }

  const even = props.groupid && props.even === undefined ? true : props.even;
  return (
    <HardwareGroup
      even={even}
      nowrap={props.nowrap}
      groupid={props.groupid}
      objects={props.ids}
    />
  );
}
