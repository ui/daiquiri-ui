import type { Hardware, HardwareTypes } from '@esrf/daiquiri-lib';
import { HardwareVariant } from '@esrf/daiquiri-lib';
import TomoDetectors from './TomoDetectors';

/**
 * Hardware as exposed by Redux
 */
export interface TomoDetectorsSchema extends HardwareTypes.Hardware {
  properties: {
    state: string;
    detectors: string[];
    active_detector: string;
    target_detector?: string; // FIXME: The server implementation is missing
  };
}

export default class Default extends HardwareVariant {
  public variants = {
    default: TomoDetectors,
  };
}

export function isTomoDetectorsHardware(
  entity: Hardware
): entity is TomoDetectorsSchema {
  return entity.type === 'tomodetectors';
}
