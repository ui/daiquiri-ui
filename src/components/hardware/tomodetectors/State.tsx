import { HardwareState } from '@esrf/daiquiri-lib';
import type { TomoDetectorsSchema } from '.';

export function TomoDetectorsState(props: { hardware: TomoDetectorsSchema }) {
  const { hardware } = props;
  const state = hardware.online ? hardware.properties.state : 'OFFLINE';
  const variants: { [state: string]: string } = {
    READY: 'success',
    MOUNTING: 'warning',
    UNMOUNTING: 'warning',
    UNKNOWN: 'fatal',
    OFFLINE: 'warning',
  };
  const variant = variants[state] || variants.UNKNOWN;
  return (
    <HardwareState.HardwareState state={state} minWidth={6} variant={variant} />
  );
}
