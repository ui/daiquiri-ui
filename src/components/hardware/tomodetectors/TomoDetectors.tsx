import classNames from 'classnames';
import { Dropdown, InputGroup, Form } from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import { TomoDetectorsState } from './State';
import type { TomoDetectorsSchema } from '.';
import ReadOnlyTextInput from 'components/common/ReadOnlyTextInput';

function getHardwareIdFromRef(ref: string | undefined) {
  if (!ref) {
    return null;
  }
  if (!ref.startsWith('hardware:')) {
    // FIXME: Handle user error, the content is not of hardware kind
    return null;
  }
  return ref.slice(9);
}

function DropdownDetectors(
  props: HardwareTypes.HardwareWidgetProps<TomoDetectorsSchema>
) {
  const { hardware } = props;
  const { properties } = hardware;
  const { detectors } = properties;
  const activeDetectorId = getHardwareIdFromRef(properties.active_detector);
  const targetDetectorId = getHardwareIdFromRef(properties.target_detector);
  const hardwareReady = properties.state === 'READY';
  const disabled = props.disabled || !hardware.online || !hardwareReady;

  function mountDetector(detectorId: string | null) {
    if (hardware.actions) {
      hardware.actions.call('mount', detectorId);
    } else {
      console.error('Change request was triggered with:', detectorId);
    }
  }

  function createSelectableItem(detectorRef: string) {
    const detectorId = getHardwareIdFromRef(detectorRef);
    const selected = detectorId === (targetDetectorId || activeDetectorId);
    const icon = selected ? 'fa fa-fw fa-dot-circle-o' : 'fa fa-fw fa-circle-o';

    return (
      <Dropdown.Item
        key={detectorRef}
        disabled={disabled}
        onClick={() => {
          mountDetector(detectorId);
        }}
      >
        <i className={icon} />
        &nbsp; {detectorId}
      </Dropdown.Item>
    );
  }

  return (
    <Dropdown>
      <Dropdown.Toggle disabled={disabled} variant="secondary" />
      <Dropdown.Menu>
        {detectors.map((m) => createSelectableItem(m))}
      </Dropdown.Menu>
    </Dropdown>
  );
}

export default function TomoDetectors(
  props: HardwareTypes.HardwareWidgetProps<TomoDetectorsSchema>
) {
  const { hardware, options = {} } = props;
  const widgetIcon = (
    <TypeIcon
      name="Optic"
      icon="fam-hardware-camera"
      online={hardware.online}
    />
  );

  const widgetState = <TomoDetectorsState hardware={hardware} />;

  const { properties } = hardware;
  const hardwareMounting = properties.state === 'MOUNTING';
  const activeDetectorId = getHardwareIdFromRef(properties.active_detector);
  const targetDetectorId = getHardwareIdFromRef(properties.target_detector);

  function getWidgetContent() {
    return (
      <InputGroup>
        <ReadOnlyTextInput
          className={classNames({
            'hw-moving': hardwareMounting,
          })}
          value={targetDetectorId || activeDetectorId || 'No detector'}
        />
        <DropdownDetectors {...props} />
      </InputGroup>
    );
  }

  const headerMode = options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={getWidgetContent()}
      headerMode={headerMode}
    />
  );
}
