import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { HardwareVariant } from '@esrf/daiquiri-lib';
import Valve from './Valve';

/**
 * Hardware as exposed by Redux
 */
export interface ValveSchema extends HardwareTypes.Hardware {
  properties: {
    state: string;
    status: string;
  };
}

export default class Default extends HardwareVariant {
  public variants = {
    default: Valve,
  };
}
