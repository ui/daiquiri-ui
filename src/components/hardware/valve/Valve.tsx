import {
  Badge,
  Button,
  ButtonGroup,
  OverlayTrigger,
  Popover,
} from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon } from '@esrf/daiquiri-lib';
import type { ValveSchema } from '.';

function ValveOverlay(props: HardwareTypes.HardwareWidgetProps<ValveSchema>) {
  const { hardware } = props;
  const reset = () => {
    hardware.actions.call('reset');
  };

  return (
    <OverlayTrigger
      trigger="click"
      rootClose
      overlay={
        <Popover id="popover">
          <Popover.Header>{hardware.name} Details</Popover.Header>
          <Popover.Body>
            <h6>Status</h6>
            <pre>{hardware.properties.status}</pre>
            <div className="d-grid gap-2">
              <Button disabled={props.disabled} onClick={reset}>
                Reset
              </Button>
            </div>
          </Popover.Body>
        </Popover>
      }
    >
      <Button className="flex-grow-0" title="Valve Details">
        <i className="fa fa-ellipsis-h" />
      </Button>
    </OverlayTrigger>
  );
}

function Valve(props: HardwareTypes.HardwareWidgetProps<ValveSchema>) {
  const { hardware, options = {} } = props;
  const onClick = () => {
    hardware.actions.call(
      hardware.properties.state === 'OPEN' ? 'close' : 'open'
    );
  };

  return (
    <div className="hw-component">
      <div className="hw-head">
        <TypeIcon
          name="Valve"
          icon="fam-hardware-valve"
          online={hardware.online}
        />
        <div className="name">{hardware.name}</div>
        <Badge
          bg={
            hardware.properties.state === 'OPEN' ||
            hardware.properties.state === 'RUNNING'
              ? 'success'
              : 'danger'
          }
        >
          {hardware.properties.state}
        </Badge>
      </div>
      <div className="hw-content">
        <ButtonGroup className="d-flex">
          <Button
            variant={
              hardware.properties.state === 'OPEN' ? 'danger' : 'success'
            }
            onClick={onClick}
            disabled={props.disabled}
          >
            {hardware.properties.state === 'OPEN' ? 'Close' : 'Open'}
          </Button>
          <ValveOverlay {...props} />
        </ButtonGroup>
      </div>
    </div>
  );
}

export default Valve;
