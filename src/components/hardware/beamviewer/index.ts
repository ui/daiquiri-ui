import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { HardwareVariant } from '@esrf/daiquiri-lib';
import Beamviewer from './Beamviewer';

/**
 * Hardware as exposed by Redux
 */
export interface BeamviewerSchema extends HardwareTypes.Hardware {
  properties: {
    state: string;
    foil: string;
    led: string;
    screen: string;
    diode_ranges: string[];
    diode_range: string;
  };
}

export default class Default extends HardwareVariant {
  public variants = {
    default: Beamviewer,
  };
}
