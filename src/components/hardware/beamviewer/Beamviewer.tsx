import { Fragment, useState } from 'react';
import type { ChangeEvent } from 'react';
import { map } from 'lodash';

import {
  Container,
  Row,
  Col,
  Badge,
  Button,
  OverlayTrigger,
  Popover,
  Form,
} from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon } from '@esrf/daiquiri-lib';
import type { BeamviewerSchema } from '.';
import { useToast } from '../../utils/appHooks';

function BeamviewerOverlay(
  props: HardwareTypes.HardwareWidgetProps<BeamviewerSchema>
) {
  const addToast = useToast();
  const [current, setCurrent] = useState();
  const { hardware, options = {} } = props;

  function read(e: any) {
    const promise = hardware.actions.call('read');
    promise
      .then((resp: any) => {
        setCurrent(resp);
      })
      .catch((error: any) => {
        addToast({
          type: 'error',
          title: 'Could not read current',
          text: error.message,
        });
      });
  }

  function onChangeRange(value: any) {
    return hardware.actions.setProperty('diode_range', value);
  }

  return (
    <OverlayTrigger
      trigger="click"
      rootClose
      overlay={
        <Popover id="popid">
          <Popover.Header>{hardware.name} Details</Popover.Header>
          <Popover.Body>
            <Container>
              <Row>
                <Col sm={6} className="text-nowrap">
                  Diode Range:
                </Col>
                <Col>
                  <Form.Control
                    size="sm"
                    as="select"
                    disabled={props.disabled}
                    onChange={(e) => {
                      onChangeRange(e.target.value);
                    }}
                    defaultValue={hardware.properties.diode_range}
                  >
                    {map(hardware.properties.diode_ranges, (r) => (
                      <option value={r} key={r}>
                        {r}
                      </option>
                    ))}
                  </Form.Control>
                </Col>
              </Row>
              <Row>
                <Col sm={6}>Current:</Col>
                <Col>
                  {current}
                  <Button size="sm" disabled={props.disabled} onClick={read}>
                    Read
                  </Button>
                </Col>
              </Row>
            </Container>
          </Popover.Body>
        </Popover>
      }
    >
      <Button title="Beamviewer Details" className="float-end" size="sm">
        <i className="fa fa-ellipsis-h" />
      </Button>
    </OverlayTrigger>
  );
}

function Beamviewer(
  props: HardwareTypes.HardwareWidgetProps<BeamviewerSchema>
) {
  const { hardware, options = {} } = props;
  interface Property {
    name: string;
    prop: 'screen' | 'led' | 'foil';
    success: string;
    toggle: boolean;
    button: string;
    test?: boolean;
  }

  const properties: Property[] = [
    {
      name: 'Screen',
      prop: 'screen',
      success: 'OUT',
      toggle: hardware.properties.screen === 'IN',
      button: hardware.properties.screen === 'IN' ? 'Out' : 'In',
    },
    {
      name: 'Led',
      prop: 'led',
      success: 'OFF',
      toggle: hardware.properties.led === 'ON',
      button: hardware.properties.led === 'ON' ? 'Off' : 'On',
    },
    {
      name: 'Foil',
      prop: 'foil',
      success: 'OUT',
      toggle: hardware.properties.foil === 'IN',
      button: hardware.properties.foil === 'IN' ? 'Out' : 'In',
      test: hardware.properties.foil !== 'NONE',
    },
  ];

  function toggle(prop: string, value: any) {
    hardware.actions.call(prop, value);
  }

  return (
    <div className="hw-component">
      <div className="hw-head">
        <TypeIcon
          name="Beamviewer"
          icon="fam-hardware-beamviewer"
          online={hardware.online}
        />
        <div className="name me-auto">{hardware.name}</div>
        <BeamviewerOverlay {...props} />
      </div>
      <div className="hw-content">
        <Container style={{ minWidth: '150px' }}>
          {map(properties, (p) => (
            <Fragment key={p.name}>
              {(p.test === true || p.test === undefined) && (
                <Row className="align-items-center mb-1">
                  <Col sm={4}>{p.name}</Col>
                  <Col sm={4}>
                    <Badge
                      className="ms-1"
                      bg={
                        hardware.properties[p.prop] === p.success
                          ? 'success'
                          : 'danger'
                      }
                    >
                      {hardware.properties[p.prop]}
                    </Badge>
                  </Col>
                  <Col sm={4}>
                    <Button
                      size="sm"
                      className="ms-1"
                      disabled={props.disabled}
                      onClick={(e) => toggle(p.prop, !p.toggle)}
                    >
                      {p.button}
                    </Button>
                  </Col>
                </Row>
              )}
            </Fragment>
          ))}
        </Container>
      </div>
    </div>
  );
}

export default Beamviewer;
