import { forwardRef } from 'react';
import type { ReactNode } from 'react';
import {
  Badge,
  Form,
  InputGroup,
  OverlayTrigger,
  Tooltip,
  Row,
  Col,
  Popover,
} from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import {
  TypeIcon,
  HardwareTemplate,
  HardwareInputNumber,
} from '@esrf/daiquiri-lib';
import type { TangoAttrSchema } from '.';

export interface TangoAttrOptions extends HardwareTypes.HardwareWidgetOptions {
  /** If true, display a header with general information */
  displayState?: boolean;
  /** If true, display the quality of the attribute */
  displayQuality?: boolean;
  /** Mapping from state to themes `success`, `info`, `warning`, `danger`
   * in order to setup the highlighed color according to the state.
   */
  stateVariants?: Record<string, string>;
  /** Display the attribute as read only */
  readonly?: boolean;
}

/**
 * Bagde containing the state of the device
 */
function StateBadge(
  props: HardwareTypes.HardwareWidgetProps<TangoAttrSchema, TangoAttrOptions>
) {
  const stateVariants: Record<string, string> = {
    ON: 'success',
    OFF: 'warning',
    CLOSE: 'success',
    OPEN: 'success',
    INSERT: 'success',
    EXTRACT: 'success',
    MOVING: 'success',
    STANDBY: 'success',
    FAULT: 'danger',
    INIT: 'success',
    RUNNING: 'success',
    ALARM: 'success',
    DISABLE: 'warning',
    UNKNOWN: 'warning',
    OFFLINE: 'danger', // Extra state
  };
  const { hardware } = props;
  const { online } = hardware;
  const state = !online ? 'OFFLINE' : hardware.properties.state;
  const status = !online ? undefined : hardware.properties.status;
  const variant = props.options.stateVariants?.[state] || stateVariants[state];
  return (
    <OverlayTrigger
      key="top"
      placement="top"
      overlay={
        <Tooltip id="state">
          The state of the device is {state.toLowerCase()}
          {status && (
            <div>
              <code>{status}</code>
            </div>
          )}
        </Tooltip>
      }
    >
      <Badge bg={variant}>{state}</Badge>
    </OverlayTrigger>
  );
}

/**
 * Bagde containing the quality of the attribute
 */
function QualityBadge(
  props: HardwareTypes.HardwareWidgetProps<TangoAttrSchema, TangoAttrOptions>
) {
  const { hardware } = props;
  const qualityVariants: Record<string, string> = {
    VALID: 'success',
    WARNING: 'warning',
    ALARM: 'danger',
  };
  const { quality } = hardware.properties;
  const variant = qualityVariants[quality] || 'info';
  return (
    <OverlayTrigger
      key="top"
      placement="top"
      overlay={
        <Tooltip id="quality">
          The quality of the attribute is {quality.toLowerCase()}
        </Tooltip>
      }
    >
      <Badge bg={variant}>{quality}</Badge>
    </OverlayTrigger>
  );
}

const TangoAttrPopOver = forwardRef<
  HTMLDivElement,
  HardwareTypes.HardwareWidgetProps<TangoAttrSchema, TangoAttrOptions>
>((props, ref) => {
  function row(key: string, title: string, data: any, component?: ReactNode) {
    if (!data) {
      return null;
    }
    return (
      <Row key={key}>
        <Col className="text-nowrap">{title}</Col>
        <Col>{component || data}</Col>
      </Row>
    );
  }

  const { hardware } = props;
  return (
    <div>
      <Popover ref={ref} {...props}>
        <Popover.Header>{hardware.name} details</Popover.Header>
        <Popover.Body>
          {row('value', 'Value', hardware.properties.value)}
          {row('unit', 'Unit', hardware.properties.display_unit)}
          {row(
            'state',
            'Device state',
            hardware.properties.state,
            <StateBadge {...props} />
          )}
          {row(
            'status',
            'Device status',
            hardware.properties.status,
            <code>{hardware.properties.status}</code>
          )}
          {row(
            'quality',
            'Value quality',
            hardware.properties.quality,
            <QualityBadge {...props} />
          )}
          {row('label', 'Tango label', hardware.properties.label)}
          {row('desc', 'Tango description', hardware.properties.description)}
          {row('name', 'Tango name', hardware.properties.name)}
        </Popover.Body>
      </Popover>
    </div>
  );
});

type MetaTypes =
  | 'void'
  | 'boolean'
  | 'int'
  | 'float'
  | 'string'
  | 'array'
  | 'other';

const TangoTypes: Record<string, MetaTypes> = {
  DevVoid: 'void',
  DevBoolean: 'boolean',
  DevShort: 'int',
  DevLong: 'int',
  DevLong64: 'int',
  DevUChar: 'int',
  DevUShort: 'int',
  DevULong: 'int',
  DevULong64: 'int',
  DevFloat: 'float',
  DevDouble: 'float',
  DevString: 'string',
  DevVarBooleanArray: 'array',
  DevVarDoubleArray: 'array',
  DevVarFloatArray: 'array',
  DevVarShortArray: 'array',
  DevVarLongArray: 'array',
  DevVarLong64Array: 'array',
  DevVarCharArray: 'array',
  DevVarStringArray: 'array',
  DevVarUShortArray: 'array',
  DevVarULongArray: 'array',
  DevVarULong64Array: 'array',
  DevVarEncodedArray: 'array',
  DevVarLongStringArray: 'array',
  DevVarDoubleStringArray: 'array',
  DevVarStateArray: 'array',
  DevEncoded: 'other',
  DevState: 'other',
  DevEnum: 'other',
  DevPipeBlob: 'other',
  DevFailed: 'other',
};

const numberFormatRegExp = /^\d*(.(\d+))?[fg]?$/; // eslint-disable-line unicorn/no-unsafe-regex

function TangoAttrInput(
  props: HardwareTypes.HardwareWidgetProps<TangoAttrSchema, TangoAttrOptions>
) {
  const { hardware } = props;
  const data_type = hardware.properties.data_type;
  const metatype = TangoTypes[data_type] ?? 'other';
  function formatValue(hardware: TangoAttrSchema) {
    const { value, format } = hardware.properties;
    if (metatype === 'string') {
      return value;
    }
    if (metatype === 'float') {
      const digits = Number.parseFloat(format.split(/\./)[1]);
      return digits !== undefined
        ? Number.parseFloat(value).toFixed(digits)
        : value;
    }
    if (metatype === 'array') {
      return `${data_type}[...]`;
    }
    if (data_type === 'DevEncoded' || data_type === 'DevPipeBlob') {
      return `${data_type}[...]`;
    }
    return `${value}`;
  }
  const text = formatValue(hardware);

  const isNumber = metatype === 'int' || metatype === 'float';
  const readOnly = props.options?.readonly ?? false;

  if (!isNumber) {
    // TODO: Text edition not yet supported
    return (
      <Form.Control
        type="text"
        value={text}
        disabled={props.disabled}
        readOnly
      />
    );
  }

  function readNumberFormat(hardware: TangoAttrSchema): [number, number] {
    if (metatype === 'int') {
      return [0, 1];
    }
    const { format } = hardware.properties;
    if (!format) {
      return [2, 0.2];
    }
    const result = numberFormatRegExp.exec(format);
    if (!result || result.length !== 3) {
      return [2, 0.2];
    }
    const digits = Number.parseInt(result[2]);
    return [digits, 10 ** -digits];
  }

  const [precision, step] = readNumberFormat(hardware);

  return (
    <HardwareInputNumber
      hardwareValue={hardware.properties.value}
      hardwareIsDisabled={props.disabled}
      hardwareIsMoving={false}
      onMoveRequested={(value) => {
        return props.hardware.actions.setProperty('value', value);
      }}
      readOnly={readOnly}
      precision={precision}
      step={step}
    />
  );
}

export default function TangoAttr(
  props: HardwareTypes.HardwareWidgetProps<TangoAttrSchema, TangoAttrOptions>
) {
  const { hardware } = props;

  const widgetIcon = (
    <TypeIcon name="TangoAttr" icon="fa-cog" online={hardware.online} />
  );

  const widgetState = (
    <>
      {props.options.displayState !== false && <StateBadge {...props} />}
      {hardware.online && props.options.displayQuality && (
        <QualityBadge {...props} />
      )}
    </>
  );

  const headerMode = props.options ? props.options.header : 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={
        <InputGroup>
          <OverlayTrigger
            placement="top"
            delay={{ show: 250, hide: 0 }}
            overlay={<TangoAttrPopOver {...props} />}
          >
            <TangoAttrInput {...props} />
          </OverlayTrigger>
          {hardware.properties.display_unit && (
            <InputGroup.Text>
              {hardware.properties.display_unit}
            </InputGroup.Text>
          )}
        </InputGroup>
      }
      headerMode={headerMode}
    />
  );
}
