import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { HardwareVariant } from '@esrf/daiquiri-lib';
import TangoAttr from './TangoAttr';

/**
 * Hardware as exposed by Redux
 */
export interface TangoAttrSchema extends HardwareTypes.Hardware {
  properties: {
    state: string;
    status: string;
    name: string;
    value: any;
    quality: string;
    data_type: string;
    description: string;
    label: string;
    unit: string;
    display_unit: string;
    format: string;
  };
}

export default class Default extends HardwareVariant {
  public variants = {
    default: TangoAttr,
  };
}
