import { HardwareVariant } from '@esrf/daiquiri-lib';
import type { Hardware, HardwareTypes } from '@esrf/daiquiri-lib';
import TomoReferencePositionDefault from './TomoReferencePositionDefault';

/**
 * Hardware as exposed by Redux
 */
export interface TomoReferencePositionSchema extends HardwareTypes.Hardware {
  properties: Record<string, never>;
}

export function isTomoReferencePositionHardware(
  entity: Hardware
): entity is TomoReferencePositionSchema {
  return entity.type === 'tomoreferenceposition';
}

export default class Default extends HardwareVariant {
  public variants = {
    default: TomoReferencePositionDefault,
  };
}
