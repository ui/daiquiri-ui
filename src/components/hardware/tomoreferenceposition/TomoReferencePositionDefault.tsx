import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import { Container, Button } from 'react-bootstrap';
import type { TomoReferencePositionSchema } from '.';

export default function TomoReferencePositionDefault(
  props: HardwareTypes.HardwareWidgetProps<TomoReferencePositionSchema>
) {
  const { hardware, options = {}, disabled } = props;
  const { actions } = hardware;

  const widgetIcon = (
    <TypeIcon
      name="TomoReferencePositionSchema"
      icon="fam-hardware-motor"
      online={hardware.online}
    />
  );

  function moveToReference() {
    actions.call('move_to_reference');
  }

  const widgetState = <></>;

  const widgetContent = (
    <Button
      className="w-100"
      onClick={() => {
        moveToReference();
      }}
      disabled={disabled}
    >
      Back to parking position
    </Button>
  );

  const headerMode = options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
