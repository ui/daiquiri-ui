import { TypeIcon, HardwareTemplate, MotorState } from '@esrf/daiquiri-lib';
import type {
  HardwareTypes,
  MotorSchema,
  MotorDefaultOptions,
} from '@esrf/daiquiri-lib';
import classNames from 'classnames';
import { Stack } from 'react-bootstrap';
import { useMotorStates } from '@esrf/daiquiri-lib/src/hardware/motor';

function Content(
  props: HardwareTypes.HardwareWidgetProps<MotorSchema, MotorDefaultOptions>
) {
  const { hardware, options = {} } = props;
  const states = useMotorStates(hardware.properties.state, hardware.user_tags);

  function normalizeNumber(value: number | null): string {
    if (value === null) {
      return '∅';
    }
    const digits =
      options.precision ?? hardware.properties.display_digits ?? undefined;
    if (digits !== undefined) {
      return value.toFixed(digits);
    }
    return `${value}`;
  }

  return (
    <Stack className="bg-light rounded-2 ps-2 p-1" direction="horizontal">
      <div
        className={classNames({
          'hw-moving': states.MOVING,
        })}
      >
        {normalizeNumber(hardware.properties.position)}
      </div>
      {hardware.properties.unit && (
        <div className="ms-auto">
          <small>
            <small>{hardware.properties.unit}</small>
          </small>
        </div>
      )}
    </Stack>
  );
}

/**
 * The default motor widget
 */
export default function MotorText(
  props: HardwareTypes.HardwareWidgetProps<MotorSchema, MotorDefaultOptions>
) {
  const { hardware } = props;

  const widgetIcon = (
    <TypeIcon name="Motor" icon="fam-hardware-motor" online={hardware.online} />
  );

  const widgetState = <MotorState hardware={hardware} />;

  const headerMode = props.options ? props.options.header : 'top';
  if (headerMode === 'none') {
    return <Content {...props} />;
  }

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={<Content {...props} />}
      headerMode={headerMode}
    />
  );
}
