import type { Hardware } from '@esrf/daiquiri-lib';
import { MotorDefault, HardwareVariant } from '@esrf/daiquiri-lib';

import MotorSmall from './MotorSmall';
import MotorText from './MotorText';
import MotorRotation from './MotorRotation';
import MotorTomoRotation from './MotorTomoRotation';
import type { MotorSchema } from '@esrf/daiquiri-lib';

export type { MotorSchema } from '@esrf/daiquiri-lib';

export default class Default extends HardwareVariant {
  public variants = {
    default: MotorDefault,
    small: MotorSmall,
    text: MotorText,
    rotation: MotorRotation,
    tomo_rotation: MotorTomoRotation,
  };
}

export function isMotorHardware(entity: Hardware): entity is MotorSchema {
  return entity.type === 'motor';
}
