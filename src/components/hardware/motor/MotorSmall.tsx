import { Button, InputGroup } from 'react-bootstrap';
import type {
  HardwareTypes,
  MotorSchema,
  MotorDefaultOptions,
} from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareNumericStep } from '@esrf/daiquiri-lib';
import { useMotorStates } from '@esrf/daiquiri-lib/src/hardware/motor';

export default function MotorSmall(
  props: HardwareTypes.HardwareWidgetProps<MotorSchema, MotorDefaultOptions>
) {
  const { hardware, options = {} } = props;
  function onAbortRequested() {
    hardware.actions.call('stop');
  }

  function onMovePositionRequested(target: number) {
    return hardware.actions.call('move', target);
  }

  const states = useMotorStates(hardware.properties.state, hardware.user_tags);

  return (
    <div className="hw-component">
      <div className="hw-single">
        <TypeIcon
          name="Motor"
          icon="fam-hardware-motor"
          online={hardware.online}
        />
        <div className="name">{hardware.name}</div>

        <div className="d-inline-block">
          <InputGroup>
            <HardwareNumericStep
              hardwareValue={hardware.properties?.position ?? 0}
              hardwareIsDisabled={props.disabled}
              hardwareIsReady={states.READY ?? false}
              hardwareIsMoving={states.MOVING ?? false}
              onMoveRequested={onMovePositionRequested}
              onAbortRequested={onAbortRequested}
              precision={
                options.precision ??
                hardware.properties.display_digits ??
                undefined
              }
              readOnly={options.readOnly}
              step={options.step}
              steps={options.steps}
              incIcon={options.incicon}
              decIcon={options.decicon}
              swapIncDec={options.swapincdec}
            />

            {states.MOVING && !props.disabled && !options.step && (
              <Button variant="danger" onClick={onAbortRequested}>
                <i className="fa fa-times" />
              </Button>
            )}
            {hardware.properties.unit && !options.step && (
              <InputGroup.Text>{hardware.properties.unit}</InputGroup.Text>
            )}
          </InputGroup>
        </div>
      </div>
    </div>
  );
}
