import { useDebouncedCallback } from '@react-hookz/web';
import type { MotorSchema, HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate, MotorState } from '@esrf/daiquiri-lib';
import InfiniteKnob360 from 'components/common/InfiniteKnob360';
import { isMotorClockWise } from './utils';
import type { MotorRotationOptions } from './utils';
import { useMotorStates } from '@esrf/daiquiri-lib/src/hardware/motor';

export default function MotorRotation(
  props: HardwareTypes.HardwareWidgetProps<MotorSchema, MotorRotationOptions>
) {
  const { hardware, options = {} } = props;

  // This have to be debounced as workaround cause it can be called twice
  const moveHardware = useDebouncedCallback(
    (value: number) => {
      hardware.actions.call('move', value);
    },
    [hardware.actions],
    100
  );

  const states = useMotorStates(hardware.properties.state, hardware.user_tags);
  const target = states.MOVING ? hardware.properties.target : undefined;
  const isClockWise = isMotorClockWise(hardware, options);

  const widgetIcon = (
    <TypeIcon
      name="Motor"
      icon="fam-hardware-motor-r"
      online={hardware.online}
    />
  );

  const widgetState = <MotorState hardware={hardware} />;

  const widgetContent = (
    <InfiniteKnob360
      className="mx-auto my-auto"
      size={100}
      value={hardware.properties.position}
      min={hardware.properties?.limits?.[0] ?? 0}
      max={hardware.properties?.limits?.[1] ?? 0}
      target={target}
      onChange={moveHardware}
      disabled={props.disabled}
      readOnly={options.readOnly || !states.READY}
      origin="down"
      direction={isClockWise ? 'clockwise' : 'anticlockwise'}
    />
  );

  const headerMode = options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
