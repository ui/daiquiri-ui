import type { MotorSchema, MotorDefaultOptions } from '@esrf/daiquiri-lib';

export interface MotorRotationOptions extends MotorDefaultOptions {
  clockwise?: boolean;
}

/**
 * True if the motor positon is clock wise.
 *
 * This is based on dial metadata, and user sign.
 */
export function isMotorClockWise(
  hardware: MotorSchema,
  options?: MotorRotationOptions
): boolean {
  if (options?.clockwise !== undefined) {
    return options.clockwise;
  }
  const inverted = hardware.properties.sign < 0;
  const anticlockwiseTag = hardware.user_tags.includes('dial.anticlockwise');
  const clockwiseTag = hardware.user_tags.includes('dial.clockwise');
  if (clockwiseTag && anticlockwiseTag) {
    console.error(
      "Both tags 'dial.clockwise' and 'dial.anticlockwise' are defined"
    );
  }
  const clockwise = clockwiseTag || !anticlockwiseTag;
  return clockwise !== inverted;
}

/**
 * Return the position as clock wise.
 *
 * This is based on dial metadata, and user sign.
 */
export function clockWisePosition(hardware: MotorSchema): number | null {
  const isClockWise = isMotorClockWise(hardware);
  const position = hardware.properties.position;
  if (position === null) {
    return null;
  }
  if (isClockWise) {
    return position;
  }
  return -position;
}
