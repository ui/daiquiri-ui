import {
  Children,
  cloneElement,
  isValidElement,
  useEffect,
  useRef,
  useState,
} from 'react';
import type { ReactChild } from 'react';
import type { HardwareTypes } from '@esrf/daiquiri-lib';

interface Props {
  minLimit?: number;
  maxLimit?: number;
  initialPosition?: number;
  initialVelocity?: number;
  children: ReactChild | ReactChild[];
}

export default function Motor(props: Props) {
  const {
    minLimit = -999_999_999,
    maxLimit = 999_999_999,
    initialPosition = 0,
    initialVelocity = 1000,
    children,
  } = props;
  const [position, setPosition] = useState(initialPosition);
  const [state, setState] = useState(['READY']);
  const [target, setTarget] = useState<number | null>(null);
  const internalTimeRef = useRef(Date.now());

  const updatePosition = () => {
    const now = Date.now();
    const deltaTime = (now - internalTimeRef.current) / 1000;
    let newPosition;
    internalTimeRef.current = now;
    if (target !== null && target !== position) {
      let newState;
      let newTarget;
      const deltaPosition = deltaTime * initialVelocity;
      if (deltaPosition > Math.abs(position - target)) {
        newPosition = target;
        newState = ['READY'];
        newTarget = null;
      } else {
        const direction = target > position ? 1 : -1;
        newPosition = position + direction * deltaPosition;
        newState = ['MOVING'];
        newTarget = target;
      }
      setPosition(newPosition);
      setState(newState);
      setTarget(newTarget);
    }
  };

  const call = (name: string, value?: any) => {
    if (name === 'stop') {
      setTarget(null);
      setState(['READY']);
      return null;
    }
    if (name === 'move') {
      const pos = Number.parseFloat(value);
      setTarget(Number.isNaN(pos) ? null : pos);
      return null;
    }
    console.log('MockMotor: Unsupported call', name, value);
    return null;
  };

  const setProperty = (name: string, value: any) => {
    console.log('MockMotor: Unsupported setProperty', name, value);
    return null;
  };

  useEffect(() => {
    const timer = setInterval(() => {
      updatePosition();
    }, 500);
    return () => clearInterval(timer);
  });

  const hardware = {
    callables: ['move', 'stop', 'wait', 'rmove'],
    id: 'omega',
    name: 'omega 123',
    online: true,
    properties: {
      acceleration: 300,
      limits: [minLimit, maxLimit],
      position,
      offset: 0,
      sign: 1,
      state,
      tolerance: 0.0001,
      unit: null,
      velocity: initialVelocity,
      target,
    },
    protocol: 'bliss',
    require_staff: false,
    type: 'motor',
    actions: {
      setProperty,
      call,
    },
    user_tags: [],
    locked: null,
  };

  return (
    <div>
      {Children.map(children, (child) => {
        return isValidElement(child)
          ? cloneElement(child, {
              hardware,
              ...child.props,
            })
          : undefined;
      })}
    </div>
  );
}
