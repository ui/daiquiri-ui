export function createHardwareMotor(name: string, position: number) {
  return {
    id: name,
    name,
    type: 'motor',
    properties: {
      position,
      unit: 'mm',
      limits: [-100, 100],
    },
    online: true,
  };
}
