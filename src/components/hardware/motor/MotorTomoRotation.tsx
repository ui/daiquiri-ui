import { useState } from 'react';
import {
  Button,
  ButtonGroup,
  Form,
  Image,
  InputGroup,
  Dropdown,
} from 'react-bootstrap';
import { map } from 'lodash';
import { useDebouncedCallback } from '@react-hookz/web';
import type { HardwareTypes, MotorSchema } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate, MotorState } from '@esrf/daiquiri-lib';
import InfiniteKnob360 from 'components/common/InfiniteKnob360';
import type { MotorRotationOptions } from './utils';
import { isMotorClockWise } from './utils';
import { angularDist } from 'helpers/math';
import { useTomoConfig } from '../../tomo/utils/hooks';
import { useOperator } from '../../utils/hooks';
import { useMotorStates } from '@esrf/daiquiri-lib/src/hardware/motor';

function asStyle(spriteSheet: string): React.CSSProperties {
  const rendering = spriteSheet.includes('_hd.') ? 'default' : 'pixelated';
  return {
    '--sprite-sheet': `url(${spriteSheet})`,
    '--spritesheet-rendering': rendering,
  } as React.CSSProperties;
}

function ToggleAngleButton(props: {
  spriteSheet: string;
  direction: 'front' | 'back' | 'right' | 'left';
  tolerance?: number;
  text?: string;
  disabled: boolean;
  readOnly: boolean;
  value: number | null;
  target: number;
  onChange: (position: number) => void;
  className?: string;
  active?: boolean;
  style?: { [name: string]: any };
}) {
  const { spriteSheet, direction, active, tolerance = 0.01 } = props;

  function propagateOnChange(position: number) {
    props.onChange(position);
  }

  /**
   * Returns the closest target mod 360 to this value.
   */
  function getClosestTarget(target: number, value: number | null) {
    if (value === null) {
      return target;
    }
    const delta = angularDist(value, target);
    const result = value + delta;
    if (Math.abs(delta) >= 179.9) {
      if (result > 270) {
        // Prefer to move back to try to stay in [0..360[
        return result - 360;
      }
    }
    return result;
  }

  const closestTarget = getClosestTarget(props.target, props.value);
  const text = props.text || closestTarget.toString();

  return (
    <Button
      size="lg"
      className={props.className}
      style={props.style}
      variant={
        props.value !== null &&
        Math.abs(props.value - closestTarget) < tolerance
          ? 'primary'
          : 'secondary'
      }
      value={closestTarget}
      disabled={props.disabled || props.readOnly}
      onClick={() => {
        propagateOnChange(closestTarget);
      }}
      title={text}
    >
      <i
        className={`tomo-avatar ${active ? `walk-${direction}` : direction}`}
        style={{
          display: 'flex',
          alignItems: 'center',
          ...asStyle(spriteSheet),
        }}
      />
      {!spriteSheet && text}
    </Button>
  );
}

interface StepperProps {
  defaultStep?: number;
  steps?: number[];
  unit: string;
  disabled: boolean;
  onChange: (rposition: number) => void;
  direction: string;
  style?: { [name: string]: any };
  className?: string;
}

function Stepper(props: StepperProps) {
  const [currentStep, setCurrentStep] = useState(props.defaultStep);

  const steps = map(props.steps || [props.defaultStep], (s) => (
    <option value={s} key={s}>
      {s}
    </option>
  ));

  const direction = props.direction === 'anticlockwise' ? 1 : -1;
  const validCurrentStep = currentStep || 0;

  return (
    <InputGroup style={props.style} className={props.className}>
      <InputGroup.Text>±</InputGroup.Text>
      <Form.Control
        as="select"
        defaultValue={currentStep}
        onChange={(e) => setCurrentStep(Number.parseFloat(e.target.value))}
      >
        {steps}
      </Form.Control>
      <>
        <Button
          disabled={props.disabled || !currentStep}
          onClick={() => props.onChange(-validCurrentStep * direction)}
        >
          <i className="fa fa-fw fa-rotate-right" aria-hidden="true" />
        </Button>
        <Button
          disabled={props.disabled || !currentStep}
          onClick={() => props.onChange(validCurrentStep * direction)}
        >
          <i className="fa fa-fw fa-rotate-left" aria-hidden="true" />
        </Button>
      </>
      <InputGroup.Text>{props.unit && <div>{props.unit}</div>}</InputGroup.Text>
    </InputGroup>
  );
}

interface UserDialOptionProps {
  hardware: HardwareTypes.EditableHardware<MotorSchema>;
  disabled: boolean;
  style?: { [name: string]: any };
}

/**
 * Display and manage the offset between user/dial
 */
function UserDialOption(props: UserDialOptionProps) {
  const { hardware } = props;
  function usePositionAsRef() {
    hardware.actions.setProperty('position', 0);
  }
  function clearOffset() {
    hardware.actions.setProperty('offset', 0);
  }

  const variant = hardware.properties.offset !== 0 ? 'warning' : 'secondary';

  function getText() {
    if (hardware.properties.offset === 0) {
      return 'No offset';
    }
    const offset = hardware.properties.offset * hardware.properties.sign;
    if (offset < 0) {
      return `${offset.toFixed(1)}`;
    }
    return `+${offset.toFixed(1)}`;
  }

  const text = getText();

  const { position = null, offset = null, sign = null } = hardware.properties;
  if (position === null || offset === null || sign === null) {
    // The hardware is disconnected
    return <></>;
  }

  const dial = (position - offset) / sign;
  const unit = hardware.properties.unit || '';

  return (
    <Dropdown style={props.style}>
      <Dropdown.Toggle
        variant={variant}
        id="dropdown-offset"
        disabled={props.disabled}
      >
        {text}
      </Dropdown.Toggle>

      <Dropdown.Menu>
        <Dropdown.Item>
          User position: {position.toFixed(1)} {unit}
        </Dropdown.Item>
        <Dropdown.Item>
          Dial position: {dial.toFixed(1)} {unit}
        </Dropdown.Item>
        <Dropdown.Divider />
        <Dropdown.Item onClick={usePositionAsRef} disabled={position === 0}>
          Use actual position as ref
        </Dropdown.Item>
        <Dropdown.Item onClick={clearOffset} disabled={offset === 0}>
          Clear the offset
        </Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  );
}

export interface MotorTomoRotationOptions extends MotorRotationOptions {
  spritesheet?: string;
}

export default function MotorTomoRotation(
  props: HardwareTypes.HardwareWidgetProps<
    MotorSchema,
    MotorTomoRotationOptions
  >
) {
  const { hardware, options = {} } = props;
  const [edited, setEdited] = useState(false);
  const tolerance = hardware.properties.tolerance ?? 0.1;

  const operator = useOperator();

  // This have to be debounced as workaround cause it can be called twice
  const moveHardware = useDebouncedCallback(
    (value: number) => {
      hardware.actions.call('move', value);
    },
    [hardware.actions],
    100
  );

  /**
   * Occured on knob value changed.
   */
  function onKnobChange(value: number) {
    moveHardware(value);
    setEdited(false);
  }

  /**
   * Occured on knob click interaction.
   *
   * Synchronize the motor only with the mouse release
   */
  function onKnobInteractiveChange(position: number) {
    setEdited(true);
  }

  /**
   * Occured on knob click interaction.
   *
   * Synchronize the motor only with the mouse release
   */
  function onToggleAngleTriggered(angle: number) {
    hardware.actions.call('move', angle);
  }

  function onStep(rangle: number) {
    hardware.actions.call('rmove', rangle);
  }

  function stop() {
    hardware.actions.call('stop');
  }

  const states = useMotorStates(hardware.properties.state, hardware.user_tags);

  const { position, unit } = hardware.properties;
  const target = states.MOVING ? hardware.properties.target : undefined;
  const normalizedTarget =
    target !== undefined ? (target + 360) % 360 : undefined;
  const isClockWise = isMotorClockWise(hardware, options);

  const tomoConfig = useTomoConfig();
  const spriteSheet =
    options.spritesheet ??
    tomoConfig.avatar?.sprite_sheet ??
    'resources/tomo/fox-yellow.png';

  const widgetIcon = (
    <TypeIcon
      name="Motor"
      icon="fam-hardware-motor-r"
      online={hardware.online}
    />
  );
  const widgetState = <MotorState hardware={hardware} />;

  const widgetContent = (
    <div
      style={{
        marginLeft: 'auto',
        marginRight: 'auto',
        display: 'grid',
        justifyContent: 'center',
        gridTemplateColumns: 'max-content max-content max-content max-content',
        gridTemplateRows: 'max-content max-content max-content max-content',
      }}
    >
      <ToggleAngleButton
        value={position}
        tolerance={tolerance}
        disabled={props.disabled}
        readOnly={options.readOnly || !states.READY}
        target={180}
        active={normalizedTarget === 180}
        onChange={onToggleAngleTriggered}
        text="Back"
        direction="back"
        spriteSheet={spriteSheet}
        style={{
          gridRow: 1,
          gridColumn: 2,
          justifySelf: 'center',
          alignSelf: 'end',
        }}
      />
      <ToggleAngleButton
        className="ms-auto"
        value={position}
        tolerance={tolerance}
        disabled={props.disabled}
        readOnly={options.readOnly || !states.READY}
        target={isClockWise ? 90 : 270}
        active={normalizedTarget === (isClockWise ? 90 : 270)}
        onChange={onToggleAngleTriggered}
        text="Right"
        direction="left"
        spriteSheet={spriteSheet}
        style={{
          gridRow: 2,
          gridColumn: 1,
          justifySelf: 'end',
          alignSelf: 'center',
        }}
      />
      <ToggleAngleButton
        value={position}
        tolerance={tolerance}
        disabled={props.disabled}
        readOnly={options.readOnly || !states.READY}
        target={isClockWise ? 270 : 90}
        active={normalizedTarget === (isClockWise ? 270 : 90)}
        onChange={onToggleAngleTriggered}
        className="me-auto"
        text="Left"
        direction="right"
        spriteSheet={spriteSheet}
        style={{
          gridRow: 2,
          gridColumn: 3,
          justifySelf: 'start',
          alignSelf: 'center',
        }}
      />
      <ToggleAngleButton
        value={position}
        tolerance={tolerance}
        disabled={props.disabled}
        readOnly={options.readOnly || !states.READY}
        target={0}
        active={normalizedTarget === 0}
        onChange={onToggleAngleTriggered}
        text="Front"
        direction="front"
        spriteSheet={spriteSheet}
        style={{
          gridRow: 3,
          gridColumn: 2,
          justifySelf: 'center',
          alignSelf: 'start',
        }}
      />
      <div
        style={{
          gridRow: 2,
          gridColumn: 2,
        }}
      >
        <InfiniteKnob360
          className="m-1"
          size={100}
          value={position}
          min={hardware.properties?.limits?.[0] ?? 0}
          max={hardware.properties?.limits?.[1] ?? 0}
          target={target}
          onChange={onKnobChange}
          onInteractiveChange={onKnobInteractiveChange}
          disabled={props.disabled}
          readOnly={options.readOnly || !states.READY}
          origin="down"
          direction={isClockWise ? 'clockwise' : 'anticlockwise'}
        />
      </div>
      <UserDialOption
        style={{
          gridRow: 1,
          gridColumn: '3 / span 2',
          justifySelf: 'center',
          alignSelf: 'center',
        }}
        disabled={props.disabled}
        hardware={hardware}
      />
      {states.MOVING && operator && !options.readOnly && (
        <Button
          variant="danger"
          onClick={stop}
          className="mt-1 ms-2"
          style={{
            gridRow: 3,
            gridColumn: 3,
            justifySelf: 'center',
            alignSelf: 'center',
          }}
        >
          <i className="fa fa-times" />
        </Button>
      )}
      {options.steps && (
        <Stepper
          className="mt-1"
          disabled={props.disabled}
          steps={options.steps}
          defaultStep={options.step}
          onChange={onStep}
          unit={unit}
          direction={isClockWise ? 'clockwise' : 'anticlockwise'}
          style={{ gridRow: 4, gridColumn: '1 / span 4' }}
        />
      )}
    </div>
  );

  const headerMode = options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
