import { HardwareState } from '@esrf/daiquiri-lib';
import type { OpticSchema } from '.';

export function OpticState(props: { hardware: OpticSchema }) {
  const { hardware } = props;
  const state = hardware.online ? hardware.properties.state : 'OFFLINE';
  const variants: { [state: string]: string } = {
    READY: 'success',
    MOVING: 'warning',
    INVALID: 'danger',
    UNKNOWN: 'danger',
    DISABLED: 'secondary',
    OFFLINE: 'warning',
  };
  const variant = variants[state] || variants.UNKNOWN;
  return (
    <HardwareState.HardwareState state={state} minWidth={6} variant={variant} />
  );
}
