import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { HardwareVariant } from '@esrf/daiquiri-lib';
import Optic from './Optic';

/**
 * Hardware as exposed by Redux
 */
export interface OpticSchema extends HardwareTypes.Hardware {
  properties: {
    state: string;
    magnification: number;
    available_magnifications: number[];
    target_magnification: number;
    magnification_range: [number, number] | null;
  };
}

export default class Default extends HardwareVariant {
  public variants = {
    default: Optic,
  };
}
