import { useEffect, useRef, useState } from 'react';
import type { KeyboardEvent } from 'react';
import classNames from 'classnames';
import { Dropdown, InputGroup, Form } from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import { OpticState } from './State';
import type { OpticSchema } from '.';
import ReadOnlyTextInput from 'components/common/ReadOnlyTextInput';

export interface OpticWidgetOptions
  extends HardwareTypes.HardwareWidgetOptions {
  readonly?: boolean;
  digits?: number;
  allowtuning?: boolean;
}

function MultiPositionMagnification(props: {
  hardware: HardwareTypes.EditableHardware<OpticSchema>;
  options: OpticWidgetOptions;
  disabled: boolean;
}) {
  const { hardware, options = {} } = props;
  const { properties } = hardware;
  const { state } = properties;
  const hardwareMoving = state === 'MOVING';
  const hardwareReady = state === 'READY';
  const magnification = hardwareMoving
    ? properties.target_magnification || 0
    : properties.magnification || 0;
  const availableMagnifications = properties.available_magnifications;
  const magnifications = [...availableMagnifications];
  magnifications.sort((a, b) => {
    if (a > b) return 1;
    if (b > a) return -1;
    return 0;
  });
  const disabled = props.disabled || !hardware.online || !hardwareReady;
  const digits = options.digits || 2;

  function moveHardwareMagnification(value: number) {
    if (hardware.actions) {
      hardware.actions.call('move', value);
    } else {
      console.error('Change request was triggered with:', value);
    }
  }

  function createSelectableItem(index: number, itemMagnification: number) {
    const selected = magnification === itemMagnification;
    const icon = selected ? 'fa fa-fw fa-dot-circle-o' : 'fa fa-fw fa-circle-o';
    return (
      <Dropdown.Item
        key={`id${index}`}
        disabled={disabled}
        onClick={() => {
          moveHardwareMagnification(itemMagnification);
        }}
      >
        <i className={icon} />
        &nbsp; × {itemMagnification}
      </Dropdown.Item>
    );
  }

  return (
    <>
      <ReadOnlyTextInput
        value={magnification.toFixed(digits)}
        className={classNames({
          'hw-moving': hardwareMoving,
        })}
      />
      <Dropdown>
        <Dropdown.Toggle disabled={!hardwareReady} variant="secondary" />
        <Dropdown.Menu>
          {magnifications.map((m, index) => createSelectableItem(index, m))}
        </Dropdown.Menu>
      </Dropdown>
    </>
  );
}

function FixedMagnification(props: {
  hardware: HardwareTypes.EditableHardware<OpticSchema>;
  options: OpticWidgetOptions;
  disabled: boolean;
}) {
  const { hardware, options = {} } = props;
  const magnification = hardware.properties.magnification || 0;
  const digits = options.digits || 2;
  const allowtuning = options.allowtuning === true;

  return (
    <>
      <ReadOnlyTextInput value={magnification.toFixed(digits)} />
      {allowtuning && (
        <Dropdown>
          <Dropdown.Toggle variant="secondary" />
          <Dropdown.Menu>
            <InputGroup>
              <InputGroup.Text>×</InputGroup.Text>
              <InputMagnification {...props} />
            </InputGroup>
          </Dropdown.Menu>
        </Dropdown>
      )}
    </>
  );
}

function InputMagnification(props: {
  hardware: HardwareTypes.EditableHardware<OpticSchema>;
  disabled: boolean;
  options: OpticWidgetOptions;
  mode?: string;
}) {
  const { hardware, options = {} } = props;
  const magnification = hardware.properties.magnification || 0;
  const { state } = hardware.properties;
  const hardwareReady = state === 'READY';
  const disabled = props.disabled || !hardware.online || !hardwareReady;

  const [rangeMin, rangeMax] = hardware.properties.magnification_range || [
    null,
    null,
  ];

  const formRef = useRef<HTMLInputElement>(null);
  const rangeRef = useRef<HTMLInputElement>(null);
  const [edited, setEdited] = useState(false);
  const digits = options.digits || 2;

  function setHardwareMagnification(value: number) {
    if (hardware.actions) {
      if (props.mode === 'move') {
        hardware.actions.call('move', value);
      } else {
        hardware.actions.setProperty('magnification', value);
      }
    } else {
      console.error('Change request was triggered with:', value);
    }
  }

  function onKeyDown(event: KeyboardEvent) {
    if (!formRef || !formRef.current) {
      console.log('formRef not yet defined');
      return;
    }
    if (event.key === 'Enter') {
      setEdited(false);
      const { value } = formRef.current;
      const valueAsFloat = Number.parseFloat(value);

      if (rangeMin !== null && valueAsFloat < rangeMin) {
        return;
      }
      if (rangeMax !== null && valueAsFloat > rangeMax) {
        return;
      }
      setHardwareMagnification(valueAsFloat);
    } else if (event.key === 'Escape') {
      formRef.current.value = magnification.toFixed(digits);
      if (rangeRef.current) {
        rangeRef.current.value = magnification.toString();
      }
      setEdited(false);
    } else {
      console.log(event.key);
    }
  }

  function updateEdit(value: number) {
    if (value !== magnification && !edited) {
      setEdited(true);
    }
    if (value === magnification && edited) {
      setEdited(false);
    }
  }

  function onInputChange(event: any) {
    if (!formRef || !formRef.current) {
      console.error('formRef is not set');
      return;
    }
    const value = Number.parseFloat(formRef.current.value);
    updateEdit(value);
    if (rangeRef.current) {
      rangeRef.current.value = value.toString();
    }
  }

  function onRangeChange(event: any) {
    if (!formRef || !formRef.current) {
      console.error('formRef is not set');
      return;
    }
    if (!rangeRef || !rangeRef.current) {
      console.error('rangeRef is not set');
      return;
    }
    const value = Number.parseFloat(rangeRef.current.value);
    updateEdit(value);
    formRef.current.value = value.toFixed(digits);
  }

  function onRangeAccepted() {
    if (!rangeRef || !rangeRef.current) {
      console.error('rangeRef is not set');
      return;
    }
    const value = Number.parseFloat(rangeRef.current.value);
    setHardwareMagnification(value);
  }

  useEffect(() => {
    if (!formRef || !formRef.current) {
      console.error('formRef is not set');
      return;
    }
    if (!edited) {
      formRef.current.value = magnification.toFixed(digits);
    }
    if (rangeRef.current) {
      rangeRef.current.value = magnification.toString();
    }
  }, [digits, edited, magnification]);

  return (
    <>
      <Form.Control
        className={edited ? 'form-control-edited' : ''}
        type="number"
        ref={formRef}
        defaultValue={magnification.toFixed(digits)}
        onChange={onInputChange}
        disabled={disabled}
        step={10 ** -digits}
        onKeyDown={onKeyDown}
      />
      {rangeMin !== null && rangeMax !== null && (
        <Form.Control
          ref={rangeRef}
          disabled={disabled}
          type="range"
          min={rangeMin}
          max={rangeMax}
          step={10 ** -digits}
          onChange={onRangeChange}
          onMouseUp={onRangeAccepted}
          defaultValue={magnification}
        />
      )}
    </>
  );
}

function RangeMagnification(props: {
  hardware: HardwareTypes.EditableHardware<OpticSchema>;
  options: OpticWidgetOptions;
  disabled: boolean;
}) {
  const { hardware, options = {} } = props;
  const { properties } = hardware;
  const hardwareMoving = properties.state === 'MOVING';
  const hardwareReady = properties.state === 'READY';
  const magnification = hardwareMoving
    ? properties.target_magnification || 0
    : properties.magnification || 0;
  const digits = props.options.digits || 2;

  return (
    <>
      <ReadOnlyTextInput
        value={magnification.toFixed(digits)}
        className={classNames({
          'hw-moving': hardwareMoving,
        })}
      />
      <Dropdown>
        <Dropdown.Toggle disabled={!hardwareReady} variant="secondary" />
        <Dropdown.Menu>
          <InputGroup>
            <InputGroup.Text>×</InputGroup.Text>
            <InputMagnification {...props} mode="move" />
          </InputGroup>
        </Dropdown.Menu>
      </Dropdown>
    </>
  );
}

function OffMagnification(props: {
  hardware: HardwareTypes.EditableHardware<OpticSchema>;
  options: OpticWidgetOptions;
}) {
  return (
    <Form.Control
      className="border-light"
      type="text"
      disabled
      defaultValue="Missing"
    />
  );
}

export default function Optic(
  props: HardwareTypes.HardwareWidgetProps<OpticSchema, OpticWidgetOptions>
) {
  const { hardware, options = {} } = props;
  const widgetIcon = (
    <TypeIcon name="Optic" icon="fam-hardware-optic" online={hardware.online} />
  );

  const widgetState = <OpticState hardware={hardware} />;

  function getWidgetContent() {
    function getOpticClass() {
      if (!hardware.online) {
        return OffMagnification;
      }
      if (hardware.properties.available_magnifications) {
        return MultiPositionMagnification;
      }
      if (hardware.properties.magnification_range) {
        return RangeMagnification;
      }
      return FixedMagnification;
    }

    const Class = getOpticClass();

    return (
      <InputGroup>
        <InputGroup.Text className="border-light">×</InputGroup.Text>
        <Class {...props} />
      </InputGroup>
    );
  }

  const headerMode = options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={getWidgetContent()}
      headerMode={headerMode}
    />
  );
}
