import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { HardwareVariant } from '@esrf/daiquiri-lib';
import Gauge from './Gauge';

/**
 * Hardware as exposed by Redux
 */
export interface GaugeSchema extends HardwareTypes.Hardware {
  properties: {
    state: string;
    status: string;
    pressure: number;
    unit?: string; // FIXME: The server implementation is missing
  };
}

export default class Default extends HardwareVariant {
  public variants = {
    default: Gauge,
  };
}
