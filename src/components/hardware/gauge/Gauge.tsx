import {
  Badge,
  Button,
  OverlayTrigger,
  Popover,
  InputGroup,
  Form,
} from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon } from '@esrf/daiquiri-lib';
import type { GaugeSchema } from '.';

export interface GaugeOptions extends HardwareTypes.HardwareWidgetOptions {
  /** Unit of pressure */
  unit?: string;
  /** Precision to show, default 3 */
  precision?: number;
}

function GaugeOverlay(
  props: HardwareTypes.HardwareWidgetProps<GaugeSchema, GaugeOptions>
) {
  const { hardware } = props;
  const toggle = () => {
    hardware.actions.call(hardware.properties.state === 'ON' ? 'off' : 'on');
  };

  return (
    <OverlayTrigger
      trigger="click"
      rootClose
      overlay={
        <Popover id="popid">
          <Popover.Header>{hardware.name} Details</Popover.Header>
          <Popover.Body>
            <h6>Status</h6>
            <pre>{hardware.properties.status}</pre>

            <div className="d-grid gap-2">
              <Button disabled={props.disabled} onClick={toggle}>
                {hardware.properties.state === 'ON' ? 'Off' : 'On'}
              </Button>
            </div>
          </Popover.Body>
        </Popover>
      }
    >
      <Button title="Gauge Details">
        <i className="fa fa-ellipsis-h" />
      </Button>
    </OverlayTrigger>
  );
}

function Gauge(
  props: HardwareTypes.HardwareWidgetProps<GaugeSchema, GaugeOptions>
) {
  const { hardware, options = {} } = props;
  return (
    <div className="hw-component">
      <div className="hw-head">
        <TypeIcon
          name="Gauge"
          icon="fam-hardware-gauge"
          online={hardware.online}
        />
        <div className="name">{hardware.name}</div>
        <Badge bg={hardware.properties.state === 'ON' ? 'success' : 'danger'}>
          {hardware.properties.state}
        </Badge>
      </div>
      <div className="hw-content">
        <InputGroup>
          <Form.Control
            value={hardware.properties.pressure.toExponential(
              options.precision || 3
            )}
            disabled
          />
          {(hardware.properties.unit || options.unit) && (
            <InputGroup.Text>
              {hardware.properties.unit || options.unit}
            </InputGroup.Text>
          )}
          <GaugeOverlay {...props} />
        </InputGroup>
      </div>
    </div>
  );
}

export default Gauge;
