import { Button, ButtonGroup } from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import { PusherState } from './State';
import type { PusherSchema } from '.';

function PusherBlock(props: {
  hardware: HardwareTypes.EditableHardware<PusherSchema>;
  options: HardwareTypes.HardwareWidgetOptions;
  disabled: boolean;
  className: string;
}) {
  const { className, hardware, options = {} } = props;
  const { state } = hardware.properties ?? 'OFFLINE';
  const isInTouch = state === 'IN_TOUCH';
  const disabled = !isInTouch || props.disabled;

  function ButtonPush(props: { distance: number }) {
    const { distance } = props;
    return (
      <Button
        variant="secondary"
        onClick={() => {
          hardware.actions.call('push', distance);
        }}
        disabled={disabled}
      >
        <span className="text-nowrap">
          <i className="fa fa-arrow-right fa-rotate-180" /> {distance}
        </span>
      </Button>
    );
  }
  return (
    <div
      className={className}
      style={{
        display: 'grid',
        gridTemplateRows: 'min-content min-content min-content',
        gridTemplateColumns: 'min-content min-content min-content',
        gridGap: '1px',
      }}
    >
      <ButtonPush distance={5} />
      <ButtonPush distance={2} />
      <ButtonPush distance={1} />
      <ButtonPush distance={0.5} />
      <ButtonPush distance={0.2} />
      <ButtonPush distance={0.1} />
      <ButtonPush distance={0.05} />
      <ButtonPush distance={0.02} />
      <ButtonPush distance={0.01} />
    </div>
  );
}

function RawPusher(props: {
  hardware: HardwareTypes.EditableHardware<PusherSchema>;
  options: HardwareTypes.HardwareWidgetOptions;
  disabled: boolean;
}) {
  const { hardware, options, disabled } = props;
  const { state } = hardware.properties ?? 'OFFLINE';
  const isRetracted = state === 'RETRACTED';
  const isInTouch = state === 'IN_TOUCH';

  const location = isRetracted ? 'ms-auto' : isInTouch ? 'ms-0' : 'mx-auto';

  return (
    <div
      style={{
        display: 'grid',
        gridTemplateRows: 'min-content min-content',
        gridTemplateColumns: 'min-content auto',
        gridGap: '1px',
      }}
    >
      <i
        className={`${
          isInTouch ? 'fas fa-dizzy' : 'fa fa-smile'
        } fa-3x my-auto`}
      />
      <PusherBlock className={location} {...props} />
      <span />
      <ButtonGroup className="mx-auto" style={{ width: '12em' }}>
        <Button
          onClick={() => {
            hardware.actions.call('move_in');
          }}
          disabled={isInTouch || disabled}
          style={{ width: '5em' }}
        >
          <span className="text-nowrap">
            <i className="fa fa-arrow-right fa-rotate-180" /> In
          </span>
        </Button>
        <Button
          onClick={() => {
            hardware.actions.call('move_out');
          }}
          disabled={isRetracted || disabled}
          style={{ width: '5em' }}
        >
          <span className="text-nowrap">
            Out <i className="fa fa-arrow-right" />
          </span>
        </Button>
      </ButtonGroup>
    </div>
  );
}

export default function Pusher(
  props: HardwareTypes.HardwareWidgetProps<
    PusherSchema,
    HardwareTypes.HardwareWidgetOptions
  >
) {
  const { hardware, options = {} } = props;
  const widgetIcon = (
    <TypeIcon
      name="Optic"
      icon="fa-hand-point-right"
      online={hardware.online}
    />
  );

  const widgetState = <PusherState hardware={hardware} />;

  function getWidgetContent() {
    return <RawPusher {...props} />;
  }

  const headerMode = options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={getWidgetContent()}
      headerMode={headerMode}
    />
  );
}
