import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { HardwareVariant } from '@esrf/daiquiri-lib';
import Pusher from './Pusher';

/**
 * Hardware as exposed by Redux
 */
export interface PusherSchema extends HardwareTypes.Hardware {
  properties: {
    state: string;
  };
}

export default class Default extends HardwareVariant {
  public variants = {
    default: Pusher,
  };
}
