import { HardwareState } from '@esrf/daiquiri-lib';
import type { PusherSchema } from '.';

export function PusherState(props: { hardware: PusherSchema }) {
  const { hardware } = props;
  const state = hardware.online ? hardware.properties.state : 'OFFLINE';
  const variants: { [state: string]: string } = {
    RETRACTED: 'success',
    IN_TOUCH: 'warning',
    MOVING_IN: 'warning',
    MOVING_OUT: 'warning',
    UNKNOWN: 'danger',
    OFFLINE: 'warning',
  };
  const variant = variants[state] || variants.UNKNOWN;
  return (
    <HardwareState.HardwareState state={state} minWidth={6} variant={variant} />
  );
}
