import { HardwareVariant, Multiposition } from '@esrf/daiquiri-lib';

export default class Default extends HardwareVariant {
  public variants = {
    default: Multiposition,
  };
}
