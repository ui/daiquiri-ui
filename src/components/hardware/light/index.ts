import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { HardwareVariant } from '@esrf/daiquiri-lib';
import Light from './Light';

/**
 * Hardware as exposed by Redux
 */
export interface LightSchema extends HardwareTypes.Hardware {
  properties: {
    state: string;
    temperature: number;
    intensity: number;
  };
}

export default class Default extends HardwareVariant {
  public variants = {
    default: Light,
  };
}
