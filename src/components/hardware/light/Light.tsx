import { useRef, useCallback } from 'react';
import _debounce from 'lodash/debounce';
import { Badge, Form, Button } from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon } from '@esrf/daiquiri-lib';
import type { LightSchema } from '.';

export default function Light(
  props: HardwareTypes.HardwareWidgetProps<LightSchema>
) {
  const sliderRef = useRef<HTMLInputElement>(null);
  const { hardware, options = {} } = props;

  function requestIntensityChange(value: number) {
    hardware.actions.setProperty('intensity', value);
  }

  const debounceIntensityChange = useCallback(
    _debounce(requestIntensityChange, 100),
    []
  );

  return (
    <div className="hw-component">
      <div className="hw-head">
        <TypeIcon name="Light" icon="fa-lightbulb-o" online={hardware.online} />
        <div className="name">{hardware.name}</div>
        <Badge bg={hardware.properties.state === 'ON' ? 'success' : 'danger'}>
          {hardware.properties.state}
        </Badge>
      </div>
      <div className="hw-content">
        <Form.Control
          ref={sliderRef}
          disabled={props.disabled}
          type="range"
          min="0"
          max="100"
          onChange={(e) =>
            debounceIntensityChange(Number.parseFloat(e.target.value))
          }
          defaultValue={hardware.properties.intensity}
        />
        <div>
          <Button
            disabled={hardware.properties.intensity === 0}
            size="sm"
            style={{
              fontSize: '0.6rem',
              padding: '0.05rem 0.6rem',
              marginTop: '0.1rem',
            }}
            onClick={() => debounceIntensityChange(0)}
          >
            <i className="fa fa-power-off" />
          </Button>
          <span className="float-end">{hardware.properties.intensity} %</span>
        </div>
      </div>
    </div>
  );
}
