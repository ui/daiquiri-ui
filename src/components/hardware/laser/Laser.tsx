import _debounce from 'lodash/debounce';
import { Button, InputGroup } from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import {
  TypeIcon,
  HardwareTemplate,
  HardwareNumericStep,
  HardwareState,
} from '@esrf/daiquiri-lib';
import type { LaserSchema } from '.';

export interface LaserOptions extends HardwareTypes.HardwareWidgetOptions {
  /** Default step size to use for up / down arrows */
  step?: number;
  /** Array of selectable step sizes */
  steps?: number[];
}

export default function Laser(
  props: HardwareTypes.HardwareWidgetProps<LaserSchema, LaserOptions>
) {
  const { hardware, options = {}, disabled } = props;

  function requestPowerChange(value: number) {
    return hardware.actions.setProperty('power', value);
  }

  function requestStateChange(state: boolean) {
    return hardware.actions.call(state ? 'on' : 'off');
  }

  const state = hardware.properties.state;

  const widgetIcon = (
    <TypeIcon name="Laser" icon="fam-hardware-laser" online={hardware.online} />
  );

  const widgetState = (
    <HardwareState.HardwareState
      state={state}
      minWidth={6}
      variant={state === 'ON' ? 'success' : 'warning'}
    />
  );

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={
        <InputGroup>
          <HardwareNumericStep
            id={hardware.id}
            hardwareValue={hardware.properties.power}
            hardwareIsDisabled={false}
            hardwareIsReady
            hardwareIsMoving={state === 'MOVING'}
            onMoveRequested={requestPowerChange}
            onAbortRequested={() => null}
            precision={1}
            readOnly={false}
            step={options.step}
            steps={options.steps}
            incIcon={undefined}
            decIcon={undefined}
            swapIncDec={undefined}
          />
          <Button
            size="sm"
            style={{
              fontSize: '0.6rem',
              padding: '0.05rem 0.6rem',
              marginTop: '0.1rem',
            }}
            onClick={() => {
              requestStateChange(hardware.properties.state !== 'ON');
            }}
          >
            <i
              className={
                hardware.properties.state === 'ON'
                  ? 'fa fa-toggle-on'
                  : 'fa fa-toggle-off'
              }
            />
          </Button>
        </InputGroup>
      }
    />
  );
}
