import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { HardwareVariant } from '@esrf/daiquiri-lib';
import Laser from './Laser';

/**
 * Hardware as exposed by Redux
 */
export interface LaserSchema extends HardwareTypes.Hardware {
  properties: {
    state: string;
    power: number;
  };
}

export default class Default extends HardwareVariant {
  public variants = {
    default: Laser,
  };
}
