import { HardwareVariant } from '@esrf/daiquiri-lib';
import Wago from './Wago';

export default class Default extends HardwareVariant {
  public variants = {
    default: Wago,
  };
}
