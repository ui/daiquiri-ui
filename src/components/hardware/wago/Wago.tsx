import { Alert, Badge } from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon } from '@esrf/daiquiri-lib';
import StandaloneSchemaField from 'components/form-widgets/StandaloneSchemaField';

export default function Wago(props: HardwareTypes.HardwareWidgetProps) {
  const { hardware, options = {}, schema = null } = props;

  function onChange(prop: any, value: any) {
    hardware.actions.setProperty(prop, value);
  }

  function onKeyPress(prop: any, value: any) {
    hardware.actions.setProperty(prop, value);
  }

  return (
    <div className="hw-component">
      <div className="hw-head">
        <TypeIcon
          name="Wago"
          icon="fam-hardware-any"
          online={hardware.online}
        />
        <div className="name">{hardware.name}</div>
        <Badge bg={hardware.properties.state === 'ON' ? 'success' : 'danger'}>
          {hardware.properties.state}
        </Badge>
      </div>
      <div className="hw-content">
        {schema && (
          <StandaloneSchemaField
            schema={schema.properties}
            formData={hardware.properties}
            disabled={props.disabled}
            hiddenFields={['state']}
            onFieldKeyPress={onKeyPress}
            onFieldChange={onChange}
          />
        )}
        {schema === null && (
          <Alert variant="danger">
            This wago do not expose any description
          </Alert>
        )}
      </div>
    </div>
  );
}
