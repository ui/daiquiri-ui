import type { MouseEvent } from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import { TomoImagingState } from './State';
import type { TomoImagingSchema } from '.';

function UpdateMode(props: {
  hardware: HardwareTypes.EditableHardware<TomoImagingSchema>;
  disabled: boolean;
}) {
  const { hardware } = props;
  const updateOnMove = hardware.properties.update_on_move;
  const disabled = props.disabled || !hardware.online;

  function setUpdateModeOff(event: MouseEvent) {
    hardware.actions.setProperty('update_on_move', false);
  }

  function setUpdateModeAuto(event: MouseEvent) {
    hardware.actions.setProperty('update_on_move', true);
  }

  return (
    <ButtonGroup className="w-100">
      <Button
        onClick={setUpdateModeAuto}
        disabled={disabled}
        variant={updateOnMove ? 'primary' : 'secondary'}
      >
        Auto
      </Button>
      <Button
        onClick={setUpdateModeOff}
        disabled={disabled}
        variant={updateOnMove ? 'secondary' : 'primary'}
      >
        Off
      </Button>
    </ButtonGroup>
  );
}

export default function TomoImagingUpdateMode(
  props: HardwareTypes.HardwareWidgetProps<TomoImagingSchema>
) {
  const { hardware, options = {} } = props;

  const widgetIcon = (
    <TypeIcon
      name="TomoImaging"
      icon="fam-hardware-camera"
      online={hardware.online}
    />
  );

  const widgetState = <TomoImagingState hardware={hardware} />;

  const widgetContent = (
    <UpdateMode hardware={hardware} disabled={props.disabled} />
  );

  const headerMode = options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
