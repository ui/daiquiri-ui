import { useEffect, useRef, useState } from 'react';
import type { ChangeEvent, KeyboardEvent } from 'react';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import { InputGroup, Form } from 'react-bootstrap';
import { TomoImagingState } from './State';
import type { TomoImagingSchema } from '.';
import { useHardware } from '../../tomo/utils/hooks';
import { guessSubframes, isLimaHardware } from '../lima';
import { isTomoDetectorHardware } from '../tomodetector';

function SettleTime(props: {
  hardware: HardwareTypes.EditableHardware<TomoImagingSchema>;
  disabled: boolean;
}) {
  const { hardware } = props;
  const settleTime = hardware.properties.settle_time || 0;
  const disabled = props.disabled || !hardware.online;

  const formRef = useRef<HTMLInputElement>(null);
  const [edited, setEdited] = useState(false);
  const digits = 3;

  function onKeyDown(e: KeyboardEvent) {
    if (!formRef || !formRef.current) {
      console.log('formRef not yet defined');
    } else if (e.key === 'Enter') {
      setEdited(false);
      hardware.actions.setProperty('settle_time', formRef.current.value);
    } else if (e.key === 'Escape') {
      formRef.current.value = settleTime.toFixed(digits);
      setEdited(false);
    } else {
      console.log(e.key);
    }
  }

  function onChange(event: ChangeEvent<HTMLInputElement>) {
    const { value } = event.target;
    if (value === undefined) {
      return;
    }
    const exp = Number.parseFloat(value);
    if (exp !== settleTime && !edited) {
      setEdited(true);
    }
    if (exp === settleTime && edited) {
      setEdited(false);
    }
  }

  useEffect(() => {
    if (!formRef || !formRef.current) {
      console.log('formRef not yet defined');
    } else if (!edited) {
      formRef.current.value = settleTime.toFixed(digits);
    }
  }, [edited, settleTime]);

  return (
    <InputGroup className="w-100">
      <Form.Control
        className={edited ? 'form-control-edited' : ''}
        type="number"
        ref={formRef}
        defaultValue={settleTime.toFixed(digits)}
        onChange={onChange}
        disabled={disabled}
        onKeyDown={onKeyDown}
      />
      <InputGroup.Text>s</InputGroup.Text>
    </InputGroup>
  );
}

export default function TomoImagingSettleTime(
  props: HardwareTypes.HardwareWidgetProps<TomoImagingSchema>
) {
  const { hardware, options = {} } = props;

  const widgetIcon = (
    <TypeIcon
      name="TomoImaging"
      icon="fam-hardware-camera"
      online={hardware.online}
    />
  );

  const widgetState = <TomoImagingState hardware={hardware} />;

  const widgetContent = (
    <SettleTime hardware={hardware} disabled={props.disabled} />
  );

  const headerMode = options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
