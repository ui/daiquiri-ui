import { HardwareState } from '@esrf/daiquiri-lib';
import type { TomoImagingSchema } from '.';

export function TomoImagingState(props: { hardware: TomoImagingSchema }) {
  const { hardware } = props;
  const state = hardware.online ? hardware.properties.state : 'OFFLINE';
  const variants: { [state: string]: string } = {
    READY: 'success',
    BLOCKED: 'secondary',
    ACQUIRING: 'warning',
    UNKNOWN: 'fatal',
  };
  const variant = variants[state] || variants.UNKNOWN;
  return (
    <HardwareState.HardwareState state={state} minWidth={6} variant={variant} />
  );
}
