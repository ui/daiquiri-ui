import type { MouseEvent } from 'react';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import { Button, ButtonGroup } from 'react-bootstrap';
import { TomoImagingState } from './State';
import type { TomoImagingSchema } from '.';

function TakeActions(props: {
  hardware: HardwareTypes.EditableHardware<TomoImagingSchema>;
  disabled: boolean;
}) {
  const { hardware } = props;
  const disabled = props.disabled || !hardware.online;

  function takeProj(event: MouseEvent) {
    hardware.actions.call('take_proj');
  }

  function takeDark(event: MouseEvent) {
    hardware.actions.call('take_dark');
  }

  function takeFlat(event: MouseEvent) {
    hardware.actions.call('take_flat');
  }

  return (
    <ButtonGroup size="lg" className="w-100">
      <Button onClick={takeProj} disabled={disabled} variant="primary">
        Proj
      </Button>
      <Button onClick={takeFlat} disabled={disabled} variant="primary">
        Flat
      </Button>
      <Button onClick={takeDark} disabled={disabled} variant="primary">
        Dark
      </Button>
    </ButtonGroup>
  );
}

export default function TomoImagingActions(
  props: HardwareTypes.HardwareWidgetProps<TomoImagingSchema>
) {
  const { hardware, options = {} } = props;

  const widgetIcon = (
    <TypeIcon
      name="TomoImaging"
      icon="fam-hardware-camera"
      online={hardware.online}
    />
  );

  const widgetState = <TomoImagingState hardware={hardware} />;

  const widgetContent = (
    <TakeActions hardware={hardware} disabled={props.disabled} />
  );

  const headerMode = options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
