import { useEffect, useRef, useState } from 'react';
import type { MouseEvent, ChangeEvent, KeyboardEvent } from 'react';

import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import {
  Button,
  ButtonGroup,
  InputGroup,
  Container,
  Row,
  Col,
  Form,
} from 'react-bootstrap';
import { TomoImagingState } from './State';
import type { TomoImagingSchema } from '.';

function UpdateMode(props: {
  hardware: HardwareTypes.EditableHardware<TomoImagingSchema>;
  disabled: boolean;
}) {
  const { hardware } = props;
  const updateOnMove = hardware.properties.update_on_move;
  const disabled = props.disabled || !hardware.online;

  function setUpdateModeOff(event: MouseEvent) {
    hardware.actions.setProperty('update_on_move', false);
  }

  function setUpdateModeAuto(event: MouseEvent) {
    hardware.actions.setProperty('update_on_move', true);
  }

  return (
    <ButtonGroup className="me-2">
      <InputGroup.Text>Update mode</InputGroup.Text>
      <Button
        onClick={setUpdateModeAuto}
        disabled={disabled}
        variant={updateOnMove ? 'primary' : 'secondary'}
      >
        Auto
      </Button>
      <Button
        onClick={setUpdateModeOff}
        disabled={disabled}
        variant={updateOnMove ? 'secondary' : 'primary'}
      >
        Off
      </Button>
    </ButtonGroup>
  );
}

function ExposureTime(props: {
  hardware: HardwareTypes.EditableHardware<TomoImagingSchema>;
  disabled: boolean;
}) {
  const { hardware } = props;
  const exposureTime = hardware.properties.exposure_time || 0;
  const disabled = props.disabled || !hardware.online;

  const formRef = useRef<HTMLInputElement>(null);
  const [edited, setEdited] = useState(false);
  const digits = 2;

  function onKeyDown(e: KeyboardEvent) {
    if (!formRef || !formRef.current) {
      console.log('formRef not yet defined');
    } else if (e.key === 'Enter') {
      setEdited(false);
      hardware.actions.setProperty('exposure_time', formRef.current.value);
    } else if (e.key === 'Escape') {
      formRef.current.value = exposureTime.toFixed(digits);
      setEdited(false);
    } else {
      console.log(e.key);
    }
  }

  function onChange(event: ChangeEvent<HTMLInputElement>) {
    const { value } = event.target;
    if (value === undefined) {
      return;
    }
    const exp = Number.parseFloat(value);
    if (exp !== exposureTime && !edited) {
      setEdited(true);
    }
    if (exp === exposureTime && edited) {
      setEdited(false);
    }
  }

  useEffect(() => {
    if (!formRef || !formRef.current) {
      console.log('formRef not yet defined');
    } else if (!edited) {
      formRef.current.value = exposureTime.toFixed(digits);
    }
  }, [edited, exposureTime]);

  return (
    <InputGroup className="me-2">
      <InputGroup.Text>Exposure time</InputGroup.Text>
      <Form.Control
        className={edited ? 'form-control-edited' : ''}
        type="number"
        ref={formRef}
        defaultValue={exposureTime.toFixed(digits)}
        onChange={onChange}
        disabled={disabled}
        onKeyDown={onKeyDown}
      />
      <InputGroup.Text>s</InputGroup.Text>
    </InputGroup>
  );
}

function TakeActions(props: {
  hardware: HardwareTypes.EditableHardware<TomoImagingSchema>;
  disabled: boolean;
}) {
  const { hardware } = props;
  const disabled = props.disabled || !hardware.online;

  function takeProj(event: MouseEvent) {
    hardware.actions.call('take_proj');
  }

  function takeDark(event: MouseEvent) {
    hardware.actions.call('take_dark');
  }

  function takeFlat(event: MouseEvent) {
    hardware.actions.call('take_flat');
  }

  return (
    <ButtonGroup size="lg" className="me-2">
      <InputGroup.Text>Take</InputGroup.Text>
      <Button onClick={takeProj} disabled={disabled} variant="primary">
        Proj
      </Button>
      <Button onClick={takeFlat} disabled={disabled} variant="primary">
        Flat
      </Button>
      <Button onClick={takeDark} disabled={disabled} variant="primary">
        Dark
      </Button>
    </ButtonGroup>
  );
}

export default function TomoImagingDefault(
  props: HardwareTypes.HardwareWidgetProps<TomoImagingSchema>
) {
  const { hardware, options = {} } = props;

  const widgetIcon = (
    <TypeIcon
      name="TomoImaging"
      icon="fam-hardware-camera"
      online={hardware.online}
    />
  );

  const widgetState = <TomoImagingState hardware={hardware} />;

  const widgetContent = (
    <Container>
      <Row className="mt-1">
        <Col>
          <UpdateMode hardware={hardware} disabled={props.disabled} />
        </Col>
      </Row>
      <Row className="mt-1">
        <Col>
          <ExposureTime hardware={hardware} disabled={props.disabled} />
        </Col>
      </Row>
      <Row className="mt-1">
        <Col>
          <TakeActions hardware={hardware} disabled={props.disabled} />
        </Col>
      </Row>
    </Container>
  );

  const headerMode = options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
