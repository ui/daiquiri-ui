import { useEffect, useRef, useState } from 'react';
import type { ChangeEvent, KeyboardEvent } from 'react';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon, HardwareTemplate } from '@esrf/daiquiri-lib';
import { InputGroup, Form } from 'react-bootstrap';
import { TomoImagingState } from './State';
import type { TomoImagingSchema } from '.';
import { useHardware } from '../../tomo/utils/hooks';
import { guessSubframes, isLimaHardware } from '../lima';
import { isTomoDetectorHardware } from '../tomodetector';

function ExposureTime(props: {
  hardware: HardwareTypes.EditableHardware<TomoImagingSchema>;
  disabled: boolean;
}) {
  const { hardware } = props;
  const exposureTime = hardware.properties.exposure_time || 0;
  const disabled = props.disabled || !hardware.online;

  const formRef = useRef<HTMLInputElement>(null);
  const [edited, setEdited] = useState(false);
  const digits = 3;

  function onKeyDown(e: KeyboardEvent) {
    if (!formRef || !formRef.current) {
      console.log('formRef not yet defined');
    } else if (e.key === 'Enter') {
      setEdited(false);
      hardware.actions.setProperty('exposure_time', formRef.current.value);
    } else if (e.key === 'Escape') {
      formRef.current.value = exposureTime.toFixed(digits);
      setEdited(false);
    } else {
      console.log(e.key);
    }
  }

  function onChange(event: ChangeEvent<HTMLInputElement>) {
    const { value } = event.target;
    if (value === undefined) {
      return;
    }
    const exp = Number.parseFloat(value);
    if (exp !== exposureTime && !edited) {
      setEdited(true);
    }
    if (exp === exposureTime && edited) {
      setEdited(false);
    }
  }

  useEffect(() => {
    if (!formRef || !formRef.current) {
      console.log('formRef not yet defined');
    } else if (!edited) {
      formRef.current.value = exposureTime.toFixed(digits);
    }
  }, [edited, exposureTime]);

  return (
    <InputGroup className="w-100">
      <Form.Control
        className={edited ? 'form-control-edited' : ''}
        type="number"
        ref={formRef}
        defaultValue={exposureTime.toFixed(digits)}
        onChange={onChange}
        disabled={disabled}
        onKeyDown={onKeyDown}
      />
      <InputGroup.Text>s</InputGroup.Text>
    </InputGroup>
  );
}

function AccumulationIfOne(props: { exposureTime: number }) {
  const { exposureTime } = props;
  const hardware = useHardware(
    'ACTIVE_TOMOCONFIG.ref.detectors.active_detector.detector'
  );
  const hardwareTomo = useHardware(
    'ACTIVE_TOMOCONFIG.ref.detectors.active_detector'
  );
  if (hardware === null || hardwareTomo === null) {
    return <></>;
  }
  if (!isLimaHardware(hardware) || !isTomoDetectorHardware(hardwareTomo)) {
    return <></>;
  }
  const accMaxExpoTime = hardware.properties.acc_max_expo_time;
  const acqMode = hardwareTomo.properties.acq_mode;
  if (!accMaxExpoTime || acqMode !== 'ACCUMULATION') {
    return <></>;
  }
  const [nbSubFrames, accExpoTime] = guessSubframes(
    exposureTime,
    accMaxExpoTime
  );
  return (
    <div className="text-center" title="Decompisiton of the accumulation">
      {nbSubFrames} × {accExpoTime.toFixed(3)}
    </div>
  );
}

export default function TomoImagingExposureTime(
  props: HardwareTypes.HardwareWidgetProps<TomoImagingSchema>
) {
  const { hardware, options = {} } = props;

  const widgetIcon = (
    <TypeIcon
      name="TomoImaging"
      icon="fam-hardware-camera"
      online={hardware.online}
    />
  );

  const widgetState = <TomoImagingState hardware={hardware} />;

  const widgetContent = (
    <>
      <ExposureTime hardware={hardware} disabled={props.disabled} />
      <AccumulationIfOne exposureTime={hardware.properties.exposure_time} />
    </>
  );

  const headerMode = options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
