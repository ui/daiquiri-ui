import { HardwareVariant } from '@esrf/daiquiri-lib';
import TomoImagingDefault from './TomoImagingDefault';
import TomoImagingExposureTime from './TomoImagingExposureTime';
import TomoImagingSettleTime from './TomoImagingSettleTime';
import TomoImagingActions from './TomoImagingActions';
import TomoImagingUpdateMode from './TomoImagingUpdateMode';
import type { HardwareTypes } from '@esrf/daiquiri-lib';

/**
 * Hardware as exposed by Redux
 */
export interface TomoImagingSchema extends HardwareTypes.Hardware {
  properties: {
    state: string;
    update_on_move: boolean;
    exposure_time: number;
    settle_time: number;
  };
}

export default class Default extends HardwareVariant {
  public variants = {
    default: TomoImagingDefault,
    exposuretime: TomoImagingExposureTime,
    settletime: TomoImagingSettleTime,
    actions: TomoImagingActions,
    updatemode: TomoImagingUpdateMode,
  };
}
