import { PropsWithChildren, useEffect } from 'react';
import { Container, Col, Row } from 'react-bootstrap';

import MainSidebar from 'connect/main/MainSidebar';
import Layout from 'connect/Layout';

import NotificationsPanel from 'connect/main/NotificationsPanel';

import app from 'providers/app';

import { useParams } from 'react-router-dom';
import SelectSession from '../../connect/SelectSession';

interface Props {
  session: string | undefined;
}

function Main(props: Props) {
  const { session } = props;
  const { layout } = useParams<{ layout?: string }>();

  useEffect(() => {
    if (layout) {
      app.dispatch('SWITCH_LAYOUT_SLUG', layout);
    }
  }, []);

  if (!session) {
    return <SelectSession />;
  }

  return (
    <>
      <NotificationsPanel />
      <MainSidebar />
      <Layout testid="main-layout" />
    </>
  );
}

export default Main;
