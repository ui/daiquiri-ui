import type { Dispatch, SetStateAction } from 'react';
import {
  Button,
  ButtonGroup,
  Col,
  Container,
  Form,
  Row,
  Stack,
} from 'react-bootstrap';
import useUserPreference from '../utils/UseUserPreference';

function EnabledDisabled(props: {
  getSet: [boolean, Dispatch<SetStateAction<boolean>>];
}) {
  const {
    getSet: [enabled, setEnabled],
  } = props;
  return (
    <Form.Check
      type="switch"
      checked={enabled}
      onChange={() => {
        setEnabled((b) => !b);
      }}
    />
  );
}

export default function GlobalOptions(props: { scroll: any }) {
  const getSetToastForScans = useUserPreference('toasts/scans', true);

  return (
    <div style={{ width: '20em' }}>
      <Container>
        <h3>Toasts</h3>
        <div className="mb-2">
          Toasts are temporary message box displayed as an overlay on the
          top-right corner of the screen.
        </div>
        <Row className="fs-5">
          <Col sm={8} className="text-bold">
            Toasts for scans
          </Col>
          <Col sm={4}>
            <EnabledDisabled getSet={getSetToastForScans} />
          </Col>
        </Row>
      </Container>
    </div>
  );
}
