import { Component } from 'react';

import { Nav, Tab } from 'react-bootstrap';

import OffCanvas from 'connect/layout/OffCanvas';
import Sessions from 'connect/Sessions';
import Logging from 'connect/Logging';
import Chat from 'connect/Chat';
import GlobalOptions from './GlobalOptions';

interface Props {
  selectedTab?: string;
  actions: {
    selectTab: (props: { key: string; value: string | null }) => void;
  };
}

export default function NotificationsPanel(props: Props) {
  const selectedTab = props.selectedTab ?? 'sess';

  return (
    <OffCanvas id="offcanvas-right">
      {/* @ts-expect-error */}
      {({ toggle }) => (
        <Tab.Container
          id="left-tabs-example"
          activeKey={selectedTab}
          onSelect={(key) =>
            props.actions.selectTab({ key: 'notification', value: key })
          }
        >
          <OffCanvas.Header id="offcanvas-right">
            <Nav variant="pills">
              <Nav.Item>
                <Nav.Link eventKey="log">Logging</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="sess">Sessions</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="chat">Chat</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="options">Options</Nav.Link>
              </Nav.Item>
            </Nav>
          </OffCanvas.Header>

          <OffCanvas.Content>
            {({ scroll }) => (
              <Tab.Content>
                <Tab.Pane eventKey="log">
                  <Logging />
                </Tab.Pane>
                <Tab.Pane eventKey="sess">
                  <Sessions toggleParent={toggle} />
                </Tab.Pane>
                <Tab.Pane eventKey="chat">
                  <Chat scroll={scroll} />
                </Tab.Pane>
                <Tab.Pane eventKey="options">
                  <GlobalOptions scroll={scroll} />
                </Tab.Pane>
              </Tab.Content>
            )}
          </OffCanvas.Content>
        </Tab.Container>
      )}
    </OffCanvas>
  );
}
