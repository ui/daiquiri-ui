import PropTypes from 'prop-types';

import Sidebar from 'connect/layout/Sidebar';
import Queue from 'connect/Queue';
import SampleList from 'connect/samples/SampleList';
import DCList from 'connect/dc/DCList';
import LayoutSideBar from 'connect/header/LayoutSideBar';
import { Tooltip, Badge } from 'react-bootstrap';
import SidebarButton from '../header/SidebarButton';
import useUserPreference from '../utils/UseUserPreference';

function QueueIndicator(props: {
  queueSize: number;
  queueRunning: boolean;
  queueReady: boolean;
}) {
  function getVariant() {
    if (!props.queueReady) {
      // Anyway a running task, display that the queue is interrupted
      return 'danger';
    }
    return 'success';
  }

  // Display the remaining tasks in the queue
  // The running task is not counted as part of the queue
  const remainingQueueSize = props.queueRunning
    ? props.queueSize - 1
    : props.queueSize;

  const variant = getVariant();

  return (
    <>
      {remainingQueueSize > 0 && (
        <div className="indicator">
          <Badge pill bg={variant}>
            {remainingQueueSize}
          </Badge>
        </div>
      )}
      {remainingQueueSize === 0 && !props.queueReady && (
        <div className="indicator">
          <i className={`fa fa-circle text-${variant}`} />
        </div>
      )}
    </>
  );
}

function QueueButton(props: {
  queueSize: number;
  queueRunning: boolean;
  queueReady: boolean;
}) {
  function getIcons() {
    if (props.queueRunning) {
      return ['fam-cog', 'fa-spin', 'Ready'];
    }
    if (props.queueReady) {
      return ['fam-cog-pause', '', 'Ready'];
    }
    return ['fam-cog-stop', '', 'Stopped'];
  }
  const [icon, animated, status] = getIcons();

  // Display the remaining tasks in the queue
  // The running task is not count as part of the queue
  const remainingQueueSize = props.queueRunning
    ? props.queueSize - 1
    : props.queueSize;

  return (
    <SidebarButton
      indicator={<QueueIndicator {...props} />}
      icon={`${icon} ${animated}`}
      text="Tasks"
      overlay={
        <Tooltip id="tooltip-queue">
          Show the queue
          <br />
          <div>Queue status: {status}</div>
          {props.queueRunning && <div>1 job running</div>}
          {remainingQueueSize === 0 && <div>Queue empty</div>}
          {remainingQueueSize === 1 && <div>1 job queued</div>}
          {remainingQueueSize > 1 && (
            <div>{remainingQueueSize} jobs queued</div>
          )}
        </Tooltip>
      }
    />
  );
}

function SampleButton(props: { currentSampleId?: number }) {
  if (props.currentSampleId === null) {
    return (
      <SidebarButton
        icon="fam-flask fa-fade"
        text="Samples"
        overlay={<Tooltip id="sample">No sample selected</Tooltip>}
      />
    );
  }
  return (
    <SidebarButton
      icon="fam-flask"
      text="Samples"
      overlay={<Tooltip id="sample">Toggle sample list</Tooltip>}
    />
  );
}

export default function MainSidebar(props: {
  currentSampleId?: number;
  queueSize: number;
  queueRunning: boolean;
  queueReady: boolean;
}) {
  const [collapsed, setCollapsed] = useUserPreference(
    'main/sidebar/collapsed',
    true
  );

  function toggleSidebarSize() {
    setCollapsed(!collapsed);
  }
  return (
    <Sidebar collapsed={collapsed}>
      <Sidebar.Inner>
        <div className="sidebar-group">
          <Sidebar.Trigger id="samples">
            <SampleButton {...props} />
          </Sidebar.Trigger>
          <Sidebar.Trigger id="queue">
            <QueueButton {...props} />
          </Sidebar.Trigger>
          <Sidebar.Trigger id="history">
            <SidebarButton
              icon="fa-history"
              text="Collections"
              overlay={
                <Tooltip id="tooltip-history">
                  Toggle data collections list
                </Tooltip>
              }
            />
          </Sidebar.Trigger>
        </div>
        <LayoutSideBar />
        <div className="sidebar-trigger">
          <SidebarButton
            icon={collapsed ? 'fa-angle-double-right' : 'fa-angle-double-left'}
            text="Collapse sidebar"
            onClick={toggleSidebarSize}
            overlay={<Tooltip id="tooltip-sidebar">Toggle sidebar</Tooltip>}
          />
        </div>
      </Sidebar.Inner>

      <Sidebar.Container id="queue">
        <Sidebar.Header>Queue</Sidebar.Header>

        <Sidebar.Content>
          <Queue />
        </Sidebar.Content>
      </Sidebar.Container>

      <Sidebar.Container id="samples">
        <Sidebar.Header>Samples</Sidebar.Header>

        <Sidebar.Content>
          <SampleList />
        </Sidebar.Content>
      </Sidebar.Container>

      <Sidebar.Container id="history">
        <Sidebar.Header>History</Sidebar.Header>

        <Sidebar.Content>
          <DCList
            sample
            pages
            filter
            createScalarMap={false}
            scan={false}
            providers={{
              metadata: {
                datacollections: {
                  pageSize: 15,
                },
              },
            }}
          />
        </Sidebar.Content>
      </Sidebar.Container>
    </Sidebar>
  );
}
