import moment from 'moment';

export default function momentCell(
  cell: number,
  row: any,
  rowIndex: number,
  formatExtraData: { format?: string }
) {
  return cell ? moment.unix(cell).format(formatExtraData.format) : '';
}
