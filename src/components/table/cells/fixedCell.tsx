export default function fixedCell(
  cell: string,
  row: any,
  rowIndex: number,
  formatExtraData: { dp?: number }
) {
  return Number.parseFloat(cell).toFixed(formatExtraData.dp || 0);
}
