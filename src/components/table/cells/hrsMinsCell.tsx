import { Formatting } from '@esrf/daiquiri-lib';

export default function HrsMinsCell(
  cell: number,
  row: any,
  rowIndex: number,
  formatExtraData: any
) {
  return cell ? Formatting.toHoursMins(cell) : '';
}
