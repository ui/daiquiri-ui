export default function rowIndexCell(
  cell: any,
  row: any,
  rowIndex: number,
  formatExtraData: any
) {
  return rowIndex + 1;
}
