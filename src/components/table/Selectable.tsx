import classNames from 'classnames';
import { range, find } from 'lodash';

import Table from 'components/table';
import type { TableRow, SelectableActions, ITable } from './models';
import type { SyntheticEvent } from 'react';

interface ISelectableTable extends ITable {
  /* Allow selecting items with the same `type` defined by this field */
  typeKey?: string;
  /* Allow multiple selection */
  multiple?: boolean;
  selectedItems?: number[];
  actions: SelectableActions;
}

function SelectableTable(props: ISelectableTable) {
  const rowClasses = (row: TableRow, rowIndex: number) => {
    return classNames('pointer', {
      // Follow mode => selection = null
      'table-warning':
        props.selectedItems === null &&
        props.data.indexOf(row) === 0 &&
        props.page === 1,
      // Multiple selection - current item
      'table-info font-weight-bold':
        props.selectedItems &&
        props.selectedItems.length > 0 &&
        props.selectedItems.length > 1 &&
        row[props.keyField] === props.selectedItems.slice(-1)[0],
      // Other items
      'table-info': props.selectedItems?.includes(row[props.keyField]),
    });
  };

  const onRowClick = (e: SyntheticEvent, row: TableRow, rowIndex: number) => {
    if ((e.target as HTMLElement)?.tagName !== 'TD') return;

    const currentId = props.selectedItems?.slice(-1)[0];
    const current = find(props.data, { [props.keyField]: currentId });

    // Multiselect
    // @ts-expect-error
    if (e.shiftKey && props.multiple && props.selectedItems) {
      // Shift modifier for selecting a range
      props.actions.resetSelection();

      const selectedIndex = current && props.data.indexOf(current);
      let start = selectedIndex || 0;
      let end = rowIndex;

      if (end < start) {
        const oldStart = start;
        start = end;
        end = oldStart;
      }

      range(start, end + 1).forEach((rid) => {
        let allowAdd = true;
        const item = props.data[rid];
        // If typeKey is specified only allow selection of same type of objects
        if (props.typeKey) {
          if (current?.[props.typeKey] !== item.type) allowAdd = false;
        }

        if (allowAdd) {
          props.actions.addSelection({
            [props.keyField]: item[props.keyField],
            index: rid,
          });
        }
      });
    } else if (
      // @ts-expect-error
      (e.ctrlKey || e.metaKey) &&
      props.multiple &&
      props.selectedItems
    ) {
      // Ctrl modifier to add/remove to/from selection
      if (props.selectedItems.includes(row[props.keyField])) {
        props.actions.removeSelection(row[props.keyField]);
      } else {
        let allowAdd = true;
        if (props.typeKey) {
          if (current?.[props.typeKey] !== row.type) allowAdd = false;
        }

        if (allowAdd) {
          props.actions.addSelection({
            [props.keyField]: row[props.keyField],
          });
        }
      }
    } else {
      // Select a single item and reset any existing multiple selection
      props.actions.resetSelection();
      props.actions.addSelection({
        [props.keyField]: row[props.keyField],
      });
    }
  };

  const { typeKey, page, selectedItems, actions, multiple, ...rest } = props;
  return (
    <Table
      rowClasses={rowClasses}
      rowEvents={{ onClick: onRowClick }}
      {...rest}
    />
  );
}

export default SelectableTable;
