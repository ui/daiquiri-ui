import type { ColumnDescription } from 'react-bootstrap-table-next';

export interface PagedType {
  fetching: boolean;
  page: number;
  pages: number;
  per_page: number;
  total: number;
}

export interface PagedActions {
  fetch: () => Promise<void>;
  setPage: (pageNumber: number) => Promise<void>;
  setPageSize: (pageSize: number) => Promise<void>;
  setParams?: (params: any, b: boolean) => Promise<void>;
}

export interface SelectableActions {
  addSelection: (selection: Record<string, number>) => void;
  removeSelection: (selection: number) => void;
  resetSelection: () => void;
}

export interface TablePages {
  page: number;
  per_page: number;
  total: number;
  fetch?: () => Promise<void>;
  setPage?: (pageNumber: number) => Promise<void>;
  setPageSize?: (pageSize: number) => Promise<void>;
  setParams?: (params: any, b: boolean) => Promise<void>;
}

export type TableRow = Record<any, any>;

export type TableColumn = ColumnDescription;

export interface ITable {
  data: TableRow[];
  page?: number;
  pages?: TablePages;
  columns: ColumnDescription[];
  keyField: string;
  noDataIndication?: string;
  bordered?: boolean;
  classes?: string;
  loading?: boolean;
  overlay?: boolean;
}
