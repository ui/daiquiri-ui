import { useCallback } from 'react';
import LoadingOverlay from 'components/layout/LoadingOverlay';
import type {
  ColumnDescription,
  RowEventHandlerProps,
  SelectRowProps,
  TableChangeState,
} from 'react-bootstrap-table-next';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';

import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import type { TablePages } from './models';

export default function Table(props: {
  keyField: string;
  data: Record<any, any>[];
  columns: ColumnDescription[];
  pages?: TablePages;
  overlay?: boolean;
  loading?: boolean;
  classes?: string;
  rowEvents?: RowEventHandlerProps<any>;
  rowClasses?: ((row: any, rowIndex: number) => string) | string | undefined;
  localPerPage?: number;
  noDataIndication?: string;
  className?: string;
  hover?: boolean;
  selectRow?: SelectRowProps<any>;
}) {
  const {
    keyField,
    data,
    columns,
    pages = undefined,
    overlay = false,
    loading = false,
    classes = '',
    rowEvents,
    rowClasses,
    localPerPage = undefined,
    ...rest
  } = props;

  const onTableChange = useCallback(
    (type: string, newState: TableChangeState<any>) => {
      if (!pages) return;
      console.log('onTableChange', type, newState);

      if (type === 'pagination') {
        pages.setPage?.(newState.page);
        pages.setPageSize?.(newState.sizePerPage);
        pages.fetch?.();
      }

      if (type === 'sort') {
        pages.setParams?.(
          {
            order: newState.sortOrder,
            order_by: newState.sortField,
          },
          true
        );
      }
    },
    [pages]
  );

  function getPagination() {
    if (localPerPage) {
      return paginationFactory({
        sizePerPageList: [5, 10, 15, 25, 50, 100],
        page: 1,
        sizePerPage: localPerPage,
        totalSize: data.length,
      });
    }
    if (pages) {
      return paginationFactory({
        sizePerPageList: [5, 10, 15, 25, 50, 100],
        page: pages.page,
        sizePerPage: pages.per_page,
        totalSize: pages.total,
      });
    }
    return undefined;
  }

  const pagination = getPagination();

  return (
    <LoadingOverlay active={props.loading} enabled={props.overlay}>
      <BootstrapTable
        data={data}
        columns={columns}
        keyField={keyField}
        bootstrap4
        bordered={false}
        classes={`table-sm noselect ${classes}`}
        rowEvents={rowEvents}
        rowClasses={rowClasses}
        striped
        pagination={pagination}
        remote={!!props.pages}
        onTableChange={onTableChange}
        {...rest}
      />
    </LoadingOverlay>
  );
}
