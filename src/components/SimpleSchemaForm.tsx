import type { ReactChild } from 'react';
import { Component } from 'react';
import { each } from 'lodash';
import { Button } from 'react-bootstrap';

import type { ErrorSchema, Field, IChangeEvent, Widget } from '@rjsf/core';
import type { JSONSchema7 } from 'json-schema';
import Form from '@rjsf/core';
import { stripWithWhitelist } from 'components/SchemaForm';

import { colFieldFactory } from 'components/form-widgets/ColField';
import CustomCheckbox from 'components/form-widgets/CustomCheckbox';
import ConnectedSelect from 'components/form-widgets/ConnectedSelect';

const WIDGETS: Record<string, Widget> = {
  CheckboxWidget: CustomCheckbox,
  connectedSelect: ConnectedSelect,
};

interface Props {
  schema: Record<string, any>;
  uiSchema?: Record<string, any>;
  formData?: Record<string, any>;
  submitText?: string | ReactChild;
  onChange?: (formData: Record<string, any>) => void;
  onSubmit?: (formData: Record<string, any>) => void;
  colField?: boolean;
  colWidth?: number;
  disabled?: boolean;
  button?: boolean;
  className?: string;
  fields?: { [name: string]: Field };
  formContext?: any;
}

export default class SimpleSchemaForm extends Component<Props> {
  public FieldTemplate: ((props: any) => JSX.Element) | undefined;

  public constructor(props: Props) {
    super(props);
    const { colField, colWidth } = props;

    // https://github.com/rjsf-team/react-jsonschema-form/issues/2106
    this.FieldTemplate = colField ? colFieldFactory({ colWidth }) : undefined;
  }

  public onChange = (
    e: IChangeEvent<Record<string, any>>,
    es?: ErrorSchema
  ) => {
    const formData = JSON.parse(JSON.stringify(e.formData));
    if (this.props.onChange) {
      this.props.onChange(formData);
    }
  };

  public onSubmit = (props: { formData: Record<string, any> }) => {
    const { formData } = props;
    if (this.props.onSubmit) {
      this.props.onSubmit(formData);
    }
  };

  public render() {
    const {
      schema,
      uiSchema = {},
      formData = {},
      submitText = 'Submit',
    } = this.props;

    const sc: Record<string, any> = {};
    each(schema, (s, k) => {
      sc[k] = stripWithWhitelist(s);
    });

    const sch: JSONSchema7 = {
      $ref: '#/definitions/SimpleSchema',
      definitions: {
        SimpleSchema: {
          properties: sc,
          type: 'object',
          additionalProperties: false,
        },
      },
    };

    const {
      button = true,
      className,
      disabled = false,
      fields,
      formContext,
    } = this.props;

    return (
      <Form
        className={className}
        schema={sch}
        uiSchema={uiSchema}
        liveValidate
        omitExtraData
        showErrorList={false}
        formData={formData}
        widgets={WIDGETS}
        onChange={this.onChange}
        onSubmit={this.onSubmit}
        disabled={disabled}
        fields={fields}
        formContext={formContext}
        FieldTemplate={this.FieldTemplate}
      >
        {button ? <Button type="submit">{submitText}</Button> : <></>}
      </Form>
    );
  }
}
