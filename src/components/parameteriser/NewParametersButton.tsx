import React, { useRef, useCallback } from 'react';

import { Button } from 'react-bootstrap';
import NewParameters from 'connect/parameteriser/NewParameters';
import ButtonTriggerModalStore from 'connect/layout/ButtonTriggerModalStore';
import type { ButtonTriggerModalStoreRef } from '../layout/models';

interface Props {
  config?: {
    parametertypes: Record<string, { actor: string }[]>;
  };
  ptype: string;
  button?: JSX.Element;
  buttonProps?: Record<string, any>;
  formData?: Record<string, any>;
  disabled?: boolean;
  inset?: boolean;
  prefix?: string;
  onSubmit?: () => void;
  schema?: string;
  overwrite?: boolean;
  title?: string;
}

function NewParametersButton(props: Props) {
  const onSubmit = useRef(() => {});
  const disabledRef = useRef(false);
  const modalRef = useRef<ButtonTriggerModalStoreRef>();

  const doOnSubmit = useCallback(() => {
    modalRef.current?.close();
    if (props.onSubmit) props.onSubmit();
  }, [modalRef, props.onSubmit]);

  return (
    <ButtonTriggerModalStore
      ref={modalRef}
      id={`newParameters${props.ptype}${props.prefix ?? ''}`}
      button={
        props.button || (
          <>
            <i className="fa fa-plus me-1" />
            New
          </>
        )
      }
      buttonProps={{
        disabled: props.disabled,
        className: props.inset ? '' : 'float-end mb-1',
        ...props.buttonProps,
      }}
      buttons={
        <>
          <Button
            onClick={() => onSubmit.current()}
            disabled={disabledRef.current}
          >
            Save
          </Button>
        </>
      }
      title={`${props.title || 'New'} ${props.ptype}`}
    >
      <NewParameters
        onSubmit={doOnSubmit}
        // @ts-expect-error  This is feed by the connect, so there is probably the problem here
        config={props.config}
        formData={{ ...props.formData }}
        ptype={props.ptype}
        initialSchema={props.schema}
        overwrite={props.overwrite}
        button={(d, os) => {
          disabledRef.current = d;
          onSubmit.current = os;
        }}
      />
    </ButtonTriggerModalStore>
  );
}

export default NewParametersButton;
