import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { FullSizer } from '@esrf/daiquiri-lib';

import Table from 'components/table';
import RemountOnResize from 'components/utils/RemountOnResize';

import NewParametersButton from 'components/parameteriser/NewParametersButton';

function ActionCell(
  cell: Record<string, any>,
  row: Record<string, any>,
  rowIndex: number,
  formatExtraData: Record<string, any>
) {
  return (
    <NewParametersButton
      ptype={formatExtraData.ptype}
      onSubmit={formatExtraData.onSubmit}
      formData={{
        ...row.parameters,
      }}
      overwrite
      schema={row.instance_type}
      button={<i className="fa fa-search" />}
      buttonProps={{ size: 'sm', variant: 'info' }}
      inset
      title="Edit"
      prefix={row.name}
    />
  );
}

interface Props {
  parameters: Record<string, any>[];
  pnamespace?: string;
  actions: {
    setParams: (params: Record<string, any>, fetch?: boolean) => Promise<void>;
    fetch: () => Promise<void>;
    setPageSize: (pageSize: number) => Promise<void>;
    setPage: (pageId: number) => Promise<void>;
  };
  type: string;
  total: number;
  per_page: number;
  page: number;
  fetching?: boolean;
}

function ParametersList(props: Props) {
  useEffect(() => {
    props.actions.setParams({ type: props.type }, true);
  }, []);

  const onSubmit = () => {
    props.actions.fetch();
  };

  const columns = [
    { dataField: 'name', text: 'Name' },
    { dataField: 'instance_type', text: 'Instance' },
    { dataField: 'file_name', text: 'File' },
    {
      dataField: 'action',
      text: '',
      formatter: ActionCell,
      formatExtraData: {
        onSubmit,
        ptype: props.type,
      },
      classes: 'text-end text-nowrap',
    },
  ];

  return (
    <RemountOnResize>
      <FullSizer>
        <div className="object">
          <h5>
            {props.type}
            <NewParametersButton ptype={props.type} onSubmit={onSubmit} />
          </h5>

          <Table
            keyField="name"
            data={props.parameters}
            columns={columns}
            noDataIndication="No parameters defined"
            pages={{
              page: props.page,
              per_page: props.per_page,
              total: props.total,
              setPage: props.actions.setPage,
              setPageSize: props.actions.setPageSize,
              setParams: props.actions.setParams,
              fetch: props.actions.fetch,
            }}
            loading={props.fetching}
            overlay
          />
        </div>
      </FullSizer>
    </RemountOnResize>
  );
}

ParametersList.defaultProps = {
  pnamespace: '',
};

ParametersList.propTypes = {
  parameters: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  pnamespace: PropTypes.string,
  actions: PropTypes.shape({
    setParams: PropTypes.func.isRequired,
  }).isRequired,
};

export default ParametersList;
