import React, { Component } from 'react';
import { map } from 'lodash';
import { Form, Row, Col } from 'react-bootstrap';
import { Formatting } from '@esrf/daiquiri-lib';
import SchemaForm from 'connect/SchemaForm';

interface Props {
  config?: {
    parametertypes: Record<string, { actor: string }[]>;
  };
  onSubmit?: () => void;
  actions: {
    setSchema: (schema: string) => void;
    setSchemaType: (type: string | undefined) => void;
  };
  schema?: string;
  initialSchema?: string;
  formData?: Record<string, any>;
  ptype?: string;
  button?: (isDisabled: boolean, doOnSubmit: () => void) => void;
  overwrite?: boolean;
}

interface State {
  asyncAdditional: Record<string, any>;
  type: string | undefined;
}

export default class NewParameters extends Component<Props, State> {
  private readonly selectRef: React.RefObject<HTMLSelectElement>;
  private readonly scans: any[];

  public constructor(props: Props) {
    super(props);

    console.log('parameteriser', this.props);
    const ptype = this.props.ptype ?? '';
    this.scans =
      this.props.config && ptype in this.props.config.parametertypes
        ? this.props.config.parametertypes[ptype]
        : [];

    this.state = {
      asyncAdditional: {},
      type: this.props.ptype,
    };

    this.selectRef = React.createRef();
  }

  public componentDidMount() {
    if (this.props.ptype !== this.state.type) {
      this.props.actions.setSchemaType(this.state.type);
      this.changeSchema();
    }

    if (this.props.initialSchema) {
      if (this.selectRef.current) {
        this.selectRef.current.value = this.props.initialSchema;
      }
      this.changeSchema();
    }
  }

  protected changeSchema = () => {
    // rjsf seems to do some internal caching of the definitions
    // part of the schema, passing a changed schema does not clear this
    // cached value, need to force a component remount on changing
    // schemas
    const newValue = this.selectRef?.current?.value;
    this.props.actions.setSchema('');
    if (newValue) {
      setTimeout(() => this.props.actions.setSchema(newValue), 50);
    }
  };

  protected onSubmit = (
    formData: Record<string, any>,
    e: any,
    doSubmit: (formData: Record<string, any>) => Promise<void>
  ) => {
    return doSubmit({ ...formData, overwrite: this.props.overwrite }).then(
      () => {
        if (this.props.onSubmit) this.props.onSubmit();
      }
    );
  };

  protected onAsyncValidate = (data: { additionalAsync: any }) => {
    this.setState({ asyncAdditional: data.additionalAsync });
  };

  public render() {
    const opts = map(this.scans, (s) => (
      <option key={s.actor} value={s.actor}>
        {s.actor}
      </option>
    ));

    const { schema = '' } = this.props;

    return (
      <>
        <Form>
          <Form.Group as={Row}>
            <Form.Label column sm={3}>
              Type
            </Form.Label>
            <Col>
              <Form.Control
                ref={this.selectRef}
                as="select"
                value={this.props.schema}
                onChange={this.changeSchema}
                disabled={this.props.overwrite}
              >
                {opts}
              </Form.Control>
            </Col>
          </Form.Group>
        </Form>

        <hr />

        {this.props.schema === '' ? null : (
          <>
            <SchemaForm
              schema={`${Formatting.ucfirst(schema)}Schema`}
              formData={{ ...this.props.formData }}
              onSubmit={this.onSubmit}
              onAsyncValidate={this.onAsyncValidate}
              submitText="Save"
              button={this.props.button}
            />
          </>
        )}
      </>
    );
  }
}
