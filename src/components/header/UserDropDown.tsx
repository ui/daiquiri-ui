import { Component } from 'react';

import { NavDropdown, Badge } from 'react-bootstrap';

import Avatar from 'components/header/Avatar';

interface Props {
  actions: {
    logout: () => void;
    toggleModal: (id: string) => void;
  };
  user: {
    givenname: string;
    fullname: string;
    is_staff: boolean;
  };
  session?: Record<any, any>;
  show: boolean;
}

export default class UserDropDown extends Component<Props> {
  public readonly logout = () => {
    this.props.actions.logout();
  };

  public readonly showDropdown = (isOpen: boolean) => {
    if (this.props.show !== isOpen) {
      this.props.actions.toggleModal('userdropdown');
    }
  };

  public render() {
    return (
      <NavDropdown
        title={<Avatar user={this.props.user} />}
        id="user-dropdown"
        align="end"
        onToggle={this.showDropdown}
        show={this.props.show}
        className="me-2"
      >
        <NavDropdown.Header>
          {this.props.user.fullname}
          {this.props.user.is_staff && (
            <Badge bg="primary" className="ms-1">
              Staff
            </Badge>
          )}
        </NavDropdown.Header>
        {this.props.session !== undefined && (
          <NavDropdown.Header>
            &raquo;
            {this.props.session.session}
          </NavDropdown.Header>
        )}

        <NavDropdown.Item onClick={this.logout}>Logout</NavDropdown.Item>
      </NavDropdown>
    );
  }
}
