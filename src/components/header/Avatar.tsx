import { map } from 'lodash';

interface Props {
  user: {
    givenname: string;
    fullname: string;
  };
}

export default function Avatar(props: Props) {
  const words = props.user.fullname.split(' ');
  const initials = map(words, (w) => w[0].toUpperCase());

  return (
    <div className="avatar">
      <span className="text">
        Hi,
        {props.user.givenname}
      </span>
      <span className="icon">{initials}</span>
    </div>
  );
}
