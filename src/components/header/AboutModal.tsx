import { Component } from 'react';
import { map, values } from 'lodash';

import { Modal, Container, Row, Col, Button, Badge } from 'react-bootstrap';

import Confirm from 'helpers/Confirm';

interface Props {
  version: {
    version: string;
    libraries: Record<string, any>;
  };
  uiVersion: {
    current?: string;
    available?: string;
    fetched?: boolean;
    fetching?: boolean;
  };
  fetched: boolean;
  fetching: boolean;
  actions: {
    fetchVersion: () => void;
    fetchUiVersion: () => Promise<void>;
    toggleModal: (id: string) => void;
    addAlert: (args: {
      message: JSX.Element;
      type: string;
      id: string;
    }) => void;
    reloadServer: () => void;
  };
  show?: boolean;
  user: {
    is_staff: boolean;
  };
}

export default class AboutModal extends Component<Props> {
  protected checkPromise: NodeJS.Timeout | undefined;
  protected pollVersion: boolean;

  public constructor(props: Props) {
    super(props);
    this.checkPromise = undefined;
    this.pollVersion = true;
  }

  public componentDidMount() {
    this.checkUiVersion();
  }

  public componentDidUpdate(prevProps: Props) {
    if (this.props.show && !prevProps.show) {
      this.props.actions.fetchVersion();
      this.checkUiVersion();
    }

    if (
      this.props.uiVersion.current &&
      this.props.uiVersion.available &&
      this.props.uiVersion.available !== this.props.uiVersion.current
    ) {
      this.props.actions.addAlert({
        message: (
          <span>
            A new version of Daiquiri UI is available. Reload
            <Button size="sm" className="text-end ms-1" onClick={this.reload}>
              <i className="fa fa-refresh" />
            </Button>
          </span>
        ),
        type: 'success',
        id: 'update',
      });
    }
  }

  public componentWillUnmount() {
    this.pollVersion = false;
    if (this.checkPromise) {
      clearTimeout(this.checkPromise);
      this.checkPromise = undefined;
    }
  }

  protected hideModal = () => {
    this.props.actions.toggleModal('about');
  };

  protected reload = () => {
    window.location.reload();
  };

  protected checkUiVersion() {
    if (this.checkPromise) {
      clearTimeout(this.checkPromise);
      this.checkPromise = undefined;
    }

    this.props.actions.fetchUiVersion().then(() => {
      if (this.pollVersion) {
        this.checkPromise = setTimeout(
          this.checkUiVersion.bind(this),
          60 * 1000
        );
      }
    });
  }

  public render() {
    const { version, fetched, fetching, uiVersion, ...rest } = this.props;
    return (
      <Modal
        {...rest}
        onHide={this.hideModal}
        size="lg"
        aria-labelledby="about-modal"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="about-modal">About</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="loader-wrap">
            <div className="mx-auto mb-3 logo logo-150" />
            <p className="text-center">Daiquiri UI</p>
          </div>
          <p className="text-center">
            A web based framework for beamline control and data acquisition
            <br />
            <a href="https://gitlab.esrf.fr/ui/daiquiri-ui">
              https://gitlab.esrf.fr/ui/daiquiri-ui
            </a>
            <br />
            &copy; 2019 - {new Date().getFullYear()}{' '}
            <a href="https://www.esrf.eu">ESRF</a>
          </p>
          <h5>Version Information</h5>
          <Container>
            <Row>
              <Col>Daiquiri UI</Col>
              <Col>
                <span>{uiVersion.current}</span>
                {uiVersion.fetching === true && (
                  <Badge bg="info">Checking...</Badge>
                )}
                {!uiVersion.fetching && uiVersion.fetched === true && (
                  <>
                    {uiVersion.available !== uiVersion.current && (
                      <>
                        <Badge bg="success" className="ms-1">
                          Update: {uiVersion.available}
                        </Badge>
                        <Button
                          size="sm"
                          className="ms-1"
                          onClick={this.reload}
                        >
                          <i className="fa fa-refresh" />
                        </Button>
                      </>
                    )}
                  </>
                )}
              </Col>
            </Row>
            <Row>
              <Col>Daiquiri Server</Col>
              <Col>
                {this.props.fetching && <Badge bg="info">Checking...</Badge>}
                {this.props.fetched && <span>{version.version}</span>}
                {this.props.user.is_staff && (
                  <Confirm
                    title="Reload Server Config"
                    message="Are you sure you want to reload the server configuration?"
                  >
                    {(confirm) => (
                      <Button
                        className="ms-1"
                        size="sm"
                        onClick={confirm(() =>
                          this.props.actions.reloadServer()
                        )}
                      >
                        <i className="fa fa-refresh" /> Reload Config
                      </Button>
                    )}
                  </Confirm>
                )}
              </Col>
            </Row>
          </Container>
          {this.props.fetched && version.libraries.size > 0 && (
            <>
              <h5>Libraries</h5>
              <Container>
                {map(version.libraries, (libVersion, library) => (
                  <Row>
                    <Col>{library}</Col>
                    <Col>{libVersion}</Col>
                  </Row>
                ))}
              </Container>
            </>
          )}
        </Modal.Body>

        <Modal.Footer>
          <Button onClick={this.hideModal}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
