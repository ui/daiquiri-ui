import { useEffect } from 'react';

import AboutModal from 'connect/header/AboutModal';
import UserDropDown from 'connect/header/UserDropDown';
import LayoutDropDown from 'connect/header/LayoutDropDown';
import AlertsDropDown from 'connect/header/AlertsDropDown';
import OffCanvas from 'connect/layout/OffCanvas';
import MonitorPanel from 'connect/monitor/MonitorPanel';
import ControlButton from 'connect/ControlButton';

import { Button, Navbar } from 'react-bootstrap';

interface Props {
  actions: {
    toggleModal: (id: string) => void;
  };
  isLoggedIn: boolean;
  session: string | undefined;
  beamline: string;
  headerColor?: string;
  headerTitle?: string;
  layout: { name: string } | undefined;
  isAboutModalVisible: boolean;
}

function Header(props: Props) {
  const {
    actions,
    isLoggedIn,
    session,
    beamline,
    headerColor,
    headerTitle,
    layout,
    isAboutModalVisible,
  } = props;

  const title = `${headerTitle ?? beamline}`;

  useEffect(() => {
    document.title = `${title}${layout ? ` » ${layout.name}` : ''}`;
  }, [layout]);

  function about() {
    actions.toggleModal('about');
  }

  return (
    <Navbar
      bg="brand"
      variant="dark"
      className="shadow"
      style={{ backgroundColor: headerColor }}
    >
      <Navbar.Brand className="me-auto">
        <Button
          onClick={about}
          variant="light"
          size="sm"
          title="About Daiquiri"
          className="about"
        >
          <i className="fad fam-daiquiri-alt fa-2x" />
        </Button>
        {title}
      </Navbar.Brand>
      {isLoggedIn && (
        <>
          {session && <MonitorPanel />}
          <UserDropDown />
          {session && (
            <>
              <AlertsDropDown />
              <ControlButton />
              <LayoutDropDown />
              <OffCanvas.Trigger id="offcanvas-right" />
            </>
          )}
        </>
      )}
      <AboutModal show={isAboutModalVisible} />
    </Navbar>
  );
}

export default Header;
