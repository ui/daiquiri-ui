import { useCallback } from 'react';
import { map, debounce, MemoVoidIteratorCapped } from 'lodash';
import classNames from 'classnames';

import { NavDropdown, Button } from 'react-bootstrap';
import Tooltip from 'components/common/Tooltip';
import { useNavigate } from 'react-router-dom';

interface Props {
  layouts: Record<number, { acronym: string; name: string }>;
  current: number;
  show: boolean;
  fetching: boolean;
  user: { is_staff?: boolean };
  actions: {
    changeLayout(lid: number): void;
    fetchLayouts(): void;
    toggleModal(id: string): void;
  };
}

function LayoutDropDown(props: Props) {
  const { show, layouts, actions } = props;
  const navigate = useNavigate();

  function changeLayout(lid: number) {
    actions.changeLayout(lid);
    const layout = layouts[lid];
    navigate(lid > 0 ? `/${layout.acronym.toLowerCase()}` : '/');
  }

  function refresh() {
    actions.fetchLayouts();
  }

  const onToggle = useCallback(
    debounce((isOpen) => {
      if (show !== isOpen) actions.toggleModal('layoutdropdown');
    }, 100),
    [show]
  );

  return (
    <NavDropdown
      className="dropdown-button me-2"
      title={<i className="fa fa-th-large" />}
      id="layout-dropdown"
      align="end"
      onToggle={onToggle}
      show={props.show}
    >
      <NavDropdown.Header className="d-flex">
        <div className="mt-auto mb-auto">Select Layout</div>
        {props.user.is_staff && (
          <Tooltip tooltip="Reload layout">
            <Button size="sm" onClick={refresh} className="ms-auto">
              <i
                className={classNames('fa', 'fa-refresh', {
                  'fa-spin': props.fetching,
                })}
              />
            </Button>
          </Tooltip>
        )}
      </NavDropdown.Header>
      {Object.entries(layouts).map(([id, { name }]) => (
        <NavDropdown.Item
          key={id}
          onClick={() => changeLayout(Number.parseInt(id, 10))}
          disabled={Number.parseInt(id, 10) === props.current}
        >
          {name}
        </NavDropdown.Item>
      ))}
    </NavDropdown>
  );
}

export default LayoutDropDown;
