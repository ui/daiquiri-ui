import { OverlayTrigger, Tooltip } from 'react-bootstrap';

interface Props {
  onClick?: () => void;
  text: string;
  icon: string;
  disabled?: boolean;
  overlay?: JSX.Element;
  indicator?: JSX.Element;
}

export default function SidebarButton(props: Props) {
  return (
    <OverlayTrigger
      placement="right"
      overlay={props.overlay || <Tooltip id="toolbar1">{props.text}</Tooltip>}
    >
      <button
        className="sidebar-button"
        disabled={props.disabled}
        type="button"
        onClick={() => {
          if (props.onClick) props.onClick();
        }}
      >
        <span className="sidebar-icon">
          <i className={`fa ${props.icon} fa-2x fa-fw`} />
          {props.indicator}
        </span>
        <span className="sidebar-label">{props.text}</span>
      </button>
    </OverlayTrigger>
  );
}
