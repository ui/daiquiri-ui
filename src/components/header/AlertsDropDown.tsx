import { useEffect, useState } from 'react';
import { useDebouncedCallback } from '@react-hookz/web';
import Alerts from 'connect/Alerts';
import { Container, NavDropdown, Button, Badge } from 'react-bootstrap';
import type { ScanSchema } from 'types/Scan';

interface Props {
  alerts?: Record<string, any>[];
  runningScan?: ScanSchema;
  networkError?: boolean;
  show: boolean;
  actions: {
    toggleModal: (id: string) => void;
    showModal: () => void;
    hideModal: (id: string) => void;
  };
}

function AlertsDropDown(props: Props) {
  const { alerts = [] } = props;
  const onToggle = useDebouncedCallback(
    (isOpen) => {
      if (props.show !== isOpen) props.actions.toggleModal('alertsdropdown');
    },
    [(props.actions.toggleModal, props.show)],
    100
  );

  const [alertClass, setAlertClass] = useState('');
  useEffect(() => {
    if (props.networkError) setAlertClass('alerts-danger');
    else if (props.runningScan) setAlertClass('alerts-info');
    else if (alerts.length > 0) setAlertClass('alerts-warning');
    else setAlertClass('');
  }, [props.runningScan, props.networkError, alerts, props.actions]);

  const alertCount =
    alerts.length + (props.runningScan ? 1 : 0) + (props.networkError ? 1 : 0);

  return (
    <NavDropdown
      className={`dropdown-button me-2 ${alertClass} alerts`}
      title={
        <>
          {alertCount > 0 && (
            <Badge bg="success" className="me-1 unread-messages">
              {alertCount}
            </Badge>
          )}
          <i className="fa fa-bell" />
        </>
      }
      id="user-dropdown"
      align="end"
      onToggle={onToggle}
      show={props.show}
    >
      <Container>
        <h5 className="mt-2">Alerts</h5>
        <Button
          size="sm"
          variant="secondary"
          className="d-block ms-auto mt-2"
          onClick={() => props.actions.hideModal('alertsdropdown')}
        >
          <i className="fa fa-times" />
        </Button>
        <Alerts />
      </Container>
    </NavDropdown>
  );
}

export default AlertsDropDown;
