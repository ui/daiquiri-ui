import { Tooltip } from 'react-bootstrap';
import SidebarButton from './SidebarButton';
import { useNavigate } from 'react-router-dom';

interface Props {
  layouts: Record<
    number,
    { name: string; acronym: string; icon?: string; insidebar?: boolean }
  >;
  current: number;
  fetching: boolean;
  user: { is_staff?: boolean };
  actions: {
    changeLayout: (id: number) => void;
    fetchLayouts: () => void;
  };
}

export default function LayoutSideBar(props: Props) {
  const { layouts, current, actions } = props;
  const navigate = useNavigate();

  function changeLayout(lid: number) {
    actions.changeLayout(lid);
    const layout = layouts[lid];
    navigate(lid > 0 ? `/${layout.acronym.toLowerCase()}` : '/');
  }

  const displayedLayoutEntries = Object.entries(layouts).filter(
    ([, { insidebar }]) => insidebar
  );

  return (
    <div className="sidebar-group">
      {displayedLayoutEntries.map(([id, layout]) => {
        const { name, icon } = layout;
        const lid = Number.parseInt(id, 10);

        return (
          <div
            key={id}
            className={`sidebar-trigger ${lid === current ? 'active' : ''}`}
          >
            <SidebarButton
              text={name}
              icon={icon || 'fam-place-holder'}
              overlay={<Tooltip id="sidebar-layout-tooltip">{name}</Tooltip>}
              onClick={() => changeLayout(lid)}
            />
          </div>
        );
      })}
    </div>
  );
}
