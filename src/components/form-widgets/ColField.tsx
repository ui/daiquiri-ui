import type { FieldTemplateProps } from '@rjsf/core';
// @ts-expect-error
import { ADDITIONAL_PROPERTY_FLAG } from '@rjsf/core/dist/es/utils';
import { Fragment } from 'react';
import type { PropsWithChildren } from 'react';
import { Form, Col, Row, InputGroup, Button } from 'react-bootstrap';
import type { JSONSchema7 } from 'json-schema';

import Tooltip from 'components/common/Tooltip';

interface ExtendedJSONSchema extends JSONSchema7 {
  unit?: string;
  tooltip?: string;
}

interface Props extends Omit<FieldTemplateProps, 'schema'> {
  noLabel?: boolean;
  colWidth?: number;
  schema: ExtendedJSONSchema;
}

export function colFieldFactory(baseProps: Partial<PropsWithChildren<Props>>) {
  function create(props: Partial<PropsWithChildren<Props>>) {
    // @ts-expect-error
    return <ColField {...props} {...baseProps} />;
  }
  return create;
}

function LabelInput(props: any) {
  const { id, label, onChange, sm } = props;
  return (
    <Col sm={sm} style={{ display: 'flex' }}>
      <input
        className="form-control"
        type="text"
        id={id}
        onBlur={(event) => onChange(event.target.value)}
        defaultValue={label}
      />
    </Col>
  );
}

export default function ColField(props: PropsWithChildren<Props>) {
  const {
    id,
    classNames,
    label,
    help,
    required,
    description,
    errors,
    children,
    schema,
    disabled,
    readonly,
    onKeyChange,
    onDropPropertyClick,
  } = props;

  // root is treated differently, undocumented :/
  if (id === 'root') {
    return (
      <div>
        <legend id={id}>{description}</legend>
        {children}
      </div>
    );
  }

  const displayLabel = props.noLabel ? false : props.displayLabel;

  const Wrap = displayLabel ? Col : Fragment;
  const wrapArgs: Record<string, any> = {};

  const colWidth = props.colWidth || 3;
  if (displayLabel) {
    wrapArgs.sm = 12 - colWidth;
  }

  const unit = props.schema.unit;

  const InputWrap = props.schema.tooltip ? Tooltip : Fragment;
  const inputWrapProps = props.schema.tooltip
    ? { tooltip: props.schema.tooltip }
    : null;

  // @ts-expect-error
  const additional = Object.hasOwn(schema, ADDITIONAL_PROPERTY_FLAG);

  return (
    <Row className={classNames}>
      {additional && (
        <LabelInput
          sm={colWidth - 2}
          label={label}
          required={required}
          id={`${id}-key`}
          onChange={onKeyChange}
        />
      )}
      {!additional && (
        <>
          {displayLabel && (
            <Form.Label
              column
              sm={colWidth}
              htmlFor={id}
              className="text-nowrap"
            >
              {label}
              {required ? '*' : null}
            </Form.Label>
          )}
        </>
      )}
      <Wrap {...wrapArgs}>
        {displayLabel && description ? description : null}
        {
          // @ts-expect-error
          <InputWrap {...inputWrapProps}>
            <InputGroup>
              {children}
              {unit && <InputGroup.Text>{unit}</InputGroup.Text>}
            </InputGroup>
          </InputWrap>
        }
        {errors}
        {help}
      </Wrap>
      {additional && (
        <Col sm={2} className="text-end p-0 d-grid">
          <Button
            size="sm"
            variant="danger"
            className="array-item-remove btn-block"
            style={{ border: '0' }}
            disabled={disabled || readonly}
            onClick={onDropPropertyClick(label)}
          >
            X
          </Button>
        </Col>
      )}
    </Row>
  );
}
