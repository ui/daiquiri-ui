import { Form, Col, Row, Container } from 'react-bootstrap';
import { map } from 'lodash';

import PlotEnhancer from 'components/PlotEnhancer';
import type { PlotParams } from 'react-plotly.js';

interface Props {
  schema: {
    title: string;
  };
  formData: {
    lines: { x0: number; y0: number; x1: number; y1: number }[];
    series: {
      data: number[];
      name: string;
      yaxis?: string;
    }[];
    x: number[];
    nodata?: string;
  };
}

export default function Plot0d(props: Props) {
  const { schema, formData } = props;

  const data: PlotParams['data'] = formData?.series?.map((series) => ({
    x: formData.x,
    y: series.data,
    name: series.name,
    yaxis: series.yaxis || 'y1',
  }));

  const shapes: Partial<Plotly.Shape>[] | undefined =
    (formData &&
      map(formData.lines, (line) => ({
        type: 'line',
        x0: line.x0,
        y0: line.y0,
        x1: line.x1,
        y1: line.y1,
        line: {
          color: '#59a7c1',
          width: 2,
          dash: 'dot',
        },
      }))) ??
    undefined;

  const layout: Partial<Plotly.Layout> = {
    shapes,
    yaxis2: {
      overlaying: 'y',
      side: 'right',
    },
  };

  return (
    <Container>
      <Row>
        <Form.Label column sm={3}>
          {schema.title}
        </Form.Label>
        <Col>
          {data && (
            <div style={{ height: 250 }}>
              <PlotEnhancer data={data} layout={layout} />
            </div>
          )}
          {!data && <p>{formData?.nodata || 'Awaiting Data'}</p>}
        </Col>
      </Row>
    </Container>
  );
}
