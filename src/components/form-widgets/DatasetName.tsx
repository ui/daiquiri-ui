import type { KeyboardEvent } from 'react';
import { useCallback } from 'react';
import { Button, Form, InputGroup } from 'react-bootstrap';

function schemaRequiresTrueValue(schema: any): boolean {
  if (schema.const) {
    return true;
  }
  if (schema.enum && schema.enum.length === 1 && schema.enum[0] === true) {
    return true;
  }
  if (schema.anyOf && schema.anyOf.length === 1) {
    return schemaRequiresTrueValue(schema.anyOf[0]);
  }
  if (schema.oneOf && schema.oneOf.length === 1) {
    return schemaRequiresTrueValue(schema.oneOf[0]);
  }
  if (schema.allOf) {
    return schema.allOf.some(schemaRequiresTrueValue);
  }
  return false;
}

interface Props {
  schema: any;
  id: string;
  value: string | undefined;
  disabled: boolean;
  readonly: boolean;
  label: string;
  autofocus: boolean;
  onChange: (value: string | undefined) => void;
}

/**
 * Characters that cannot be used for ESRF datapolicy.
 *
 * See https://gitlab.esrf.fr/bliss/bliss/-/blob/master/bliss/scanning/scan_saving.py
 */
const FORBIDDEN_CHARS = '{}%<>:"\'/\\|?*';

/**
 * Increment by one the number at the tail of the sequence.
 *
 * It handles the zero padding number.
 *
 * If there is no numbers at the tail a `2` is append to the result.
 *
 * Examples:
 * - `foo` becames `foo2`
 * - `foo1` becames `foo2`
 * - `foo0001` becames `foo0002`
 */
function incrementTailNumber(sequence: string): string {
  const matching = /\d+/.exec(sequence);
  if (matching === null) {
    return `${sequence}2`;
  }
  const tailNumber = matching[0];
  const base = sequence.slice(0, sequence.length - tailNumber.length);
  const inc = Number.parseInt(tailNumber) + 1;
  if (tailNumber.startsWith('0')) {
    return `${base}${String(inc).padStart(tailNumber.length, '0')}`;
  }
  return `${base}${inc}`;
}

/**
 * Display the sample id as the sample name if available
 */
export default function DatasetName(props: Props) {
  const { schema, id, value = '', disabled, readonly, autofocus } = props;
  const required = schemaRequiresTrueValue(schema);

  const increment = useCallback(() => {
    if (value === undefined) {
      return;
    }
    props.onChange(incrementTailNumber(value));
  }, [value]);

  function onKeyDown(event: KeyboardEvent<HTMLInputElement>) {
    if (FORBIDDEN_CHARS.includes(event.key)) {
      event.preventDefault();
    }
  }

  return (
    <InputGroup className="mb-3">
      <Form.Control
        className="form-control"
        type="text"
        id={id}
        value={value}
        required={required}
        disabled={disabled || readonly}
        autoFocus={autofocus}
        onChange={(event) => {
          const v = event.target.value;
          props.onChange(v === '' ? undefined : v);
        }}
        onKeyDown={onKeyDown}
      />
      <Button disabled={value === ''} variant="primary" onClick={increment}>
        +1
      </Button>
    </InputGroup>
  );
}
