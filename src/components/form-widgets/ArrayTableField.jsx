import { createRef } from 'react';
import { map, keys } from 'lodash';
import { Table, Button, Form, Col, Row, Container } from 'react-bootstrap';

import shortid from 'shortid';

import ArrayField from '@rjsf/core/dist/es/components/fields/ArrayField';
import ObjectField from '@rjsf/core/dist/es/components/fields/ObjectField';
import SchemaField from '@rjsf/core/dist/es/components/fields/SchemaField';
import {
  ADDITIONAL_PROPERTY_FLAG,
  retrieveSchema,
  toIdSchema,
  getDefaultRegistry,
  orderProperties,
  getDefaultFormState,
  isFixedItems,
  allowAdditionalItems
} from '@rjsf/core/dist/es/utils';

// These could be class methods and inheritable :(
// https://github.com/rjsf-team/react-jsonschema-form/blob/master/packages/core/src/components/fields/ArrayField.js`
function keyedToPlainFormData(keyedFormData) {
  return keyedFormData.map(keyedItem => keyedItem.item);
}

function HeadFieldTemplate(props) {
  return (
    <>
      {props.label}
      {props.schema.unit && (
        <span className="ms-1 unit">{props.schema.unit}</span>
      )}
      {props.required ? '*' : null}
    </>
  );
}

function CellFieldTemplate(props) {
  const { description, errors, children, displayLabel } = props;
  return (
    <>
      {displayLabel && description ? description : null}
      {children}
      {errors}
    </>
  );
}

export default class ArrayTableField extends ArrayField {
  constructor(props) {
    super(props);
    this.presetSelectRef = createRef();
  }

  _getPreseteFormDataRow() {
    const { schema, registry = getDefaultRegistry() } = this.props;
    const { definitions } = registry;
    let itemSchema = schema.items;
    if (isFixedItems(schema) && allowAdditionalItems(schema)) {
      itemSchema = schema.additionalItems;
    }
    return getDefaultFormState(itemSchema, undefined, definitions);
  }

  onApplyPreset = e => {
    e.preventDefault();

    const resolved = retrieveSchema(
      this.props.schema.items,
      this.props.registry.definitions
    );

    const val = this.presetSelectRef.current.value;
    if (val in resolved.presets) {
      const preset = resolved.presets[val];

      const newKeyedFormData = map(preset, row => ({
        key: shortid.generate(),
        item: {
          ...this._getNewFormDataRow(),
          ...row
        }
      }));

      const { onChange } = this.props;
      this.setState(
        {
          keyedFormData: newKeyedFormData,
          updatedKeyedFormData: true
        },
        () => onChange(keyedToPlainFormData(newKeyedFormData))
      );
    }
  };

  render() {
    const { props } = this;
    const {
      schema,
      uiSchema,
      errorSchema,
      idSchema,
      idPrefix,
      onBlur,
      onFocus,
      autofocus,
      rawErrors,
      registry = getDefaultRegistry()
    } = this.props;
    const { rootSchema } = registry;
    const resolved = retrieveSchema(schema.items, rootSchema);

    return (
      <Container className="array-table">
        <Row>
          <Form.Label column sm={3}>
            {schema.title}
          </Form.Label>
          <Col>
            {resolved.presets && (
              <Row>
                <Form.Group as={Col} xs={4}>
                  Presets:
                </Form.Group>
                <Form.Group as={Col} xs={5}>
                  <Form.Control as="select" ref={this.presetSelectRef}>
                    {map(resolved.presets, (v, p) => (
                      <option key={p} value={p}>
                        {p}
                      </option>
                    ))}
                  </Form.Control>
                </Form.Group>
                <Form.Group as={Col} className="text-end">
                  <Button
                    className="array-preset-apply mb-1"
                    onClick={this.onApplyPreset}
                    disabled={props.disabled || props.readonly}
                  >
                    <i className="fa fa-copy" />
                    Apply
                  </Button>
                </Form.Group>
              </Row>
            )}
            <div className="text-end">
              <Button
                className="array-item-add mb-1"
                onClick={this.onAddClick}
                disabled={props.disabled || props.readonly}
              >
                <i className="fa fa-plus" />
                Add
              </Button>
            </div>
            <Table bordered={false} size="sm" classes="noselect" striped>
              <thead>
                {this.state.keyedFormData.map((keyedItem, index) => {
                  if (index > 0) return null;

                  const { key, item } = keyedItem;
                  const itemSchema = retrieveSchema(
                    schema.items,
                    rootSchema,
                    item
                  );
                  const itemErrorSchema = errorSchema
                    ? errorSchema[index]
                    : undefined;
                  const itemIdPrefix = `${idSchema.$id}_${index}`;
                  const itemIdSchema = toIdSchema(
                    itemSchema,
                    itemIdPrefix,
                    rootSchema,
                    item,
                    idPrefix
                  );
                  return (
                    <ArrayTableObjectRow
                      key={key}
                      index={index}
                      schema={itemSchema}
                      isHeader
                      uiSchema={uiSchema.items}
                      formData={item}
                      errorSchema={itemErrorSchema}
                      idSchema={itemIdSchema}
                      required={this.isItemRequired(itemSchema)}
                      onChange={this.onChangeForIndex(index)}
                      onBlur={onBlur}
                      onFocus={onFocus}
                      registry={this.props.registry}
                      disabled={this.props.disabled}
                      readonly={this.props.readonly}
                      autofocus={autofocus && index === 0}
                      rawErrors={rawErrors}
                    />
                  );
                })}
              </thead>

              <tbody>
                {this.state.keyedFormData.map((keyedItem, index) => {
                  const { key, item } = keyedItem;
                  const itemSchema = retrieveSchema(
                    schema.items,
                    rootSchema,
                    item
                  );
                  const itemErrorSchema = errorSchema
                    ? errorSchema[index]
                    : undefined;
                  const itemIdPrefix = `${idSchema.$id}_${index}`;
                  const itemIdSchema = toIdSchema(
                    itemSchema,
                    itemIdPrefix,
                    rootSchema,
                    item,
                    idPrefix
                  );
                  return (
                    <ArrayTableObjectRow
                      key={key}
                      index={index}
                      schema={itemSchema}
                      uiSchema={uiSchema.items}
                      formData={item}
                      errorSchema={itemErrorSchema}
                      idSchema={itemIdSchema}
                      required={this.isItemRequired(itemSchema)}
                      onChange={this.onChangeForIndex(index)}
                      onBlur={onBlur}
                      onFocus={onFocus}
                      registry={this.props.registry}
                      disabled={this.props.disabled}
                      readonly={this.props.readonly}
                      autofocus={autofocus && index === 0}
                      rawErrors={rawErrors}
                      onDropIndexClick={this.onDropIndexClick}
                    />
                  );
                })}
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
    );
  }
}

class ArrayTableObjectRow extends ObjectField {
  isAddedByAdditionalProperties(name) {
    return this.props.schema.properties[name].hasOwnProperty(
      ADDITIONAL_PROPERTY_FLAG
    );
  }

  render() {
    const {
      schema,
      uiSchema,
      idSchema,
      errorSchema,
      formData,
      index,
      disabled,
      readonly
    } = this.props;

    const template = this.props.isHeader
      ? HeadFieldTemplate
      : CellFieldTemplate;
    const Wrap = this.props.isHeader ? 'th' : 'td';
    const order = schema.uiorder || uiSchema['ui:order'];
    const orderedProperties = orderProperties(
      keys(this.props.schema.properties),
      order
    );

    const extraUI = schema.uischema || {};

    return (
      <tr>
        {map(orderedProperties, name => (
          <Wrap key={name}>
            <SchemaField
              {...this.props}
              key={name}
              name={name}
              errorSchema={errorSchema[name]}
              formData={(formData || {})[name]}
              idSchema={idSchema[name]}
              schema={schema.properties[name]}
              uiSchema={{
                ...(this.isAddedByAdditionalProperties(name)
                  ? uiSchema.additionalProperties
                  : uiSchema[name]),
                ...extraUI[name],
                'ui:FieldTemplate': template
              }}
              onKeyChange={this.onKeyChange(name)}
              onChange={this.onPropertyChange(
                name,
                this.isAddedByAdditionalProperties(name)
              )}
            />
          </Wrap>
        ))}
        {this.props.isHeader && <th />}
        {!this.props.isHeader && (
          <td className="text-end">
            <Button
              variant="danger"
              className="array-item-remove"
              disabled={disabled || readonly}
              onClick={this.props.onDropIndexClick(index)}
            >
              <i className="fa fa-times" />
            </Button>
          </td>
        )}
      </tr>
    );
  }
}
