import { useRef, useState, useEffect, useCallback } from 'react';
import { map } from 'lodash';
import { Button, Row, Container, Col, Form, Alert } from 'react-bootstrap';

import ButtonTriggerModalStore from 'connect/layout/ButtonTriggerModalStore';
import type { ButtonTriggerModalStoreRef } from '../layout/models';
import SimpleSchemaForm from 'components/SimpleSchemaForm';
import Tooltip from 'components/common/Tooltip';
import useUserCache from '../utils/useUserCache';

interface IPresetNameForm {
  onSave: (formData: any) => Promise<any>;
}

function PresetNameForm(props: IPresetNameForm) {
  const onSave = (formData: any) => {
    props.onSave(formData.presetName);
  };

  const schema = {
    presetName: {
      title: 'Name',
      type: 'string',
    },
  };

  return (
    <SimpleSchemaForm
      colField
      className="save-form-preset"
      schema={schema}
      onSubmit={onSave}
      submitText="Save"
    />
  );
}

interface IFormPresets {
  presets: Record<string, any>;
  formData: Record<string, any>;
  savePresets: boolean;
  autoLoadPreset: boolean;
  onChange: (formData: any) => Promise<any>;
  savePreset: (payload: any) => Promise<any>;
  schema: string;
  fetchSchema: () => Promise<any>;
}

function FormPresets(props: IFormPresets) {
  const [lastPreset, setLastPreset] = useUserCache<string | null>(
    `schema/${props.schema}/lastpreset`,
    null
  );
  const [unsubmittedFormData, _] = useUserCache(
    `schema/${props.schema}/unsubmitted`,
    null
  );

  const presetSelectRef = useRef<HTMLSelectElement>(null);
  const modalRef = useRef<ButtonTriggerModalStoreRef>();
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(false);
  const [pending, setPending] = useState(false);

  const updateLastPreset = useCallback(
    (preset: string) => {
      setLastPreset(preset);
    },
    [setLastPreset, props.schema]
  );

  const onApplyPreset = useCallback(() => {
    const preset =
      presetSelectRef.current && props.presets[presetSelectRef.current.value];
    if (preset) {
      const newFormData = { ...props.formData, ...preset };
      props.onChange({ formData: newFormData });
      updateLastPreset(presetSelectRef.current.value);
    }
  }, [props, updateLastPreset]);

  const onSavePreset = (preset: string, toggleModal = true) => {
    setPending(true);
    props
      .savePreset({
        name: props.schema,
        data: props.formData,
        preset,
      })
      .then(() => {
        setPending(false);
        setSuccess(true);
        setTimeout(() => {
          setSuccess(false);
        }, 2000);
        if (toggleModal) modalRef.current?.close();
        updateLastPreset(preset);
        props.fetchSchema();
      })
      .catch((error_) => {
        setPending(false);
        setError(error_.response?.data?.error);
        setTimeout(() => {
          setError(null);
        }, 8000);
      });
  };

  const onOverwritePreset = () => {
    if (presetSelectRef.current) {
      onSavePreset(presetSelectRef.current.value, false);
    }
  };

  useEffect(() => {
    if (lastPreset) {
      if (presetSelectRef.current) {
        presetSelectRef.current.value = lastPreset;
      }
      if (!unsubmittedFormData && props.autoLoadPreset) onApplyPreset();
    }
  }, [
    lastPreset,
    unsubmittedFormData,
    props.schema,
    onApplyPreset,
    props.autoLoadPreset,
  ]);

  const hasPresets = !!props.presets;
  if (!hasPresets) return null;

  return (
    <>
      {pending && (
        <Alert variant="warning">
          <i className="fa fa-cog fa-spin" />
          Trying to save preset
        </Alert>
      )}
      {error !== null && <Alert variant="danger">{error}</Alert>}
      {success && <Alert variant="success">Preset saved</Alert>}
      <Container>
        <Row>
          <Col xs="3">Preset</Col>
          <Col>
            <Form.Control as="select" ref={presetSelectRef}>
              {map(props.presets, (v, p) => (
                <option key={p} value={p}>
                  {p}
                </option>
              ))}
            </Form.Control>
          </Col>
          <Col xs="3" className="text-end">
            <Button className="me-1" onClick={onApplyPreset}>
              <i className="fa fa-folder-open-o" />
            </Button>
            {props.savePresets && (
              <>
                <ButtonTriggerModalStore
                  ref={modalRef}
                  id="saveFormPreset"
                  button={<i className="fa fa-save" />}
                  buttonProps={{
                    variant: 'info',
                    className: 'me-1',
                  }}
                  title="Save Preset"
                >
                  {
                    // @ts-expect-error
                    <PresetNameForm onSave={onSavePreset} />
                  }
                </ButtonTriggerModalStore>
                <Tooltip tooltip="Overwrite current preset">
                  <Button onClick={onOverwritePreset} variant="warning">
                    <i className="fa fa-save" />
                  </Button>
                </Tooltip>
              </>
            )}
          </Col>
        </Row>
      </Container>
      <hr />
    </>
  );
}

export default FormPresets;
