import { useCallback, useEffect, useRef } from 'react';
import { debounce, isFunction, keys } from 'lodash';
import type { FormProps } from '@rjsf/core';
import type { JSONSchema7 } from 'json-schema';
import Form from '@rjsf/core';
import type { ButtonProps } from 'react-bootstrap';
import {
  Button,
  Row,
  Container,
  Col,
  OverlayTrigger,
  Tooltip,
} from 'react-bootstrap';
import useUserCache from '../utils/useUserCache';

function derefData(additionalFormData: any) {
  return isFunction(additionalFormData)
    ? additionalFormData()
    : additionalFormData;
}

interface ExtendedJSONSchema extends JSONSchema7 {
  cache?: boolean;
  name: string;
}

interface FormData {
  sampleid?: number;
  subsampleid?: number;
  objects?: Record<string, any>[];
  [key: string]: any;
}

interface Props extends Omit<FormProps<FormData>, 'schema'> {
  schema: ExtendedJSONSchema;
  additionalFormData: any;
}

/**
 * The form is initialized with info from `formData`, overrided by data from the cache,
 * overrided itself by data from `additionalFormData`.
 */
export default function FormWithCache(props: Props) {
  const [submittedCache, setSubmittedCache] = useUserCache<Record<string, any>>(
    `schema/${props.schema.name}/submitted`,
    {}
  );
  const [unsubmittedCache, setUnsubmittedCache] = useUserCache<
    Record<string, any>
  >(`schema/${props.schema.name}/unsubmitted`, {});

  const renderedRef = useRef(false);
  useEffect(() => {
    onApplyCache();
    setTimeout(() => {
      renderedRef.current = true;
    }, 200);
  }, []);

  function onSubmit(e: any, nativeEvent: any) {
    updateCache(e.formData, true);
    updateCache({}, false, true);
    props.onSubmit?.(e, nativeEvent);
  }

  function onChange(e: any) {
    if (!renderedRef.current) return;
    console.log('on change', e);
    debounceUpdateCache(e.formData);
    props.onChange?.(e);
  }

  const onClearCache = useCallback(() => {
    // @ts-expect-error
    const { sampleid, subsampleid, objects } = props.formData;
    // @ts-expect-error
    props.onChange?.({
      formData: {
        sampleid,
        subsampleid,
        objects,
        ...derefData(props.additionalFormData),
      },
    });
  }, [props.formData]);

  const onApplyCache = useCallback(
    (submitted?: boolean) => {
      if (!props.schema.cache) return;

      const cache = submitted ? submittedCache : unsubmittedCache;
      console.log('onApplyCache', cache);
      if (cache) {
        const newFormData = {
          ...props.formData,
          ...cache,
          ...derefData(props.additionalFormData),
        };
        // @ts-expect-error
        props.onChange?.({
          formData: newFormData,
        });
      }
    },
    [props.formData, props.additionalFormData]
  );

  const updateCache = useCallback(
    (newFormData: any, submitted?: boolean, clear?: boolean) => {
      if (!props.schema.cache || !props.formData) return null;

      const { sampleid, subsampleid, objects, ...formData } = newFormData;
      const value = clear ? {} : { ...formData };
      if (submitted) {
        setSubmittedCache(value);
      } else {
        setUnsubmittedCache(value);
      }
      return null;
    },
    [props.formData]
  );

  const debounceUpdateCache = useCallback(debounce(updateCache, 1000), [
    updateCache,
  ]);

  if (props.schema.cache) {
    const buttonProps: ButtonProps = {};
    if (!(submittedCache && keys(submittedCache).length > 0)) {
      buttonProps.disabled = true;
    }

    const loadToolTip = <Tooltip>Load last used parameters</Tooltip>;
    const clearToolTip = <Tooltip>Clear parameters</Tooltip>;

    return (
      <>
        <Container>
          <Row>
            <Col className="text-end">
              <OverlayTrigger placement="top" overlay={loadToolTip}>
                <Button
                  size="sm"
                  className="me-1"
                  onClick={() => onApplyCache(true)}
                  {...buttonProps}
                >
                  <i className="fa fa-history" />
                </Button>
              </OverlayTrigger>
              <OverlayTrigger placement="top" overlay={clearToolTip}>
                <Button size="sm" onClick={() => onClearCache()}>
                  <i className="fa fa-times" />
                </Button>
              </OverlayTrigger>
            </Col>
          </Row>
        </Container>
        <Form {...props} onSubmit={onSubmit} onChange={onChange} />
      </>
    );
  }
  return <Form {...props} />;
}
