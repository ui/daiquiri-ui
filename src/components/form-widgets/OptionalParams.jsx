import { map, each, keys, debounce } from 'lodash';

import {
  Form,
  Col,
  Row,
  Container,
  Button,
  OverlayTrigger,
  Tooltip
} from 'react-bootstrap';

import SchemaField from '@rjsf/core/dist/es/components/fields/SchemaField';
import ObjectField from '@rjsf/core/dist/es/components/fields/ObjectField';
import { ADDITIONAL_PROPERTY_FLAG } from '@rjsf/core/dist/es/utils';

import { Typeahead } from 'react-bootstrap-typeahead';

import 'react-bootstrap-typeahead/css/Typeahead.css';
import 'react-bootstrap-typeahead/css/Typeahead.bs5.css';
import useUserCache from '../utils/useUserCache';

class OptionalParams extends ObjectField {
  constructor(props) {
    super(props);
    this.state.displayedFields = keys(props.formData);
    this.updateCache = debounce(this.updateCache, 2500);
  }

  updateDisplayed() {
    this.setState({
      displayedFields: keys(this.props.formData)
    });
  }

  componentDidUpdate(last) {
    if (last.formData !== this.props.formData) this.updateDisplayed();
  }

  setSelection = selected => {
    const ids = map(selected, s => s.id);
    const removed = this.state.displayedFields.filter(x => !ids.includes(x));

    this.setState({
      displayedFields: ids
    });

    each(removed, r => {
      this.onPropertyChange(r, this.isAddedByAdditionalProperties(r))();
    });

    if (removed) this.updateCache();
  };

  isAddedByAdditionalProperties(name) {
    return this.props.schema.properties[name].hasOwnProperty(
      ADDITIONAL_PROPERTY_FLAG
    );
  }

  updateCache() {
    if (!this.props.schema.cache) return;
    const { formData } = this.props;
    this.props.setCache({ ...formData });
  }

  onChangeFactory = name => {
    const onPropertyChange = this.onPropertyChange(
      name,
      this.isAddedByAdditionalProperties(name)
    );

    return value => {
      const ret = onPropertyChange(value);
      this.updateCache();
      return ret;
    };
  };

  updateFormData(formData) {
    this.props.onChange(formData);

    this.setState({
      displayedFields: keys(formData)
    });
  }

  onClearCache = e => {
    e.preventDefault();
    this.updateFormData({});
  };

  onApplyCache = e => {
    if (!this.props.schema.cache) return;
    if (this.props.cache) {
      const newFormData = { ...this.props.formData, ...this.props.cache };
      this.updateFormData(newFormData);
    }
  };

  render() {
    const { schema, uiSchema, idSchema, errorSchema, formData, name } =
      this.props;
    const extraUI = schema.uischema || {};

    const cache = this.props.schema.cache && this.props.cache;
    const buttonProps = {};
    if (!(cache && keys(cache).length)) buttonProps.disabled = true;

    const loadToolTip = <Tooltip>Load last used parameters</Tooltip>;
    const clearToolTip = <Tooltip>Clear parameters</Tooltip>;

    return (
      <Container className="optional-params">
        <Row>
          <Form.Label column sm={3}>
            {schema.title}
          </Form.Label>
          <Col>
            <Row>
              <Col>
                <Typeahead
                  id={`typeahead_${name}`}
                  className="mb-2"
                  multiple
                  onChange={this.setSelection}
                  selected={map(this.state.displayedFields, f => ({
                    label: this.props.schema.properties[f].title,
                    id: f
                  }))}
                  options={map(this.props.schema.properties, (prop, name) => ({
                    id: name,
                    label: prop.title
                  }))}
                />
              </Col>
              {schema.cache === true && (
                <Col xs="2" className="text-end text-nowrap">
                  <OverlayTrigger placement="top" overlay={loadToolTip}>
                    <Button
                      size="sm"
                      className="me-1"
                      onClick={this.onApplyCache}
                      {...buttonProps}
                    >
                      <i className="fa fa-history" />
                    </Button>
                  </OverlayTrigger>
                  <OverlayTrigger placement="top" overlay={clearToolTip}>
                    <Button size="sm" onClick={this.onClearCache}>
                      <i className="fa fa-times" />
                    </Button>
                  </OverlayTrigger>
                </Col>
              )}
            </Row>

            <div className="fields">
              {this.state.displayedFields.map(fname => (
                <SchemaField
                  {...this.props}
                  key={fname}
                  name={fname}
                  errorSchema={errorSchema[fname]}
                  formData={(formData || {})[fname]}
                  idSchema={idSchema[fname]}
                  schema={schema.properties[fname]}
                  uiSchema={{
                    ...(this.isAddedByAdditionalProperties(fname)
                      ? uiSchema.additionalProperties
                      : uiSchema[fname]),
                    ...extraUI[fname]
                  }}
                  onKeyChange={this.onKeyChange(fname)}
                  onChange={this.onChangeFactory(fname)}
                />
              ))}
            </div>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default function CachedOptionalParams(props) {
  const [cache, setCache] = useUserCache(`schema/${props.name}`);
  return <OptionalParams cache={cache} setCache={setCache} {...props} />;
}
