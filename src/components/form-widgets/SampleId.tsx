import type { ReduxState } from 'types/ReduxStore';
import { useSelector } from 'react-redux';

function schemaRequiresTrueValue(schema: any): boolean {
  if (schema.const) {
    return true;
  }
  if (schema.enum && schema.enum.length === 1 && schema.enum[0] === true) {
    return true;
  }
  if (schema.anyOf && schema.anyOf.length === 1) {
    return schemaRequiresTrueValue(schema.anyOf[0]);
  }
  if (schema.oneOf && schema.oneOf.length === 1) {
    return schemaRequiresTrueValue(schema.oneOf[0]);
  }
  if (schema.allOf) {
    return schema.allOf.some(schemaRequiresTrueValue);
  }
  return false;
}

interface Props {
  schema: any;
  id: string;
  value: number;
  disabled: boolean;
  readonly: boolean;
  label: string;
  autofocus: boolean;
}

/**
 * Display the sample id as the sample name if available
 */
export default function SampleId(props: Props) {
  const { schema, id, value, disabled, readonly, autofocus } = props;

  const required = schemaRequiresTrueValue(schema);

  const sampleName = useSelector<ReduxState, string | undefined>((state) => {
    return state.metadata.ns_sample.default.results[value]?.name;
  });

  return (
    <input
      className="form-control"
      type="text"
      id={id}
      value={sampleName ?? `${value}`}
      readOnly
      required={required}
      disabled={disabled || readonly}
      autoFocus={autofocus}
    />
  );
}
