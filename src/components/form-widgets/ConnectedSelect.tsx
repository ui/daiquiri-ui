import type { RefObject } from 'react';
import { createRef, Component } from 'react';
import { Form } from 'react-bootstrap';
import { connect } from 'react-redux';

import { map } from 'lodash';

import { withNamespace } from 'providers/namespace';
import metadata from 'providers/metadata';
import type { Widget, WidgetProps } from '@rjsf/core';

interface Props extends WidgetProps {
  options: {
    value: any;
    key: any;
  };
  selectOptions?: Record<string, Record<string, any>>;
}

class Select extends Component<Props> {
  private readonly ref: RefObject<HTMLSelectElement>;
  public constructor(props: Props) {
    super(props);
    this.ref = createRef<HTMLSelectElement>();
  }

  public setValue() {
    setTimeout(() => {
      if (this.ref.current) this.props.onChange(this.ref.current.value);
    }, 1000);
  }

  public componentDidUpdate(prevProps: Props) {
    if (this.props.value !== prevProps.value) {
      if (this.props.value === undefined) {
        this.setValue();
      }
    }
  }

  public componentDidMount() {
    this.setValue();
  }

  public render() {
    const {
      id,
      value,
      disabled,
      readonly,
      autofocus,
      onBlur,
      onFocus,
      onChange,
      options,
      selectOptions,
    } = this.props;

    return (
      <Form.Control
        ref={this.ref}
        as="select"
        className="connected-select"
        value={value}
        id={id}
        disabled={disabled || readonly}
        autoFocus={autofocus}
        onChange={(event: any) => onChange(event.target.value)}
        onBlur={onBlur && ((event: any) => onBlur(id, event.target.value))}
        onFocus={onFocus && ((event: any) => onFocus(id, event.target.value))}
      >
        {map(selectOptions, (c) => (
          <option key={c[options.value]} value={c[options.value]}>
            {c[options.key]}
          </option>
        ))}
      </Form.Control>
    );
  }
}

const mapStateToProps = (state: any, props: any) => {
  const provider = props.providers.metadata[props.options.selector];
  return {
    selectOptions: provider.selector('results', state),
  };
};

const ConnSelect = withNamespace({ metadata })(
  connect(mapStateToProps)(Select)
);

// rjsf does not like connected component as a custom widget
function ConnectedSelect(props: Props) {
  return <ConnSelect {...props} />;
}

export default ConnectedSelect as Widget;
