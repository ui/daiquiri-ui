import { Button } from 'react-bootstrap';
import type { Widget, WidgetProps } from '@rjsf/core';

interface Props extends WidgetProps {
  options: {
    value: any;
    key: any;
  };
  selectOptions?: Record<string, Record<string, any>>;
  schema: Record<string, any>;
}

function BoolButton(props: Props) {
  const {
    schema,
    id,
    value,
    disabled,
    readonly,
    onBlur,
    onFocus,
    onChange,
    DescriptionField,
  } = props;

  return (
    <div className="d-grid" style={{ flex: 1 }}>
      <Button
        disabled={disabled || readonly}
        variant={value ? 'success' : 'danger'}
        onClick={() => onChange(!value)}
        onBlur={onBlur && ((event: any) => onBlur(id, value))}
        onFocus={onFocus && ((event: any) => onFocus(id, value))}
        id={id}
      >
        {value ? 'ON' : 'OFF'}
      </Button>
      {schema.description && (
        <DescriptionField description={schema.description} />
      )}
    </div>
  );
}

export default BoolButton as Widget;
