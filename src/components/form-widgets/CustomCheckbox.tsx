import { Form, Col } from 'react-bootstrap';
import type { Widget, WidgetProps } from '@rjsf/core';

function schemaRequiresTrueValue(schema: Record<string, any>): boolean {
  if (schema.const) {
    return true;
  }
  if (schema.enum && schema.enum.length === 1 && schema.enum[0] === true) {
    return true;
  }
  if (schema.anyOf && schema.anyOf.length === 1) {
    return schemaRequiresTrueValue(schema.anyOf[0]);
  }
  if (schema.oneOf && schema.oneOf.length === 1) {
    return schemaRequiresTrueValue(schema.oneOf[0]);
  }
  if (schema.allOf) {
    return schema.allOf.some(schemaRequiresTrueValue);
  }
  return false;
}

interface Props extends WidgetProps {
  options: {
    value: any;
    key: any;
  };
  selectOptions?: Record<string, Record<string, any>>;
  schema: Record<string, any>;
}

function CustomCheckbox(props: Props) {
  const {
    schema,
    id,
    value,
    disabled,
    readonly,
    label,
    autofocus,
    onBlur,
    onFocus,
    onChange,
    DescriptionField,
  } = props;

  const required = schemaRequiresTrueValue(schema);

  return (
    <>
      <Form.Label column sm={3} htmlFor={id}>
        {label}
        {required ? '*' : null}
      </Form.Label>
      <Col>
        <label>
          <input
            className="me-2"
            type="checkbox"
            id={id}
            checked={typeof value === 'undefined' ? false : value}
            required={required}
            disabled={disabled || readonly}
            autoFocus={autofocus}
            onChange={(event: any) => onChange(event.target.checked)}
            onBlur={
              onBlur && ((event: any) => onBlur(id, event.target.checked))
            }
            onFocus={
              onFocus && ((event: any) => onFocus(id, event.target.checked))
            }
          />

          {schema.description && (
            <DescriptionField description={schema.description} />
          )}
        </label>
      </Col>
    </>
  );
}

export default CustomCheckbox as Widget;
