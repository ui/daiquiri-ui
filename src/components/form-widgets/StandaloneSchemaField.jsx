import { Component } from 'react';
import PropTypes from 'prop-types';
import { each, debounce, isEqual } from 'lodash';

import Form from '@rjsf/core';
import { getUiOptions } from '@rjsf/core/dist/es/utils';

import BaseInput from '@rjsf/core/dist/es/components/widgets/BaseInput';
import SchemaField from '@rjsf/core/dist/es/components/fields/SchemaField';
import BoolButton from 'components/form-widgets/BoolButton';

import { colFieldFactory } from 'components/form-widgets/ColField';

import { Button } from 'react-bootstrap';

function ObjectFieldTemplate(props) {
  const canExpand = function canExpand() {
    const { formData, schema, uiSchema } = props;
    if (!schema.additionalProperties) {
      return false;
    }
    const { expandable } = getUiOptions(uiSchema);
    if (expandable === false) {
      return expandable;
    }
    // if ui:options.expandable was not explicitly set to false, we can add
    // another property if we have not exceeded maxProperties yet
    if (schema.maxProperties !== undefined) {
      return Object.keys(formData).length < schema.maxProperties;
    }
    return true;
  };

  const { TitleField, DescriptionField } = props;
  return (
    <div className="nested-object" style={{ flex: 1 }}>
      <div>{props.title}</div>
      <fieldset id={props.idSchema.$id}>
        {(props.uiSchema['ui:title'] || props.title) && (
          <TitleField
            id={`${props.idSchema.$id}__title`}
            title={props.title || props.uiSchema['ui:title']}
            required={props.required}
            formContext={props.formContext}
          />
        )}
        {props.description && (
          <DescriptionField
            id={`${props.idSchema.$id}__description`}
            description={props.description}
            formContext={props.formContext}
          />
        )}
        {props.properties.map(prop => prop.content)}
        {canExpand() && (
          <div className="d-grid">
            <Button
              variant="info"
              size="sm"
              className="object-property-expand"
              onClick={props.onAddClick(props.schema)}
              disabled={props.disabled || props.readonly}
            >
              Add
            </Button>
          </div>
        )}
      </fieldset>
    </div>
  );
}

class CustomBaseInput extends Component {
  static propTypes = SchemaField.propTypes;

  onKeyPress = e => {
    const { formContext } = this.props.registry;
    if (formContext && formContext._onFieldKeyPress) {
      formContext._onFieldKeyPress(e);
    }
  };

  render() {
    return <BaseInput {...this.props} onKeyPress={this.onKeyPress} />;
  }
}

class CustomSchemaField extends Component {
  static propTypes = SchemaField.propTypes;

  constructor(props) {
    super(props);
    this.propagateChange = debounce(this.propagateChange.bind(this), 100);
  }

  onChange = formData => {
    if (this.props.onChange) {
      this.props.onChange(formData);
    }

    // TODO: More none text field types will need to be detected here
    if (
      !!this.props.schema.enum ||
      (this.props.uiSchema && this.props.uiSchema['ui:widget'] === 'range') ||
      this.props.schema.type === 'boolean' ||
      this.props.schema.type === 'object'
    ) {
      // TODO: Race condition ahoy
      //  We need to wait for the parent onChange to setState otherwise
      //  errorSchema is not yet populated
      setTimeout(() => {
        this.propagateChange(this.props.name, formData);
      }, 1);
    }
  };

  onFieldChange = (prop, formData) => {
    if (prop in this.props.errorSchema) {
      console.log(
        `onFieldChange: ${prop} is invalid`,
        this.props.errorSchema[prop].__errors
      );
    } else {
      const { formContext } = this.props.registry;
      if (formContext && formContext.onFieldChange) {
        formContext.onFieldChange(prop, formData);
      }
    }
  };

  onFieldKeyPress = e => {
    if (e.key === 'Enter') {
      const prop = this.findFromIdSchema(e.target.id);

      if (!prop) {
        console.log(`onKeyPress: cant find property for ${e.target.id}`);
        return;
      }

      if (prop in this.props.errorSchema) {
        console.log(
          `onKeyPress: ${prop} is invalid`,
          this.props.errorSchema[prop].__errors
        );
      } else {
        const { formContext } = this.props.registry;
        if (formContext && formContext.onFieldKeyPress) {
          formContext.onFieldKeyPress(prop, this.props.formData[prop]);
        }
      }
    }
  };

  propagateChange(name, value) {
    const { formContext } = this.props.registry;
    if (formContext && formContext._onFieldChange) {
      formContext._onFieldChange(name, value);
    }
  }

  findFromIdSchema(id) {
    let key = null;
    each(this.props.idSchema, (v, k) => {
      if (typeof v === 'object') {
        if (v.$id === id) key = k;
      }
    });

    return key;
  }

  render() {
    const customProps = {};
    if (this.props.name) {
      customProps.onChange = this.onChange;
    }

    const registry = {
      formContext: {
        ...this.props.registry.formContext
      },
      ...this.props.registry
    };

    if (!this.props.name) {
      registry.formContext._onFieldKeyPress = this.onFieldKeyPress;
      registry.formContext._onFieldChange = this.onFieldChange;
    }

    const pendingUpdate =
      this.props.name in this.props.registry.formContext.initialFormData &&
      !isEqual(
        this.props.registry.formContext.initialFormData[this.props.name],
        this.props.formData
      );
    return (
      <SchemaField
        {...this.props}
        {...customProps}
        registry={registry}
        uiSchema={{
          ...this.props.uiSchema,
          ...(pendingUpdate ? { classNames: 'pending-update' } : null)
        }}
      />
    );
  }
}

function CustomCheckbox(props) {
  const {
    schema,
    id,
    value,
    disabled,
    readonly,
    autofocus,
    onBlur,
    onFocus,
    onChange,
    DescriptionField
  } = props;

  return (
    <>
      <input
        className="mt-2"
        type="checkbox"
        id={id}
        checked={typeof value === 'undefined' ? false : value}
        disabled={disabled || readonly}
        autoFocus={autofocus}
        onChange={event => onChange(event.target.checked)}
        onBlur={onBlur && (event => onBlur(id, event.target.checked))}
        onFocus={onFocus && (event => onFocus(id, event.target.checked))}
      />
      {schema.description && (
        <DescriptionField description={schema.description} />
      )}
    </>
  );
}

export default class StandaloneSchemaField extends Component {
  static defaultProps = {
    hiddenFields: [],
    onFieldKeyPress: null,
    onFieldChange: null,
    noLabel: false,
    colWidth: 4
  };

  static propTypes = {
    ...Form.propTypes,
    hiddenFields: PropTypes.arrayOf(PropTypes.string),
    onFieldKeyPress: PropTypes.func,
    onFieldChange: PropTypes.func,
    noLabel: PropTypes.bool,
    colWidth: PropTypes.number
  };

  constructor(props) {
    super(props);
    this.FieldTemplate = colFieldFactory({
      colWidth: props.colWidth,
      noLabel: props.noLabel
    });
    this.state = {
      localFormData: this.props.formData
    };
  }

  componentDidUpdate(lastProps) {
    if (lastProps.formData != this.props.formData) {
      this.setState({ localFormData: this.props.formData });
    }
  }

  onChange(form) {
    const formData = JSON.parse(JSON.stringify(form.formData));
    this.setState({ localFormData: formData });
    if (this.props.onChange) this.props.onChange(formData);
  }

  resetPendingChanges() {
    setTimeout(() => {
      this.setState({ localFormData: this.props.formData });
      if (this.props.onChange) this.props.onChange(this.props.formData);
    }, 500);
  }

  render() {
    const uiSchema = {
      'ui:order': this.props.schema.uiorder,
      ...this.props.schema.uischema,
      ...this.props.uiSchema
    };

    each(this.props.hiddenFields, f => {
      uiSchema[f] = {
        'ui:widget': 'hidden',
        classNames: 'hidden-row'
      };
    });

    each(this.props.schema.properties, (f, k) => {
      if (f.type === 'boolean' && !(k in uiSchema)) {
        uiSchema[k] = {
          'ui:widget': 'BoolCheckbox'
        };
      }
    });

    const { formData, onChange, ...rest } = this.props;
    return (
      <Form
        FieldTemplate={this.FieldTemplate}
        ObjectFieldTemplate={ObjectFieldTemplate}
        formContext={{
          onFieldKeyPress: this.props.onFieldKeyPress,
          onFieldChange: this.props.onFieldChange,
          initialFormData: { ...this.props.formData }
        }}
        showErrorList={false}
        liveValidate
        uiSchema={uiSchema}
        widgets={{
          BoolCheckbox: CustomCheckbox,
          BoolButton,
          BaseInput: CustomBaseInput
        }}
        fields={{ SchemaField: CustomSchemaField }}
        className="compact"
        children
        onChange={form => this.onChange(form)}
        onBlur={() => this.resetPendingChanges()}
        formData={this.state.localFormData}
        {...rest}
      />
    );
  }
}
