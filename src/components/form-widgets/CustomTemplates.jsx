import PropTypes from 'prop-types';
import { each, keys } from 'lodash';

import ColField from 'components/form-widgets/ColField';

import {
  DefaultArrayItem,
  DefaultObjectFieldTemplate
} from 'components/form-widgets/DefaultTemplates';

function defaultOrComponent(DefaultTemplate) {
  function DefaultOrComponent(props) {
    const Template = props.uiSchema['ui:template'];
    if (Template) {
      return <Template {...props} />;
    }
    return <DefaultTemplate {...props} />;
  }

  DefaultOrComponent.defaultProps = {
    uiSchema: {}
  };

  DefaultOrComponent.propTypes = {
    uiSchema: PropTypes.shape({
      'ui:template': PropTypes.oneOfType([PropTypes.func, PropTypes.element])
    })
  };

  return DefaultOrComponent;
}

export function TemplatedFieldTemplate(props) {
  const Template = props.uiSchema['ui:template'];
  if (
    Template &&
    props.schema.type !== 'object' &&
    props.schema.type !== 'array'
  ) {
    return <Template {...props} />;
  }

  // grim
  let colWidth = 3;
  if (props.formContext.groups) {
    const children = props.children && props.children.props.children;
    const key = children && children[0].props.name;
    if (key && props.formContext.groups.indexOf(key) === -1) {
      let groupHasWidth = false;
      each(props.formContext.groups, group => {
        if (typeof group === 'object' && group !== null) {
          each(group, val => {
            if (Array.isArray(val) && val.indexOf(key) > -1) {
              groupHasWidth = group['ui:colwidth'];
            }
          });
        }
      });

      if (groupHasWidth) {
        colWidth = groupHasWidth;
      } else {
        const widths = {
          15: 5,
          11: 4,
          8: 3
        };

        const wk = keys(widths);
        wk.reverse();
        each(wk, w => {
          if (props.schema.title.length < w) colWidth = widths[w];
        });
      }
    }
  }

  return <ColField colWidth={colWidth} {...props} />;
}

TemplatedFieldTemplate.defaultProps = {
  uiSchema: {}
};

TemplatedFieldTemplate.propTypes = {
  schema: PropTypes.shape({
    type: PropTypes.string,
    title: PropTypes.string
  }).isRequired,
  uiSchema: PropTypes.shape({
    'ui:template': PropTypes.oneOfType([PropTypes.func, PropTypes.element])
  }),
  formContext: PropTypes.shape({
    groups: PropTypes.array
  }).isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export const TemplatedObjectFieldTemplate = defaultOrComponent(
  DefaultObjectFieldTemplate
);

export const TemplatedArrayFieldTemplate = defaultOrComponent(DefaultArrayItem);
