import { map } from 'lodash';

import { Row, Col, Alert } from 'react-bootstrap';

// Stolen from https://codesandbox.io/s/0y7787xp0l
function ErrorAlert(props) {
  return (
    <Alert variant="danger">
      <Alert.Heading>Field Grouping Error</Alert.Heading>
      <p>{props.message}</p>
    </Alert>
  );
}

class ExtraneousField {
  constructor(field) {
    this.field = field;
  }
}

function doGrouping({ properties, groups, props, allMapped }) {
  if (!Array.isArray(groups)) {
    return properties.map(p => p.content);
  }

  const DefaultTemplate = props.properties.map(p => p.children);

  const mapped = map(groups, (g, gid) => {
    if (Array.isArray(g)) {
      return (
        <ErrorAlert message={`Groups cannot be arrays: [${g.join(',')}]`} />
      );
    }

    if (typeof g === 'string') {
      const found = properties.filter(p => p.name === g);
      if (found.length === 1) {
        const el = found[0];
        allMapped.push(g);
        return el.content;
      }
      return new ExtraneousField(g);
    }

    if (typeof g === 'object') {
      const { templates } = props.formContext;
      const GroupComponent = templates
        ? templates[g['ui:template'] || 'flex']
        : DefaultTemplate;
      const rProperties = Object.keys(g).reduce((acc, key) => {
        const field = g[key];
        if (key.startsWith('ui:')) return acc;
        if (!Array.isArray(field)) return acc;
        return [
          ...acc,
          {
            name: key,
            children: doGrouping({
              groups: field,
              properties,
              props,
              allMapped
            })
          }
        ];
      }, []);
      return (
        <GroupComponent
          key={`group${gid}`}
          properties={rProperties}
          group={g}
        />
      );
    }
    return (
      <ErrorAlert
        message={`Invalid object type for group: ${typeof g} - ${g}`}
      />
    );
  });
  const extraneous = mapped.filter(m => m instanceof ExtraneousField);
  if (extraneous.length) {
    return (
      <ErrorAlert
        message={`Extraneous fields: ${extraneous
          .map(e => e.field)
          .join(', ')}`}
      />
    );
  }

  return mapped;
}

export function ObjectFieldTemplate(props) {
  // console.log('Grouped ObjectFieldTemplate', props);
  const { TitleField, DescriptionField } = props;

  const allMapped = [];
  const groupedFields = doGrouping({
    properties: props.properties,
    groups: props.uiSchema['ui:groups'],
    props,
    allMapped
  });

  if (props.uiSchema['ui:groups']) {
    const mapDifference = map(props.properties, p => p.name).filter(
      x => !allMapped.includes(x)
    );

    if (mapDifference.length) {
      return (
        <ErrorAlert
          message={`Not all fields were mapped into groups, remaining fields: ${mapDifference.join(
            ', '
          )}`}
        />
      );
    }
  }

  return (
    <fieldset>
      {(props.uiSchema['ui:title'] || props.title) && (
        <TitleField
          id={`${props.idSchema.$id}__title`}
          title={props.title || props.uiSchema['ui:title']}
          required={props.required}
          formContext={props.formContext}
        />
      )}
      {props.description && (
        <DescriptionField
          id={`${props.idSchema.$id}__description`}
          description={props.description}
          formContext={props.formContext}
        />
      )}
      {groupedFields}
    </fieldset>
  );
}

function FlexGroupTemplate(props) {
  return (
    <div className="grouped-fields">
      {props.properties.map(p => (
        <Row className="form-group" key={p.name}>
          <Col xs="3">{p.name}</Col>
          <Col xs="9">
            <Row>
              {p.children.map((c, i) => (
                <Col
                  key={`${p.name}${i}`}
                  xs={props.group['ui:minwidth'] || 6}
                  className="pl-0 pr-0"
                >
                  {c}
                </Col>
              ))}
            </Row>
          </Col>
        </Row>
      ))}
    </div>
  );
}

export const GroupTemplates = {
  flex: FlexGroupTemplate
};
