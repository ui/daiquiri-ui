import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import ParametersList from 'connect/parameteriser/ParametersList';

export default function Yaml(props: YamlComponent) {
  const { providers, yamlNode, paramtype, ...unknownOptions } = props;
  YamlAsserts.assertOptionalString(yamlNode, 'paramtype', paramtype);
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  return <ParametersList providers={providers} type={paramtype} />;
}
