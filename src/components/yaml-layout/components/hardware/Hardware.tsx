import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import Hardware from 'connect/hardware/Hardware';

export default function Yaml(props: YamlComponent) {
  const {
    providers = {},
    yamlNode,
    id,
    variant,
    header,
    width,
    colWidth,
    emptyifnone,
    ...unknownOptions
  } = props;
  YamlAsserts.assertString(yamlNode, 'id', id);
  YamlAsserts.assertOptionalString(yamlNode, 'variant', variant);
  YamlAsserts.assertOptionalString(yamlNode, 'header', header);
  YamlAsserts.assertOptionalNumber(yamlNode, 'width', width);
  YamlAsserts.assertOptionalNumber(yamlNode, 'colWidth', colWidth);
  YamlAsserts.assertOptionalBoolean(yamlNode, 'emptyifnone', emptyifnone);
  // FIXME: Each hardware variant/kind have it's own options
  //        And the hardware kind can change dynamically
  //        We could try to normalize that in the future
  // assertNoUnknownKeys(yamlNode, unknownOptions);
  const options = {
    variant,
    header,
    width,
    colWidth,
    emptyifnone,
    ...unknownOptions,
  };
  return <Hardware providers={providers} id={id} options={options} />;
}
