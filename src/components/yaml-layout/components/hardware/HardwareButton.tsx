import type { YamlComponent, YamlNode } from '@esrf/daiquiri-lib';
import { YamlAsserts, KeyError, MissingKeysError } from '@esrf/daiquiri-lib';
import HardwareButton from 'connect/hardware/HardwareButton';
import type { HardwareButtonObject } from 'components/hardware/HardwareButton';

type HardwareButtonObjectList = HardwareButtonObject[];

function assertHardwareButtonObject(
  yamlNode: YamlNode,
  value: any
): asserts value is HardwareButtonObject {
  if (value === undefined) {
    // FIXME: That's not a nice exception in this case, the rendering will be bad
    throw new MissingKeysError(yamlNode, ['id']);
  }
  if (typeof value.id !== 'string') {
    // FIXME: That's not a nice exception in this case, the rendering will be bad
    throw new KeyError(yamlNode, 'id', 'A string is expected');
  }
}

function assertHardwareButtonObjectListDesc(
  yamlNode: YamlNode,
  key: string,
  value: any
): asserts value is HardwareButtonObjectList | undefined {
  if (value === undefined) {
    return;
  }
  if (!Array.isArray(value)) {
    throw new KeyError(yamlNode, 'ids', 'An option list is expected');
  }

  value.forEach((hardwareDesc, i) => {
    const {
      id,
      function: fn,
      property,
      value,
      increment,
      reference,
      ...unknownOptions
    } = hardwareDesc;
    YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);

    try {
      assertHardwareButtonObject(yamlNode, hardwareDesc);
    } catch {
      // FIXME: For now we catch it, but assertHardwareDesc could do a better job
      throw new KeyError(
        yamlNode,
        'ids',
        `The hardware button object description ${i} is wrong`
      );
    }
  });
}

export default function Yaml(props: YamlComponent) {
  const {
    providers,
    yamlNode,
    ids,
    steps,
    buttonTitle,
    icon,
    ...unknownOptions
  } = props;
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  assertHardwareButtonObjectListDesc(yamlNode, 'ids', ids);
  YamlAsserts.assertOptionalString(yamlNode, 'buttonTitle', buttonTitle);
  YamlAsserts.assertOptionalString(yamlNode, 'icon', icon);
  YamlAsserts.assertOptionalNumberList(yamlNode, 'steps', steps);
  return (
    <HardwareButton
      steps={steps}
      ids={ids}
      buttonTitle={buttonTitle}
      icon={icon}
      providers={providers}
    />
  );
}
