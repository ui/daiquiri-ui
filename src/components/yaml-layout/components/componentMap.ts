import { lazy } from 'react';

export const componentMap = {
  console: './Console',
  editor: './Editor',
  editortree: './EditorTree',
  hardware: './hardware/Hardware',
  hardwarebutton: './hardware/HardwareButton',
  h5viewer: './hdf5/H5Viewer',
  hdf5plot0d: './hdf5/Hdf5Plot',
  hdf5plot: './hdf5/Hdf5Plot',
  newscanbutton: './NewScanButton',
  parameterslist: './ParametersList',
  sampledclist: './SampleDCList',
  sampleregistration: './samples/SampleRegistration',
  scanplot0d: './scans/ScanPlot0d',
  scanplot0dvalue: './scans/ScanPlot0dValue',
  scanvalue: './scans/ScanValue',
  scanplot1d: './scans/ScanPlot1d',
  scanplot2d: './scans/ScanPlot2d',
  scantable: './scans/ScanTable',
  schemaform: './SchemaForm',
  synoptic: './Synoptic',
  tomoalign: './tomo/TomoAlign',
  tomodetectorview: './tomo/TomoDetectorView',
  tomodetector: './tomo/TomoDetector',
  tomoholo: './tomo/TomoHolo',
  tomoscaninfo: './tomo/TomoScanInfo',
  tomosinogram: './tomo/TomoSinogram',
  tomoreconstructedsinogram: './tomo/TomoReconstructedSinogram',
  tomotiling: './tomo/TomoTiling',
  tomoscanlist: './tomo/TomoScanList',
  twod: './2dview/TwoD',
  twodobject: './2dview/TwoDObject',
  twodobjectlist: './2dview/TwoDObjectList',
  videostream: './VideoStream',
};

export function lazyComponentMap() {
  return Object.fromEntries(
    Object.entries(componentMap).map(([componentName, componentPath]) => [
      componentName,
      /* eslint-disable import/dynamic-import-chunkname */
      lazy(() => import(`${componentPath}`)),
    ])
  );
}
