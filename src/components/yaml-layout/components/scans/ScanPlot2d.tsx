import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import ScanPlot2d from 'connect/scans/ScanPlot2d';

export default function Yaml(props: YamlComponent) {
  const { providers, yamlNode, events, ...unknownOptions } = props;
  YamlAsserts.assertOptionalStringList(yamlNode, 'events', events);
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  return <ScanPlot2d providers={providers} />;
}
