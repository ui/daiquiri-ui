import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import ScanPlot0dValue from 'connect/scans/ScanPlot0dValue';

export default function Yaml(props: YamlComponent) {
  const {
    yamlNode,
    providers,
    series,
    name,
    round,
    comparison,
    comparator,
    ...unknownOptions
  } = props;
  YamlAsserts.assertString(yamlNode, 'series', series);
  YamlAsserts.assertOptionalString(yamlNode, 'name', name);
  YamlAsserts.assertOptionalNumber(yamlNode, 'round', round);
  YamlAsserts.assertOptionalString(yamlNode, 'comparison', comparison);
  YamlAsserts.assertOptionalString(yamlNode, 'comparator', comparator);

  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  return (
    <ScanPlot0dValue
      providers={providers}
      options={{ series, name, round, comparison, comparator }}
    />
  );
}
