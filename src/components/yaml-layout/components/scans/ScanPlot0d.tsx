import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import ScanPlot0d from 'connect/scans/ScanPlot0d';

export default function Yaml(props: YamlComponent) {
  const {
    providers,
    yamlNode,
    backend,
    preSelectedXAxis,
    preSelectedSeries,
    ...unknownOptions
  } = props;
  YamlAsserts.assertOptionalBackend(yamlNode, 'backend', backend);
  YamlAsserts.assertOptionalString(
    yamlNode,
    'preSelectedXAxis',
    preSelectedXAxis
  );
  YamlAsserts.assertOptionalStringOrStringList(
    yamlNode,
    'preSelectedSeries',
    preSelectedSeries
  );
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  return (
    <ScanPlot0d
      providers={providers}
      options={{ backend, preSelectedXAxis, preSelectedSeries }}
    />
  );
}
