import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import ScanTable from 'connect/scans/ScanTable';

export default function Yaml(props: YamlComponent) {
  const { providers, yamlNode, events, ...unknownOptions } = props;
  YamlAsserts.assertOptionalStringList(yamlNode, 'events', events);
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  return <ScanTable providers={providers} />;
}
