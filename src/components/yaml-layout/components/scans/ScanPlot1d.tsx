import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import ScanPlot1d from 'connect/scans/ScanPlot1d';

export default function Yaml(props: YamlComponent) {
  const {
    providers,
    yamlNode,
    backend,
    preSelectedXAxis,
    preSelectedSeries,
    plotTitle,
    insetLegend,
    ...unknownOptions
  } = props;
  YamlAsserts.assertOptionalBackend(yamlNode, 'backend', backend);
  YamlAsserts.assertOptionalString(
    yamlNode,
    'preSelectedXAxis',
    preSelectedXAxis
  );
  YamlAsserts.assertOptionalStringOrStringList(
    yamlNode,
    'preSelectedSeries',
    preSelectedSeries
  );
  YamlAsserts.assertOptionalString(yamlNode, 'title', plotTitle);
  YamlAsserts.assertOptionalBoolean(yamlNode, 'insetLegend', insetLegend);
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  return (
    <ScanPlot1d
      providers={providers}
      options={{
        backend,
        preSelectedXAxis,
        preSelectedSeries,
        plotTitle,
        insetLegend,
      }}
    />
  );
}
