import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import ScanValue from 'connect/scans/ScanValue';

export default function Yaml(props: YamlComponent) {
  const { yamlNode, providers, scanKey, name, ...unknownOptions } = props;
  YamlAsserts.assertString(yamlNode, 'scanKey', scanKey);
  YamlAsserts.assertOptionalString(yamlNode, 'name', name);
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  return <ScanValue providers={providers} options={{ scanKey, name }} />;
}
