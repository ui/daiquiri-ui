import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import SchemaForm from 'connect/SchemaForm';

export default function Yaml(props: YamlComponent) {
  const {
    providers,
    yamlNode,
    schema,
    url,
    requireOperator,
    submitText,
    ...unknownOptions
  } = props;
  YamlAsserts.assertString(yamlNode, 'schema', schema);
  YamlAsserts.assertString(yamlNode, 'url', url);
  YamlAsserts.assertOptionalBoolean(
    yamlNode,
    'requireOperator',
    requireOperator
  );
  YamlAsserts.assertOptionalString(yamlNode, 'submitText', submitText);
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  return (
    <SchemaForm
      providers={providers}
      options={{
        schema,
        url,
        requireOperator,
        submitText,
      }}
    />
  );
}
