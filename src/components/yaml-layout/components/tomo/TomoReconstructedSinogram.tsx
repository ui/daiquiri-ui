import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import TomoReconstructedSinogram from 'connect/tomo/TomoReconstructedSinogram';

export default function Yaml(props: YamlComponent) {
  const {
    providers = {},
    yamlNode,
    datacollectionid,
    uri,
    tomoconfig,
    events,
    ...unknownOptions
  } = props;
  YamlAsserts.assertStringList(yamlNode, 'events', events);
  YamlAsserts.assertString(yamlNode, 'tomoconfig', tomoconfig);
  YamlAsserts.assertString(yamlNode, 'uri', uri);
  YamlAsserts.assertDataCollectionId(
    yamlNode,
    'datacollectionid',
    datacollectionid
  );
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  const options = { datacollectionid, uri, tomoconfig, events };
  return <TomoReconstructedSinogram providers={providers} options={options} />;
}
