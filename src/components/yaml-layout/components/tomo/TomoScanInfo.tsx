import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import TomoScanInfo from 'components/tomo/scaninfo/Default';

export default function Yaml(props: YamlComponent) {
  const { providers, yamlNode, ...unknownOptions } = props;
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  return <TomoScanInfo />;
}
