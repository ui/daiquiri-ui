import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import TomoDetectorView from 'connect/tomo/TomoDetectorView';

export default function Yaml(props: YamlComponent) {
  const {
    providers = {},
    yamlNode,
    tomoconfig,
    moveenabled,
    ...unknownOptions
  } = props;
  YamlAsserts.assertString(yamlNode, 'tomoconfig', tomoconfig);
  YamlAsserts.assertOptionalBoolean(yamlNode, 'moveenabled', moveenabled);
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  const options = { tomoconfig, moveenabled };
  return <TomoDetectorView providers={providers} options={options} />;
}
