import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import TomoTiling from 'connect/tomo/TomoTiling';

export default function Yaml(props: YamlComponent) {
  const {
    providers = {},
    yamlNode,
    showrois,
    showlaunchscan,
    tomoconfig,
    ...unknownOptions
  } = props;
  YamlAsserts.assertString(yamlNode, 'tomoconfig', tomoconfig);
  YamlAsserts.assertOptionalBoolean(yamlNode, 'showrois', showrois);
  YamlAsserts.assertOptionalBoolean(yamlNode, 'showlaunchscan', showlaunchscan);
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);

  const options = {
    showrois,
    showlaunchscan,
    tomoconfig,
  };

  return <TomoTiling providers={providers} options={options} />;
}
