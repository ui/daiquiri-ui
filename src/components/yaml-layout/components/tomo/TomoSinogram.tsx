import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import TomoSinogram from 'connect/tomo/TomoSinogram';

export default function Yaml(props: YamlComponent) {
  const {
    providers = {},
    yamlNode,
    tomoconfig,
    datacollectionid,
    ...unknownOptions
  } = props;

  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  YamlAsserts.assertString(yamlNode, 'tomoconfig', tomoconfig);
  YamlAsserts.assertDataCollectionId(
    yamlNode,
    'datacollectionid',
    datacollectionid
  );
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);

  const options = {
    datacollectionid,
    tomoconfig,
  };

  return <TomoSinogram providers={providers} options={options} />;
}
