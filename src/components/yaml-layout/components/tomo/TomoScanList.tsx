import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import TomoScanList from 'connect/tomo/TomoScanList';

export default function Yaml(props: YamlComponent) {
  const { providers = {}, yamlNode, ...unknownOptions } = props;
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  return <TomoScanList providers={providers} />;
}
