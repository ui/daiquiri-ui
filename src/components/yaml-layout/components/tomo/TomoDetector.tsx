import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import TomoDetector from 'connect/tomo/TomoDetector';

export default function Yaml(props: YamlComponent) {
  const { providers = {}, yamlNode, tomoconfig, ...unknownOptions } = props;
  YamlAsserts.assertString(yamlNode, 'tomoconfig', tomoconfig);
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  const options = { tomoconfig };
  return <TomoDetector providers={providers} options={options} />;
}
