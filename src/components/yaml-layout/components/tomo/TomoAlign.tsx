import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import TomoAlign from 'connect/tomo/TomoAlign';

export default function Yaml(props: YamlComponent) {
  const {
    providers = {},
    yamlNode,
    procedure,
    ignoretilt,
    decimals,
    lateralunit,
    ...unknownOptions
  } = props;
  YamlAsserts.assertString(yamlNode, 'procedure', procedure);
  YamlAsserts.assertOptionalBoolean(yamlNode, 'ignoretilt', ignoretilt);
  YamlAsserts.assertOptionalNumber(yamlNode, 'decimals', decimals);
  YamlAsserts.assertOptionalString(yamlNode, 'lateralunit', lateralunit);
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  const options = { procedure, ignoretilt, decimals, lateralunit };
  return <TomoAlign providers={providers} options={options} />;
}
