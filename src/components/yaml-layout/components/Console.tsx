import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import Console from 'connect/Console';

export default function Yaml(props: YamlComponent) {
  const { providers, yamlNode, ...unknownOptions } = props;
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  return <Console providers={providers} />;
}
