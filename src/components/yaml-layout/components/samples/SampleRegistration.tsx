import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import SampleRegistration from 'connect/samples/SampleRegistration';

export default function Yaml(props: YamlComponent) {
  const { providers, yamlNode, datacollectionid, fileName, ...unknownOptions } =
    props;
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  return <SampleRegistration />;
}
