import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import ObjectView from 'connect/2dview/ObjectView';

export default function Yaml(props: YamlComponent) {
  const { providers, yamlNode, ...unknownOptions } = props;
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  return <ObjectView providers={providers} />;
}
