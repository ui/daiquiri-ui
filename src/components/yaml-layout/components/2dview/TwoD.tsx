import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import TwoDView from 'connect/2dview/TwoDView';

export default function Yaml(props: YamlComponent) {
  const { providers, yamlNode, ...unknownOptions } = props;
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  return <TwoDView providers={providers} />;
}
