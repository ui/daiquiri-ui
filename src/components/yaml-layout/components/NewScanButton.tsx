import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import NewScanButton from 'connect/samples/NewScanButton';

export default function Yaml(props: YamlComponent) {
  const { providers, yamlNode, tags, ...unknownOptions } = props;
  YamlAsserts.assertOptionalStringList(yamlNode, 'tags', tags);
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  const options = {
    tags,
  };
  return <NewScanButton providers={providers} options={options} />;
}
