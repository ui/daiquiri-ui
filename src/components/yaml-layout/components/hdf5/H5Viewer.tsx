import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import H5Viewer from 'components/h5viewer/H5Viewer';

export default function Yaml(props: YamlComponent) {
  const { providers, yamlNode, datacollectionid, fileName, ...unknownOptions } =
    props;
  YamlAsserts.assertNumber(yamlNode, 'datacollectionid', datacollectionid);
  YamlAsserts.assertOptionalString(yamlNode, 'fileName', fileName);
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  return <H5Viewer datacollectionid={datacollectionid} fileName={fileName} />;
}
