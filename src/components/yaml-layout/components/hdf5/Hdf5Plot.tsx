import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import Hdf5Plot from 'connect/hdf5/Hdf5Plot';

export default function Yaml(props: YamlComponent) {
  const { providers, yamlNode, events, url, ...unknownOptions } = props;
  YamlAsserts.assertOptionalStringList(yamlNode, 'events', events);
  YamlAsserts.assertString(yamlNode, 'url', url);
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  return <Hdf5Plot providers={providers} options={{ url, events }} />;
}
