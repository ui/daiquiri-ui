import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import SampleDCList from 'connect/samples/SampleDCList';

export default function Yaml(props: YamlComponent) {
  const { providers, yamlNode, selectable, ...unknownOptions } = props;
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  YamlAsserts.assertOptionalBoolean(yamlNode, 'selectable', selectable);
  return <SampleDCList providers={providers} selectable={selectable} />;
}
