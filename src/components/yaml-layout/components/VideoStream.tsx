import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import VideoStream from 'components/general/VideoStream';

export default function Yaml(props: YamlComponent) {
  const { providers, yamlNode, url, ...unknownOptions } = props;
  YamlAsserts.assertString(yamlNode, 'url', url);
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  return <VideoStream url={url} />;
}
