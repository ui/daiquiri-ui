import type { YamlComponent } from '@esrf/daiquiri-lib';
import { YamlAsserts } from '@esrf/daiquiri-lib';
import FileEditor from 'connect/FileEditor';

export default function Yaml(props: YamlComponent) {
  const { providers, yamlNode, ...unknownOptions } = props;
  YamlAsserts.assertNoUnknownKeys(yamlNode, unknownOptions);
  return <FileEditor providers={providers} />;
}
