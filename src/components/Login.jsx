import { createRef, Component } from 'react';
import PropTypes from 'prop-types';

import { Form, Button, Container, Col, Row, Alert } from 'react-bootstrap';

export default class Login extends Component {
  static defaultProps = {
    error: null,
    userError: null
  };

  static propTypes = {
    actions: PropTypes.shape({
      login: PropTypes.func.isRequired
    }).isRequired,
    error: PropTypes.string,
    userError: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      validated: false,
      pending: false
    };

    this.userRef = createRef();
  }

  componentDidMount() {
    this.userRef.current.focus();
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  login = e => {
    e.preventDefault();
    this.setState({ validated: true });

    const form = e.currentTarget;
    if (form.checkValidity() === false) {
      e.stopPropagation();
      return;
    }

    const { username, password } = this.state;

    this.setState({
      pending: true
    });
    this.props.actions.login({ username, password }).catch(() => {
      console.log('could not log user in');
      setTimeout(() => {
        this.setState({
          pending: false
        });
      }, 1000);
    });
  };

  renderError() {
    return (
      <>
        <Alert show={!!this.props.error} variant="danger">
          {typeof this.props.error === 'object' && this.props.error !== null
            ? this.props.error.message
            : this.props.error}
        </Alert>
        <Alert show={!!this.props.userError} variant="danger">
          {this.props.userError}
        </Alert>
      </>
    );
  }

  render() {
    const { validated, pending } = this.state;
    return (
      <Container className="login">
        <Row>
          <Col md="4" sm="12" />
          <Col md="4" sm="12">
            <Container className="mt-3 shadow rounded pb-1 pt-3">
              <div className="mx-auto mb-3 logo logo-150" />
              {this.renderError()}
              <Form noValidate validated={validated} onSubmit={this.login}>
                <Container>
                  <Form.Group as={Row} controlId="username" className="mb-3">
                    <Form.Label column sm={3}>
                      Username
                    </Form.Label>
                    <Col>
                      <Form.Control
                        ref={this.userRef}
                        type="text"
                        placeholder="Username"
                        onChange={this.handleChange}
                        value={this.state.username}
                        name="username"
                        required
                      />
                      <Form.Control.Feedback type="invalid">
                        Please provide a username
                      </Form.Control.Feedback>
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row} controlId="password" className="mb-3">
                    <Form.Label column sm={3}>
                      Password
                    </Form.Label>
                    <Col>
                      <Form.Control
                        type="password"
                        placeholder="Password"
                        onChange={this.handleChange}
                        value={this.state.password}
                        name="password"
                        required
                      />
                      <Form.Control.Feedback type="invalid">
                        Please provide a password
                      </Form.Control.Feedback>
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row} className="mb-3">
                    <Button variant="primary" type="submit" disabled={pending}>
                      {pending && <i className="fa fa-spinner fa-spin me-2" />}
                      Login
                    </Button>
                  </Form.Group>
                </Container>
              </Form>
            </Container>
          </Col>
          <Col md="4" sm="12" />
        </Row>
      </Container>
    );
  }
}
