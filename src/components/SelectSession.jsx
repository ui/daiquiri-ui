import { Component } from 'react';
import PropTypes from 'prop-types';

import { values } from 'lodash';

import { Container, Row, Col, Badge } from 'react-bootstrap';

import Table from 'components/table';

function ActiveCell(cell) {
  if (cell) {
    return <Badge bg="success">Active</Badge>;
  }
}

export default class SelectSession extends Component {
  static propTypes = {
    sessions: PropTypes.objectOf(
      PropTypes.shape({
        session: PropTypes.string,
        startdate: PropTypes.string,
        enddate: PropTypes.string
      })
    )
  };

  onRowClick = (e, row, rowIndex) => {
    this.props.actions.selectSession(row.session);
  };

  rowClasses = (row, rowIndex) => 'pointer';

  render() {
    const columns = [
      { dataField: 'session', text: 'Session' },
      { dataField: 'startdate', text: 'Start' },
      { dataField: 'enddate', text: 'End' },
      {
        dataField: 'active',
        text: '',
        formatter: ActiveCell,
        classes: 'text-end'
      }
    ];

    return (
      <Container className="select-session mt-3">
        <h1>Select a session</h1>
        <Row>
          <Col>
            <Table
              keyField="session"
              data={values(this.props.sessions) || []}
              overlay={false}
              hover
              columns={columns}
              rowClasses={this.rowClasses}
              rowEvents={{ onClick: this.onRowClick }}
              noDataIndication="No sessions found"
            />
          </Col>
        </Row>
      </Container>
    );
  }
}
