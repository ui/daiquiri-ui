class MockCache {
  cache = {};

  load(props) {
    if (!(props.mock in this.cache)) {
      fetch(props.mock).then(resp => {
        resp.json().then(obj => {
          obj[props.keyid] = props.key;
          this.cache[props.mock] = obj;
          props.resolve(obj);
        });
      });
    } else {
      props.resolve(this.cache[props.mock]);
    }
  }
}

export default new MockCache();
