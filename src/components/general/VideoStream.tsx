import React, { useEffect } from 'react';

import type { Player } from '@cycjimmy/jsmpeg-player';
import JSMpeg from '@cycjimmy/jsmpeg-player';

interface Props {
  /** Url to the video stream, either websocket ws(s):// or mjpeg http(s)://  */
  url: string;
}

function VideoStream(props: Props) {
  const canvasRef = React.useRef<HTMLCanvasElement>(null);
  const mplayerRef = React.useRef<Player>();

  useEffect(() => {
    if (canvasRef.current && props.url.startsWith('ws')) {
      mplayerRef.current = new JSMpeg.Player(props.url, {
        canvas: canvasRef.current,
        videoBufferSize: 1024 * 1024 * 4,
        audio: false,
        protocols: [],
      });
    }
    return () => {
      if (mplayerRef.current) {
        mplayerRef.current.destroy();
        mplayerRef.current = undefined;
      }
    };
  }, [props.url]);

  return (
    <>
      {!props.url.startsWith('ws') && (
        <img
          className="video-stream"
          style={{ width: '100%' }}
          src={props.url}
          alt={props.url}
        />
      )}
      {props.url.startsWith('ws') && (
        <canvas
          className="video-stream"
          style={{ width: '100%' }}
          ref={canvasRef}
        />
      )}
    </>
  );
}

export default VideoStream;
