import { Component } from 'react';
import PropTypes from 'prop-types';

import Alerts from 'connect/Alerts';

import { Container } from 'react-bootstrap';

export default class Loader extends Component {
  static defaultProps = {
    children: null
  };

  static propTypes = {
    children: PropTypes.oneOfType([PropTypes.func, PropTypes.element])
  };

  render() {
    return (
      <Container className="loader">
        <Alerts inline />
        <div className="mx-auto mt-4">
          <div className="loader-wrap shadow">
            <div className="mx-auto mb-3 logo logo-150" />
            <p className="text-center">Daiquiri UI</p>
          </div>

          <p className="text-center mt-5">
            {this.props.children && <>{this.props.children}</>}

            {!this.props.children && (
              <>
                <i className="fa fa-spinner fa-pulse fa-2x" />
                <br />
                Starting Up
              </>
            )}
          </p>
        </div>
      </Container>
    );
  }
}
