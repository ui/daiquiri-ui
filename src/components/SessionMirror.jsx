import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { throttle, debounce } from 'lodash';
import { Button } from 'react-bootstrap';

import ConditionalWrap from 'helpers/ConditionalWrap';

function SessionMirror(props) {
  const updateCursor = useCallback(
    throttle(pos => {
      return props.actions.updateCursor(pos);
    }, 200),
    [props.actions.updateCursor]
  );

  const updateWindow = useCallback(
    debounce(pos => {
      return props.actions.updateWindow(pos);
    }, 500),
    [props.actions.updateWindow]
  );

  const setCursor = e => {
    if (!props.mirrorid) updateCursor({ x: e.clientX, y: e.clientY });
  };

  const stopMirror = () => {
    props.actions.requestMirror({ sessionid: null });
  };

  useEffect(() => {
    if (!props.mirrorid) {
      updateWindow({ width: window.innerWidth, height: window.innerHeight });
    }
    function handleResize() {
      if (!props.mirrorid) {
        updateWindow({ width: window.innerWidth, height: window.innerHeight });
      }
    }

    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [props.mirrorid, updateWindow]);

  const style = {};
  if (props.mirrorid) {
    style.width = props.window.width;
    style.height = props.window.height;
    style.overflowY = 'hidden';
  }

  return (
    <>
      {props.mirrorid && (
        <>
          <div className="mirror-overlay" />
          <div className="mirror-overlayinner">
            Mirroring session {props.mirrorid}
            <Button size="sm" className="ms-1" onClick={stopMirror}>
              Stop
            </Button>
          </div>
          <div
            style={{
              position: 'absolute',
              left: props.cursor.x,
              top: props.cursor.y,
              zIndex: 200
            }}
          >
            <i className="fa fa-mouse-pointer" />
          </div>
        </>
      )}
      <ConditionalWrap
        condition={props.mirrorid}
        wrap={children => <div className="mirror-wrapper">{children}</div>}
      >
        <div className="app-wrap" onMouseMove={setCursor} style={style}>
          {props.children}
        </div>
      </ConditionalWrap>
    </>
  );
}

SessionMirror.defaultProps = {
  mirrorid: undefined
};

SessionMirror.propTypes = {
  children: PropTypes.node.isRequired,
  actions: PropTypes.shape({
    updateCursor: PropTypes.func.isRequired,
    updateWindow: PropTypes.func.isRequired,
    requestMirror: PropTypes.func.isRequired
  }).isRequired,
  cursor: PropTypes.shape({
    x: PropTypes.number,
    y: PropTypes.number
  }).isRequired,
  window: PropTypes.shape({
    width: PropTypes.number,
    height: PropTypes.number
  }).isRequired,
  mirrorid: PropTypes.string
};

export default SessionMirror;
