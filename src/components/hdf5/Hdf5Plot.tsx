import { useEffect, useState, useRef, useCallback } from 'react';
import { map, each, values } from 'lodash';
import { Form, Alert } from 'react-bootstrap';
import PlotEnhancer from 'components/PlotEnhancer';
import RemountOnResize from 'components/utils/RemountOnResize';
import type { H5Group } from './models';
import { assertH5Group, H5Dataset } from './models';
import { assertH5Dataset } from './models';
import type { BeamlineNotification } from '../../types/Events';
import { useLastNotification } from '../utils/hooks';

function PlotSelect(props: {
  group: H5Group | null;
  onChange: (value: string) => void;
}) {
  if (!props.group) return null;
  if (values(props.group.children).length === 1) return null;

  return (
    <Form.Control
      as="select"
      size="sm"
      onChange={(e) => props.onChange(e.target.value)}
    >
      {map(props.group.children, (group) => (
        <option key={group.name} value={group.name}>
          {group.name}
        </option>
      ))}
    </Form.Control>
  );
}

/** Normalize NXdata axes and signal reading */
function readCharAsList(attr: any): string[] | null {
  if (attr === undefined) {
    return null;
  }
  if (typeof attr === 'string') {
    return [attr];
  }
  return attr;
}

/**
 * Exception returned whenever the parsing of the data do not result on any plot
 */
class NoPlotCanBeParsed extends Error {
  public constructor(message: string) {
    super(message);
    Object.setPrototypeOf(this, NoPlotCanBeParsed.prototype);
  }
}

function createCurvePlotFromNxDataGroup(
  group: H5Group
): [Plotly.Data[], Partial<Plotly.Layout>] {
  const layout = {
    xaxis: {
      title: { text: '' },
    },
    yaxis: {
      title: { text: '' },
      type: 'linear' as Plotly.AxisType,
      showexponent: 'all' as const,
      exponentformat: 'e' as const,
    },
  };

  const axes = readCharAsList(group.attrs.axes);
  if (!axes) {
    throw new NoPlotCanBeParsed('No axes found in NXdata');
  }
  const data: Plotly.Data[] = [];
  if (axes.length !== 1) {
    throw new NoPlotCanBeParsed(
      `Expect 1 and only 1 axis, found ${axes.length}`
    );
  }
  const xdataset = group.children[group.attrs.axes[0]];
  assertH5Dataset(xdataset);
  const x = xdataset.data;
  layout.xaxis.title.text = group.children[group.attrs.axes[0]].attrs.units;

  const ydataset = group.children[group.attrs.signal];
  assertH5Dataset(ydataset);
  const y = ydataset.data;
  layout.yaxis.title.text = group.children[group.attrs.signal].attrs.units;

  if (group.attrs.SILX_style) {
    const style = JSON.parse(group.attrs.SILX_style);
    if (style.signal_scale_type === 'log') {
      layout.yaxis.type = 'log' as Plotly.AxisType;
    }
  }

  const sc: Plotly.Data = {
    x,
    y,
    type: 'scattergl',
    name: group.attrs.signal,
  };

  data.push(sc);

  const auxs = readCharAsList(group.attrs.auxiliary_signals);
  if (auxs) {
    each(auxs, (aux) => {
      const auxDataset = group.children[aux];
      assertH5Dataset(auxDataset);
      const y = auxDataset.data;
      const asc: Plotly.Data = {
        x,
        y,
        yaxis: 'y2',
        type: 'scattergl',
        name: aux,
      };

      data.push(asc);
    });
  }

  if (data.length === 0) {
    throw new NoPlotCanBeParsed('No data was found');
  }

  return [data, layout];
}

function createImagePlotFromNxDataGroup(
  group: H5Group
): [Plotly.Data[], Partial<Plotly.Layout>] {
  const signal = readCharAsList(group.attrs.signal);
  if (!signal) {
    throw new NoPlotCanBeParsed('No signal found in NXdata');
  }
  if (signal.length !== 1) {
    throw new NoPlotCanBeParsed(
      `Expect one and only one signal, found ${signal.length}`
    );
  }

  const image = group.children[signal[0]];
  assertH5Dataset(image);
  if (!image) {
    throw new NoPlotCanBeParsed(
      `Dataset from signal ${signal[0]} does not exists`
    );
  }

  if (image.ndim !== 2) {
    throw new NoPlotCanBeParsed(
      `Dataset from signal ${signal[0]} must be 2 ndim (found ${image.ndim})`
    );
  }

  const data: Plotly.Data[] = [
    {
      z: image.data,
      type: 'heatmapgl',
    },
  ];

  return [data, {}];
}

function createPlotFromNxDataGroup(
  group: H5Group
): [Plotly.Data[], Partial<Plotly.Layout>] {
  let errors = '';
  try {
    return createImagePlotFromNxDataGroup(group);
  } catch (error) {
    if (error instanceof NoPlotCanBeParsed) {
      errors += `Image plot: ${error.message}`;
    } else {
      throw error;
    }
  }
  try {
    return createCurvePlotFromNxDataGroup(group);
  } catch (error) {
    if (error instanceof NoPlotCanBeParsed) {
      errors += `Curve plot: ${error.message}`;
    } else {
      throw error;
    }
  }
  throw new NoPlotCanBeParsed(errors);
}

function createPlotFromGroup(
  group: H5Group
): [Plotly.Data[], Partial<Plotly.Layout>] {
  if (group?.attrs.NX_class === 'NXdata') {
    return createPlotFromNxDataGroup(group);
  }
  throw new NoPlotCanBeParsed('Only NXdata group is supported');
}

interface Props {
  error?: string;
  group?: H5Group;
  params?: {
    autoprocprogramid?: number;
    datacollectionid?: number;
  };
  autoprocprogramid?: number;
  datacollectionid?: number;
  options: {
    /** Hdf5 uri to the group of interest */
    uri: string;
    events?: string[];
  };
  /** Whether to poll the group data, poll time in seconds */
  poll?: number;
  actions?: {
    setParams?: (
      params: {
        type?: string;
        uri?: string;
        datacollectionid?: number | null;
        autoprocprogramid?: number;
      },
      flush?: boolean
    ) => Promise<void>;
  };
}

export default function Hdf5Plot(props: Props) {
  const refreshThread = useRef<NodeJS.Timeout>();
  const [selectedPlot, setSelectedPlot] = useState<string | undefined>(
    props.group?.attrs?.default as string | undefined
  );
  const {
    autoprocprogramid = null,
    datacollectionid = null,
    params = {},
    poll = 0,
    options,
  } = props;

  const event = useLastNotification(props.options.events);

  const refresh = useCallback(
    (newpoll?: number) => {
      console.log('refresh');
      props.actions
        ?.setParams?.(
          {
            type: 'processing',
            uri: options.uri,
          },
          true
        )
        .then(() => {
          if (newpoll) {
            refreshThread.current = setTimeout(
              () => refresh(newpoll),
              newpoll * 1000
            );
          }
        })
        .catch((error) => {
          console.log('Could not fetch group data');
        });
    },
    [options.uri, props.actions]
  );

  useEffect(() => {
    if (event !== null) {
      // If in follow mode (=== null) auto load
      if (datacollectionid === null) {
        if (props.actions?.setParams) {
          props.actions.setParams({
            datacollectionid: event?.datacollectionid,
          });
          refresh();
        }
      }
    }
  }, [
    autoprocprogramid,
    datacollectionid,
    event,
    props.actions,
    options.uri,
    refresh,
  ]);

  useEffect(() => {
    const newparams: {
      datacollectionid?: number;
      autoprocprogramid?: number;
    } = {};
    if (datacollectionid) {
      newparams.datacollectionid = datacollectionid;
    }
    if (autoprocprogramid) {
      newparams.autoprocprogramid = autoprocprogramid;
    }

    if (
      params.datacollectionid !== newparams.datacollectionid ||
      params.autoprocprogramid !== newparams.autoprocprogramid
    ) {
      if (props.actions?.setParams) {
        props.actions.setParams(newparams);
        refresh(poll);
      }
    }
  }, [
    autoprocprogramid,
    datacollectionid,
    poll,
    props.actions,
    params,
    refresh,
  ]);

  useEffect(() => {
    return () => {
      if (refreshThread.current) clearTimeout(refreshThread.current);
    };
  }, []);

  useEffect(() => {
    if (!selectedPlot && props.group) {
      if (props.group.attrs.default) {
        setSelectedPlot(props.group.attrs.default as string);
      }
    }
  }, [props.group, selectedPlot]);

  if (autoprocprogramid === null && datacollectionid === null) {
    return <p>Select a processing program output</p>;
  }

  if (props.error) {
    const id = datacollectionid || autoprocprogramid;
    return (
      <Alert variant="warning">
        Could not load data for processing program id &quot;
        {id}&quot; with uri &quot;{options.uri}&quot;
      </Alert>
    );
  }

  if (!props.group) {
    return (
      <Alert variant="info">
        <p>Waiting for data</p>
      </Alert>
    );
  }

  const group = selectedPlot
    ? props.group?.children[selectedPlot]
    : props.group?.children[props.group?.attrs.default as string] ||
      props.group ||
      null;

  if (!group) {
    // FIXME it would be better to display something
    return (
      <Alert variant="info">
        <p>No data to display</p>
      </Alert>
    );
  }

  assertH5Group(group);
  try {
    const [data, layout] = createPlotFromGroup(group);
    return (
      <div className="plot1d-container">
        {selectedPlot && (
          <PlotSelect group={props.group} onChange={setSelectedPlot} />
        )}
        <RemountOnResize>
          <PlotEnhancer data={data} layout={layout} />
        </RemountOnResize>
      </div>
    );
  } catch (error) {
    if (error instanceof NoPlotCanBeParsed) {
      return (
        <Alert variant="error">
          <p>Data format unsupported:</p>
          <p>{error.message}</p>
        </Alert>
      );
    }
    throw error;
  }
}
