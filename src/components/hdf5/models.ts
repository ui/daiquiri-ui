import type { NdArray, TypedArray } from 'ndarray';
import { toNdArray } from '../../helpers/ndarray';

/**
 * HDF5 dataset as exposed by the HDF5 Redux provider
 */
export interface H5Dataset {
  name: string;
  attrs: {
    [name: string]: any;
  };
  ndim: number;
  shape: number[];
  data: any;
  dtype: string;
}

/**
 * HDF5 group as exposed by the HDF5 Redux provider
 */
export interface H5Group {
  name: string;
  attrs: {
    [name: string]: any;
  };
  children: {
    [name: string]: H5Dataset | H5Group;
  };
}

/**
 * Read a dataset as a typed array.
 */
export function datasetToNdarray(dataset: H5Dataset): NdArray<TypedArray> {
  const { data, shape, dtype } = dataset;

  const normalizedDtypes: Record<string, string> = {
    float16: '<f2',
    float32: '<f4',
    float64: '<f8',
    uint8: 'u1',
    uint16: '<u2',
    uint32: '<u4',
    uint64: '<u8',
    int8: 'i1',
    int16: '<i2',
    int32: '<i4',
    int64: '<i8',
  };
  const normalizedDtype = normalizedDtypes[dtype];
  if (normalizedDtype === undefined) {
    throw new Error(`Unsupported dtype: ${dtype}`);
  }
  // Flatten source array
  const flatData = data.flat(Infinity);
  return toNdArray(flatData, shape, normalizedDtypes[dtype]);
}

export function isH5Dataset(entity: unknown): entity is H5Dataset {
  return entity instanceof Object && 'data' in entity;
}

export function assertH5Dataset(
  entity: H5Dataset | H5Group,
  message = 'Expected H5Dataset'
): asserts entity is H5Dataset {
  if (!isH5Dataset(entity)) {
    throw new Error(message);
  }
}

export function isH5Group(entity: unknown): entity is H5Group {
  return entity instanceof Object && 'children' in entity;
}

export function assertH5Group(
  entity: H5Dataset | H5Group,
  message = 'Expected H5Group'
): asserts entity is H5Group {
  if (!isH5Group(entity)) {
    throw new Error(message);
  }
}
