Plot from HDF5 data.

It supposts few kind of HDF5 groups and NXdata.

- `uri` should be passed to the component to specify the group to load
- `event` can be specified to wait for a processing

The following kind can be expended to supposet other use cases.

## NXdata curves

A NXdata containing curves.

```js
import plot from 'components/mocks/Hdf5PlotGroup.json';

const props = {
  group: plot['children']['plot_0'],
  autoprocprogramid: 1,
};

<div style={{ height: 300 }}>
  <Hdf5Plot {...props} />
</div>;
```

## NXdata image

A NXdata containing an image.

```js
import plot from 'components/mocks/Hdf5PlotNXdataImage.json';

const props = {
  group: plot,
  autoprocprogramid: 1,
};

<div style={{ height: 300 }}>
  <Hdf5Plot {...props} />
</div>;
```

## Group

A group containing different NXdata. In this case a selector is displayed to
allow to select one of the sub groups. The nexus `default` attribute is used
to select the default plot.

```js
import plot from 'components/mocks/Hdf5PlotGroup.json';

const props = {
  group: plot,
  autoprocprogramid: 1,
};

<div style={{ height: 300 }}>
  <Hdf5Plot {...props} />
</div>;
```
