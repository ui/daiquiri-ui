import { createRef, Component } from 'react';
import PropTypes from 'prop-types';
import { map, sum, find } from 'lodash';
import moment from 'moment';

import {
  Badge,
  Button,
  Row,
  Col,
  Container,
  OverlayTrigger,
  Form,
  Tooltip
} from 'react-bootstrap';
import { Formatting } from '@esrf/daiquiri-lib';

import Confirm from 'helpers/Confirm';
import Table from 'components/table';
import momentCell from 'components/table/cells/momentCell';
import hrsMinsCell from 'components/table/cells/hrsMinsCell';
import ButtonTriggerModalStore from 'connect/layout/ButtonTriggerModalStore';
import CopyScan from 'connect/samples/CopyScan';
import LogViewer from 'components/LogViewer';

const queueItem = {
  name: PropTypes.string.isRequired,
  uid: PropTypes.string.isRequired,
  created: PropTypes.number.isRequired,
  args: PropTypes.shape({
    sampleid: PropTypes.number.isRequired,
    subsampleid: PropTypes.number
  }).isRequired
};

function argFormatter(cell) {
  if (typeof cell === 'object') {
    const columns = [
      { dataField: 'key', text: 'Key' },
      {
        dataField: 'value',
        text: 'Value',
        formatter: argFormatter,
        classes: 'queue-item-args'
      }
    ];

    const args = map(cell, (v, k) => ({ key: k, value: v }));

    return (
      <Table
        keyField="key"
        data={args}
        columns={columns}
        bordered={false}
        classes="table-sm"
        striped
        noDataIndication="No arguments"
      />
    );
  }
  return <span>{cell}</span>;
}

class QueueItemDetail extends Component {
  static propTypes = queueItem;

  render() {
    return (
      <Container>
        <Row>
          <Col xs={2}>Type</Col>
          <Col>{this.props.name}</Col>
        </Row>
        <Row>
          <Col xs={2}>UUID</Col>
          <Col>{this.props.uid}</Col>
        </Row>
        <Row>
          <Col xs={2}>Created</Col>
          <Col>
            {moment.unix(this.props.created).format('DD-MM-YYYY HH:mm:ss')}
          </Col>
        </Row>
        <Row>
          <Col xs={2}>Sample</Col>
          <Col>{this.props.args.sampleid}</Col>
        </Row>
        {this.props.args.subsampleid && (
          <Row>
            <Col xs={2}>Subsample</Col>
            <Col>{this.props.args.subsampleid}</Col>
          </Row>
        )}
        <Row>
          <Col xs={2}>Arguments</Col>
          <Col>{argFormatter(this.props.args)}</Col>
        </Row>
        <Row>
          <Col xs={2}>Output</Col>
          <Col>
            <div className="log-viewer-wrap">
              <div className="log-viewer">{this.props.stdout}</div>
            </div>
          </Col>
        </Row>
      </Container>
    );
  }
}

function QueueItemButton(props) {
  return (
    <ButtonTriggerModalStore
      id={`queueItemDetail${props.uid}${props.prefix}`}
      button={<i className="fa fa-search" />}
      buttonProps={{
        className: `me-1${props.inset ? ' float-end mb-3' : ''}`,
        variant: 'info',
        size: props.inset ? '' : 'sm'
      }}
      title="Queue Item Details"
    >
      <QueueItemDetail {...props} />
    </ButtonTriggerModalStore>
  );
}

QueueItemButton.defaultProps = {
  inset: false,
  prefix: ''
};

QueueItemButton.propTypes = {
  inset: PropTypes.bool,
  uid: PropTypes.string.isRequired,
  prefix: PropTypes.string
};

function statusCell(cell, row) {
  const variants = {
    finished: { bg: 'success', title: 'Finished', icon: 'check' },
    failed: {
      bg: 'danger',
      title: 'Failed',
      icon: 'exclamation-triangle'
    },
    running: { bg: 'info', title: 'Running', icon: 'cog fa-spin' },
    queued: { bg: 'warning', title: 'Queued', icon: 'pause' }
  };

  const v = variants[row.status] || variants.queued;
  return (
    <>
      <Badge className="me-1" bg={v.bg}>
        {v.title}
      </Badge>
      <QueueItemButton {...row} />
    </>
  );
}

function sampleCell(cell, row) {
  return (
    <span>
      {row.args.sample && <>{row.args.sample} [</>}
      {row.args.sampleid}
      {row.args.subsampleid && <>-{row.args.subsampleid}</>}
      {row.args.sample && <>]</>}
    </span>
  );
}

class QueueItemButtons extends Component {
  static defaultProps = {
    selectedItem: undefined
  };

  static propTypes = {
    selectedItem: PropTypes.string,
    operator: PropTypes.bool.isRequired,
    stack: PropTypes.arrayOf(PropTypes.shape(queueItem)).isRequired,
    all: PropTypes.arrayOf(PropTypes.shape(queueItem)).isRequired,
    actions: PropTypes.shape({
      moveItem: PropTypes.func.isRequired,
      removeItem: PropTypes.func.isRequired
    }).isRequired
  };

  render() {
    const item = find(this.props.all, { uid: this.props.selectedItem });
    const itemInStack = find(this.props.stack, {
      uid: this.props.selectedItem
    });
    const idx = this.props.stack.indexOf(itemInStack);

    const { operator, selectedItem, stack } = this.props;

    return (
      <div className="text-nowrap">
        <OverlayTrigger
          placement="top"
          overlay={<Tooltip>Create a new scan from this one</Tooltip>}
        >
          <CopyScan
            sampleid={item && item.args.sampleid}
            subsampleid={item && item.args.subsampleid}
            args={item && item.args}
            type={item && item.name}
            inset
            disabled={!selectedItem || !operator}
          />
        </OverlayTrigger>
        <OverlayTrigger
          placement="top"
          overlay={<Tooltip>Move item to top</Tooltip>}
        >
          <Button
            size="sm"
            className="me-1"
            onClick={() =>
              this.props.actions.moveItem({
                uid: selectedItem,
                position: 0
              })
            }
            disabled={
              (item && item.status !== 'queued') || idx === 0 || !operator
            }
          >
            <i className="fa fa-arrow-circle-up" />
          </Button>
        </OverlayTrigger>
        <OverlayTrigger
          placement="top"
          overlay={<Tooltip>Move item up</Tooltip>}
        >
          <Button
            size="sm"
            className="me-1"
            onClick={() =>
              this.props.actions.moveItem({
                uid: selectedItem,
                position: idx - 1
              })
            }
            disabled={
              (item && item.status !== 'queued') || idx === 0 || !operator
            }
          >
            <i className="fa fa-arrow-up" />
          </Button>
        </OverlayTrigger>

        <OverlayTrigger
          placement="top"
          overlay={<Tooltip>Move item down</Tooltip>}
        >
          <Button
            size="sm"
            className="me-1"
            onClick={() =>
              this.props.actions.moveItem({
                uid: selectedItem,
                position: idx + 1
              })
            }
            disabled={
              (item && item.status !== 'queued') ||
              idx === stack.length - 1 ||
              !operator
            }
          >
            <i className="fa fa-arrow-down" />
          </Button>
        </OverlayTrigger>

        <OverlayTrigger
          placement="top"
          overlay={<Tooltip>Delete item</Tooltip>}
        >
          <Confirm
            title="Delete Queue Item"
            message="Are you sure you want to delete this item? This operation cannot be undone"
          >
            {confirm => (
              <Button
                variant="danger"
                size="sm"
                onClick={confirm(() =>
                  this.props.actions.removeItem(selectedItem)
                )}
                disabled={
                  (item && item.status !== 'queued') ||
                  !selectedItem ||
                  !operator
                }
              >
                <i className="fa fa-times" />
              </Button>
            )}
          </Confirm>
        </OverlayTrigger>
      </div>
    );
  }
}

export default class Queue extends Component {
  static defaultProps = {
    queue: {
      stack: [],
      all: [],
      current: undefined
    },
    selectedItem: undefined
  };

  static propTypes = {
    actions: PropTypes.shape({
      fetchQueue: PropTypes.func.isRequired,
      setQueueStatus: PropTypes.func.isRequired,
      updateQueue: PropTypes.func.isRequired,
      removeItem: PropTypes.func.isRequired,
      moveItem: PropTypes.func.isRequired,
      addPause: PropTypes.func.isRequired,
      addSleep: PropTypes.func.isRequired,
      selectItem: PropTypes.func.isRequired,
      clear: PropTypes.func.isRequired,
      kill: PropTypes.func.isRequired,
      setParams: PropTypes.func.isRequired
    }).isRequired,
    running: PropTypes.bool.isRequired,
    ready: PropTypes.bool.isRequired,
    queue: PropTypes.shape({
      stack: PropTypes.arrayOf(PropTypes.object),
      all: PropTypes.arrayOf(PropTypes.object),
      current: PropTypes.object,
      pause_on_fail: PropTypes.bool
    }),
    selectedItem: PropTypes.string,
    params: PropTypes.shape({
      status: PropTypes.string
    }).isRequired,
    fetched: PropTypes.bool.isRequired,
    operator: PropTypes.bool.isRequired
  };

  constructor(props) {
    super(props);
    this.props.actions.fetchQueue();

    this.sleepRef = createRef();
  }

  toggle = () => {
    this.props.actions.setQueueStatus(!this.props.running);
  };

  togglePauseOnFail = e => {
    this.props.actions.updateQueue({
      setting: 'pause_on_fail',
      value: e.target.checked
    });
  };

  removeItem = queueid => {
    this.props.actions.removeItem(queueid);
  };

  moveItem = props => {
    this.props.actions.moveItem(props);
  };

  addPause = () => {
    this.props.actions.addPause();
  };

  addSleep = () => {
    this.props.actions.addSleep({ time: this.sleepRef.current.value });
  };

  clearQueue = () => {
    this.props.actions.clear();
  };

  killQueue = () => {
    this.props.actions.kill();
  };

  rowClasses = row => {
    const classes = ['pointer'];

    if (row.uid === this.props.selectedItem) classes.push('table-info');
    classes.push(row.status);

    return classes.join(' ');
  };

  onRowClick = (e, row) => {
    if (['i', 'button'].indexOf(e.target.tagName.toLowerCase()) > -1) return;
    this.props.actions.selectItem(row.uid);
  };

  filter(value) {
    const v = this.props.params.status === value ? null : value;
    this.props.actions.setParams(
      {
        status: v
      },
      true
    );
  }

  render() {
    const columns = [
      { dataField: 'name', text: 'Type' },
      { dataField: 'args.subsampleid', text: 'Sample', formatter: sampleCell },
      {
        dataField: 'created',
        text: 'Created',
        formatter: momentCell,
        formatExtraData: { format: 'DD-MM HH:mm:ss' }
      },
      { dataField: 'estimate', text: 'Estimated', formatter: hrsMinsCell },
      {
        dataField: '',
        text: '',
        formatter: statusCell,
        classes: 'text-end text-nowrap'
      }
    ];

    const timeEst = Formatting.toHoursMins(
      sum(map(this.props.queue.stack, qi => qi.estimate))
    );

    return (
      <>
        {this.props.fetched && (
          <div className="queue">
            <Container className="collapse-margins">
              <Row>
                <Col>
                  State is currently:{' '}
                  <Badge
                    bg={this.props.running ? 'success' : 'warning'}
                    className="me-1"
                  >
                    {this.props.running ? 'Running' : 'Paused'}
                  </Badge>
                  <Badge bg={this.props.ready ? 'success' : 'warning'}>
                    {this.props.ready ? 'Ready' : 'Waiting'}
                  </Badge>
                  <Form.Switch
                    disabled={!this.props.operator}
                    label="Pause on Error"
                    id="error-pause"
                    onClick={this.togglePauseOnFail}
                    defaultChecked={this.props.queue.pause_on_fail}
                  />
                </Col>
                <Col xs={3} className="text-end">
                  <Button
                    variant={this.props.running ? 'warning' : 'success'}
                    onClick={this.toggle}
                    className="me-1"
                    disabled={!this.props.operator}
                    title={
                      this.props.running
                        ? 'Pause the queue after the current item'
                        : 'Start the queue'
                    }
                  >
                    <i
                      className={`fa fa-${
                        this.props.running ? 'pause' : 'play'
                      }`}
                    />
                  </Button>
                  <Confirm
                    title="Empty Queue"
                    message="Are you sure you want to empty the queue? This will delete all queued items and cannot be undone"
                  >
                    {confirm => (
                      <Button
                        variant="danger"
                        onClick={confirm(this.clearQueue)}
                        disabled={!this.props.operator}
                        title="Clear Queue"
                      >
                        <i className="fa fa-trash" />
                      </Button>
                    )}
                  </Confirm>
                </Col>
              </Row>
            </Container>

            <div>
              <h4>Current Item</h4>
              <Container className="collapse-margins">
                {this.props.queue.current && (
                  <>
                    <Confirm
                      title="Kill Running Queue Item"
                      message="Are you sure you want to kill the currently running queue item? This could result in loss of data"
                    >
                      {confirm => (
                        <Button
                          variant="danger"
                          onClick={confirm(this.killQueue)}
                          className="float-end"
                          disabled={
                            !this.props.operator ||
                            (!this.props.running && !this.props.queue.current)
                          }
                          title="Kill current queue item"
                        >
                          <i className="fa fa-stop" />
                        </Button>
                      )}
                    </Confirm>
                    <QueueItemButton
                      prefix="running"
                      {...this.props.queue.current}
                      inset
                    />
                    <Row>
                      <Col>Name</Col>
                      <Col>{this.props.queue.current.name}</Col>
                    </Row>
                    <Row>
                      <Col>Sample</Col>
                      <Col>{sampleCell(null, this.props.queue.current)}</Col>
                    </Row>
                    <Row>
                      <Col>Estimated</Col>
                      <Col>
                        {Formatting.toHoursMins(
                          this.props.queue.current.estimate
                        )}
                      </Col>
                    </Row>
                    {this.props.actorLog[this.props.queue.current.uid] && (
                      <>
                        <Row>
                          <Col>
                            <LogViewer
                              log={
                                this.props.actorLog[
                                  this.props.queue.current.uid
                                ]
                              }
                            />
                          </Col>
                        </Row>
                      </>
                    )}
                  </>
                )}
              </Container>
              {this.props.queue.current === null && (
                <span>No queue item running</span>
              )}
            </div>

            <h4 className="mt-2">Queue Items</h4>
            <div className="clearfix">
              <OverlayTrigger
                placement="top"
                overlay={<Tooltip>Add a pause to the queue</Tooltip>}
              >
                <Button
                  size="sm"
                  onClick={this.addPause}
                  className="float-end mb-1"
                  disabled={!this.props.operator}
                >
                  <i className="fa fa-plus me-1" />
                  <i className="fa fa-pause" />
                </Button>
              </OverlayTrigger>

              <OverlayTrigger
                placement="top"
                overlay={<Tooltip>Add a sleep to the queue</Tooltip>}
              >
                <Button
                  size="sm"
                  onClick={this.addSleep}
                  className="float-end mb-1 me-1"
                  disabled={!this.props.operator}
                >
                  <i className="fa fa-plus me-1" />
                  <i className="fa fa-hourglass-half" />
                </Button>
              </OverlayTrigger>

              <Form className="float-end me-1">
                <Form.Control as="select" size="sm" ref={this.sleepRef}>
                  {map([5, 10, 20, 30, 60, 120, 300], t => (
                    <option value={t} key={t}>
                      {t}
                    </option>
                  ))}
                </Form.Control>
              </Form>

              <span>
                Estimated time:
                {timeEst}
              </span>
            </div>

            <Container className="collapse-margins">
              <Row>
                <Col>
                  <Button
                    className="mb-1"
                    size="sm"
                    variant={
                      this.props.params.status === 'Finished'
                        ? 'primary'
                        : 'secondary'
                    }
                    onClick={() => this.filter('Finished')}
                  >
                    Finished
                  </Button>
                </Col>
                <Col className="text-end">
                  <QueueItemButtons
                    actions={{
                      moveItem: this.moveItem,
                      removeItem: this.removeItem
                    }}
                    selectedItem={this.props.selectedItem}
                    all={this.props.queue.all}
                    stack={this.props.queue.stack}
                    operator={this.props.operator}
                  />
                </Col>
              </Row>
            </Container>

            <Table
              localPerPage={15}
              keyField="uid"
              data={this.props.queue.all}
              columns={columns}
              bordered={false}
              classes="table-sm queue"
              striped
              noDataIndication="No items in queue"
              rowClasses={this.rowClasses}
              rowEvents={{ onClick: this.onRowClick }}
            />
          </div>
        )}
      </>
    );
  }
}
