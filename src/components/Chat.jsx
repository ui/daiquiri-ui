import { createRef, Component } from 'react';
import PropTypes from 'prop-types';

import { map, values, each } from 'lodash';
import moment from 'moment';
import classNames from 'classnames';

import { OverlayTrigger, Tooltip, Form } from 'react-bootstrap';

const messageProps = {
  messageid: PropTypes.number.isRequired,
  timestamp: PropTypes.number.isRequired,
  message: PropTypes.string.isRequired,
  user: PropTypes.string.isRequired,
  read: PropTypes.arrayOf(PropTypes.string).isRequired
};

const scrollProps = {
  ref: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.instanceOf(Element) })
  ])
};

const sessionsProps = {};

class Message extends Component {
  static defaultProps = {
    chatCursor: undefined
  };

  static propTypes = {
    messagesInView: PropTypes.bool.isRequired,
    scroll: PropTypes.shape(scrollProps).isRequired,
    message: PropTypes.shape(messageProps).isRequired,
    sessionid: PropTypes.string.isRequired,
    sessions: PropTypes.arrayOf(PropTypes.shape(sessionsProps)).isRequired,
    markRead: PropTypes.func.isRequired,
    setCursor: PropTypes.func.isRequired,
    chatCursor: PropTypes.number,
    unread: PropTypes.number.isRequired,
    atBottom: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.messageRef = createRef();

    this.observer = new IntersectionObserver(
      entries => {
        entries.forEach(entry => {
          if (entry.isIntersecting && this.props.messagesInView) {
            this.setRead();
          }
        });
      },
      { threshold: 1, root: this.props.scroll.ref.current }
    );
  }

  componentDidMount() {
    this.setObserve();
  }

  componentDidUpdate(prevProps) {
    if (this.props.messagesInView !== prevProps.messagesInView) {
      this.setObserve();
    }
  }

  setObserve() {
    if (this.props.messagesInView) {
      this.observer.observe(this.messageRef.current);
    } else {
      this.observer.unobserve(this.messageRef.current);
    }
  }

  setRead() {
    if (this.props.message.read.indexOf(this.props.sessionid) === -1) {
      this.props.markRead(this.props.message.messageid);
      this.props.setCursor(this.props.message.messageid);
    }
  }

  render() {
    const { message } = this.props;
    let read = true;
    const readBy = [];
    each(this.props.sessions, s => {
      if (message.read.indexOf(s.sessionid) === -1) {
        read = false;
      } else if (s.sessionid !== this.props.sessionid) {
        readBy.push(s.data.username);
      }
    });

    return (
      <div
        className={classNames('message', {
          'message-author': this.props.sessionid === message.sessionid,
          'message-lastread':
            this.props.chatCursor === message.messageid &&
            this.props.unread &&
            !this.props.atBottom()
        })}
        ref={this.messageRef}
      >
        <div className="message-title">
          <span className="message-user">{message.user}</span>
          <span className="message-time">
            {moment.unix(message.timestamp).format('HH:mm')}
          </span>
          <OverlayTrigger
            placement="left"
            overlay={
              <Tooltip>
                {readBy.length === 0 && <p>Not yet read</p>}
                {readBy.length > 0 && (
                  <p>
                    Read by {read && 'everyone'}: {readBy.join(', ')}
                  </p>
                )}
              </Tooltip>
            }
          >
            <span className="message-read">
              {readBy.length > 0 && !read && (
                <i className="fa fa-check fa-fixed partial" />
              )}
              {read && <i className="fa fa-check fa-fixed read" />}
              {!read && readBy.length === 0 && (
                <i className="fa fa-clock-o fa-fixed waiting" />
              )}
            </span>
          </OverlayTrigger>
        </div>
        <div className="message-body">{message.message}</div>
      </div>
    );
  }
}

export default class Chat extends Component {
  static defaultProps = {
    loadCursor: null
  };

  static propTypes = {
    actions: PropTypes.shape({
      fetchMessages: PropTypes.func.isRequired,
      sendMessage: PropTypes.func.isRequired,
      markRead: PropTypes.func.isRequired,
      updateCache: PropTypes.func.isRequired
    }).isRequired,
    messages: PropTypes.objectOf(PropTypes.shape(messageProps)).isRequired,
    scroll: PropTypes.shape(scrollProps).isRequired,
    sessions: PropTypes.arrayOf(PropTypes.shape(sessionsProps)).isRequired,
    sessionid: PropTypes.string.isRequired,
    cache: PropTypes.shape({
      chatCursor: PropTypes.number
    }).isRequired,
    unread: PropTypes.number.isRequired,
    loadCursor: PropTypes.number
  };

  constructor(props) {
    super(props);

    this.topRef = createRef();
    this.bottomRef = createRef();
    this.messagesRef = createRef();
    this.messageRefs = {};

    this.state = {
      inView: false,
      bottomInView: false
    };

    this.lastSetCursor = 0;
    this.setCursorTimeout = null;
    this.lastAtBottom = false;
    this.scrollTimeout = null;
  }

  componentDidMount() {
    this.props.actions.fetchMessages().then(() => {
      this.scrollToCurrent();
    });

    this.observer = new IntersectionObserver(entries => {
      entries.forEach(entry => {
        this.setState({ inView: entry.isIntersecting });
      });
    });
    this.observer.observe(this.messagesRef.current);

    this.topObserver = new IntersectionObserver(entries => {
      entries.forEach(entry => {
        if (this.state.inView && entry.isIntersecting && !this.nowInView) {
          console.log('top in view');
          this.loadPrev();
        }
      });
    });
    this.topObserver.observe(this.topRef.current);

    this.bottomObserver = new IntersectionObserver(entries => {
      entries.forEach(entry => {
        if (this.state.inView) {
          this.setState({ bottomInView: entry.isIntersecting });
        }
      });
    });
    this.bottomObserver.observe(this.bottomRef.current);

    if (this.props.loadCursor) {
      this.setCursor(this.props.loadCursor);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.messages !== this.props.messages) this.scrollToBottom();

    if (
      this.props.unread > prevProps.unread &&
      this.props.unread > 0 &&
      (!this.atBottom() || !this.state.inView)
    ) {
      const audio = new Audio('/droplet.mp3');
      audio.play();
    }

    if (prevState.inView !== this.state.inView && this.state.inView) {
      this.scrollToCurrent();
      this.nowInView = true;
      clearTimeout(this.nowInViewTimeout);
      this.nowInViewTimeout = setTimeout(() => {
        this.nowInView = false;
      }, 200);
    }

    if (prevProps.loadCursor !== this.props.loadCursor) {
      if (this.props.loadCursor) {
        this.setCursor(this.props.loadCursor);
      }
    }
  }

  componentWillUnmount() {
    this.observer.unobserve(this.messagesRef.current);
    this.topObserver.unobserve(this.topRef.current);
    this.bottomObserver.unobserve(this.bottomRef.current);
  }

  onKeyPress = e => {
    if (e.key === 'Enter') {
      if (e.target.value) {
        this.props.actions.sendMessage({ message: e.target.value }).then(() => {
          setTimeout(() => {
            this.bottomRef.current.scrollIntoView({ behavior: 'smooth' });
          }, 200);
        });
        e.target.value = '';
      }
    }
  };

  setCursor = messageid => {
    clearTimeout(this.setCursorTimeout);
    if (messageid > this.lastSetCursor) {
      this.lastSetCursor = messageid;
    }
    this.setCursorTimeout = setTimeout(() => {
      this.doSetCursor(this.lastSetCursor);
    }, 500);
  };

  setMessageRef(messageid, element) {
    this.messageRefs[messageid] = element;
  }

  doSetCursor = messageid => {
    if (
      this.props.cache.chatCursor === undefined ||
      messageid > this.props.cache.chatCursor
    ) {
      this.props.actions.updateCache({
        cache: {
          ...this.props.cache,
          chatCursor: messageid
        }
      });
    }
  };

  atBottom = () => {
    if (this.lastAtBottom) return true;
    return this.state.bottomInView;
  };

  scrollToBottom() {
    if (this.atBottom()) {
      this.lastAtBottom = true;
      this.bottomRef.current.scrollIntoView({ behavior: 'smooth' });

      clearTimeout(this.scrollTimeout);
      this.scrollTimeout = setTimeout(() => {
        this.lastAtBottom = false;
      }, 200);
    }
  }

  scrollToCurrent() {
    if (!this.props.messages.length) return;
    if (this.props.cache.chatCursor === undefined) {
      this.scrollToBottom();
    } else {
      this.messageRefs[
        this.props.cache.chatCursor
      ].messageRef.current.scrollIntoView();
    }
  }

  loadPrev() {
    const lastId = values(this.props.messages)[0].messageid;
    this.props.actions
      .fetchMessages({
        offset: values(this.props.messages).length
      })
      .then(() => {
        this.messageRefs[lastId].messageRef.current.scrollIntoView();
      });
  }

  render() {
    return (
      <div className="chat">
        {this.props.unread > 0 && !this.atBottom() && (
          <div className="messages-unread">
            {this.props.unread} unread message(s)
          </div>
        )}
        <div ref={this.topRef} />
        <div className="messages" ref={this.messagesRef}>
          {!values(this.props.messages).length && (
            <div className="messages-empty">No messages yet</div>
          )}
          {map(this.props.messages, m => (
            <Message
              ref={el => this.setMessageRef(m.messageid, el)}
              key={m.messageid}
              message={m}
              sessions={this.props.sessions}
              sessionid={this.props.sessionid}
              scroll={this.props.scroll}
              messagesInView={this.state.inView}
              markRead={this.props.actions.markRead}
              setCursor={this.setCursor}
              chatCursor={this.props.cache.chatCursor}
              atBottom={this.atBottom}
              unread={this.props.unread}
            />
          ))}
        </div>
        <div ref={this.bottomRef} style={{ height: 5 }} />
        <div className="message-input">
          <Form.Control
            type="text"
            placeholder="Message"
            onKeyPress={this.onKeyPress}
          />
        </div>
      </div>
    );
  }
}
