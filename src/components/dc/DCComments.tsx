import InlineEditable from 'components/common/InlineEditable';

interface Props {
  schema: { properties: { comments: object } };
  actions: {
    updateDataCollection: (v: {
      datacollectionid: number;
      comments: string;
    }) => Promise<void>;
  };
  datacollectionid: number;
  comments: string;
}

function DCComments(props: Props) {
  const { properties } = props.schema;
  const schema = {
    comments: properties.comments,
  };

  function onChange(e: { field: string; value: any }) {
    return props.actions.updateDataCollection({
      datacollectionid: props.datacollectionid,
      comments: e.value,
    });
  }

  return (
    <InlineEditable
      className="collapse-margins"
      colWidth={4}
      editable={['comments']}
      schema={schema}
      formData={{
        comments: props.comments,
      }}
      onChange={onChange}
    />
  );
}

export default DCComments;
