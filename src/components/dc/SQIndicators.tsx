import { Component } from 'react';

import { map } from 'lodash';

import PlotEnhancer from 'components/PlotEnhancer';
import RemountOnResize from 'components/utils/RemountOnResize';

interface PlotlyClickEventData {
  points: {
    pointNumber: number;
  }[];
}

interface Props {
  datacollectionid: number;
  actions?: {
    fetch: () => void;
    setParams: (p: { datacollectionid: number }, b: boolean) => Promise<void>;
    selectAutoProcProgram: (appid: number) => void;
    onClick?: (pt: number) => void;
  };
  sqis: {
    point: number[];
    total: number[];
    spots: number[];
    autoprocprogramid?: number[];
  };
  xTitle?: string;
}

export default class SQIndicators extends Component<Props> {
  public refreshTimeout: NodeJS.Timeout | null;

  public constructor(props: Props) {
    super(props);
    this.refreshTimeout = null;
  }

  public componentDidMount() {
    this.refresh();
  }

  public componentWillUnmount() {
    if (this.refreshTimeout) {
      clearTimeout(this.refreshTimeout);
    }
  }

  public onClick = (e: PlotlyClickEventData) => {
    if (e.points.length <= 0) {
      return;
    }

    const point = e.points[0].pointNumber;
    if (this.props.sqis.autoprocprogramid) {
      if (point < this.props.sqis.autoprocprogramid.length) {
        const appid = this.props.sqis.autoprocprogramid[point];
        if (appid !== null && this.props.actions?.selectAutoProcProgram) {
          this.props.actions.selectAutoProcProgram(appid);
        }
      }
    }

    if (this.props.actions?.onClick) {
      this.props.actions.onClick(point);
    }
  };

  public refresh() {
    if (this.refreshTimeout) {
      clearTimeout(this.refreshTimeout);
    }
    if (this.props.actions?.fetch) {
      this.props.actions
        .setParams({ datacollectionid: this.props.datacollectionid }, true)
        .then(() => {
          this.refreshTimeout = setTimeout(() => {
            this.refresh();
          }, 5000);
        });
    }
  }

  public render() {
    const { point, autoprocprogramid, ...ys } = this.props.sqis;
    let i = 0;
    const data: Plotly.Data[] = map(ys, (series, y) => ({
      x: this.props.sqis.point,
      y: series,
      yaxis: `y${i++}`,
      visible: series[0] !== null,
      type: 'scatter',
      name: y,
    }));

    const layout = {
      xaxis: {
        title: this.props.xTitle || 'Point',
      },
    };

    return (
      <>
        <h5>Scan Indicators</h5>
        <div className="sqi-plot">
          <RemountOnResize>
            <PlotEnhancer data={data} layout={layout} onClick={this.onClick} />
          </RemountOnResize>
        </div>
      </>
    );
  }
}
