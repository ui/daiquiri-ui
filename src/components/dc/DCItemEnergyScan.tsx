import { Row, Col, Container } from 'react-bootstrap';
import { Formatting } from '@esrf/daiquiri-lib';

import DCKeyValueList from 'components/dc/DCKeyValueList';
import Snapshots from 'components/dc/Snapshots';

import DCComments from 'connect/dc/DCComments';
import SQIndicators from 'connect/dc/SQIndicators';
import DCAttachmentPlot from 'connect/dc/DCAttachmentPlot';
import type { DataCollection } from '../../types/DataCollection';
import DCExtraTabs from './DCExtraTabs';

export default function DCItemEnergyScan(props: {
  dataCollection: DataCollection;
  showDataCollection?: (dc: DataCollection) => void;
}) {
  const { dataCollection } = props;
  const fields = [
    { title: 'Points', value: dataCollection.numberofimages },
    { title: 'Repeats', value: dataCollection.numberofpasses },
    {
      title: 'Energy Ref',
      value: Formatting.toEnergy(dataCollection.wavelength ?? 0),
      unit: 'keV',
      test: dataCollection.wavelength !== null,
    },
    {
      title: 'Beam Size',
      value: `${dataCollection.beamsizeatsamplex} x ${dataCollection.beamsizeatsampley}`,
      unit: '\u00B5m',
      test: !!(
        dataCollection.beamsizeatsamplex || dataCollection.beamsizeatsampley
      ),
    },
  ];

  return (
    <Container>
      <Row>
        <Col>
          <DCKeyValueList fields={fields} dataCollection={dataCollection} />
          <DCComments
            datacollectionid={dataCollection.datacollectionid}
            comments={dataCollection.comments}
          />
        </Col>
        {dataCollection.snapshots && (
          <Col>
            <Snapshots
              datacollectionid={dataCollection.datacollectionid}
              snapshots={dataCollection.snapshots}
            />
          </Col>
        )}
      </Row>
      <Row className="mt-2">
        <Col>
          <DCAttachmentPlot
            providers={{ metadata: { dcattachments: { namespace: 'plot' } } }}
            datacollectionid={dataCollection.datacollectionid}
          />
        </Col>
        <Col>
          <SQIndicators
            xTitle="Repeat"
            endtime={dataCollection.endtime}
            datacollectionid={dataCollection.datacollectionid}
          />
        </Col>
      </Row>
      <Row className="mt-2">
        <Col xs={12}>
          <DCExtraTabs
            dataCollection={props.dataCollection}
            showDataCollection={props.showDataCollection}
            createScalarMap
          />
        </Col>
      </Row>
    </Container>
  );
}
