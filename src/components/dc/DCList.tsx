import { useEffect, useState } from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';
import { Formatting } from '@esrf/daiquiri-lib';

import Table from 'components/table';
import SelectableTable from 'components/table/Selectable';
import ButtonTriggerModalStore from 'connect/layout/ButtonTriggerModalStore';

import StatusBadge from 'components/dc/StatusBadge';
import MapFromScalar from 'connect/dc/MapFromScalar';
import RegenerateMapsButton from 'connect/2dview/RegenerateMapsButton';
import type {
  PagedType,
  PagedActions,
  SelectableActions,
  TableColumn,
} from 'components/table/models';
import type { DataCollection } from '../../types/DataCollection';
import DCDialogButton from './DCDialogButton';

function DCStatusCell(cell: string) {
  return <StatusBadge status={cell} />;
}

function DCItemButton(
  cell: string,
  row: DataCollection,
  rowIndex: number,
  formatExtraData: {
    namespace: string;
    createScalarMap: boolean;
    showViewButton?: boolean;
  }
) {
  return (
    <>
      {formatExtraData.createScalarMap && (
        <ButtonTriggerModalStore
          id={`createScalarMap${row.datacollectionid}${formatExtraData.namespace}`}
          button={<i className="fa fa-map" />}
          buttonProps={{
            size: 'sm',
            className: 'me-1',
          }}
          title="Create Map from Scalar"
        >
          <MapFromScalar
            datacollectionid={row.datacollectionid}
            scanid={row.datacollectionnumber}
            providers={{
              scans: { list: { namespace: 'mfs' }, data: { namespace: 'mfs' } },
            }}
          />
        </ButtonTriggerModalStore>
      )}
      {formatExtraData.createScalarMap && (
        <RegenerateMapsButton
          subsampleid={row.subsampleid ?? 0}
          datacollectionid={row.datacollectionid}
          buttonProps={{
            size: 'sm',
            className: 'me-1',
          }}
        />
      )}
      {formatExtraData.showViewButton && (
        <DCDialogButton dataCollection={row} />
      )}
    </>
  );
}

function DurationCell(cell: string) {
  return cell ? Formatting.toHoursMins(Number.parseFloat(cell)) : '';
}

function SampleCell(cell: string, row: DataCollection) {
  if (!row) {
    return '';
  }

  if (row.subsampleid) {
    return (
      <span>
        {row.sampleid}-{row.subsampleid}
        {row.sample && (
          <>
            <br />[{row.sample}]
          </>
        )}
      </span>
    );
  }

  return <span>{row.sampleid}</span>;
}

interface Actions extends Partial<PagedActions>, Partial<SelectableActions> {}

interface Props extends PagedType {
  className?: string;
  actions?: Actions;
  datacollections: DataCollection[];
  createScalarMap?: boolean;
  dccount?: boolean;
  filter?: boolean;
  sample?: boolean;
  scan?: boolean;
  params?: {
    status: string;
    ungroup: number;
  };
  columns?: TableColumn[];
  namespace: string;
  selected?: number[];
  /** Whether to allow selection of a datacollection */
  selectable?: boolean;
  showFollow?: boolean;
  showViewButton?: boolean;
}

export default function DCList(props: Props) {
  const {
    createScalarMap = false,
    dccount = true,
    scan = true,
    params = { status: '', ungroup: 0 },
    actions = {},
    columns: inputColumns = [],
    namespace = '',
    showFollow = true,
    showViewButton = true,
  } = props;

  useEffect(() => {
    if (actions.fetch) {
      actions.fetch();
    }
  }, [actions]);

  const [showStatus, setShowStatus] = useState('');

  const [showChild, setShowChild] = useState(false);

  useEffect(() => {
    if (actions.setParams) {
      actions.setParams(
        {
          status: showStatus !== '' ? showStatus : undefined,
          ungroup: showChild ? 1 : undefined,
        },
        true
      );
    }
  }, [showStatus, showChild, actions.setParams]);

  const columns = [
    { dataField: 'datacollectionid', text: 'Id' },
    { dataField: 'starttime', text: 'Start', sort: true },
    {
      dataField: 'duration',
      text: 'Took',
      formatter: DurationCell,
      sort: true,
    },
    {
      dataField: 'runstatus',
      text: 'Status',
      formatter: DCStatusCell,
      sort: true,
    },
    {
      dataField: 'experimenttype',
      text: 'Type',
      sort: true,
      classes: 'text-break',
    },
    ...inputColumns,
    {
      dataField: 'item',
      text: '',
      formatter: DCItemButton,
      classes: 'text-end text-nowrap',
      formatExtraData: {
        createScalarMap,
        namespace,
        showViewButton,
      },
    },
  ];

  if (dccount) {
    columns.splice(4, 0, { dataField: 'datacollections', text: '#DC' });
  }

  if (scan) {
    columns.splice(4, 0, { dataField: 'datacollectionnumber', text: 'Scan' });
  }

  if (props.sample) {
    columns.splice(1, 0, {
      dataField: 'sampledetails',
      text: 'Sample',
      formatter: SampleCell,
    });
  }

  const pages =
    props.pages &&
    actions.fetch &&
    actions.setPage &&
    actions.setPageSize &&
    actions.setParams
      ? {
          page: props.page,
          per_page: props.per_page,
          total: props.total,
          setPage: actions.setPage,
          setPageSize: actions.setPageSize,
          setParams: actions.setParams,
          fetch: actions.fetch,
        }
      : undefined;

  const { addSelection, removeSelection, resetSelection } = actions;
  const selectableActions = addSelection &&
    removeSelection &&
    resetSelection && {
      addSelection,
      removeSelection,
      resetSelection,
    };

  const tableProps = {
    keyField: 'datacollectionid',
    data: props.datacollections,
    columns,
    noDataIndication: 'No data collections',
    pages,
    loading: props.fetching,
    overlay: true,
    page: props.page,
  };

  return (
    <div className={`dc-list ${props.className ?? ''}`}>
      <div className="filter">
        {props.selectable && showFollow && (
          <Button
            className="mb-1 me-2"
            size="sm"
            onClick={() => resetSelection?.()}
            disabled={props.selected === null}
          >
            Follow
          </Button>
        )}

        {props.filter && (
          <>
            <ButtonGroup size="sm" className="ms-1 mb-1">
              <Button
                variant={
                  params.status === 'Successful' ? 'primary' : 'secondary'
                }
                onClick={() =>
                  setShowStatus((s) => (s === 'Successful' ? '' : 'Successful'))
                }
                title="Only display successful collections"
              >
                Success
              </Button>
              <Button
                variant={params.status === 'Failed' ? 'primary' : 'secondary'}
                onClick={() =>
                  setShowStatus((s) => (s === 'Failed' ? '' : 'Failed'))
                }
                title="Only display failed collections"
              >
                Failed
              </Button>
            </ButtonGroup>
            <Button
              variant={params.ungroup ? 'primary' : 'secondary'}
              className="ms-1 mb-1"
              size="sm"
              onClick={() => setShowChild((s) => !s)}
              title="Display child data collections"
            >
              Children
            </Button>
          </>
        )}
      </div>
      {props.selectable && selectableActions && (
        <SelectableTable
          selectedItems={props.selected}
          actions={selectableActions}
          {...tableProps}
        />
      )}
      {!props.selectable && <Table {...tableProps} />}
    </div>
  );
}
