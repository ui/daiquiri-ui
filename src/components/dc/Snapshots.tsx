import { useCallback } from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { map } from 'lodash';

import config from 'config/config';

import XHRImage from 'helpers/XHRImage';
import XHRLightbox from 'helpers/XHRLightbox';
import Holder from 'helpers/Holder';

interface Props {
  snapshots: Record<string, boolean>;
  datacollectionid: number;
}

export default function Snapshots(props: Props) {
  const [firstSrc, setFirstSrc] = useState<string>();
  const [progress, setProgress] = useState(0);
  const [photoIndex, setPhotoIndex] = useState(0);
  const [isOpen, setOpen] = useState(false);

  const xhr = new XHRImage();

  const onLoad = useCallback(() => {
    setProgress(100);
    setFirstSrc(xhr.src);
  }, [setProgress, setFirstSrc]);

  const onProgress = useCallback(
    (progress: number) => {
      setProgress(progress);
    },
    [setProgress]
  );

  xhr.onload = onLoad;
  xhr.onprogress = onProgress;

  const images = map(props.snapshots, (sn, i) => {
    return sn
      ? `${config.baseUrl}/metadata/datacollections/snapshots/${props.datacollectionid}/${i}`
      : undefined;
  }).filter((v) => v !== undefined) as string[];

  useEffect(() => {
    if (images.length > 0) {
      xhr.load(`${images[0]}?thumb=1`);
    }
  }, []);

  return (
    <div className="snapshots">
      <div className="first">
        {firstSrc ? (
          <button
            type="button"
            className="wrap"
            style={{ width: '100%' }}
            onClick={() => setOpen(true)}
          >
            <img src={firstSrc} alt="snapshot1" style={{ width: '100%' }} />
          </button>
        ) : (
          <figure>
            <Holder width="100p" height="250" text="No snapshot" />
            {progress > 0 && <figcaption>Loading {progress}%</figcaption>}
          </figure>
        )}
      </div>

      {isOpen && (
        <XHRLightbox
          reactModalStyle={{
            overlay: {
              zIndex: 1200,
            },
          }}
          mainSrc={images[photoIndex]}
          nextSrc={images[(photoIndex + 1) % images.length]}
          prevSrc={images[(photoIndex + images.length - 1) % images.length]}
          onCloseRequest={() => setOpen(false)}
          onMovePrevRequest={() =>
            setPhotoIndex((photoIndex + images.length - 1) % images.length)
          }
          onMoveNextRequest={() =>
            setPhotoIndex((photoIndex + 1) % images.length)
          }
        />
      )}
    </div>
  );
}
