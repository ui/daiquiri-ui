import { Fragment } from 'react';
import { Row, Col } from 'react-bootstrap';
import { Formatting } from '@esrf/daiquiri-lib';
import StatusBadge from '../dc/StatusBadge';
import H5ViewerButton from '../h5viewer/H5ViewerButton';
import type { DataCollection } from 'types/DataCollection';

interface Field {
  title: string;
  value: any;
  classes?: string;
  unit?: string;
  test?: boolean;
}

const DEFAUT_VALS: Partial<DataCollection> = {
  datacollectionid: -1,
  experimenttype: '',
  starttime: null,
  endtime: null,
  filetemplate: '',
  datacollections: -1,
  duration: 0,
  datacollectionnumber: -1,
  imagedirectory: '',
};

function DCKeyValueList(props: {
  fields?: Field[];
  dataCollection: DataCollection;
}) {
  const { fields: propFields = [], dataCollection = DEFAUT_VALS } = props;

  const fields = [
    {
      title: 'Data Collection',
      value: (
        <>
          {dataCollection.datacollectionid}{' '}
          <StatusBadge status={dataCollection.runstatus} />
        </>
      ),
    },
    {
      title: 'Group',
      value: `${dataCollection.datacollections} data collections`,
      test: (dataCollection.datacollections ?? 0) > 1,
    },
    { title: 'Type', value: dataCollection.experimenttype },
    { title: 'Start', value: dataCollection.starttime },
    { title: 'End', value: dataCollection.endtime },
    {
      title: 'Took',
      value: Formatting.toHoursMins(dataCollection.duration ?? 0),
      test: dataCollection.duration !== undefined,
    },
    { title: 'Scan Number', value: dataCollection.datacollectionnumber },
    {
      title: 'Files',
      value: (
        <>
          <div>{`${dataCollection.imagedirectory}/${dataCollection.filetemplate}`}</div>
          {(dataCollection.filetemplate ?? '').endsWith('.h5') && (
            <H5ViewerButton
              datacollectionid={dataCollection.datacollectionid}
              fileName={dataCollection.filetemplate ?? ''}
            />
          )}
        </>
      ),
      classes: 'text-break',
      test:
        dataCollection.filetemplate !== undefined &&
        dataCollection.filetemplate !== null,
    },
    ...propFields,
  ];

  return (
    <div className="parameters mb-3">
      {fields.map((f) => (
        <Fragment key={f.title}>
          {(f.test === undefined || f.test) && (
            <Row>
              <Col>{f.title}</Col>
              <Col className={f.classes}>
                {f.value}
                {f.unit && <span className="ms-1 unit">{f.unit}</span>}
              </Col>
            </Row>
          )}
        </Fragment>
      ))}
    </div>
  );
}

export default DCKeyValueList;
