import { useEffect, useMemo, useState } from 'react';
import type { DataCollection } from '../../types/DataCollection';
import DCItem from './DCItem';

export default function DCForm(props: {
  dataCollectionId: number;
  dataCollectionGroupId: number;
  datacollections: DataCollection[];
  actions?: {
    fetch?: () => Promise<void>;
    setParams?: (params: any, flush?: boolean) => Promise<void>;
    setPageSize?: (pageSize: number) => Promise<void>;
  };
}) {
  const [selectedDcId, setSelectedDcId] = useState(props.dataCollectionId);

  useEffect(() => {
    // Number of dcs in a group may be > default 25
    props.actions?.setPageSize?.(999);
    props.actions?.setParams?.(
      { datacollectiongroupid: props.dataCollectionGroupId, ungroup: 1 },
      true
    );
  }, [props.dataCollectionGroupId]);

  const dataCollection = useMemo(() => {
    const l = props.datacollections.filter(
      (d) => d.datacollectionid === selectedDcId
    );
    if (l.length === 0) {
      return null;
    }
    return l[0];
  }, [selectedDcId, props.datacollections]);

  function showDataCollection(dc: DataCollection) {
    setSelectedDcId(dc.datacollectionid);
  }

  const patchedDataCollection = useMemo(() => {
    // When the request is "ungroup", the "datacollections" field is wrong, but we can patch it
    if (!dataCollection) {
      return null;
    }
    return {
      ...dataCollection,
      datacollections: props.datacollections.length,
    };
  }, [dataCollection]);

  return (
    <>
      {patchedDataCollection && (
        <DCItem
          dataCollection={patchedDataCollection}
          showDataCollection={showDataCollection}
        />
      )}
    </>
  );
}
