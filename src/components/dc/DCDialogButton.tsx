import ButtonTriggerModalStore from 'connect/layout/ButtonTriggerModalStore';
import DCForm from 'connect/dc/DCForm';
import type { DataCollection } from '../../types/DataCollection';

export default function DCDialogButton(props: {
  dataCollection: DataCollection;
}) {
  const { dataCollection } = props;
  return (
    <ButtonTriggerModalStore
      id={`dataCollectionInfo${dataCollection.datacollectionid}${'maindialog'}`}
      button={<i className="fa fa-search" />}
      buttonProps={{
        variant: 'info',
        size: 'sm',
      }}
      title="Data Collection Details"
    >
      <DCForm
        providers={{
          metadata: {
            datacollections: {
              namespace: 'maindialog',
            },
          },
        }}
        dataCollectionId={dataCollection.datacollectionid}
        dataCollectionGroupId={dataCollection.datacollectiongroupid}
      />
    </ButtonTriggerModalStore>
  );
}
