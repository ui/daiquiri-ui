import { useEffect } from 'react';
import StackedNameState from 'components/common/StackedNameState';
import type { AutoProcDescription } from 'types/Metadata';

function getStyleFromAutoProc(
  autoproc: AutoProcDescription | undefined
): [string, string] {
  if (!autoproc) {
    return ['NONE', 'secondary'];
  }
  if (autoproc.status === null) {
    return ['PROCESSING', 'warning'];
  }
  if (autoproc.status === 0) {
    return ['FAILED', 'danger'];
  }
  if (autoproc.status === 1) {
    return ['DONE', 'success'];
  }
  return ['UNKNOWN', 'fatal'];
}

/**
 * Autoprocessing widget to be integrated inside toolbars
 */
export default function AutoProcessingState(props: {
  className?: string;
  datacollectionid: number | undefined;
  programs: string | undefined;
  name: string;
  description: string;
  actions: {
    fetch: (params: { datacollectionid: number; programs: string }) => void;
  };
  autoprocprogram: AutoProcDescription | undefined;
}) {
  useEffect(() => {
    if (props.datacollectionid !== undefined && props.programs !== undefined) {
      const refreshTimeout = setInterval(() => {
        if (
          props.datacollectionid !== undefined &&
          props.programs !== undefined
        ) {
          props.actions.fetch({
            datacollectionid: props.datacollectionid,
            programs: props.programs,
          });
        }
      }, 500);
      return () => {
        clearInterval(refreshTimeout);
      };
    }
    return undefined;
  }, [props.datacollectionid, props.programs]);

  const [label, variant] = getStyleFromAutoProc(props.autoprocprogram);
  return (
    <StackedNameState
      className={props.className}
      name={`${props.name}`}
      state={label}
      stateVariant={variant}
      description={`${props.description}`}
      minWidth={5}
    />
  );
}
