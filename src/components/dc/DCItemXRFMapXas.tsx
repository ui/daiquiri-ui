import { Row, Col, Container } from 'react-bootstrap';
import { Formatting } from '@esrf/daiquiri-lib';

import DCKeyValueList from 'components/dc/DCKeyValueList';
import Snapshots from 'components/dc/Snapshots';
import SQIndicators from 'connect/dc/SQIndicators';

import DCComments from 'connect/dc/DCComments';
import type { DataCollection } from '../../types/DataCollection';
import DCExtraTabs from './DCExtraTabs';

export default function DCItemXRFMapXas(props: {
  dataCollection: DataCollection;
  showDataCollection?: (dc: DataCollection) => void;
}) {
  const { dataCollection } = props;
  const fields = [
    { title: 'Dwell', value: dataCollection.exposuretime, unit: 's' },
    {
      title: 'Beam Size',
      value: `${dataCollection.beamsizeatsamplex * 1e3} x ${
        dataCollection.beamsizeatsampley * 1e3
      }`,
      unit: '\u00B5m',
      test: !!(
        dataCollection.beamsizeatsamplex || dataCollection.beamsizeatsampley
      ),
    },
    {
      title: 'Steps',
      value: `${dataCollection.steps_x} x ${dataCollection.steps_y}`,
    },
    {
      title: 'Step Size',
      value: `${dataCollection.dx_mm * 1e3} x ${dataCollection.dy_mm * 1e3}`,
      unit: '\u00B5m',
    },
    {
      title: 'Map Size',
      value: `${dataCollection.dx_mm * 1e3 * (dataCollection.steps_x ?? 0)}x${
        dataCollection.dy_mm * 1e3 * (dataCollection.steps_y ?? 0)
      }`,
      unit: '\u00B5m',
    },
    { title: 'Points', value: dataCollection.numberofimages },
  ];

  return (
    <Container>
      <Row>
        <Col>
          <DCKeyValueList fields={fields} dataCollection={dataCollection} />
          <DCComments
            datacollectionid={dataCollection.datacollectionid}
            comments={dataCollection.comments}
          />
        </Col>
        {dataCollection.snapshots !== undefined && (
          <Col>
            <Snapshots
              datacollectionid={dataCollection.datacollectionid}
              snapshots={dataCollection.snapshots}
            />
          </Col>
        )}
      </Row>
      <Row className="mt-2">
        <Col xs={12}>
          <SQIndicators
            xTitle="Sum Xanes"
            endtime={dataCollection.endtime}
            datacollectionid={dataCollection.datacollectionid}
          />
        </Col>
      </Row>
      <Row className="mt-2">
        <Col xs={12}>
          <DCExtraTabs
            dataCollection={props.dataCollection}
            showDataCollection={props.showDataCollection}
            dcColumns={[
              {
                dataField: 'wavelength',
                text: 'Energy',
                formatter: Formatting.toEnergy,
              },
              {
                dataField: 'exposuretime',
                text: 'Dwell',
              },
            ]}
            createScalarMap
          />
        </Col>
      </Row>
    </Container>
  );
}
