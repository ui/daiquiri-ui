import { Suspense } from 'react';
import type { DataCollection } from '../../types/DataCollection';
import { getDCView } from './utils';

export default function DCItem(props: {
  dataCollection: DataCollection;
  showDataCollection?: (dc: DataCollection) => void;
}) {
  const { dataCollection } = props;
  const DCView = getDCView(dataCollection.experimenttype);

  return (
    <Suspense fallback={<p>Loading...</p>}>
      <DCView
        dataCollection={dataCollection}
        showDataCollection={props.showDataCollection}
      />
    </Suspense>
  );
}
