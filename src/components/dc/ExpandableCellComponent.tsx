import { useState } from 'react';
import { Collapse } from 'react-bootstrap';

interface Props {
  message: string;
  description: string;
}

function ExpandableCellComponent(props: Props) {
  const [show, setShow] = useState(false);

  return (
    <div>
      <div
        className="pointer"
        onClick={() => setShow(!show)}
        onKeyPress={() => setShow(!show)}
        aria-controls="collapseable-description"
        aria-expanded={show}
        role="button"
        tabIndex={0}
      >
        <i className={`me-1 fa fa-${show ? 'caret-down' : 'caret-right'}`} />
        {props.message}
      </div>
      <Collapse in={show}>
        <pre className="text-break text-wrap">{props.description}</pre>
      </Collapse>
    </div>
  );
}

export default ExpandableCellComponent;
