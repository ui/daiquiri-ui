import { Component } from 'react';

import { each, map, filter } from 'lodash';

import RestService from 'services/RestService';

import PlotEnhancer from 'components/PlotEnhancer';
import RemountOnResize from 'components/utils/RemountOnResize';
import type { PlotData } from '../plotting/models';

interface Attachment {
  datacollectionfileattachmentid: number;
  filename: string;
}

interface Props {
  attachments: Record<string, Attachment>;
  datacollectionid: number;
  actions?: {
    setParams: (params: any, b: boolean) => Promise<void>;
  };
}

type Contents = Record<string, { headers: string[]; x: any[]; series: any[] }>;

export default class DCAttachmentPlot extends Component<
  Props,
  { contents: Contents }
> {
  public constructor(props: Props) {
    super(props);
    this.state = {
      contents: {},
    };
  }

  public componentDidMount() {
    if (this.props.actions?.setParams) {
      this.props.actions
        .setParams(
          {
            datacollectionid: this.props.datacollectionid,
            filetype: 'xy',
          },
          true
        )
        .then(() => this.fetchAttachments());
    }
  }

  // TODO: Not sure how to handle this better, should not be fetching resources
  //  locally within the component...
  public fetchAttachments() {
    const newContents: Contents = {};
    each(this.props.attachments, (att) => {
      RestService.get(
        `/metadata/datacollections/attachments/${att.datacollectionfileattachmentid}`
      ).then((resp) => {
        const data = filter(
          map(resp.data.split(/\n/), (line) => line.split(/[\s|]+/)),
          (line) => !!line[0]
        );

        let headers: string[] = [];
        if (data[0] && data[0][0] === '#') {
          headers = data.shift();
          headers.shift();
        }

        const transpose = map(data[0], (col, i) => map(data, (row) => row[i]));
        newContents[att.filename] = {
          headers,
          x: transpose.shift() || [],
          series: transpose,
        };
      });
    });
    this.setState((prev) => ({
      contents: {
        ...newContents,
        ...prev.contents,
      },
    }));
  }

  public render() {
    let xTitle = null;

    const data: PlotData[] = [];
    each(this.state.contents, (fdata) => {
      [xTitle] = fdata.headers;
      each(fdata.series, (fseries, i) => {
        data.push({
          x: fdata.x,
          y: fseries,
          name: fdata.headers[i + 1],
        });
      });
    });

    const layout = {
      xaxis: {
        title: xTitle || 'Point',
      },
    };

    return (
      <>
        <h5>Scan Average</h5>
        <div className="attachment-plot">
          <RemountOnResize>
            <PlotEnhancer data={data} layout={layout} />
          </RemountOnResize>
        </div>
      </>
    );
  }
}
