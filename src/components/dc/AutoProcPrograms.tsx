import { Component } from 'react';
import { values } from 'lodash';

import Tooltip from 'components/common/Tooltip';
import Table from 'components/table';

import ButtonTriggerModalStore from 'connect/layout/ButtonTriggerModalStore';
import APPAttachments from 'connect/dc/APPAttachments';
import APPMessages from 'connect/dc/APPMessages';
import type { PagedActions, PagedType } from 'components/table/models';
import type { AutoProcDescription } from 'types/Metadata';

function ActionCell(cell: number | null) {
  return (
    <>
      <ButtonTriggerModalStore
        id={`autoProcProgramMessages${cell}`}
        button={<i className="fa fa-comments-o" />}
        buttonProps={{
          variant: 'info',
          size: 'sm',
        }}
        title="Messages"
      >
        <APPMessages autoprocprogramid={cell} />
      </ButtonTriggerModalStore>
      <ButtonTriggerModalStore
        id={`autoProcProgramAttachments${cell}`}
        button={<i className="fa fa-files-o" />}
        buttonProps={{
          variant: 'info',
          size: 'sm',
          className: 'ms-1',
        }}
        title="Files"
      >
        <APPAttachments autoprocprogramid={cell} />
      </ButtonTriggerModalStore>
    </>
  );
}

function StatusCell(cell: number | null, row: AutoProcDescription) {
  const statuses = [
    <i className="fa fa-times text-danger" title="Failed" />,
    <i className="fa fa-check text-success" title="Success" />,
  ];

  return (
    <>
      {row.warnings > 0 && (
        <Tooltip tooltip="Processed with warnings">
          <div className="btn btn-sm border border-warning">
            <i className="fa fa-exclamation-triangle text-warning" />
          </div>
        </Tooltip>
      )}
      {row.errors > 0 && (
        <Tooltip tooltip="Processed with errors">
          <div className="btn btn-sm border border-danger ms-1">
            <i className="fa fa-exclamation-circle text-danger" />
          </div>
        </Tooltip>
      )}
      {row.automatic === 0 && (
        <Tooltip tooltip="Reprocessing job">
          <div className="btn btn-sm border ms-1">
            <i className="fa fa-refresh" />
          </div>
        </Tooltip>
      )}
      <div className="btn btn-sm border ms-1">
        {cell !== null ? (
          statuses[cell]
        ) : (
          <i className="fa fa-cog fa-spin text-muted" title="Running" />
        )}
      </div>
    </>
  );
}

interface Props extends PagedType {
  autoprocprograms: Record<number, AutoProcDescription>;
  datacollectionid: number;
  actions?: Required<PagedActions>;
  className?: string;
}

export default class AutoProcPrograms extends Component<Props> {
  public refreshTimeout: NodeJS.Timeout | null;

  public constructor(props: Props) {
    super(props);
    this.refreshTimeout = null;
  }

  public clearTimeout() {
    if (this.refreshTimeout) {
      clearTimeout(this.refreshTimeout);
    }
  }

  public componentDidMount() {
    this.refresh();
  }

  public componentWillUnmount() {
    this.clearTimeout();
  }

  public refresh() {
    this.clearTimeout();
    if (this.props.actions?.setParams) {
      this.props.actions
        .setParams({ datacollectionid: this.props.datacollectionid }, true)
        .then(() => {
          this.refreshTimeout = setTimeout(() => {
            this.refresh();
          }, 5000);
        });
    }
  }

  public render() {
    const columns = [
      { dataField: 'programs', text: 'Program' },
      { dataField: 'starttime', text: 'Started' },
      { dataField: 'duration', text: 'Took' },
      { dataField: 'message', text: 'Message' },
      {
        dataField: 'status',
        text: '',
        formatter: StatusCell,
        classes: 'text-center text-nowrap',
      },
      {
        dataField: 'autoprocprogramid',
        text: '',
        formatter: ActionCell,
        classes: 'text-end text-nowrap',
      },
    ];

    const pages = {
      page: this.props.page,
      per_page: this.props.per_page,
      total: this.props.total,
      setPage: this.props.actions?.setPage,
      setPageSize: this.props.actions?.setPageSize,
      fetch: this.props.actions?.fetch,
    };
    return (
      <div className={this.props.className}>
        <Table
          keyField="autoprocprogramid"
          data={values(this.props.autoprocprograms) || []}
          columns={columns}
          noDataIndication="No auto processing results found"
          pages={pages}
        />
      </div>
    );
  }
}
