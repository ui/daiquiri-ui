import { Tabs, Tab } from 'react-bootstrap';
import DCAttachments from 'connect/dc/DCAttachments';
import AutoProcPrograms from 'connect/dc/AutoProcPrograms';
import type { DataCollection } from 'types/DataCollection';
import DCList from '../../connect/dc/DCList';
import { useState } from 'react';
import type { TableColumn } from '../table/models';

export default function DCExtraTabs(props: {
  dataCollection: DataCollection;
  showDataCollection?: (dc: DataCollection) => void;
  dcColumns?: TableColumn[];
  createScalarMap?: boolean;
}) {
  const { dataCollection } = props;
  const [selectedTab, setSelectedTab] = useState('attachments');
  const [selectedDC, setSelectedDC] = useState(dataCollection.datacollectionid);

  return (
    <Tabs
      activeKey={selectedTab}
      onSelect={(k) => {
        if (k) {
          setSelectedTab(k);
        }
      }}
    >
      <Tab eventKey="attachments" title="Attachments">
        <DCAttachments
          className="mt-2"
          datacollectionid={dataCollection.datacollectionid}
        />
      </Tab>
      {dataCollection.datacollections > 1 && (
        <Tab eventKey="collections" title="Data Collections">
          <DCList
            className="mt-2"
            providers={{
              metadata: {
                datacollections: {
                  namespace: 'dcg',
                  params: {
                    datacollectiongroupid: dataCollection.datacollectiongroupid,
                  },
                },
              },
            }}
            pages
            scan={false}
            dccount={false}
            columns={props.dcColumns}
            showViewButton={false}
            showFollow={false}
            selectable
            selected={[selectedDC]}
            actions={{
              addSelection: (payload: any) => {
                props.showDataCollection?.(payload);
                setSelectedDC(payload.datacollectionid);
              },
              removeSelection: () => null,
              resetSelection: () => null,
            }}
          />
        </Tab>
      )}
      <Tab eventKey="programs" title="Processing">
        <AutoProcPrograms
          className="mt-2"
          datacollectionid={dataCollection.datacollectionid}
        />
      </Tab>
    </Tabs>
  );
}
