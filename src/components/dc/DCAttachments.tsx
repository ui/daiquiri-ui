import { useEffect } from 'react';
import { values } from 'lodash';

import LogView from 'components/common/LogView';
import DownloadButton from 'components/common/DownloadButton';
import Table from 'components/table';
import type { PagedActions, PagedType } from 'components/table/models';

interface RowType {
  datacollectionfileattachmentid: string;
  filename: string;
  filetype: string;
}

function ViewCell(cell: number, row: RowType) {
  const Comp = row.filetype === 'log' ? LogView : DownloadButton;
  return (
    <Comp
      filename={row.filename}
      url={`/metadata/datacollections/attachments/${row.datacollectionfileattachmentid}`}
    />
  );
}

interface Props extends PagedType {
  attachments: Record<string, RowType>;
  datacollectionid: number;
  actions: Required<PagedActions>;
  className?: string;
}

export default function DCAttachments(props: Props) {
  const { actions, datacollectionid } = props;

  useEffect(() => {
    if (actions?.setParams) {
      actions.setParams({ datacollectionid: props.datacollectionid }, true);
    }
  }, [actions, datacollectionid]);

  const columns = [
    { dataField: 'filename', text: 'File' },
    { dataField: 'filetype', text: 'Type' },
    {
      dataField: 'item',
      text: '',
      formatter: ViewCell,
      classes: 'text-end text-nowrap',
    },
  ];

  const pages = props.pages
    ? {
        page: props.page,
        per_page: props.per_page,
        total: props.total,
        setPage: props.actions.setPage,
        setPageSize: props.actions.setPageSize,
        fetch: props.actions.fetch,
      }
    : undefined;

  return (
    <Table
      classes={props.className}
      keyField="datacollectionfileattachmentid"
      data={values(props.attachments) || []}
      columns={columns}
      noDataIndication="No datacollection attachments found"
      pages={pages}
      overlay
      loading={props.fetching}
    />
  );
}
