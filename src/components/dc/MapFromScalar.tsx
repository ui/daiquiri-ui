import type { RefObject, RefAttributes } from 'react';
import { createRef, Component } from 'react';

import { map, filter } from 'lodash';

import { Container, Form, Row, Col, Button, Alert } from 'react-bootstrap';
import { Typeahead } from 'react-bootstrap-typeahead';

interface Props {
  datacollectionid: number;
  scanid: number;
  actions: {
    fetchScan: (scanid: number) => void;
    fetchScanData: (props: { scanid: number }) => void;
    addToast?: (props: { type: string; title: string; text: string }) => void;
    createMap: (props: {
      datacollectionid: number;
      scalars: string[];
    }) => Promise<void>;
    updateMapSettings: (props: Record<string, any>) => Promise<void>;
  };
  error?: string;
  scanError?: string;
  fetching: boolean;
  fetched: boolean;
  maps?: Record<any, any>;
  mapSettings: Record<string, any>;
  data?: Record<
    number,
    {
      axes: {
        ys: {
          scalars: string[];
        };
      };
    }
  >;
  scan: Record<string, any>;
  creatingAdditional?: boolean;
}

interface State {
  selected: string[];
  pendingAutoList: boolean;
}

export default class MapFromScalar extends Component<Props, State> {
  // @ts-expect-error
  private readonly typeaheadRef: RefObject<Typeahead>;

  public constructor(props: Props) {
    super(props);
    this.state = { selected: [], pendingAutoList: false };
    this.typeaheadRef = createRef();
  }

  public componentDidMount() {
    const { scanid } = this.props;
    if (scanid) {
      this.props.actions?.fetchScan(scanid);
    }
  }

  public componentDidUpdate(lastProps: Props) {
    if (this.props.scan !== lastProps.scan) {
      if (this.props.scan.group) {
        this.props.actions.fetchScanData({
          scanid: this.props.scan.children[0].scanid,
        });
      } else {
        this.props.actions.fetchScanData({ scanid: this.props.scanid });
      }
    }
  }

  private createMap() {
    this.props.actions
      .createMap({
        datacollectionid: this.props.datacollectionid,
        scalars: this.state.selected,
      })
      .catch((error) => {
        this.props.actions.addToast?.({
          type: 'error',
          title: 'Could not create scalar map',
          text: error.message,
        });
      })
      .then(() => {
        this.typeaheadRef?.current?.clear();
      });
  }

  private addToAutoScalarMaps() {
    const newScalarMaps = [...this.props.mapSettings.scalar_maps];
    this.state.selected.forEach((selectedScalarName) => {
      if (!newScalarMaps.includes(selectedScalarName)) {
        newScalarMaps.push(selectedScalarName);
      }
    });

    this.setState({ pendingAutoList: true });
    this.props.actions
      .updateMapSettings({ scalar_maps: newScalarMaps })
      .then(() => {
        setTimeout(() => {
          this.setState({ pendingAutoList: false });
        }, 500);
      });
  }

  private setSelection(selected: string[]) {
    this.setState({
      selected,
    });
  }

  public render() {
    const created = map(
      filter(
        this.props.maps,
        (m) => m.datacollectionid === this.props.datacollectionid && m.scalar
      ),
      (m) => m.scalar
    );

    const scanid = this.props.scan?.group
      ? this.props.scan.children[0].scanid
      : this.props.scanid;
    const data = this.props.data?.[scanid];
    const opts = data?.axes
      ? data.axes.ys.scalars.filter((s) => !created.includes(s))
      : [];

    return (
      <Container className="mt-3">
        {this.props.scan?.group && (
          <Alert variant="success">
            Scan is a group, loading child scalars
          </Alert>
        )}

        {!this.props.scanid && (
          <Alert variant="warning">No scan id available</Alert>
        )}

        {(this.props.error || this.props.scanError) && (
          <Alert variant="warning">
            Scan number {this.props.scanid} is no longer available
          </Alert>
        )}

        {this.props.fetching && (
          <Alert variant="info">
            <i className="fa fa-spin fa-spinner me-1" />
            Fetching available scalars
          </Alert>
        )}

        {!(this.props.error || this.props.scanError) &&
          this.props.fetched &&
          !this.props.fetching &&
          this.props.scanid && (
            <Form>
              <Form.Group
                as={Row}
                controlId="scalar-selection"
                className="mb-2"
              >
                <Form.Label column sm={3}>
                  Scalar
                </Form.Label>
                <Col>
                  <Typeahead
                    autoFocus
                    ref={this.typeaheadRef}
                    id="scalar-selection"
                    // @ts-expect-error
                    inputProps={{ 'data-testid': 'scalar-selection' }}
                    multiple
                    onChange={(selection) =>
                      this.setSelection(selection as string[])
                    }
                    options={[...opts]}
                  />
                </Col>
              </Form.Group>
              <Button
                onClick={() => this.createMap()}
                disabled={
                  this.props.creatingAdditional ||
                  this.state.selected.length === 0
                }
              >
                {!this.props.creatingAdditional && <>Create Map</>}

                {this.props.creatingAdditional && (
                  <>
                    Creating Map <i className="fa fa-spin fa-spinner" />
                  </>
                )}
              </Button>
              <Button
                onClick={() => this.addToAutoScalarMaps()}
                className="ms-1"
                disabled={
                  this.state.pendingAutoList || this.state.selected.length === 0
                }
              >
                {!this.state.pendingAutoList && <>Add to Auto List</>}

                {this.state.pendingAutoList && (
                  <>
                    Adding to Auto List <i className="fa fa-spin fa-spinner" />
                  </>
                )}
              </Button>
            </Form>
          )}
      </Container>
    );
  }
}
