import { Row, Col, Container } from 'react-bootstrap';
import { Formatting } from '@esrf/daiquiri-lib';

import DCKeyValueList from 'components/dc/DCKeyValueList';
import Snapshots from 'components/dc/Snapshots';

import DCComments from 'connect/dc/DCComments';
import DCAttachments from 'connect/dc/DCAttachments';
import AutoProcPrograms from 'connect/dc/AutoProcPrograms';
import type { DataCollection } from '../../types/DataCollection';
import DCExtraTabs from './DCExtraTabs';

export default function DCItemXRFMap(props: {
  dataCollection: DataCollection;
  showDataCollection?: (dc: DataCollection) => void;
}) {
  const { dataCollection } = props;
  const fields = [
    {
      title: 'Energy',
      value: Formatting.toEnergy(dataCollection.wavelength ?? 0),
      unit: 'keV',
      test: dataCollection.wavelength !== null,
    },
    { title: 'Dwell', value: dataCollection.exposuretime, unit: 's' },
    {
      title: 'Beam Size',
      value: `${dataCollection.beamsizeatsamplex * 1e3} x ${
        dataCollection.beamsizeatsampley * 1e3
      }`,
      unit: '\u00B5m',
      test: !!(
        dataCollection.beamsizeatsamplex || dataCollection.beamsizeatsampley
      ),
    },
    {
      title: 'Patches',
      value: `${dataCollection.patchesx} x ${dataCollection.patchesy}`,
      test: !!(
        (dataCollection.patchesx && dataCollection.patchesx > 1) ||
        (dataCollection.patchesy && dataCollection.patchesy > 1)
      ),
    },
    {
      title: 'Steps',
      value: `${dataCollection.steps_x} x ${dataCollection.steps_y}`,
    },
    {
      title: 'Step Size',
      value: `${dataCollection.dx_mm * 1e3} x ${dataCollection.dy_mm * 1e3}`,
      unit: '\u00B5m',
    },
    {
      title: 'Map Size',
      value: `${
        (dataCollection.dx_mm ?? 0) *
        1e3 *
        (dataCollection.steps_x ?? 0) *
        (dataCollection.patchesx || 1)
      }x${
        (dataCollection.dy_mm ?? 0) *
        1e3 *
        (dataCollection.steps_y ?? 0) *
        (dataCollection.patchesy || 1)
      }`,
      unit: '\u00B5m',
    },
    { title: 'Points', value: dataCollection.numberofimages },
  ];

  return (
    <Container>
      <Row>
        <Col>
          <DCKeyValueList fields={fields} dataCollection={dataCollection} />
          <DCComments
            datacollectionid={dataCollection.datacollectionid}
            comments={dataCollection.comments}
          />
        </Col>
        {dataCollection.snapshots && (
          <Col>
            <Snapshots
              datacollectionid={dataCollection.datacollectionid}
              snapshots={dataCollection.snapshots}
            />
          </Col>
        )}
      </Row>
      <Row className="mt-2">
        <Col xs={12}>
          <DCExtraTabs
            dataCollection={props.dataCollection}
            showDataCollection={props.showDataCollection}
            createScalarMap
          />
        </Col>
      </Row>
    </Container>
  );
}
