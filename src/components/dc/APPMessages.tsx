import { useEffect } from 'react';
import { values } from 'lodash';

import Table from 'components/table';
import type { PagedActions, PagedType } from 'components/table/models';
import ExpandableCellComponent from './ExpandableCellComponent';

interface RowType {
  description: string;
  severity: string;
  timestamp: number;
}

export function ExpandableCell(cell: string, row: RowType) {
  return (
    <ExpandableCellComponent message={cell} description={row.description} />
  );
}

export function SeverityCell(cell: string) {
  const severities: Record<string, JSX.Element> = {
    INFO: (
      <div className="btn btn-sm border border-success">
        <i className="fa fa-info text-success" />
      </div>
    ),
    WARNING: (
      <div className="btn btn-sm border border-warning">
        <i className="fa fa-exclamation-triangle text-warning" />
      </div>
    ),
    ERROR: (
      <div className="btn btn-sm border border-danger">
        <i className="fa fa-exclamation-circle text-danger" />
      </div>
    ),
  };

  return severities[cell];
}

interface Props extends PagedType {
  attachments: Record<number, RowType>;
  actions: Required<PagedActions>;
  autoprocprogramid: number;
}

export default function APPMessages(props: Props) {
  const { actions, autoprocprogramid } = props;

  useEffect(() => {
    if (actions?.setParams) {
      actions.setParams({ autoprocprogramid: props.autoprocprogramid }, true);
    }
  }, [actions, autoprocprogramid]);

  const columns = [
    { dataField: 'severity', text: '', formatter: SeverityCell },
    { dataField: 'timestamp', text: 'Time' },
    { dataField: 'message', text: 'Message', formatter: ExpandableCell },
  ];

  const pages = {
    page: props.page,
    per_page: props.per_page,
    total: props.total,
    setPage: props.actions.setPage,
    setPageSize: props.actions.setPageSize,
    fetch: props.actions.fetch,
  };

  return (
    <Table
      keyField="autoprocprogrammessageid"
      data={values(props.attachments) || []}
      columns={columns}
      noDataIndication="No auto processing messages found"
      pages={pages}
      overlay
      loading={props.fetching}
    />
  );
}
