import type { ComponentType } from 'react';
import DCItemXRFMap from './DCItemXRFMap';
import DCItemXRFMapXas from './DCItemXRFMapXas';
import DCItemEnergyScan from './DCItemEnergyScan';
import DCItemDefault from './DCItemDefault';
import type { DataCollection } from '../../types/DataCollection';

export function getDCView(type: string): ComponentType<{
  dataCollection: DataCollection;
  showDataCollection?: (dc: DataCollection) => void;
}> {
  if (['XRF map', 'XRD map', 'XRF xrd map'].includes(type)) {
    return DCItemXRFMap;
  }

  if (type === 'XRF map xas') {
    return DCItemXRFMapXas;
  }

  if (type === 'Energy scan') {
    return DCItemEnergyScan;
  }

  return DCItemDefault;
}
