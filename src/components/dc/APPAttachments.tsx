import { useEffect } from 'react';
import { values } from 'lodash';
import { Button, OverlayTrigger, Popover } from 'react-bootstrap';

import Table from 'components/table';
import type { PagedActions, PagedType } from 'components/table/models';
import LogView from '../common/LogView';
import DownloadButton from '../common/DownloadButton';
import H5ViewerButton from '../h5viewer/H5ViewerButton';

interface RowType {
  filetype: string;
  filepath: string;
  filename: string;
  autoprocprogramattachmentid: number;
  size: number;
}

function FileNameButton({ filename }: { filename: string }) {
  const popover = (
    <Popover id="popover-filename">
      <Popover.Header as="h3">File Name</Popover.Header>
      <Popover.Body>{filename}</Popover.Body>
    </Popover>
  );
  return (
    <OverlayTrigger
      trigger="click"
      placement="left"
      overlay={popover}
      rootClose
    >
      <Button size="sm" className="me-1">
        <i className="fa fa-file" />
      </Button>
    </OverlayTrigger>
  );
}

function ViewCell(cell: string, row: RowType) {
  const Comp = row.filetype === 'Log' ? LogView : DownloadButton;
  return (
    <>
      <FileNameButton filename={`${row.filepath}/${row.filename}`} />
      {(row.filename ?? '').endsWith('.h5') && (
        <H5ViewerButton
          buttonProps={{ className: 'me-1' }}
          autoprocprogramattachmentid={row.autoprocprogramattachmentid}
          fileName={row.filename ?? ''}
        />
      )}

      <Comp
        filename={row.filename}
        url={`/metadata/autoprocs/attachments/${row.autoprocprogramattachmentid}`}
      />
    </>
  );
}

interface Props extends PagedType {
  attachments: Record<number, RowType>;
  autoprocprogramid: number;
  actions: Required<PagedActions>;
}

function APPAttachments(props: Props) {
  const { actions, page, per_page, total, attachments, autoprocprogramid } =
    props;

  useEffect(() => {
    if (actions?.setParams) {
      actions.setParams({ autoprocprogramid }, true);
    }
  }, [actions, autoprocprogramid]);

  const columns = [
    { dataField: 'filename', text: 'File' },
    { dataField: 'filetype', text: 'Type' },
    { dataField: 'size', text: 'Size' },
    {
      dataField: 'item',
      text: '',
      formatter: ViewCell,
      classes: 'text-end text-nowrap',
    },
  ];

  const pages = {
    page,
    per_page,
    total,
    setPage: actions?.setPage,
    setPageSize: actions?.setPageSize,
    fetch: actions?.fetch,
  };

  return (
    <Table
      keyField="autoprocprogramattachmentid"
      data={values(props.attachments) || []}
      columns={columns}
      noDataIndication="No auto processing attachments found"
      pages={pages}
      overlay
      loading={props.fetching}
    />
  );
}

export default APPAttachments;
