import { Row, Col, Container } from 'react-bootstrap';
import DCKeyValueList from 'components/dc/DCKeyValueList';
import Snapshots from 'components/dc/Snapshots';
import DCComments from 'connect/dc/DCComments';
import type { DataCollection } from 'types/DataCollection';
import DCExtraTabs from './DCExtraTabs';

export default function DCItemDefault(props: {
  dataCollection: DataCollection;
  showDataCollection?: (dc: DataCollection) => void;
}) {
  const { dataCollection } = props;
  return (
    <Container>
      <Row>
        <Col>
          {dataCollection.filetemplate !== null && (
            <DCKeyValueList dataCollection={dataCollection} />
          )}
          <DCComments
            datacollectionid={dataCollection.datacollectionid}
            comments={dataCollection.comments}
          />
        </Col>
        {dataCollection.snapshots && (
          <Col>
            <Snapshots
              datacollectionid={dataCollection.datacollectionid}
              snapshots={dataCollection.snapshots}
            />
          </Col>
        )}
      </Row>
      <Row className="mt-2">
        <Col xs={12}>
          <DCExtraTabs
            dataCollection={props.dataCollection}
            showDataCollection={props.showDataCollection}
          />
        </Col>
      </Row>
    </Container>
  );
}
