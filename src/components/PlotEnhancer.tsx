/* eslint-disable import/dynamic-import-chunkname */
import { lazy, Suspense } from 'react';
import createPlotlyComponent from 'react-plotly.js/factory';
import { filter } from 'lodash';
import type { PlotParams } from 'react-plotly.js';

const PlotGl = lazy(() =>
  // @ts-expect-error
  import('plotly.js-gl2d-dist').then((Plotly) => ({
    default: createPlotlyComponent(Plotly),
  }))
);

const PlotBasic = lazy(() =>
  // @ts-expect-error
  import('plotly.js-basic-dist').then((Plotly) => ({
    default: createPlotlyComponent(Plotly),
  }))
);

export default function PlotEnhancer(props: PlotParams) {
  const { layout = {}, config = {}, ...rest } = props;
  const layoutn: Partial<Plotly.Layout> = {
    margin: {
      l: 50,
      r: 20,
      b: 50,
      t: 20,
      pad: 10,
    },
    hovermode: 'closest',
    autosize: true,
    ...layout,
  };

  const confign = {
    displayModeBar: false,
    ...config,
  };

  const bar = filter(props.data, (series) => series.type === 'bar');
  const Plot = bar.length > 0 ? PlotBasic : PlotGl;

  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Plot
        style={{ width: '100%', height: '100%' }}
        useResizeHandler
        config={confign}
        layout={layoutn}
        {...rest}
      />
    </Suspense>
  );
}
