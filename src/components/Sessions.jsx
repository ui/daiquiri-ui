import { Component } from 'react';
import PropTypes from 'prop-types';
import { Badge, Button, Alert } from 'react-bootstrap';

import ModalDialog from 'components/layout/ModalDialog';
import Table from 'components/table';
import momentCell from 'components/table/cells/momentCell';

function StatusCell(cell, row, rowIndex, formatExtraData) {
  const text = [];
  if (row.sessionid === formatExtraData.sessionid) {
    text.push(
      <Badge key="current" bg="primary" className="me-1">
        You
      </Badge>
    );
  }
  if (row.operator) {
    text.push(
      <Badge key="operator" bg="secondary" className="me-1">
        Operator
      </Badge>
    );
  }
  if (row.operator_pending) {
    text.push(
      <Badge key="pending" bg="warning" className="me-1">
        Pending
      </Badge>
    );
  }

  if (row.mirrored) {
    text.push(
      <Badge key="mirrored" bg="secondary" className="me-1">
        <i className="fa fa-eye" />
      </Badge>
    );
  }

  return text;
}

function MirrorCell(cell, row, rowIndex, formatExtraData) {
  const onClick = () => {
    formatExtraData
      .requestMirror({
        sessionid: row.sessionid
      })
      .then(() => {
        formatExtraData.toggleParent();
      });
  };

  return row.sessionid !== formatExtraData.sessionid ? (
    <Button size="sm" onClick={onClick} title="Mirror session">
      <i className="fa fa-television" />
    </Button>
  ) : null;
}

export default class Sessions extends Component {
  static defaultProps = {
    mirrorid: undefined,
    controlRequest: undefined,
    toggleParent: undefined
  };

  static propTypes = {
    actions: PropTypes.shape({
      fetchSessions: PropTypes.func.isRequired,
      requestControl: PropTypes.func.isRequired,
      respondControlRequest: PropTypes.func.isRequired,
      yieldControl: PropTypes.func.isRequired,
      requestMirror: PropTypes.func.isRequired
    }).isRequired,
    sessions: PropTypes.arrayOf(PropTypes.object).isRequired,
    user: PropTypes.shape({
      is_staff: PropTypes.bool
    }).isRequired,
    operator: PropTypes.bool.isRequired,
    sessionid: PropTypes.string.isRequired,
    mirrorid: PropTypes.string,
    controlRequest: PropTypes.string,
    toggleParent: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.props.actions.fetchSessions();
  }

  respondControlRequest = grant => {
    this.props.actions.respondControlRequest(grant);
  };

  requestControl = () => {
    this.props.actions.requestControl().then(() => {
      this.toggleParent();
    });
  };

  yieldControl = () => {
    this.props.actions.yieldControl().then(() => {
      this.toggleParent();
    });
  };

  onCloseRequest = () => {};

  toggleParent = () => {
    if (this.props.toggleParent) {
      this.props.toggleParent();
    }
  };

  render() {
    const columns = [
      { dataField: 'data.username', text: 'User' },
      { dataField: 'data.client', text: 'Client' },
      {
        dataField: 'last_access',
        text: 'Last Access',
        formatter: momentCell,
        formatExtraData: { format: 'DD-MM-YYYY HH:mm:ss' },
        classes: 'text-nowrap'
      },
      {
        dataField: 'status',
        text: '',
        formatter: StatusCell,
        formatExtraData: { sessionid: this.props.sessionid }
      }
    ];

    if (this.props.user.is_staff) {
      columns.push({
        dataField: '',
        text: '',
        classes: 'text-end',
        formatter: MirrorCell,
        formatExtraData: {
          sessionid: this.props.sessionid,
          mirrorid: this.props.mirrorid,
          requestMirror: this.props.actions.requestMirror,
          toggleParent: this.toggleParent
        }
      });
    }

    return (
      <>
        <ModalDialog
          show={this.props.controlRequest !== null && this.props.operator}
          actions={{ onClose: this.onCloseRequest }}
          title="Control Requested"
          showClose={false}
          buttons={
            <>
              <Button onClick={() => this.respondControlRequest(true)}>
                Grant
              </Button>
              <Button onClick={() => this.respondControlRequest(false)}>
                Deny
              </Button>
            </>
          }
        >
          Another session &quot;
          {this.props.controlRequest}
          &quot; is requesting control. Without a response the other session
          will automatically be granted control
        </ModalDialog>

        {!this.props.operator && (
          <div className="d-grid gap-2 mb-2">
            <Button size="sm" onClick={this.requestControl}>
              Request Control
            </Button>
          </div>
        )}

        <Alert
          variant={this.props.operator ? 'success' : 'warning'}
          className="d-flex"
        >
          <div className="flex-grow-1 m-auto">
            This session
            {this.props.operator ? ' has ' : ' does not have '}
            control
          </div>
          {this.props.operator === true && (
            <Button
              size="sm"
              className="d-block flex-grow-0"
              variant="warning"
              onClick={this.yieldControl}
            >
              Yield
            </Button>
          )}
        </Alert>

        <Table
          keyField="sessionid"
          data={this.props.sessions || []}
          columns={columns}
          bordered={false}
          classes="table-sm"
          striped
        />
      </>
    );
  }
}
