import type { ReactElement } from 'react';
import type { OverlayProps } from 'react-bootstrap';

export interface ModalClassProps {
  target: OverlayProps['target'];
  show?: boolean;
  actions?: {
    onShow?: () => void;
    onClose?: () => void;
  };
  title: string;
  buttons?: ReactElement;
  otherButtons?: ReactElement;
  bodyClassName?: string;
  size?: 'sm' | 'lg' | 'xl';
  fullscreen?: true | string;
}

export interface ButtonTriggerModalStoreRef {
  showModal: () => void;
  close: () => void;
}
