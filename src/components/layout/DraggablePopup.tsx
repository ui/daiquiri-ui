import Draggable from 'react-draggable';
import { Popover, Overlay, Button } from 'react-bootstrap';
import type { ModalClassProps } from './models';
import type { PropsWithChildren } from 'react';

interface Props extends ModalClassProps {
  popoverid?: string;
}

export default function DraggablePopup(props: PropsWithChildren<Props>) {
  function onClose() {
    if (props.actions?.onClose) {
      props.actions.onClose();
    }
  }

  return (
    <Overlay show={props.show} target={props.target}>
      {(injectedProps) => {
        const { style, show, ...rest } = injectedProps;

        // ridiculous
        const reg =
          style.transform &&
          /translate3d\((\d+)px, (\d+)px/.exec(style.transform);
        const start = reg
          ? { x: Number.parseInt(reg[1]), y: Number.parseInt(reg[2]) }
          : {};

        // Overlay injects className into its child, have to wrap in another div to
        // stop this throwin an error in Draggable
        return (
          <div>
            <Draggable handle=".popover-header">
              <Popover
                id={`popover-${props.popoverid}`}
                className="draggable-popup"
                {...rest}
                style={{
                  top: start.y,
                  left: start.x,
                }}
              >
                <Popover.Header as="h3">
                  {props.title}
                  <Button onClick={onClose} className="close">
                    <span aria-hidden="true">×</span>
                    <span className="sr-only">Close</span>
                  </Button>
                </Popover.Header>
                <Popover.Body>{props.children}</Popover.Body>
              </Popover>
            </Draggable>
          </div>
        );
      }}
    </Overlay>
  );
}
