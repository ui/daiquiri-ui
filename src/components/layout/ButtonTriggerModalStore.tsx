import {
  useRef,
  Children,
  cloneElement,
  isValidElement,
  useImperativeHandle,
  forwardRef,
  createRef,
} from 'react';
import type { ReactChild, ReactElement, Ref, ComponentType } from 'react';
import type { Placement } from 'react-bootstrap/types';
import type { OverlayProps, ButtonProps } from 'react-bootstrap';
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import ModalDialog from 'components/layout/ModalDialog';
import ConditionalWrap from 'helpers/ConditionalWrap';
import type { ButtonTriggerModalStoreRef, ModalClassProps } from './models';

interface Props {
  children: ReactChild | ReactChild[];
  button?: ReactElement;
  buttons?: ReactElement;
  otherButtons?: ReactElement;
  title: string;
  buttonProps?: Omit<ButtonProps, 'onClick'>;
  showButton?: boolean;
  tooltip?: string;
  tooltipPlacement?: Placement;
  onShow?: () => void;
  onClose?: () => void;
  actions: {
    toggleModal: (id: string) => void;
  };
  show: boolean;
  id: string;
  modalClass?: ComponentType<ModalClassProps>;
  target?: OverlayProps['target'];
  modalBodyClassName?: string;
  size?: 'sm' | 'lg' | 'xl';
  fullscreen?: true | string;
}

function ButtonTriggerModalStore(
  props: Props,
  ref: Ref<ButtonTriggerModalStoreRef>
) {
  const { showButton = true, buttonProps = {}, target } = props;
  const buttonRef = useRef(null);

  function showModal() {
    props.actions.toggleModal(props.id);
  }

  function onCloseModal() {
    props.actions.toggleModal(props.id);
    if (props.onClose) props.onClose();
  }

  useImperativeHandle(ref, () => ({
    showModal: () => showModal(),
    close: () => onCloseModal(),
  }));

  const ModalClass = props.modalClass || ModalDialog;

  return (
    <>
      {showButton && (
        <ConditionalWrap
          condition={props.tooltip}
          wrap={(children) => (
            <OverlayTrigger
              placement={props.tooltipPlacement || 'top'}
              overlay={<Tooltip id="tooltip1">{props.tooltip}</Tooltip>}
            >
              {children}
            </OverlayTrigger>
          )}
        >
          <Button ref={buttonRef} onClick={() => showModal()} {...buttonProps}>
            {props.button}
          </Button>
        </ConditionalWrap>
      )}
      <ModalClass
        target={target || createRef<HTMLElement>()}
        show={props.show}
        actions={{ onShow: props.onShow, onClose: onCloseModal }}
        title={props.title}
        buttons={props.buttons}
        otherButtons={props.otherButtons}
        bodyClassName={props.modalBodyClassName}
        size={props.size}
        fullscreen={props.fullscreen}
      >
        {Children.map(props.children, (child) =>
          isValidElement(child)
            ? cloneElement(child, {
                ...child.props,
                requestCloseModal: onCloseModal,
              })
            : undefined
        )}
      </ModalClass>
    </>
  );
}

export default forwardRef(ButtonTriggerModalStore);
