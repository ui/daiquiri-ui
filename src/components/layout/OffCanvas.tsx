import type { ReactChild, RefObject, MouseEvent } from 'react';
import { createRef, Component, useState } from 'react';
import { Button, Badge } from 'react-bootstrap';

function OffCanvasCover(props: {
  id: string;
  toggleOffCanvas: (id: string) => void;
}) {
  return (
    <div
      role="dialog"
      onClick={() => {
        props.toggleOffCanvas(props.id);
      }}
      className="offcanvas-panel-cover"
    />
  );
}

function OffCanvasContent(props: {
  children: (params: {
    scroll: { ref: RefObject<HTMLDivElement> };
  }) => ReactChild | ReactChild[];
}) {
  const containerRef = createRef<HTMLDivElement>();
  const [scroll, setScroll] = useState({
    scroll: {
      ref: containerRef,
    },
  });

  return (
    <div className="offcanvas-panel-content" ref={containerRef}>
      {props.children(scroll)}
    </div>
  );
}

export function OffCanvasHeader(props: {
  actions: {
    toggleOffCanvas: (id: string) => void;
  };
  children: ReactChild | ReactChild[];
  id: string;
}) {
  function onClick(e: MouseEvent) {
    e.preventDefault();
    props.actions.toggleOffCanvas(props.id);
  }

  return (
    <div className="offcanvas-panel-header">
      <Button onClick={onClick} size="sm" variant="light" className="float-end">
        <i className="fa fa-times" />
      </Button>
      {props.children}
    </div>
  );
}

export function OffCanvasTrigger(props: {
  actions: {
    toggleOffCanvas: (id: string) => void;
  };
  id: string;
  unread: number;
}) {
  function onClick(e: MouseEvent) {
    e.preventDefault();
    props.actions.toggleOffCanvas(props.id);
  }

  return (
    <div className="offcanvas-panel-trigger">
      <Button onClick={onClick} size="sm" variant="light" className="float-end">
        {props.unread > 0 && (
          <Badge bg="success" className="me-1 unread-messages">
            {props.unread}
          </Badge>
        )}
        <i className="fa fa-bars" />
      </Button>
    </div>
  );
}

function OffCanvas(props: {
  id: string;
  children: (params: { toggle: () => void }) => ReactChild | ReactChild[];
  enabled?: boolean;
  actions: {
    toggleOffCanvas: (id: string) => void;
  };
}) {
  function toggleOffCanvas() {
    props.actions.toggleOffCanvas(props.id);
  }
  return (
    <div
      className={`offcanvas-panel shadow-sm ${props.enabled && ' enable'}`}
      id={props.id}
    >
      <OffCanvasCover id={props.id} toggleOffCanvas={toggleOffCanvas} />
      <div className="offcanvas-panel-container">
        {props.children({ toggle: toggleOffCanvas })}
      </div>
    </div>
  );
}

OffCanvas.Cover = OffCanvasCover;
OffCanvas.Content = OffCanvasContent;
// OffCanvas.Header = OffCanvasHeader;
// OffCanvas.Trigger = OffCanvasTrigger;

export default OffCanvas;
