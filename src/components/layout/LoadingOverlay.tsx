import type { PropsWithChildren, ReactChild } from 'react';

interface Props {
  enabled?: boolean;
  active?: boolean;
  text?: string;
}

export default function LoadingOverlay(props: PropsWithChildren<Props>) {
  if (!props.enabled) {
    return <>{props.children}</>;
  }

  return (
    <div className="loading-wrapper">
      {props.active && (
        <div className="loading-overlay">
          <div className="loading-inner">
            <i className="fa fa-circle-o-notch fa-spin fa-3x" />
            <div>{props.text || 'Loading...'}</div>
          </div>
        </div>
      )}
      {props.children}
    </div>
  );
}
