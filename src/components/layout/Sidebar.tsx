/* eslint-disable @typescript-eslint/ban-types */
import type { MouseEvent, PropsWithChildren } from 'react';

function Sidebar(
  props: PropsWithChildren<{
    collapsed?: boolean;
    enabledId: string | null;
    actions: {
      toggleSidebar: (id: string) => void;
    };
  }>
) {
  return (
    <div
      className={`sidebar shadow-sm ${
        props.collapsed ? 'sidebar-collapsed' : ''
      }`}
    >
      <SidebarCover
        id={props.enabledId}
        toggleSidebar={props.actions.toggleSidebar}
      />
      {props.children}
    </div>
  );
}

function SidebarCover(props: {
  id: string | null;
  toggleSidebar: (id: string) => void;
}) {
  const enabledClass = props.id ? 'enabled' : '';

  function toggleSidebar() {
    if (props.id !== null) {
      props.toggleSidebar(props.id);
    }
  }

  return (
    <div
      role="dialog"
      onClick={toggleSidebar}
      className={`sidebar-cover ${enabledClass}`}
    />
  );
}

function SidebarHeader(props: PropsWithChildren<{}>) {
  return <div className="sidebar-header">{props.children}</div>;
}

function SidebarContent(props: PropsWithChildren<{}>) {
  return <div className="sidebar-content">{props.children}</div>;
}

function SidebarInner(props: PropsWithChildren<{}>) {
  return <div className="sidebar-inner">{props.children}</div>;
}

export function SidebarTrigger(
  props: PropsWithChildren<{
    actions: {
      toggleSidebar: (id: string) => void;
    };
    id: string;
    enabled?: boolean;
    className?: string;
  }>
) {
  function onClick(e: MouseEvent) {
    e.preventDefault();
    props.actions.toggleSidebar(props.id);
  }

  return (
    <div
      className={`sidebar-trigger ${props.enabled ? 'active' : ''} ${
        props.className
      }`}
      onClick={onClick}
    >
      {props.children}
    </div>
  );
}

export function SidebarContainer(
  props: PropsWithChildren<{ enabled: boolean }>
) {
  return (
    <div
      className={`sidebar-container shadow-sm ${props.enabled && ' enable'}`}
    >
      {props.children}
    </div>
  );
}

export default Object.assign(Sidebar, {
  // Container: SidebarContainer,
  Header: SidebarHeader,
  Content: SidebarContent,
  Inner: SidebarInner,
  // Trigger: SidebarTrigger,
  Cover: SidebarCover,
});
