import type { ReactChild } from 'react';
import { Modal, Button } from 'react-bootstrap';
import type { ModalClassProps } from './models';

interface Props extends ModalClassProps {
  children: ReactChild | ReactChild[];
  cancelText?: string;
  footer?: boolean;
  showClose?: boolean;
}

export default function ModalDialog(props: Props) {
  const {
    title,
    children,
    buttons,
    cancelText,
    footer = true,
    showClose = true,
    show = false,
    actions = {},
    otherButtons,
    bodyClassName,
    size = 'lg',
    fullscreen,
  } = props;

  const { onShow, onClose: onHide } = actions;

  return (
    <Modal
      fullscreen={fullscreen}
      size={size}
      aria-labelledby="modal-title"
      centered
      onShow={onShow}
      onHide={onHide}
      show={show}
    >
      <Modal.Header closeButton>
        <Modal.Title id="modal-title">{title}</Modal.Title>
      </Modal.Header>
      <Modal.Body className={bodyClassName}>{children}</Modal.Body>
      {footer && (
        <Modal.Footer>
          {otherButtons && (
            <div className="form-other-buttons">{otherButtons}</div>
          )}
          {buttons}
          {showClose && (
            <Button onClick={onHide}>{cancelText || 'Close'}</Button>
          )}
        </Modal.Footer>
      )}
    </Modal>
  );
}
