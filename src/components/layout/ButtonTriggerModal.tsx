import { useRef, useState } from 'react';
import type { ReactElement, ReactChild, ComponentType } from 'react';
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import ModalDialog from 'components/layout/ModalDialog';
import ConditionalWrap from 'helpers/ConditionalWrap';
import type { Placement } from 'react-bootstrap/types';
import type { ModalClassProps } from './models';

interface Props {
  children: ReactChild | ReactChild[];
  button?: ReactElement;
  buttons?: ReactElement;
  title: string;
  buttonProps?: object;
  showButton?: boolean;
  tooltip?: string;
  tooltipPlacement?: Placement;
  onClose?: () => void;
  modalClass?: ComponentType<ModalClassProps>;
}

export default function ButtonTriggerModal(props: Props) {
  const [showModal, setShowModal] = useState(false);
  const { showButton = true } = props;
  const targetRef = useRef(null);

  function close() {
    setShowModal(false);
    if (props.onClose) {
      props.onClose();
    }
  }

  function onCloseModal() {
    close();
  }

  const ModalClass = props.modalClass || ModalDialog;
  return (
    <>
      {showButton && (
        <ConditionalWrap
          condition={props.tooltip}
          wrap={(children) => (
            <OverlayTrigger
              placement={props.tooltipPlacement || 'top'}
              overlay={<Tooltip id="tooltip1">{props.tooltip}</Tooltip>}
            >
              {children}
            </OverlayTrigger>
          )}
        >
          <Button
            onClick={() => {
              setShowModal(true);
            }}
            {...props.buttonProps}
            ref={targetRef}
          >
            {props.button}
          </Button>
        </ConditionalWrap>
      )}
      <ModalClass
        target={targetRef}
        show={showModal}
        actions={{ onClose: onCloseModal }}
        title={props.title}
        buttons={props.buttons}
      >
        {props.children}
      </ModalClass>
    </>
  );
}
