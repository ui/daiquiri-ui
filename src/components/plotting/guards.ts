import type { PlotData, MeshData } from './models';

export function isMeshData(data: PlotData[] | MeshData[]): data is MeshData[] {
  const [oneDataItem] = data;

  return (
    !!oneDataItem && Object.getOwnPropertyNames(oneDataItem).includes('marker')
  );
}

export function isDefined<T>(value: T): value is NonNullable<T> {
  return value !== undefined && value !== null;
}

export function hasValues(arr: number[] | undefined): arr is number[] {
  return isDefined(arr) && arr.length > 0;
}
