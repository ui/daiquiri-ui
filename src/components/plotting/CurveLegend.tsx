import React from 'react';
import LegendEntry from './LegendEntry';
import type { CurveData } from './models';

interface Props {
  curves: CurveData[];
  onEntryClick: (i: number) => void;
  onEntryDoubleClick: (i: number) => void;
}

function CurveLegend(props: Props) {
  const { curves, onEntryClick, onEntryDoubleClick } = props;
  return (
    <div>
      {curves.map((c, i) => (
        <LegendEntry
          key={c.name}
          curve={c}
          onClick={() => onEntryClick(i)}
          onDoubleClick={() => onEntryDoubleClick(i)}
        />
      ))}
    </div>
  );
}

export default CurveLegend;
