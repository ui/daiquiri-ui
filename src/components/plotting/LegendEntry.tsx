import type { CurveData } from './models';

interface Props {
  curve: CurveData;
  onClick: () => void;
  onDoubleClick: () => void;
}

function LegendEntry(props: Props) {
  const { curve, onClick, onDoubleClick } = props;

  return (
    <button
      className="btn-clean entry"
      key={curve.name}
      type="button"
      data-not-visible={curve.visible ? undefined : ''}
      onClick={() => onClick()}
      onDoubleClick={() => onDoubleClick()}
    >
      <div
        className="handle"
        style={{
          backgroundColor: curve.color,
        }}
      />
      {curve.name}
    </button>
  );
}

export default LegendEntry;
