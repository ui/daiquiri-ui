import type { DataCurveProps } from '@h5web/lib';
import { Annotation } from '@h5web/lib';
import {
  DataCurve,
  useValueToIndexScale,
  useVisCanvasContext,
} from '@h5web/lib';
import type { ThreeEvent } from '@react-three/fiber';
import { useState } from 'react';

function InteractiveCurve(props: DataCurveProps) {
  const { onDataPointClick, ...curveProps } = props;
  const { abscissas, ordinates, color } = curveProps;

  const [hoveredIndex, setHoveredIndex] = useState<number>();

  const abscissaToIndex = useValueToIndexScale(abscissas, true);
  const { worldToData } = useVisCanvasContext();

  const handleClick = onDataPointClick
    ? (index: number, evt: ThreeEvent<MouseEvent>) => {
        const { unprojectedPoint } = evt;
        const dataPt = worldToData(unprojectedPoint);

        const newIndex = abscissaToIndex(dataPt.x);

        onDataPointClick(newIndex, evt);
      }
    : undefined;

  const handlePointerEnter = onDataPointClick
    ? (index: number, evt: ThreeEvent<MouseEvent>) => {
        const { unprojectedPoint } = evt;
        const dataPt = worldToData(unprojectedPoint);

        setHoveredIndex(abscissaToIndex(dataPt.x));
      }
    : undefined;

  const handlePointerLeave = onDataPointClick
    ? () => setHoveredIndex(undefined)
    : undefined;

  return (
    <>
      <DataCurve
        onDataPointClick={handleClick}
        onDataPointEnter={handlePointerEnter}
        onDataPointLeave={handlePointerLeave}
        {...curveProps}
      />
      {hoveredIndex && (
        <Annotation
          x={abscissas[hoveredIndex]}
          y={ordinates[hoveredIndex]}
          center
        >
          <svg
            className="line-hover-svg"
            style={{
              fill: color,
            }}
          >
            <circle r={4} />
          </svg>
        </Annotation>
      )}
    </>
  );
}

export default InteractiveCurve;
