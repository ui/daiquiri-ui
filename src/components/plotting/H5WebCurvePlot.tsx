import type { Domain, CurveType, Selection } from '@h5web/lib';
import { SelectToZoom } from '@h5web/lib';
import { GlyphType } from '@h5web/lib';
import {
  getAxisValues,
  Html,
  Pan,
  ResetZoomButton,
  useCombinedDomain,
  useDomains,
  VisCanvas,
  Zoom,
} from '@h5web/lib';
import { useState } from 'react';

import type { Layout } from '../scans/models';
import CurveLegend from './CurveLegend';
import MultiCurveTooltipMesh from './MultiCurveTooltipMesh';
import type { CurveData, PlotData } from './models';
import InteractiveCurve from './InteractiveCurve';
import { hasValues, isDefined } from './guards';
import PlotOverlay from './PlotOverlay';
import RoiSelectionTool from './RoiSelectionTool';

const COLORS = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728'];

const DEFAULT_DOMAIN: Domain = [0.1, 1];

const POINT_SIZE = 6;

interface Props {
  data: (PlotData | undefined)[];
  insetLegend?: boolean;
  onClick?: (index: number) => void;
  curveType?: CurveType;
  layout?: Layout;
  onRoiSelection?: (points: Selection) => void;
}

function H5WebCurvePlot(props: Props) {
  const { data, insetLegend, onClick, curveType, layout, onRoiSelection } =
    props;

  const [visibilities, setVisibilities] = useState(data.map(() => true));
  const curves: CurveData[] = data.filter(isDefined).flatMap((d, i) => {
    const { x, y, name } = d;
    if (!hasValues(y)) {
      return [];
    }
    const axisValues = getAxisValues(x, y.length);
    return [
      {
        color: COLORS[i % COLORS.length],
        visible: visibilities[i],
        name,
        x: Array.isArray(axisValues) ? axisValues : [...axisValues],
        y,
      },
    ];
  });

  const xDomain =
    useCombinedDomain(useDomains(curves.map((c) => c.x))) || DEFAULT_DOMAIN;
  const yDomain =
    useCombinedDomain(useDomains(curves.map((c) => c.y))) || DEFAULT_DOMAIN;

  if (curves.length === 0) {
    return <div>No curve data. Please select a series.</div>;
  }

  const toggleCurve = (curveIndex: number) => {
    setVisibilities(
      visibilities.map((val, index) => (index === curveIndex ? !val : val))
    );
  };

  const showOnlyThisCurve = (curveIndex: number) => {
    setVisibilities(visibilities.map((_, index) => index === curveIndex));
  };

  return (
    <div className="h5web-plot">
      <VisCanvas
        abscissaConfig={{
          visDomain: xDomain,
          showGrid: true,
          label: layout?.xaxis?.title,
        }}
        ordinateConfig={{
          visDomain: yDomain,
          showGrid: true,
          label: layout?.yaxis?.title,
        }}
        raycasterThreshold={POINT_SIZE}
      >
        {curves.map((c) => (
          <InteractiveCurve
            key={c.name}
            abscissas={c.x}
            ordinates={c.y}
            color={c.color}
            visible={c.visible}
            onDataPointClick={onClick}
            glyphSize={POINT_SIZE}
            glyphType={GlyphType.Circle}
            curveType={curveType}
          />
        ))}
        <Pan />
        <Zoom />
        <SelectToZoom modifierKey="Control" />
        <MultiCurveTooltipMesh curves={curves} />
        <ResetZoomButton />
        {insetLegend && (
          <Html>
            {curves.length > 1 && (
              <div className="h5web-legend" data-inset="">
                <CurveLegend
                  curves={curves}
                  onEntryClick={toggleCurve}
                  onEntryDoubleClick={showOnlyThisCurve}
                />
              </div>
            )}
          </Html>
        )}
        {layout && (
          <PlotOverlay
            shapes={layout.shapes}
            annotations={layout.annotations}
          />
        )}
        {onRoiSelection && (
          <RoiSelectionTool onValidSelection={onRoiSelection} />
        )}
      </VisCanvas>
      {!insetLegend && curves.length > 1 && (
        <div className="h5web-legend">
          <CurveLegend
            curves={curves}
            onEntryClick={toggleCurve}
            onEntryDoubleClick={showOnlyThisCurve}
          />
        </div>
      )}
    </div>
  );
}

export default H5WebCurvePlot;
