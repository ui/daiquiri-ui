import { useVisCanvasContext } from '@h5web/lib';
import { Vector3 } from 'three';

export function useScaleCoord() {
  const { abscissaScale, ordinateScale } = useVisCanvasContext();
  const [xMin, xMax] = abscissaScale.domain();
  const [yMin, yMax] = ordinateScale.domain();

  // https://plotly.com/javascript/reference/#layout-shapes-items-shape-xref
  return (x: number, y: number, xref: string, yref: string) => {
    const newX = xref === 'paper' ? xMin + x * (xMax - xMin) : x;
    const newY = yref === 'paper' ? yMin + y * (yMax - yMin) : y;

    return new Vector3(newX, newY);
  };
}
