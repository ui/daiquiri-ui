import { CurveType } from '@h5web/lib';
import { isMeshData } from './guards';
import H5WebCurvePlot from './H5WebCurvePlot';
import H5WebScatterContainer from './H5WebScatterContainer';
import type { MeshData, PlotData } from './models';

interface Props {
  data: PlotData[] | MeshData[];
  onClick?: (index: number) => void;
}

function H5Web0dPlot(props: Props) {
  const { data, onClick } = props;

  if (data.length === 0) {
    return <div>No data to display</div>;
  }

  if (isMeshData(data)) {
    return <H5WebScatterContainer data={data} onClick={onClick} />;
  }

  return (
    <H5WebCurvePlot
      data={data}
      insetLegend
      onClick={onClick}
      curveType={CurveType.LineAndGlyphs}
    />
  );
}

export default H5Web0dPlot;
