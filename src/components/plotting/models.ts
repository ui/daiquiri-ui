import type { ColorMap } from '@h5web/lib';

export interface PlotData {
  x?: number[];
  y?: number[];
  name: string;
}

export interface CurveData extends Required<PlotData> {
  color: string;
  visible: boolean;
}

interface MarkerData {
  size: number;
  color?: number[];
  colorscale: ColorMap;
}

export interface MeshData extends PlotData {
  marker: MarkerData;
}
