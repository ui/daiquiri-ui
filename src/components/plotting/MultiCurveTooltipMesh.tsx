import {
  TooltipMesh,
  useValueToIndexScale,
  useVisCanvasContext,
} from '@h5web/lib';
import type { CurveData } from './models';

interface Props {
  curves: CurveData[];
}

function MultiCurveTooltipMesh(props: Props) {
  const { curves } = props;

  // Consider that all curve abscissas are the same
  const abscissas = curves[0].x;
  const xToIndexScale = useValueToIndexScale(abscissas, true);

  const { abscissaConfig } = useVisCanvasContext();

  return (
    <TooltipMesh
      guides="both"
      renderTooltip={(x) => {
        const xi = xToIndexScale(x);

        return (
          <div className="h5web-tooltip">
            {abscissaConfig.label || 'x'}={abscissas[xi]}
            {curves
              .filter((c) => c.visible)
              .map((c) => (
                <div key={c.name}>
                  <strong>{c.name}:</strong> {c.y[xi]}
                </div>
              ))}
          </div>
        );
      }}
    />
  );
}

export default MultiCurveTooltipMesh;
