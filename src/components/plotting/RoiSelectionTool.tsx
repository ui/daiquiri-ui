import {
  AxialSelectionTool,
  SvgElement,
  SvgRect,
  useVisCanvasContext,
} from '@h5web/lib';
import type { Selection } from '@h5web/lib';
import { useThree } from '@react-three/fiber';
import { Vector3 } from 'three';

interface Props {
  onValidSelection: (selection: Selection) => void;
}

function RoiSelectionTool(props: Props) {
  const { onValidSelection } = props;
  const camera = useThree((state) => state.camera);
  const { ordinateConfig, dataToHtml, dataToWorld } = useVisCanvasContext();
  const [yMin, yMax] = ordinateConfig.visDomain;

  return (
    <AxialSelectionTool
      axis="x"
      onValidSelection={(selection: Selection) => {
        const { data } = selection;

        const startPoint = new Vector3(data[0].x, yMin);
        const endPoint = new Vector3(data[1].x, yMax);
        onValidSelection({
          data: [startPoint, endPoint],
          html: [dataToHtml(camera, startPoint), dataToHtml(camera, endPoint)],
          world: [dataToWorld(startPoint), dataToWorld(endPoint)],
        });
      }}
    >
      {({ html }) => {
        return (
          <SvgElement>
            <SvgRect coords={html} fill="gray" fillOpacity={0.25} />
          </SvgElement>
        );
      }}
    </AxialSelectionTool>
  );
}

export default RoiSelectionTool;
