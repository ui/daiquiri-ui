import { Overlay, useCameraState, useVisCanvasContext } from '@h5web/lib';
import { useThree } from '@react-three/fiber';
import type { Shape, Annotation as AnnotationType } from '../scans/models';
import { useScaleCoord } from './hooks';

interface Props {
  shapes?: Shape[];
  annotations?: AnnotationType[];
}

function PlotOverlay(props: Props) {
  const { shapes: rawShapes = [], annotations: rawAnnotations = [] } = props;

  const { width, height } = useThree((state) => state.size);
  const { dataToHtml } = useVisCanvasContext();

  const scaleCoord = useScaleCoord();

  const shapes = useCameraState(
    (camera) => {
      return rawShapes.map((shape) => {
        const v0 = dataToHtml(
          camera,
          scaleCoord(shape.x0, shape.y0, shape.xref, shape.yref)
        );
        const v1 = dataToHtml(
          camera,
          scaleCoord(shape.x1, shape.y1, shape.xref, shape.yref)
        );
        return { v0, v1, ...shape };
      });
    },
    [rawShapes]
  );
  const annotations = useCameraState(
    (camera) => {
      return rawAnnotations.map((annotation, i) => {
        const v = dataToHtml(
          camera,
          scaleCoord(
            annotation.x,
            annotation.y,
            annotation.xref,
            annotation.yref
          )
        );
        return { v, ...annotation };
      });
    },
    [rawAnnotations]
  );

  return (
    <Overlay>
      <svg width={width} height={height}>
        {shapes.map((shape, i) => {
          const { v0, v1, fillcolor, line, opacity } = shape;
          return (
            <rect
              key={i}
              x={Math.min(v0.x, v1.x)}
              y={Math.min(v0.y, v1.y)}
              width={Math.abs(v1.x - v0.x)}
              height={Math.abs(v1.y - v0.y)}
              fill={fillcolor}
              strokeWidth={line?.width}
              fillOpacity={opacity}
            />
          );
        })}
        {annotations.map((annotation, i) => {
          const { text, v } = annotation;
          return (
            <text key={i} x={v.x} y={v.y} textAnchor="middle">
              {text}
            </text>
          );
        })}
      </svg>
    </Overlay>
  );
}

export default PlotOverlay;
