import type { ColorMap, Domain } from '@h5web/lib';
import { ScatterVis, useAxisValues, useDomain } from '@h5web/lib';
import ndarray from 'ndarray';
import { useMemo } from 'react';

const DEFAULT_DOMAIN: Domain = [0, 1];

interface Props {
  x: number[] | undefined;
  y: number[] | undefined;
  dataValues: number[];
  colorMap: ColorMap;
  size: number;
  title: string;
  onClick?: (index: number) => void;
}

function H5WebScatterPlot(props: Props) {
  const { x, y, dataValues, size, colorMap, title, onClick } = props;

  const domain = useDomain(dataValues) || DEFAULT_DOMAIN;

  const nPoints = dataValues.length;

  const abscissaValue = useAxisValues(x, nPoints);
  const ordinateValue = useAxisValues(y, nPoints);
  const dataArray = useMemo(() => ndarray(dataValues), [dataValues]);

  return (
    <ScatterVis
      abscissaParams={{ value: abscissaValue }}
      ordinateParams={{ value: ordinateValue }}
      dataArray={dataArray}
      domain={domain}
      colorMap={colorMap}
      size={size}
      onPointClick={onClick}
      title={title}
    />
  );
}

export default H5WebScatterPlot;
