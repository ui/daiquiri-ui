import { hasValues } from './guards';
import H5WebScatterPlot from './H5WebScatterPlot';
import type { MeshData } from './models';

interface Props {
  data: (MeshData | undefined)[];
  onClick?: (index: number) => void;
}

function H5WebScatterContainer(props: Props) {
  const { data, onClick } = props;
  const [scatterData] = data;

  if (scatterData === undefined) {
    return <div>No data. Please select a scan.</div>;
  }

  const { marker, x, y } = scatterData;
  const { color: dataValues, colorscale, size } = marker;

  if (!hasValues(dataValues)) {
    return <div>Values are undefined. Please select a series.</div>;
  }

  return (
    <H5WebScatterPlot
      x={x}
      y={y}
      dataValues={dataValues}
      colorMap={colorscale}
      size={size}
      onClick={onClick}
      title={scatterData.name}
    />
  );
}

export default H5WebScatterContainer;
