import '@testing-library/jest-dom';
import '@testing-library/jest-dom/extend-expect';

// blobUrl
window.URL.createObjectURL = jest.fn(() => '');

// Intersection Observer
const observeFn = jest.fn();
const unobserveFn = jest.fn();
class MockObserver {
  constructor(fn) {
    fn([], this);
  }

  observe() {
    return observeFn();
  }

  unobserve() {
    return unobserveFn();
  }
}
window.IntersectionObserver = MockObserver;
window.ResizeObserver = MockObserver;

// XHRImage not inherited properly with jest
window.HTMLImageElement.prototype.load = jest.fn(() => {
  // console.log('HTMLImageElement loading', url)
  // this.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII='
});

// increase timeout
jest.setTimeout(20000);

// Something broken in jest 27
// https://github.com/prisma/prisma/issues/8558#issuecomment-1006100001
global.setImmediate = jest.useRealTimers;

// ResizeObserver
class ResizeObserver {
  observe() {
    /* do nothing */
  }
  unobserve() {
    /* do nothing */
  }
  disconnect() {
    /* do nothing */
  }
}

window.ResizeObserver = ResizeObserver;
