import MessageHandler from 'messagehandler/MessageHandler';

import { throttle } from 'lodash';

import app from 'providers/app';
import metadata from 'providers/metadata';
import scans from 'providers/scans';
import { getUserPreferencesValue } from '../components/utils/UseUserPreference';

export default class ScansMessageHandler extends MessageHandler {
  protected namespace = 'scans';
  protected fetchScanDataDebounced: (scanid: number) => void;

  protected handlers = {
    new_scan: this.onNewScan,
    end_scan: this.onEndScan,
    new_data: this.onNewData,
  };

  public constructor() {
    super();
    this.fetchScanDataDebounced = throttle(this.fetchScanData, 5000);
  }

  private isToastForScans(): boolean {
    return getUserPreferencesValue<boolean>(
      'toasts/scans',
      true,
      this.store.getState()
    );
  }

  private onNewScan(data: any) {
    scans.dispatch('SCAN_NEW', data);
    this.fetchScans();
    scans.getNamespace('data').setPage(null, 'default');

    if (this.isToastForScans()) {
      app.dispatch('ADD_TOAST', {
        title: 'New Scan',
        text: `Title: ${data.title}, id ${data.scanid}`,
        type: 'info',
      });
    }
  }

  private onEndScan(data: any) {
    scans.dispatch('SCAN_END', data);
    this.fetchScans();
    this.fetchScanData(data.scanid);
    if (this.isToastForScans()) {
      app.dispatch('ADD_TOAST', {
        title: 'Scan Finished',
        text: `Scan ${data.scanid} finished`,
        type: 'info',
      });
    }
  }

  private onNewData(data: any) {
    scans.dispatch('SCAN_PROGRESS', data);
    this.fetchScanDataDebounced(data.scanid);
  }

  private fetchScans() {
    scans.getNamespace('list').fetch(null, true);
  }

  private fetchScanData(scanid: any) {
    const instances = scans.getNamespace('data').getInstances();
    instances.forEach((instance: string) => {
      const sns = scans.getNamespace('data').getInstance(instance);
      const currentScan = scans.selector('selectedScan', this.store.getState());
      const fetching = sns.selector('fetching', this.store.getState());
      if (!fetching && (scanid === currentScan || currentScan === null)) {
        sns.setParams({ scanid }, true);
      }
    });
  }
}
