import MessageHandler from 'messagehandler/MessageHandler';

import app from 'providers/app';

export default class ChatMessageHandler extends MessageHandler {
  protected namespace = 'chat';

  protected handlers = {
    new_message: this.onNewMessage,
    message_read: this.onMessageRead,
    all_message_read: this.onAllMessageRead,
  };

  private onNewMessage(data: any) {
    app.getNamespace('chat').addLocal(null, true, data);
  }

  private onMessageRead(data: any) {
    app.getNamespace('chat').updateLocal(null, true, data);
  }

  private onAllMessageRead() {
    app.getNamespace('chat').fetch(null, true);
  }
}
