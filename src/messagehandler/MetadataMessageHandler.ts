import { debounce } from 'lodash';
import MessageHandler from 'messagehandler/MessageHandler';

import metadata from 'providers/metadata';

export default class MetadataMessageHandler extends MessageHandler {
  protected namespace = 'metadata';

  protected handlers = { message: this.onMessage };

  public constructor() {
    super();
    this.fetchXrfMaps = debounce(this.fetchXrfMaps, 500);
  }

  private onMessage(data: any) {
    console.log('onMessage', data);
    if (data.type === 'sample') {
      metadata.getNamespace('samples').fetch(null, true);
      metadata.getNamespace('components').fetch(null, true);
    }

    if (data.type === 'subsample') {
      metadata.getNamespace('subsamples').fetch(null, true);
    }

    if (data.type === 'sampleimage') {
      metadata.getNamespace('images').fetch(null, true);
    }

    if (data.type === 'datacollection') {
      metadata.getNamespace('datacollections').fetch(null, true);
      metadata.getNamespace('subsamples').fetch(null, true);
      metadata.getNamespace('samples').fetch(null, true);
      metadata.getNamespace('components').fetch(null, true);
    }

    if (data.type === 'xrf_map') {
      this.fetchXrfMaps();
    }
  }

  private fetchXrfMaps() {
    metadata.getNamespace('xrf_maps').fetch(null, true);
  }
}
