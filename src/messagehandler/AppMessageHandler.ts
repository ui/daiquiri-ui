import MessageHandler from 'messagehandler/MessageHandler';

import app from 'providers/app';

export default class AppMessageHandler extends MessageHandler {
  protected namespace = 'app';

  protected handlers = {
    log_record: this.onLogRecord,
    reloader: this.onReloader,
  };

  private onLogRecord(data: any) {
    app.dispatch('ADD_LOG', data);

    if (
      data.type === 'actor' &&
      (data.level === 'EXCEPTION' || data.level === 'ERROR')
    ) {
      app.dispatch('ADD_TOAST', {
        type: 'error',
        title: 'Actor Failed',
        text: data.message,
      });
    }

    if (
      data.type === 'app' &&
      (data.level === 'EXCEPTION' || data.level === 'ERROR')
    ) {
      app.dispatch('ADD_TOAST', {
        type: 'error',
        title: 'Application Exception',
        text: data.message,
      });
    }

    if (
      data.type === 'hardware' &&
      (data.level === 'EXCEPTION' || data.level === 'ERROR')
    ) {
      app.dispatch('ADD_TOAST', {
        type: 'error',
        title: 'Hardware Exception',
        text: data.message,
      });
    }
  }

  private onReloader(success: any) {
    if (success) {
      const reload = 5;
      app.dispatch('ADD_TOAST', {
        type: 'warning',
        title: 'Server Config Reloaded',
        text: `UI will reload in ${reload}s`,
      });

      setTimeout(() => {
        app.dispatch('ADD_TOAST', {
          type: 'success',
          title: 'Reloading',
          text: `Reloading UI`,
        });
        setTimeout(() => {
          window.location.reload();
        }, 500);
      }, reload * 1000);
    } else {
      app.dispatch('ADD_TOAST', {
        type: 'error',
        title: 'Server Config Not Reloaded',
        text: 'Config could not be reloaded, check the log',
      });
    }
  }
}
