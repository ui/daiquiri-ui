import MessageHandler from 'messagehandler/MessageHandler';
import { each } from 'lodash';

export default class PersistMessageHandler extends MessageHandler {
  protected namespace = 'persist';

  protected handlers = { state_update: this.onStateUpdate };

  private onStateUpdate(data: any) {
    each(data, (state, key) => {
      const normKey = key.replace('persist:', '');
      this.store.dispatch({
        type: 'persist/REHYDRATE',
        key: normKey,
        payload: state,
      });
    });
  }
}
