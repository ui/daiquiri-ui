import MessageHandler from 'messagehandler/MessageHandler';

import app from 'providers/app';
import session from 'providers/session';

export default class SessionMessageHandler extends MessageHandler {
  protected namespace = 'session';

  protected handlers = { message: this.onMessage };

  private onMessage(data: any) {
    switch (data.type) {
      case 'request_control': {
        session.dispatch('FETCH_SESSIONS');
        session.dispatch('CONTROL_REQUEST', data.sessionid);

        break;
      }
      case 'deny_control': {
        session.dispatch('DENY_REQUEST', data.sessionid);

        break;
      }
      case 'timeout_control': {
        session.dispatch('CONTROL_REQUEST', null);
        app.dispatch('ADD_TOAST', {
          title: 'Control Granted',
          type: 'warning',
          text: `Control was automatically granted to session: ${data.sessionid} due to response timeout`,
        });

        break;
      }
      default: {
        const auth = session.selector('auth', this.store.getState());
        if (auth) {
          session.dispatch('FETCH_SESSIONS');
        }

        if (data.type === 'connect') {
          app.dispatch('ADD_TOAST', {
            title: 'Session Connected',
            type: 'success',
            text: `New session connected for ${data.username}`,
          });
        }

        if (data.type === 'disconnect') {
          app.dispatch('ADD_TOAST', {
            title: 'Session Disconnected',
            text: `Session disconnected ${data.sessionid}`,
          });
        }

        if (data.type === 'mirror') {
          session.dispatch('FETCH_CURRENT_SESSION');
          const sessionid = session.selector(
            'sessionid',
            this.store.getState()
          );
          if (sessionid === data.mirrorid) {
            this.store.enablePersist();
          }
        }

        if (data.type === 'mirror_disconnect') {
          session.dispatch('FETCH_CURRENT_SESSION');
          app.dispatch('ADD_TOAST', {
            title: 'Mirrored Session Disconnected',
            type: 'warning',
            text: `Mirrored session has disconnected`,
          });
        }
      }
    }
  }
}
