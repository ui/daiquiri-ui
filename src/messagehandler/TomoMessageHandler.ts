import MessageHandler from 'messagehandler/MessageHandler';
import tomo from 'providers/tomo';

export default class TomoMessageHandler extends MessageHandler {
  protected namespace = 'tomo';

  protected handlers = {
    new_scan: this.onNewScan,
    end_scan: this.onEndScan,
    new_data: this.onNewData,
    new_dark: this.onNewDark,
    new_flat: this.onNewFlat,
    update_scan_info: this.onUpdateScanInfo,
    update_scan_info_sinogram: this.onUpdateScanInfoSinogram,
    update_scan_info_subscan: this.onUpdateScanInfoSubscan,
  };

  private onNewScan(data: any) {
    this.fetchScanInfo();
    this.fetchDetectors();
    for (let i = 0; i < data.node_names.length; i += 1) {
      tomo.dispatch('DETECTOR_SCAN_START', {
        node_name: data.node_names[i],
        detector_id: data.detector_ids[i],
        scanid: data.scanid,
        frame_no: data.frame_no,
      });
    }
  }

  private onEndScan(data: any) {
    for (let i = 0; i < data.node_names.length; i += 1) {
      tomo.dispatch('DETECTOR_SCAN_STOP', {
        node_name: data.node_names[i],
        detector_id: data.detector_ids[i],
        scanid: data.scanid,
        frame_no: data.frame_no,
      });
    }
  }

  private onNewData(data: any) {
    tomo.dispatch('DETECTOR_DATA_UPDATE', data);
  }

  private onNewFlat(data: any) {
    tomo.dispatch('DETECTOR_FLAT_UPDATE', data);
  }

  private onNewDark(data: any) {
    tomo.dispatch('DETECTOR_DARK_UPDATE', data);
  }

  private onUpdateScanInfo(data: any) {
    tomo.dispatch('UPDATE_SCAN_INFO', data);
  }

  private onUpdateScanInfoSinogram(data: any) {
    tomo.dispatch('UPDATE_SCAN_INFO_SINOGRAM', data);
  }

  private onUpdateScanInfoSubscan(data: any) {
    tomo.dispatch('UPDATE_SCAN_INFO_SUBSCAN', data);
  }

  private fetchDetectors() {
    tomo.getNamespace('detectors').fetch(null, true);
  }

  private fetchScanInfo() {
    tomo.getNamespace('scaninfo').fetch(null, true);
  }
}
