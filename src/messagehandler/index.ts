import { each } from 'lodash';
import type { PersistStore } from '../store';
import type MessageHandler from './MessageHandler';

import AppMessageHandler from 'messagehandler/AppMessageHandler';
import HardwareMessageHandler from 'messagehandler/HardwareMessageHandler';
import QueueMessageHandler from 'messagehandler/QueueMessageHandler';
import ScansMessageHandler from 'messagehandler/ScansMessageHandler';
import SessionMessageHandler from 'messagehandler/SessionMessageHandler';
import TwodMessageHandler from 'messagehandler/TwodMessageHandler';
import MetadataMessageHandler from 'messagehandler/MetadataMessageHandler';
import ChatMessageHandler from 'messagehandler/ChatMessageHandler';
import PersistMessageHandler from 'messagehandler/PersistMessageHandler';
import CeleryMessageHandler from 'messagehandler/CeleryMessageHandler';
import TomoMessageHandler from 'messagehandler/TomoMessageHandler';

class InitHandlers {
  public handlers: Record<string, typeof MessageHandler> = {
    App: AppMessageHandler,
    Hardware: HardwareMessageHandler,
    Queue: QueueMessageHandler,
    Scans: ScansMessageHandler,
    Tomo: TomoMessageHandler,
    Session: SessionMessageHandler,
    Twod: TwodMessageHandler,
    Metadata: MetadataMessageHandler,
    Chat: ChatMessageHandler,
    Persist: PersistMessageHandler,
    Celery: CeleryMessageHandler,
  };

  private instances: Record<string, MessageHandler> = {};

  public start(store: PersistStore) {
    each(this.handlers, (Handler, name) => {
      try {
        const inst = new Handler();
        inst.start(store);
        this.instances[name] = inst;
      } catch (error) {
        console.error(`${name} handler was not created`, error);
      }
    });
  }

  public getHandler(key: string): MessageHandler {
    const handler = this.instances[key];
    if (handler === undefined) {
      throw new Error(`No handler defined for ${key}`);
    }
    return handler;
  }
}

export default new InitHandlers();
