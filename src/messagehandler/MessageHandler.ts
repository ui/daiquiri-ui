import { camelCase, upperFirst, each } from 'lodash';
import SocketIOService from 'services/SocketIOService';
import type { PersistStore } from '../store';

export default class MessageHandler {
  protected namespace = 'undefined';

  protected handlers: Record<string, (data: any) => void> = {};
  protected store!: PersistStore;

  public start(store: PersistStore) {
    this.store = store;
    SocketIOService.addNamespace(this.namespace);
    each(this.handlers, (callback, eventName) => {
      SocketIOService.addCallback(
        this.namespace,
        eventName,
        callback.bind(this)
      );
    });
  }
}
