import MessageHandler from 'messagehandler/MessageHandler';

import hardware from 'providers/hardware';

export default class HardwareMessageHandler extends MessageHandler {
  protected namespace = 'hardware';

  protected handlers = {
    change: this.onChange,
    online: this.onOnline,
    locked: this.onLocked,
    group: this.onGroup,
  };

  private onChange(data: any) {
    hardware.dispatch('UPDATE_HARDWARE', data);
  }

  private onOnline(data: any) {
    hardware.dispatch('UPDATE_HARDWARE_ONLINE', data);
  }

  private onLocked(data: any) {
    hardware.dispatch('UPDATE_HARDWARE_LOCKED', data);
  }

  private onGroup(data: any) {
    hardware.dispatch('UPDATE_HARDWARE_GROUP', data);
  }
}
