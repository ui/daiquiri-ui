import MessageHandler from 'messagehandler/MessageHandler';

import app from 'providers/app';
import queue from 'providers/queue';

export default class QueueMessageHandler extends MessageHandler {
  protected namespace = 'queue';

  protected handlers = {
    message: this.onMessage,
    actor_output: this.onActorOutput,
  };

  private onMessage(data: any) {
    queue.getNamespace('queue').fetch(null, true);

    const toastType =
      data.type === 'error'
        ? 'error'
        : data.type === 'end'
        ? 'success'
        : 'warning';
    app.dispatch('ADD_TOAST', {
      type: toastType,
      title: 'Queue Changed',
      text: `Type: ${data.type}, Message: ${data.message}`,
    });
  }

  private onActorOutput(data: any) {
    queue.dispatch('UPDATE_ACTOR_LOG', data);
  }
}
