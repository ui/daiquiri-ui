import MessageHandler from 'messagehandler/MessageHandler';

import twod from 'providers/2dview';
import app from 'providers/app';

export default class TwodMessageHandler extends MessageHandler {
  protected namespace = 'imageviewer';

  protected handlers = { message: this.onMessage };

  private onMessage(data: any) {
    if (data.type === 'origin' && data.prop === 'additional') {
      twod.dispatch('UPDATE_ADDITIONAL', { [data.object]: data.value });
    }

    if (data.type === 'source') {
      twod.dispatch('UPDATE_SOURCE', data.info);
    }

    if (data.type === 'generate_maps') {
      const messages: Record<string, { message: string; type: string }> = {
        started: {
          message: `Started map generation`,
          type: 'success',
        },
        progress: {
          message: `Maps for ${data.remaining} data collection(s) left to generate`,
          type: 'success',
        },
        finished: { message: 'Map generation complete', type: 'success' },
        warning: { message: data.message, type: 'warning' },
      };

      const status = data.status;
      app.dispatch('ADD_TOAST', {
        type: messages[status].type,
        title: 'Map Generation',
        text: messages[status].message,
      });
    }
  }
}
