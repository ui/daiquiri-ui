import { each } from 'lodash';
import MessageHandler from 'messagehandler/MessageHandler';

type Listener = (data: any) => void;

export default class CeleryMessageHandler extends MessageHandler {
  protected namespace = 'celery';

  protected handlers = { message: this.onMessage };

  private readonly listeners: Listener[] = [];

  public addListener(listener: Listener) {
    this.listeners.push(listener);
  }

  public removeListener(listener: Listener) {
    const idx = this.listeners.indexOf(listener);
    if (idx > -1) this.listeners.splice(idx, 1);
  }

  private onMessage(data: any) {
    each(this.listeners, (listener) => {
      listener(data);
    });
  }
}
