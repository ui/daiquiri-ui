import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import App from 'App';
import store from 'store';
import type { Component } from 'react';

import 'scss/main.scss';
import '@h5web/app/dist/styles.css';

import * as serviceWorker from 'serviceWorker';

import Start from 'Start';
import { CacheProvider } from '@rest-hooks/react';

Start.start(store, undefined);

function render(Comp: any) {
  ReactDOM.render(
    <CacheProvider>
      <Provider store={store}>
        <Comp />
      </Provider>
    </CacheProvider>,
    document.querySelector('#root')
  );
}

render(App);
serviceWorker.unregister();

// HMR
// @ts-expect-error
if (module.hot) {
  // @ts-expect-error
  module.hot.accept('./App', () => {
    const NextApp = require('./App').default; // eslint-disable-line @typescript-eslint/no-require-imports
    render(NextApp);
  });
}
