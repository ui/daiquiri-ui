import { connect } from 'react-redux';
import { values } from 'lodash';

import { withNamespace } from 'providers/namespace';
import app from 'providers/app';
import session from 'providers/session';
import metadata from 'providers/metadata';
import { getUnread } from 'helpers/chat';

import Chat from 'components/Chat';

const mapStateToProps = (state, own) => ({
  messages: own.providers.app.chat.selector('results', state),
  sessions: session.selector('sessions', state),
  sessionid: session.selector('sessionid', state),
  cache: own.providers.metadata.cache.selector('results', state).cache,
  unread: getUnread({
    messages: values(own.providers.app.chat.selector('results', state)),
    chatCursor: own.providers.metadata.cache.selector('results', state).cache
      .chatCursor,
    sessionid: session.selector('sessionid', state)
  }),
  loadCursor: app.selector('chatCursor', state)
});

const mapDispatchToProps = (dispatch, own) => ({
  actions: {
    fetchMessages: payload =>
      own.providers.app.chat.fetch({
        limit: 25,
        ...payload
      }),
    sendMessage: payload => own.providers.app.chat.add(payload),
    markRead: payload => app.dispatch('MARK_MESSAGE_READ', payload),
    updateCache: payload => own.providers.metadata.cache.update(payload)
  }
});

export default withNamespace({ app, metadata })(
  connect(mapStateToProps, mapDispatchToProps)(Chat)
);
