import type { ComponentProps, ComponentType } from 'react';

export interface ComponentWithOptions<C extends ComponentType<any>> {
  providers: Record<string, any>;
  options: ComponentProps<C>['options'];
}
