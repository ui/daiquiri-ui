import { connect } from 'react-redux';

import LoginRoute from 'components/utils/LoginRoute';
import session from 'providers/session';

const mapStateToProps = (state, ownProps) => ({
  isLoggedOut: !session.selector('auth', state)
});

export default connect(mapStateToProps)(LoginRoute);
