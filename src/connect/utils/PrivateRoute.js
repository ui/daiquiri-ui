import { connect } from 'react-redux';

import PrivateRoute from 'components/utils/PrivateRoute';
import session from 'providers/session';
import app from 'providers/app';

const mapStateToProps = (state, ownProps) => ({
  isLoggedOut: !session.selector('auth', state)
});

export default connect(mapStateToProps)(PrivateRoute);
