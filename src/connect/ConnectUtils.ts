import lodash from 'lodash';

export class Cache {
  private cache: Record<string, any>;

  public constructor() {
    this.cache = {};
  }

  /**
   * Returns and store value if the previous stored value deep differ.
   *
   * Else returns the previous stored value
   */
  public debounce<T>(name: string, value: T): T {
    const previous = this.cache[name];
    if (lodash.isEqual(previous, value)) {
      return previous;
    }
    this.cache[name] = value;
    return value;
  }

  /**
   * Compute and returns and cache a value if the previous stored value deep differ.
   *
   * Else returns the previous computed result.
   */
  public memo<T>(name: string, callable: () => T, deps: unknown[]): T {
    const defaultResult = [undefined, undefined];
    const [previousResult, previousDeps] = this.cache[name] || defaultResult;
    if (lodash.isEqual(previousDeps, deps)) {
      return previousResult;
    }
    const result = callable();
    this.cache[name] = [result, deps];
    return result;
  }
}

export default function mapStateToPropsWithCache<S, O, R>(
  mapStateToProps: (state: S, own: O, cache: Cache) => R
): (state: S, own: O) => R {
  const cache = new Cache();
  return (state, own) => mapStateToProps(state, own, cache);
}
