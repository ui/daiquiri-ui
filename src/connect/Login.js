import { connect } from 'react-redux';

import Login from 'components/Login';
import { withNamespace } from 'providers/namespace';

import session from 'providers/session';
import metadata from 'providers/metadata';

const mapStateToProps = (state, own) => ({
  error: session.selector('error', state),
  userError: own.providers.metadata.user.selector('error', state)
});

const mapDispatchToProps = () => ({
  actions: {
    login: payload => session.dispatch('LOGIN', payload)
  }
});

export default withNamespace({ metadata })(
  connect(mapStateToProps, mapDispatchToProps)(Login)
);
