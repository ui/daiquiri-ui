import { connect } from 'react-redux';
import { withNamespace } from 'providers/namespace';
import tomo from 'providers/tomo';
import TomoScanList from 'components/tomo/scanlist/Default';

function mapStateToProps(state: any, own: any) {
  return {
    tasks: tomo.getScanTasks(state),
  };
}

function mapDispatchToProps(dispatch: any, own: any) {
  return {
    actions: tomo.scanTaskActions(),
  };
}

export default withNamespace({ tomo })(
  connect(mapStateToProps, mapDispatchToProps)(TomoScanList)
);
