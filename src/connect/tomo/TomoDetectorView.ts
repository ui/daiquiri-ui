import { connect } from 'react-redux';
import TomoDetectorView from 'components/tomo/detectorview/Default';
import type { ComponentWithOptions } from '../utils/withOptions';

function mapStateToProps(
  state: any,
  own: ComponentWithOptions<typeof TomoDetectorView>
) {
  return {};
}

function mapDispatchToProps(dispatch: any, own: any) {
  return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(TomoDetectorView);
