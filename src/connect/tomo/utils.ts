import type { Hardware, MotorSchema } from '@esrf/daiquiri-lib';
import { getObjFromId } from 'connect/hardware/utils';
import type { HardwareStore } from 'types/Hardware';
import type { TomoDetectorSchema } from 'components/hardware/tomodetector';
import type { LimaSchema } from '../../components/hardware/lima';
import type { TomoSampleStageSchema } from '../../components/hardware/tomosamplestage';

/**
 * Helper to use tomoconfig which is the default, but allow to enforce different
 * hardware in the configuration
 */
function getFromIdElseFromTomoConfig<H extends Hardware>(
  hardwareMap: HardwareStore,
  id: undefined | null | string,
  tomoconfigId: undefined | null | string,
  tomoconfigAttr: string
): H | null {
  if (id) {
    return getObjFromId<H>(hardwareMap, id);
  }
  if (tomoconfigId) {
    const otherId = `${tomoconfigId}.${tomoconfigAttr}`;
    return getObjFromId<H>(hardwareMap, otherId);
  }
  return null;
}

/**
 * Stable description refering to the different object used by the tomo beamline
 */
export interface TomoSampleStageRefs {
  sx: string | null;
  sy: string | null;
  sampx: string | null;
  sampy: string | null;
  sampu: string | null;
  sampv: string | null;
  sz: string | null;
  somega: string | null;
  detector: string | null;
  tomoDetector: string | null;
  holotomo: string | null;
  sampleStage: string | null;
}

export interface TomoConfigHardware {
  sampleStage: TomoSampleStageSchema | null;
  sy: MotorSchema | null;
  sampx: MotorSchema | null;
  sampy: MotorSchema | null;
  sampu: MotorSchema | null;
  sampv: MotorSchema | null;
  sz: MotorSchema | null;
  somega: MotorSchema | null;
  detector: LimaSchema | null;
  tomoDetector: TomoDetectorSchema | null;
}

export function getTomoHardware(
  hardwareMap: Record<string, Hardware>,
  own: any
): TomoConfigHardware {
  const tomoconfigId = own.tomoconfig ?? 'ACTIVE_TOMOCONFIG.ref';

  const sampleStage = getFromIdElseFromTomoConfig<TomoSampleStageSchema>(
    hardwareMap,
    own.sampleStageId,
    tomoconfigId,
    'sample_stage'
  );

  const sy = getFromIdElseFromTomoConfig<MotorSchema>(
    hardwareMap,
    own.syId,
    tomoconfigId,
    'sample_stage.sy'
  );
  const sampx = getFromIdElseFromTomoConfig<MotorSchema>(
    hardwareMap,
    own.sampxId,
    tomoconfigId,
    'sample_stage.sampx'
  );
  const sampy = getFromIdElseFromTomoConfig<MotorSchema>(
    hardwareMap,
    own.sampyId,
    tomoconfigId,
    'sample_stage.sampy'
  );
  const sampu = getFromIdElseFromTomoConfig<MotorSchema>(
    hardwareMap,
    own.sampuId,
    tomoconfigId,
    'sample_stage.sampu'
  );
  const sampv = getFromIdElseFromTomoConfig<MotorSchema>(
    hardwareMap,
    own.sampvId,
    tomoconfigId,
    'sample_stage.sampv'
  );
  const sz = getFromIdElseFromTomoConfig<MotorSchema>(
    hardwareMap,
    own.szId,
    tomoconfigId,
    'sample_stage.sz'
  );
  const somega = getFromIdElseFromTomoConfig<MotorSchema>(
    hardwareMap,
    own.somegaId,
    tomoconfigId,
    'sample_stage.somega'
  );
  const detector = getFromIdElseFromTomoConfig<LimaSchema>(
    hardwareMap,
    own.detectorId,
    tomoconfigId,
    'detectors.active_detector.detector'
  );
  const tomoDetector = getFromIdElseFromTomoConfig<TomoDetectorSchema>(
    hardwareMap,
    own.tomoDetectorId,
    tomoconfigId,
    'detectors.active_detector'
  );

  return {
    sy,
    sampx,
    sampy,
    sampu,
    sampv,
    sz,
    somega,
    detector,
    tomoDetector,
    sampleStage,
  };
}
