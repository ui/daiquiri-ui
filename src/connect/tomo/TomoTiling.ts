import TomoTiling from 'components/tomo/tiling/Default';
import { withNamespace } from 'providers/namespace';
import tomo from 'providers/tomo';
import hardwarep from 'providers/hardware';
import { connect } from 'react-redux';
import type { ComponentWithOptions } from '../utils/withOptions';

function mapStateToProps(
  state: any,
  own: ComponentWithOptions<typeof TomoTiling>
) {
  return {
    tasks: tomo.getScanTasks(state),
  };
}

function mapDispatchToProps(dispatch: any, own: any) {
  return {
    actions: {
      ...tomo.scanTaskActions(),
    },
  };
}

export default withNamespace({ tomo, hardware: hardwarep })(
  connect(mapStateToProps, mapDispatchToProps)(TomoTiling)
);
