import { connect } from 'react-redux';
import TomoSinogram from 'components/tomo/sinogram/Default';
import { withNamespace } from 'providers/namespace';
import tomo from 'providers/tomo';
import type { Cache } from 'connect/ConnectUtils';
import mapStateToPropsWithCache from 'connect/ConnectUtils';
import type { ComponentWithOptions } from '../utils/withOptions';

function mapStateToPropsSimple(
  state: any,
  own: ComponentWithOptions<typeof TomoSinogram>,
  cache: Cache
) {
  const { providers, options } = own;

  const scaninfo = providers?.tomo.scaninfo.selector('results', state);
  const { lastgroup } = scaninfo;
  const sinogram = lastgroup ? lastgroup.sinogram : undefined;

  function getDataCollectionId() {
    const id = options.datacollectionid;
    if (id === 'last') {
      const scaninfo = providers?.tomo.scaninfo.selector('results', state);
      const { lastgroup } = scaninfo;
      if (!lastgroup) {
        return null;
      }
      return lastgroup.datacollectionid;
    }
    return id;
  }
  const realdatacollectionid = getDataCollectionId();

  return {
    selectedScan: lastgroup ? lastgroup.scanid : null,
    sinogram: cache.debounce('sinogram', sinogram),
    datacollectionid: realdatacollectionid,
    datacollectionMeta: tomo.getDataCollectionMeta(state, realdatacollectionid),
  };
}

function mapDispatchToProps(dispatch: any, own: any) {
  return {
    actions: {
      ...tomo.dataCollectionMetaActions(),
    },
  };
}

export default withNamespace({ tomo })(
  connect(
    mapStateToPropsWithCache(mapStateToPropsSimple),
    mapDispatchToProps
  )(TomoSinogram)
);
