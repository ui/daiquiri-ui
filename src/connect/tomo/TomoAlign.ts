import { connect } from 'react-redux';
import TomoAlign from 'components/tomo/align/Default';
import type { ComponentWithOptions } from '../utils/withOptions';

function mapStateToProps(
  state: any,
  own: ComponentWithOptions<typeof TomoAlign>
) {
  const { providers } = own;
  return {};
}

function mapDispatchToProps(dispatch: any, own: any) {
  return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(TomoAlign);
