import { connect } from 'react-redux';

import DropdownFieldOfView from 'components/tomo/dropdownfieldofview/DropdownFieldOfView';
import { withNamespace } from 'providers/namespace';
import scans from 'providers/scans';
import tomo from 'providers/tomo';
import hardwarep from 'providers/hardware';
import { getObjFromId } from 'connect/hardware/utils';
import type { TomoDetectorSchema } from 'components/hardware/tomodetector';
import { isTomoDetectorHardware } from 'components/hardware/tomodetector';
import type { OpticSchema } from 'components/hardware/optic';
import type { Cache } from '../ConnectUtils';
import mapStateToPropsWithCache from '../ConnectUtils';
import type { TomoAvailableFieldOfView } from 'types/Tomo';
import type { LimaSchema } from 'components/hardware/lima';
import { isLimaHardware } from 'components/hardware/lima';

function getAvailableFieldOfViews(
  hardwareMap: any,
  detectors: any
): TomoAvailableFieldOfView[] {
  if (detectors === undefined) {
    // tomo provider is not yet feed
    return [];
  }
  const fovs: TomoAvailableFieldOfView[] = [];

  const tomo_detectors = Object.keys(detectors).map((key) => {
    return [key, detectors[key].tomo_detector_id];
  });
  tomo_detectors.forEach((ids) => {
    const [lima, id] = ids;
    const tomoDetector: TomoDetectorSchema | null = getObjFromId(
      hardwareMap,
      id
    );
    const detector: LimaSchema | null = getObjFromId(
      hardwareMap,
      `${id}.detector`
    );
    if (tomoDetector === null || detector === null) {
      console.debug(`${id} not there`);
      return;
    }
    if (!isLimaHardware(detector)) {
      return;
    }
    if (!isTomoDetectorHardware(detector)) {
      return;
    }
    if (!tomoDetector.properties.state) {
      return;
    }
    if (!tomoDetector.properties.sample_pixel_size_mode) {
      return;
    }

    function readState(): TomoAvailableFieldOfView['state'] {
      if (
        !tomoDetector ||
        !tomoDetector.online ||
        tomoDetector.properties.state === 'OFFLINE'
      ) {
        return 'DETECTOR_OFFLINE';
      }
      return 'READY';
    }
    const state = readState();

    const mode = tomoDetector.properties.sample_pixel_size_mode;
    if (mode === 'USER') {
      // the optic will not be used
      if (tomoDetector.properties.user_sample_pixel_size !== null) {
        fovs.push({
          detector: lima,
          state,
          size: detector.properties.size,
          userPixelSize: tomoDetector.properties.user_sample_pixel_size,
        });
      }
    } else {
      const optic: OpticSchema | null = getObjFromId(
        hardwareMap,
        `${id}.optic`
      );
      if (optic === null) {
        console.debug(`${tomoDetector.properties.optic} not there`);
        return;
      }
      if (!optic.properties.available_magnifications) {
        console.debug(`${tomoDetector.properties.optic} no magnifications`);
        return;
      }
      optic.properties.available_magnifications.forEach(
        (magnification: number) => {
          fovs.push({
            detector: lima,
            state,
            // @ts-expect-error // null was already excluded
            size: tomoDetector.properties.actual_size,
            autoPixelSize: {
              magnification,
              // @ts-expect-error // null was already excluded
              cameraPixelSize: tomoDetector.properties.camera_pixel_size,
            },
          });
        }
      );
    }
  });
  return fovs;
}

function mapStateToProps(state: any, own: any, cache: Cache) {
  const { hardware } = own.providers.hardware;
  const hardwareMap = hardware.selector('results', state);
  const detectors = own.providers.tomo.detectors.selector('results', state);
  const fovs: TomoAvailableFieldOfView[] = getAvailableFieldOfViews(
    hardwareMap,
    detectors
  );

  return {
    detectors,
    options: cache.debounce('options', {}),
    value: own.value || null,
    variant: own.variant || undefined,
    onSelect: own.onSelect,
    availableSelections: fovs,
  };
}

function mapDispatchToProps(dispatch: any, own: any) {
  return {
    actions: {},
  };
}

export default withNamespace({ scans, tomo, hardware: hardwarep })(
  connect(
    mapStateToPropsWithCache(mapStateToProps),
    mapDispatchToProps
  )(DropdownFieldOfView)
);
