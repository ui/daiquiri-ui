import { connect } from 'react-redux';
import TomoReconstructedSinogram from 'components/tomo/reconstructedslice/Default';
import { withNamespace } from 'providers/namespace';
import tomo from 'providers/tomo';
import type { ComponentWithOptions } from '../utils/withOptions';

function mapStateToPropsSimple(
  state: any,
  own: ComponentWithOptions<typeof TomoReconstructedSinogram>
) {
  const { providers, options } = own;

  function getDataCollectionIds() {
    const id = options.datacollectionid;
    if (id === 'last') {
      const scaninfo = providers?.tomo.scaninfo.selector('results', state);
      const { lastgroup } = scaninfo;
      if (!lastgroup) {
        return null;
      }
      return {
        datacollectionid: lastgroup.datacollectionid,
        datacollectiongroupid: lastgroup.datacollectiongroupid,
      };
    }
    return {
      datacollectionid: options.datacollectionid,
      datacollectiongroupid: undefined,
    };
  }
  const realDataCollectionIds = getDataCollectionIds();

  return {
    ...realDataCollectionIds,
    datacollectionMeta: tomo.getDataCollectionMeta(
      state,
      realDataCollectionIds?.datacollectionid
    ),
  };
}

function mapDispatchToProps(dispatch: any, own: any) {
  return {
    actions: {
      ...tomo.dataCollectionMetaActions(),
    },
  };
}

export default withNamespace({ tomo })(
  connect(mapStateToPropsSimple, mapDispatchToProps)(TomoReconstructedSinogram)
);
