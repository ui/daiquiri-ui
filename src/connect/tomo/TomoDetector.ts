import { connect } from 'react-redux';
import TomoDetector from 'components/tomo/detector/Default';
import type { ComponentWithOptions } from '../utils/withOptions';

function mapStateToProps(
  state: any,
  own: ComponentWithOptions<typeof TomoDetector>
) {
  return {};
}

function mapDispatchToProps(dispatch: any, own: any) {
  return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(TomoDetector);
