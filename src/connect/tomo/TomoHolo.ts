import { connect } from 'react-redux';
import TomoHolo from 'components/tomo/holo/Default';
import type { ComponentWithOptions } from '../utils/withOptions';

function mapStateToProps(
  state: any,
  own: ComponentWithOptions<typeof TomoHolo>
) {
  return {};
}

function mapDispatchToProps(dispatch: any, own: any) {
  return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(TomoHolo);
