import { connect } from 'react-redux';
import Console from 'components/Console';
import session from 'providers/session';

function mapStateToProps(state: any, own: any) {
  const { providers } = own;
  return {
    operator: session.selector('operator', state),
  };
}

function mapDispatchToProps() {
  return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(Console);
