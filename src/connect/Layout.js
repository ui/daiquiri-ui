import { connect } from 'react-redux';
import {
  YAMLLayout,
  registerYamlComponents,
  registerHardwareComponent
} from '@esrf/daiquiri-lib';
import app from 'providers/app';
import {
  baseComponentDidCatch,
  ResolvedErrorMessage
} from 'components/ErrorBoundary';
import { lazyComponentMap } from '../components/yaml-layout/components/componentMap';
import HardwareGroup2 from 'connect/hardware/HardwareGroup2';

registerHardwareComponent('HardwareGroup', HardwareGroup2);
registerYamlComponents(lazyComponentMap());

const mapStateToProps = state => ({
  layout: app.selector('selectedLayout', state),
  className: 'main',
  unhandledComponentDidCatch: baseComponentDidCatch,
  UnhandledErrorComponent: ResolvedErrorMessage
});

export default connect(mapStateToProps)(YAMLLayout);
