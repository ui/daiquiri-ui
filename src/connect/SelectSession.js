import { connect } from 'react-redux';

import { withNamespace } from 'providers/namespace';

import SelectSession from 'components/SelectSession';

import session from 'providers/session';
import metadata from 'providers/metadata';

const mapStateToProps = (state, own) => ({
  sessions: own.providers.metadata.blsessions.selector('results', state)
});

const mapDispatchToProps = dispatch => ({
  actions: {
    selectSession: payload => session.dispatch('SELECT_BLSESSION', payload)
  }
});

export default withNamespace({ metadata })(
  connect(mapStateToProps, mapDispatchToProps)(SelectSession)
);
