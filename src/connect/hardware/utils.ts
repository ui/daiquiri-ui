import type { Hardware, MotorSchema } from '@esrf/daiquiri-lib';
import type { TomoConfigSchema } from 'components/hardware/tomoconfig';
import type { TomoDetectorSchema } from 'components/hardware/tomodetector';
import type { TomoDetectorsSchema } from 'components/hardware/tomodetectors';
import type { TomoHoloSchema } from '../../components/hardware/tomoholo';

/**
 * Get hardware object description from an id
 *
 * The id can contains dot to express reference to subcomponent exposed by
 * object property.
 *
 * Here is few examples:
 *
 * - `obj`
 * - `obj.subcomponent`
 * - `obj.subcomponent.subsubcomponent`
 */
export function getObjFromId<H extends Hardware = Hardware>(
  hardwareMap: Record<string, Hardware>,
  id: string
): H | null {
  if (!id) {
    return null;
  }
  const idElements = id.split(/\./);
  let obj: Hardware | undefined = hardwareMap[idElements[0]];
  if (!obj) {
    return null;
  }
  for (let i = 1; i < idElements.length; i += 1) {
    const subobjName: string = obj.properties[idElements[i]];
    if (!subobjName) {
      return null;
    }
    if (!subobjName.startsWith('hardware:')) {
      // FIXME: Handle user error, the content is not of hardware kind
      return null;
    }
    const hardwareName: string = subobjName.slice(9);
    if (subobjName === '') {
      return null;
    }
    obj = hardwareMap[hardwareName];
    if (obj === undefined || obj === null) {
      // FIXME: An error could be displayed if there is still idElements to read
      return null;
    }
  }
  // FIXME: Would be nice to have a real/basic type checking, for example based on Hardware['type']
  return obj as H;
}

export function getObjFromRef<H extends Hardware = Hardware>(
  hardwareMap: Record<string, Hardware>,
  ref: string | undefined | null
): H | null {
  if (!ref) {
    return null;
  }
  if (!ref.startsWith('hardware:')) {
    return null;
  }
  const id = ref.slice(9);
  return getObjFromId(hardwareMap, id);
}
