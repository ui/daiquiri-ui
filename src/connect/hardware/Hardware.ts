import { connect } from 'react-redux';
import { Formatting } from '@esrf/daiquiri-lib';

import { withNamespace } from 'providers/namespace';
import session from 'providers/session';
import hardwarep from 'providers/hardware';
import { getObjFromId } from 'connect/hardware/utils';
import scans from 'providers/scans';
import app from 'providers/app';

import MockCache from 'components/MockCache';
import HardwareObject from 'components/hardware/HardwareObject';
import type { Cache } from 'connect/ConnectUtils';
import mapStateToPropsWithCache from 'connect/ConnectUtils';
import type { ComponentWithOptions } from '../utils/withOptions';

function mapStateToProps(
  state: any,
  own: ComponentWithOptions<typeof HardwareObject> & {
    id: string;
    mock?: string;
  },
  cache: Cache
) {
  const { id, providers, options, mock } = own;
  const { hardware } = providers.hardware;
  const hardwareMap = hardware.selector('results', state);
  const obj = getObjFromId(hardwareMap, id);
  let propertiesSchema = {};
  let callablesSchema = {};
  if (obj) {
    const objectSchema = providers.app.schema.selector('results', state)[
      `HW${obj.id}Schema`
    ];
    const schema =
      objectSchema ||
      providers.app.schema.selector('results', state)[
        `HW${Formatting.ucfirst(obj.type)}Schema`
      ];
    const key = objectSchema ? obj.id : Formatting.ucfirst(obj.type);

    // TODO: Resolve the property/callable schemas from the $ref of:
    //   HW<name>Schema.properties[properties/callables].$ref
    if (schema) {
      propertiesSchema = schema.definitions[`${key}PropertiesSchema`];
      callablesSchema = schema.definitions[`${key}CallablesSchema`];
    }
  }

  if (!obj && mock) {
    MockCache.load({
      keyid: 'id',
      key: id,
      mock: `./mock/hardware/${mock}.json`,
      resolve: (obj2: any) => hardware.addLocal(obj2),
    });
  }

  return {
    id,
    obj,
    propertiesSchema,
    callablesSchema,
    options: cache.debounce('options', options),
    operator: session.selector('operator', state),
    runningScan: scans.selector('runningScanid', state),
  };
}

function mapDispatchToProps(dispatch: any) {
  return {};
}

export default withNamespace({ hardware: hardwarep, app })(
  connect(
    mapStateToPropsWithCache(mapStateToProps),
    mapDispatchToProps
  )(HardwareObject)
);
