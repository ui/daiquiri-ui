import { connect } from 'react-redux';
import { withNamespace } from 'providers/namespace';
import session from 'providers/session';
import hardwarep from 'providers/hardware';
import scans from 'providers/scans';
import HardwareButton from 'components/hardware/HardwareButton';

function mapStateToProps(state: any, own: any) {
  const { providers } = own;
  const { hardware } = providers.hardware;
  return {
    objects: hardware.selector('results', state),
    operator: session.selector('operator', state),
    runningScan: scans.selector('runningScanid', state),
  };
}

function mapDispatchToProps(dispatch: any) {
  return {};
}

export default withNamespace({ hardware: hardwarep })(
  connect(mapStateToProps, mapDispatchToProps)(HardwareButton)
);
