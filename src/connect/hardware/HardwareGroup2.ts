import { connect } from 'react-redux';

import YmlHardwareGroup from 'components/hardware/YmlHardwareGroup';
import { withNamespace } from 'providers/namespace';
import hardwarep from 'providers/hardware';
import type { Cache } from 'connect/ConnectUtils';
import mapStateToPropsWithCache from 'connect/ConnectUtils';

function mapStateToProps(state: any, own: { providers: any }, cache: Cache) {
  const { providers } = own;
  const { hardware, groups } = providers.hardware;
  return {
    fetching: own.providers.hardware.hardware.selector('fetching', state),
    fetched: hardware.selector('fetched', state),
    fetchingGroups: groups.selector('fetching', state),
    fetchedGroups: groups.selector('fetched', state),
  };
}

function mapDispatchToProps(dispatch: any, own: any) {
  const { hardware, groups } = own.providers.hardware;
  return {
    actions: {
      fetch: () => hardware.fetch({ first: true }),
      fetchGroups: () => groups.fetch({ first: true }),
    },
  };
}

export default withNamespace({ hardware: hardwarep })(
  connect(
    mapStateToPropsWithCache(mapStateToProps),
    mapDispatchToProps
  )(YmlHardwareGroup)
);
