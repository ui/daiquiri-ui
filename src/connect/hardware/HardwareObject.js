import { connect } from 'react-redux';
import { resolveSchema, retrieveSchema } from '@rjsf/core/dist/es/utils';
import { Formatting } from '@esrf/daiquiri-lib';

import { withNamespace } from 'providers/namespace';
import session from 'providers/session';
import hardwarep from 'providers/hardware';
import { getObjFromId } from 'connect/hardware/utils';
import scans from 'providers/scans';
import app from 'providers/app';

import MockCache from 'components/MockCache';
import HardwareObject from 'components/hardware/HardwareObject';

const mapStateToProps = (state, own) => {
  const { hardware } = own.providers.hardware;
  const hardwareMap = hardware.selector('results', state);
  const obj = getObjFromId(hardwareMap, own.id);
  let propertiesSchema = {};
  let callablesSchema = {};
  if (obj) {
    // This object may have a dedicated schema
    const objectSchema = own.providers.app.schema.selector('results', state)[
      `HW${obj.id}Schema`
    ];
    const schema =
      objectSchema ||
      own.providers.app.schema.selector('results', state)[
        `HW${Formatting.ucfirst(obj.type)}Schema`
      ];
    const key = objectSchema ? obj.id : Formatting.ucfirst(obj.type);

    // Resolve properties and callables schemas
    if (schema) {
      const resolvedSchema = resolveSchema(schema, schema);
      propertiesSchema = retrieveSchema(
        resolvedSchema.properties.properties,
        resolvedSchema
      );
      // TODO: Have to drop the dependecies back onto the schema?
      propertiesSchema.definitions = resolvedSchema.definitions;
      callablesSchema = retrieveSchema(
        resolvedSchema.properties.callables,
        resolvedSchema
      );
      callablesSchema.definitions = resolvedSchema.definitions;
    }
  }

  if (!obj && own.options.mock) {
    MockCache.load({
      keyid: 'id',
      key: own.id,
      mock: `./mock/hardware/${own.options.mock}.json`,
      resolve: obj2 => hardware.addLocal(obj2)
    });
  }

  return {
    obj,
    propertiesSchema,
    callablesSchema,
    options: own.options,
    operator: session.selector('operator', state),
    runningScan: scans.selector('runningScanid', state)
  };
};

const mapDispatchToProps = (dispatch, own) => {
  return {};
};

export default withNamespace({ hardware: hardwarep, app })(
  connect(mapStateToProps, mapDispatchToProps)(HardwareObject)
);
