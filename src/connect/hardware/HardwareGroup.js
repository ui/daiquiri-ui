import { connect } from 'react-redux';
import { map, find } from 'lodash';

import HardwareGroup from 'components/hardware/HardwareGroup';
import { withNamespace } from 'providers/namespace';
import hardware from 'providers/hardware';

const mapStateToProps = (state, own) => {
  const { groups } = own.providers.hardware;
  const group = groups.selector('results', state)[own.groupid];
  if (group) {
    return {
      ...group,
      objects: [
        ...map(group.objects, id => {
          const obj = find(own.objects, { id });
          return { id, ...obj };
        })
      ]
    };
  }

  return {};
};

const mapDispatchToProps = (dispatch, own) => {
  return {};
};

export default withNamespace({ hardware })(
  connect(mapStateToProps, mapDispatchToProps)(HardwareGroup)
);
