import { connect } from 'react-redux';

import { withNamespace } from 'providers/namespace';

import session from 'providers/session';
import metadata from 'providers/metadata';

import Sessions from 'components/Sessions';

const mapStateToProps = (state, own) => ({
  user: own.providers.metadata.user.selector('results', state),
  sessions: session.selector('sessions', state),
  operator: session.selector('operator', state),
  sessionid: session.selector('sessionid', state),
  mirrorid: session.selector('mirrorid', state),
  controlRequest: session.selector('controlRequest', state)
});

const mapDispatchToProps = () => ({
  actions: {
    fetchSessions: () => session.dispatch('FETCH_SESSIONS'),
    requestControl: () => session.dispatch('REQUEST_CONTROL'),
    yieldControl: () => session.dispatch('YIELD_CONTROL'),
    respondControlRequest: payload =>
      session.dispatch('RESPOND_CONTROL_REQUEST', payload),
    requestMirror: payload => session.dispatch('REQUEST_MIRROR', payload)
  }
});

export default withNamespace({ metadata })(
  connect(mapStateToProps, mapDispatchToProps)(Sessions)
);
