import { connect } from 'react-redux';
import { withNamespace } from 'providers/namespace';

import session from 'providers/session';
import queuep from 'providers/queue';

import Queue from 'components/Queue';

const mapStateToProps = (state, own) => {
  const { queue } = own.providers.queue;
  return {
    queue: queue.selector('results', state),
    operator: session.selector('operator', state),
    selectedItem: queuep.selector('selectedItem', state),
    params: queue.selector('params', state),
    fetched: queue.selector('fetched', state),
    running: queuep.selector('running', state),
    ready: queuep.selector('ready', state),
    actorLog: queuep.selector('actorLog', state)
  };
};

const mapDispatchToProps = (dispatch, own) => {
  const { queue } = own.providers.queue;
  return {
    actions: {
      fetchQueue: () => queue.fetch(),
      setParams: payload => queue.setParams(payload, true),
      setQueueStatus: payload => queuep.dispatch('SET_QUEUE_STATUS', payload),
      updateQueue: payload => queuep.dispatch('UPDATE_QUEUE', payload),
      removeItem: payload => queuep.dispatch('REMOVE_ITEM', payload),
      moveItem: payload => queuep.dispatch('MOVE_ITEM', payload),
      addPause: () => queuep.dispatch('ADD_PAUSE'),
      addSleep: payload => queuep.dispatch('ADD_SLEEP', payload),
      clear: () => queuep.dispatch('CLEAR_QUEUE'),
      kill: () => queuep.dispatch('KILL_QUEUE'),
      selectItem: payload => queuep.dispatch('SELECT_ITEM', payload)
    }
  };
};

export default withNamespace({ queue: queuep })(
  connect(mapStateToProps, mapDispatchToProps)(Queue)
);
