import { connect } from 'react-redux';

import ButtonTriggerModalStore from 'components/layout/ButtonTriggerModalStore';
import app from 'providers/app';

const mapStateToProps = (state: any, own: any) => ({
  show: app.selector('modal', state)[own.id] || false,
});

const mapDispatchToProps = (dispatch: any, own: any) => ({
  actions: {
    toggleModal: () => app.dispatch('TOGGLE_MODAL', own.id),
  },
});

export default connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true,
})(ButtonTriggerModalStore);
