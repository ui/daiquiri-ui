import { connect } from 'react-redux';
import { each } from 'lodash';

import app from 'providers/app';
import Sidebar, {
  SidebarContainer,
  SidebarTrigger,
} from 'components/layout/Sidebar';

const mapDispatchToProps = () => ({
  actions: {
    toggleSidebar: (id: string) => {
      app.dispatch('TOGGLE_SIDEBAR', id);
    },
  },
});

const mapStateToProps = (state: any, ownProps: any) => ({
  enabled: app.selector('sidebar', state)[ownProps.id],
});

const ConnectedSidebarTrigger = connect(
  mapStateToProps,
  mapDispatchToProps
)(SidebarTrigger);

const ConnectedSidebarContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SidebarContainer);

const mapStateToPropsSidebar = (state: any) => {
  let enabledId: string | null = null;
  each(app.selector('sidebar', state), (v, k) => {
    if (v) enabledId = k;
  });
  return { enabledId };
};

const ConnectedSidebar = connect(
  mapStateToPropsSidebar,
  mapDispatchToProps
)(Sidebar);

export default Object.assign(ConnectedSidebar, {
  Container: ConnectedSidebarContainer,
  Header: Sidebar.Header,
  Content: Sidebar.Content,
  Inner: Sidebar.Inner,
  Trigger: ConnectedSidebarTrigger,
  Cover: Sidebar.Cover,
});
