import { connect } from 'react-redux';
import { values } from 'lodash';

import { withNamespace } from 'providers/namespace';
import metadata from 'providers/metadata';
import app from 'providers/app';
import session from 'providers/session';
import OffCanvas, {
  OffCanvasTrigger,
  OffCanvasHeader,
} from 'components/layout/OffCanvas';
import { getUnread } from 'helpers/chat';

const mapStateToProps = (state: any, own: any) => {
  return {
    unread: getUnread({
      messages: values(own.providers.app.chat.selector('results', state)),
      chatCursor: own.providers.metadata.cache.selector('results', state).cache
        .chatCursor,
      sessionid: session.selector('sessionid', state),
    }),
  };
};

const mapDispatchToProps = () => ({
  actions: {
    toggleOffCanvas: (payload: any) => {
      app.dispatch('TOGGLE_OFFCANVAS', payload);
    },
  },
});

const ConnectedOffCanvasHeader = connect(
  null,
  mapDispatchToProps
)(OffCanvasHeader);

const ConnectedOffCanvasTrigger = withNamespace({ app, metadata })(
  connect(mapStateToProps, mapDispatchToProps)(OffCanvasTrigger)
);

const mapStateToProps2 = (state: any, ownProps: any) => ({
  enabled: app.selector('offcanvas', state)[ownProps.id],
});

const ConnectedOffCanvas = connect(
  mapStateToProps2,
  mapDispatchToProps
)(OffCanvas);

/* @ts-expect-error */
ConnectedOffCanvas.Header = ConnectedOffCanvasHeader;
/* @ts-expect-error */
ConnectedOffCanvas.Trigger = ConnectedOffCanvasTrigger;

export default ConnectedOffCanvas as typeof ConnectedOffCanvas & {
  Header: typeof ConnectedOffCanvasHeader;
  Trigger: typeof ConnectedOffCanvasTrigger;
};
