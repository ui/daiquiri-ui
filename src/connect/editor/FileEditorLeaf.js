import { connect } from 'react-redux';

import { withNamespace } from 'providers/namespace';
import editor from 'providers/editor';

import FileEditorLeaf from 'components/editor/FileEditorLeaf';

const mapStateToProps = (state, own) => ({
  leafState: editor.selector('leafState', state)[own.path]
});

const mapDispatchToProps = (dispatch, own) => ({
  actions: {
    toggleLeafState: () => editor.dispatch('TOGGLE_LEAF', own.path)
  }
});

export default withNamespace({ editor })(
  connect(mapStateToProps, mapDispatchToProps)(FileEditorLeaf)
);
