import { connect } from 'react-redux';
import { withNamespace } from 'providers/namespace';
import editor from 'providers/editor';
import FileEditorTree from 'components/editor/FileEditorTree';

function mapStateToProps(state: any, own: any) {
  const { providers } = own;
  return {
    directories: providers.editor.directories.selector('results', state),
    error: providers.editor.directories.selector('error', state),
  };
}

function mapDispatchToProps(dispatch: any, own: any) {
  return {
    actions: {
      fetchDirectories: () => own.providers.editor.directories.fetch(),
      fetchFile: (payload: any) =>
        own.providers.editor.files.fetchSingle(payload),
      selectFile: (payload: any) => editor.dispatch('SELECT_FILE', payload),
    },
  };
}

export default withNamespace({ editor })(
  connect(mapStateToProps, mapDispatchToProps)(FileEditorTree)
);
