import { connect } from 'react-redux';
import { withNamespace } from 'providers/namespace';
import editor from 'providers/editor';
import FileEditor from 'components/editor/FileEditor';

function mapStateToProps(state: any, own: any) {
  const { providers } = own;
  return {
    selectedFile: editor.selector('selectedFile', state),
    files: providers.editor.files.selector('results', state),
    error: providers.editor.files.selector('error', state),
  };
}

function mapDispatchToProps(dispatch: any, own: any) {
  return {
    actions: {
      selectFile: (payload: any) => editor.dispatch('SELECT_FILE', payload),
      updateFile: (payload: any) => own.providers.editor.files.update(payload),
      closeFile: (payload: any) =>
        own.providers.editor.files.deleteLocal(payload),
    },
  };
}

export default withNamespace({ editor })(
  connect(mapStateToProps, mapDispatchToProps)(FileEditor)
);
