import { connect } from 'react-redux';

import TwoDView from 'components/2dview/TwoDView';
import twod from 'providers/2dview';

function mapStateToProps(state: any) {
  return {
    messages: twod.selector('messages', state),
  };
}

function mapDispatchToProps(dispatch: any) {
  return {
    actions: {},
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TwoDView);
