import { connect } from 'react-redux';

import app from 'providers/app';
import twod from 'providers/2dview';
import metadata from 'providers/metadata';
import session from 'providers/session';

import { withNamespace } from 'providers/namespace';
import ObjectView from 'components/2dview/ObjectView';

const mapStateToProps = (state: any, own: any) => {
  const {
    subsamples,
    xrf_composites: xrfComposites,
    xrf_map_histograms: xrfMapHistograms,
  } = own.providers.metadata;
  return {
    object: subsamples.selector('results', state)[
      metadata.selector('currentSubSample', state)
    ],
    subsamples: subsamples.selector('results', state),
    selectedSubSamples: metadata.selector('selectedSubSamples', state),
    maps: own.providers.metadata.xrf_maps.selector('results', state),
    composites: xrfComposites.selector('results', state),
    mapSelection: twod.selector('mapSelection', state),
    datacollections: own.providers.metadata.datacollections.selector(
      'ordered',
      state
    ),
    dcnamespace: own.providers.metadata.datacollections.getNamespace(),
    config: app.selector('config', state, 'imageviewer'),
    operator: session.selector('operator', state),
    compositeAdding: xrfComposites.selector('adding', state),
    compositeError: xrfComposites.selector('addingError', state),
    mapHistograms: xrfMapHistograms.selector('results', state),
    mapSettings: own.providers.twod.maps.selector('results', state),
  };
};

const mapDispatchToProps = (dispatch: any, own: any) => {
  const {
    xrf_composites: xrfComposites,
    xrf_maps: xrfMaps,
    xrf_map_histograms: xrfMapHistograms,
    datacollections,
  } = own.providers.metadata;
  return {
    actions: {
      fetchMapSettings: () => own.providers.twod.maps.fetch(),
      updateMapSettings: (payload: any) =>
        own.providers.twod.maps.update(payload),
      fetchMapHistogram: (payload: any) => xrfMapHistograms.fetch(payload),
      updateMap: (payload: any) => xrfMaps.update(payload),
      selectMap: (payload: any) => twod.dispatch('SELECT_MAP', payload),
      unselectMap: (payload: any) => twod.dispatch('UNSELECT_MAP', payload),
      resetMapSelection: () => twod.dispatch('RESET_MAP_SELECTION'),
      deleteMap: (payload: any) => xrfMaps.delete(payload),
      addCompositeMap: (payload: any) => xrfComposites.add(payload),
      clearErrorCompositeMap: (payload: any) =>
        xrfComposites.clearError(payload),
      updateCompositeMap: (payload: any) => xrfComposites.update(payload),
      deleteCompositeMap: (payload: any) => xrfComposites.delete(payload),
      fetchAssociated: () => {
        const ps: Promise<any>[] = [];

        ps.push(
          xrfComposites.fetch(),
          xrfMaps.fetch(),
          datacollections.fetch()
        );

        return Promise.all(ps);
      },
    },
  };
};

export default withNamespace({ metadata, twod })(
  connect(mapStateToProps, mapDispatchToProps)(ObjectView)
);
