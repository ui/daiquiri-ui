import { connect } from 'react-redux';
import { withNamespace } from 'providers/namespace';

import ExtraMetadata from 'components/2dview/ExtraMetadata';
import app from 'providers/app';
import metadata from 'providers/metadata';

const mapStateToProps = (state, own) => ({
  availableSampleTags: own.providers.metadata.sampletags.selector(
    'results',
    state
  ),
  sampleSchema: own.providers.app.schema.selector('results', state).SampleSchema
});

const mapDispatchToProps = (dispatch, own) => ({
  actions: {
    fetchSampleTags: payload => own.providers.metadata.sampletags.fetch(payload)
  }
});

export default withNamespace({ metadata, app })(
  connect(mapStateToProps, mapDispatchToProps)(ExtraMetadata)
);
