import { connect } from 'react-redux';

import ROIBrowser from 'components/2dview/ROIBrowser';
import { withNamespace } from 'providers/namespace';

import session from 'providers/session';
import metadata from 'providers/metadata';

const mapStateToProps = (state, own) => {
  const { xrf_map_rois: xrfMapRois, samples } = own.providers.metadata;
  return {
    samples: samples.selector('results', state),
    rois: xrfMapRois.selector('results', state),
    operator: session.selector('operator', state)
  };
};

const mapDispatchToProps = (dispatch, own) => {
  const { xrf_map_rois: xrfMapRois } = own.providers.metadata;
  return {
    actions: {
      fetchMapROIs: payload =>
        xrfMapRois.setParams(
          {
            sampleid: payload
          },
          true
        )
    }
  };
};

export default withNamespace({ metadata })(
  connect(mapStateToProps, mapDispatchToProps)(ROIBrowser)
);
