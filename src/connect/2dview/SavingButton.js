import { connect } from 'react-redux';

import { withNamespace } from 'providers/namespace';
import SavingButton from 'components/2dview/SavingButton';

import app from 'providers/app';

const mapStateToProps = (state, own) => ({
  saving: own.providers.app.saving.selector('results', state)
});

const mapDispatchToProps = (dispatch, own) => ({
  actions: {
    fetchSaving: () => own.providers.app.saving.fetch()
  }
});

export default withNamespace({ app })(
  connect(mapStateToProps, mapDispatchToProps)(SavingButton)
);
