import { connect } from 'react-redux';

import ValueModal from 'components/2dview/ValueModal';
import { withNamespace } from 'providers/namespace';
import twod from 'providers/2dview';
import metadata from 'providers/metadata';

const mapStateToProps = (state: any, own: any) => {
  const { xrf_maps: xrfMaps, xrf_map_value: xrfMapValue } =
    own.providers.metadata;

  return {
    pointer: twod.selector('mapHover', state),
    maps: xrfMaps.selector('results', state),
    selection: twod.selector('mapSelection', state),
    value: xrfMapValue.selector('results', state),
    valueError: xrfMapValue.selector('error', state),
    valueFetched: xrfMapValue.selector('fetched', state),
  };
};

const mapDispatchToProps = (dispatch: any, own: any) => ({
  actions: {
    fetchValue: (payload: any) =>
      own.providers.metadata.xrf_map_value.fetch(payload),
    setMapHover: (payload: any) => twod.dispatch('SET_MAP_HOVER', payload),
  },
});

export default withNamespace({ metadata })(
  connect(mapStateToProps, mapDispatchToProps)(ValueModal)
);
