import { connect } from 'react-redux';

import twod from 'providers/2dview';

import RegenerateMapButton from 'components/2dview/RegenerateMapsButton';

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({
  actions: {
    regenerateMaps: payload => twod.dispatch('REGENERATE_MAPS', payload)
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegenerateMapButton);
