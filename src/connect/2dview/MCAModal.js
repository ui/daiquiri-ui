import { connect } from 'react-redux';

import MCAModal from 'components/2dview/MCAModal';
import { withNamespace } from 'providers/namespace';
import scans from 'providers/scans';
import twod from 'providers/2dview';
import metadata from 'providers/metadata';

const mapStateToProps = (state, own) => {
  const { xrf_map_rois: xrfMapRois } = own.providers.metadata;
  const { spectra } = own.providers.scans;
  return {
    pointer: twod.selector('mapPointer', state),
    spectra: spectra.selector('results', state),
    spectraError: spectra.selector('error', state),
    spectraFetched: spectra.selector('fetched', state),
    spectraFetching: spectra.selector('fetching', state),
    rois: xrfMapRois.selector('results', state)
  };
};

const mapDispatchToProps = (dispatch, own) => ({
  actions: {
    fetchScanSpectra: payload => own.providers.scans.spectra.fetch(payload),
    setMapPointer: payload => twod.dispatch('SET_MAP_POINTER', payload)
  }
});

export default withNamespace({ scans, metadata })(
  connect(mapStateToProps, mapDispatchToProps)(MCAModal)
);
