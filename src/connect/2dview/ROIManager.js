import { connect } from 'react-redux';

import ROIManager from 'components/2dview/ROIManager';
import { withNamespace } from 'providers/namespace';

import app from 'providers/app';
import session from 'providers/session';
import metadata from 'providers/metadata';

const mapStateToProps = (state, own) => {
  const { xrf_map_rois: xrfMapRois } = own.providers.metadata;
  return {
    rois: xrfMapRois.selector('results', state),
    modal: app.selector('modal', state),
    operator: session.selector('operator', state),
    currentSample: metadata.selector('currentSample', state),
    config: app.selector('config', state, 'scans')
  };
};

const mapDispatchToProps = (dispatch, own) => {
  const { xrf_map_rois: xrfMapRois } = own.providers.metadata;
  return {
    actions: {
      fetchMapROIs: () => xrfMapRois.fetch(),
      addMapROI: payload => xrfMapRois.add(payload),
      deleteMapROI: payload => xrfMapRois.delete(payload),
      updateMapROI: payload => xrfMapRois.update(payload),
      toggleModal: payload => app.dispatch('TOGGLE_MODAL', payload)
    }
  };
};

export default withNamespace({ metadata })(
  connect(mapStateToProps, mapDispatchToProps)(ROIManager)
);
