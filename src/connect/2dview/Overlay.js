import { connect } from 'react-redux';

import Overlay from 'components/2dview/Overlay';
import { withNamespace } from 'providers/namespace';
import { withSampleActionPositions } from 'components/samples/registration/hoc';

import twod from 'providers/2dview';
import metadata from 'providers/metadata';
import app from 'providers/app';
import scans from 'providers/scans';
import session from 'providers/session';

const mapStateToProps = (state, own) => {
  const { samples } = own.providers.metadata;
  return {
    runningScan: scans.selector('runningScanid', state),
    samples: samples.selector('results', state),
    images: own.providers.metadata.images.selector('results', state),
    objects: own.providers.metadata.subsamples.selector('results', state),
    maps: own.providers.metadata.xrf_maps.selector('results', state),
    composites: own.providers.metadata.xrf_composites.selector(
      'results',
      state
    ),
    sources: own.providers.twod.sources.selector('results', state),
    currentAction: twod.selector('currentAction', state),
    currentSampleId: metadata.selector('currentSample', state),
    currentSample: samples.selector('results', state)[
      metadata.selector('currentSample', state)
    ],
    selectedObjects: metadata.selector('selectedSubSamples', state),
    clampOrigin: twod.selector('clampOrigin', state),
    fillOrigin: twod.selector('fillOrigin', state),
    sourceUrl: twod.selector('sourceUrl', state),
    centreOnClick: twod.selector('centreOnClick', state),
    drawQuantize: twod.selector('drawQuantize', state),
    drawSnap: twod.selector('drawSnap', state),
    multiROI: twod.selector('multiROI', state),
    clampLOI: twod.selector('clampLOI', state),
    cursor: twod.selector('cursor', state),
    config: app.selector('config', state, 'imageviewer'),
    zoom: twod.selector('zoom', state),
    viewport: twod.selector('viewport', state),
    mirrorid: session.selector('mirrorid', state),
    saveCanvas: twod.selector('saveCanvas', state),
    sourceHover: twod.selector('sourceHover', state)
  };
};

const mapDispatchToProps = (dispatch, own) => {
  const { subsamples } = own.providers.metadata;
  return {
    actions: {
      selectAction: payload => twod.dispatch('SELECT_ACTION', payload),
      addObject: payload => subsamples.add(payload),
      updateObject: payload => subsamples.update(payload),
      addSelection: payload => metadata.dispatch('SELECT_SUB_SAMPLE', payload),
      removeSelection: payload =>
        metadata.dispatch('UNSELECT_SUB_SAMPLE', payload),
      resetSelection: () => metadata.dispatch('RESET_SUB_SAMPLE_SELECTION'),
      fetchSources: () => own.providers.twod.sources.fetch(),
      selectSample: payload => metadata.dispatch('SELECT_SAMPLE', payload),
      fetchComponents: () => own.providers.metadata.components.fetch(),
      fetchSamples: () => own.providers.metadata.samples.fetch(),
      fetchSample: (payload, state) => {
        const ps = [];

        ps.push(own.providers.metadata.xrf_maps.fetch());
        ps.push(own.providers.metadata.xrf_composites.fetch());
        ps.push(own.providers.metadata.xrf_map_rois.fetch());
        ps.push(own.providers.metadata.images.fetch());
        ps.push(subsamples.fetch());

        return Promise.all(ps);
      },
      moveToCoords: payload => twod.dispatch('MOVE_TO_COORDS', payload),
      updateMarking: payload => own.providers.twod.markings.update(payload),
      setZoom: payload => twod.dispatch('SET_ZOOM', payload),
      setViewport: payload => twod.dispatch('SET_VIEWPORT', payload),
      mosaic: payload => twod.dispatch('MOSAIC', payload),
      setMapPointer: payload => twod.dispatch('SET_MAP_POINTER', payload),
      setMapHover: payload => twod.dispatch('SET_MAP_HOVER', payload),
      setSourceHover: payload => twod.dispatch('SET_SOURCE_HOVER', payload),
      setMessage: payload => twod.dispatch('SET_MESSAGE', payload),
      clearSaveCanvas: () => twod.dispatch('CLEAR_SAVE_CANVAS'),
      saveCanvasRemote: payload => twod.dispatch('SAVE_CANVAS_REMOTE', payload),
      addToast: payload => app.dispatch('ADD_TOAST', payload)
    }
  };
};

export default withSampleActionPositions(
  withNamespace({ metadata, twod })(
    connect(mapStateToProps, mapDispatchToProps)(Overlay)
  )
);
