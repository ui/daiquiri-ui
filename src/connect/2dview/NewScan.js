import { connect } from 'react-redux';

import NewScan from 'components/2dview/NewScan';
import app from 'providers/app';

const mapStateToProps = state => ({
  schema: app.selector('form', state).newscanSchema || '',
  type: app.selector('form', state).newscanSchemaType || ''
});

const mapDispatchToProps = () => ({
  actions: {
    setSchema: payload =>
      app.dispatch('SET_FORM', { key: 'newscanSchema', value: payload }),
    setSchemaType: payload =>
      app.dispatch('SET_FORM', { key: 'newscanSchemaType', value: payload })
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(NewScan);
