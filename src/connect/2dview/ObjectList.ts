import { connect } from 'react-redux';

import { withNamespace } from 'providers/namespace';

import twod from 'providers/2dview';
import metadata from 'providers/metadata';
import session from 'providers/session';
import scans from 'providers/scans';
import app from 'providers/app';

import ObjectList from 'components/2dview/ObjectList';

const mapStateToProps = (state: any, own: any) => {
  const { subsamples, samples } = own.providers.metadata;
  const { schema } = own.providers.app;
  return {
    objects: subsamples.selector('results', state),
    fetchingObjects: subsamples.selector('fetching', state),
    fetchedObjects: subsamples.selector('fetched', state),
    selectedObjects: metadata.selector('selectedSubSamples', state),
    selectedObject: metadata.selector('currentSubSample', state),
    operator: session.selector('operator', state),
    runningScan: scans.selector('runningScanid', state),
    currentSample: metadata.selector('currentSample', state),
    samples: samples.selector('results', state),
    subSampleSchema: schema.selector('results', state).SubSampleSchema,
    pendingExportSubsamples: twod.selector('pendingExportSubsamples', state),
    config: app.selector('config', state, 'imageviewer'),
  };
};

const mapDispatchToProps = (dispatch: any, own: any) => {
  const { subsamples } = own.providers.metadata;
  return {
    actions: {
      fetchObjects: () => subsamples.fetch(),
      removeObject: (payload: any) => subsamples.delete(payload),
      updateObject: (payload: any) => subsamples.update(payload),
      addSelection: (payload: any) =>
        metadata.dispatch('SELECT_SUB_SAMPLE', payload),
      removeSelection: (payload: any) =>
        metadata.dispatch('UNSELECT_SUB_SAMPLE', payload),
      resetSelection: () => metadata.dispatch('RESET_SUB_SAMPLE_SELECTION'),
      moveToSubSample: (payload: any) =>
        twod.dispatch('MOVE_TO_SUBSAMPLE', payload),
      exportSubSamples: (payload: any) =>
        twod.dispatch('EXPORT_SUBSAMPLES', payload),
      addToast: (payload: any) => app.dispatch('ADD_TOAST', payload),
    },
  };
};

export default withNamespace({ metadata, app })(
  connect(mapStateToProps, mapDispatchToProps)(ObjectList)
);
