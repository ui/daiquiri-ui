import { connect } from 'react-redux';

import Controls from 'components/2dview/Controls';
import { withNamespace } from 'providers/namespace';
import { withSampleActionPositions } from 'components/samples/registration/hoc';

import twod from 'providers/2dview';
import session from 'providers/session';
import metadata from 'providers/metadata';
import scans from 'providers/scans';
import app from 'providers/app';

const mapStateToProps = (state, own) => ({
  currentAction: twod.selector('currentAction', state),
  clampOrigin: twod.selector('clampOrigin', state),
  fillOrigin: twod.selector('fillOrigin', state),
  sourceUrl: twod.selector('sourceUrl', state),
  disableOrigin: twod.selector('disableOrigin', state),
  centreOnClick: twod.selector('centreOnClick', state),
  operator: session.selector('operator', state),
  drawQuantize: twod.selector('drawQuantize', state),
  drawSnap: twod.selector('drawSnap', state),
  multiROI: twod.selector('multiROI', state),
  clampLOI: twod.selector('clampLOI', state),
  cursor: twod.selector('cursor', state),
  zoom: twod.selector('zoom', state),
  currentSample: metadata.selector('currentSample', state),
  runningScan: scans.selector('runningScanid', state),
  user: own.providers.metadata.user.selector('results', state),
  pendingSaveImage: twod.selector('pendingSaveImage', state),
  pendingSaveCanvas: twod.selector('saveCanvas', state),
  config: app.selector('config', state, 'imageviewer'),
  originSettings: own.providers.twod.origin.selector('results', state),
  sources: own.providers.twod.sources.selector('results', state)
});

const mapDispatchToProps = (state, own) => ({
  actions: {
    selectAction: payload => twod.dispatch('SELECT_ACTION', payload),
    toggleClampOrigin: (payload, fillOrigin) => {
      twod.dispatch('CLAMP_ORIGIN', payload);
      if (fillOrigin) twod.dispatch('FILL_ORIGIN', false);
    },
    toggleFillOrigin: (payload, clampOrigin) => {
      if (!clampOrigin && payload) twod.dispatch('CLAMP_ORIGIN', true);
      twod.dispatch('FILL_ORIGIN', payload);
    },
    setSourceUrl: payload => {
      twod.dispatch('SOURCE_URL', payload);
    },
    toggleDisableOrigin: payload => {
      return twod.dispatch('UPDATE_ORIGIN', { hide: payload });
    },
    toggleCentreOnClick: payload => twod.dispatch('CENTRE_ON_CLICK', payload),
    changeDrawQuantize: payload =>
      twod.dispatch('CHANGE_DRAW_QUANTIZE', payload),
    toggleDrawSnap: payload => twod.dispatch('TOGGLE_DRAW_SNAP', payload),
    toggleMultiROI: payload => twod.dispatch('TOGGLE_MULTI_ROI', payload),
    toggleClampLOI: payload => twod.dispatch('TOGGLE_CLAMP_LOI', payload),
    toggleCursor: payload => twod.dispatch('TOGGLE_CURSOR', payload),
    saveImage: payload => twod.dispatch('SAVE_IMAGE', payload),
    saveCanvas: payload => twod.dispatch('SAVE_CANVAS', payload),
    addToast: payload => app.dispatch('ADD_TOAST', payload),
    autoFocusImage: payload => twod.dispatch('AUTOFOCUS_IMAGE', payload),
    setSourceHover: payload => twod.dispatch('SET_SOURCE_HOVER', payload),
    fetchOriginSettings: payload => own.providers.twod.origin.fetch(payload),
    updateOriginSettings: payload => own.providers.twod.origin.update(payload)
  }
});

export default withSampleActionPositions(
  withNamespace({ metadata, twod })(
    connect(mapStateToProps, mapDispatchToProps)(Controls)
  )
);
