import { connect } from 'react-redux';
import { withNamespace } from 'providers/namespace';
import MainSidebar from 'components/main/MainSidebar';
import queuep from 'providers/queue';
import metadata from 'providers/metadata';

const mapStateToProps = (state, own) => {
  const { queue } = own.providers.queue;
  const stack = queue.selector('results', state);
  return {
    currentSampleId: metadata.selector('currentSample', state),
    queueSize: stack.all?.length ?? 0,
    queueRunning: queuep.selector('running', state),
    queueReady: queuep.selector('ready', state)
  };
};

const mapDispatchToProps = (dispatch, own) => ({});

export default withNamespace({ metadata, queue: queuep })(
  connect(mapStateToProps, mapDispatchToProps)(MainSidebar)
);
