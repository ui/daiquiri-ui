import { connect } from 'react-redux';

import NotificationsPanel from 'components/main/NotificationsPanel';
import app from 'providers/app';

const mapStateToProps = state => ({
  selectedTab: app.selector('tab', state).notification
});

const mapDispatchToProps = () => ({
  actions: {
    selectTab: payload => app.dispatch('SELECT_TAB', payload)
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(NotificationsPanel);
