import { connect } from 'react-redux';

import Main from 'components/main/Main';
import session from 'providers/session';

const mapStateToProps = state => ({
  session: session.selector('currentBLSession', state)
});

export default connect(mapStateToProps)(Main);
