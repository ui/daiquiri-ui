import { connect } from 'react-redux';

import LayoutSideBar from 'components/header/LayoutSideBar';

import { withNamespace } from 'providers/namespace';
import app from 'providers/app';
import metadata from 'providers/metadata';

const mapStateToProps = (state, own) => ({
  layouts: own.providers.app.layouts.selector('results', state),
  current: app.selector('layoutCurrent', state),
  fetching: own.providers.app.layouts.selector('fetching', state),
  user: own.providers.metadata.user.selector('results', state)
});

const mapDispatchToProps = (dispatch, own) => ({
  actions: {
    changeLayout: payload => {
      app.dispatch('SWITCH_LAYOUT', payload);
    },
    fetchLayouts: () => own.providers.app.layouts.fetch()
  }
});

export default withNamespace({ app, metadata })(
  connect(mapStateToProps, mapDispatchToProps)(LayoutSideBar)
);
