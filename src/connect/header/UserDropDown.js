import { connect } from 'react-redux';

import UserDropDown from 'components/header/UserDropDown';

import { withNamespace } from 'providers/namespace';
import session from 'providers/session';
import metadata from 'providers/metadata';
import app from 'providers/app';

const mapStateToProps = (state, own) => {
  const { blsessions } = own.providers.metadata;
  return {
    user: own.providers.metadata.user.selector('results', state),
    session: blsessions.selector('results', state)[
      session.selector('currentBLSession', state)
    ],
    show: app.selector('modal', state).userdropdown || false
  };
};

const mapDispatchToProps = () => ({
  actions: {
    logout: () => session.dispatch('LOGOUT'),
    toggleModal: payload => app.dispatch('TOGGLE_MODAL', payload)
  }
});

export default withNamespace({ metadata })(
  connect(mapStateToProps, mapDispatchToProps)(UserDropDown)
);
