import { connect } from 'react-redux';

import AboutModal from 'components/header/AboutModal';
import { withNamespace } from 'providers/namespace';
import RestService from 'services/RestService';
import app from 'providers/app';
import metadata from 'providers/metadata';

const mapStateToProps = (state, own) => ({
  version: own.providers.app.version.selector('results', state),
  fetching: own.providers.app.version.selector('fetching', state),
  fetched: own.providers.app.version.selector('fetched', state),
  uiVersion: app.selector('uiVersion', state),
  user: own.providers.metadata.user.selector('results', state)
});

const mapDispatchToProps = (dispatch, own) => ({
  actions: {
    fetchVersion: () => own.providers.app.version.fetch(),
    fetchUiVersion: () => app.dispatch('FETCH_UI_VERSION'),
    toggleModal: payload => app.dispatch('TOGGLE_MODAL', payload),
    addAlert: payload => app.dispatch('ADD_ALERT', payload),
    reloadServer: payload => RestService.post('/components/reload')
  }
});

export default withNamespace({ app, metadata })(
  connect(mapStateToProps, mapDispatchToProps)(AboutModal)
);
