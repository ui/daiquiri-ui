import { connect } from 'react-redux';

import AlertsDropDown from 'components/header/AlertsDropDown';

import { withNamespace } from 'providers/namespace';
import app from 'providers/app';
import scans from 'providers/scans';

const mapStateToProps = state => {
  return {
    show: app.selector('modal', state).alertsdropdown || false,
    runningScan: scans.selector('runningScan', state),
    networkError: app.selector('networkError', state),
    alerts: app.selector('alerts', state)
  };
};

const mapDispatchToProps = () => ({
  actions: {
    showModal: payload => app.dispatch('SHOW_MODAL', payload),
    hideModal: payload => app.dispatch('HIDE_MODAL', payload),
    toggleModal: payload => app.dispatch('TOGGLE_MODAL', payload)
  }
});

export default withNamespace({})(
  connect(mapStateToProps, mapDispatchToProps)(AlertsDropDown)
);
