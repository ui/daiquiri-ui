import { connect } from 'react-redux';

import { withNamespace } from 'providers/namespace';

import Header from 'components/header/Header';
import session from 'providers/session';
import app from 'providers/app';

const mapStateToProps = (state, ownProps) => {
  const layouts = ownProps.providers.app.layouts.selector('results', state);

  return {
    isLoggedIn: session.selector('auth', state),
    session: session.selector('currentBLSession', state),
    isAboutModalVisible: app.selector('modal', state).about || false,
    beamline: app.selector('config', state, 'beamline'),
    headerColor: app.selector('config', state, 'header_color'),
    headerTitle: app.selector('config', state, 'header_title'),
    layout: layouts && layouts[app.selector('layoutCurrent', state)]
  };
};

const mapDispatchToProps = () => ({
  actions: {
    toggleModal: payload => app.dispatch('TOGGLE_MODAL', payload)
  }
});

export default withNamespace({ app })(
  connect(mapStateToProps, mapDispatchToProps)(Header)
);
