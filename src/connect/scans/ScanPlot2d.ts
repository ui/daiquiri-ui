import { connect } from 'react-redux';

import ScanPlot2d from 'components/scans/ScanPlot2d';
import { withNamespace } from 'providers/namespace';
import scans from 'providers/scans';

function mapStateToProps(state: any, own: any) {
  return {
    scans: own.providers.scans.list.selector('results', state),
    data: own.providers.scans.data.selector('results', state),
    spectra: own.providers.scans.spectra.selector('results', state),
    currentPoint: scans.selector('currentPoint', state),
    currentImageNode: scans.selector('currentImageNode', state),
    selectedScan: scans.selector('selectedScan', state),
  };
}

function mapDispatchToProps(dispatch: any, own: any) {
  return {
    actions: {
      selectPoint: (payload: any) =>
        scans.dispatch('SCAN_CURRENT_POINT', Number.parseInt(payload, 10)),
      fetchScanSpectra: (payload: any) =>
        own.providers.scans.spectra.fetch(payload),
    },
  };
}

export default withNamespace({ scans })(
  connect(mapStateToProps, mapDispatchToProps)(ScanPlot2d)
);
