import { connect } from 'react-redux';
import { withNamespace } from 'providers/namespace';
import scans from 'providers/scans';

import ScanTable from 'components/scans/ScanTable';

const mapStateToProps = (state: any, own: any) => {
  const { list } = own.providers.scans;
  return {
    selected: scans.selector('selectedScan', state),
    selectedItems: scans.selector('selectedScans', state),
    scans: list.selector('ordered', state),
    params: list.selector('params', state),
    per_page: list.selector('per_page', state),
    page: list.selector('page', state),
    total: list.selector('total', state),
    fetching: list.selector('fetching', state),
  };
};

const mapDispatchToProps = (dispatch: any, own: any) => {
  const { list } = own.providers.scans;
  return {
    actions: {
      fetchData: (payload: any) => {
        own.providers.scans.data.setPage(1);
        return own.providers.scans.data.setParams({ scanid: payload }, true);
      },
      fetch: () => list.fetch(),
      setParams: (payload: any) => list.setParams(payload, true),
      setPage: (payload: any) => list.setPage(payload),
      setPageSize: (payload: any) => list.setPageSize(payload),
      addSelection: (payload: any) => scans.dispatch('SELECT_SCAN', payload),
      removeSelection: (payload: any) =>
        scans.dispatch('UNSELECT_SCAN', payload),
      resetSelection: () => scans.dispatch('RESET_SCAN_SELECTION'),
    },
  };
};

export default withNamespace({ scans })(
  connect(mapStateToProps, mapDispatchToProps)(ScanTable)
);
