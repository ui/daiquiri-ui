import ScanPlot0dValue from 'components/scans/ScanPlot0dValue';
import { withNamespace } from 'providers/namespace';
import scans from 'providers/scans';
import { connect } from 'react-redux';

import mapStateToPropsWithCache from '../ConnectUtils';

const mapStateAndCacheToProps = (state, own, cache) => {
  const { data, list } = own.providers.scans;
  return {
    data: data.selector('results', state),
    scans: list.selector('results', state),
    currentPoint: scans.selector('currentPoint', state),
    selectedScan: scans.selector('selectedScan', state)
  };
};

const mapDispatchToProps = (dispatch, own) => ({
  actions: {
    setFollow: () => {
      if (own.selectedScan === undefined) {
        scans.dispatch('RESET_SCAN_SELECTION');
      }
    },
    setPageSize: payload => own.providers.scans.data.setPageSize(payload, true),
    setPage: (payload, fetch) =>
      own.providers.scans.data.setPage(payload, fetch),
    setParams: (payload, fetch) =>
      own.providers.scans.data.setParams(payload, fetch)
  }
});

export default withNamespace({ scans })(
  connect(
    mapStateToPropsWithCache(mapStateAndCacheToProps),
    mapDispatchToProps
  )(ScanPlot0dValue)
);
