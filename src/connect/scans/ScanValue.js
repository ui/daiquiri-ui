import ScanValue from 'components/scans/ScanValue';
import { withNamespace } from 'providers/namespace';
import scans from 'providers/scans';
import { connect } from 'react-redux';

import mapStateToPropsWithCache from '../ConnectUtils';

const mapStateAndCacheToProps = (state, own, cache) => {
  const { list } = own.providers.scans;
  return {
    scans: list.selector('results', state),
    selectedScan: scans.selector('selectedScan', state)
  };
};

const mapDispatchToProps = (dispatch, own) => ({
  actions: {
    setFollow: () => {
      if (own.selectedScan === undefined) {
        scans.dispatch('RESET_SCAN_SELECTION');
      }
    }
  }
});

export default withNamespace({ scans })(
  connect(
    mapStateToPropsWithCache(mapStateAndCacheToProps),
    mapDispatchToProps
  )(ScanValue)
);
