import type { ScanPlot0dActions } from 'components/scans/ScanPlot0d';
import ScanPlot0d from 'components/scans/ScanPlot0d';
import { withNamespace } from 'providers/namespace';
import scans from 'providers/scans';
import { connect } from 'react-redux';
import type { AxesParams } from 'components/scans/models';

import type { Cache } from '../ConnectUtils';
import mapStateToPropsWithCache from '../ConnectUtils';

function mapStateAndCacheToProps(state: any, own: any, cache: Cache) {
  const { data, list } = own.providers.scans;
  return {
    data: data.selector('results', state),
    scans: list.selector('results', state),
    pageSize: data.selector('per_page', state) || 25,
    fetching: data.selector('fetching', state),
    selectedSeries: scans.selector('selectedDataSeries', state),
    selectedAxes: scans.selector('selectedDataAxes', state),
    selectedScan: scans.selector('selectedScan', state),
    selectedScans: scans.selector('selectedScans', state),
  };
}

function mapDispatchToProps(dispatch: any, own: any) {
  const actions: ScanPlot0dActions = {
    selectPoint: (index: string | number) =>
      scans.dispatch(
        'SCAN_CURRENT_POINT',
        Number.parseInt(index.toString(), 10)
      ),
    setPageSize: (size: number) =>
      own.providers.scans.data.setPageSize(size, true),
    setPage: (pageNumber: number, fetch?: boolean) =>
      own.providers.scans.data.setPage(pageNumber, fetch),
    setParams: (p: { scalars: string[] }, fetch?: boolean) =>
      own.providers.scans.data.setParams(p, fetch),
    fetchScanSpectra: (payload: unknown) =>
      own.providers.scans.spectra.fetch(payload),
    selectSeries: (s: string) => scans.dispatch('SCAN_DATA_SELECT_SERIES', s),
    unselectSeries: (s: string) =>
      scans.dispatch('SCAN_DATA_UNSELECT_SERIES', s),
    resetSeries: (payload: unknown) =>
      scans.dispatch('SCAN_DATA_RESET_SERIES', payload),
    selectAxes: (axisParams: AxesParams) =>
      scans.dispatch('SCAN_DATA_SELECT_AXES', axisParams),
    unselectAxes: (payload: unknown) =>
      scans.dispatch('SCAN_DATA_UNSELECT_AXES', payload),
    resetAxes: (payload: unknown) =>
      scans.dispatch('SCAN_DATA_RESET_AXES', payload),
    setFollow: () => {
      if (own.selectedScan === undefined) {
        scans.dispatch('RESET_SCAN_SELECTION');
      }
    },
  };
  return { actions };
}

export default withNamespace({ scans })(
  connect(
    mapStateToPropsWithCache(mapStateAndCacheToProps),
    mapDispatchToProps
  )(ScanPlot0d)
);
