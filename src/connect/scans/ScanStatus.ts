import { connect } from 'react-redux';
import scans from 'providers/scans';

import ScanStatus from 'components/scans/ScanStatus';

function mapStateToProps(state: any) {
  return {
    runningScan: scans.selector('runningScan', state),
    running: scans.selector('running', state),
  };
}

function mapDispatchToProps() {
  return {
    actions: {
      fetch: () => scans.dispatch('FETCH_SCAN_STATUS'),
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ScanStatus);
