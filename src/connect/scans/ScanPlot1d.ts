import { connect } from 'react-redux';

import ScanPlot1d from 'components/scans/ScanPlot1d';
import { withNamespace } from 'providers/namespace';
import scans from 'providers/scans';
import type { Cache } from '../ConnectUtils';
import mapStateToPropsWithCache from '../ConnectUtils';

function mapStateAndCacheToProps(state: any, own: any, cache: Cache) {
  return {
    data: own.providers.scans.data.selector('results', state),
    scans: own.providers.scans.list.selector('results', state),
    spectra: own.providers.scans.spectra.selector('results', state),
    fetching: own.providers.scans.spectra.selector('fetching', state),
    currentPoint: scans.selector('currentPoint', state),
    currentImageNode: scans.selector('currentImageNode', state),
    selectedScan: scans.selector('selectedScan', state),
  };
}

function mapDispatchToProps(dispatch: any, own: any) {
  return {
    actions: {
      selectPoint: (payload: any) =>
        scans.dispatch('SCAN_CURRENT_POINT', Number.parseInt(payload, 10)),
      clearPoint: () => scans.dispatch('SCAN_CURRENT_POINT', null),
      fetchScanSpectra: (payload: any) =>
        own.providers.scans.spectra.fetch(payload),
      setPage: (payload: any) => own.providers.scans.spectra.setPage(payload),
    },
  };
}

export default withNamespace({ scans })(
  connect(
    mapStateToPropsWithCache(mapStateAndCacheToProps),
    mapDispatchToProps
  )(ScanPlot1d)
);
