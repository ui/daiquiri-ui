import { connect } from 'react-redux';

import NewParameters from 'components/parameteriser/NewParameters';
import app from 'providers/app';

const mapStateToProps = state => ({
  config: app.selector('config', state, 'parameteriser'),
  schema: app.selector('form', state).newParametersSchema || '',
  type: app.selector('form', state).newParametersSchemaType || ''
});

const mapDispatchToProps = () => ({
  actions: {
    setSchema: payload =>
      app.dispatch('SET_FORM', { key: 'newParametersSchema', value: payload }),
    setSchemaType: payload =>
      app.dispatch('SET_FORM', {
        key: 'newParametersSchemaType',
        value: payload
      })
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(NewParameters);
