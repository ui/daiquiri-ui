import { connect } from 'react-redux';
import { withNamespace } from 'providers/namespace';
import parameteriser from 'providers/parameteriser';
import ParametersList from 'components/parameteriser/ParametersList';

function mapStateToProps(state: any, own: any) {
  const { providers } = own;
  const { parameters } = providers.parameteriser;
  return {
    parameters: parameters.selector('ordered', state),
    namespace: parameters.getNamespace(),
    per_page: parameters.selector('per_page', state),
    page: parameters.selector('page', state),
    total: parameters.selector('total', state),
    fetching: parameters.selector('fetching', state),
  };
}

function mapDispatchToProps(dispatch: any, own: any) {
  const { providers } = own;
  const { parameters } = providers.parameteriser;
  return {
    actions: {
      fetch: () => parameters.fetch(),
      setParams: (payload: any) => parameters.setParams(payload, true),
      setPage: (payload: any) => parameters.setPage(payload),
      setPageSize: (payload: any) => parameters.setPageSize(payload),
    },
  };
}

export default withNamespace({ parameteriser })(
  connect(mapStateToProps, mapDispatchToProps)(ParametersList)
);
