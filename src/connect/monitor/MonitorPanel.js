import { connect } from 'react-redux';

import { withNamespace } from 'providers/namespace';
import app from 'providers/app';

import MonitorPanel from 'components/monitor/MonitorPanel';

const mapStateToProps = (state, own) => ({
  monitors: app.selector('config', state, 'hardware')?.monitor
});

const mapDispatchToProps = (dispatch, own) => ({});

export default withNamespace({})(
  connect(mapStateToProps, mapDispatchToProps)(MonitorPanel)
);
