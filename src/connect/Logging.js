import { connect } from 'react-redux';

import { withNamespace } from 'providers/namespace';

import Logging from 'components/Logging';

import app from 'providers/app';
import session from 'providers/session';
import metadata from 'providers/metadata';

const mapStateToProps = (state, own) => ({
  user: own.providers.metadata.user.selector('results', state),
  logging: app.selector('logging', state),
  fetching: app.selector('loggingFetching', state),
  empty: app.selector('loggingEmpty', state),
  current: session.selector('current', state)
});

const mapDispatchToProps = dispatch => ({
  actions: {
    fetchLogs: payload =>
      app.dispatch('FETCH_LOGS', {
        lines: 20,
        ...payload
      })
  }
});

export default withNamespace({ metadata })(
  connect(mapStateToProps, mapDispatchToProps)(Logging)
);
