import { connect } from 'react-redux';
import RestService from 'services/RestService';
import { withNamespace } from 'providers/namespace';
import app from 'providers/app';
import hardware from 'providers/hardware';
import Synoptic from 'components/Synoptic';

function mapStateToProps(state: any, own: any) {
  const { providers } = own;
  return {
    synoptic: app.selector('config', state, 'synoptic'),
    hardwareGroups: providers.hardware.groups.selector('results', state),
    hardwareObjects: providers.hardware.hardware.selector('results', state),
  };
}

function mapDispatchToProps(dispatch: any, own: any) {
  return {
    actions: {
      fetchHardware: () =>
        own.providers.hardware.hardware.fetch({ first: true }),
      fetchHardwareGroups: () =>
        own.providers.hardware.groups.fetch({ first: true }),
      fetchSVG: (payload: any) => RestService.get(`/synoptic/${payload}`),
    },
  };
}

export default withNamespace({ hardware })(
  connect(mapStateToProps, mapDispatchToProps)(Synoptic)
);
