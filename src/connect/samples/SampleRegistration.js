import { connect } from 'react-redux';

import { withNamespace } from 'providers/namespace';

import metadata from 'providers/metadata';
import session from 'providers/session';
import twod from 'providers/2dview';
import app from 'providers/app';

import SampleRegistration from 'components/samples/SampleRegistration';

const mapStateToProps = (state, own) => {
  const { samples } = own.providers.metadata;
  const { sources } = own.providers.twod;
  return {
    operator: session.selector('operator', state),
    currentSample: metadata.selector('currentSample', state),
    sample: samples.selector('results', state)[
      metadata.selector('currentSample', state)
    ],
    sources: sources.selector('results', state)
  };
};

const mapDispatchToProps = (dispatch, own) => {
  return {};
};

export default withNamespace({ metadata, twod })(
  connect(mapStateToProps, mapDispatchToProps)(SampleRegistration)
);
