import { connect } from 'react-redux';

import NewScan from 'components/samples/NewScan';
import { withNamespace } from 'providers/namespace';

import samplescan from 'providers/samplescan';
import app from 'providers/app';

const mapStateToProps = (state, own) => {
  const { scans } = own.providers.samplescan;
  return {
    scans: scans.selector('results', state),
    schema: app.selector('form', state).newscanSampleSchema || ''
  };
};

const mapDispatchToProps = (dispatch, own) => ({
  actions: {
    fetchScansTypes: payload => own.providers.samplescan.scans.fetch(payload),
    setSchema: payload =>
      app.dispatch('SET_FORM', { key: 'newscanSampleSchema', value: payload })
  }
});

export default withNamespace({ samplescan })(
  connect(mapStateToProps, mapDispatchToProps)(NewScan)
);
