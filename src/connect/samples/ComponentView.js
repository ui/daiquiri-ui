import { connect } from 'react-redux';

import { withNamespace } from 'providers/namespace';
import metadata from 'providers/metadata';
import app from 'providers/app';

import ComponentView from 'components/samples/ComponentView';

const mapStateToProps = (state, own) => ({
  component: own.providers.metadata.components.selector('results', state)[
    own.componentid
  ],
  componentSchema: own.providers.app.schema.selector('results', state)
    .ComponentSchema
});

const mapDispatchToProps = (dispatch, own) => {
  const { components } = own.providers.metadata;
  return {
    actions: {
      updateComponent: payload => components.update(payload)
    }
  };
};

export default withNamespace({ metadata, app })(
  connect(mapStateToProps, mapDispatchToProps)(ComponentView)
);
