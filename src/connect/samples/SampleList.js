import { connect } from 'react-redux';

import { withNamespace } from 'providers/namespace';
import metadata from 'providers/metadata';
import app from 'providers/app';

import SampleList from 'components/samples/SampleList';

const mapStateToProps = (state, own) => ({
  currentSampleId: metadata.selector('currentSample', state),
  selectedSamples: metadata.selector('selectedSamples', state),
  samples: own.providers.metadata.samples.selector('results', state),
  sampleSchema: own.providers.app.schema.selector('results', state).SampleSchema
});

const mapDispatchToProps = (dispatch, own) => {
  const { samples, components } = own.providers.metadata;
  return {
    actions: {
      fetchSamples: payload => samples.fetch(payload),
      fetchComponents: payload => components.fetch(payload),
      addSample: payload => samples.add(payload),
      updateSample: payload => samples.update(payload),
      addComponent: payload => components.add(payload),
      updateComponent: payload => components.update(payload),
      addSelection: payload => metadata.dispatch('SELECT_SAMPLE', payload),
      removeSelection: payload => metadata.dispatch('UNSELECT_SAMPLE', payload),
      resetSelection: () => metadata.dispatch('RESET_SAMPLE_SELECTION')
    }
  };
};

export default withNamespace({ metadata, app })(
  connect(mapStateToProps, mapDispatchToProps)(SampleList)
);
