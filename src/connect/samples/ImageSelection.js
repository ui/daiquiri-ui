import { connect } from 'react-redux';

import ImageSelection from 'components/samples/ImageSelection';
import { withNamespace } from 'providers/namespace';
import metadata from 'providers/metadata';

const mapStateToProps = (state, own) => ({
  images: own.providers.metadata.images.selector('results', state)
});

const mapDispatchToProps = (dispatch, own) => ({
  actions: {
    updateImage: payload => own.providers.metadata.images.updateLocal(payload)
  }
});

export default withNamespace({ metadata })(
  connect(mapStateToProps, mapDispatchToProps)(ImageSelection)
);
