import { connect } from 'react-redux';
import { withNamespace } from 'providers/namespace';
import metadata from 'providers/metadata';
import session from 'providers/session';
import SampleDCList from 'components/samples/SampleDCList';

function mapStateToProps(state: any, own: any) {
  const { datacollections, samples } = own.providers.metadata;
  return {
    operator: session.selector('operator', state),
    currentSample: metadata.selector('currentSample', state),
    sample: samples.selector('results', state)[
      metadata.selector('currentSample', state)
    ],
    datacollections: datacollections.selector('ordered', state),
    dcnamespace: own.providers.metadata.datacollections.getNamespace(),
    namespace: datacollections.getNamespace(),
    params: datacollections.selector('params', state),
    per_page: datacollections.selector('per_page', state),
    page: datacollections.selector('page', state),
    total: datacollections.selector('total', state),
    fetching: datacollections.selector('fetching', state),
    selected: metadata.selector('selectedDataCollections', state),
  };
}

function mapDispatchToProps(dispatch: any, own: any) {
  const { datacollections } = own.providers.metadata;
  return {
    actions: {
      fetch: () => datacollections.fetch(),
      setParams: (payload: any) => datacollections.setParams(payload, true),
      setPage: (payload: any) => datacollections.setPage(payload),
      setPageSize: (payload: any) => datacollections.setPageSize(payload),
      addSelection: (payload: any) =>
        metadata.dispatch('SELECT_DATA_COLLECTION', payload),
      removeSelection: (payload: any) =>
        metadata.dispatch('UNSELECT_DATA_COLLECTION', payload),
      resetSelection: () =>
        metadata.dispatch('RESET_DATA_COLLECTION_SELECTION'),
    },
  };
}

export default withNamespace({ metadata })(
  connect(mapStateToProps, mapDispatchToProps)(SampleDCList)
);
