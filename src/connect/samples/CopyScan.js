import { connect } from 'react-redux';

import { withNamespace } from 'providers/namespace';

import session from 'providers/session';
import metadata from 'providers/metadata';
import app from 'providers/app';

import CopyScan from 'components/samples/CopyScan';

const mapStateToProps = (state, own) => ({
  operator: session.selector('operator', state),
  config: app.selector('config', state, 'imageviewer'),
  objects: own.providers.metadata.subsamples.selector('results', state)
});

export default withNamespace({ metadata })(connect(mapStateToProps)(CopyScan));
