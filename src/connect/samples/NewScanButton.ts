import { connect } from 'react-redux';

import { withNamespace } from 'providers/namespace';

import metadata from 'providers/metadata';
import session from 'providers/session';
import queuep from 'providers/queue';

import NewScanButtonSample2 from 'components/samples/NewScanButtonSample2';
import type { Cache } from '../ConnectUtils';
import mapStateToPropsWithCache from '../ConnectUtils';

function mapStateToProps(state: any, own: any, cache: Cache) {
  const { providers, button, disabled, additionalFormData, options } = own;

  const { datacollections, samples } = providers.metadata;
  return {
    operator: session.selector('operator', state),
    currentSample: metadata.selector('currentSample', state),
    sample: samples.selector('results', state)[
      metadata.selector('currentSample', state)
    ],
    button,
    disabled,
    additionalFormData,
    options,
    datacollections: datacollections.selector('ordered', state),
    dcnamespace: providers.metadata.datacollections.getNamespace(),
    namespace: datacollections.getNamespace(),
    params: datacollections.selector('params', state),
    per_page: datacollections.selector('per_page', state),
    page: datacollections.selector('page', state),
    total: datacollections.selector('total', state),
    fetching: datacollections.selector('fetching', state),
    selected: metadata.selector('selectedDataCollection', state),
    queueRunning: queuep.selector('running', state),
    queueReady: queuep.selector('ready', state),
  };
}

function mapDispatchToProps(dispatch: any, own: any) {
  const { datacollections } = own.providers.metadata;
  return {
    actions: {
      fetch: () => datacollections.fetch(),
      setParams: (payload: any) => datacollections.setParams(payload, true),
      setPage: (payload: any) => datacollections.setPage(payload),
      setPageSize: (payload: any) => datacollections.setPageSize(payload),
      select: (payload: any) =>
        metadata.dispatch('SELECT_DATA_COLLECTION', payload),
      setFollow: (payload: any) =>
        metadata.dispatch('SELECT_DATA_COLLECTION', null),
    },
  };
}

export default withNamespace({ metadata })(
  connect(
    mapStateToPropsWithCache(mapStateToProps),
    mapDispatchToProps
  )(NewScanButtonSample2)
);
