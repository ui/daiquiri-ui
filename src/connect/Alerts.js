import { connect } from 'react-redux';

import Alerts from 'components/Alerts';
import app from 'providers/app';
import scans from 'providers/scans';

const mapDispatchToProps = () => ({
  actions: {
    removeAlert: payload => {
      app.dispatch('REMOVE_ALERT', payload);
    }
  }
});

const mapStateToProps = state => ({
  alerts: app.selector('alerts', state),
  ready: app.selector('ready', state),
  networkError: app.selector('networkError', state),
  runningScan: scans.selector('runningScan', state)
});

export default connect(mapStateToProps, mapDispatchToProps)(Alerts);
