import { connect } from 'react-redux';

import app from 'providers/app';
import session from 'providers/session';

import SessionMirror from 'components/SessionMirror';

const mapStateToProps = state => ({
  mirrorid: session.selector('mirrorid', state),
  cursor: app.selector('cursor', state),
  window: app.selector('window', state)
});

const mapDispatchToProps = () => ({
  actions: {
    updateCursor: payload => app.dispatch('UPDATE_CURSOR', payload),
    updateWindow: payload => app.dispatch('UPDATE_WINDOW', payload),
    requestMirror: payload => session.dispatch('REQUEST_MIRROR', payload)
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(SessionMirror);
