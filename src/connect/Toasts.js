import { connect } from 'react-redux';

import app from 'providers/app';
import Toasts from 'components/Toasts';

const mapDispatchToProps = dispatch => ({
  actions: {
    removeToast: payload => {
      app.dispatch('REMOVE_TOAST', payload);
    }
  }
});

const mapStateToProps = state => ({
  toasts: app.selector('toasts', state)
});

export default connect(mapStateToProps, mapDispatchToProps)(Toasts);
