import { connect } from 'react-redux';

import Hdf5Plot from 'components/hdf5/Hdf5Plot';
import { withNamespace } from 'providers/namespace';
import hdf5 from 'providers/hdf5';
import metadata from 'providers/metadata';
import type { Cache } from '../ConnectUtils';
import mapStateToPropsWithCache from '../ConnectUtils';

const mapStateToPropsSimple = (state: any, own: any, cache: Cache) => {
  const { providers, options } = own;
  const { groups } = providers.hdf5;
  return {
    group: groups.selector('results', state)[options.uri],
    error: groups.selector('error', state),
    params: providers.hdf5.groups.selector('params', state),
    datacollectionid: metadata.selector('selectedDataCollection', state),
  };
};

function mapDispatchToProps(dispatch: any, own: any) {
  return {
    actions: {
      setParams: (
        params: {
          type?: string;
          uri?: string;
          datacollectionid?: number | null;
          autoprocprogramid?: number;
        },
        fetch?: boolean
      ) => own.providers.hdf5.groups.setParams(params, fetch),
    },
  };
}

export default withNamespace({ hdf5 })(
  connect(
    mapStateToPropsWithCache(mapStateToPropsSimple),
    mapDispatchToProps
  )(Hdf5Plot)
);
