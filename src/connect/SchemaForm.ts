import { connect } from 'react-redux';

import { withNamespace } from 'providers/namespace';
import app from 'providers/app';
import session from 'providers/session';
import scans from 'providers/scans';

import SchemaForm from 'components/SchemaForm';

function mapStateToProps(state: any, own: any) {
  return {
    schema: own.schema,
    fetching: own.providers.app.schema.selector('fetching', state),
    fetched: own.providers.app.schema.selector('fetched', state),
    schemas: own.providers.app.schema.selector('results', state),
    operator: session.selector('operator', state),
    runningScan: scans.selector('runningScanid', state),
    formStore: app.selector('form', state)[own.schema],
    mirrorid: session.selector('mirrorid', state),
  };
}

function mapDispatchToProps(dispatch: any, own: any) {
  return {
    actions: {
      fetch: () => own.providers.app.schema.fetchSingle(own.schema),
      alert: (payload: any) => app.dispatch('ADD_ALERT', payload),
      validate: (payload: any) => app.dispatch('VALIDATE_SCHEMA', payload),
      savePreset: (payload: any) => app.dispatch('SAVE_SCHEMA_PRESET', payload),
      setForm: (payload: any) =>
        app.dispatch('SET_FORM', { key: own.schema, value: payload }),
    },
  };
}

export default withNamespace({ app })(
  connect(mapStateToProps, mapDispatchToProps)(SchemaForm)
);
