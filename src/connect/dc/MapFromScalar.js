import { connect } from 'react-redux';

import scans from 'providers/scans';
import twod from 'providers/2dview';
import app from 'providers/app';
import metadata from 'providers/metadata';

import { withNamespace } from 'providers/namespace';
import MapFromScalar from 'components/dc/MapFromScalar';

const mapStateToProps = (state, own) => {
  const { data, list } = own.providers.scans;
  return {
    scan: list.selector('results', state)[own.scanid],
    fetchedScan: list.selector('fetched', state),
    fetchingScan: list.selector('fetching', state),
    scanError: list.selector('error', state),
    data: data.selector('results', state),
    error: data.selector('error', state),
    maps: own.providers.metadata.xrf_maps.selector('results', state),
    fetched: data.selector('fetched', state),
    fetching: data.selector('fetching', state),
    creatingAdditional: twod.selector('pendingAdditionalMap', state),
    mapSettings: own.providers.twod.maps.selector('results', state)
  };
};

const mapDispatchToProps = (dispatch, own) => {
  const { data, list } = own.providers.scans;
  return {
    actions: {
      fetchScanData: payload => data.fetch(payload),
      fetchScan: payload => list.fetchSingle(payload),
      createMap: payload => twod.dispatch('ADDITIONAL_MAP', payload),
      addToast: payload => app.dispatch('ADD_TOAST', payload),
      fetchMapSettings: () => own.providers.twod.maps.fetch(),
      updateMapSettings: payload => own.providers.twod.maps.update(payload)
    }
  };
};

export default withNamespace({ scans, metadata, twod })(
  connect(mapStateToProps, mapDispatchToProps)(MapFromScalar)
);
