import { connect } from 'react-redux';

import metadata from 'providers/metadata';

import { withNamespace } from 'providers/namespace';
import AutoProcPrograms from 'components/dc/AutoProcPrograms';

const mapStateToProps = (state, own) => {
  const { autoprocprograms } = own.providers.metadata;
  return {
    className: own.className,
    autoprocprograms: autoprocprograms.selector('results', state),
    params: autoprocprograms.selector('params', state),
    per_page: autoprocprograms.selector('per_page', state),
    page: autoprocprograms.selector('page', state),
    total: autoprocprograms.selector('total', state)
  };
};

const mapDispatchToProps = (dispatch, own) => {
  const { autoprocprograms } = own.providers.metadata;
  return {
    actions: {
      fetch: () => autoprocprograms.fetch(),
      setParams: payload => autoprocprograms.setParams(payload, true),
      setPage: payload => autoprocprograms.setPage(payload),
      setPageSize: payload => autoprocprograms.setPageSize(payload)
    }
  };
};

export default withNamespace({ metadata })(
  connect(mapStateToProps, mapDispatchToProps)(AutoProcPrograms)
);
