import { connect } from 'react-redux';

import metadata from 'providers/metadata';

import { withNamespace } from 'providers/namespace';
import SQIndicators from 'components/dc/SQIndicators';

const mapStateToProps = (state, own) => {
  const { sqis } = own.providers.metadata;
  return {
    sqis: sqis.selector('results', state)
  };
};

const mapDispatchToProps = (dispatch, own) => {
  const { sqis } = own.providers.metadata;
  return {
    actions: {
      fetch: () => sqis.fetch(),
      setParams: payload => sqis.setParams(payload, true),
      selectAutoProcProgram: payload =>
        metadata.dispatch('SELECT_AUTO_PROC_PROGRAM', payload),
      ...own.actions
    }
  };
};

export default withNamespace({ metadata })(
  connect(mapStateToProps, mapDispatchToProps)(SQIndicators)
);
