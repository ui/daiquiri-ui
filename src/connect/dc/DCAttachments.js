import { connect } from 'react-redux';

import metadata from 'providers/metadata';

import { withNamespace } from 'providers/namespace';
import DCAttachments from 'components/dc/DCAttachments';

const mapStateToProps = (state, own) => {
  const { dcattachments } = own.providers.metadata;
  return {
    attachments: dcattachments.selector('results', state),
    fetched: dcattachments.selector('fetched', state),
    fetching: dcattachments.selector('fetching', state),
    params: dcattachments.selector('params', state),
    per_page: dcattachments.selector('per_page', state),
    page: dcattachments.selector('page', state),
    total: dcattachments.selector('total', state)
  };
};

const mapDispatchToProps = (dispatch, own) => {
  const { dcattachments } = own.providers.metadata;
  return {
    actions: {
      fetch: () => dcattachments.fetch(),
      setParams: payload => dcattachments.setParams(payload, true),
      setPage: payload => dcattachments.setPage(payload),
      setPageSize: payload => dcattachments.setPageSize(payload)
    }
  };
};

export default withNamespace({ metadata })(
  connect(mapStateToProps, mapDispatchToProps)(DCAttachments)
);
