import { connect } from 'react-redux';

import metadata from 'providers/metadata';

import { withNamespace } from 'providers/namespace';
import DCAttachmentPlot from 'components/dc/DCAttachmentPlot';

const mapStateToProps = (state, own) => {
  const { dcattachments } = own.providers.metadata;
  return {
    attachments: dcattachments.selector('results', state),
    fetched: dcattachments.selector('fetched', state),
    fetching: dcattachments.selector('fetching', state),
    params: dcattachments.selector('params', state)
  };
};

const mapDispatchToProps = (dispatch, own) => {
  const { dcattachments } = own.providers.metadata;
  return {
    actions: {
      fetch: () => dcattachments.fetch(),
      setParams: payload => dcattachments.setParams(payload, true)
    }
  };
};

export default withNamespace({ metadata })(
  connect(mapStateToProps, mapDispatchToProps)(DCAttachmentPlot)
);
