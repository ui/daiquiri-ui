import { connect } from 'react-redux';

import metadata from 'providers/metadata';

import { withNamespace } from 'providers/namespace';
import DCList from 'components/dc/DCList';

const mapStateToProps = (state, own) => {
  const { datacollections } = own.providers.metadata;
  return {
    className: own.className,
    showDataCollection: own.showDataCollection,
    datacollections: datacollections.selector('ordered', state),
    params: datacollections.selector('params', state),
    per_page: datacollections.selector('per_page', state),
    page: datacollections.selector('page', state),
    total: datacollections.selector('total', state),
    fetching: datacollections.selector('fetching', state),
    namespace: datacollections.getNamespace(),
    selected:
      own.selected !== undefined
        ? own.selected
        : metadata.selector('selectedDataCollections', state)
  };
};

const mapDispatchToProps = (dispatch, own) => {
  const { datacollections } = own.providers.metadata;
  return {
    actions: {
      fetch: () => datacollections.fetch(),
      setParams: payload => datacollections.setParams(payload, true),
      setPage: payload => datacollections.setPage(payload),
      setPageSize: payload => datacollections.setPageSize(payload),
      ...(own.actions ? { ...own.actions } : null)
    }
  };
};

export default withNamespace({ metadata })(
  connect(mapStateToProps, mapDispatchToProps)(DCList)
);
