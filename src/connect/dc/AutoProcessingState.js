import { connect } from 'react-redux';

import metadata from 'providers/metadata';

import { withNamespace } from 'providers/namespace';
import AutoProcessingState from 'components/dc/AutoProcessingState';

const mapStateToProps = (state, own) => {
  const { autoprocprograms } = own.providers.metadata;
  const order = autoprocprograms.selector('order', state);
  const lastresult = order ? (order[0] ? order[0][0] : null) : null;
  return {
    autoprocprogram: lastresult
  };
};

const mapDispatchToProps = (dispatch, own) => {
  const { autoprocprograms } = own.providers.metadata;
  return {
    actions: {
      fetch: payload => {
        autoprocprograms.setParams({ ...payload, order: 'desc' }, true);
      }
    }
  };
};

export default withNamespace({ metadata })(
  connect(mapStateToProps, mapDispatchToProps)(AutoProcessingState)
);
