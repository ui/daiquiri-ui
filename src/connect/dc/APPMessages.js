import { connect } from 'react-redux';

import metadata from 'providers/metadata';

import { withNamespace } from 'providers/namespace';
import APPMessages from 'components/dc/APPMessages';

const mapStateToProps = (state, own) => {
  const { appmessages } = own.providers.metadata;
  return {
    attachments: appmessages.selector('results', state),
    fetched: appmessages.selector('fetched', state),
    fetching: appmessages.selector('fetching', state),
    params: appmessages.selector('params', state),
    per_page: appmessages.selector('per_page', state),
    page: appmessages.selector('page', state),
    total: appmessages.selector('total', state)
  };
};

const mapDispatchToProps = (dispatch, own) => {
  const { appmessages } = own.providers.metadata;
  return {
    actions: {
      fetch: () => appmessages.fetch(),
      setParams: payload => appmessages.setParams(payload, true),
      setPage: payload => appmessages.setPage(payload),
      setPageSize: payload => appmessages.setPageSize(payload)
    }
  };
};

export default withNamespace({ metadata })(
  connect(mapStateToProps, mapDispatchToProps)(APPMessages)
);
