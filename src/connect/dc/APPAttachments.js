import { connect } from 'react-redux';

import metadata from 'providers/metadata';

import { withNamespace } from 'providers/namespace';
import APPAttachments from 'components/dc/APPAttachments';

const mapStateToProps = (state, own) => {
  const { appattachments } = own.providers.metadata;
  return {
    attachments: appattachments.selector('results', state),
    fetched: appattachments.selector('fetched', state),
    fetching: appattachments.selector('fetching', state),
    params: appattachments.selector('params', state),
    per_page: appattachments.selector('per_page', state),
    page: appattachments.selector('page', state),
    total: appattachments.selector('total', state)
  };
};

const mapDispatchToProps = (dispatch, own) => {
  const { appattachments } = own.providers.metadata;
  return {
    actions: {
      fetch: () => appattachments.fetch(),
      setParams: payload => appattachments.setParams(payload, true),
      setPage: payload => appattachments.setPage(payload),
      setPageSize: payload => appattachments.setPageSize(payload)
    }
  };
};

export default withNamespace({ metadata })(
  connect(mapStateToProps, mapDispatchToProps)(APPAttachments)
);
