import { connect } from 'react-redux';

import metadata from 'providers/metadata';
import app from 'providers/app';

import { withNamespace } from 'providers/namespace';
import DCComments from 'components/dc/DCComments';

const mapStateToProps = (state, own) => {
  const { schema } = own.providers.app;
  return {
    schema: schema.selector('results', state).DataCollectionSchema.definitions
      .DataCollectionSchema
  };
};

const mapDispatchToProps = (dispatch, own) => {
  const { datacollections } = own.providers.metadata;
  return {
    actions: {
      updateDataCollection: payload => datacollections.update(payload)
    }
  };
};

export default withNamespace({ metadata, app })(
  connect(mapStateToProps, mapDispatchToProps)(DCComments)
);
