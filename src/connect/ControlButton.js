import { connect } from 'react-redux';

import session from 'providers/session';

import ControlButton from 'components/ControlButton';

const mapStateToProps = state => ({
  operator: session.selector('operator', state)
});

const mapDispatchToProps = () => ({
  actions: {
    requestControl: () => session.dispatch('REQUEST_CONTROL'),
    yieldControl: () => session.dispatch('YIELD_CONTROL')
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(ControlButton);
