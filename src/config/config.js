const customApiUrl = process.env.REACT_APP_API_URL || undefined;
const isProd = process.env.NODE_ENV === 'production';
const requireHttps = process.env.REACT_APP_HTTPS === 'true';

const baseUrl =
  customApiUrl ||
  (isProd ? '/api' : `${requireHttps ? 'https' : 'http'}://localhost:8080/api`);

export default {
  baseUrl,
  pixelSize: 1e-9
};
