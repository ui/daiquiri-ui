import type {
  KeyboardEvent,
  ChangeEvent,
  MutableRefObject,
  ReactChild,
} from 'react';
import { forwardRef, useEffect, useRef, useState } from 'react';
import { map } from 'lodash';

import { Form, Button, InputGroup } from 'react-bootstrap';
import classNames from 'classnames';

interface Props {
  step?: number | string;
  steps?: (number | string)[];
  disabled: boolean;
  id?: string;
  unit?: string;
  overlay?: ReactChild | null;
  incIcon?: string;
  decIcon?: string;
  swapIncDec?: boolean;
  className?: string;
  type?: 'number' | 'text';
  precision?: number;
  horizontalArrows?: boolean;
  largeArrows?: boolean;
  onBlur: () => void;
  onChange: (e: ChangeEvent<HTMLInputElement>) => void;
  onStep: (params: { target: HTMLInputElement }) => void;
  onKeyDown?: (e: KeyboardEvent<HTMLInputElement>) => void;
}

const NumericStep = forwardRef<HTMLInputElement, Props>((props, ref) => {
  const stepSizeRef = useRef<HTMLSelectElement>(null);
  const focusTimeoutRef = useRef<NodeJS.Timeout>();
  const [currentStep, setCurrentStep] = useState(props.step);
  const [retakeFocus, setRetakeFocus] = useState(false);
  const onStep = (up: boolean) => {
    // FIXME: Would be good to remove that cast
    const ref2 = ref as MutableRefObject<HTMLInputElement>;
    if (stepSizeRef.current === null) {
      return;
    }
    let val = Number.parseFloat(ref2.current.value);

    // Exponential step up / down
    if (stepSizeRef.current.value.startsWith('*')) {
      const multiplier = Number.parseFloat(
        stepSizeRef.current.value.replace('*', '')
      );
      val = up ? val * multiplier : val / multiplier;

      // Normal step up / down
    } else {
      const stepSize = Number.parseFloat(stepSizeRef.current.value);
      val += up ? stepSize : -stepSize;
    }
    ref2.current.value = `${val}`;

    if (props.onStep) {
      props.onStep({
        target: ref2.current,
      });
    }
  };

  const {
    onStep: onStepProp,
    step,
    overlay,
    incIcon,
    decIcon,
    swapIncDec,
    onKeyDown,
    onBlur,
    horizontalArrows,
    largeArrows,
    ...rest
  } = props;

  const onKeyDown2 = (e: KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'ArrowUp') {
      e.preventDefault();
      setRetakeFocus(true);
      onStep(true);
    } else if (e.key === 'ArrowDown') {
      e.preventDefault();
      setRetakeFocus(true);
      onStep(false);
    } else if (onKeyDown) {
      onKeyDown(e);
    }
  };

  if (retakeFocus) {
    if (focusTimeoutRef.current) clearTimeout(focusTimeoutRef.current);
    focusTimeoutRef.current = setTimeout(() => {
      (ref as MutableRefObject<HTMLInputElement>)?.current?.focus();
    }, 100);
  }

  useEffect(() => {
    return () => {
      if (focusTimeoutRef.current) clearTimeout(focusTimeoutRef.current);
    };
  }, []);

  const onBlur2 = () => {
    if (focusTimeoutRef.current) {
      clearTimeout(focusTimeoutRef.current);
    }
    setRetakeFocus(false);
    if (onBlur) onBlur();
  };

  if (!props.step) {
    return (
      <Form.Control
        ref={ref}
        type="number"
        step="any"
        onKeyDown={onKeyDown2}
        onBlur={onBlur2}
        {...rest}
      />
    );
  }

  const steps = map(props.steps || [props.step], (s) => (
    <option value={s} key={s}>
      {s}
    </option>
  ));

  return (
    <div
      className={classNames('numeric-step', {
        'numeric-step-large': largeArrows,
        'numeric-step-horizontal': horizontalArrows,
      })}
    >
      <InputGroup>
        <Form.Control
          onKeyDown={onKeyDown2}
          onBlur={onBlur2}
          ref={ref}
          type="number"
          step="any"
          {...rest}
        />
        <>
          {overlay}
          {!overlay && (
            <InputGroup.Text className="d-flex flex-column">
              <Form.Control
                ref={stepSizeRef}
                as="select"
                className="step-size"
                defaultValue={currentStep}
                onChange={(e) =>
                  setCurrentStep(Number.parseFloat(e.target.value))
                }
              >
                {steps}
              </Form.Control>
              {props.unit && <div>{props.unit}</div>}
              {!incIcon && !decIcon && (
                <>
                  <Button
                    className="step step-up"
                    disabled={props.disabled}
                    onClick={(e) => onStep(true)}
                  />
                  <Button
                    className="step step-down"
                    disabled={props.disabled}
                    onClick={(e) => onStep(false)}
                  />
                </>
              )}
            </InputGroup.Text>
          )}
          {decIcon && swapIncDec && (
            <Button disabled={props.disabled} onClick={(e) => onStep(false)}>
              <i className={`fa fa-fw fa-${decIcon}`} />
            </Button>
          )}
          {incIcon && (
            <Button disabled={props.disabled} onClick={(e) => onStep(true)}>
              <i className={`fa fa-fw fa-${incIcon}`} />
            </Button>
          )}
          {decIcon && !swapIncDec && (
            <Button disabled={props.disabled} onClick={(e) => onStep(false)}>
              <i className={`fa fa-fw fa-${decIcon}`} />
            </Button>
          )}
        </>
      </InputGroup>
    </div>
  );
});

export default NumericStep;
