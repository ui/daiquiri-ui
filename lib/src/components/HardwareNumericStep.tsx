import { useRef, useState, useEffect } from 'react';
import type { KeyboardEvent, ChangeEvent } from 'react';
import classNames from 'classnames';
import { Button } from 'react-bootstrap';
import NumericStep from './NumericStep';

/**
 * NumericStep handled hardware properties.
 *
 * Input related to hardware have specificities because the hardware state can
 * change during the user interaction.
 */
export default function HardwareNumericStep(props: {
  hardwareValue: number | null;
  hardwareIsDisabled: boolean;
  hardwareIsMoving: boolean;
  hardwareIsReady: boolean;
  onMoveRequested: (value: number) => Promise<void> | null;
  onAbortRequested: () => void;
  /** Default step size to use for up / down arrows */
  step?: number | string;
  /** Array of selectable step sizes */
  steps?: (number | string)[];
  /** Number of decimals to show */
  precision?: number;
  /** Whether this widget is read only */
  readOnly?: boolean;
  /** Specify a fontawesome to inc the value */
  incIcon?: string;
  /** Specify a fontawesome to dec the value */
  decIcon?: string;
  /** If true inc and dec icon are swapped */
  swapIncDec?: boolean;
  /** Show the step arrows horizontally instead of vertically */
  horizontalArrows?: boolean;
  /** Show large step arrows */
  largeArrows?: boolean;
  /** Identifier passed to the input element */
  id?: string;
}) {
  const valueRef = useRef<HTMLInputElement>(null);
  const [edited, setEdited] = useState(false);
  const [error, setError] = useState(false);
  useEffect(() => {
    if (valueRef.current) {
      if (props.hardwareValue === null) {
        valueRef.current.value = '';
      } else if (props.precision !== undefined) {
        valueRef.current.value = props.hardwareValue.toFixed(props.precision);
      } else {
        valueRef.current.value = props.hardwareValue.toString();
      }
      setEdited(false);
    }
  }, [props.hardwareValue]);

  function updateRef(value: any) {
    let newValue = value;
    if (props.precision !== undefined) {
      newValue = Number.parseFloat(newValue).toFixed(props.precision);
    }
    if (valueRef?.current) {
      valueRef.current.value = newValue;
    }
  }

  function onBlur() {
    if (edited) {
      setTimeout(() => {
        updateRef(props.hardwareValue);
        setEdited(false);
      }, 3000);
    }
  }

  function onChange(e: ChangeEvent<HTMLInputElement>) {
    if (props.hardwareValue === null) {
      return;
    }
    setEdited(!(e.target.value === props.hardwareValue.toString()));
  }

  function onStep(e: any) {
    setEdited(false);
    const promise = props.onMoveRequested(e.target.value);
    if (promise) {
      promise.catch(() => {
        setError(true);
        setTimeout(() => {
          updateRef(props.hardwareValue);
          setError(false);
        }, 2000);
      });
    }
  }

  function onKeyDown(e: KeyboardEvent<HTMLInputElement>) {
    switch (e.key) {
      case 'Enter':
        {
          setEdited(false);
          // @ts-expect-error
          const value: number = Number.parseFloat(e.target.value);
          const promise = props.onMoveRequested(value);
          if (promise) {
            promise.catch(() => {
              setError(true);
              setTimeout(() => {
                updateRef(props.hardwareValue);
                setError(false);
              }, 2000);
            });
          }
        }
        e.preventDefault();
        e.stopPropagation();
        break;
      case 'Esc': // IE/Edge specific value
      case 'Escape':
        if (edited) {
          setError(false);
          setEdited(false);
          updateRef(props.hardwareValue);
        }
        // @ts-expect-error
        e.target.blur();
        e.preventDefault();
        e.stopPropagation();
        break;
    }
  }

  return (
    <NumericStep
      id={props.id}
      className={classNames({
        'form-control-edited': edited,
        'form-control-error': error,
        'hw-moving': props.hardwareIsMoving,
      })}
      type={props.readOnly ? 'text' : 'number'}
      ref={valueRef}
      precision={props.precision}
      onChange={onChange}
      onBlur={onBlur}
      onStep={onStep}
      onKeyDown={onKeyDown}
      disabled={
        props.hardwareIsDisabled || props.readOnly || !props.hardwareIsReady
      }
      step={props.step}
      steps={props.steps}
      incIcon={props.incIcon}
      decIcon={props.decIcon}
      swapIncDec={props.swapIncDec}
      horizontalArrows={props.horizontalArrows}
      largeArrows={props.largeArrows}
      overlay={
        props.hardwareIsMoving && !props.hardwareIsDisabled ? (
          <Button variant="danger" onClick={props.onAbortRequested}>
            <i className="fa fa-times" />
          </Button>
        ) : null
      }
    />
  );
}
