import { useEffect, useRef, useState } from 'react';
import type { KeyboardEvent, ChangeEvent } from 'react';
import { Form } from 'react-bootstrap';
import classNames from 'classnames';

/**
 * Input number synchronized with a hardware.
 *
 * Input related to hardware have specificities because the hardware state can
 * change during the user interaction.
 *
 * TODO: Maybe normalize `precision` and `step` together
 */
export default function HardwareInputNumber(props: {
  hardwareValue: number;
  hardwareIsDisabled: boolean;
  hardwareIsMoving?: boolean;
  hardwareIsReady?: boolean;
  onMoveRequested: (value: number) => Promise<void> | null;
  onAbortRequested?: () => void;
  readOnly?: boolean;
  precision?: number;
  step?: number;
}) {
  const valueRef = useRef<HTMLInputElement>(null);
  const [edited, setEdited] = useState(false);
  const [error, setError] = useState(false);

  function normalizeNumber(value: string): string {
    if (props.precision !== undefined) {
      return Number.parseFloat(value).toFixed(props.precision);
    }
    return value;
  }

  useEffect(() => {
    if (valueRef.current) {
      valueRef.current.value = normalizeNumber(props.hardwareValue?.toString());
      setEdited(false);
    }
  }, [props.hardwareValue, props.precision]);

  function updateRef(value: any) {
    const newValue = normalizeNumber(value);
    if (valueRef?.current) {
      valueRef.current.value = newValue;
    }
  }

  function onKeyDown(e: KeyboardEvent<HTMLInputElement>) {
    switch (e.key) {
      case 'Enter': {
        setEdited(false);
        // @ts-expect-error
        const value: number = Number.parseFloat(e.target.value);
        const promise = props.onMoveRequested(value);
        if (promise) {
          promise.catch(() => {
            setError(true);
            setTimeout(() => {
              updateRef(props.hardwareValue);
              setError(false);
            }, 2000);
          });
        }
        e.preventDefault();
        e.stopPropagation();
        break;
      }
      case 'Esc': // IE/Edge specific value
      case 'Escape':
        if (edited) {
          setError(false);
          setEdited(false);
          updateRef(props.hardwareValue);
        }
        // @ts-expect-error
        e.target.blur();
        e.preventDefault();
        e.stopPropagation();
        break;
    }
  }

  function onChange(e: ChangeEvent<HTMLInputElement>) {
    setEdited(!(e.target.value === props.hardwareValue.toString()));
  }

  return (
    <Form.Control
      className={classNames({
        'form-control-edited': edited,
        'form-control-error': error,
        'hw-moving': props.hardwareIsMoving,
      })}
      type="number"
      ref={valueRef}
      onChange={onChange}
      onKeyDown={onKeyDown}
      disabled={
        props.readOnly || props.hardwareIsMoving || props.hardwareIsDisabled
      }
      step={props.step}
    />
  );
}
