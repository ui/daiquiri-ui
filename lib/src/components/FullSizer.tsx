import type { PropsWithChildren } from 'react';
import { useEffect, useRef, useState } from 'react';

interface Props {
  className?: string;
}

export default function FullSizer(props: PropsWithChildren<Props>) {
  const [state, setState] = useState<Record<string, number>>({});
  const containerRef = useRef<HTMLDivElement>(null);
  useEffect(() => {
    if (containerRef.current) {
      const view = containerRef.current;
      setState({
        width: view.clientWidth,
        height: view.clientHeight,
      });
    }
  }, []);

  return (
    <div
      ref={containerRef}
      style={{ width: '100%', height: '100%' }}
      className={props.className}
    >
      {state.width > 0 && (
        <div
          className="full-sizer"
          style={{
            width: `${state.width}px`,
            height: `${state.height}px`,
            overflow: 'scroll',
          }}
        >
          {props.children}
        </div>
      )}
    </div>
  );
}
