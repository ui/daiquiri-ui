// https://github.com/gentooboontoo/js-quantities/issues/30
export function formatEng(scalar: number) {
  const powerPrefix: Record<number, string> = {
    24: 'Y',
    21: 'Z',
    18: 'E',
    15: 'P',
    12: 'T',
    9: 'G',
    6: 'M',
    3: 'k',
    '-3': 'm',
    '-6': '\u00B5',
    '-9': 'n',
    '-12': 'p',
    '-15': 'f',
    '-18': 'a',
    '-21': 'z',
    '-24': 'y',
  };

  const q = Math.log(scalar) / Math.log(1e3);
  // so that for example '3 km' does not get converted to '3000 m'
  const subtrhnd = !Number.isInteger(q);

  const pow10 = 3 * Math.ceil(q - (subtrhnd ? 1 : 0));
  const prefix = pow10 === 0 ? '' : powerPrefix[pow10];

  return {
    scalar: scalar * 10 ** -pow10,
    prefix,
    multiplier: 10 ** -pow10,
  };
}

/**
 * Returns the argument as capitalized string.
 */
export function ucfirst(str: string) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

export function toHoursMins(seconds: number) {
  if (seconds < 60) return `${Math.round(seconds)} sec`;

  const min = Math.round(seconds / 60);

  const mins = min % 60;
  const hours = Math.floor(min / 60);

  return hours ? `${hours} hr ${mins} min` : `${mins} min`;
}

export function toEnergy(wavelength: number) {
  return wavelength > 0
    ? (
        ((6.626_070_04e-34 * 2.997_924_58e8) /
          (wavelength * 1e-10) /
          1.602_18e-19) *
        1e-3
      ).toFixed(4)
    : 0;
}

export function round(value: number, digits: number) {
  return Number.parseFloat(value.toFixed(digits));
}
