export interface Monitor {
  name: string;
  type?: string;
  value: string;
  overlay?: string;
  comparator?: string;
  comparison?: any;
  unit?: string;
}
