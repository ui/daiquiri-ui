import { map } from 'lodash';
import type { ComponentType, PropsWithChildren } from 'react';
import classNames from 'classnames';
import { Row, Col, Container } from 'react-bootstrap';
import { MonitorHardware } from './MonitorHardware';
import type { Monitor } from './types';

interface MonitorMapping {
  [name: string]: ComponentType<any>;
}

export const monitorMap: MonitorMapping = {
  hardware: MonitorHardware,
};

export function registerMonitorComponent(
  name: string,
  component: React.ComponentType<any>
) {
  monitorMap[name] = component;
}

function YmlError(props: { monitor: Monitor; message: string }) {
  return (
    <Col
      className="monitor-panel-item bg-fatal"
      title={`Error from yaml configuration: ${props.message}`}
    >
      <h1>{props.monitor.name}</h1>
      <div>Parsing error</div>
    </Col>
  );
}

export interface MonitorPanelProps {
  monitors: Monitor[];
  margin?: 'start' | null;
  bg?: 'light' | null;
}

export function MonitorPanel(props: MonitorPanelProps) {
  const properties = map(props.monitors, (monitor, i) => {
    const type = monitor.type ?? 'hardware';
    const Comp = monitorMap[type];
    if (Comp === undefined) {
      return (
        <YmlError
          key={`m${i}`}
          monitor={monitor}
          message={`Type ${type} unsupported`}
        />
      );
    }
    return <Comp key={`m${i}`} monitor={monitor} />;
  });

  return (
    <div
      className={classNames(
        { 'ms-auto': props.margin === 'start' },
        'me-auto',
        'monitor-panel',
        { 'monitor-panel-light': props.bg === 'light' }
      )}
    >
      <Container fluid>
        <Row className="flex-nowrap">{properties}</Row>
      </Container>
    </div>
  );
}

interface MonitorPanelItemProps {}

export function MonitorPanelItem(
  props: PropsWithChildren<MonitorPanelItemProps>
) {
  return <Col className="monitor-panel-item">{props.children}</Col>;
}
