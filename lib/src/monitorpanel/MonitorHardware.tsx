import { Col, OverlayTrigger, Tooltip } from 'react-bootstrap';
import type { Monitor } from './types';
import type { Hardware } from '../hardware/utils/types';
import { MonitorPanelItem } from './MonitorPanel';

const runtimeHooks: Record<string, (id: string | null) => Hardware | null> = {};

export function registerRuntimeHook(
  name: string,
  hook: (id: string | null) => Hardware | null
) {
  runtimeHooks[name] = hook;
}

function useHardware(id: string | null) {
  if (!runtimeHooks.useHardware) {
    throw new Error('No `useHardware` hook registered');
  }
  return runtimeHooks.useHardware(id);
}

export const dynamicOp = (op: string, a1: any, b1: any) => {
  const operators: { [op: string]: (a: any, b: any) => boolean } = {
    '>': (a, b) => a > b,
    '<': (a, b) => a < b,
    '>=': (a, b) => a >= b,
    '<=': (a, b) => a <= b,
    '==': (a, b) => a === b,
    '!=': (a, b) => a !== b,
    in: (a, b) => b.indexOf(a) > -1,
  };

  if (op in operators) return operators[op](a1, b1);
  return undefined;
};

function rsplit(s: string, separator: string, limits?: number): string[] {
  const split = s.split(separator);
  return limits
    ? [split.slice(0, -limits).join(separator)].concat(split.slice(-limits))
    : split;
}

export function useValue(valuePath: string | undefined): unknown {
  const path = valuePath !== undefined ? rsplit(valuePath, '.', 1) : [null, ''];
  const obj = useHardware(path[0]);
  if (!obj) {
    return undefined;
  }
  if (!obj.properties) {
    return undefined;
  }
  if (path[1] === null) {
    return undefined;
  }
  if (!Array.isArray(obj.properties[path[1]])) {
    return obj.properties[path[1]];
  }
  return obj.properties[path[1]][0];
}

export function useUnit(unitPath?: string): string | undefined {
  const isFixedUnit = unitPath !== undefined && !unitPath.includes('.');
  const path = unitPath !== undefined ? rsplit(unitPath, '.', 1) : [null, ''];
  const obj = useHardware(path[0]);
  if (isFixedUnit) {
    // That's a fixed unit defined as an option
    return unitPath;
  }
  if (!obj) {
    return undefined;
  }
  if (!obj.properties) {
    // The unit is maybe not yet there
    return undefined;
  }
  if (path[1] === null) {
    return undefined;
  }
  return `${obj.properties[path[1]]}`;
}

export function MonitorHardware(props: { monitor: Monitor }) {
  const { monitor } = props;

  let value = useValue(monitor.value) ?? '-';
  const unit = useUnit(monitor.unit);
  const overlay = useValue(monitor.overlay);

  const flt = Number.parseFloat(`${value}`);
  if (!Number.isNaN(flt)) {
    const str = String(flt);
    if (str.length > 5) {
      value = flt.toPrecision(5);
    }
  }

  let checkClass;
  if (monitor.comparator && monitor.comparison) {
    const check = dynamicOp(monitor.comparator, value, monitor.comparison);
    checkClass = check ? 'valid' : 'invalid';
  }

  if (!Number.isNaN(flt)) {
    if (unit !== undefined) {
      value = `${value} ${unit}`;
    }
  }

  if (overlay) {
    const overlayStr = `${overlay}`;

    return (
      <OverlayTrigger
        key={monitor.name}
        placement="bottom"
        overlay={
          <Tooltip id="tooltipid" className="monitor-panel-tooltip">
            {overlayStr.includes('\n') ? <pre>{overlayStr}</pre> : overlayStr}
          </Tooltip>
        }
      >
        <Col className="monitor-panel-item">
          <h1>{monitor.name}</h1>
          <div className={checkClass}>{`${value}`}</div>
        </Col>
      </OverlayTrigger>
    );
  }

  return (
    <Col key={monitor.name} className="monitor-panel-item">
      <h1>{monitor.name}</h1>
      <div className={checkClass}>{`${value}`}</div>
    </Col>
  );
}
