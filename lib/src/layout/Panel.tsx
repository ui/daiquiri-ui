import type { ReactChild, PropsWithChildren } from 'react';
import { Fragment } from 'react';
import FullSizer from '../components/FullSizer';

function PanelHeader(props: {
  children?: ReactChild | ReactChild[];
  style?: Record<string, any>;
}) {
  return (
    <div className="panel-header" style={props.style}>
      {props.children}
    </div>
  );
}

function PanelContents(props: {
  children?: ReactChild | ReactChild[];
  style?: Record<string, any>;
  className?: string;
}) {
  return (
    <div className={`panel-contents ${props.className}`} style={props.style}>
      {props.children}
    </div>
  );
}

interface Props {
  style?: Record<string, any>;
  scroll?: boolean;
  className?: string;
}

function Panel(props: PropsWithChildren<Props>) {
  const Wrap = props.scroll ? FullSizer : Fragment;

  return (
    <div className={`panel ${props.className ?? ''}`} style={props.style}>
      <Wrap>{props.children}</Wrap>
    </div>
  );
}

Panel.Header = PanelHeader;
Panel.Contents = PanelContents;

export default Panel;
