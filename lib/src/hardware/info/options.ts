import type { HardwareWidgetOptions } from '../utils/types';

export interface InfoOptions extends HardwareWidgetOptions {
  /** The property */
  property?: string;
  /** Unit for the property */
  unit?: string;
}
