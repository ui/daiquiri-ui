import type { Hardware, HardwareWidgetProps } from '../utils/types';
import type { InfoOptions } from './options';

export default function Info(
  props: HardwareWidgetProps<Hardware, InfoOptions>
) {
  const { hardware, options = {} } = props;
  function getValue() {
    const propertyName = options.property;
    if (propertyName === undefined) {
      return `property is not set`;
    }
    if (propertyName === null) {
      return `property is null`;
    }
    let result: any = hardware;
    for (const segment of propertyName.split('/')) {
      result = result[segment];
      if (result === undefined) {
        return `property '${propertyName}' not found`;
      }
    }
    return `${result}`;
  }

  return <>{getValue()}</>;
}
