import type { MouseEvent } from 'react';
import { map } from 'lodash';
import {
  Badge,
  Button,
  ButtonGroup,
  Container,
  Row,
  Col,
  OverlayTrigger,
  Popover,
} from 'react-bootstrap';

import TypeIcon from '../utils/TypeIcon';
import { toHoursMins } from '../../utils/formatting';
import type { HardwareWidgetProps } from '../utils/types';
import type { FrontendSchema } from '.';

function FrontendOverlay(props: HardwareWidgetProps<FrontendSchema>) {
  const { hardware } = props;
  function reset() {
    void hardware.actions.call('reset');
  }

  type StateEnum = 'feitlk' | 'expitlk' | 'pssitlk';

  const itlks: { [name: string]: StateEnum } = {
    'Front End': 'feitlk',
    Experimental: 'expitlk',
    PSS: 'pssitlk',
  };

  const ring = {
    Current: `${hardware.properties.current.toFixed(1)} mA`,
    Mode: hardware.properties.mode,
    Refill: toHoursMins(hardware.properties.refill),
    Message: <pre>{hardware.properties.message}</pre>,
  };

  function getPropertyState(key: StateEnum): string {
    if (!(key in hardware.properties)) {
      return 'UNKNOWN';
    }
    const state: any = hardware.properties[key];
    if (!state || typeof state !== 'string') {
      return 'UNKNOWN';
    }
    return state;
  }

  return (
    <OverlayTrigger
      trigger="click"
      rootClose
      overlay={
        <Popover id="popid">
          <Popover.Header>{hardware.name} Details</Popover.Header>
          <Popover.Body>
            <h6>Status</h6>
            <pre>{hardware.properties.status}</pre>
            <h6>Interlocks</h6>
            <Container>
              {map(itlks, (key, name) => (
                <Row key={name}>
                  <Col>{name}:</Col>
                  <Col>
                    <Badge
                      bg={getPropertyState(key) === 'ON' ? 'success' : 'danger'}
                    >
                      {getPropertyState(key)}
                    </Badge>
                  </Col>
                </Row>
              ))}
            </Container>

            <h6>Ring Status</h6>
            <Container>
              {map(ring, (prop, name) => (
                <Row key={name}>
                  <Col>{name}:</Col>
                  <Col>{prop}</Col>
                </Row>
              ))}
            </Container>

            <div className="d-grid gap-2">
              <Button disabled={props.disabled} onClick={reset}>
                Reset
              </Button>
            </div>
          </Popover.Body>
        </Popover>
      }
    >
      <Button className="flex-grow-0" title="Shutter Details">
        <i className="fa fa-ellipsis-h" />
      </Button>
    </OverlayTrigger>
  );
}

function Frontend(props: HardwareWidgetProps<FrontendSchema>) {
  const { hardware, options = {} } = props;
  function onClick() {
    void hardware.actions.call(
      hardware.properties.frontend === 'FE open' ? 'close' : 'open'
    );
  }

  return (
    <div className="hw-component">
      <div className="hw-head">
        <TypeIcon
          name="Frontend"
          icon="fam-hardware-frontend"
          online={hardware.online}
        />
        <div className="name">{hardware.name}</div>
        <Badge
          bg={
            hardware.properties.state === 'OPEN' ||
            hardware.properties.state === 'RUNNING'
              ? 'success'
              : 'danger'
          }
        >
          {hardware.properties.state}
        </Badge>
      </div>
      <div className="hw-content">
        <ButtonGroup className="d-flex">
          <Button
            variant={
              hardware.properties.frontend === 'FE open' ? 'danger' : 'success'
            }
            onClick={onClick}
            disabled={props.disabled}
          >
            {hardware.properties.frontend === 'FE open' ? 'Close' : 'Open'}
          </Button>
          <FrontendOverlay {...props} />
        </ButtonGroup>
      </div>
    </div>
  );
}

export default Frontend;
