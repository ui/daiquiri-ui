import type { Hardware } from '../utils/types';

export interface FrontendSchema extends Hardware {
  properties: {
    state: string;
    status: string;
    automatic: boolean;
    frontend: string;
    // current intensity
    current: number;
    // number of seconds since the last refill
    refill: number;
    mode: string;
    message: string;
    feitlk: string;
    pssitlk: string;
    expitlk: string;
  };
}
