import { useRef, useEffect } from 'react';
import { map } from 'lodash';
import { Badge, Button, InputGroup, Form } from 'react-bootstrap';

import TypeIcon from '../utils/TypeIcon';
import type { MultipositionSchema } from '.';
import type { HardwareWidgetProps } from '../utils/types';

export default function Multiposition(
  props: HardwareWidgetProps<MultipositionSchema>
) {
  const { hardware, options = {} } = props;
  const selectionRef = useRef<HTMLSelectElement>(null);

  useEffect(() => {
    if (!selectionRef.current) {
      console.error('selectionRef ref is unset');
      return;
    }
    selectionRef.current.value = hardware.properties.position;
  }, [hardware.properties.position]);

  function onMove(e: any) {
    console.debug('change', e);
    if (!selectionRef || !selectionRef.current) {
      console.error('selection ref is unset');
      return;
    }
    void hardware.actions.call('move', selectionRef.current.value);
  }

  function stop(e: any) {
    void hardware.actions.call('stop');
  }

  const opts = map(hardware.properties.positions, (p) => (
    <option key={p.position} title={p.description}>
      {p.position}
    </option>
  ));

  return (
    <div className="hw-component">
      <div className="hw-head">
        <TypeIcon
          name="Multiposition"
          icon="fam-hardware-multiposition"
          online={hardware.online}
        />
        <div className="name">{hardware.name}</div>
        <Badge
          bg={hardware.properties.state === 'READY' ? 'success' : 'warning'}
        >
          {hardware.properties.state}
        </Badge>
      </div>
      <div className="hw-content">
        <InputGroup>
          <Form.Control
            className="custom-select"
            as="select"
            ref={selectionRef}
            disabled={props.disabled}
            defaultValue={hardware.properties.position}
          >
            <option disabled>unknown</option>
            {opts}
          </Form.Control>
          {hardware.properties.state !== 'MOVING' && (
            <Button onClick={onMove} disabled={props.disabled}>
              Move
            </Button>
          )}

          {hardware.properties.state === 'MOVING' && !props.disabled && (
            <Button variant="danger" onClick={stop}>
              <i className="fa fa-times" />
            </Button>
          )}
        </InputGroup>
      </div>
    </div>
  );
}
