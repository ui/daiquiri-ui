import type { Hardware } from '../utils/types';

export interface MultipositionSchema extends Hardware {
  properties: {
    position: string;
    positions: {
      position: string;
      description: string;
      target: {
        axis: string;
        destination: number;
        tolerance: number;
      }[];
    }[];
    state: string;
  };
}
