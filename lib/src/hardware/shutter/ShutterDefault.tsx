import { Button, OverlayTrigger, Popover, ButtonGroup } from 'react-bootstrap';

import TypeIcon from '../utils/TypeIcon';
import HardwareTemplate from '../utils/HardwareTemplate';
import { ShutterState } from './State';
import type { ShutterSchema } from '.';
import type {
  HardwareWidgetProps,
  HardwareWidgetOptions,
} from '../utils/types';

function ShutterOverlay(
  props: HardwareWidgetProps<ShutterSchema, HardwareWidgetOptions>
) {
  const { hardware } = props;
  function reset() {
    void hardware.actions.call('reset');
  }

  return (
    <OverlayTrigger
      trigger="click"
      rootClose
      overlay={
        <Popover id="popid">
          <Popover.Header>{hardware.name} Details</Popover.Header>
          <Popover.Body>
            <h6>Status</h6>
            <pre>{hardware.properties.status}</pre>
            <div className="d-grid gap-2">
              <Button onClick={reset} disabled={props.disabled}>
                Reset
              </Button>
            </div>
          </Popover.Body>
        </Popover>
      }
    >
      <Button className="flex-grow-0" title="Shutter Details">
        <i className="fa fa-ellipsis-h" />
      </Button>
    </OverlayTrigger>
  );
}

export default function ShutterDefault(
  props: HardwareWidgetProps<ShutterSchema, HardwareWidgetOptions>
) {
  const { hardware, options = {} } = props;
  const widgetIcon = (
    <TypeIcon
      name="Shutter"
      icon="fam-hardware-shutter"
      online={hardware.online}
    />
  );

  const widgetState = <ShutterState hardware={hardware} />;

  function getState() {
    if (!hardware.properties) {
      return 'UNKNOWN';
    }
    return hardware.properties.state;
  }

  function getAction(s: string) {
    if (s === 'OPEN') {
      return ['Close', 'close'];
    }
    if (s === 'CLOSED') {
      return ['Open', 'open'];
    }
    if (s === 'DISABLED' || s === 'STANDBY' || s === 'FAULT') {
      return ['Closed', ''];
    }
    if (s === 'MOVING') {
      return ['...', ''];
    }
    return ['Unknown', ''];
  }

  const state = getState();
  const [label, action] = getAction(state);
  const variants: { [name: string]: string } = {
    OPEN: 'danger',
    CLOSED: 'success',
    MOVING: 'warning',
    DISABLED: 'secondary',
    STANDBY: 'danger',
    FAULT: 'fatal',
    UNKNOWN: 'warning',
  };
  const variant = variants[state] || 'danger';

  function onClick() {
    void hardware.actions.call(action);
  }

  const widgetContent = (
    <ButtonGroup className="d-flex flex-nowrap">
      <Button
        variant={variant}
        onClick={onClick}
        disabled={props.disabled || action === ''}
      >
        {label}
      </Button>

      {options.extended && <ShutterOverlay {...props} />}
    </ButtonGroup>
  );

  const headerMode = props.options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
