import type { ShutterSchema } from '.';
import { HardwareState } from '../utils/State';

export function ShutterState(props: {
  hardware: ShutterSchema;
  useReadyState?: boolean;
}) {
  const { hardware } = props;
  function getState() {
    if (!hardware.online) {
      return 'OFFLINE';
    }
    const s = hardware.properties.state;
    if (props.useReadyState && (s === 'OPEN' || s === 'CLOSED')) {
      return 'READY';
    }
    return s;
  }

  const state = getState();

  function getVariant() {
    switch (state) {
      case 'READY':
        return 'success';
      case 'OPEN':
        return 'success';
      case 'CLOSED':
        return 'danger';
      case 'DISABLED':
        return 'secondary';
      case 'MOVING':
      case 'STANDBY':
      case 'OFFLINE':
        return 'warning';
      case 'FAULT':
        return 'fatal';
      default:
        return 'fatal';
    }
  }

  return <HardwareState state={state} minWidth={6} variant={getVariant()} />;
}
