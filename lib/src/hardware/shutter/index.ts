import type { Hardware } from '../utils/types';

export interface ShutterSchema extends Hardware {
  properties: {
    state: string;
    status: string;
    valid: boolean;
    open_text: string;
    closed_text: string;
  };
}
