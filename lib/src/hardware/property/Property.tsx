import { InputGroup, Form, Badge } from 'react-bootstrap';

import TypeIcon from '../utils/TypeIcon';
import HardwareTemplate from '../utils/HardwareTemplate';
import type { GenericSchema } from '../utils/schema';
import type { HardwareWidgetProps } from '../utils/types';
import type { PropertyWidgetOptions } from './options';

function DeviceState(props: GenericSchema) {
  const state = props.online ? 'READY' : 'OFFLINE';
  return <Badge bg={state === 'READY' ? 'success' : 'warning'}>{state}</Badge>;
}

export default function Property(
  props: HardwareWidgetProps<GenericSchema, PropertyWidgetOptions>
) {
  const { hardware, options = {} } = props;
  const widgetIcon = (
    <TypeIcon name="Optic" icon="fa-cog" online={hardware.online} />
  );

  const widgetState = <DeviceState {...hardware} />;

  function getValue() {
    const propertyName = options.property;
    if (propertyName === undefined) {
      return `property is not set`;
    }
    if (propertyName === null) {
      return `property is null`;
    }
    let result: any = hardware;
    for (const segment of propertyName.split('/')) {
      result = result[segment];
      if (result === undefined) {
        return `property '${propertyName}' not found`;
      }
    }
    return `${result}`;
  }

  function getUnit() {
    const unitName = options.unit;
    if (!unitName) {
      return null;
    }
    if (!unitName.includes('properties')) {
      // That's a fixed unit defined as an option
      return unitName;
    }

    let result: any = props;
    for (const segment of unitName.split('/')) {
      result = result[segment];
      if (result === undefined) {
        return null;
      }
    }
    return `${result}`;
  }

  const unit = getUnit();

  const widgetContent = (
    <InputGroup>
      <Form.Control value={getValue()} readOnly />
      {unit && <InputGroup.Text>{unit}</InputGroup.Text>}
    </InputGroup>
  );

  const headerMode = props.options.header || 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
