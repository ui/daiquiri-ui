import type { HardwareWidgetOptions } from '../utils/types';

export interface PropertyWidgetOptions extends HardwareWidgetOptions {
  header?: string;
  property?: string;
  unit?: string;
}
