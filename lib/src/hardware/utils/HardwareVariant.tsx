import { Component } from 'react';
import type { ComponentType } from 'react';

interface HardwareVariantOptions {
  options: {
    variant: string;
  };
}

export default class HardwareVariant extends Component<
  HardwareVariantOptions,
  void
> {
  public variants: {
    [name: string]: ComponentType<any>;
  } = {};

  public render() {
    let Comp = this.variants.default;

    if (this.props.options.variant in this.variants) {
      Comp = this.variants[this.props.options.variant];
    }

    return <Comp {...this.props} />;
  }
}
