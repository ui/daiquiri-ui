import type { ComponentType } from 'react';
import type HardwareVariant from './HardwareVariant';
import type { Hardware } from './types';

export interface HardwareObjectProps {
  id: string;
  options: {
    variant?: string;
    header?: string;
    width?: number;
    colWidth?: number;
    emptyifnone?: boolean;
  };
  propertiesSchema?: Record<string, unknown>;
  callablesSchema?: Record<string, unknown>;
}

export interface ResolvedHardwareObjectProps extends HardwareObjectProps {
  operator?: boolean;
  runningScan?: boolean;
  obj: Hardware | null;
}

export interface ComponentMapping {
  [name: string]: ComponentType<any> | typeof HardwareVariant;
}
