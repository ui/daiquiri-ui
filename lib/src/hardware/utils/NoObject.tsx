import debug from 'debug';

import TypeIcon from './TypeIcon';
import HardwareTemplate from './HardwareTemplate';
import type { Hardware } from './types';

const logger = debug('daiquiri.components.hardware.NoObject');

interface NoObjectProps {
  id: string;
  name?: string;
  options: {
    header?: string;
    emptyifnone?: boolean;
  };
}

export default function NoObject(props: NoObjectProps) {
  if (props.options.emptyifnone) {
    logger(
      'Component id:"%s" name:"%s" not displayed cause setup with emptyifnone.',
      props.id,
      props.name
    );
    return <></>;
  }

  const widgetIcon = (
    <TypeIcon name="NoObject" icon="fam-hardware-any" online={false} />
  );

  const widgetState = <></>;

  const widgetContent = <>Missing</>;

  const headerMode = props.options ? props.options.header : 'top';

  const hardware: Hardware = {
    id: props.id,
    name: props.name ?? '',
    alias: null,
    user_tags: [],
    online: false,
    locked: null,
    properties: {},
    type: '',
  };

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={widgetContent}
      headerMode={headerMode}
    />
  );
}
