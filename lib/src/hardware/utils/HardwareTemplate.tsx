import type { ReactElement } from 'react';
import { Form, OverlayTrigger, Popover, Button } from 'react-bootstrap';
import type { Hardware } from './types';

interface Props {
  hardware: Hardware;
  headerMode?: string;
  widgetIcon: ReactElement;
  widgetState: ReactElement;
  widgetContent: ReactElement;
}

/**
 * Normalized way to compose hardware component in order to provide the same
 * kind of layout
 */
export default function HardwareTemplate(props: Props) {
  const { headerMode, hardware } = props;

  function getLabel() {
    if (hardware.alias) {
      return hardware.alias;
    }
    if (hardware.name) {
      return hardware.name;
    }
    return hardware.id;
  }

  const label = getLabel();

  switch (headerMode) {
    case 'front':
      return (
        <div className="hw-component">
          <div className="hw-single">
            {props.widgetIcon}
            <div className="name">{label}</div>
            <div className="d-inline-block">{props.widgetContent}</div>
          </div>
        </div>
      );
    case 'none':
      return (
        <div className="hw-component">
          <div className="hw-single">{props.widgetContent}</div>
        </div>
      );
    case 'state':
      return props.widgetState;
    // case 'top':
    default:
      return (
        <div className="hw-component">
          <div className="hw-head">
            {props.widgetIcon}
            <div className="name">
              <Form.Label htmlFor={hardware.id}>{label}</Form.Label>
            </div>
            {props.widgetState}
            {props.hardware.errors && props.hardware.errors.length > 0 && (
              <OverlayTrigger
                trigger="click"
                placement="bottom"
                rootClose
                overlay={
                  <Popover id={props.hardware.id} style={{ maxWidth: 500 }}>
                    <Popover.Header>Device Errors</Popover.Header>
                    <Popover.Body>
                      There were errors with properties on this device:
                      <ul>
                        {props.hardware.errors.map((error) => (
                          <li>
                            {error.property}:
                            <span className="stack-trace">
                              {error.traceback}
                              <br />
                              {error.exception}
                            </span>
                          </li>
                        ))}
                      </ul>
                    </Popover.Body>
                  </Popover>
                }
              >
                <Button variant="danger" size="sm">
                  <i className="fa fa-exclamation-triangle" />
                </Button>
              </OverlayTrigger>
            )}
          </div>
          <div className="hw-content">{props.widgetContent}</div>
        </div>
      );
  }
}
