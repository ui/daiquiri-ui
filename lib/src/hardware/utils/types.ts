interface HardwareError {
  property: string;
  exception: string;
  traceback: string;
}

export interface HardwareSchemaDescription {
  properties: {
    additionalProperties?: boolean;
    properties: { [property: string]: any };
    type: string;
    uischema: { position: [Record<string, unknown>] };
    uiorder?: string[];
  };
  callables: {
    additionalProperties?: boolean;
    properties: { [property: string]: any };
    type: string;
    uischema: { position: [Record<string, unknown>] };
    uiorder?: string[];
  };
}

export interface Hardware {
  /** Object name */
  name: string;
  /** Object id */
  id: string;
  /** An optional object alias */
  alias: string | null;
  /** A list of user tags */
  user_tags: string[];
  /** The object properties as defined in the relevant schema */
  properties: {
    [name: string]: any;
  };
  /** Whether the device is available */
  online: boolean;
  /** The reason why the hardware is locked, else null */
  locked: string | null;
  /** The object type as defined by the REST API */
  type: string;
  /** Any potential errors initialising this object */
  errors?: HardwareError[];
}

export interface HardwareChangeActions {
  setProperty: (name: string, value: any) => Promise<void>;
  call: (name: string, ...args: any[]) => Promise<void>;
}

/**
 * Common options exposed to user as yaml configuration for hardware widgets
 */
export interface HardwareWidgetOptions {
  /** Whether to show the extended popup */
  extended?: boolean;
  /** Kind of header displayed */
  header?: string;
}

export type EditableHardware<H extends Hardware = Hardware> = H & {
  /** Call the API to make a change on the hardware object */
  actions: HardwareChangeActions;
};

/**
 * Properties which are passed to the widgets
 */
export interface HardwareWidgetProps<
  H extends Hardware = Hardware,
  O extends HardwareWidgetOptions = HardwareWidgetOptions
> {
  /** Dynamic state of the hardware */
  hardware: EditableHardware<H>;
  /** Static schema describing the hardware */
  schema: HardwareSchemaDescription;
  /** Options passed to the widget by the yaml configuration */
  options: O;
  /** Global state of the widget (aggregating scan/operator/hardware states) */
  disabled: boolean;
  /** True if the session have the control */
  operator: boolean;
}
