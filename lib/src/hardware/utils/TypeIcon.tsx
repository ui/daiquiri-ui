export interface Props {
  online: boolean;
  activeMessage?: string;
  inactiveMessage?: string;
  message?: string;
  icon: string;
  name: string;
}

export function OnlineStatus(props: Props) {
  const {
    activeMessage = 'Online',
    inactiveMessage = 'Offline',
    message = 'This device is',
  } = props;

  return (
    <div
      className={`dot-indicator small bg-${
        props.online ? 'success' : 'danger'
      }`}
      title={`${message} ${props.online ? activeMessage : inactiveMessage}`}
    />
  );
}

export default function TypeIcon(props: Props) {
  const { name, icon } = props;
  return (
    <div className="icon" title={name}>
      <i className={`fa ${icon}`} />
      <OnlineStatus {...props} />
    </div>
  );
}
