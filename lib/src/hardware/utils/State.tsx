import { Badge, Col, OverlayTrigger, Row, Tooltip } from 'react-bootstrap';

/**
 * Normalize the way to display an hardware state.
 *
 * If `minWidth` is pecified (in `em`) it ensure the widget width will stay the
 * same when the state change
 */
export function HardwareState(props: {
  state: string;
  variant: string;
  minWidth?: number;
  description?: string;
}) {
  const style = {
    display: 'inline-block',
    minWidth: '',
  };

  if (props.minWidth) style.minWidth = `${props.minWidth}em`;
  return (
    <div style={style}>
      <Badge bg={props.variant} title={props.description}>
        {props.state}
      </Badge>
    </div>
  );
}

/**
 * Normalize the way to display an hardware exposing multiple states.
 *
 * - `states` is the states exposed by the hardware.
 * - `variants` contains a variant for each of this states, else `fatal` is used.
 * - `descriptions` can be provided to add a description to the state.
 *
 * If `minWidth` is pecified (in `em`) it ensure the widget width will stay the
 * same when the state change
 */
export function HardwareMultiState(props: {
  states: string[];
  variants?: Record<string, string>;
  descriptions?: Record<string, string>;
  minWidth?: number;
}) {
  const { states, variants = {}, descriptions = {}, minWidth } = props;

  const mainState = states[0] ?? '';

  if (states.length === 0) {
    // No state is not supposed to happen
    // At least for now there is no use case
    return (
      <HardwareState
        state="NoState"
        variant="fatal"
        description="The device no not expose any state"
        minWidth={minWidth}
      />
    );
  }

  if (states.length === 1) {
    // Fallback to a single state, as result the tooltip is smaller
    return (
      <HardwareState
        state={mainState}
        variant={variants[mainState] ?? 'fatal'}
        description={descriptions[mainState]}
        minWidth={minWidth}
      />
    );
  }

  const style = {
    display: 'inline-block',
    minWidth: minWidth ? `${minWidth}em` : '',
  };

  function renderTooltip(props: any) {
    return (
      <Tooltip id="device-state" {...props}>
        {states.map((s, i) => {
          const desc = descriptions[s];
          // Assume a state starting with `_` is a custom state
          // which have to be displayed without this underscore
          const name = s.startsWith('_') ? s.slice(1) : s;
          return (
            <Row className="gx-0">
              <Col sm="3" className="text-end">
                <Badge className="text-right" bg={variants[s] ?? 'fatal'}>
                  {name}
                </Badge>
              </Col>
              <Col sm="9" className="text-start lh-1 my-auto ps-1">
                {desc}
              </Col>
            </Row>
          );
        })}
      </Tooltip>
    );
  }

  return (
    <div style={style}>
      <OverlayTrigger
        placement="top"
        delay={{ show: 250, hide: 400 }}
        overlay={renderTooltip}
      >
        <span className="text-nowrap">
          <Badge className="rounded-end-0" bg={variants[mainState] ?? 'fatal'}>
            {mainState}
          </Badge>
          <Badge className="rounded-start-0" bg="secondary">
            +{states.length - 1}
          </Badge>
        </span>
      </OverlayTrigger>
    </div>
  );
}
