import type { Hardware } from './types';

export interface GenericSchema extends Hardware {
  properties: {
    state: string;
  };
}
