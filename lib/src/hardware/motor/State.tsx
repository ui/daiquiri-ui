import { useMemo } from 'react';
import type { MotorSchema } from '.';
import { HardwareMultiState } from '../utils/State';

interface State {
  description?: string;
  variant: string;
  priority: number;
}

/** Boiler plate */
const UNKNOWN: State = {
  description: 'Unknown state is exposed',
  variant: 'fatal',
  priority: 100,
};

/**
 * Available default state.
 *
 * The list is ordered by priority: the first will override the second.
 *
 * More 'dynamic' states can also be exposed.
 */
const AVAILABLE_STATES: Record<string, State> = {
  // Reserved by Daiquiri
  OFFLINE: {
    description: 'Axis is offline',
    variant: 'secondary',
    priority: 0,
  },
  // Real hardware state
  MOVING: {
    description: 'Axis is moving',
    variant: 'warning',
    priority: 10,
  },
  FAULT: {
    description: 'Axis is in fault',
    variant: 'danger',
    priority: 20,
  },
  // Reserved by Daiquiri
  LOCKED: {
    description: 'Axis is locked',
    variant: 'warning',
    priority: 25,
  },
  OFF: {
    description: 'Power is off',
    variant: 'secondary',
    priority: 30,
  },
  DISABLED: {
    description: 'Axis is disabled',
    variant: 'warning',
    priority: 40,
  },
  LOWLIMIT: {
    description: 'Low limit is reached',
    variant: 'warning',
    priority: 50,
  },
  HIGHLIMIT: {
    description: 'High limit is reached',
    variant: 'warning',
    priority: 51,
  },
  HOME: {
    description: 'Home signal active',
    variant: 'info',
    priority: 52,
  },
  READY: {
    description: 'Ready to handle request',
    variant: 'success',
    priority: 60,
  },
  // Reserved by Daiquiri
  NOT_READY: {
    description: 'Not ready',
    variant: 'warning',
    priority: 100,
  },
  // Reserved by Daiquiri
  UNKNOWN,
};

/**
 * A motor have a set of flags as state.
 *
 * For example an axis can expose `READY` and `MOVING` at the same time.
 * It means the controller is ready to change it's target, even
 * during a motion.
 *
 * For now we only display a "main" state.
 */
export function MotorState(props: { hardware: MotorSchema }) {
  const { hardware } = props;
  const { locked = null } = hardware;
  const { state = ['UNKNOWN'] } = hardware.properties;

  const [states, descriptions] = useMemo(() => {
    if (!hardware.online) {
      return [['OFFLINE'], undefined];
    }
    function priority(n1: string, n2: string) {
      const s1: State = AVAILABLE_STATES[n1] ?? UNKNOWN;
      const s2: State = AVAILABLE_STATES[n2] ?? UNKNOWN;
      return s1.priority - s2.priority;
    }
    const orderedStates = [
      ...state,
      ...(locked !== null ? ['LOCKED'] : []),
      ...(state.length === 0 ? ['NOT_READY'] : []),
    ];
    orderedStates.sort(priority);

    function splitCustomStates(s: string): [string, string | undefined] {
      const elems = s.split(':', 2);
      if (elems.length === 1) {
        return [s, undefined];
      }
      // @ts-expect-error
      return elems;
    }

    const descriptions: Record<string, string> = {};
    const states = orderedStates.map((s) => {
      if (s.startsWith('_')) {
        const desc = splitCustomStates(s);
        if (desc[1] !== undefined) {
          descriptions[desc[0]] = desc[1];
        }
        return desc[0];
      }
      if (s === 'LOCKED') {
        descriptions[s] = `Locked by ${locked}`;
      } else {
        descriptions[s] = AVAILABLE_STATES[s].description ?? '';
      }
      return s;
    });

    return [states, descriptions];
  }, [state, hardware.online, locked]);

  return (
    <HardwareMultiState
      states={states}
      descriptions={descriptions}
      minWidth={6}
      variants={Object.fromEntries(
        Object.entries(AVAILABLE_STATES).map(([n, s]) => [n, s.variant])
      )}
    />
  );
}
