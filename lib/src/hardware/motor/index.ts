import { useMemo } from 'react';
import type { Hardware } from '../utils/types';

export interface MotorSchema extends Hardware {
  properties: {
    position: number | null;
    target: number;
    tolerance: number;
    acceleration: number;
    velocity: number;
    limits: [number, number];
    state: string[];
    unit: string;
    offset: number;
    sign: number;
    display_digits: number | null;
  };
}

/**
 * Exposes the standard supported motor states
 */
export interface MotorStates {
  // Axis is READY
  READY?: true;
  // Axis is MOVING
  MOVING?: true;
  // Error from controller
  FAULT?: true;
  // Hardware high limit active
  HIGHLIMIT?: true;
  // Hardware low limit active
  LOWLIMIT?: true;
  // Home signal active
  HOME?: true;
  // Axis power is off
  OFF?: true;
  // Axis cannot move
  DISABLED?: true;
  // The state of the motor is not handlable
  UNKNOWN?: true;
  // If none of the state are set, this flag is set, to simplify the logic
  NOT_READY?: true;
}

/**
 * Normalize the motor state exposed by the hardware.
 *
 * This does not exposes the custom states in a useful maner.
 *
 * Store states are changed depending of existing user tags
 *
 * - `auto_on`: This allow to move the motor even if `OFF` is enabled.
 *              When `OFF` is enabled `READY` is turned to true.
 * - `auto_ready`: This allow to move the motor even if `NOT_READY` is enabled.
 *
 * TODO: An improvement would be to normalize that when redux is populated.
 *       instead of storing a list.
 *
 * @param state: List of the controller state
 * @param userTags: List of the hardware user tags
 */
export function useMotorStates(
  state: string[] | undefined,
  userTags: string[] = []
): MotorStates {
  return useMemo(() => {
    const autoReady = userTags.includes('auto_ready');
    const autoOn = userTags.includes('auto_on');

    if (state === undefined) {
      return { UNKNOWN: true };
    }
    if (state?.length === 0) {
      if (autoReady) {
        return { READY: true };
      }
      return { NOT_READY: true };
    }
    const newState = state.reduce((reduced: Record<string, boolean>, s) => {
      reduced[s] = true;
      return reduced;
    }, {});
    if (newState.READY === undefined) {
      if (autoOn && newState.OFF) {
        newState.READY = true;
      }
    }
    return newState;
  }, [state, userTags]);
}
