import type { HardwareWidgetOptions } from '../utils/types';

export interface MotorDefaultOptions extends HardwareWidgetOptions {
  /** Default step size to use for up / down arrows */
  step?: number;
  /** Array of selectable step sizes */
  steps?: number[];
  /** Number of decimals to show */
  precision?: number;
  /** Whether to show popup to configure extended parameters */
  extended?: boolean;
  /** Whether this widget is read only */
  readOnly?: boolean;
  /** Kind of header displayed */
  header?: string;
  /** Specify a fontawesome to inc the value */
  incicon?: string;
  /** Specify a fontawesome to dec the value */
  decicon?: string;
  /** If true inc and dec icon are swapped */
  swapincdec?: boolean;
  /** Show the step arrows horizontally instead of vertically */
  horizontalarrows?: boolean;
  /** Show large step arrows */
  largearrows?: boolean;
}
