import {
  Button,
  Form,
  OverlayTrigger,
  InputGroup,
  Popover,
} from 'react-bootstrap';

import TypeIcon from '../utils/TypeIcon';
import HardwareTemplate from '../utils/HardwareTemplate';
import { MotorState } from './State';
import HardwareNumericStep from '../../components/HardwareNumericStep';
import HardwareInputNumber from '../../components/HardwareInputNumber';
import { useMotorStates } from '.';
import type { MotorSchema, MotorStates } from '.';
import type { HardwareWidgetProps, EditableHardware } from '../utils/types';
import type { MotorDefaultOptions } from './options';

interface MotorOverlayProps {
  hardware: EditableHardware<MotorSchema>;
  disabled: boolean;
  readOnly?: boolean;
  states: MotorStates;
}

function MotorOverlay(props: MotorOverlayProps) {
  const { states } = props;
  function onMoveVelocityRequested(target: number) {
    return props.hardware.actions.setProperty('velocity', target);
  }

  function onMoveAccelerationRequested(target: number) {
    return props.hardware.actions.setProperty('acceleration', target);
  }

  return (
    <OverlayTrigger
      trigger="click"
      rootClose
      overlay={
        <Popover id="popid">
          <Popover.Header>{props.hardware.name} details</Popover.Header>
          <Popover.Body>
            <Form.Group>
              <Form.Label>Velocity</Form.Label>
              <HardwareInputNumber
                hardwareValue={props.hardware.properties?.velocity ?? 0}
                onMoveRequested={onMoveVelocityRequested}
                hardwareIsDisabled={props.disabled}
                hardwareIsReady={states.READY}
                hardwareIsMoving={states.MOVING}
                readOnly={props.readOnly}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Acceleration</Form.Label>
              <HardwareInputNumber
                hardwareValue={props.hardware.properties?.acceleration ?? 0}
                onMoveRequested={onMoveAccelerationRequested}
                hardwareIsDisabled={props.disabled}
                hardwareIsReady={states.READY}
                hardwareIsMoving={states.MOVING}
                readOnly={props.readOnly}
              />
            </Form.Group>
          </Popover.Body>
        </Popover>
      }
    >
      <Button>
        <i className="fa fa-ellipsis-h" />
      </Button>
    </OverlayTrigger>
  );
}

/**
 * The default motor widget
 */
export default function MotorDefault(
  props: HardwareWidgetProps<MotorSchema, MotorDefaultOptions>
) {
  const { hardware, options = {} } = props;
  const { properties, user_tags } = hardware;
  function onAbortRequested() {
    void hardware.actions.call('stop');
  }

  function onMovePositionRequested(target: number) {
    return hardware.actions.call('move', target);
  }

  const states = useMotorStates(properties.state, user_tags);
  const widgetIcon = (
    <TypeIcon name="Motor" icon="fam-hardware-motor" online={hardware.online} />
  );

  const widgetState = <MotorState hardware={hardware} />;

  const headerMode = props.options ? props.options.header : 'top';

  return (
    <HardwareTemplate
      hardware={hardware}
      widgetIcon={widgetIcon}
      widgetState={widgetState}
      widgetContent={
        <InputGroup>
          <HardwareNumericStep
            id={hardware.id}
            hardwareValue={properties?.position ?? null}
            hardwareIsDisabled={props.disabled}
            hardwareIsReady={states.READY ?? false}
            hardwareIsMoving={states.MOVING ?? false}
            onMoveRequested={onMovePositionRequested}
            onAbortRequested={onAbortRequested}
            precision={
              options.precision ?? properties.display_digits ?? undefined
            }
            readOnly={options.readOnly}
            step={options.step}
            steps={options.steps}
            incIcon={options.incicon}
            decIcon={options.decicon}
            swapIncDec={options.swapincdec}
            horizontalArrows={options.horizontalarrows}
            largeArrows={options.largearrows}
          />
          {properties.unit && (
            <InputGroup.Text>{properties.unit}</InputGroup.Text>
          )}

          {!states.MOVING && options.extended && (
            <MotorOverlay
              hardware={hardware}
              disabled={props.disabled}
              readOnly={options.readOnly}
              states={states}
            />
          )}

          {states.MOVING && !props.disabled && !options.step && (
            <Button variant="danger" onClick={onAbortRequested}>
              <i className="fa fa-times" />
            </Button>
          )}
        </InputGroup>
      }
      headerMode={headerMode}
    />
  );
}
