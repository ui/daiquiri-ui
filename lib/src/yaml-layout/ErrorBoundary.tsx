import type { PropsWithChildren } from 'react';
import React, { Component } from 'react';
import { Container, Alert, Button } from 'react-bootstrap';
import {
  KeyError,
  MissingKeysError,
  UnknownKeysError,
  getYamlHierarchy,
  getYamlRoot,
  LayoutError,
} from './utils';

function DefaultErrorMessage(props: {
  message: string;
  error: Error | undefined;
}) {
  return (
    <Alert variant="danger">
      {props.message}
      <br />
      <pre>{props.error?.stack}</pre>
    </Alert>
  );
}

function LayoutErrorMessage(props: {
  message: string;
  error: Error | undefined;
}) {
  const { error } = props;
  if (!(error instanceof LayoutError)) {
    return <></>;
  }
  function formatPreview(
    error: MissingKeysError | UnknownKeysError | KeyError,
    indent = 3
  ) {
    const path = getYamlHierarchy(error.yamlNode);
    const lines: string[] = [];
    let actualIndentation = '';
    for (let i = 1; i < path.length; i++) {
      const { node, indexFromParent } = path[i];
      lines.push(`${actualIndentation}children:`);
      actualIndentation += ' '.repeat(indent);
      if (indexFromParent === 1) {
        lines.push(`${actualIndentation}- type: ...`);
      } else if (indexFromParent ?? 0 > 1) {
        lines.push(
          `${actualIndentation}...`,
          `${actualIndentation}...`,
          `${actualIndentation}# children number ${indexFromParent ?? 0 + 1}`
        );
      }
      lines.push(`${actualIndentation}- type: ${node.type}`);
      actualIndentation += ' '.repeat(2);
    }
    if (error instanceof UnknownKeysError) {
      error.keys.forEach((k) => {
        lines.push(
          `${actualIndentation}${k}: ...  ${' '.repeat(
            50 - actualIndentation.length - k.length
          )}<-- unexpected key`
        );
      });
    } else if (error instanceof MissingKeysError) {
      error.keys.forEach((k) => {
        lines.push(
          `${actualIndentation}???:  ${' '.repeat(
            50 - actualIndentation.length - k.length
          )}<-- key **${k}** is expected`
        );
      });
    } else if (error instanceof KeyError) {
      lines.push(
        `${actualIndentation}${error.key}:  ${' '.repeat(
          50 - actualIndentation.length - error.key.length
        )}<-- ${error.keyMessage}`
      );
    }
    return lines.join('\n');
  }
  const layoutRoot = getYamlRoot(error.yamlNode);
  const layoutName = layoutRoot.name;

  if (error instanceof UnknownKeysError) {
    const key = error.keys.length === 1 ? 'key' : 'keys';
    return (
      <Alert variant="danger">
        Unexpected {key}{' '}
        {error.keys.map((k, i) => {
          return (
            <>
              {i !== 0 && ', '}
              <b>{k}</b>
            </>
          );
        })}{' '}
        from the layout name <b>{layoutName}</b>.
        <br />
        <pre className="mt-3">{formatPreview(error)}</pre>
        <br />
      </Alert>
    );
  }
  if (error instanceof MissingKeysError) {
    const key = error.keys.length === 1 ? 'Key' : 'Keys';
    return (
      <Alert variant="danger">
        Expected {key}{' '}
        {error.keys.map((k, i) => {
          return (
            <>
              {i !== 0 && ', '}
              <b>{k}</b>
            </>
          );
        })}{' '}
        from the layout name <b>{layoutName}</b>.
        <br />
        <pre className="mt-3">{formatPreview(error)}</pre>
        <br />
      </Alert>
    );
  }
  if (error instanceof KeyError) {
    return (
      <Alert variant="danger">
        Error on key <b>{error.key}</b> from the layout name <b>{layoutName}</b>
        .
        <br />
        <pre className="mt-3">{formatPreview(error)}</pre>
        <br />
      </Alert>
    );
  }
  return <DefaultErrorMessage {...props} />;
}

interface State {
  hasError: boolean;
  message: string;
  error?: Error;
}

interface Props {
  resetKey?: any;
  unhandledComponentDidCatch?: (
    error: Error,
    setStateCallback: (data: Record<string, any>) => void
  ) => void;
  UnhandledErrorComponent?: React.ComponentType<any>;
}

const INITIAL_STATE = {
  hasError: false,
  message: '',
  error: undefined,
};

export default class ErrorBoundary extends Component<
  PropsWithChildren<Props>,
  State
> {
  public constructor(props: Props) {
    super(props);
    this.state = INITIAL_STATE;
  }

  public static getDerivedStateFromError() {
    return { hasError: true };
  }

  private readonly reload = () => {
    window.location.reload();
  };

  public componentDidCatch(error: Error) {
    if (
      error instanceof LayoutError ||
      !this.props.unhandledComponentDidCatch
    ) {
      this.setState({
        hasError: true,
        message: error.message,
        error,
      });
      return;
    }

    this.props.unhandledComponentDidCatch?.(error, (data) => {
      this.setState({
        hasError: true,
        message: error.message,
        error,
        ...data,
      });
    });
  }

  // https://github.com/bvaughn/react-error-boundary/blob/23a4d779744f3079dfe0960acaa4bdfd5dede87b/src/ErrorBoundary.ts#L53
  public componentDidUpdate(prevProps: Props, prevState: State) {
    if (
      this.state.hasError &&
      prevState.error !== undefined &&
      prevProps.resetKey !== this.props.resetKey
    ) {
      this.setState(INITIAL_STATE);
    }
  }

  public render() {
    if (this.state.hasError) {
      const { UnhandledErrorComponent } = this.props;
      const { hasError, ...props } = this.state;
      return (
        <Container className="mt-2">
          <h4>Something went wrong</h4>
          {this.state.error instanceof LayoutError && (
            <LayoutErrorMessage
              message={this.state.message}
              error={this.state.error}
            />
          )}
          {!(this.state.error instanceof LayoutError) && (
            <>
              {UnhandledErrorComponent ? (
                <UnhandledErrorComponent {...props} />
              ) : (
                <DefaultErrorMessage
                  message={this.state.message}
                  error={this.state.error}
                />
              )}
            </>
          )}
          <p>
            This error has been logged. Please{' '}
            <Button size="sm" onClick={this.reload}>
              <i className="fa fa-refresh" /> Reload
            </Button>{' '}
            the application
          </p>
        </Container>
      );
    }
    return this.props.children;
  }
}
