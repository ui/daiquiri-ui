import { map } from 'lodash';
import Panel from '../layout/Panel';
import type { ReactChild } from 'react';
import { assertOnRemainingKeys, renderYamlNode } from './utils';
import type { YamlNode } from './model';

interface YamlFormHeaderNode extends YamlNode {
  type: 'formheader';
  title?: string;
}

interface YamlFormRowNode extends YamlNode {
  type: string;
  title: string;
  component?: string;
  stateinheader?: boolean;
  titlealign?: string;
  id?: string;
  variant?: string;
  options?: Record<string, any>;
  even: boolean;
}

interface YamlFormNode extends YamlNode {
  children: (YamlFormHeaderNode | YamlFormRowNode)[];
  title?: string;
  [key: string]: any;
}

function Header(props: { node: YamlFormHeaderNode; row: number }) {
  const { node } = props;
  const { type, title, ...remainingContent } = node;
  assertOnRemainingKeys(node, remainingContent);
  return (
    <div className="ymllayout-form-header" style={{ gridColumn: '1 / 3' }}>
      <h3>{title}</h3>
    </div>
  );
}

/**
 * Render the title of a row.
 *
 * It supports a fixed title, and can display formatted hardware id and state.
 */
function RowTitle(props: {
  node: YamlFormRowNode;
  state: ReactChild | null;
  row: number;
}) {
  const { node, state, row } = props;
  const { title } = node;

  function getId() {
    if (node.id) {
      return node.id;
    }
    return undefined;
  }

  function getText() {
    if (title !== undefined) {
      return title;
    }
    if (node.type === 'hardware') {
      const id = getId();
      const idContent = {
        _parentNode: node._parentNode,
        _indexFromParent: node._indexFromParent,
        type: 'hardware',
        id,
        variant: 'name',
        emptyifnone: true,
      };
      return renderYamlNode(idContent, 'a', false);
    }
    return '';
  }
  const text = getText();

  return (
    <div
      style={{
        textAlign: 'center',
      }}
    >
      <h5
        style={{
          margin: '0px',
          whiteSpace: 'nowrap',
        }}
      >
        {text}
      </h5>
      {state && <div style={{ position: 'relative', top: -5 }}>{state}</div>}
    </div>
  );
}

function Row(props: { node: YamlFormRowNode; row: number }) {
  const { node, row } = props;
  const {
    titlealign,
    title,
    stateinheader = false,
    variant,
    id,
    ...remainingContent
  } = node;
  let newContent;
  let state;
  if (remainingContent.type === 'hardware') {
    newContent = {
      ...remainingContent,
      id,
      variant,
    };
    if (stateinheader) {
      const stateContent = {
        ...remainingContent,
        id,
        variant: 'state',
        emptyifnone: true,
      };
      state = renderYamlNode(stateContent, 'k', false);
    } else {
      state = null;
    }
  } else {
    newContent = remainingContent;
    state = null;
  }
  return (
    <>
      <div
        style={{
          gridColumn: 1,
          alignSelf: titlealign ?? 'center',
        }}
      >
        <RowTitle node={node} state={state} row={row} />
      </div>
      <div style={{ gridColumn: 2, alignSelf: 'center' }}>
        {renderYamlNode(newContent, 'k', false)}
      </div>
    </>
  );
}

export default function YamlForm(props: {
  yamlNode: YamlFormNode;
  panel: boolean;
}) {
  const { yamlNode, panel } = props;
  const { type, children, options = {}, title, ...remainingContent } = yamlNode;
  assertOnRemainingKeys(yamlNode, remainingContent);

  return (
    <Panel className="ymllayout-form" {...options}>
      {yamlNode.title && <Panel.Header>{yamlNode.title}</Panel.Header>}
      <Panel.Contents className="full-sizer" style={{ overflow: 'auto' }}>
        <div
          style={{
            display: 'grid',
            gridTemplateColumns: 'min-content auto',
          }}
        >
          {map(children, (content, key) => {
            if (content.type === 'formheader') {
              return (
                <Header
                  node={content as YamlFormHeaderNode}
                  key={key}
                  row={key}
                />
              );
            }
            return (
              <Row node={content as YamlFormRowNode} key={key} row={key} />
            );
          })}
        </div>
      </Panel.Contents>
    </Panel>
  );
}
