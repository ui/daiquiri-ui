import type { ReactChild } from 'react';
import { Children, cloneElement, isValidElement } from 'react';
import Panel from '../layout/Panel';
import type { AnyYamlNode, YamlNode, YamlRoot } from './model';

/**
 * Store the mapping from Yaml `type` to React component handling the
 * rendering of such Yaml node
 */
export const yamlWrapperMap: Record<string, React.ComponentType<any>> = {};

/**
 * Register the Yaml type with the component used to render this Yaml node.
 */
export function registerYamlType(
  name: string,
  component: React.ComponentType<any>
) {
  yamlWrapperMap[name] = component;
}

/**
 * Render a Yaml node using registred component from the node type
 */
export function renderYamlNode(
  yamlNode: AnyYamlNode,
  key: number | string,
  panel = true
): ReactChild {
  // Allow null nodes to be ignored (useful for templating)
  if (!yamlNode) return <></>;
  const { type } = yamlNode;
  const Comp =
    type && type in yamlWrapperMap
      ? yamlWrapperMap[type]
      : yamlWrapperMap.component;
  return <Comp yamlNode={yamlNode} key={key} panel={panel} />;
}

interface OptionalPanelProps {
  children?: ReactChild | ReactChild[];
  [name: string]: any;
  scroll?: boolean;
  style?: Record<string, any>;
  panel: boolean;
  key?: number | string;
}

/**
 * Wrap the children into a panel if it is requested by the options
 */
export function OptionalPanel({
  children,
  key,
  panel,
  options,
}: OptionalPanelProps) {
  return panel ? (
    <Panel key={key} style={options.style} scroll={options.scroll}>
      <Panel.Contents>{children}</Panel.Contents>
    </Panel>
  ) : (
    <>
      {Children.map(children, (child) =>
        isValidElement(child)
          ? cloneElement(child, { key, ...child.props })
          : undefined
      )}
    </>
  );
}

export function assertOnRemainingKeys(
  yamlNode: YamlNode | undefined,
  remainingContent: Record<string, any>
) {
  const { _parentNode, _indexFromParent, ...remainingContent2 } =
    remainingContent;
  if (yamlNode) {
    if (Object.keys(remainingContent2).length > 0) {
      throw new UnknownKeysError(yamlNode, Object.keys(remainingContent2));
    }
  }
}

export class LayoutError extends Error {
  public readonly yamlNode: YamlNode;
  public constructor(message: string, yamlNode: YamlNode) {
    super(message);
    this.yamlNode = yamlNode;
  }
}

export class UnknownKeysError extends LayoutError {
  public readonly keys: string[];
  public constructor(yamlNode: YamlNode, unknownKeys: string[]) {
    super(`Unexpected keys: ${unknownKeys.join(', ')}`, yamlNode);
    this.keys = unknownKeys;
  }
}

export class MissingKeysError extends LayoutError {
  public readonly keys: string[];
  public constructor(yamlNode: YamlNode, missingKeys: string[]) {
    super(`Keys ${missingKeys.join(', ')} are missing`, yamlNode);
    this.keys = missingKeys;
  }
}

export class KeyError extends LayoutError {
  public readonly key: string;
  public readonly keyMessage: string;
  public constructor(yamlNode: YamlNode, key: string, message: string) {
    super(`Key ${key} error: ${message}`, yamlNode);
    this.key = key;
    this.keyMessage = message;
  }
}

/**
 * Returns the root of the YamlNode
 */
export function getYamlRoot(node: YamlNode): YamlRoot {
  let n = node;
  while (n._parentNode) {
    n = n._parentNode;
  }
  return n as YamlRoot;
}

/**
 * Returns a list of nodes from the root to this node.
 */
export function getYamlHierarchy(
  node: YamlNode
): { node: YamlNode; indexFromParent: number | null }[] {
  const path: { node: YamlNode; indexFromParent: number | null }[] = [];
  let n: YamlNode | null = node;
  while (n) {
    path.push({
      node: n,
      indexFromParent: n._indexFromParent,
    });
    n = n._parentNode;
  }
  path.reverse();
  return path;
}

/**
 * Create links between layout node in order to simplify further processing
 * (mainly error handling).
 */
export function normalizeLayout<R extends YamlNode>(
  node: R,
  parentNode: YamlNode | null = null,
  indexFromParent: number | null = null
): R {
  if (node._parentNode) {
    throw new Error(
      'Attribute _parentNode was already defined by the yml layout.'
    );
  }
  const newNode = {
    ...node,
    _parentNode: parentNode,
    _indexFromParent: indexFromParent,
  };
  if (node.children) {
    // NOTE: use a map from a real array
    const children = [...node.children];
    Object.freeze(children);
    newNode.children = children.map((c, i) => normalizeLayout(c, newNode, i));
  }
  Object.freeze(newNode);
  return newNode;
}
