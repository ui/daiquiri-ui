import type { ReactElement, CSSProperties } from 'react';
import { map } from 'lodash';
import Panel from '../layout/Panel';
import { renderYamlNode } from './utils';

interface Props {
  yamlNode: {
    children: any[];
    options?: Record<string, any>;
    title?: string;
  };
  panel: boolean;
}

export default function YamlPanel(props: Props): ReactElement {
  const { yamlNode } = props;
  const options = yamlNode.options || {};
  const contentsStyle: CSSProperties = {};
  if (options.centerContent) {
    contentsStyle.margin = 'auto';
  }

  return (
    <Panel className="ymllayout-panel" {...options}>
      {yamlNode.title && <Panel.Header>{yamlNode.title}</Panel.Header>}
      <Panel.Contents style={contentsStyle}>
        {map(yamlNode.children, (node, key) =>
          renderYamlNode(node, key, false)
        )}
      </Panel.Contents>
    </Panel>
  );
}
