import { Alert } from 'react-bootstrap';
import type { YamlNode } from '../model';
import { KeyError, MissingKeysError } from '../utils';
import {
  assertNoUnknownKeys,
  assertOptionalBoolean,
  assertOptionalString,
} from './asserts';
import type { YamlComponent } from '../model';

/**
 * Store the mapping from Yaml `type` to React component handling the
 * rendering of such Yaml node
 */
export const yamlHardwareComponent: Record<
  string,
  React.ComponentType<any>
> = {};

/**
 * Register the Yaml type with the component used to render this Yaml node.
 */
export function registerHardwareComponent(
  name: string,
  component: React.ComponentType<any>
) {
  yamlHardwareComponent[name] = component;
}

export interface HardwareDesc {
  id: string;
}

export type HardwareListDesc = HardwareDesc[];

export interface HardwareGroupProps {
  fetched: boolean;
  fetchedGroups: boolean;
  ids?: HardwareListDesc;
  groupid?: string;
  actions?: {
    fetch: () => void;
    fetchGroups: () => void;
  };
  even?: boolean;
  nowrap: boolean;
}

function assertHardwareDesc(
  yamlNode: YamlNode,
  value: any
): asserts value is HardwareDesc {
  if (value === undefined) {
    // FIXME: That's not a nice exception in this case, the rendering will be bad
    throw new MissingKeysError(yamlNode, ['id']);
  }
  if (typeof value.id !== 'string') {
    // FIXME: That's not a nice exception in this case, the rendering will be bad
    throw new KeyError(yamlNode, 'id', 'A string is expected');
  }
}

function assertOptionalHardwareListDesc(
  yamlNode: YamlNode,
  key: string,
  value: any
): asserts value is HardwareListDesc | undefined {
  if (value === undefined) {
    return;
  }
  if (!Array.isArray(value)) {
    throw new KeyError(yamlNode, key, 'A option list is expected');
  }

  value.forEach((hardwareDesc, i) => {
    try {
      assertHardwareDesc(yamlNode, hardwareDesc);
    } catch {
      // FIXME: For now we catch it, but assertHardwareDesc could do a better job
      throw new KeyError(
        yamlNode,
        key,
        `The hardware description ${i} is wrong`
      );
    }
  });
}

export default function YamlHardwareGroup(props: YamlComponent) {
  const {
    providers = {},
    yamlNode,
    ids,
    groupid,
    even,
    nowrap,
    ...unknownOptions
  } = props;
  assertOptionalHardwareListDesc(yamlNode, 'ids', ids);
  assertOptionalString(yamlNode, 'groupid', groupid);
  assertOptionalBoolean(yamlNode, 'even', even);
  assertOptionalBoolean(yamlNode, 'nowrap', nowrap);
  assertNoUnknownKeys(yamlNode, unknownOptions);

  if (!yamlHardwareComponent.HardwareGroup) {
    return (
      <Alert variant="warning">No `HardwareGroup` component registered</Alert>
    );
  }

  const HardwareGroup = yamlHardwareComponent.HardwareGroup;
  return (
    <HardwareGroup
      providers={providers}
      ids={ids}
      groupid={groupid}
      nowrap={nowrap || false}
      even={even || false}
    />
  );
}
