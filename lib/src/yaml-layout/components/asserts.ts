import type { YamlNode } from '../model';
import { KeyError, MissingKeysError, UnknownKeysError } from '../utils';

type Defined<T> = T extends undefined ? never : T;

export function assertKeyExpected<T>(
  yamlNode: YamlNode | undefined,
  key: string,
  value: T
): asserts value is Defined<T> {
  if (yamlNode) {
    if (value === undefined) {
      throw new MissingKeysError(yamlNode, [key]);
    }
  }
}

export function assertString(
  yamlNode: YamlNode,
  key: string,
  value: any
): asserts value is string {
  if (typeof value !== 'string') {
    throw new KeyError(yamlNode, key, 'A string is expected');
  }
}

export function assertBoolean(
  yamlNode: YamlNode,
  key: string,
  value: unknown
): asserts value is boolean {
  if (typeof value !== 'boolean') {
    throw new KeyError(yamlNode, key, 'A boolean is expected');
  }
}

export function assertNumber(
  yamlNode: YamlNode,
  key: string,
  value: unknown
): asserts value is number {
  if (typeof value !== 'number') {
    throw new KeyError(yamlNode, key, 'A number is expected');
  }
}

export function assertOptionalBoolean(
  yamlNode: YamlNode,
  key: string,
  value: unknown
): asserts value is boolean | undefined {
  if (value === undefined) {
    return;
  }
  if (typeof value !== 'boolean') {
    throw new KeyError(yamlNode, key, 'An optional boolean is expected');
  }
}

export function assertOptionalString(
  yamlNode: YamlNode,
  key: string,
  value: unknown
): asserts value is string | undefined {
  if (value === undefined) {
    return;
  }
  if (typeof value !== 'string') {
    throw new KeyError(yamlNode, key, 'An optional string is expected');
  }
}

export function assertStringList(
  yamlNode: YamlNode,
  key: string,
  value: unknown
): asserts value is string[] {
  if (!Array.isArray(value)) {
    throw new KeyError(yamlNode, key, 'A list of string is expected');
  }
  value.forEach((v) => {
    if (typeof v !== 'string') {
      throw new KeyError(yamlNode, key, 'A list of string is expected');
    }
  });
}

export function assertOptionalStringOrStringList(
  yamlNode: YamlNode,
  key: string,
  value: unknown
): asserts value is string | string[] | undefined {
  if (value === undefined) {
    return;
  }
  if (typeof value === 'string') {
    return;
  }
  if (!Array.isArray(value)) {
    throw new KeyError(
      yamlNode,
      key,
      'An optional string or list of string is expected'
    );
  }
  value.forEach((v) => {
    if (typeof v !== 'string') {
      throw new KeyError(
        yamlNode,
        key,
        'An optional string or list of string is expected'
      );
    }
  });
}

export function assertOptionalStringList(
  yamlNode: YamlNode,
  key: string,
  value: unknown
): asserts value is string[] | undefined {
  if (value === undefined) {
    return;
  }
  if (!Array.isArray(value)) {
    throw new KeyError(yamlNode, key, 'An optional list of string is expected');
  }
  value.forEach((v) => {
    if (typeof v !== 'string') {
      throw new KeyError(
        yamlNode,
        key,
        'A optional list of string is expected'
      );
    }
  });
}

export function assertOptionalNumber(
  yamlNode: YamlNode,
  key: string,
  value: unknown
): asserts value is number | undefined {
  if (value === undefined) {
    return;
  }
  if (typeof value !== 'number') {
    throw new KeyError(yamlNode, key, 'An optional number is expected');
  }
}

export function assertOptionalNumberList(
  yamlNode: YamlNode,
  key: string,
  value: unknown
): asserts value is string[] | undefined {
  if (value === undefined) {
    return;
  }
  if (!Array.isArray(value)) {
    throw new KeyError(
      yamlNode,
      key,
      'An optional list of numbers is expected'
    );
  }
  value.forEach((v) => {
    if (typeof v !== 'number') {
      throw new KeyError(
        yamlNode,
        key,
        'An optional list of numbers is expected'
      );
    }
  });
}

export function assertNoUnknownKeys(
  yamlNode: YamlNode | undefined,
  remainingContent: Record<string, any>
) {
  const { _parentNode, _indexFromParent, ...remainingContent2 } =
    remainingContent;
  if (yamlNode) {
    if (Object.keys(remainingContent2).length > 0) {
      throw new UnknownKeysError(yamlNode, Object.keys(remainingContent2));
    }
  }
}

export function assertDataCollectionId(
  yamlNode: YamlNode,
  key: string,
  value: unknown
): asserts value is number | 'last' {
  if (value === 'last') {
    return;
  }
  if (typeof value === 'number') {
    return;
  }
  throw new KeyError(yamlNode, key, '"last" or a number is expected');
}

export function assertOptionalBackend(
  yamlNode: YamlNode,
  key: string,
  value: unknown
): asserts value is 'plotly' | 'h5web' | undefined {
  if (value === undefined) {
    return;
  }
  if (value === 'h5web') {
    return;
  }
  if (value === 'plotly') {
    return;
  }
  throw new KeyError(yamlNode, key, '"h5web" or "plotly" string is expected');
}
