import React from 'react';
import { OptionalPanel } from './utils';

interface Props {
  yamlNode: {
    children: any[];
    options?: {
      [name: string]: any;
      scroll?: boolean;
      style?: Record<string, any>;
    };
    label?: string;
  };
  panel: boolean;
}

export default function YamlLabel({ yamlNode, panel }: Props) {
  const options = yamlNode.options || {};
  return (
    <OptionalPanel panel={panel} options={options}>
      <div>{yamlNode.label}</div>
    </OptionalPanel>
  );
}
