import { map } from 'lodash';
import { Col } from 'react-bootstrap';
import classNames from 'classnames';
import { renderYamlNode } from './utils';
import {
  assertNoUnknownKeys,
  assertOptionalNumber,
} from './components/asserts';
import type { AnyYamlNode } from './model';

interface Props {
  yamlNode: AnyYamlNode;
  panel: boolean;
}

export default function YamlCol({ yamlNode, panel }: Props) {
  const { type, style, xs, children, ...unknownOptions } = yamlNode;
  assertNoUnknownKeys(yamlNode, unknownOptions);
  assertOptionalNumber(yamlNode, 'xs', xs);
  return (
    <Col
      className={classNames('yamymllayout-col', { col: !!xs })}
      style={style}
      xs={xs}
    >
      {map(yamlNode.children, (node, key) => renderYamlNode(node, key, panel))}
    </Col>
  );
}
