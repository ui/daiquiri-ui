import React, { Suspense } from 'react';
import { Alert } from 'react-bootstrap';
import Panel from '../layout/Panel';
import type { YamlNode } from './model';
import YamlHardwareGroup from './components/HardwareGroup';

export const componentMap: Record<string, React.ComponentType<any>> = {
  hardwaregroup: YamlHardwareGroup,
};
export function registerComponent(
  name: string,
  component: React.ComponentType<any>
) {
  componentMap[name] = component;
}

export function registerComponents(
  map: Record<string, React.ComponentType<any>>
) {
  Object.entries(map).forEach(([key, component]) =>
    registerComponent(key, component)
  );
}

interface YamlComponentNode extends YamlNode {
  type: string;
  [name: string]: any;
  scroll?: boolean;
  style?: Record<string, any>;
  title?: string;
}

export default function YamlComponent(props: {
  yamlNode: YamlComponentNode;
  panel: boolean;
}) {
  const { yamlNode, panel } = props;
  const { type, style, scroll, title, ...remainingContent } = yamlNode;
  if (!(type in componentMap)) {
    return (
      <Panel>
        <Panel.Contents>
          <Alert variant="warning">Unknown component &quot;{type}&quot;</Alert>
        </Panel.Contents>
      </Panel>
    );
  }
  const Comp = componentMap[yamlNode.type];
  if (panel) {
    return (
      <Panel style={style} scroll={scroll}>
        <Suspense fallback={<p>Loading...</p>}>
          {title && <Panel.Header>{title}</Panel.Header>}
          <Panel.Contents>
            <Comp {...remainingContent} yamlNode={yamlNode} />
          </Panel.Contents>
        </Suspense>
      </Panel>
    );
  }
  return (
    <Suspense fallback={<p>Loading...</p>}>
      <Comp {...remainingContent} yamlNode={yamlNode} />
    </Suspense>
  );
}
