import { map } from 'lodash';
import { Row } from 'react-bootstrap';
import { renderYamlNode } from './utils';
import { assertNoUnknownKeys } from './components/asserts';
import type { AnyYamlNode } from './model';

interface Props {
  yamlNode: AnyYamlNode;
  panel: boolean;
}

export default function YamlRow({ yamlNode, panel }: Props) {
  const { type, style, children, ...unknownOptions } = yamlNode;
  assertNoUnknownKeys(yamlNode, unknownOptions);
  return (
    <Row className="ymllayout-row gx-0" style={style}>
      {map(children, (node, key) => renderYamlNode(node, key, panel))}
    </Row>
  );
}
