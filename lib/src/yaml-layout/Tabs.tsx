import { each } from 'lodash';
import { Tab, Tabs } from 'react-bootstrap';
import Panel from '../layout/Panel';
import { renderYamlNode } from './utils';

const hash = () => {
  return (
    Math.random().toString(36).slice(2, 15) +
    Math.random().toString(36).slice(2, 15)
  );
};

interface Props {
  yamlNode: {
    children: any[];
    options?: Record<string, any>;
    title: string;
  };
  panel: boolean;
}

export default function YamlTabs({ yamlNode, panel }: Props) {
  const tabs: React.ReactElement[] = [];
  each(yamlNode.children, (t, i) => {
    tabs.push(
      <Tab key={i} eventKey={hash()} title={t.title}>
        {renderYamlNode(t, 0, false)}
      </Tab>
    );
  });

  return panel ? (
    <Panel className="ymllayout-tabs">
      {yamlNode.title && <Panel.Header>{yamlNode.title}</Panel.Header>}
      <Panel.Contents>
        <Tabs mountOnEnter>{tabs}</Tabs>
      </Panel.Contents>
    </Panel>
  ) : (
    <Tabs>{tabs}</Tabs>
  );
}
