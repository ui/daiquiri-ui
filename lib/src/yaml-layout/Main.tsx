import { useMemo } from 'react';
import { Alert } from 'react-bootstrap';
import Panel from '../layout/Panel';
import ErrorBoundary from './ErrorBoundary';
import YamlRow from './Row';
import YamlCol from './Col';
import YamlComponent from './Component';
import YamlContainer from './Container';
import YamlPanel from './Panel';
import YamlTabs from './Tabs';
import YamlForm from './Form';
import YamlGrid from './Grid';
import YamlLabel from './Label';
import { renderYamlNode, registerYamlType, normalizeLayout } from './utils';
import type { YamlRoot } from './model';

export const yamlMap = {
  row: YamlRow,
  col: YamlCol,
  container: YamlContainer,
  component: YamlComponent,
  panel: YamlPanel,
  tabs: YamlTabs,
  form: YamlForm,
  grid: YamlGrid,
  label: YamlLabel,
};

Object.entries(yamlMap).forEach(([type, YamlComponent]) => {
  registerYamlType(type, YamlComponent);
});

interface Props {
  testid: string;
  layout: YamlRoot;
  className?: string;
  unhandledComponentDidCatch?: (
    error: Error,
    setStateCallback: (data: Record<string, any>) => void
  ) => void;
  UnhandledErrorComponent?: React.ComponentType<any>;
}

export default function Main(props: Props) {
  const layout = useMemo(() => {
    return normalizeLayout(props.layout);
  }, [props.layout]);

  if (layout.error) {
    return (
      <div
        className={`${props.className ?? ''} ymllayout-main`}
        data-testid={props.testid}
      >
        <Panel>
          <Panel.Header>Layout Error</Panel.Header>
          <Panel.Contents>
            <p>File: {layout.name}</p>
            <Alert variant="warning">
              <pre className="mb-0">{layout.error}</pre>
            </Alert>
          </Panel.Contents>
        </Panel>
      </div>
    );
  }

  return (
    <div
      className={`${props.className ?? ''} ymllayout-main`}
      data-testid={props.testid}
    >
      <ErrorBoundary
        resetKey={layout}
        unhandledComponentDidCatch={props.unhandledComponentDidCatch}
        UnhandledErrorComponent={props.UnhandledErrorComponent}
      >
        {layout.children?.map((node, key) => renderYamlNode(node, key))}
      </ErrorBoundary>
    </div>
  );
}
