export interface YamlNode {
  type: string;
  children?: YamlNode[];

  // Reference to the parent node
  _parentNode: YamlNode | null;
  // Index of this node from the parent node
  _indexFromParent: number | null;
}

export interface YamlRoot extends YamlNode {
  name: string;
  error: string;
}

export interface AnyYamlNode extends YamlNode {
  [key: string]: any;
}

export interface YamlComponent {
  providers?: Record<string, any>;
  yamlNode: YamlNode;
  [option: string]: unknown;
}
