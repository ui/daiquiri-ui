import { map } from 'lodash';
import { Container } from 'react-bootstrap';
import { renderYamlNode } from './utils';

interface Props {
  yamlNode: {
    children: any[];
    options?: Record<string, any>;
  };
  panel: boolean;
}

export default function YamlContainer({ yamlNode, panel }: Props) {
  const options = yamlNode.options || {};
  return (
    <Container className="ymllayout-container" {...options}>
      {map(yamlNode.children, (node, key) => renderYamlNode(node, key, panel))}
    </Container>
  );
}
