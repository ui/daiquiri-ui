import { map } from 'lodash';
import { renderYamlNode } from './utils';
import type { YamlNode } from './model';

interface YamlGridNode extends YamlNode {
  children: YamlGridCellNode[];
  title?: string;
  columns?: string;
  rows?: string;
}

interface YamlGridCellNode extends YamlNode {
  columnSpan?: number;
  rowSpan?: number;
}

export default function YamlGrid(props: {
  yamlNode: YamlGridNode;
  panel: boolean;
}) {
  const { yamlNode, panel } = props;
  return (
    <div
      className="ymllayout-grid"
      style={{
        gridTemplateColumns: yamlNode.columns,
        gridTemplateRows: yamlNode.rows,
      }}
    >
      {map(yamlNode.children, (content, key) => {
        const { columnSpan = 1, rowSpan = 1, ...remainingContent } = content;
        const gridColumn = columnSpan > 1 ? `span ${columnSpan}` : undefined;
        const gridRow = rowSpan > 1 ? `span ${rowSpan}` : undefined;
        return (
          <div
            key={key}
            className="ymllayout-grid-cell"
            style={{
              gridColumn,
              gridRow,
            }}
          >
            {renderYamlNode(remainingContent, key, panel)}
          </div>
        );
      })}{' '}
    </div>
  );
}
