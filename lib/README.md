# @esrf/daiquiri-lib

Shared UI components and associated machinary from the [daiquiri-ui](https://gitlab.esrf.fr/ui/daiquiri-ui) project for beamline control and data acquisition. These shared components allow for a consistent look and feel for projects outside of the [daiquiri](https://ui.gitlab-pages.esrf.fr/daiquiri-landing/) core.

The library currently exports the YAML layout language, validator, and renderer, and a selection of hardware components.

See our [documentation](https://ui.gitlab-pages.esrf.fr/daiquiri-ui/daiquiri-lib) for more information.
